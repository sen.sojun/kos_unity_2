﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

using UnityEngine.Networking;
using UnityEngine.Networking.Types;
using UnityEngine.Networking.Match;

public class KOSLobbyManager : NetworkLobbyManager
{
    public enum MatchingStatus
    {
          Ready
        , Finding
        , Joining
        , Creating
        , Waiting
        , Matched
        , Canceling
    }
    
    public struct RoomDetail
    {
        public string ID;
        public float Time;
    };
    
    static private float _roomBlackListTime = 5.0f;
    
    static private KOSLobbyManager _instance = null;
    static public KOSLobbyManager Instance { get { return _instance; } }

    #region Properties
    private List<RoomDetail> _roomBlackList;
    
    private string _statusInfo;
    private string _hostInfo;
    private string _ip;
    private bool _isHost = false;
    private bool _isReady = false;
    private bool _isOnlineMode = false;
    private bool _isStartMatching = false;
    private bool _isMatchingTimeout = false;
    private bool _isInRoom = false;
    private bool _isVSBot = false;
    private float _matchingTimer = 0.0f;
    private MatchingStatus _matchingStatus = MatchingStatus.Ready;
    
    private List<string> _debugTextList;
    private List<KOSLobbyPlayer> _kosPlayers = new List<KOSLobbyPlayer>();
    
    protected bool _isMatchmaking = false;
    protected bool _disconnectServer = false;
    protected bool _isInGame = false;
    protected bool _isConnect = false;
    protected ulong _currentMatchID;
    
    private Coroutine _countdownCoroutine;
    #endregion

    #region Public Properties
    public float PreMatchCountdown = 5.0f;
    public float MatchTimeout = 30.0f;
    
    public MatchingStatus Status { get { return _matchingStatus; } }
    public bool IsStartMatching { get { return _isStartMatching; } }
    public bool IsInGame { get { return _isInGame; } }
    public bool IsConnect { get { return _isConnect; } }
    
    public string LobbyScene { get {return "LobbyScene"; } }
    public string PlayScene { get { return "NetworkBattleScene"; } }
    
    public List<KOSLobbyPlayer> KOSPlayers { get { return _kosPlayers; } }
    #endregion
    
    #region Start
    void Start()
    {
        _instance = this;
        
        offlineScene = "";
        onlineScene = "";
        SetLobbyScene(LobbyScene);
        playScene = PlayScene;
        
        DontDestroyOnLoad(gameObject);
    }
    #endregion

    private void Update()
    {
        if(!IsInGame)
        {
            if(IsStartMatching && Status != MatchingStatus.Ready)
            {
                if(!_isMatchingTimeout)
                {
                    _matchingTimer += Time.deltaTime;
                    
                    if(_matchingTimer >= MatchTimeout)
                    {
                        OnMatchingTimeout();
                    }
                }
            }
        }
        
        UpdateBlackList();
    }

    /*
    public void OnDestroy()
    {
        _instance = null;
    }
    */

    #region NetworkLobbyManager Methods       
    public override void OnLobbyClientConnect(NetworkConnection conn)
    {
        AddDebugText("OnLobbyClientConnect");
        base.OnLobbyClientConnect(conn);
    }
    
    public override void OnLobbyClientDisconnect(NetworkConnection conn)
    {
        AddDebugText("OnLobbyClientDisconnect");
        
        if(LobbyUIManager.Instance != null)
        {
            if (conn.lastError != NetworkError.Ok)
            {
                //if (LogFilter.logError)
                {
                    string errorText = string.Format(
                          "Disconnected due to error: {0}"
                        , conn.lastError
                    );
                    
                    AddDebugText(errorText);
                    
                    LobbyUIManager.Instance.ShowStatusMessage(
                          errorText
                        //, LobbyUIManager.Instance.ServerListUI.OnClickRefresh
                    );
                }
                
                //LobbyUIManager.Instance.HidePlayerList();
            }
        }
    
        if(_isReady)
        {
            _isReady = false;   
            if(!_isInGame)
            {
                OnReadyInterrupt();
            }
        }
        
        if(IsInGame)
        {
        }
        else
        {
            base.OnLobbyClientDisconnect(conn);
        }
    }

    public override void OnLobbyStartServer()
    {
        base.OnLobbyStartServer();
    }

    public override void OnLobbyClientSceneChanged(NetworkConnection conn)
    {
        string sceneName = SceneManager.GetSceneAt(0).name;
        AddDebugText("OnLobbyClientSceneChanged : " + sceneName);
    
        if(SceneManager.GetSceneAt(0).name == PlayScene)
        {           
            _isInGame = true;
        }
        else
        {
            _isInGame = false;
        }
                
        base.OnLobbyClientSceneChanged(conn);
    }
    
    public override void OnLobbyServerPlayerRemoved(NetworkConnection conn, short playerControllerId)
    {
        AddDebugText("OnLobbyServerPlayerRemoved");
        
        if(_isReady)
        {
            _isReady = false;   
            OnReadyInterrupt();
        }
        
        base.OnLobbyServerPlayerRemoved(conn, playerControllerId);
    }

    public override void OnLobbyServerConnect(NetworkConnection conn)
    {
        AddDebugText("OnLobbyServerConnect");
        base.OnLobbyServerConnect(conn);
    }

    public override void OnLobbyServerDisconnect(NetworkConnection conn)
    {
        AddDebugText("OnLobbyServerDisconnect");
    
        if(_isReady)
        {
            _isReady = false;   
            if(!_isInGame)
            {
                OnReadyInterrupt();
            }
        }
       
        if(IsInGame)
        {  
        }
        else
        {
            base.OnLobbyServerDisconnect(conn);
        }
    }

    public override void OnLobbyServerSceneChanged(string sceneName)
    {
        AddDebugText("OnLobbyServerSceneChanged : " + sceneName);
         
        base.OnLobbyServerSceneChanged(sceneName);
    }

    public override bool OnLobbyServerSceneLoadedForPlayer(GameObject lobbyPlayer, GameObject gamePlayer)
    {
        AddDebugText("OnLobbyServerSceneLoadedForPlayer " + lobbyPlayer.name);
    
        //This hook allows you to apply state data from the lobby-player to the game-player
        //just subclass "LobbyHook" and add it to the lobby object.
        
        /*
        if (_lobbyHooks)
            _lobbyHooks.OnLobbyServerSceneLoadedForPlayer(this, lobbyPlayer, gamePlayer);
        */
        
        DontDestroyOnLoad(gamePlayer);
        
        return true;
    }

    //we want to disable the button JOIN if we don't have enough player
    //But OnLobbyClientConnect isn't called on hosting player. So we override the lobbyPlayer creation
    public override GameObject OnLobbyServerCreateLobbyPlayer(NetworkConnection conn, short playerControllerId)
    {
        AddDebugText("OnLobbyServerCreateLobbyPlayer");
    
        //GameObject obj = Instantiate(lobbyPlayerPrefab.gameObject) as GameObject;
    
        /*
        LobbyPlayer newPlayer = obj.GetComponent<LobbyPlayer>();
        newPlayer.ToggleJoinButton(numPlayers + 1 >= minPlayers);


        for (int i = 0; i < lobbySlots.Length; ++i)
        {
            LobbyPlayer p = lobbySlots[i] as LobbyPlayer;

            if (p != null)
            {
                p.RpcUpdateRemoveButton();
                p.ToggleJoinButton(numPlayers + 1 >= minPlayers);
            }
        }
        */

        return null;
    }
    
    // --- Countdown management
    public override void OnLobbyServerPlayersReady()
    {
        AddDebugText("OnLobbyServerPlayersReady");

        bool allready = true;
        for(int i = 0; i < lobbySlots.Length; ++i)
        {
            if(lobbySlots[i] != null)
                allready &= lobbySlots[i].readyToBegin;
        }

        if(allready)
        {
            _isReady = true;
            
            KOSLobbyPlayer host  = lobbySlots[0] as KOSLobbyPlayer;
            host.CmdSetReady(_isReady);
            
            _countdownCoroutine = StartCoroutine(ServerCountdownCoroutine());
        }
        
        _isMatchingTimeout = true;
    }
    
    public void SetReady(bool isReady)
    {
        _isReady = isReady;
        if(_isReady)
        {
            _matchingStatus = MatchingStatus.Matched;
        }
    }
    
    public void OnReadyInterrupt()
    {   
        if(_countdownCoroutine != null)
        {
            StopCoroutine(_countdownCoroutine);
            _countdownCoroutine = null;
        }

        //SetLobbyScene(LobbyScene);
        
        if(_isHost)
        {          
            AddDebugText("OnReadyInterrupt : RequestStopHost");        
     
            RequestStopHost();
        }
        else
        {      
            if (_isMatchmaking)
            {                
                matchMaker.DestroyMatch((NetworkID)_currentMatchID, 0, null);
            }
            
            AddDebugText("OnReadyInterrupt : RequestStopClient");
        
            RequestStopClient();
        }
    
        //LobbyUIManager.Instance.LoadScene(lobbyScene);
    }
    
    public override void ServerChangeScene(string sceneName)
    {    
        AddDebugText("ServerChangeScene " + sceneName);
    
        if(SceneManager.GetActiveScene().name == LobbyScene)
        {
            // Current in lobby scene.
            if(sceneName != LobbyScene)
            {
                // New scene is not lobby.  
                if(LobbyUIManager.Instance != null)
                {
                    LobbyUIManager.Instance.ShowLoading(true); 
                }                
     
                base.ServerChangeScene(sceneName);
            }
        }
        else
        {
            // Not in lobby scene.
            base.ServerChangeScene(sceneName);
        }
    }    
    #endregion
    
    #region NetworkManager Methods   
    public void SetLobbyScene(string sceneName)
    {
        lobbyScene = sceneName;
        offlineScene = sceneName;
        
        Debug.LogFormat("SetLobbyScene : {0} {1}", lobbyScene, offlineScene);
    }
    
    public override void OnStartHost()
    {
        _ip = Network.player.ipAddress;
        
        string roomStatus = "";
        if(_isMatchmaking)
        {
            roomStatus = string.Format("{0}"
                , this.matchName
            );
        }
        else
        {
            roomStatus = string.Format(
                  LocalizationManager.GetText("LOBBY_PLAYER_ROOM_IP")
                , Network.player.ipAddress
            );
        }
    
        AddDebugText(string.Format(
              "OnStartHost: {0}"
            , roomStatus
        ));
        
        if(IsStartMatching)
        {
            _matchingStatus = MatchingStatus.Waiting;
        }
                
        _isHost = true;
        _isInRoom = true;

        SetLobbyScene(LobbyScene);
        base.OnStartHost();
        
         //LobbyUIManager.Instance.BackDelegate = this.RequestStopHost;    
         
        if(!_isMatchmaking)  
        {
            if(LobbyUIManager.Instance != null)
            {          
                LobbyUIManager.Instance.ShowPlayerList(roomStatus, this.RequestStopHost);
            }
        }
    }
    
    public override void OnStopHost()
    {
        AddDebugText("OnStopHost");
        
        SetLobbyScene(""); // Ensures we don't reload the scene after quitting        
        base.OnStopHost();
        // lobbyScene = LobbyScene;
        
        _isHost = false;
        _isInRoom = false;
        _kosPlayers.Clear();
        
        if(!_isOnlineMode)  
        {
            if(LobbyUIManager.Instance != null)
            {          
                LobbyUIManager.Instance.ShowLocalLobby();
            }
        }
        else
        {
            OnCompletelyCancelMatching();
            
            if(LobbyUIManager.Instance != null && !_isVSBot)
            {
                if(!LobbyUIManager.Instance.MainUI.gameObject.activeSelf)
                {
                    LobbyUIManager.Instance.ShowOnlineMatch();
                }
            }
        }
    }
    
    public override void OnStartClient(NetworkClient lobbyClient)
    {
        AddDebugText("OnStartClient");
        
        if(IsStartMatching)
        {
            _matchingStatus = MatchingStatus.Waiting;
        }
         
        _isInRoom = true;
        SetLobbyScene(LobbyScene);
        base.OnStartClient(lobbyClient);
    }
    
    public override void OnStopClient()
    {
        AddDebugText("OnStopClient");
        
        SetLobbyScene(""); // Ensures we don't reload the scene after quitting    
        base.OnStopClient();
        // lobbyScene = LobbyScene;
        
        _isInRoom = false;
        
        _kosPlayers.Clear();
        
        if(!_isOnlineMode)  
        {
            if(LobbyUIManager.Instance != null)
            {          
                LobbyUIManager.Instance.ShowLocalLobby();
            }
        }
        else
        {
            OnCompletelyCancelMatching();
            
            if(LobbyUIManager.Instance != null && !_isVSBot)
            {
                if(!LobbyUIManager.Instance.MainUI.gameObject.activeSelf)
                {
                    LobbyUIManager.Instance.ShowOnlineMatch();
                }
            }
        }
    }

    public override void OnStartServer()
    {
        AddDebugText("OnStartServer");            
       
        base.OnStartServer();
    }
    
    public override void OnStopServer()
    {
        AddDebugText("OnStopServer");     
    
        SetLobbyScene(""); // Ensures we don't reload the scene after quitting                
        base.OnStopServer();
        //SetLobbyScene(LobbyScene);
    }

    public override void OnDropConnection(bool success, string extendedInfo)
    {
        AddDebugText("OnDropConnection " + success.ToString() );
    
        base.OnDropConnection(success, extendedInfo);
    }

    public override void OnMatchCreate(bool success, string extendedInfo, MatchInfo matchInfo)
    {
        _currentMatchID = (System.UInt64)matchInfo.networkId;
        
        AddBlackList(matchInfo.networkId.ToString());

        AddDebugText(string.Format(
             "OnMatchCreate: Success = {0}, extendedInfo: {1}, MatchID: {2}"
            , success.ToString()
            , extendedInfo
            , _currentMatchID
        ));
        
        base.OnMatchCreate(success, extendedInfo, matchInfo);
        
        if(!success)
        {
            if(IsStartMatching)
            {
                // Fail to create. Start retry.
                StartCoroutine(OnRetryMatching());
            }
        
            /*
            if(LobbyUIManager.Instance != null)
            {
                LobbyUIManager.Instance.HidePlayerList();
                LobbyUIManager.Instance.ShowStatusMessage("Fail to create match.", null);    
            }
            */
        }
    }

    public override void OnMatchJoined(bool success, string extendedInfo, MatchInfo matchInfo)
    {       
        _currentMatchID = (System.UInt64)matchInfo.networkId;
        
        AddBlackList(matchInfo.networkId.ToString());
      
        AddDebugText(string.Format(
             "OnMatchJoined: Success = {0}, extendedInfo: {1}, MatchID: {2}"
            , success.ToString()
            , extendedInfo
            , (System.UInt64)matchInfo.networkId
        ));
        
        base.OnMatchJoined(success, extendedInfo, matchInfo);
        
        if(!success)
        {
            if(IsStartMatching)
            {
                // Fail to join. Start retry.
                StartCoroutine(OnRetryMatching());
            }
        
            /*
            if(LobbyUIManager.Instance != null)
            {
                LobbyUIManager.Instance.HidePlayerList();
                LobbyUIManager.Instance.ShowStatusMessage(
                      "Fail to join match."
                    , LobbyUIManager.Instance.ServerListUI.OnClickRefresh
                );     
            }
            */
        }
    }

    public override void OnMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matchList)
    {        
        AddDebugText(string.Format(
             "OnMatchList: Success = {0}, extendedInfo: {1}, MatchList: {2}"
            , success.ToString()
            , extendedInfo
            , matchList.Count
        ));
    
        base.OnMatchList(success, extendedInfo, matchList);
        
        if(LobbyUIManager.Instance != null)
        {
            LobbyUIManager.Instance.UpdateRoomList(matchList);
        }
    }
    
    public override void OnDestroyMatch(bool success, string extendedInfo)
    {    
        AddDebugText(string.Format(
             "OnDestroyMatch: Success = {0}, extendedInfo: {1}"
            , success.ToString()
            , extendedInfo
        ));
    
        //base.OnDestroyMatch(success, extendedInfo);
        
        if (_disconnectServer)
        {
            //if(_isMatchmaking)
            {
                SetLobbyScene("");
                StopMatchMaker();
                // lobbyScene = LobbyScene;StopHost
                
                _isMatchmaking = false;
            }
            
            _isHost = false;
            SetLobbyScene("");
            StopHost();
            // lobbyScene = LobbyScene;
            _disconnectServer = false;
        }
        else
        {
            OnCompletelyCancelMatching();
        }
    }      
    
    // ----------------- Client callbacks ------------------

    public override void OnClientConnect(NetworkConnection conn)
    {  
        AddDebugText(string.Format(
             "OnClientConnect: address = {0}"
            , conn.address
        ));
    
        _isConnect = true;
        base.OnClientConnect(conn);

        if(LobbyUIManager.Instance != null)
        {
            LobbyUIManager.Instance.HideStatusMessage();
        }
        
        //conn.RegisterHandler(MsgKicked, KickedMessageHandler);

        if (!NetworkServer.active)
        {//only to do on pure client (not self hosting client)
            //ChangeTo(lobbyPanel);
            SetServerInfo("Client", networkAddress);
        }
    }

    public override void OnClientDisconnect(NetworkConnection conn)
    {    
        // Me is client and other disconnect.
    
        AddDebugText("OnClientDisconnect");
        _isConnect = false;
        
        SetLobbyScene(""); // Ensures we don't reload the scene after quitting
                
        if(IsInGame)
        {
            OnInGameClientDisconnect(conn);
        }
        else
        {
            base.OnClientDisconnect(conn);
        }
    }

    public override void OnClientError(NetworkConnection conn, int errorCode)
    {        
        //ChangeTo(mainMenuPanel);
        
        AddDebugText(string.Format(
             "OnClientError: address = {0}, error = {1}"
            , conn.address
            , (errorCode == 6 ? "timeout" : errorCode.ToString())
        ));
    }

    public override void OnClientSceneChanged(NetworkConnection conn)
    {
        AddDebugText("OnClientSceneChanged");            
        base.OnClientSceneChanged(conn);
    }

    public override void OnServerConnect(NetworkConnection conn)
    {
        AddDebugText("OnServerConnect");
        
        _isConnect = true;
        base.OnServerConnect(conn);
    }

    public override void OnServerDisconnect(NetworkConnection conn)
    {
        // Me is server and other disconnect.
    
        AddDebugText("OnServerDisconnect");
        
        _isConnect = false;
                
        if(IsInGame)
        {
            OnInGameServerDisconnect(conn);
        }
        else
        {
            base.OnServerDisconnect(conn);
        }
    }
    
    public IEnumerator ServerCountdownCoroutine()
    {
        Debug.Log("Start ServerCountdownCoroutine");
    
        float remainingTime = PreMatchCountdown;
        int floorTime = Mathf.FloorToInt(remainingTime);
        float prevTime = Time.time;
        
        KOSLobbyPlayer host  = lobbySlots[0] as KOSLobbyPlayer;
        host.CmdShowVS();

        while (remainingTime > 0)
        {            
            yield return new WaitForEndOfFrame();
        
            float currentTime = Time.time;
            float deltaTime =  currentTime - prevTime;
            prevTime = currentTime;
            
            remainingTime -= deltaTime;      
            
            Debug.Log("remainingTime " + remainingTime);      
            if(remainingTime > 0)
            {
                int newFloorTime = Mathf.FloorToInt(remainingTime);
    
                if (newFloorTime != floorTime)
                {
                    //to avoid flooding the network of message, we only send a notice to client when the number of plain seconds change.
                    floorTime = newFloorTime;
    
                    for (int i = 0; i < lobbySlots.Length; ++i)
                    {
                        if (lobbySlots[i] != null)
                        {
                            //there is maxPlayer slots, so some could be == null, need to test it before accessing!
                            (lobbySlots[i] as KOSLobbyPlayer).RpcUpdateCountdown(floorTime);
                        }
                    }
                }
            }
            
            if(!_isReady)
            {
                break;
            }
        }

        if(_isReady)
        {            
            ServerChangeScene(PlayScene);
        }
    }
        
    public override void OnServerSceneChanged(string sceneName)
    {               
        AddDebugText("OnServerSceneChanged " + sceneName);
        base.OnServerSceneChanged(sceneName);
    }
	#endregion

	#region Methods
	public void Init()
	{                
        _isInRoom = false;
        _isHost = false;
        _isReady = false;
        _isOnlineMode = false;
        _isStartMatching = false;
        _isMatchingTimeout = false;
        _isVSBot = false;
        _matchingTimer = 0.0f;

        _isMatchmaking = false;
        _disconnectServer = false;
        _isInGame = false;
        _isConnect = false;
        
        _roomBlackList = new List<RoomDetail>();
        _kosPlayers = new List<KOSLobbyPlayer>();
        
        if(_countdownCoroutine != null)
        {
            StopCoroutine(_countdownCoroutine);
            _countdownCoroutine = null;
        }
        
        SetLobbyScene(LobbyScene);
        playScene = PlayScene;
	}

	public void RequestStartHost()
    {
        AddDebugText("RequestStartHost");
        
        if(LobbyUIManager.Instance != null)
        {
            LobbyUIManager.Instance.ShowStatusMessage(
                  LocalizationManager.GetText("LOBBY_STATUS_CREATE_ROOM")
                , this.RequestStopHost
            );    
        }
        
        _isMatchmaking = false;
        
        SetLobbyScene("");
        StopMatchMaker();
        
        _isOnlineMode = false;
        _isHost = true;
        
        SetLobbyScene(LobbyScene);
        StartHost();
    }
    
    public void RequestStopHost()
    {
        AddDebugText("RequestStopHost");
    
        if (_isMatchmaking)
        {
            AddDebugText(string.Format(
                 "RequestStopHost/DestroyMatch: MatchID = {0}"
                , _currentMatchID
            ));
            
            SetLobbyScene("");
            matchMaker.DestroyMatch((NetworkID)_currentMatchID, 0, OnDestroyMatch);
            _disconnectServer = true;
        }
        else
        {
            AddDebugText("RequestStopHost/StopHost");
        
            _isHost = false;
            
            SetLobbyScene("");
            StopHost();
            // lobbyScene = LobbyScene;
        }
    }
    
    public void RequestJoinLocal(string ipAddress)
    {
        AddDebugText("RequestJoinLocal ipAddress: " + ipAddress);
    
        _isMatchmaking = false;
        
        SetLobbyScene("");
        StopMatchMaker();
        // lobbyScene = LobbyScene;
        
        _isOnlineMode = false;
    
        if(LobbyUIManager.Instance != null)
        {
            LobbyUIManager.Instance.ShowPlayerList("", this.RequestStopClient);
            LobbyUIManager.Instance.ShowStatusMessage(
                  LocalizationManager.GetText("LOBBY_STATUS_CONNECT")
                , this.RequestStopClient
            );
        }

        
        this.networkAddress = ipAddress;
        SetLobbyScene(LobbyScene);
        StartClient();
    }

    public void RequestStopClient()
    {
        AddDebugText("RequestStopClient");   
                
        SetLobbyScene("");
        StopClient();
        // lobbyScene = LobbyScene;
        
        if(LobbyUIManager.Instance != null)
        {
            LobbyUIManager.Instance.HidePlayerList();
        }
        
        if (_isMatchmaking)
        {
            AddDebugText("RequestStopClient/StopMatchMaker");
            
            SetLobbyScene("");
            StopMatchMaker();
            // lobbyScene = LobbyScene;
            
            _isMatchmaking = false;
            
            /*
            if(LobbyUIManager.Instance != null)
            {
                LobbyUIManager.Instance.ShowOnlineLobby();
            }
            */
        }
        else
        {
            /*
            if(LobbyUIManager.Instance != null)
            {
                LobbyUIManager.Instance.ShowLocalLobby();
            }
            */
        }
    }
    
    public void RequestCreateMatch(string matchName)
    {
        AddDebugText(string.Format(
             "RequestCreateMatch: Name = {0}"
            , matchName
        ));
        
        /*
        if(LobbyUIManager.Instance != null)
        {
            LobbyUIManager.Instance.ShowStatusMessage("Creating online room...", this.RequestStopHost);    
        }
        */
        
        //if(!_isMatchmaking)
        {
            SetLobbyScene(LobbyScene);
            StartMatchMaker();
            _isMatchmaking = true;      
        }
        
        _isOnlineMode = true;
        
        this.matchName = matchName;
        this.matchMaker.CreateMatch(
              matchName
            , (uint)this.maxPlayers
            , true
            , "", "", "", 0, 0,
            this.OnMatchCreate
        );

        //lobbyManager.backDelegate = lobbyManager.StopHost;
        
        _ip = matchHost;
        SetServerInfo("Matchmaker Host", matchHost);
    }
    
    public void RequestCancelCreateMatch()
    {
        AddDebugText("RequestCancelJoinMatch");
    
        SetLobbyScene("");
        StopHost();
        // lobbyScene = LobbyScene;
    }
        
    public void RequestRoomList(int pageIndex)
    {       
        AddDebugText("RequestRoomList " + pageIndex.ToString());
      
        //if(!_isMatchmaking)
        {
            SetLobbyScene(LobbyScene);
            StartMatchMaker();
            _isMatchmaking = true;
        }
        
        this.matchMaker.ListMatches(pageIndex, 6, "", true, 0, 0, this.OnMatchList);
    }
    
    public void RequestRoomList(int pageIndex, NetworkMatch.DataResponseDelegate<List<MatchInfoSnapshot>> onGetList)
    {       
        AddDebugText("RequestRoomList " + pageIndex.ToString());
      
        //if(!_isMatchmaking)
        {
            SetLobbyScene(LobbyScene);
            StartMatchMaker();
            _isMatchmaking = true;
        }
        
        this.matchMaker.ListMatches(pageIndex, 6, "", true, 0, 0, onGetList);
    }
        
    public void RequestJoinMatch(NetworkID netID)
    {
        AddDebugText(string.Format(
             "RequestJoinMatch: NetID = {0}"
            , netID
        ));
        
        /*
        if(LobbyUIManager.Instance != null)
        {
            LobbyUIManager.Instance.ShowPlayerList("", this.RequestStopClient);
            LobbyUIManager.Instance.ShowStatusMessage("Connecting...", this.RequestStopClient);
        }
        */
        
        //if(!_isMatchmaking)
        {
            SetLobbyScene(LobbyScene);
            StartMatchMaker();
            _isMatchmaking = true;
        }
        
        _isOnlineMode = true;
                
        AddBlackList(netID.ToString());
        this.matchMaker.JoinMatch(
              netID
            , "", "", "", 0, 0
            , this.OnMatchJoined
        );
    }
    
    public void RequestCancelJoinMatch()
    {
        AddDebugText("RequestCancelJoinMatch");
    
        RequestStopClient();
    }
     
    public void SetServerInfo(string status, string host)
    {
        _statusInfo = status;
        _hostInfo = host;
        
        AddDebugText("SetServerInfo: " +  _statusInfo + ", " + _hostInfo);
    }
    
    void UpdateStatus(string status)
    {
        AddDebugText("UpdateStatus: " +  status);
    }
    
    public void AddPlayer(KOSLobbyPlayer player)
    {
        if (_kosPlayers.Contains(player))
            return;

        AddDebugText("AddPlayer " + player.PlayerName);
        _kosPlayers.Add(player);
        
        for (int i = 0; i < _kosPlayers.Count; ++i)
        {
            KOSLobbyPlayer p = _kosPlayers[i];

            if (p != null && p.isLocalPlayer)
            {
                p.SendNotReadyToBeginMessage();
            }
        }
        
        if(LobbyUIManager.Instance != null)
        {
           LobbyUIManager.Instance.PlayerListUI.PlayerListModified();
        }
        
        if(_kosPlayers.Count >= 2)
        {
            _matchingTimer = 0.0f;
            _isMatchingTimeout = true;
            _matchingStatus = MatchingStatus.Waiting;
            Debug.Log("Contain >= 2 players.");
        }
    }

    public void RemovePlayer(KOSLobbyPlayer player)
    {
        bool isSuccess = _kosPlayers.Remove(player);
        
        if(isSuccess)
        {
            AddDebugText("RemovePlayer " + player.PlayerName);
        
            if(_isReady)
            {
                _isReady = false;
                OnReadyInterrupt();
                
                return;
            }
            
            for (int i = 0; i < _kosPlayers.Count; ++i)
            {
                KOSLobbyPlayer p = _kosPlayers[i];
    
                if (p != null && p.isLocalPlayer)
                {
                    p.SendNotReadyToBeginMessage();
                }
            }

            if(LobbyUIManager.Instance != null)
            {
               LobbyUIManager.Instance.PlayerListUI.PlayerListModified();
            }
        }
    }
    
    public void StartMatching()
    {
        if(!IsStartMatching)
        {
            _matchingTimer = 0.0f;
            _isStartMatching = true;
            
            RetryMatching();
        }
    }
    
    private void RetryMatching()
    {
        if(IsStartMatching)
        {
            _matchingStatus = MatchingStatus.Finding;            
            KOSLobbyManager.Instance.RequestRoomList(0, this.CheckMatchList); 
        }
    }
    
    private void OnCompletelyCancelMatching()
    {
        AddDebugText("OnCompletelyCancelMatching");
        
        //Init();
    
        //if(Status == MatchingStatus.Canceling)
        {
            _isStartMatching = false;
            _isReady = false;
            _matchingStatus = MatchingStatus.Ready;
        }
    }
    
    private void CheckMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matchList)
    {        
        if(Status == MatchingStatus.Finding)
        {
            if(success)
            {
                if(matchList != null && matchList.Count > 0)
                {
                    // have some room avalible.
                    foreach(MatchInfoSnapshot match in matchList)
                    {
                        if(match.currentSize < match.maxSize)
                        {
                            if(!IsInBlackList(match.networkId.ToString()))
                            {
                                // found avalible room.
                                Debug.Log("Try join match. " + match.name);
                                
                                _matchingStatus = MatchingStatus.Joining;
                                KOSLobbyManager.Instance.RequestJoinMatch(match.networkId); 
                                
                                return;
                            }
                        }
                    }
                }
            }
                
            string matchName = string.Format(
                  "{0}#{1}"
                , (PlayerSave.GetPlayerName().Length > 0) ? PlayerSave.GetPlayerName() : "Player"
                , Random.Range(0, 100000).ToString("00000")
            );
            
            Debug.Log("Create match. " + matchName);
            
            _matchingStatus = MatchingStatus.Creating;
            KOSLobbyManager.Instance.RequestCreateMatch(matchName);   
        }     
    }
    
    private bool IsInBlackList(string id)
    {
        if(_roomBlackList != null && _roomBlackList.Count > 0)
        {
            foreach(RoomDetail rd in _roomBlackList)
            {
                if(rd.ID == id)
                {
                    return true;
                }
            }
        }
        
        return false;
    }
    
    private void UpdateBlackList()
    {
        if(_roomBlackList != null && _roomBlackList.Count > 0)
        {
            float deltaTime = Time.deltaTime;
            for(int index = 0; index < _roomBlackList.Count; ++index)
            {
                RoomDetail r = _roomBlackList[index];
                float newTime = r.Time - deltaTime;
                
                if(newTime <= 0.0f)
                {
                    _roomBlackList.RemoveAt(index);
                    --index;
                }
                else
                {
                    _roomBlackList[index] = r;
                }
            }
        }
    }
    
    private void AddBlackList(string id)
    {
        if(_roomBlackList == null)
        {
            _roomBlackList = new List<RoomDetail>();
        }
    
        if(_roomBlackList != null)
        {
            RoomDetail r;
            for(int index = 0; index < _roomBlackList.Count; ++index)
            {
                if(_roomBlackList[index].ID == id)
                {
                    // found
                    float remainTime = _roomBlackList[index].Time + _roomBlackListTime;
                    
                    r.ID = id;
                    r.Time = remainTime;
                    _roomBlackList[index] = r;
                    return;
                }
            }
            
            r.ID = id;
            r.Time = _roomBlackListTime;
            _roomBlackList.Add(r);
        }
    }
    
    public void ClearBlackList()
    {
        if(_roomBlackList != null)
        {
            _roomBlackList.Clear();
        }
    }
    
    public void CancelMatching()
    {
        if(_isOnlineMode)
        {
            if(IsStartMatching && Status != MatchingStatus.Canceling)
            {
                switch(Status)
                {
                    case MatchingStatus.Joining:
                    {
                        _matchingStatus = MatchingStatus.Canceling;
                        RequestStopClient();                   
                    }
                    break;
                    
                    case MatchingStatus.Waiting:
                    {
                        _matchingStatus = MatchingStatus.Canceling;
                        RequestStopHost();
                    }
                    break;
                    
                    case MatchingStatus.Matched:
                    {
                        _matchingStatus = MatchingStatus.Canceling;
                        if(_isHost)
                        {
                            RequestStopHost();
                            RequestStopClient();
                        }
                        else
                        {
                            RequestStopClient(); 
                        }
                    }
                    break;
                    
                    default:
                    {
                        //_matchingStatus = MatchingStatus.Canceling;
                        OnCompletelyCancelMatching();
                    }
                    break;
                }
            }
        }
        else
        {
            if(_isInRoom)
            {
                if(_isHost)
                {
                    RequestStopHost();
                }
                else
                {
                    RequestStopClient(); 
                }
            }
        }
    }
    
    private void OnMatchingTimeout()
    {            
        if(IsStartMatching && Status != MatchingStatus.Matched)
        {
            AddDebugText("OnMatchingTimeout");       
            
            _isVSBot = true;
            CancelMatching();
            
            MatchMakingManager matchMaking = GameObject.FindObjectOfType<MatchMakingManager>();
            bool isSuccess = matchMaking.CreateVSBotNetworkData();            
            if (isSuccess) 
            {
                //MatchingData matchingData = GameObject.FindObjectOfType<MatchingData>();
                KOSNetwork.NetworkSetting.IsNetworkEnabled = false;               
                _isMatchingTimeout = true;
                
                StartCoroutine(VSBotCountdownCoroutine());
            }
        }
    }
    
    private IEnumerator VSBotCountdownCoroutine()
    {
        float remainingTime = PreMatchCountdown;
        int floorTime = Mathf.FloorToInt(remainingTime);
        float prevTime;
        
        MatchingData matchingData = GameObject.FindObjectOfType<MatchingData>();
        
        LobbyUIManager.Instance.ShowVS(
              matchingData.PlayerData[0].ImageName
            , matchingData.PlayerData[1].ImageName
            , matchingData.PlayerData[0].Name
            , matchingData.PlayerData[1].Name
        );

        prevTime = Time.time;
        while (remainingTime > 0)
        {
            yield return new WaitForEndOfFrame();
        
            float currentTime = Time.time;
            float deltaTime =  currentTime - prevTime;
            prevTime = currentTime;
            
            remainingTime -= deltaTime;
            if(remainingTime > 0)
            {
                int newFloorTime = Mathf.FloorToInt(remainingTime);
    
                if (newFloorTime != floorTime)
                {
                    //to avoid flooding the network of message, we only send a notice to client when the number of plain seconds change.
                    floorTime = newFloorTime;
                    
                    if(floorTime <= 0) 
                    {
                        LobbyUIManager.Instance.ShowLoading(true);
                    }
                    LobbyUIManager.Instance.UpdateVSTime(floorTime);
                }
            }
        }        
        
        if(LobbyUIManager.Instance != null)
        {
            LobbyUIManager.Instance.LoadScene(PlayScene);
        }
    }
    
    private IEnumerator OnRetryMatching()
    {        
        yield return new WaitForSeconds(1.0f);
              
        RetryMatching();
    }
    
    private void OnInGameServerDisconnect(NetworkConnection conn)
    {
        AddDebugText("OnInGameServerDisconnect");

        if(BattleManager.Instance != null)
        {
            BattleManager.Instance.OnPlayerDisconnect();
        }
        if(BattleController.Instance != null)
        {
            BattleController.Instance.OnPlayerDisconnect();
        }
        
        _isReady = false;
        _isInGame = false;     
        
        //base.OnLobbyServerDisconnect(conn);
    }
    
    private void OnInGameClientDisconnect(NetworkConnection conn)
    {
        AddDebugText("OnInGameClientDisconnect");
        
        if(BattleManager.Instance != null)
        {
            BattleManager.Instance.OnPlayerDisconnect();
        }
        if(BattleController.Instance != null)
        {
            BattleController.Instance.OnPlayerDisconnect();
        }
        
        _isReady = false;
        _isInGame = false;     
        
        //base.OnLobbyClientDisconnect(conn);
    }

    public void FullyShutdown()
    {
        Shutdown();
        NetworkServer.Shutdown();
        NetworkServer.Reset();
        
        ClientScene.ClearSpawners();
        ClientScene.DestroyAllClientObjects();
        
        _instance = null;
        Destroy(gameObject);
    }
    #endregion    

    #region Debug Log
    public void ClearDebugText()
    {
        if(_debugTextList != null)
        { 
            _debugTextList.Clear();
        }
    }
    
    public void AddDebugText(string text)
    {
        //#if UNITY_EDITOR
        if(_debugTextList == null) _debugTextList = new List<string>();
        
        //_debugTextList.Add(text);
        
        if(_debugTextList.Count > 20)
        {
            _debugTextList.RemoveAt(0);
        }
        //#endif
        
        Debug.Log(text);
    }
    #endregion
    
    #if UNITY_EDITOR
    private void OnGUI()
    {
        if(_debugTextList != null && _debugTextList.Count > 0)
        {
            string outputText = "";
            foreach(string text in _debugTextList)
            {
                if(outputText.Length > 0)
                {
                    outputText = text + '\n' + outputText;
                }
                else
                {
                    outputText = text;
                }
            }
            GUI.color = Color.yellow;
            GUI.Label(new Rect(10, 10, Screen.width * 2.0f, Screen.height * 2.0f), outputText);   
        } 
    }
    #endif
}
