﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace KOSNetwork
{
    public class GamePlayer : NetworkBehaviour
    {
        [SyncVar]
        private bool m_IsReadyStartGame = false;

        static public GamePlayer localPlayer = null;

        [SyncVar]
        private PlayerIndex m_playerIndex;
        
        [SyncVar]
        private bool m_IsCmdNextPhase = false;

        public PlayerIndex playerIndex { get { return m_playerIndex; } }

        public bool IsReadyStartGame { get { return m_IsReadyStartGame; } }

        public bool isLocalGamePlayer 
        { 
            get
            {
                if (this == localPlayer) return true;
                return false;
            } 
        }
        
        public bool IsCmdNextPhase { get { return m_IsCmdNextPhase; } }
        
        public bool IsConnect
        {
            get
            {
                if(KOSLobbyManager.Instance != null)
                {
                    return KOSLobbyManager.Instance.IsConnect;
                }
                
                return false;
            }
        }
        
        public bool IsMyTurn
        {
            get
            {
                if(BattleManager.Instance.CurrentActivePlayerIndex == this.playerIndex)
                {
                    return true;
                }
                
                return false;
            }
        }

        public void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }

        public override void OnStartAuthority()
        {
            DontDestroyOnLoad(gameObject);
        
            base.OnStartAuthority();

            if(isServer)
                CmdReadyStartGame(PlayerIndex.One);
            else
                CmdReadyStartGame(PlayerIndex.Two);

            localPlayer = this;
            m_IsReadyStartGame = true;
        }

        public void OnDisconnectedFromMasterServer(NetworkDisconnection info)
        {
            Debug.Log("OnDisconnectedFromMasterServer : " + info.ToString());
        }

        public void OnDisconnectedFromServer(NetworkDisconnection info)
        {
            Debug.Log("OnDisconnectedFromServer : " + info.ToString());
        }

        [Command]
        private void CmdReadyStartGame(PlayerIndex playerIndex)
        {
            m_IsReadyStartGame = true;
            m_playerIndex = playerIndex;
        }
        
        // Sync player profile.
        public void RequestSyncPlayerProfile(string profileDataText, int randomSeed)
        {
            if(IsConnect)
            {
                CmdSyncPlayerProfile(profileDataText, randomSeed);
            }
            else
            {
                Debug.Log("CmdSyncPlayerProfile : send command when not connect." );
            }
        }
        [Command]
        private void CmdSyncPlayerProfile(string profileDataText, int randomSeed)
        {
            RpcSyncPlayerProfile(m_playerIndex, profileDataText, randomSeed);
        }
        [ClientRpc]
        private void RpcSyncPlayerProfile(PlayerIndex playerIndex, string profileDataText, int randomSeed)
        {
            BattleManager.Instance.SetRandomSeed(randomSeed);
            BattleManager.Instance.OnSyncPlayerProfile(playerIndex, profileDataText);
        }
        
        /*
        // Sync random seed.
        [Command]
        public void CmdSyncRandomSeed(int randomSeed)
        {
            RpcSyncRandomSeed(randomSeed);
        }
        [ClientRpc]
        public void RpcSyncRandomSeed(int randomSeed)
        {
            BattleManager.Instance.SetRandomSeed(randomSeed);
        }
        */
        
        // print log
        [Command]
        public void CmdPrintLog(string text)
        {
            RpcPrintLog(text);
        }
        [ClientRpc]
        private void RpcPrintLog(string text)
        {
            Debug.Log(text);
        }

        // Find First Opeion
        public void RequestSelectRPS(GamePlayerUI.RPSType optionType)
        {
            if(IsConnect)
            {
                CmdSelectRPS(optionType);
            }
            else
            {
                Debug.Log("CmdSelectRPS : send command when not connect." );
            }
        }        
        [Command]
        private void CmdSelectRPS(GamePlayerUI.RPSType optionType)
        {
            RpcSelectRPS(m_playerIndex, optionType);
        }
        [ClientRpc]
        private void RpcSelectRPS(PlayerIndex playerIndex, GamePlayerUI.RPSType optionType)
        {
            BattleController.Instance.BattleManager.OnSelectRPS(playerIndex, optionType);
        }

        /*
        [Command]
        public void CmdShowRPSResult()
        {
            RpcShowRPSResult();
        }

        [ClientRpc]
        public void RpcShowRPSResult(PlayerIndex playerIndex, GamePlayerUI.RPSType optionType)
        {
            BattleController.Instance.BattleManager.OnSelectRPS(playerIndex, optionType);
        }
        */

        // Find First Play
        public void RequestSelectFindFirstPlay(GamePlayerUI.PlayOrderType playType)
        {
            if(IsConnect)
            {
                CmdSelectFindFirstPlay(playType);
            }
            else
            {
                Debug.Log("CmdSelectFindFirstPlay : send command when not connect." );
            }
        }
        [Command]
        private void CmdSelectFindFirstPlay(GamePlayerUI.PlayOrderType playType)
        {
            RpcSelectFindFirstPlay(playType);
        }
        [ClientRpc]
        private void RpcSelectFindFirstPlay(GamePlayerUI.PlayOrderType playType)
        {
            BattleController.Instance.BattleManager.OnSelectPlayOrder(playType);
        }

        // Select Base
        public void RequestSelectBase(int uniqueID, string playerCardID, CardZoneIndex zone)
        {
            if(IsConnect)
            {
                CmdSelectBase(uniqueID, playerCardID, zone);
            }
            else
            {
                Debug.Log("CmdSelectBase : send command when not connect." );
            }
        }
        [Command]
        private void CmdSelectBase(int uniqueID, string playerCardID, CardZoneIndex zone)
        {
            //Debug.Log(m_playerIndex + " : Select Base -> UID : " + uniqueID);
            RpcSelectBase(m_playerIndex, uniqueID, playerCardID, zone);
        }
        [ClientRpc]
        private void RpcSelectBase(PlayerIndex playerIndex, int uniqueID, string playerCardID, CardZoneIndex zone)
        {
            BattleManager manager = BattleController.Instance.BattleManager;
            UniqueCardData data = new UniqueCardData(uniqueID, playerCardID, playerIndex, zone);
            manager.OnSelectBase(manager.GetPlayer(playerIndex), data);
        }

        // Select Leader
        public void RequestSelectLeader(int uniqueID, string playerCardID, CardZoneIndex zone)
        {
            //Debug.Log(m_playerIndex + " : Select Leader -> UID : " + uniqueID);
            if(IsConnect)
            {
                CmdSelectLeader(uniqueID, playerCardID, zone);
            }
            else
            {   
                Debug.Log("CmdSelectLeader : send command when not connect." );
            }
        }
        [Command]
        private void CmdSelectLeader(int uniqueID, string playerCardID, CardZoneIndex zone)
        {
            //Debug.Log(m_playerIndex + " : Select Leader -> UID : " + uniqueID);
            RpcSelectLeader(m_playerIndex, uniqueID, playerCardID, zone);
        }
        [ClientRpc]
        private void RpcSelectLeader(PlayerIndex playerIndex, int uniqueID, string playerCardID, CardZoneIndex zone)
        {
            BattleManager manager = BattleController.Instance.BattleManager;
            manager.OnSelectLeader(manager.GetPlayer(playerIndex), new UniqueCardData(uniqueID, playerCardID, playerIndex, zone));
        }

        // Select Re-Hand
        public void RequestSelectReHand(bool isReHand)
        {
            if(IsConnect)
            {
                CmdSelectReHand(isReHand);
            }
            else
            {
                Debug.Log("CmdSelectReHand : send command when not connect." );
            }
        }
        [Command]
        private void CmdSelectReHand(bool isReHand)
        {
            RpcSelectReHand(isReHand);
        }
        [ClientRpc]
        private void RpcSelectReHand(bool isReHand)
        {
            BattleManager manager = BattleController.Instance.BattleManager;
            manager.OnSelectReHand(manager.GetPlayer(m_playerIndex), isReHand);
        }
        
        // Dis Hand Card Overflow
        public void RequestDisHandCardOverflow(List<int> uniqueIDList)
        {
            if(IsConnect)
            {
                string uniqueIDListText = "";
                foreach(int id in uniqueIDList)
                {
                    if(uniqueIDListText.Length <= 0)
                    {
                        uniqueIDListText += id.ToString();
                    }
                    else
                    {
                        uniqueIDListText += "," + id.ToString();
                    }
                }
            
                CmdDisHandCardOverflow(uniqueIDListText);
            }
            else
            {
                Debug.Log("CmdDisHandCardOverflow : send command when not connect." );
            }
        }       
        [Command]
        private void CmdDisHandCardOverflow(string uniqueIDListText)
        {
            RpcDisHandCardOverflow(uniqueIDListText);
        }
        [ClientRpc]
        private void RpcDisHandCardOverflow(string uniqueIDListText)
        {
            List<int> uniqueCardIDList = new List<int>();
            string[] texts = uniqueIDListText.Split(',');
            foreach(string text in texts)
            {
                int id = int.Parse(text);
                uniqueCardIDList.Add(id);
            }
        
            BattleManager manager = BattleController.Instance.BattleManager;
            manager.DisHandCardOverflow(uniqueCardIDList);
        }
        
        public void RequestNextPhase(int turnIndex, BattlePhase phase)
        {
            if(IsConnect)
            {
                CmdNextPhase(turnIndex, phase);
            }
            else
            {
                Debug.Log("RequestNextPhase : send command when not connect." );
            }
        }
        // Next Phase
        [Command]
        private void CmdNextPhase(int turnIndex, BattlePhase phase)
        {
            if(!IsCmdNextPhase)
            {
                m_IsCmdNextPhase = true;  
                RpcNextPhase(m_playerIndex, turnIndex, phase, IsMyTurn);
            }
        }
        [ClientRpc]
        private void RpcNextPhase(PlayerIndex playerIndex, int turnIndex, BattlePhase phase, bool isAddTurnTime)
        {
            BattleManager manager = BattleController.Instance.BattleManager;

            /*
            {
                Debug.Log(string.Format("[{0}]GamePlayer/RpcNextPhase: RPC {1} {2}"
                    , m_playerIndex.ToString()
                    , playerIndex.ToString()
                    , BattleManager.Instance.CurrentBattlePhase.ToString()
                )); 
            }
            */
            
            manager.RequestNextPhase_Local(playerIndex, turnIndex, phase, isAddTurnTime);
            m_IsCmdNextPhase = false;
        }
        
        /*
        // Get Gold
        [Command]
        public void CmdGetGold(PlayerIndex playerIndex, int gold)
        {
            RpcGetGold(playerIndex, gold);
        }
        [ClientRpc]
        public void RpcGetGold(PlayerIndex playerIndex, int gold)
        {
            BattleManager manager = BattleController.Instance.BattleManager;
            manager.RequestGetGold_Local(playerIndex, gold);
        }
        */
        
        /*
        // Action Draw Card
        [Command]
        public void CmdDrawCard(PlayerIndex playerIndex, int amount)
        {
            RpcDrawCard(playerIndex, amount);
        }
        [ClientRpc]
        public void RpcDrawCard(PlayerIndex playerIndex, int amount)
        {
            BattleManager manager = BattleController.Instance.BattleManager;
            manager.RequestDrawCard_Local(playerIndex, amount);
        }
        */
        
        // Action Summon UniqueCard
        public void RequestSummonUniqueCard(int uniqueCardID, LocalBattleZoneIndex localZone, bool isFree, bool isFacedown)
        {
            if(IsConnect)
            {
                CmdSummonUniqueCard(uniqueCardID, localZone, isFree, isFacedown);
            }
            else
            {
                Debug.Log("RequestSummonUniqueCard : send command when not connect.");
            }
        }
        [Command]
        private void CmdSummonUniqueCard(int uniqueCardID, LocalBattleZoneIndex localZone, bool isFree, bool isFacedown)
        {
            RpcSummonUniqueCard(uniqueCardID, localZone, isFree, isFacedown, IsMyTurn);
        }
        [ClientRpc]
        private void RpcSummonUniqueCard(int uniqueCardID, LocalBattleZoneIndex localZone, bool isFree, bool isFacedown, bool isAddTurnTime)
        {
            BattleManager manager = BattleController.Instance.BattleManager;
            
            manager.RequestSummonUniqueCard(uniqueCardID, isAddTurnTime, localZone, isFree, isFacedown);
        }
        
        /*
        // Action Use HandCard
        [Command]
        public void CmdUseHandCard(int uniqueCardID)
        {
            RpcUseHandCard(uniqueCardID);
        }
        [ClientRpc]
        public void RpcUseHandCard(int uniqueCardID)
        {
            BattleManager manager = BattleController.Instance.BattleManager;
            manager.RequestUseHandCard(uniqueCardID);
        }
        */
        
        // Action Attack BattleCard
        public void RequestAttackBattleCard(int attackerID, int targetID)
        {
            if(IsConnect)
            {
                CmdAttackBattleCard(attackerID, targetID);
            }
            else
            {
                Debug.Log("RequestAttackBattleCard : send command when not connect.");
            }
        }
        [Command]
        private void CmdAttackBattleCard(int attackerID, int targetID)
        {
            RpcAttackBattleCard(attackerID, targetID, IsMyTurn, BattleManager.Instance.TurnTimer);
        }
        [ClientRpc]
        private void RpcAttackBattleCard(int attackerID, int targetID, bool isAddTurnTime, float remainTime)
        {
            BattleManager manager = BattleController.Instance.BattleManager;
            
            manager.SetPauseTimeout(true);
            manager.SetRemainTurnTime(remainTime);
            manager.RequestAttackBattleCard(attackerID, targetID, isAddTurnTime);
        }
        
        // Action Move BattleCard
        public void RequestMoveBattleCard(int moveID, BattleZoneIndex targetZone, bool isForceMove)
        {
            if(IsConnect)
            {
                CmdMoveBattleCard(moveID, targetZone, isForceMove);
            }
            else
            {
                Debug.Log("RequestMoveBattleCard : send command when not connect.");
            }
        }
        [Command]
        private void CmdMoveBattleCard(int moveID, BattleZoneIndex targetZone, bool isForceMove)
        {
            RpcMoveBattleCard(moveID, targetZone, isForceMove, IsMyTurn);
        }
        [ClientRpc]
        private void RpcMoveBattleCard(int moveID, BattleZoneIndex targetZone, bool isForceMove, bool isAddTurnTime)
        {
            BattleManager manager = BattleController.Instance.BattleManager;
            
            manager.RequestMoveBattleCard(moveID, targetZone, isAddTurnTime, isForceMove);
        }
        
        // Action Summon BattleCard
        public void RequestSummonBattleCard(int battleCardID)
        {
            if(IsConnect)
            {
                CmdSummonBattleCard(battleCardID);
            }
            else
            {
                Debug.Log("RequestSummonBattleCard : send command when not connect.");
            }
        }
        [Command]
        private void CmdSummonBattleCard(int battleCardID)
        {
            RpcSummonBattleCard(battleCardID, IsMyTurn);
        }
        [ClientRpc]
        private void RpcSummonBattleCard(int battleCardID, bool isAddTurnTime)
        {
            BattleManager manager = BattleController.Instance.BattleManager;
            
            manager.RequestFaceupBattleCard(battleCardID, isAddTurnTime);
        }
        
        /*
        // Action Use Ability BattleCard
        [Command]
        public void CmdUseAbilityBattleCard(int battleCardID, int abilityIndex)
        {
            RpcUseAbilityBattleCard(battleCardID, abilityIndex);
        }
        [ClientRpc]
        public void RpcUseAbilityBattleCard(int battleCardID, int abilityIndex)
        {
            BattleManager manager = BattleController.Instance.BattleManager;
            manager.RequestUseAbilityBattleCard(battleCardID, abilityIndex);
        }
        */
        /*
        // Request Preform Ability BattleCard
        [Command]
        public void CmdPreformAbilityBattleCard(int battleCardID)
        {
            RpcPreformAbilityBattleCard(battleCardID);
        }
        [ClientRpc]
        public void RpcPreformAbilityBattleCard(int battleCardID)
        {
            BattleManager manager = BattleController.Instance.BattleManager;
            manager.RequestPreformAbilityBattleCard(battleCardID);
        }
        
        // Request Preform Ability HandCard
        [Command]
        public void CmdPreformAbilityHandCard(int uniqueCardID)
        {
            RpcPreformAbilityHandCard(uniqueCardID);
        }
        [ClientRpc]
        public void RpcPreformAbilityHandCard(int uniqueCardID)
        {
            BattleManager manager = BattleController.Instance.BattleManager;
            manager.RequestPreformUseHandCard(uniqueCardID);
        }
        */
        /*
        // Request Finish Select Battle Card
        public void CmdFinishSelectBattleCard(List<int> battleIDList)
        {
            string battleIDListText = "";
            foreach(int id in battleIDList)
            {
                if(battleIDListText.Length <= 0)
                {
                    battleIDListText += id.ToString();
                }
                else
                {
                    battleIDListText += "," + id.ToString();
                }
            }
        
            CmdFinishSelectBattleCard(battleIDListText);
        }       
        [Command]
        private void CmdFinishSelectBattleCard(string battleIDListText)
        {
            RpcFinishSelectBattleCard(battleIDListText);
        }
        [ClientRpc]
        public void RpcFinishSelectBattleCard(string battleIDListText)
        {
            List<int> battleCardIDList = new List<int>();
            string[] texts = battleIDListText.Split(',');
            foreach(string text in texts)
            {
                int id = int.Parse(text);
                battleCardIDList.Add(id);
            }
        
            BattleManager manager = BattleController.Instance.BattleManager;
            manager.FinishSelectTarget(battleCardIDList);
        }
        
        // Request Cancel Select Battle Card
        [Command]
        public void CmdCancelSelectBattleCard()
        {
            RpcCancelSelectBattleCard();
        }
        [ClientRpc]
        public void RpcCancelSelectBattleCard()
        {        
            BattleManager manager = BattleController.Instance.BattleManager;
            manager.CancelSelectTarget();
        }
        */
        /*
        // Request Select Unique Card
        public void FinishSelectUniqueCard(List<int> uniqueIDList)
        {
            string uniqueIDListText = "";
            foreach(int id in uniqueIDList)
            {
                if(uniqueIDListText.Length <= 0)
                {
                    uniqueIDListText += id.ToString();
                }
                else
                {
                    uniqueIDListText += "," + id.ToString();
                }
            }
        
            CmdFinishSelectUniqueCard(uniqueIDListText);
        }      
        [Command]
        public void CmdFinishSelectUniqueCard(string uniqueIDListText)
        {
            RpcFinishSelectUniqueCard(uniqueIDListText);
        }
        [ClientRpc]
        public void RpcFinishSelectUniqueCard(string uniqueIDListText)
        {
            List<int> uniqueIDList = new List<int>();
            string[] texts = uniqueIDListText.Split(',');
            foreach(string text in texts)
            {
                int id = int.Parse(text);
                uniqueIDList.Add(id);
            }
        
            BattleManager manager = BattleController.Instance.BattleManager;
            //manager.GetFinishSelectTarget(battleCardIDTextList);
        }
        
        // Request Cancel Select Unique Card
        [Command]
        public void CmdCancelSelectUniqueCard()
        {
            RpcCancelSelectUniqueCard();
        }
        [ClientRpc]
        public void RpcCancelSelectUniqueCard()
        {        
            BattleManager manager = BattleController.Instance.BattleManager;
            //manager.CancelSelectTarget();
        }
        */

        // Request Action Ability Unique Card
        public void RequestActionAbilityUniqueCard(int uniqueCardID, int abilityIndex, List<string> paramList)
        {
            if(IsConnect)
            {
                string paramListText = "";
                foreach(string text in paramList)
                {
                    if(paramListText.Length <= 0)
                    {
                        paramListText += text;
                    }
                    else
                    {
                        paramListText += "|" + text;
                    }
                }
            
                CmdActionAbilityUniqueCard(uniqueCardID, abilityIndex, paramListText);
            }
            else
            {
                Debug.Log("RequestActionAbilityUniqueCard : send command when not connect.");
            }
        }
        [Command]
        private void CmdActionAbilityUniqueCard(int uniqueCardID, int abilityIndex, string paramListText)
        {
            RpcActionAbilityUniqueCard(uniqueCardID, abilityIndex, paramListText, IsMyTurn);
        }
        [ClientRpc]
        private void RpcActionAbilityUniqueCard(int uniqueCardID, int abilityIndex, string paramListText, bool isAddTurnTime)
        {        
            List<string> paramList = new List<string>();
            
            string[] texts = paramListText.Split('|');
            foreach(string text in texts)
            {
                paramList.Add(text);
            }
        
            BattleManager manager = BattleController.Instance.BattleManager;           
            manager.RequestActionAbilityUniqueCard(uniqueCardID, abilityIndex, paramList, isAddTurnTime);
            manager.RequestResumeActiveQueue();
        }
        
        // Request Action Ability Battle Card
        public void RequestActionAbilityBattleCard(int battleCardID, int abilityIndex, List<string> paramList)
        {
            if(IsConnect)
            {
                string paramListText = "";
                foreach(string text in paramList)
                {
                    if(paramListText.Length <= 0)
                    {
                        paramListText += text;
                    }
                    else
                    {
                        paramListText += "|" + text;
                    }
                }
            
                CmdActionAbilityBattleCard(battleCardID, abilityIndex, paramListText);
            }
            else
            {
                Debug.Log("RequestActionAbilityBattleCard : send command when not connect.");
            }
        }
        [Command]
        private void CmdActionAbilityBattleCard(int battleCardID, int abilityIndex, string paramListText)
        {
            RpcActionAbilityBattleCard(battleCardID, abilityIndex, paramListText, IsMyTurn);
        }
        [ClientRpc]
        private void RpcActionAbilityBattleCard(int battleCardID, int abilityIndex, string paramListText, bool isAddTurnTime)
        {       
            List<string> paramList = new List<string>();
            
            string[] texts = paramListText.Split('|');
            foreach(string text in texts)
            {
                paramList.Add(text);
            }
          
            BattleManager manager = BattleController.Instance.BattleManager;
            manager.RequestActionAbilityBattleCard(battleCardID, abilityIndex, paramList, isAddTurnTime);
            manager.RequestResumeActiveQueue();
        }
        
        public void RequestReadyBeginTurn()
        {
            //Debug.Log("CmdReadyBeginTurn" + m_playerIndex.ToString());
            if(IsConnect)
            {
                CmdReadyBeginTurn();
            }
            else
            {
                Debug.Log("RequestActionAbilityBattleCard : send command when not connect.");
            }
        }
        [Command]
        private void CmdReadyBeginTurn()
        {
            //Debug.Log("CmdReadyBeginTurn" + m_playerIndex.ToString());
            
            RpcReadyBeginTurn(m_playerIndex);
        }
        [ClientRpc]
        private void RpcReadyBeginTurn(PlayerIndex playerIndex)
        {        
            BattleManager manager = BattleController.Instance.BattleManager;
            
            manager.OnReadyBegin(playerIndex);
        }
        
        public void RequestReadyEndPreBattle()
        {
            //Debug.Log("CmdReadyEndPreBattle" + m_playerIndex.ToString());
            if(IsConnect)
            {
                CmdReadyEndPreBattle();
            }
            else
            {
                Debug.Log("RequestReadyEndPreBattle : send command when not connect.");
            }
        }
        [Command]
        private void CmdReadyEndPreBattle()
        {
            //Debug.Log("CmdReadyEndPreBattle" + m_playerIndex.ToString());
            
            RpcReadyEndPreBattle(m_playerIndex);
        }
        [ClientRpc]
        private void RpcReadyEndPreBattle(PlayerIndex playerIndex)
        {        
            BattleManager manager = BattleController.Instance.BattleManager;
            
            manager.OnReadyEndPreBattle(playerIndex);
        }
        
        public void RequestReadyEndBattle()
        {
            //Debug.Log("CmdReadyEndBattle" + m_playerIndex.ToString());
            if(IsConnect)
            {
                CmdReadyEndBattle();
            }
            else
            {
                Debug.Log("RequestReadyEndBattle : send command when not connect.");
            }
        }
        [Command]
        private void CmdReadyEndBattle()
        {
            //Debug.Log("CmdReadyEndBattle" + m_playerIndex.ToString());
            
            RpcReadyEndBattle(m_playerIndex);
        }
        [ClientRpc]
        private void RpcReadyEndBattle(PlayerIndex playerIndex)
        {        
            BattleManager manager = BattleController.Instance.BattleManager;
            
            manager.OnReadyEndBattle(playerIndex);
        }
        
        public void RequestReadyEndPostBattle()
        {
            //Debug.Log("CmdReadyEndPostBattle" + m_playerIndex.ToString());
            if(IsConnect)
            {
                CmdReadyEndPostBattle();
            }
            else
            {
                Debug.Log("RequestReadyEndPostBattle : send command when not connect.");
            }
        }
        [Command]
        private void CmdReadyEndPostBattle()
        {
            //Debug.Log("CmdReadyEndPostBattle" + m_playerIndex.ToString());
            
            RpcReadyEndPostBattle(m_playerIndex);
        }
        [ClientRpc]
        private void RpcReadyEndPostBattle(PlayerIndex playerIndex)
        {        
            BattleManager manager = BattleController.Instance.BattleManager;
            
            manager.OnReadyEndPostBattle(playerIndex);
        }
        
        public void RequestPauseTimer(bool isPause, float remainTime)
        {
            //Debug.Log("CmdPauseTimer " + isPause.ToString());
            if(IsConnect)
            {
                CmdPauseTimer(isPause, remainTime);
            }
            else
            {
                Debug.Log("RequestPauseTimer : send command when not connect.");
            }
        }
        [Command]
        private void CmdPauseTimer(bool isPause, float remainTime)
        {
            //Debug.Log("CmdPauseTimer " + isPause.ToString());
            
            RpcPauseTimer(isPause, remainTime);
        }
        [ClientRpc]
        private void RpcPauseTimer(bool isPause, float remainTime)
        {        
            BattleManager manager = BattleController.Instance.BattleManager;
            
            manager.SetPauseTimeout(isPause);
            manager.SetRemainTurnTime(remainTime);
        }
        
        /*
        [Command]
        public void CmdSetRemainTurnTime(float remainTime)
        {
            //Debug.Log("CmdPauseTimer " + isPause.ToString());
            
            RpcSetRemainTurnTime(remainTime);
        }
        [ClientRpc]
        public void RpcSetRemainTurnTime(float remainTime)
        {        
            BattleManager manager = BattleController.Instance.BattleManager;
            
            manager.SetRemainTurnTime(remainTime);
        }
        */
        
        public void RequestSetTimeout(bool isTimeout)
        {
            //Debug.Log("CmdPauseTimer " + isPause.ToString());
            if(IsConnect)
            {
                CmdSetTimeout(isTimeout);
            }
            else
            {
                Debug.Log("RequestSetTimeout : send command when not connect.");
            }
        }
        [Command]
        private void CmdSetTimeout(bool isTimeout)
        {
            //Debug.Log("CmdPauseTimer " + isPause.ToString());
            
            RpcSetTimeout(isTimeout);
        }
        [ClientRpc]
        private void RpcSetTimeout(bool isTimeout)
        {        
            BattleManager manager = BattleController.Instance.BattleManager;
            
            manager.SetTimeout(isTimeout);
        }
    }

    public class NetworkSetting
    {
        private static bool _isNetworkEnabled;
        public static bool IsNetworkEnabled 
        { 
            get { return _isNetworkEnabled; } 
            set 
            { 
                _isNetworkEnabled = value;
                //Debug.Log("IsNetworkEnabled Setter!!");
            }
        }
        
        private static bool _isHaveTimeout;
        public static bool IsHaveTimeout 
        { 
            get { return _isHaveTimeout; } 
            set { _isHaveTimeout = value; }
        }
    }
}
