#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("z1tknyYI2zlGsbskwmEP6bgIAjBgf86MSUdkSU5KSkhNTX/O+VXO/IZWPboSQZowENS9akz1GsACEkK+jyx8OLh1SGMZpJVAbkGV9TxWAPoGlznQfFsq7jjbhmJNTE5PTuzNTsA8zi+JVBRGYN39twsHvy930Vq6PS4sOyYsKm88Oy47KiIqITs8YX9lyQfJuEJOTkpKT38tfkR/RklMGjsmKSYsLjsqby02by4hNm8/Lj07CjFQAyQf2Q7GizstRF/MDsh8xc7k7D7dCBwajuBgDvy3tKw/gqnsA0pPTM1OQE9/zU5FTc1OTk+r3uZGby4hK28sKj07JikmLC47JiAhbz9vIClvOycqbzsnKiFvLj8/IyYsLi0jKm88Oy4hKy49K287Kj0iPG8uYQ/puAgCMEcRf1BJTBpSbEtXf1nNTk9JRmXJB8m4LCtKTn/OvX9lSUJJRmXJB8m4Qk5OSkpPTM1OTk8TSX9ASUwaUlxOTrBLSn9MTk6wf1I1f81OOX9BSUwaUkBOTrBLS0xNTueTMW16hWqalkCZJJvta2xeuO7ja62knvg/kEAKrmiFviI3oqj6WFgdKiMmLiEsKm8gIW87JyY8bywqPVDelFEIH6RKohE2y2Kkee0YAxqjf81L9H/NTOzvTE1OTU1OTX9CSUb+fxejFUt9wyf8wFKRKjywKBEq8/p14rtAQU/dRP5uWWE7mnNClC1Z2tE1Q+sIxBSbWXh8hItAAoFbJp551gNiN/iiw9STvDjUvTmdOH8AjklMGlJBS1lLW2SfJgjbOUaxuyTCMA7n17aehSnTayRen+z0q1RljFCWeTCOyBqW6Nb2fQ20l5o+0THuHUtJXE0aHH5cf15JTBpLRVxFDj8/OycgPSY7Nn5Zf1tJTBpLTFxCDj8/IypvDCo9OyYpJiwuOyYgIW8OOhboSkYzWA8ZXlE7nPjEbHQI7JogODhhLj8/IyphLCAiYC4/PyMqLC74VPLcDWtdZYhAUvkC0xEshwTPWGNvLCo9OyYpJiwuOypvPyAjJiw2Nm8uPDw6Iio8by4sLCo/Oy4hLCrxuzzUoZ0rQIQ2AHuX7XG2N7AkhyErbywgISsmOyYgITxvIClvOjwqf15JTBpLRVxFDj8/IypvBiEsYX5yaShvxXwluELNgJGk7GC2HCUUK0dkSU5KSkhNTllRJzs7Pzx1YGA4RxF/zU5eSUwaUm9LzU5Hf81OS39IozJ2zMQcb5x3i/7w1QBFJLBks3p9fnt/fHkVWEJ8en99f3Z9fnt/KMBH+2+4hONjbyA/+XBOf8P4DIAremxaBFoWUvzbuLnT0YAf9Y4XH1l/W0lMGktMXEIOPz8jKm8dICA7IypvBiEsYX5pf2tJTBpLRFxSDj8/IypvHSAgO28MDn9RWEJ/eX97fUDScrxkBmdVh7GB+vZBlhFTmYRyJikmLC47JiAhbw46OycgPSY7Nn5pf2tJTBpLRFxSDj8/IypvDCo9O28MDn/NTm1/QklGZckHybhCTk5OUMrMylTWcgh4vebUD8Fjm/7fXZd8eRV/LX5Ef0ZJTBpLSVxNGhx+XMRWxpG2BCO6SORtf02nV3G3H0acH+XFmpWrs59GSHj/Ojpu");
        private static int[] order = new int[] { 32,37,19,57,27,55,46,37,39,25,24,14,45,41,42,31,34,47,29,28,54,53,38,31,32,26,31,29,48,30,47,58,48,55,47,52,36,59,44,40,50,43,58,56,52,55,51,59,53,54,55,57,57,57,58,57,56,58,58,59,60 };
        private static int key = 79;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
