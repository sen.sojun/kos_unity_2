﻿using UnityEngine;
using System.Collections;

public class TestFieldUIManager : MonoBehaviour 
{
	public FieldUIManager FieldUIManager;

	public GameObject BattleCardPrefab;


	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void CreateCard()
	{
		FieldUIManager.CreateDummyBattleCardUI(BattleZoneIndex.BaseP1, PlayerIndex.One);
	}
}
