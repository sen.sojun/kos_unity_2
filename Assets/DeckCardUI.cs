﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;
using UnityEngine.Events;

public class DeckCardUI : FullCardUI2 {
    public Text QtyText;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void setQtyText(string newqty)
    {
        QtyText.text = newqty;
        QtyText.gameObject.SetActive(false);
    }

    public void hideQtyText()
    {
        QtyText.gameObject.SetActive(false);
    }

}
