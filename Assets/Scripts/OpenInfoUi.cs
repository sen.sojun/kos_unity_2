﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenInfoUi : MonoBehaviour {

	string sHeadTitle;
	string sDesc;
	public UILabel HeadLabel;
	public UILabel DescriptionLabel;

	// Use this for initialization
	void Start () {
		sDesc = string.Format (Localization.Get ("SHOP_PREMIUM_BOOSTER_DESCRIPTION"));
		sHeadTitle = string.Format (Localization.Get ("SHOP_PREMIUM_BOOSTER_HEAD"));	
		HeadLabel.text = sHeadTitle;
		DescriptionLabel.text = sDesc;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
