﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


#region Equation Class
public class Equation
{
    #region EquationVar Class
    public class EquationVar
    {
        public enum VarType
        {
              Value
            , Operator
        }

        private VarType _type;
        private string _value;

        public VarType Type { get { return _type; } }
        public string Value { get { return _value; } }

        public EquationVar(VarType type, string value)
        {
            _type = type;
            _value = value;
        }
    }
    #endregion

    #region Enums
    public enum EquationType 
    {
          Bool
        , BattleCardFilter
        , UniqueCardFilter
        , BuffParam
    }
    #endregion

    #region Properties
    private EquationType _equationType;
    private List<EquationVar> _varList;

    public EquationType Type { get { return _equationType; } }
    public List<EquationVar> VarList { get { return _varList; } }
    #endregion

    #region Constructors
    private Equation(EquationType equationType, List<EquationVar> varList)
    {
        _equationType = equationType;
        _varList = varList;
    }
    #endregion

    #region Methods
    public bool GetBool(CardAbility.Ability ability)
    {
        #if UNITY_EDITOR
        if(this.Type != EquationType.Bool)
        {
            Debug.LogError("Equation/GetBool: Error! you try to get bool result from non-bool equation.");
            return false;
        }
        #endif

        Stack<bool> stack = new Stack<bool>();
        for(int index = 0; index < VarList.Count; ++index)
        {
            EquationVar equaVar = VarList[index];

            if (equaVar.Type == EquationVar.VarType.Value)
            {
                // Value
                CardAbility.AbilityParams abParam_0 = new CardAbility.AbilityParams(VarList[index].Value);

                bool result;
                CardAbility.AbilityParams.ParamToBool(abParam_0, ability, out result);

                stack.Push(result);
            }
            else
            {
                // Op
                switch(equaVar.Value)
                {
                    case "!":
                    {
                        bool v1 = stack.Pop();
                        stack.Push(!v1);
                    }
                    break;

                    case "&":
                    {
                        bool v1 = stack.Pop();
                        bool v2 = stack.Pop();
                        stack.Push(v1 && v2);
                    }
                    break;

                    case "|":
                    {
                        bool v1 = stack.Pop();
                        bool v2 = stack.Pop();
                        stack.Push(v1 || v2);
                    }
                    break;
                }
            }
        }

        bool finalResult = stack.Pop();
        return finalResult;
    }
        
    public List<BattleCardData> GetBattleCard(CardAbility.Ability ability)
    {
        List<BattleCardData> result = new List<BattleCardData>();

        #if UNITY_EDITOR
        if(this.Type != EquationType.BattleCardFilter)
        {
            Debug.LogError("Equation/GetBattleCard: Error! you try to get battlecard result from non-battlecard equation.");
            return result;
        }
        #endif

        if(BattleManager.Instance != null)
        {
            List<BattleCardData> cardList;
            BattleManager.Instance.GetAllBattleCard(out cardList);

            if (cardList.Count > 0)
            {
                foreach (BattleCardData card in cardList)
                {
                    Stack<bool> stack = new Stack<bool>();
                    for(int index = 0; index < VarList.Count; ++index)
                    {
                        EquationVar equaVar = VarList[index];

                        if (equaVar.Type == EquationVar.VarType.Value)
                        {
                            // Value
                            CardAbility.AbilityParams abParam_0 = new CardAbility.AbilityParams(VarList[index].Value);
                            BattleCardFilter filter;
                            CardAbility.AbilityParams.ParamToBattleCardFilter(abParam_0, ability, out filter);

                            stack.Push(filter.FilterResult(card));
                        }
                        else
                        {
                            // Op
                            switch(equaVar.Value)
                            {
                                case "!":
                                    {
                                        bool v1 = stack.Pop();
                                        stack.Push(!v1);
                                    }
                                    break;

                                case "&":
                                    {
                                        bool v1 = stack.Pop();
                                        bool v2 = stack.Pop();
                                        stack.Push(v1 && v2);
                                    }
                                    break;

                                case "|":
                                    {
                                        bool v1 = stack.Pop();
                                        bool v2 = stack.Pop();
                                        stack.Push(v1 || v2);
                                    }
                                    break;
                            }
                        }
                    }

                    bool isCandidate = stack.Pop();

                    if (isCandidate)
                    {
                        result.Add(card);
                    }
                }
            }
        }
        return result;
    }

    public List<UniqueCardData> GetUniqueCard(CardAbility.Ability ability)
    {
        List<UniqueCardData> result = new List<UniqueCardData>();

        #if UNITY_EDITOR
        if(this.Type != EquationType.BattleCardFilter)
        {
            Debug.LogError("Equation/GetBattleCard: Error! you try to get uniquecard result from non-uniquecard equation.");
            return result;
        }
        #endif

        if(BattleManager.Instance != null)
        {
            List<UniqueCardData> cardList;
            BattleManager.Instance.GetAllUniqueCard(out cardList);

            if (cardList.Count > 0)
            {
                foreach (UniqueCardData card in cardList)
                {
                    Stack<bool> stack = new Stack<bool>();
                    for(int index = 0; index < VarList.Count; ++index)
                    {
                        Equation.EquationVar equaVar = VarList[index];

                        if (equaVar.Type == Equation.EquationVar.VarType.Value)
                        {
                            // Value
                            CardAbility.AbilityParams abParam_0 = new CardAbility.AbilityParams(VarList[index].Value);
                            UniqueCardFilter filter;
                            CardAbility.AbilityParams.ParamToUniqueCardFilter(abParam_0, ability, out filter);

                            stack.Push(filter.FilterResult(card));
                        }
                        else
                        {
                            // Op
                            switch(equaVar.Value)
                            {
                                case "!":
                                    {
                                        bool v1 = stack.Pop();
                                        stack.Push(!v1);
                                    }
                                    break;

                                case "&":
                                    {
                                        bool v1 = stack.Pop();
                                        bool v2 = stack.Pop();
                                        stack.Push(v1 && v2);
                                    }
                                    break;

                                case "|":
                                    {
                                        bool v1 = stack.Pop();
                                        bool v2 = stack.Pop();
                                        stack.Push(v1 || v2);
                                    }
                                    break;
                            }
                        }
                    }

                    bool isCandidate = stack.Pop();

                    if (isCandidate)
                    {
                        result.Add(card);
                    }
                }
            }
        }
        return result;
    }

    public List<UniqueCardData> GetHandCard(CardAbility.Ability ability)
    {
        List<UniqueCardData> result = new List<UniqueCardData>();

        #if UNITY_EDITOR
        if(this.Type != EquationType.BattleCardFilter)
        {
            Debug.LogError("Equation/GetBattleCard: Error! you try to get uniquecard result from non-uniquecard equation.");
            return result;
        }
        #endif

        if(BattleManager.Instance != null)
        {
            List<UniqueCardData> cardList;
            BattleManager.Instance.GetAllUniqueCard(out cardList);

            if (cardList.Count > 0)
            {
                foreach (UniqueCardData card in cardList)
                {
                    if(card.CurrentZone != CardZoneIndex.Hand)
                    {
                        // if not hand card skip
                        continue;
                    }

                    Stack<bool> stack = new Stack<bool>();
                    for(int index = 0; index < VarList.Count; ++index)
                    {
                        Equation.EquationVar equaVar = VarList[index];

                        if (equaVar.Type == Equation.EquationVar.VarType.Value)
                        {
                            // Value
                            CardAbility.AbilityParams abParam_0 = new CardAbility.AbilityParams(VarList[index].Value);
                            UniqueCardFilter filter;
                            CardAbility.AbilityParams.ParamToUniqueCardFilter(abParam_0, ability, out filter);

                            stack.Push(filter.FilterResult(card));
                        }
                        else
                        {
                            // Op
                            switch(equaVar.Value)
                            {
                                case "!":
                                    {
                                        bool v1 = stack.Pop();
                                        stack.Push(!v1);
                                    }
                                    break;

                                case "&":
                                    {
                                        bool v1 = stack.Pop();
                                        bool v2 = stack.Pop();
                                        stack.Push(v1 && v2);
                                    }
                                    break;

                                case "|":
                                    {
                                        bool v1 = stack.Pop();
                                        bool v2 = stack.Pop();
                                        stack.Push(v1 || v2);
                                    }
                                    break;
                            }
                        }
                    }

                    bool isCandidate = stack.Pop();

                    if (isCandidate)
                    {
                        result.Add(card);
                    }
                }
            }
        }
        return result;
    }

    public override string ToString()
    {
        string resultText = "";
        foreach(EquationVar equaVar in VarList)
        {
            if (resultText.Length > 0)
            {
                resultText += " " + equaVar.Value;
            }
            else
            {
                resultText += equaVar.Value;
            }
        }
        return resultText;
    }
    #endregion

    #region Static Methods
    public static Equation BoolEquaTextToVar(string infixText, EquationType equationType)
    {
        List<EquationVar> result = new List<EquationVar>();

        Dictionary<char, int> opeatorList = new Dictionary<char, int>();
        opeatorList.Add('!', 3); // NOT
        opeatorList.Add('|', 2); // OR
        opeatorList.Add('&', 2); // AND
        opeatorList.Add('(', 1); 

        List<char> ignoreList = new List<char>();
        ignoreList.Add(' ');

        Stack<char> opStack = new Stack<char>();
        string text = "";

        bool isFunction = false;
        int quoteCount = 0;

        for (int index = 0; index < infixText.Length; ++index)
        {
            char c = infixText[index];
            bool isSkip = false;
            foreach(char ignore in ignoreList)
            {
                if (ignore == c)
                {
                    isSkip = true;
                    break;
                }
            }

            if (isSkip)
            {
                continue;
            }

            if (!opeatorList.ContainsKey(c) && c != ')')
            {
                // Not operator
                text = text + c.ToString();
            }
            else
            {
                // Operator
                if (isFunction)
                {
                    text = text + c.ToString();

                    if(c == '(')
                    {
                        ++quoteCount;
                    }
                    else if(c == ')')
                    {
                        --quoteCount;
                        if (quoteCount <= 0)
                        {
                            isFunction = false;

                            if (text.Length > 0)
                            {
                                EquationVar valueVar = new EquationVar(EquationVar.VarType.Value, text);
                                result.Add(valueVar);
                                text = "";
                            }
                        }
                    }
                }
                else
                {
                    if (c == '(')
                    {
                        if (text.Length > 0)
                        {
                            isFunction = true;
                            ++quoteCount;
                            text = text + c.ToString();
                        }
                        else
                        {
                            if(text.Length > 0)
                            {
                                EquationVar valueVar = new EquationVar(EquationVar.VarType.Value, text);
                                result.Add(valueVar);
                                text = "";
                            }

                            opStack.Push(c);
                        }
                    }
                    else if (c == ')')
                    {
                        if(text.Length > 0)
                        {
                            EquationVar valueVar = new EquationVar(EquationVar.VarType.Value, text);
                            result.Add(valueVar);
                            text = "";
                        }

                        char topToken = opStack.Pop();
                        while (topToken != '(')
                        {
                            EquationVar opVar = new EquationVar(EquationVar.VarType.Operator, topToken.ToString());
                            result.Add(opVar);
                            topToken = opStack.Pop();
                        }
                    }
                    else
                    {
                        if(text.Length > 0)
                        {
                            EquationVar valueVar = new EquationVar(EquationVar.VarType.Value, text);
                            result.Add(valueVar);
                            text = "";
                        }

                        while (opStack.Count > 0 && opeatorList[opStack.Peek()] >= opeatorList[c])
                        {
                            EquationVar opVar = new EquationVar(EquationVar.VarType.Operator, opStack.Pop().ToString());
                            result.Add(opVar);
                        }
                        opStack.Push(c);
                    }
                }
            }
        }

        if (text.Length > 0)
        {
            EquationVar valueVar = new EquationVar(EquationVar.VarType.Value, text);
            result.Add(valueVar);
            text = ""; 
        }

        while (opStack.Count > 0)
        {
            EquationVar opVar = new EquationVar(EquationVar.VarType.Operator, opStack.Pop().ToString());
            result.Add(opVar);
        }

        return new Equation(equationType, result);
    }
    #endregion
}
#endregion