﻿using UnityEngine;
using System.Collections;

public class PopupBoxUI : MonoBehaviour 
{
	public delegate void OnClickPopup();

	public PopupMessageBoxUI MessageBox;
	public PopupImageBoxUI ImageBox;

    private PopupBoxUI.OnClickPopup _onClickCallback = null;

	public void ShowMessageUI(string message, string buttonText = "OK", OnClickPopup callback = null, bool isKey = true)
    {
		ImageBox.HideUI();

        _onClickCallback = callback;
		MessageBox.ShowUI(message, buttonText, isKey);
       
        gameObject.SetActive(true);
		BattleUIManager.RequestBringForward(gameObject);
    }

	public void ShowImageUI(string imageFile, string title,string page,string message, string buttonText = "OK", OnClickPopup callback = null, bool isKey = true)
	{
		MessageBox.HideUI();

        _onClickCallback = callback;   
		ImageBox.ShowUI(imageFile, title, page, message, buttonText, isKey);

        gameObject.SetActive(true);
		BattleUIManager.RequestBringForward(gameObject);
	}

    public void HideUI()
    {
        gameObject.SetActive(false);
    }

    public void OnClickButton()
    {
        if (_onClickCallback != null)
        {
            _onClickCallback.Invoke();
        }
    }
}
