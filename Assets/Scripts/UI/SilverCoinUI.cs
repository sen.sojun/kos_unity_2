﻿using UnityEngine;
using System.Collections;

public class SilverCoinUI : MonoBehaviour 
{
	#region Private
	private int __currentSilverCoin = 0;

	private int _currentSilverCoin
	{
		get { return __currentSilverCoin; }
		set 
		{
			__currentSilverCoin = value;

			UpdateSilverCoinLabel ();
		}
	}
	#endregion

	#region Public Inspector Properties
	public UILabel SilverCoinLabel;
	public UIButton AddSilverButton;

	#endregion

	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
		/*
		if (MyAdmobManager.Instance.IsRewardedVideoReady)
		{
			if(!AddSilverButton.gameObject.activeSelf)
				AddSilverButton.gameObject.SetActive (true);
		} 
		else
		{
			if(AddSilverButton.gameObject.activeSelf)
				AddSilverButton.gameObject.SetActive (false);
		}
		*/
	}

	void OnEnable () 
	{
		_currentSilverCoin = PlayerSave.GetPlayerSilverCoin();
	}
		
	public void UpdateSilverCoin()
	{
		_currentSilverCoin = PlayerSave.GetPlayerSilverCoin();
	}
		
	private void UpdateSilverCoinLabel()
	{
		
		SilverCoinLabel.text = "" + _currentSilverCoin.ToString ("##,###,##0") + "";
	}
}
