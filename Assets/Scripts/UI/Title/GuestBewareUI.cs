﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class GuestBewareUI : MonoBehaviour 
{
    private UnityEvent _clickContinueButtonEvent = null;
    private UnityEvent _clickCancelButtonEvent = null;
    
    public Button ContinueButton;
    public Button CancelButton;

    /*
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    */
    
    public void ShowUI(UnityAction onClickContinueButton, UnityAction onClickCancelButton)
    {
        // Continue
        if(_clickContinueButtonEvent == null)
        {
            _clickContinueButtonEvent = new UnityEvent();
        }      
        _clickContinueButtonEvent.RemoveAllListeners();
        _clickContinueButtonEvent.AddListener(onClickContinueButton);
        
        ContinueButton.onClick.RemoveAllListeners();
        ContinueButton.onClick.AddListener(OnClickContinueButton);
        
        // Cancel
        if(_clickCancelButtonEvent == null)
        {
            _clickCancelButtonEvent = new UnityEvent();
        }
        _clickCancelButtonEvent.RemoveAllListeners();
        _clickCancelButtonEvent.AddListener(onClickCancelButton);
        
        CancelButton.onClick.RemoveAllListeners();
        CancelButton.onClick.AddListener(OnClickCancelButton);
    
        gameObject.SetActive(true);
    }
    
    public void HideUI()
    {
        gameObject.SetActive(false);
    }
    
    private void OnClickContinueButton()
    {
        HideUI();
    
        if(_clickContinueButtonEvent != null)
        {
            _clickContinueButtonEvent.Invoke();
        }
    }
    
    private void OnClickCancelButton()
    {
        HideUI();
    
        if(_clickCancelButtonEvent != null)
        {
            _clickCancelButtonEvent.Invoke();
        }
    }
}
