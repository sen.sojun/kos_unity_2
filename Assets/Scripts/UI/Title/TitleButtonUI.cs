﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class TitleButtonUI : MonoBehaviour 
{
    private static TitleButtonUI _instance = null;
    public static TitleButtonUI Instance { get { return _instance; } }

    private UnityEvent _clickGuestButtonEvent = null;
    private UnityEvent _clickKOSButtonEvent   = null;
    private UnityEvent _clickStartButtonEvent = null;

    public Button GuestButton;
    public Button KOSButton;
    public Button StartGameButton;

    /*
    // Use this for initialization
    void Start () {
        
    }
    
    // Update is called once per frame
    void Update () {
        
    }
    */

    private void Start()
    {
        _instance = this;
    }

    public void ShowUI(UnityAction onClickGuestButton, UnityAction onClickKOSButton)
    {
        ShowNormallButton();

        // Guest
        if (_clickGuestButtonEvent == null)
        {
            _clickGuestButtonEvent = new UnityEvent();
        }      
        _clickGuestButtonEvent.RemoveAllListeners();
        _clickGuestButtonEvent.AddListener(onClickGuestButton);
        
        GuestButton.onClick.RemoveAllListeners();
        GuestButton.onClick.AddListener(OnClickGuestButton);
        
        // KOS
        if(_clickKOSButtonEvent == null)
        {
            _clickKOSButtonEvent = new UnityEvent();
        }
        _clickKOSButtonEvent.RemoveAllListeners();
        _clickKOSButtonEvent.AddListener(onClickKOSButton);
        
        KOSButton.onClick.RemoveAllListeners();
        KOSButton.onClick.AddListener(OnClickKOSButton);
        StartGameButton.gameObject.SetActive(false);
    
        gameObject.SetActive(true);
    }

    public void ShowStartButton(UnityAction onClickStartButton)
    {
        //StartButton
        GuestButton.gameObject.SetActive(false);
        KOSButton.gameObject.SetActive(false);
        StartGameButton.gameObject.SetActive(true);

        if (_clickStartButtonEvent == null)
        {
            _clickStartButtonEvent = new UnityEvent();
        }
        _clickStartButtonEvent.RemoveAllListeners();
        _clickStartButtonEvent.AddListener(onClickStartButton);

        StartGameButton.onClick.RemoveAllListeners();
        StartGameButton.onClick.AddListener(onClickStartButton);

        gameObject.SetActive(true);
    }

    public void HideAllButton()
    {
        //StartButton
        GuestButton.gameObject.SetActive(false);
        KOSButton.gameObject.SetActive(false);
        StartGameButton.gameObject.SetActive(false);
    }

    public void ShowNormallButton()
    {
        //StartButton
        GuestButton.gameObject.SetActive(true);
        KOSButton.gameObject.SetActive(true);
        StartGameButton.gameObject.SetActive(false);
    }




    public void HideUI()
    {
        gameObject.SetActive(false);
    }
    
    private void OnClickGuestButton()
    {
        HideUI();
    
        if(_clickGuestButtonEvent != null)
        {
            _clickGuestButtonEvent.Invoke();
        }
    }
    
    private void OnClickKOSButton()
    {
        HideUI();
    
        if(_clickKOSButtonEvent != null)
        {
            _clickKOSButtonEvent.Invoke();
        }
    }

    public void HideStartButton()
    {
        StartGameButton.gameObject.SetActive(false);
    }

    public void ShowOnlyStartButton()
    {
        StartGameButton.gameObject.SetActive(true);
    }
}
