﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class MessageBoxUI : MonoBehaviour 
{
    private UnityEvent _clickCloseButtonEvent = null;

    public Text HeaderText;
    public Text MessageText;
    public Button CloseButton;

    /*
    // Use this for initialization
    void Start () {
        
    }
    
    // Update is called once per frame
    void Update () {
        
    }
    */
    
    public void ShowUI(string header, string message, string buttonText, UnityAction onClickCloseButton)
    {        
        // Close
        if(_clickCloseButtonEvent == null)
        {
            _clickCloseButtonEvent = new UnityEvent();
        }
        _clickCloseButtonEvent.RemoveAllListeners();
        _clickCloseButtonEvent.AddListener(onClickCloseButton);

        CloseButton.onClick.RemoveAllListeners();
        CloseButton.onClick.AddListener(OnClickCloseButton);
        
        HeaderText.text = header;
        MessageText.text = message;
        CloseButton.GetComponentInChildren<Text>().text = buttonText;
    
        gameObject.SetActive(true);
    }
    
    public void HideUI()
    {
        gameObject.SetActive(false);
    }
    
    private void OnClickCloseButton()
    {
        HideUI();
    
        if(_clickCloseButtonEvent != null)
        {
            _clickCloseButtonEvent.Invoke();
        }
    }
}
