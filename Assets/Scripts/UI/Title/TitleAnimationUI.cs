﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleAnimationUI : MonoBehaviour 
{
    private bool _isTrugger = false;

    /*
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    */
    
    public void OnTitleAnimationCompleted()
    {
        //Debug.Log("Title animation completed!");
        
        if(TitleManager.Instance != null)
        {
            if(!_isTrugger)
            {
                _isTrugger = true;
                TitleManager.Instance.OnTitleAnimationCompleted();
            }
        }
    }
}
