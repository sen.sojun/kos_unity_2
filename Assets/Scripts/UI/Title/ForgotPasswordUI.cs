﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class ForgotPasswordUI : MonoBehaviour 
{
    private UnityStringEvent _clickSendButtonEvent = null;
    private UnityEvent _clickCancelButtonEvent = null;
    
    public InputField EmailInputField;
    public Button SendButton;
    public Button CancelButton;

    /*
    // Use this for initialization
    void Start () {
        
    }
    
    // Update is called once per frame
    void Update () {
        
    }
    */
    
    public void ShowUI(UnityAction<string> onClickSendButton, UnityAction onClickCancelButton)
    {
        // Send
        if(_clickSendButtonEvent == null)
        {
            _clickSendButtonEvent = new UnityStringEvent();
        }      
        _clickSendButtonEvent.RemoveAllListeners();
        _clickSendButtonEvent.AddListener(onClickSendButton);
        
        SendButton.onClick.RemoveAllListeners();
        SendButton.onClick.AddListener(OnClickSendButton);
        
        // Cancel
        if(_clickCancelButtonEvent == null)
        {
            _clickCancelButtonEvent = new UnityEvent();
        }
        _clickCancelButtonEvent.RemoveAllListeners();
        _clickCancelButtonEvent.AddListener(onClickCancelButton);

        CancelButton.onClick.RemoveAllListeners();
        CancelButton.onClick.AddListener(OnClickCancelButton);
        
        // Clear text
        EmailInputField.text = "";
    
        gameObject.SetActive(true);
    }
    
    public void HideUI()
    {
        gameObject.SetActive(false);
    }
    
    private void OnClickSendButton()
    {
        HideUI();
    
        if(_clickSendButtonEvent != null)
        {
            _clickSendButtonEvent.Invoke(EmailInputField.text);
        }
    }
    
    private void OnClickCancelButton()
    {
        HideUI();
    
        if(_clickCancelButtonEvent != null)
        {
            _clickCancelButtonEvent.Invoke();
        }
    }
}


