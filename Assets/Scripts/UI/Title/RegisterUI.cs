﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class RegisterUI : MonoBehaviour 
{
    private UnityString2Event _clickCreateButtonEvent = null;
    private UnityEvent _clickCancelButtonEvent = null;
    
    public InputField EmailInputField;
    public InputField PasswordInputField;
    public Button CreateButton;
    public Button CancelButton;

    //for terms of use
    public Image imageCover;
    public Text textCheckMark;
    public Button buttonCheckMark;
    public Button buttonLinkTermsofuse;


    /*
    // Use this for initialization
    void Start () {
        
    }
    
    // Update is called once per frame
    void Update () {
        
    }
    */
    
    public void ShowUI(UnityAction<string, string> onClickCreateButton, UnityAction onClickCancelButton)
    {
        //OnClickCheckMark();
        // Create
        if(_clickCreateButtonEvent == null)
        {
            _clickCreateButtonEvent = new UnityString2Event();
        }      
        _clickCreateButtonEvent.RemoveAllListeners();
        _clickCreateButtonEvent.AddListener(onClickCreateButton);
        
        CreateButton.onClick.RemoveAllListeners();
        CreateButton.onClick.AddListener(OnClickCreateButton);
        
        // Cancel
        if(_clickCancelButtonEvent == null)
        {
            _clickCancelButtonEvent = new UnityEvent();
        }
        _clickCancelButtonEvent.RemoveAllListeners();
        _clickCancelButtonEvent.AddListener(onClickCancelButton);

        CancelButton.onClick.RemoveAllListeners();
        CancelButton.onClick.AddListener(OnClickCancelButton);
        
        // Clear text
        EmailInputField.text = "";
        PasswordInputField.text = "";
    
        gameObject.SetActive(true);
    }
    
    public void HideUI()
    {
        gameObject.SetActive(false);
    }
    
    private void OnClickCreateButton()
    {
        HideUI();
    
        if(_clickCreateButtonEvent != null)
        {
            _clickCreateButtonEvent.Invoke(EmailInputField.text, PasswordInputField.text);
        }
    }
    
    private void OnClickCancelButton()
    {
        HideUI();
    
        if(_clickCancelButtonEvent != null)
        {
            _clickCancelButtonEvent.Invoke();
        }
    }

    public void OnClickCheckMark()
    {
        if (textCheckMark.gameObject.activeSelf)
        {
            textCheckMark.gameObject.SetActive(false);
            imageCover.gameObject.SetActive(true);
        }
        else
        {
            textCheckMark.gameObject.SetActive(true);
            imageCover.gameObject.SetActive(false);
        }
    }

    public void OnClickTermOfUse()
    {
        Application.OpenURL("http://koscardgame.com/terms-of-use-and-privacy-policy/");
    }
}
