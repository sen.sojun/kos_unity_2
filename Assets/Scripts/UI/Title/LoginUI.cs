﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class LoginUI : MonoBehaviour 
{
    private UnityString2Event _clickLoginButtonEvent = null;
    private UnityEvent _clickCancelButtonEvent = null;
    private UnityEvent _clickCreateButtonEvent = null;
    private UnityEvent _clickForgotPasswordButtonEvent = null;
    
    public InputField EmailInputField;
    public InputField PasswordInputField;
    public Button LoginButton;
    public Button CancelButton;
    public Button CreateButton;
    public Button ForgotPasswordButton;

    /*
    // Use this for initialization
    void Start () {
        
    }
    
    // Update is called once per frame
    void Update () {
        
    }
    */
    
    public void ShowUI(UnityAction<string, string> onClickLoginButton, UnityAction onClickCancelButton, UnityAction onClickCreateButton, UnityAction onClickForgotPasswordButton)
    {
        // Login
        if(_clickLoginButtonEvent == null)
        {
            _clickLoginButtonEvent = new UnityString2Event();
        }      
        _clickLoginButtonEvent.RemoveAllListeners();
        _clickLoginButtonEvent.AddListener(onClickLoginButton);
        
        LoginButton.onClick.RemoveAllListeners();
        LoginButton.onClick.AddListener(OnClickLoginButton);
        
        // Cancel
        if(_clickCancelButtonEvent == null)
        {
            _clickCancelButtonEvent = new UnityEvent();
        }
        _clickCancelButtonEvent.RemoveAllListeners();
        _clickCancelButtonEvent.AddListener(onClickCancelButton);
        
        CancelButton.onClick.RemoveAllListeners();
        CancelButton.onClick.AddListener(OnClickCancelButton);
        
        // Create
        if(_clickCreateButtonEvent == null)
        {
            _clickCreateButtonEvent = new UnityEvent();
        }
        _clickCreateButtonEvent.RemoveAllListeners();
        _clickCreateButtonEvent.AddListener(onClickCreateButton);
        
        CreateButton.onClick.RemoveAllListeners();
        CreateButton.onClick.AddListener(OnClickCreateButton);
        
        // Forgot Password
        if(_clickForgotPasswordButtonEvent == null)
        {
            _clickForgotPasswordButtonEvent = new UnityEvent();
        }
        _clickForgotPasswordButtonEvent.RemoveAllListeners();
        _clickForgotPasswordButtonEvent.AddListener(onClickForgotPasswordButton);
        
        ForgotPasswordButton.onClick.RemoveAllListeners();
        ForgotPasswordButton.onClick.AddListener(OnClickForgotPasswordButton);
        
        // Clear text
        EmailInputField.text = "";
        PasswordInputField.text = "";
    
        gameObject.SetActive(true);
    }
    
    public void HideUI()
    {
        gameObject.SetActive(false);
    }
    
    private void OnClickLoginButton()
    {
        HideUI();
    
        if(_clickLoginButtonEvent != null)
        {
            _clickLoginButtonEvent.Invoke(EmailInputField.text, PasswordInputField.text);
        }
    }
    
    private void OnClickCancelButton()
    {
        HideUI();
    
        if(_clickCancelButtonEvent != null)
        {
            _clickCancelButtonEvent.Invoke();
        }
    }
    
    private void OnClickCreateButton()
    {
        HideUI();
    
        if(_clickCreateButtonEvent != null)
        {
            _clickCreateButtonEvent.Invoke();
        }
    }
    
    private void OnClickForgotPasswordButton()
    {
        HideUI();
    
        if(_clickForgotPasswordButtonEvent != null)
        {
            _clickForgotPasswordButtonEvent.Invoke();
        }
    }
}

