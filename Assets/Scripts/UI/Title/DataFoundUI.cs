﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class DataFoundUI : MonoBehaviour 
{
    private UnityEvent _clickCloseButtonEvent = null;
    
    public Button CloseButton;
    
    /*
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    */
    
    public void ShowUI(UnityAction onClickButton)
    {
        if(_clickCloseButtonEvent == null)
        {
            _clickCloseButtonEvent = new UnityEvent();
        }
        
        _clickCloseButtonEvent.RemoveAllListeners();
        _clickCloseButtonEvent.AddListener(onClickButton);
        
        CloseButton.onClick.RemoveAllListeners();
        CloseButton.onClick.AddListener(OnClickButton);
    
        gameObject.SetActive(true);
    }
    
    public void HideUI()
    {
        gameObject.SetActive(false);
    }
    
    private void OnClickButton()
    {
        HideUI();
    
        if(_clickCloseButtonEvent != null)
        {
            _clickCloseButtonEvent.Invoke();
        }
    }
}
