﻿using UnityEngine;
using System.Collections;

public class PopupImageBoxUI : MonoBehaviour 
{
    public UITexture ImageTexture;
    public UILabel   MessageLabel;
	public UILabel   TutorialTitle; //added vit21092016
	public UILabel   TutorialPage;
    public UIButton  Button;
    public UILabel   ButtonLabel;

	// Use this for initialization
	void Start () 
    {
	}
	
    /*
	// Update is called once per frame
	void Update () {
	
	}
    */

	public void ShowUI(string imageFilePath, string title,string page,string message,string buttonText = "OK", bool isKey = true)
    {
        Texture imageTexture = Resources.Load(imageFilePath, typeof(Texture)) as Texture;

        if (imageTexture != null)
        {
            ImageTexture.mainTexture = imageTexture;
			Resources.UnloadUnusedAssets ();
			//Resources.UnloadAsset(imageTexture);
        }

		if(isKey)
		{
			MessageLabel.GetComponent<UILocalize>().key = message;
			ButtonLabel.GetComponent<UILocalize>().key = buttonText;
			TutorialTitle.GetComponent<UILocalize>().key = title; 
			TutorialPage.text = page;
			ButtonLabel.GetComponent<UILocalize>().key = buttonText;
		}
		else
		{
			MessageLabel.GetComponent<UILocalize>().key = message;
			ButtonLabel.GetComponent<UILocalize>().key = buttonText;
			TutorialTitle.GetComponent<UILocalize>().key = title; 
			TutorialPage.text = page;
			ButtonLabel.GetComponent<UILocalize>().key = buttonText;

			MessageLabel.GetComponent<UILocalize>().value = message;
			ButtonLabel.GetComponent<UILocalize>().value = buttonText;
			TutorialTitle.GetComponent<UILocalize>().value = title; 
			TutorialPage.text = page;
			ButtonLabel.GetComponent<UILocalize>().value = buttonText;
		}

        gameObject.SetActive(true);
		BattleUIManager.RequestBringForward(gameObject);
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }
}
