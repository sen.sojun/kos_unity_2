﻿using UnityEngine;
using System.Collections;

public class SelectBattleCardUI : MonoBehaviour 
{
	public delegate void OnCancelSelectTarget();

	private OnCancelSelectTarget _onCancelSelectTarget = null;

	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
	}

	public void ShowUI(OnCancelSelectTarget onCancel)
	{
		_onCancelSelectTarget = onCancel;
		gameObject.SetActive (true);
	}

	public void HideUI()
	{
		gameObject.SetActive (false);
	}

	public void OnCancel()
	{
		HideUI ();

		if (_onCancelSelectTarget != null)
		{
			_onCancelSelectTarget.Invoke ();
		}
	}
}
