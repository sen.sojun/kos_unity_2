﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(UISlider))]
public class TimerUI : MonoBehaviour 
{
    private UISlider _sider;
    
    public Color PlayerColor;
    public Color EnemyColor;

    void Awake()
    {
        _sider = GetComponent<UISlider>();
        _sider.value = 0.0f;
        _sider.foregroundWidget.color = PlayerColor;
    }
    
	// Use this for initialization
	void Start () 
    {	
        if(!BattleManager.Instance.IsHaveTimeout)
        {
            gameObject.SetActive(false);
        }
        
        //_sider.foregroundWidget.depth = _sider.GetComponent<UIWidget>().depth + 1;
	}
	
	// Update is called once per frame
	void Update () 
    {
        UpdateTime();
	}
    
    void UpdateTime()
    {
        if(BattleManager.Instance != null)
        {
            if(BattleManager.Instance.IsHaveTimeout)
            {               
                float ratio = BattleManager.Instance.TurnTimer/BattleManager.Instance.PlayTimePerTurn;
                ratio = Mathf.Clamp(ratio, 0.0f, 1.0f); 
                
                _sider.value = ratio;
                _sider.foregroundWidget.depth = _sider.GetComponent<UIWidget>().depth + 1;

                if(ratio > 0.0f)
                {
                    if(BattleManager.Instance.IsLocalTurn())
                    {
                        _sider.foregroundWidget.color = PlayerColor;
                    }
                    else
                    {
                        _sider.foregroundWidget.color = EnemyColor;
                    }
                }
                
                return;
            }
        }
        
        _sider.value = 0.0f;
        _sider.foregroundWidget.depth = _sider.GetComponent<UIWidget>().depth + 1;
    }
}
