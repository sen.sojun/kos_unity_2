﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShopCardResultUI : MonoBehaviour 
{
	public List<FullCardUI> FullCardList;
	public List<GameObject> GlowList;
	public List<GameObject> NewLabelList;

    public UILabel lblHeaderText;



	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
	}
    ////start of insertion vit 02112017 w31 h140 38 39 
    /// 
    public void ShowUI(string sAmtCard)
    {
        string headText = "";
        string phural = "s";

        if (sAmtCard == "1")
        {
            phural = "";
        }

       
		gameObject.SetActive (true);

		BattleUIManager.RequestBringForward (gameObject);
        Debug.Log("Amount = " + sAmtCard);

        if (LocalizationManager.CurrentLanguage == LocalizationManager.Language.Thai )
        {
            headText = string.Format(Localization.Get("SHOP_NEW_CARD_HEADER_AMOUNT"), sAmtCard);
        }
        else
        {
            headText = string.Format(Localization.Get("SHOP_NEW_CARD_HEADER_AMOUNT"), sAmtCard, phural);
        }
      //  headText = string.Format(Localization.Get("SHOP_NEW_CARD_HEADER"), sAmtCard, phural);

        lblHeaderText.text = headText;
        //lblHeaderText.gameObject.SetActive(true);
       // Debug.Log(headText);


		foreach(FullCardUI card in FullCardList)
		{
			card.gameObject.SetActive (false);
		}

		foreach(GameObject glow in GlowList)
		{
			glow.SetActive (false);
		}

		foreach(GameObject newLabel in NewLabelList)
		{
			newLabel.SetActive (false);
		}
	}
    /////end of insertion vit 02112017

    ////start of deletion 02112017
    //public void ShowUI()
    //{

    //    gameObject.SetActive(true);

    //    BattleUIManager.RequestBringForward(gameObject);

    //    foreach (FullCardUI card in FullCardList)
    //    {
    //        card.gameObject.SetActive(false);
    //    }

    //    foreach (GameObject glow in GlowList)
    //    {
    //        glow.SetActive(false);
    //    }

    //    foreach (GameObject newLabel in NewLabelList)
    //    {
    //        newLabel.SetActive(false);
    //    }
    //}
    ////end of deletion 02112017



	public void ShowCard(int index, PlayerCardData data, bool isGlow, bool isNew, FullCardUI.OnClickEvent onClickedPreview)
	{
		FullCardList[index].SetCardData (data);
		FullCardList [index].gameObject.SetActive (true);
		GlowList[index].SetActive (isGlow);
		NewLabelList[index].SetActive (isNew);

		FullCardList [index].RemoveAllOnClickCallback ();
		FullCardList [index].BindOnClickCallback (onClickedPreview);
	}

	public void HideUI()
	{
		gameObject.SetActive (false);
	}
}
