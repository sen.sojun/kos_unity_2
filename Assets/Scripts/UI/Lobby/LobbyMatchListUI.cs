﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LobbyMatchListUI : MonoBehaviour 
{
    public GameObject MatchDetailPrefab;
    public ScrollRect ScrollView;
    public GameObject Content;

	// Use this for initialization
	void Start () 
    {	
	}
	
	// Update is called once per frame
	void Update () 
    {	
	}
    
    public void SetData(List<MatchLogData> dataList)
    {
        ClearLog();
        
        if(dataList != null && dataList.Count > 0)
        {
            //ScrollView.gameObject.SetActive(true);
            
            int count = 0;
            for(int i = dataList.Count - 1; i >= 0 && count < 5; --i)
            {
                CreateLog(dataList[i]);
                ++count;
            }
        }
        else
        {
            //ScrollView.gameObject.SetActive(false);
        }
    }
    
        
    private void ClearLog()
    {
        while (Content.transform.childCount != 0)
        {
            Transform child = Content.transform.GetChild(0);

            child.SetParent(null);
            UnityEngine.Object.Destroy(child.gameObject);
        }
    }
    
    private void CreateLog(MatchLogData data)
    {
        if(data != null)
        {
            GameObject obj = Instantiate(MatchDetailPrefab, Content.transform) as GameObject;
            obj.GetComponent<LobbyMatchDetailUI>().SetData(data);
        }
    }
}
