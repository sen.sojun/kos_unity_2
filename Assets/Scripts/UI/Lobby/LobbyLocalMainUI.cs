﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class LobbyLocalMainUI : MonoBehaviour 
{
    public Button CreateRoomButton;
    public Button JoinRoomButton;
    public Button BackButton;
    
    public UnityEvent OnClickCreateRoomEvent;
    public UnityEvent OnClickJoinRoomEvent;
    public UnityEvent OnClickBackEvent;
    
    /*
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    */
    
    #region Methods
    public void ShowUI()
    {        
        CreateRoomButton.onClick.RemoveAllListeners();
        JoinRoomButton.onClick.RemoveAllListeners();
        BackButton.onClick.RemoveAllListeners();
        
        CreateRoomButton.onClick.AddListener(this.OnClickCreateRoom);
        JoinRoomButton.onClick.AddListener(this.OnClickJoinRoom);
        BackButton.onClick.AddListener(this.OnClickBack);
          
        LobbyUIManager.Instance.TopBar.SetUpBar(
              true
            , LocalizationManager.GetText("LOBBY_DIRECT_CONNECT_HEADER")
            , this.OnClickBack
        );
          
        gameObject.SetActive(true);
    }
    
    public void HideUI()
    {
        gameObject.SetActive(false);
    }
    #endregion
    
    #region Event Methods
    public void OnClickCreateRoom()
    {
        if(OnClickCreateRoomEvent != null)
        {
            OnClickCreateRoomEvent.Invoke();
        }
    }
    
    public void OnClickJoinRoom()
    {
        if(OnClickJoinRoomEvent != null)
        {
            OnClickJoinRoomEvent.Invoke();
        }
    }
    
    public void OnClickBack()
    {
        if(OnClickBackEvent != null)
        {
            OnClickBackEvent.Invoke();
        }
    }
    #endregion
}
