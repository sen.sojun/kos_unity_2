﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class LobbyLocalJoinUI : MonoBehaviour 
{
    public InputField IPInputField;
    public Button JoinButton;
    public Button BackButton;
    
    public UnityStringEvent OnClickJoinRoomEvent;
    public UnityEvent OnClickBackEvent;

    /*
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    */
    
    #region Methods
    public void ShowUI()
    {        
        JoinButton.onClick.RemoveAllListeners();
        BackButton.onClick.RemoveAllListeners();
        
        JoinButton.onClick.AddListener(this.OnClickJoin);
        BackButton.onClick.AddListener(this.OnClickBack);
          
        gameObject.SetActive(true);
    }
    
    public void HideUI()
    {
        gameObject.SetActive(false);
    }
    #endregion
    
    #region Event Methods
    public void OnClickJoin()
    {
        if(OnClickJoinRoomEvent != null)
        {
            OnClickJoinRoomEvent.Invoke(IPInputField.text);
        }
    }
       
    public void OnClickBack()
    {
        if(OnClickBackEvent != null)
        {
            OnClickBackEvent.Invoke();
        }
    }
    #endregion
}
