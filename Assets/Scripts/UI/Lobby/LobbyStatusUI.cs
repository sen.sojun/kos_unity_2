﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class LobbyStatusUI : MonoBehaviour 
{
    public Text StatusText;
    public Button CancelButton;
    
    /*
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    */
    
    public void ShowUI(string message, UnityAction onClickCancel)
    {
        StatusText.text = message;
        
        CancelButton.onClick.RemoveAllListeners();       
        CancelButton.onClick.AddListener(this.OnClickCancel);
        if(onClickCancel != null)
        {
            CancelButton.onClick.AddListener(onClickCancel);
        }
        
        //LobbyUIManager.Instance.TopBar.DisableTopbarButtonButton();        
        gameObject.SetActive(true);
    }
    
    public void HideUI()
    {
        //LobbyUIManager.Instance.TopBar.EnableTopbarButtonButton();    
        gameObject.SetActive(false);
    }
    
    public void OnClickCancel()
    {
        HideUI();
    }
}
