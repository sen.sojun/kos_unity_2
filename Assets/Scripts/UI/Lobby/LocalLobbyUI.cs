﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class LocalLobbyUI : MonoBehaviour 
{    
    public LobbyLocalMainUI MainUI;
    public LobbyLocalJoinUI JoinUI;

	// Use this for initialization
	void Start () 
    {	        
	}
	
    /*
	// Update is called once per frame
	void Update () 
    {	
	}
    */
    
    public void ShowUI()
    {       
        ShowMainUI();
        
        gameObject.SetActive(true);
    }
    
    public void HideUI()
    {
        gameObject.SetActive(false);
    }
    
    public void ShowMainUI()
    {
        MainUI.ShowUI();
        JoinUI.HideUI();
        
        LobbyUIManager.Instance.TopBar.SetUpBar(
              true
            , LocalizationManager.GetText("LOBBY_DIRECT_CONNECT_HEADER")
            , OnClickBackMain
        );    
    }
    
    public void ShowJoinUI()
    {
        MainUI.HideUI();
        JoinUI.ShowUI();
        
        LobbyUIManager.Instance.TopBar.SetUpBar(
              true
            , LocalizationManager.GetText("LOBBY_JOIN_BATTLE_ROOM_HEADER")
            , OnClickBackJoin
        ); 
    }
    
    public void OnClickHost()
    {
        KOSLobbyManager.Instance.RequestStartHost();
    }
    
    public void OnClickJoin(string ip)
    {
        KOSLobbyManager.Instance.RequestJoinLocal(ip);
    }
    
    public void OnClickBackMain()
    {
        LobbyUIManager.Instance.ShowMainLobby();
    }
    
    public void OnClickBackJoin()
    {
        ShowMainUI();
    }
}
