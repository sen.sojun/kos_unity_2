﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

//Player entry in the lobby. Handle selecting color/setting name & getting ready for the game
//Any LobbyHook can then grab it and pass those value to the game player prefab (see the Pong Example in the Samples Scenes)
public class KOSLobbyPlayer : NetworkLobbyPlayer
{
    private static float readyWaitTime = 1.0f;

    public enum PlayerStatus
    {
          Waiting
        , NotReady
        , Ready
    }

    [SyncVar(hook = "OnMyImage")]
    private string _imagePath;
    [SyncVar(hook = "OnMyName")]
    private string _name;
    [SyncVar(hook = "OnMyStatus")]
    private PlayerStatus _status;
    
    private float _timer = 0.0f;
    private bool _isEnter = false;
    
    public string ImagePath { get { return _imagePath; } }
    public string PlayerName { get { return _name; } }
    public PlayerStatus Status { get { return _status; } }

    private void Update()
    {
        if(this.isLocalPlayer)
        {
            if(!this.readyToBegin && _status == PlayerStatus.NotReady && _isEnter)
            {
                _timer -= Time.deltaTime;
                if(_timer <= 0.0f)
                {
                    OnReadyClicked();
                    
                    _timer = readyWaitTime;
                }
            }
        }
    }

    public override void OnClientEnterLobby()
    {
        base.OnClientEnterLobby();
        
        //_timer = 0.0f;
        _status = PlayerStatus.NotReady;
        KOSLobbyManager.Instance.AddPlayer(this);
        
        if (isLocalPlayer)
        {
            SetupLocalPlayer();
        }
        else
        {
            SetupOtherPlayer();
        }
        
        OnMyName(_name);
        OnMyImage(_imagePath);
    }
    
    /*
    public override void OnClientExitLobby()
    {
        base.OnClientExitLobby();
        
        KOSLobbyManager.Instance.RemovePlayer(this);
    }
    */

    public override void OnStartAuthority()
    {
        base.OnStartAuthority();

        SetupLocalPlayer();
    }

    void SetupOtherPlayer()
    {
        OnClientReady(false);
        
        if(LobbyPlayerListUI.Instance != null)
        {
            LobbyPlayerListUI.Instance.PlayerListModified();
        }
    }

    void SetupLocalPlayer()
    {
        /*
        nameInput.interactable = true;

        CheckRemoveButton();

        if (playerColor == Color.white)
            CmdColorChange();

        ChangeReadyButtonColor(JoinColor);

        readyButton.transform.GetChild(0).GetComponent<Text>().text = "JOIN";
        readyButton.interactable = true;

        //have to use child count of player prefab already setup as "this.slot" is not set yet
        if (playerName == "")
            CmdNameChanged("Player" + (LobbyPlayerList._instance.playerListContentTransform.childCount-1));

        //we switch from simple name display to name input
        colorButton.interactable = true;
        nameInput.interactable = true;

        nameInput.onEndEdit.RemoveAllListeners();
        nameInput.onEndEdit.AddListener(OnNameChanged);

        colorButton.onClick.RemoveAllListeners();
        colorButton.onClick.AddListener(OnColorClicked);

        readyButton.onClick.RemoveAllListeners();
        readyButton.onClick.AddListener(OnReadyClicked);

        //when OnClientEnterLobby is called, the loval PlayerController is not yet created, so we need to redo that here to disable
        //the add button if we reach maxLocalPlayer. We pass 0, as it was already counted on OnClientEnterLobby
        if (LobbyManager.s_Singleton != null) LobbyManager.s_Singleton.OnPlayersNumberModified(0);
        */
        
        string playerName = string.Format(
              "{0}"
            , (PlayerSave.GetPlayerName().Length > 0) ? PlayerSave.GetPlayerName() : "Player"
            //, (isServer ? "Server" : "Client")
        );     
            
        CmdSetName(playerName);
        CmdSetImagePath(PlayerSave.GetPlayerAvatar());
        
        if(LobbyPlayerListUI.Instance != null)
        {
            LobbyPlayerListUI.Instance.PlayerListModified();
        }
        
        // Auto ready when setup local.
        //OnReadyClicked();
        
        _timer = readyWaitTime;
        _isEnter = true;
    }

    //This enable/disable the remove button depending on if that is the only local player or not
    public void CheckRemoveButton()
    {
        if (!isLocalPlayer)
            return;

        /*
        int localPlayerCount = 0;
        foreach (PlayerController p in ClientScene.localPlayers)
            localPlayerCount += (p == null || p.playerControllerId == -1) ? 0 : 1;
        */
    }

    public override void OnClientReady(bool readyState)
    {
        Debug.LogFormat("OnClientReady : {0} {1}", _name, readyState);
    
        if (readyState)
        {
            _status = PlayerStatus.Ready;
        }
        else
        {
            _status = PlayerStatus.NotReady;
        }
        
        if(LobbyPlayerListUI.Instance != null)
        {
            LobbyPlayerListUI.Instance.PlayerListModified();
        }
    }
    
    ///===== callback from sync var

    public void OnMyName(string newName)
    {
        Debug.Log("OnMyName : " + newName);
        
        _name = newName;
        
        if(LobbyPlayerListUI.Instance != null)
        {
            LobbyPlayerListUI.Instance.PlayerListModified();
        }
    }

    public void OnMyImage(string imagePath)
    {
        Debug.Log("OnMyImage : " + imagePath);
    
        _imagePath = imagePath;
        
        if(LobbyPlayerListUI.Instance != null)
        {
            LobbyPlayerListUI.Instance.PlayerListModified();
        }
    }
    
    public void OnMyStatus(PlayerStatus status)
    {
        _status = status;
        
        if(LobbyPlayerListUI.Instance != null)
        {
            LobbyPlayerListUI.Instance.PlayerListModified();
        }
    }
    //===== UI Handler

    //Note that those handler use Command function, as we need to change the value on the server not locally
    //so that all client get the new value throught syncvar
    public void OnReadyClicked()
    {
        if(_status == PlayerStatus.NotReady)
        {
            SendReadyToBeginMessage();
        }
        else
        {
            SendNotReadyToBeginMessage();
        }
    }

    /*
    public void OnRemovePlayerClick()
    {
        if (isLocalPlayer)
        {
            RemovePlayer();
        }
       
        else if(isServer)
        {
            KOSLobbyManager.s_Singleton.KickPlayer(connectionToClient);
        }
    }

    /*
    public void ToggleJoinButton(bool enabled)
    {
        if(!isLocalPlayer)
        {
            return;
        }
        
        //readyButton.gameObject.SetActive(enabled);
    }
    */
    
    [ClientRpc]
    public void RpcLoadBattleScene()
    {
        if(LobbyUIManager.Instance != null)
        {
           LobbyUIManager.Instance.LoadScene("NetworkBattleScene");
        }
    }

    [ClientRpc]
    public void RpcUpdateCountdown(int countdown)
    {
        if(LobbyUIManager.Instance != null)
        {
            if(countdown <= 0) 
                LobbyUIManager.Instance.ShowLoading(true);
                
            LobbyUIManager.Instance.UpdateVSTime(countdown);
        }
    }

    [ClientRpc]
    public void RpcUpdateRemoveButton()
    {
        CheckRemoveButton();
    }
    
    [Command]
    public void CmdSetName(string name)
    {
        _name = name;
    }
    
    [Command]
    public void CmdSetImagePath(string imagePath)
    {
        _imagePath = imagePath;
    }
    
    [Command]
    public void CmdShowVS()
    {
        RpcShowVS();
    }
    
    [ClientRpc]
    public void RpcShowVS()
    {
        if(LobbyUIManager.Instance != null)
        {
            LobbyUIManager.Instance.ShowVS();
        }
    }
    
    [Command]
    public void CmdSetReady(bool isReady)
    {
        RpcSetReady(isReady);
    }
    
    [ClientRpc]
    public void RpcSetReady(bool isReady)
    {
        if(KOSLobbyManager.Instance != null)
        {
            KOSLobbyManager.Instance.SetReady(isReady);
        }
    }

    //====== Server Command
    
    //Cleanup thing when get destroy (which happen when client kick or disconnect)
    public void OnDestroy()
    {
        KOSLobbyManager.Instance.RemovePlayer(this);
    }
}
