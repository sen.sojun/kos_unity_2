﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.Networking.Types;

public class LobbyPlayerListUI : MonoBehaviour 
{    
    public static LobbyPlayerListUI Instance = null;
   
    public Text StatusText;    
    public Button BackButton;
    
    public List<LobbyPlayerProfileUI> ProfileUIList;

    void Awake()
    {
        Instance = this;
    }

	// Use this for initialization
	void Start () 
    {
	}

    /*
	// Update is called once per frame
	void Update () 
    {	
	}
    */

    public void OnEnable()
    {
        Instance = this;
    }

    public void OnDestroy()
    {
        Instance = null;
    }

    /*
    public void AddPlayer(KOSLobbyPlayer player)
    {
        if (KOSLobbyManager.Instance.KOSPlayers.Contains(player))
            return;

        KOSLobbyManager.Instance.AddPlayer(player);
        
        PlayerListModified();
    }

    public void RemovePlayer(KOSLobbyPlayer player)
    {

        KOSLobbyManager.Instance.RemovePlayer(player);   
        
        PlayerListModified();
    }
    */
    
    public void PlayerListModified()
    {
        //if(this && gameObject.activeSelf)
        {
            for(int index = 0; index < ProfileUIList.Count; ++index)
            {
                if(KOSLobbyManager.Instance.KOSPlayers != null && KOSLobbyManager.Instance.KOSPlayers.Count > index)
                {
                    // have player
                    SetProfile(index, KOSLobbyManager.Instance.KOSPlayers[index]);
                    
                    if(KOSLobbyManager.Instance.KOSPlayers[index].isLocalPlayer)
                    {
                        SetButton(
                              index
                            , (KOSLobbyManager.Instance.KOSPlayers.Count >= KOSLobbyManager.Instance.minPlayers)
                            , KOSLobbyManager.Instance.KOSPlayers[index].OnReadyClicked
                        );
                    }
                    else
                    {
                        SetButton(index, false);
                    }
                }
                else
                {
                    // no player
                    SetEmpty(index);
                }
            }
        }
    }

    public void ShowUI(string roomStatus, UnityAction onClickBack)
    {   
        SetEmpty(0);
        SetEmpty(1);
    
        StatusText.text = roomStatus;
        
        BackButton.onClick.RemoveAllListeners();
        BackButton.onClick.AddListener(onClickBack);
        BackButton.onClick.AddListener(this.HideUI);
        
        LobbyUIManager.Instance.TopBar.SetUpBar(
              true
            , LocalizationManager.GetText("LOBBY_CREATE_BATTLE_ROOM_HEADER")
            , delegate()
            {
                if(onClickBack != null)
                {
                    onClickBack.Invoke();
                }
                this.HideUI();
            }
        );        
        LobbyUIManager.Instance.TopBar.DisableTopbarButtonButton();
        
        gameObject.SetActive(true);
    }
    
    public void SetProfile(int slotIndex, KOSLobbyPlayer player)
    {
        if(ProfileUIList != null && ProfileUIList.Count > slotIndex)
        {
            ProfileUIList[slotIndex].SetProfile(player);
        }
    }
    
    public void SetStatus(int slotIndex, KOSLobbyPlayer.PlayerStatus status)
    {
        if(ProfileUIList != null && ProfileUIList.Count > slotIndex)
        {
            ProfileUIList[slotIndex].SetStatus(status);
        }
    }
    
    public void SetButton(int slotIndex, bool isShow, UnityAction onClickReady = null)
    {
        if(ProfileUIList != null && ProfileUIList.Count > slotIndex)
        {
            ProfileUIList[slotIndex].SetButton(isShow, onClickReady);
        }
    }
    
    public void SetEmpty(int slotIndex)
    {
        if(ProfileUIList != null && ProfileUIList.Count > slotIndex)
        {
            ProfileUIList[slotIndex].SetEmpty();
        }
    }
    
    public void HideUI()
    {
        LobbyUIManager.Instance.TopBar.EnableTopbarButtonButton();
        gameObject.SetActive(false);
    }
}
