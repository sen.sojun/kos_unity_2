﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.Networking.Types;
using UnityEngine.Networking.Match;

public class LobbyServerListUI : MonoBehaviour 
{
    private int _pageIndex = 0;

    public Button RefreshButton;
    public Button BackButton;
    
    public Text StatusText;
    
    public Text PageText;
    public Button PrevButton;
    public Button NextButton;
    
    public List<ServerRoomUI> RoomSelectUIList;
    
    public int PageIndex { get { return _pageIndex; } }

	// Use this for initialization
	void Start () 
    {	
	}
	
	// Update is called once per frame
	void Update () 
    {	
	}
    
    public void ShowUI()
    {
        _pageIndex = 0;
    
        RequestPage(_pageIndex);
        
        gameObject.SetActive(true);  
    }
    
    public void HideUI()
    {
        gameObject.SetActive(false);
    }
    
    void ClearRoomList()
    {
        if(RoomSelectUIList != null && RoomSelectUIList.Count > 0)
        {
            foreach(ServerRoomUI room in RoomSelectUIList)
            {
                room.gameObject.SetActive(false);
            }
        }
    }
    
    public void UpdateRoomList(List<MatchInfoSnapshot> matchList)
    {
        if(RoomSelectUIList != null && RoomSelectUIList.Count > 0)
        {
            for(int index = 0; index < RoomSelectUIList.Count; ++index)
            {
                if(matchList.Count > index)
                {
                    // have room
                    RoomSelectUIList[index].ShowUI(
                          matchList[index]
                        , KOSLobbyManager.Instance.RequestJoinMatch
                    );
                }
                else
                {
                    // no room
                    RoomSelectUIList[index].HideUI();
                }
            }
        }
        
        if(matchList == null || matchList.Count <= 0)
        {
            ShowStatusText(true, "NO ROOM AVALIBLE");
            
            _pageIndex = 0;
            PageText.text = "";
            PageText.gameObject.SetActive(false);
            PrevButton.gameObject.SetActive(false);
            NextButton.gameObject.SetActive(false);
        }
        else
        {
            ShowStatusText(false, "");
            
            PageText.text = (_pageIndex + 1).ToString();
            PageText.gameObject.SetActive(true);
            if(_pageIndex <= 0)
            {
                PrevButton.gameObject.SetActive(false);
            }
            else
            {
                PrevButton.gameObject.SetActive(true);
            }
            
            if(matchList.Count >= RoomSelectUIList.Count)
            {
                NextButton.gameObject.SetActive(true);
            }
            else
            {
                NextButton.gameObject.SetActive(false);
            }
        }
    }
    
    public void ShowStatusText(bool isShow, string text)
    {
        if(isShow)
        {
            StatusText.text = text;
        }
        
        StatusText.gameObject.SetActive(isShow);
    }
    
    private void RequestPage(int pageIndex)
    {
        _pageIndex = pageIndex;
        
        if(_pageIndex <= 0)
        {
            _pageIndex = 0;
        }
        
        ClearRoomList();
        ShowStatusText(true, "CONNECTING...");
        KOSLobbyManager.Instance.RequestRoomList(_pageIndex); 
        
        PageText.text = "";
        PageText.gameObject.SetActive(false);
        PrevButton.gameObject.SetActive(false);
        NextButton.gameObject.SetActive(false);   
    }
    
    public void OnClickRefresh()
    {
        if(gameObject.activeSelf)
        {
            RequestPage(_pageIndex);
        }
    }   
    
    public void OnClickBack()
    {
        HideUI();
    }
    
    public void OnClickNext()
    {
        RequestPage(_pageIndex + 1);
    }
    
    public void OnClickPrev()
    {        
        RequestPage(_pageIndex - 1);
    }
}
