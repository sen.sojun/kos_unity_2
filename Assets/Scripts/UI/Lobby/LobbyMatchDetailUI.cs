﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LobbyMatchDetailUI : MonoBehaviour 
{
    public static readonly string TimeFormat = "HH:mm";

    public Text TimeText;
    public LobbyMatchPlayerDetailUI Player_L;
    public LobbyMatchPlayerDetailUI Player_R;

    /*
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    */
    
    public void SetData(MatchLogData data)
    {
        System.TimeSpan diff = (System.DateTime.Now - System.DateTime.UtcNow);
        System.DateTime dateTime = data.Time + diff;
    
        TimeText.text = "TIME : " + dateTime.ToString(TimeFormat);
        
        Player_L.SetData(data.ImagePath_L, data.PlayerName_L);
        Player_R.SetData(data.ImagePath_R, data.PlayerName_R);
    }
}
