﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

using UnityEngine.Networking;
using UnityEngine.Networking.Types;
using UnityEngine.Networking.Match;

public class LobbyMatchUI : MonoBehaviour 
{
    private float _timer;

    public LobbyMatchPlayerDetailUI PlayerUI;
    public Text StatusText;
    public LobbyMatchListUI ListUI;
    
    public Button BattleButton;
    public Button CancelButton;
    public Button BackButton;
    
    public UnityEvent OnClickBackEvent;

    /*
	// Use this for initialization
	void Start () {
		
	}
    */
	
	// Update is called once per frame
	void Update () 
    {	
        if(KOSLobbyManager.Instance != null)
        {
            SetStatus(KOSLobbyManager.Instance.Status);
        }
	}
       
    public void ShowUI()
    {     
        Debug.Log("LobbyMatchUI: ShowUI");   
        BattleButton.onClick.RemoveAllListeners();
        CancelButton.onClick.RemoveAllListeners();
        BackButton.onClick.RemoveAllListeners();
        
        BattleButton.onClick.AddListener(this.OnClickStartMatchMaking);
        CancelButton.onClick.AddListener(this.OnClickCancelMatchMaking);
        BackButton.onClick.AddListener(this.OnClickBack);
        
        LobbyUIManager.Instance.TopBar.SetUpBar(
              true
            , LocalizationManager.GetText("LOBBY_ONLINE_PVP_HEADER")
            , this.OnClickBack
        );
                   
        ClearMatchLogList();
        KOSServer.RequestMatchLog(this.SetMatchLogList);
        
        gameObject.SetActive(true);
        BattleButton.gameObject.SetActive(true);
        CancelButton.gameObject.SetActive(false);
        
        SetMyPlayer();
    }
    
    public void HideUI()
    {   
        gameObject.SetActive(false);
    }
    
    public void SetMyPlayer()
    {    
        PlayerUI.SetData(
              PlayerSave.GetPlayerAvatar()
            , PlayerSave.GetPlayerName()
        );
    }
    
    public void ClearMatchLogList()
    {
        ListUI.SetData(null);
    }
    
    public void SetMatchLogList(bool isSuccess, List<MatchLogData> dataList)
    {
        if(isSuccess)
        {
            ListUI.SetData(dataList);
        }
        else
        {
            ClearMatchLogList();
        }
    }
    
    public void OnClickStartMatchMaking()
    {
        //KOSLobbyManager.Instance.StartMatching(); 
        StartCoroutine(OnStartMatching());             
    }
       
    public void OnClickCancelMatchMaking()
    {
        //KOSLobbyManager.Instance.CancelMatching(); 
        StartCoroutine(OnCancelMatching()); 
    }
    
    public void OnClickBack()
    {
        if(KOSLobbyManager.Instance.Status != KOSLobbyManager.MatchingStatus.Ready)
        {
            KOSLobbyManager.Instance.CancelMatching();
            
            //OnClickCancelMatchMaking();
        }
        
        HideUI();
        LobbyUIManager.Instance.TopBar.EnableTopbarButtonButton();
        
        if(OnClickBackEvent != null)
        {
            OnClickBackEvent.Invoke();
        }
    }
    
    private IEnumerator OnStartMatching()
    {
        LobbyUIManager.Instance.TopBar.DisableTopbarButtonButton();
        BattleButton.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.1f);        
        KOSLobbyManager.Instance.StartMatching(); 
        
        yield return new WaitForSeconds(0.9f);    
        CancelButton.gameObject.SetActive(true);     
    }
    
    private IEnumerator OnCancelMatching()
    {    
        CancelButton.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.1f);        
        KOSLobbyManager.Instance.CancelMatching(); 
        
        yield return new WaitForSeconds(0.9f);            
        BattleButton.gameObject.SetActive(true);
        LobbyUIManager.Instance.TopBar.EnableTopbarButtonButton();
    }
    
    private void SetStatus(KOSLobbyManager.MatchingStatus status)
    {        
        string text = "";
        switch(status)
        {
            case KOSLobbyManager.MatchingStatus.Ready:
            {
                text = LocalizationManager.GetText("LOBBY_MATCH_STATUS_READY");
            }
            break;
            
            case KOSLobbyManager.MatchingStatus.Finding:
            {
                text = LocalizationManager.GetText("LOBBY_MATCH_STATUS_FIND");
            }
            break;
            
            case KOSLobbyManager.MatchingStatus.Joining:
            {
                text = LocalizationManager.GetText("LOBBY_MATCH_STATUS_JOIN");
            }
            break;
            
            case KOSLobbyManager.MatchingStatus.Creating:
            {
                text = LocalizationManager.GetText("LOBBY_MATCH_STATUS_CREATE");
            }
            break;
            
            case KOSLobbyManager.MatchingStatus.Waiting:
            {
                text = LocalizationManager.GetText("LOBBY_MATCH_STATUS_WAIT");
            }
            break;
            
            case KOSLobbyManager.MatchingStatus.Matched:
            {
                text = LocalizationManager.GetText("LOBBY_MATCH_STATUS_MATCHED");
            }
            break;
            
            case KOSLobbyManager.MatchingStatus.Canceling:
            {
                text = LocalizationManager.GetText("LOBBY_MATCH_STATUS_CANCEL");
            }
            break;
            
            default:
            {
                text = "";
            }
            break;
        }
        
        StatusText.text = text;
    }
}
