﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class MainLobbyUI : MonoBehaviour 
{
    private static string _rootImagePath = "Images/Avatars/Full/";

    public Button PlayOnlineButton;
    public Button PlayLocalButton;
    public Button MainMenuButton;
    
    public Button AvatarMenuButton;
    public Button AvatarTopButton;
    
	// Use this for initialization
	void Start () 
    {	
	}
	
    /*
	// Update is called once per frame
	void Update () 
    {	
	}
    */
    
    public void ShowUI()
    {
        string imagePath = _rootImagePath + PlayerSave.GetPlayerAvatar();
        Sprite sprite = Resources.Load<Sprite>(imagePath);
        
        if(AvatarMenuButton != null) AvatarMenuButton.image.sprite = sprite;
        if(AvatarTopButton != null) AvatarTopButton.image.sprite = sprite;
    
        gameObject.SetActive(true);
    }
    
    public void HideUI()
    {        
        gameObject.SetActive(false);
    }
    
    public void OnClickPlayOnline()
    {
        //LobbyUIManager.Instance.ShowOnlineLobby();
        LobbyUIManager.Instance.ShowOnlineMatch();
    }
    
    public void OnClickPlayLocal()
    {
        LobbyUIManager.Instance.ShowLocalLobby();
    }
    
    public void OnClickMenu()
    {
        LobbyUIManager.Instance.BackToMenu();
    }
}
