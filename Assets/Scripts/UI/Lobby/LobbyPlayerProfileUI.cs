﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;

using UnityEngine.Events;

public class LobbyPlayerProfileUI : MonoBehaviour 
{    
    private static string _rootImagePath = "Images/Avatars/Small/";

    private KOSLobbyPlayer _player;
    private KOSLobbyPlayer.PlayerStatus _status;
    private UnityIntEvent _onClickReady;
    private Coroutine _loadImageCoroutine = null;

    public int Index;
    public Image ProfileImage;
    public Text PlayerNameText;
    public Text StatusText;
    public Button ReadyButton;

	// Use this for initialization
	void Start () 
    {	
	}
	
    /*
	// Update is called once per frame
	void Update () 
    {	
	}
    */
    
    public void SetEmpty()
    {
        _player = null;
        
        ProfileImage.gameObject.SetActive(false);
        PlayerNameText.text = "";
        
        SetStatus(KOSLobbyPlayer.PlayerStatus.Waiting);
        SetButton(false);
    }
    
    public void SetProfile(KOSLobbyPlayer player)
    {
        _player = player;
        
        ProfileImage.gameObject.SetActive(false);
        
        if(player.ImagePath != null && player.ImagePath.Length > 0)
        {
            string imagePath = _rootImagePath + player.ImagePath;
            //Debug.Log("LobbyPlayerProfileUI/LoadImage: " + imagePath);
            
            /*
            if(_loadImageCoroutine != null)
            {
                StopCoroutine(_loadImageCoroutine);
            }
            _loadImageCoroutine = StartCoroutine(LoadImageAsync(imagePath));
            */
            
            LoadImage(imagePath);
        }

        PlayerNameText.text = _player.PlayerName;       
        SetStatus(_player.Status);
        
        /*
        if(_player.isLocalPlayer)
        {
            SetButton(true, _player.OnReadyClicked);
        }
        else
        {
            SetButton(false);
        }
        */
    }
    
    public void SetButton(bool isShow, UnityAction onClickReady = null)
    {
        ReadyButton.onClick.RemoveAllListeners();
        
        if(isShow)
        {   
            if(onClickReady != null)
            {                
                ReadyButton.onClick.AddListener(onClickReady);
            }
        }
       
        ReadyButton.gameObject.SetActive(isShow);
    }
    
    private void SetButtonText(string buttonText)
    {
        Transform text = ReadyButton.transform.Find("Text");
        text.GetComponent<Text>().text = buttonText;
    }
    
    public void SetStatus(KOSLobbyPlayer.PlayerStatus status)
    {        
        _status = status;
        
        UpdateStatus();
    }
    
    private void UpdateStatus()
    {                
        switch(_status)
        {
            case KOSLobbyPlayer.PlayerStatus.NotReady:
            {
                StatusText.text = LocalizationManager.GetText("LOBBY_PLAYER_STATUS_NOT_READY");
                StatusText.color = Color.red;
                
                SetButtonText(LocalizationManager.GetText("LOBBY_READY_BUTTON"));
            }
            break;
        
            case KOSLobbyPlayer.PlayerStatus.Ready:
            {
                StatusText.text = LocalizationManager.GetText("LOBBY_PLAYER_STATUS_READY");
                StatusText.color = Color.green;
                
                SetButtonText(LocalizationManager.GetText("LOBBY_CANCEL_BUTTON"));
            }
            break;
            
            case KOSLobbyPlayer.PlayerStatus.Waiting:
            default:
            {
                StatusText.text = LocalizationManager.GetText("LOBBY_PLAYER_STATUS_WAIT");
                StatusText.color = Color.yellow;
            }
            break;
        }
    }
    
    private IEnumerator LoadImageAsync(string imagePath)
    {
        ResourceRequest request = Resources.LoadAsync<Sprite>(imagePath);
        yield return request;
        
        Sprite sprite = request.asset as Sprite;
        if(sprite != null)
        {
            ProfileImage.sprite = sprite;
            ProfileImage.gameObject.SetActive(true);
        }
        else
        {
            ProfileImage.sprite = null;
            ProfileImage.gameObject.SetActive(false);
            Debug.Log("LobbyPlayerProfileUI/LoadImageAsync: Failed to load image. " + imagePath);
        }  
        _loadImageCoroutine = null;     
    }
    
    private bool LoadImage(string imagePath)
    {
        Sprite sprite = Resources.Load<Sprite>(imagePath);
        if(sprite != null)
        {
            ProfileImage.sprite = sprite;
            ProfileImage.gameObject.SetActive(true);
            return true;
        }
        else
        {
            ProfileImage.sprite = null;
            ProfileImage.gameObject.SetActive(false);
            Debug.Log("LobbyPlayerProfileUI/LoadImage: Failed to load image. " + imagePath);
            return false;
        }  
    }
}
