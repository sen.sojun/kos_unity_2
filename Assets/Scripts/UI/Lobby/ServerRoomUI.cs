﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.Networking.Types;

public class ServerRoomUI : MonoBehaviour 
{
    private MatchInfoSnapshot _match;
    private UnityNetworkIDEvent _onClickJoin;

    public Text RoomNameText;
    public Button JoinButton;

    /*
	// Use this for initialization
	void Start () 
    {	
	}
	
	// Update is called once per frame
	void Update () 
    {	
	}
    */
    
    public void ShowUI(MatchInfoSnapshot match, UnityAction<NetworkID> onClickJoin)
    {
        _match = match;
        RoomNameText.text = string.Format(
              "{0} {1}/{2}"
            , _match.name
            , match.currentSize.ToString()
            , match.maxSize.ToString()
        );
        
        if(_onClickJoin == null) _onClickJoin = new UnityNetworkIDEvent();        
        _onClickJoin.RemoveAllListeners();
        _onClickJoin.AddListener(onClickJoin);
        
        gameObject.SetActive(true);
    }
    
    public void HideUI()
    {
        gameObject.SetActive(false);
    }
    
    public void OnClickJoin()
    {
        if(_onClickJoin != null)
        {
            _onClickJoin.Invoke(_match.networkId);
        }
    }
}
