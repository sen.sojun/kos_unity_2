﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.Networking.Types;

public class OnlineLobbyUI : MonoBehaviour 
{
    public Button CreateMatchButton;
    public Button RoomListButton;
    public Button BackButton;

	// Use this for initialization
	void Start () 
    {	
	}
	
	// Update is called once per frame
    /*
	void Update () 
    {	
	}
    */
    
    public void ShowUI()
    {        
        gameObject.SetActive(true);
    }
    
    public void HideUI()
    {   
        gameObject.SetActive(false);
    }
    
    public void OnClickCreateMatch()
    {
        string matchName = string.Format(
              "{0}#{1}"
            , (PlayerSave.GetPlayerName().Length > 0) ? PlayerSave.GetPlayerName() : "Player"
            , Random.Range(0, 100000).ToString("00000")
        );
        KOSLobbyManager.Instance.RequestCreateMatch(matchName);
    }
    
    public void OnCLickRoomList()
    {
        LobbyUIManager.Instance.ShowServerList();
        //KOSLobbyManager.Instance.RequestRoomList(0);
    }
    
    public void OnClickBack()
    {
        KOSLobbyManager.Instance.StopMatchMaker();
        HideUI();
        
        LobbyUIManager.Instance.ShowMainLobby();
    }
}
