﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;

public class LobbyMatchPlayerDetailUI : MonoBehaviour 
{
    private static string _rootImagePath = "Images/Avatars/Banner/";

    public Image PlayerImage;
    public Text PlayerName;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    
    public void SetData(string imagePath, string playerName)
    {    
        StartCoroutine(LoadImage(_rootImagePath + imagePath));
        
        PlayerName.text = playerName;
    }
    
    private IEnumerator LoadImage(string imagePath)
    {
        ResourceRequest request = Resources.LoadAsync<Sprite>(imagePath);
        yield return request;
        
        Sprite sprite = request.asset as Sprite;
        if(sprite != null)
        {
            PlayerImage.sprite = sprite;
        }
        else
        {
            PlayerImage.sprite = null;
            Debug.LogError("LobbyMatchPlayerDetailUI/LoadImage: Failed to load image. " + imagePath);
        }       
    }
}
