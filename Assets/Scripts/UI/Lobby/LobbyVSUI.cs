﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;

public class LobbyVSUI : MonoBehaviour 
{
    private static string _rootImagePath = "Images/Avatars/Small/";

    public Image Image_L;
    public Text NameText_L;
    
    public Image Image_R;
    public Text NameText_R;
    
    public Text CountdownText;
  
    /*
	// Use this for initialization
	void Start () 
    {	
	}
    */
	
    /*
	// Update is called once per frame
	void Update () 
    {	
	}
    */
    
    public void ShowUI(KOSLobbyPlayer player1, KOSLobbyPlayer player2)
    {
        LoadImage(Image_L, _rootImagePath + player1.ImagePath);
        LoadImage(Image_R, _rootImagePath + player2.ImagePath);
        NameText_L.text = player1.PlayerName;        
        NameText_R.text = player2.PlayerName;
    
        CountdownText.text = "";    
        gameObject.SetActive(true);
    }
    
    public void ShowUI(string imagePath_L, string imagePath_R, string name_L, string name_R)
    {
        LoadImage(Image_L, _rootImagePath + imagePath_L);
        LoadImage(Image_R, _rootImagePath + imagePath_R);
        NameText_L.text = name_L;        
        NameText_R.text = name_R;
    
        CountdownText.text = "";    
        gameObject.SetActive(true);
    }
    
    public void SetTime(int second)
    {
        CountdownText.text = second.ToString();
    }
    
    public void HideUI()
    {
        gameObject.SetActive(false);
    }
    
    private void LoadImage(Image image, string imagePath)
    {
        Sprite sprite = Resources.Load<Sprite>(imagePath);
       
        if(sprite != null)
        {
            image.sprite = sprite;
        }
        else
        {
            Debug.Log("LobbyVSUI/LoadImage: Failed to load image. " + imagePath);
        }       
    }
    
    private void LoadImageAsync(Image image, string imagePath)
    {
        StartCoroutine(OnLoadImageAsync(image, imagePath));      
    }
    
    private IEnumerator OnLoadImageAsync(Image image, string imagePath)
    {
        ResourceRequest request = Resources.LoadAsync<Sprite>(imagePath);
        yield return request;
        
        Sprite sprite = request.asset as Sprite;
        if(sprite != null)
        {
            image.sprite = sprite;
        }
        else
        {
            Debug.Log("LobbyVSUI/LoadImage: Failed to load image. " + imagePath);
        }       
    }   
}
