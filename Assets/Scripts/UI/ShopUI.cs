﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Firebase.Auth;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using UnityEngine.UI;
using UnityEngine.Purchasing;

public class ShopUI : MonoBehaviour 
{
	public enum ShopMode
	{
		SilverMode,
		GoldMode
	}

    enum goldQty
    {
        GoldType1 = 15,
        GoldType2 = 30,
        GoldType3 = 105,
        GoldType4 = 375,
        GoldType5 = 750
    }

    const int GoldType1 = 15;
    const int GoldType2 = 30;
    const int GoldType3 = 105;
    const int GoldType4 = 375;
    const int GoldType5 = 750;
		
	#region Private Properties
	private Dictionary<string, ShopPackData> _shopList;
	private ShopMode _mode;
	private int _goldIndex;
	#endregion

	#region Public Properties
	public SilverCoinUI SilverCoinUI;
	public GoldCoinUI GoldCoinUI;


	public ShopConfirmationUI ConfirmPurchaseUI;
	public UILabel LabelConfirmation;
	public GameObject NotEnoughUI;
	public ShopCardResultUI ShopCardResultUI;
	public ShopPreviewCardUI ShopPreviewCardUI;
    public GameObject MainUI;
    public GameObject ShopGUI;
	public GameObject OpenGoldUI;
	public GameObject ShopPackUI;
	public GameObject ShopStoreFrontUI;
	public GameObject ShopInfoUI;

	public AdsMessageUI ResultUI;

	public int NormalPrice =  150; // 150 silver
	public int GoldPrice = 15; //15 gold
	int amountOfGold = 0;
    int completeGoldAdd = 0;
    string logText = "";

    // Use this for initialization
    static Dictionary<string, int> card = new Dictionary<string, int>();
    StashFirebaseData stashdb;
    private string userID;

	#endregion

	// Use this for initialization
	void Start () 
	{
		//_mode = ShopMode.SilverMode;

		_shopList = new Dictionary<string, ShopPackData> ();
		_shopList.Add ("SP0001", new ShopPackData("SP0001"));

        userID = PlayerSave.GetPlayerUid();
        stashdb = new StashFirebaseData(userID, card);

        Debug.Log("Start Shop uid = " + userID);

	}
	
	// Update is called once per frame
	void Update () 
	{
	}
		
	/*
	public void OnShowConfirmPurchaseNormalBooster()
	{
		ConfirmPurshaseUI.SetActive (true);
	}

	public void OnHideConfirmPurshaeNormalBooster()
	{
		ConfirmPurshaseUI.SetActive (false);
	}

	public void onShowNotEnoughUI()
	{
		NotEnoughUI.SetActive (true);

	}

	public void onHideNotEnoughUI()
	{
		NotEnoughUI.SetActive (false);
	}
	*/

	//public void onClickBuyGold (int index)
	//{
	//	Debug.Log (index);
	//	_goldIndex = index;

	//	string phaseText = "";
	//	string headText = "";

	//	string amountOfDollarText = "";
	//	string amountOfGoldText = "";


	//	switch (_goldIndex) 
	//	{
	//	case 0:
	//		amountOfDollarText = "$1.99";
	//		amountOfGoldText = "15";
	//		amountOfGold = 15;
	//		break;
	//	case 1:
	//		amountOfDollarText = "$4.99";
	//		amountOfGoldText = "30";
	//		amountOfGold = 30;
	//		break;
	//	case 2:
	//		amountOfDollarText = "$9.99";
	//		amountOfGoldText = "105";
	//		amountOfGold = 105;
	//		break;
	//	case 3:
	//		amountOfDollarText = "$24.99";
	//		amountOfGoldText = "375";
	//		amountOfGold = 375;
	//		break;
	//	case 4:
	//		amountOfDollarText = "$39.99";
	//		amountOfGoldText = "750";
	//		amountOfGold = 750;
	//		break;
	//	}

	//	Debug.Log (index);

	//	phaseText = string.Format (Localization.Get ("SHOP_GOLD_BUY_CONFIRMATION")
	//		, amountOfGoldText, amountOfDollarText 
	//    );
	
	//	headText = string.Format (Localization.Get ("SHOP_GOLD_BUY_CONGRATULATION_HEAD"));

	//	Debug.Log ("descr = " + amountOfDollarText + " " + amountOfGoldText + " pahse = " + phaseText + " head = " + headText);
	//	ConfirmPurchaseUI.ShowUI (headText,phaseText,this.ConfirmAddGold, ConfirmPurchaseUI.HideUI,true,true,false);
	//	//ConfirmPurchaseUI.ShowUI (headText,descText, this.ConfirmBuyStarterSet_5, ConfirmPurchaseUI.HideUI,true,true,false);
	//	//ConfirmPurchaseUI.ShowUI (headText,phaseText, ConfirmPurchaseUI.HideUI, ConfirmPurchaseUI.HideUI,true,true,false);


	//}

	public void ShowUI()
	{
		gameObject.SetActive (true);
	}

	public void HideUI()
	{
		gameObject.SetActive (false);
	}
		
	public void ShowGoldPriceListUI()
	{
		//ShopStoreFrontUI.SetActive (false);
        if (MainUI.activeSelf)
        {
          MainUI.SetActive(false);  
          ShopGUI.SetActive(true);
        }
		OpenGoldUI.SetActive (true);
	}

	public void HideGoldPriceListUI()
	{
		Debug.Log ("close gold price");
		OpenGoldUI.SetActive (false);
		ShopStoreFrontUI.SetActive (true);
	}

	public void ShowShopResultUI()
	{
		SetShowCardResultUI(false);
	}

	public void HideShopResultUI()
	{
		ShopStoreFrontUI.SetActive (true);
		SetShowCardResultUI(false);
	}

	public void ShowShopPackUI()
	{
		ShopStoreFrontUI.SetActive (false);
		ShopPackUI.SetActive (true);
	}

	public void CloseShopPackUI()
	{
		ShopPackUI.SetActive (false);
		ShopStoreFrontUI.SetActive (true);

	}

	public void ShowPackInfoUI()
	{
		ShopInfoUI.SetActive (true);
	}

	public void HidePackInfoUI()
	{
		ShopInfoUI.SetActive (false);
	}


	private void SetShowCardResultUI(bool isShow)
	{
		if (isShow)
		{
			ShopCardResultUI.ShowUI ("5");
		} 
		else
		{
			ShopCardResultUI.HideUI ();
		}
	}
		
	public void ShowNotEnoughUI()
	{
		SetShowNotEnoughUI (true);
	}

	public void HideNotEnoughUI()
	{
		SetShowNotEnoughUI (false);
	}

	private void SetShowNotEnoughUI(bool isShow)
	{
		NotEnoughUI.SetActive(isShow);
		if (isShow)
		{
			BattleUIManager.RequestBringForward (NotEnoughUI);
		}
	}

	public void BuyStarterSet_5()
	{
		int currentSilverCoin = PlayerSave.GetPlayerSilverCoin();

		if (currentSilverCoin >= NormalPrice)
		{
			string descText;
			string headText;
			descText = string.Format (Localization.Get ("SHOP_CONFIRMATION_DESCRIPTION"));
			headText = string.Format (Localization.Get ("SHOP_CONFIRMATION_HEADER"));	
		 
			ConfirmPurchaseUI.ShowUI (headText,descText, this.ConfirmBuyStarterSet_5, ConfirmPurchaseUI.HideUI,true,true,false);
		} 
		else
		{
			ConfirmPurchaseUI.HideUI ();
			SetShowNotEnoughUI(true);
		}

		//SoundManager.PlayEFXSound (OpenPackSFX);
	}

	public void ConfirmBuyStarterSet_5()
	{
		BuyStarterSet(NormalPrice);
	}

	private void BuyStarterSet(int iSilverToPaid)
	{
		Debug.Log ("Buy: " + iSilverToPaid.ToString());

		int currentSilverCoin = PlayerSave.GetPlayerSilverCoin();

		if (currentSilverCoin >= iSilverToPaid) 
		{
			currentSilverCoin -= iSilverToPaid;

			// Save coin.
			PlayerSave.SavePlayerSilverCoin(currentSilverCoin);
			SilverCoinUI.UpdateSilverCoin ();

			List<string> resultCardList; 
			RandomNormal("SP0001", out resultCardList);
			//DebugPrintText (resultCardList);
		} 
		else 
		{
			//Debug.Log("Not enough silver coin.");
			ConfirmPurchaseUI.HideUI();
			SetShowNotEnoughUI(true);
		}
	}

    private void SaveCardToDB(string cardID)
    {
        string text = "";
        bool foundCard = false;
        int currQty = 1;
        Debug.Log("on save stash cashed");
        try
        {
            Debug.Log("on stash cashed");
            Debug.Log("on stash cashed successfully");

            stashdb = KOSServer.Instance.StashData;

            Debug.Log("stashDB");


            Debug.Log("uid =  " + stashdb.UID);

            foundCard = stashdb.Card.ContainsKey(cardID);


            if (foundCard)
            {
                currQty = stashdb.Card[cardID];
                ++currQty;
            }
            else
            {
                stashdb.Card.Add(cardID, 1);
            }

            KOSServer.UpdateStashData(stashdb, cardID, currQty);
        }

        catch (SystemException e)
        {
            Debug.Log("cash stash erro " + e.Message);
        }
    }

	private void RandomNormal(string shopPackID, out List<string> resultList)
	{
		ShopPackData shop = null;
		bool isFound = _shopList.TryGetValue (shopPackID, out shop);
		if (isFound) 
		{
			ConfirmPurchaseUI.HideUI();
			SetShowCardResultUI (true);

			shop.RandomNormal(out resultList);
			int index = 0;
			foreach (string id in resultList) 
			{
				PlayerCardData card = new PlayerCardData (new CardData(id));

				// add new card to stash
				bool isSuccess = AddCardToStash(card.PlayerCardID);
				if(isSuccess)
				{
                    SaveCardToDB(card.PlayerCardID);
					bool isNew = false;
					if (PlayerSave.SavePlayerCardCollection(card.PlayerCardID)) 
					{
						// New card
						isNew = true;
					} 
						
					bool isGlow = false;
					if ( (card.SymbolData == CardSymbolData.CardSymbol.Rare) ||
						(card.SymbolData == CardSymbolData.CardSymbol.SuperRare) ||
						(card.SymbolData == CardSymbolData.CardSymbol.Forbidden)) 
					{
						isGlow = true;
					} 

					ShopCardResultUI.ShowCard (index, new PlayerCardData (card), isGlow, isNew, this.OnClickedPreview);
				}
				++index;
			}

			return;
		}

		resultList = null;
	}

	private void RandomPremium(string shopPackID, out List<string> resultList)
	{
		ShopPackData shop = null;
		bool isFound = _shopList.TryGetValue (shopPackID, out shop);
		if (isFound) 
		{
			ConfirmPurchaseUI.HideUI();
			SetShowCardResultUI (true);

			shop.RandomPremium(out resultList);
			int index = 0;
			foreach (string id in resultList) 
			{
				PlayerCardData card = new PlayerCardData (new CardData(id));

				// add new card to stash
				bool isSuccess = AddCardToStash(card.PlayerCardID);
				if(isSuccess)
				{
					bool isNew = false;
					if (PlayerSave.SavePlayerCardCollection(card.PlayerCardID)) 
					{
						// New card
						isNew = true;
					} 

					bool isGlow = false;
					if ( (card.SymbolData == CardSymbolData.CardSymbol.Rare) ||
						(card.SymbolData == CardSymbolData.CardSymbol.SuperRare) ||
						(card.SymbolData == CardSymbolData.CardSymbol.Forbidden)) 
					{
						isGlow = true;
					} 

					ShopCardResultUI.ShowCard (index, new PlayerCardData (card), isGlow, isNew, this.OnClickedPreview);
				}
				++index;
			}

			return;
		}

		resultList = null;
	}
		
	private bool AddCardToStash(string playerCardID)
	{
		List<PlayerCardData> stash;
		bool isSuccess = PlayerSave.GetPlayerCardStash(out stash);
		if(isSuccess)
		{
			stash.Add(new PlayerCardData(playerCardID));
			PlayerSave.SavePlayerCardStash (stash);

			return true;
		}
		else
		{
			stash = new List<PlayerCardData>();
			stash.Add(new PlayerCardData(playerCardID));
			PlayerSave.SavePlayerCardStash (stash);

			return true;
		}

		return false;
	}

	void OnClickedPreview(FullCardUI fullCardUI)
	{
		Debug.Log ("Name:" + fullCardUI.PlayerCardData.Name_EN);

		ShopPreviewCardUI.ShowUI (fullCardUI.PlayerCardData);
	}
		
	private void DebugPrintText(List<string> cardIDList)
	{
		string resultText = "";
		foreach(string id in cardIDList)
		{
			CardData data = new CardData (id);
			resultText += data.SymbolData.SymbolName_EN + " " + id + ":" + data.Name_EN + "\n";
		}

		Debug.Log (resultText);
	}

	public void BuyPremiumSet_5()
	{
		int currentGoldCoin = PlayerSave.GetPlayerGoldCoin();

		if (currentGoldCoin >= GoldPrice)
		{
			string descText;
			string headText;
			descText = string.Format (Localization.Get ("SHOP_PREMIUM_CONFIRMATION_DESCRIPTION"));
			headText = string.Format (Localization.Get ("SHOP_CONFIRMATION_HEADER"));	

			ConfirmPurchaseUI.ShowUI (headText,descText, this.ConfirmBuyPremiumSet_5, ConfirmPurchaseUI.HideUI,true,true,false);
		} 
		else
		{
			ConfirmPurchaseUI.HideUI ();
			SetShowNotEnoughUI(true);
		}

		//SoundManager.PlayEFXSound (OpenPackSFX);
	}

	public void ConfirmBuyPremiumSet_5()
	{
		BuyPremiumSet(GoldPrice);
	}

	private void BuyPremiumSet(int iGoldToPaid)
	{
		Debug.Log ("Buy: " + iGoldToPaid.ToString());

		int currentGoldCoin = PlayerSave.GetPlayerGoldCoin();

		if (currentGoldCoin >= iGoldToPaid) 
		{
			currentGoldCoin -= iGoldToPaid;

			// Save coin.
			PlayerSave.SavePlayerGoldCoin(currentGoldCoin);

			GoldCoinUI.UpdateGoldCoin ();

			List<string> resultCardList; 
			//RandomNormal("SP0001", out resultCardList);
			RandomPremium("SP0001", out resultCardList);
			//DebugPrintText (resultCardList);
		} 
		else 
		{
			//Debug.Log("Not enough silver coin.");
			ConfirmPurchaseUI.HideUI();
			SetShowNotEnoughUI(true);
		}
	}

	//public void ConfirmAddGold()
	//{
	//	AddGoldCoin (amountOfGold);
	//}

    //public void ConfirmAddProduct(Product p)
    //{
    //}
    public void ConfirmBuyGold(Product p)
    {
        int icurGold;
        Debug.Log("ConfirmBuyGold!!");

        if (p != null)
        {
            icurGold = PlayerSave.GetPlayerGoldCoin();
            Debug.Log("Initial gold = " + icurGold);

            switch (p.definition.id)
            {
                case "com.lunarstudio.koscard.Gold15":
                    Debug.Log("You Got Money! " + GoldType1);
                    icurGold += GoldType1;
                    PlayerSave.SavePlayerGoldCoin(icurGold);
                    break;
                case "com.lunarstudio.koscard.Gold30":
                    Debug.Log("You Got Money! " + GoldType2);
                    icurGold += GoldType2;
                    PlayerSave.SavePlayerGoldCoin(icurGold);
                    break;
                case "com.lunarstudio.koscard.Gold105":
                    Debug.Log("You Got Money! " + GoldType3);
                    icurGold += GoldType3;
                    PlayerSave.SavePlayerGoldCoin(icurGold);
                    break;
                case "com.lunarstudio.koscard.Gold375":
                    Debug.Log("You Got Money! " + GoldType4);
                    icurGold += GoldType4;
                    PlayerSave.SavePlayerGoldCoin(icurGold);
                    break;
                case "com.lunarstudio.koscard.Gold750":
                    Debug.Log("You Got Money! " + GoldType5);
                    icurGold += GoldType5;
                    PlayerSave.SavePlayerGoldCoin(icurGold);
                    break;
                default:
                    Debug.Log(
                    string.Format("Unrecognized productId \"{0}\"", p.definition.id)
                        );
                    break;
            }
            icurGold = PlayerSave.GetPlayerGoldCoin();
            Debug.Log("New gold = " + icurGold);
        }
        else
        {
            Debug.Log("no product found"); 
        }
    
    }

    public void CompleteAddGold(int amount)
    {
        int icurGold;
        icurGold = PlayerSave.GetPlayerGoldCoin();
        Debug.Log("Initial gold = " + icurGold);
        icurGold += amount;
        PlayerSave.SavePlayerGoldCoin(icurGold);
        //GoldCoinUI.UpdateGoldCoin();
            logText = string.Format(
                Localization.Get("SHOP_GOLD_BUY_CONGRATULATION")
                , amount
            );

        icurGold = PlayerSave.GetPlayerGoldCoin();
        Debug.Log("New gold = " + icurGold);
        //ShowResultMsg(logText);  
    }

    public void FailedToAddGold(int amount)
    {

            logText = "Error purchasing.."; 
            //string.Format(
                //Localization.Get("SHOP_GOLD_BUY_CONGRATULATION")
                //, addGoldCoin
            //);
        ShowResultMsg(logText); 
    }

	public void AddGoldCoin(int addGoldCoin)
	{
        bool bisSuccess = false;

        completeGoldAdd = addGoldCoin;
        Debug.Log("Update gold coin here ...." + addGoldCoin);
        iAPManager.Instance.InitializePurchasing();
        iAPManager.Instance.BuyGoldAll(addGoldCoin);

        //IAPListener.Instance.onPurchaseComplete.RemoveAllListeners();
        //IAPListener.Instance.onPurchaseFailed.RemoveAllListeners();

        //IAPListener.Instance.onPurchaseComplete.AddListener(this.OnPurchasingComplete);
        //IAPListener.Instance.onPurchaseFailed.AddListener(this.OnPurchasingFailed);

		//int goldCoin = PlayerSave.GetPlayerGoldCoin ();
		//goldCoin += addGoldCoin;

		//// save add coin.
		//PlayerSave.SavePlayerGoldCoin(goldCoin);
        //if (bisSuccess)
        //{
        //    GoldCoinUI.UpdateGoldCoin();

        //    logText = string.Format(
        //        Localization.Get("SHOP_GOLD_BUY_CONGRATULATION")
        //        , addGoldCoin
        //    );
             
        //}
        //else
        //{
        //    logText = "Error purchasing.."; 
        //    //string.Format(
        //        //Localization.Get("SHOP_GOLD_BUY_CONGRATULATION")
        //        //, addGoldCoin
        //    //);

        //}
        //ShowResultMsg(logText);  

	}


	public void ShowResultMsg(string text)
	{
		ResultUI.ShowUI (text);
	}

	public void HideResultMsg()
	{
		ResultUI.HideUI ();
	}

    void OnPurchasingComplete(Product p)
    {
        GoldCoinUI.UpdateGoldCoin();

        logText = string.Format(
            Localization.Get("SHOP_GOLD_BUY_CONGRATULATION")
            , completeGoldAdd
        );
        ShowResultMsg(logText);  
        Debug.Log("ShopUI: OnPurchasingComplete " + p.ToString());
    }

    void OnPurchasingFailed(Product p, PurchaseFailureReason error)
    {
        logText = "Error purchasing.."; 
        ShowResultMsg(logText);  
        Debug.Log("ShopUI: OnPurchasingFailed " + error.ToString());   
    }
}


