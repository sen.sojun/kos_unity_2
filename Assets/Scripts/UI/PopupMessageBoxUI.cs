﻿using UnityEngine;
using System.Collections;

public class PopupMessageBoxUI : MonoBehaviour 
{
    public UILabel MessageLabel;

    public UIButton Button;
    public UILabel ButtonLabel;

	// Use this for initialization
	void Start () 
    {
	}
	
    /*
	// Update is called once per frame
	void Update () {
	
	}
    */

	public void ShowUI(string message, string buttonText = "OK", bool isKey = true)
    {
		if(isKey)
		{
			MessageLabel.GetComponent<UILocalize>().key = message;
			ButtonLabel.GetComponent<UILocalize>().key = buttonText;
		}
		else
		{
			MessageLabel.GetComponent<UILocalize>().key = message;
			ButtonLabel.GetComponent<UILocalize>().key = buttonText;

			MessageLabel.GetComponent<UILocalize>().value = message;
			ButtonLabel.GetComponent<UILocalize>().value = buttonText;
		}

        gameObject.SetActive(true);
		BattleUIManager.RequestBringForward(gameObject);
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }
}
