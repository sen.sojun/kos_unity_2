﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuUI : MonoBehaviour 
{
    public AudioClip BGMClip;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ShowUI()
    {
     
        TopBarManager.Instance.SetUpBar(
           false
            ,Localization.Get("MAINMENU_TITLE") // "MAIN MENU" 
         ,null
        //   , MenuManagerV2.Instance.ShowPlayerDeckCollectionUI
       );
        // TopBarManager.Instance.SetBackButton(false);

        //if (!SoundManager.IsPlayBGM())
        //{
        //    SoundManager.PlayBGM(BGMClip);
        //}

        if (!SoundManager.IsPlayBGM(BGMClip))
        {
            SoundManager.PlayBGM(BGMClip);
        }

        gameObject.SetActive(true);
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }
}
