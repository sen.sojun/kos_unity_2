﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class BattleUI : MonoBehaviour 
{
    public Button SinglePlayerButton;
    public Button   VSPlayerButton;
    public Button BattleArenaButton;
    public PopupBoxUINew PopUpBoxUiNew;
    public bool FirstFlow = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ShowUI(UnityAction onClickSinglePlayer, UnityAction onClickVSPlayer, UnityAction onClickBattleArena)
    {
        TopBarManager.Instance.SetUpBar(
              true
            , Localization.Get("MAINMENU_BATTLE_TITLE") //"BATTLE MODE"
            , MenuManagerV2.Instance.ShowMainMenu
        );
        
        SinglePlayerButton.onClick.RemoveAllListeners();
        VSPlayerButton.onClick.RemoveAllListeners();
        BattleArenaButton.onClick.RemoveAllListeners();
        
        if(onClickSinglePlayer != null) SinglePlayerButton.onClick.AddListener(onClickSinglePlayer);
        if(onClickVSPlayer != null)     VSPlayerButton.onClick.AddListener(onClickVSPlayer);
        if(onClickBattleArena != null)  BattleArenaButton.onClick.AddListener(onClickBattleArena);

        gameObject.SetActive(true);
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }
}
