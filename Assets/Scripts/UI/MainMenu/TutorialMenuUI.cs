﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TutorialMenuUI : MonoBehaviour 
{
	public TutorialDetailUI TutorialDetailUI;

	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
	}

	public void ShowUI()
	{
		gameObject.SetActive(true);
	}

	public void HideUI(bool isHideDetail = true)
	{
		gameObject.SetActive(false);
		if(isHideDetail) TutorialDetailUI.HideUI();
	}
		
	public void ShowTutorialDetail(List<string> tutorialCodeList)
	{
		List<TutorialDBData> dataList = new List<TutorialDBData>();

		bool isFound = true;
		foreach(string tutorialCode in tutorialCodeList)
		{
			List<TutorialDBData> list = null;
			isFound |= TutorialDB.GetDataByCode(tutorialCode, out list);
			if(isFound)
			{
				dataList.AddRange(list);
			}
		}

		if(isFound)
		{
			string title = "";
			string message = "";

			if(LocalizationManager.IsCurrentLanguage(LocalizationManager.Language.Thai))
			{
				title = dataList[0].TitleTh;
			}
			else
			{
				title = dataList[0].TitleEng;
			}
				
			List<Texture> imageList = new List<Texture>();
			List<string> descriptions = new List<string>();

			foreach(TutorialDBData data in dataList)
			{
				Texture imageTexture = Resources.Load(data.ImagePath, typeof(Texture)) as Texture;

				if (imageTexture != null)
				{
					imageList.Add(imageTexture);
					Resources.UnloadUnusedAssets ();
					//Resources.UnloadAsset(imageTexture);
				}
					
				if(LocalizationManager.IsCurrentLanguage(LocalizationManager.Language.Thai))
				{
					descriptions.Add(data.TextTh);
				}
				else
				{
					descriptions.Add(data.TextEng);
				}
			}

			this.HideUI(false);
			TutorialDetailUI.ShowUI(title, imageList, descriptions, this.ShowUI);
		}
	}
}
