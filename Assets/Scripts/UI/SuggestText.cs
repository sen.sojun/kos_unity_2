﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(UILabel))]
public class SuggestText : MonoBehaviour 
{
	List<string> _showTextList;

	void Awake()
	{
		_showTextList = new List<string>();
	}

	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
	}

	public void AddText(string text)
	{
		_showTextList.Insert(0, text);

		ShowText();
	}

	public void RepeatText()
	{
		if(_showTextList != null && _showTextList.Count > 0)
		{
			string text = _showTextList[0];
			_showTextList.Insert(0, text);
		}

		ShowText();
	}

	public void RemoveText()
	{
		if(_showTextList != null && _showTextList.Count > 0)
		{
			_showTextList.RemoveAt(0);
		}

		ShowText();
	}

	public void ClearText()
	{
		if(_showTextList != null)
		{
			_showTextList.Clear();
		}

		ShowText();
	}

	private void ShowText()
	{
		if(_showTextList != null && _showTextList.Count > 0)
		{
			GetComponent<UILabel>().text = _showTextList[0];
		}
		else
		{
			GetComponent<UILabel>().text = "";
		}
	}
}
