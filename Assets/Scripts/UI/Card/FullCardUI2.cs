﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;
using UnityEngine.Events;

public class FullCardUI2 : MonoBehaviour 
{
    public enum CardUISize
    {
          S
        , M
        , L
    }

    public delegate void OnClickEvent(FullCardUI2 fullcardUI);

    #region Private Properties
    private PlayerCardData _data;
    private bool _isNoStat = true;
    private bool _isShowBackCard = false;
    private OnClickEvent _onClickCallback = null;
    #endregion

    #region Public Properties    
    public CardUISize Size;
    
    public SpriteAtlas CardAtlas_L;
    public SpriteAtlas CardAtlas_M;
    public SpriteAtlas CardAtlas_S;
    
    public RectTransform Front;
    public RectTransform Back;
    
    public Image CardImage;
    public Image CardFrame;
    public Image RarityImage;
    
    public Text CostText;
    public Text NameText;
    public Text ShieldText;
    public Text HealthText;
    public Text AttackText;
    public Text SpeedText;
    public Text Description;
    
    public Text TypeText;
    public Text SuiteText;
    public Text ArtistText;
    
    public Image BackImage;

    public PlayerCardData Data
    {
        get { return _data; }
    }
    #endregion

	// Use this for initialization
	void Start () 
    {
        /*
        string cardID = "C0003";
    
        CardDBData data;
        CardDB.GetData(cardID, out data);
        
        //Debug.Log(cardID);
        
        SetData(new PlayerCardData(data.CardID));	
        */

        SetupButton();
	}

	/*
	// Update is called once per frame
	void Update () 
    {	
	}
    */

	private void OnEnable()
	{
        SetupButton();
	}

    private void SetupButton()
    {
        Button button = GetComponent<Button>();
        button.onClick.RemoveAllListeners();
        button.onClick.AddListener(this.OnClick);
    }

	public void SetData(PlayerCardData data)
    {
        _data = data;
        
        if (   Data.IsCardType(CardTypeData.CardType.Unit)
            || Data.IsCardType(CardTypeData.CardType.Structure)
            || Data.IsCardType(CardTypeData.CardType.Base) 
        ) 
        {
            _isNoStat = false;
        }
        else
        {
            _isNoStat = true;
        }
        
        UpdateDetail();
    }
    
    public void Flip(bool isShowBackCard)
    {
        _isShowBackCard = isShowBackCard;
        
        UpdateDetail();
    }
    
    private void UpdateDetail()
    {
        if(!_isShowBackCard)
        {
            CostText.text = Data.Cost.ToString();
            
            ShieldText.text = Data.Shield.ToString();
            HealthText.text = Data.HitPoint.ToString();
            AttackText.text = Data.Attack.ToString();
            SpeedText.text = Data.Speed.ToString();
            
            ShieldText.gameObject.SetActive(!_isNoStat);
            HealthText.gameObject.SetActive(!_isNoStat);
            AttackText.gameObject.SetActive(!_isNoStat);
            SpeedText.gameObject.SetActive(!_isNoStat);
            
            SetNameLabel();
            SetTypeLabel();
            SetDescription();
            SetArtistNameLabel();
            SetFrame();
            SetCardImage();
            SetCardSymbol();
        }
        
        Front.gameObject.SetActive(!_isShowBackCard);
        Back.gameObject.SetActive(_isShowBackCard);
    }
    
    private void SetNameLabel()
    {
        string name = "";
        switch (LocalizationManager.CurrentLanguage)
        {
            case LocalizationManager.Language.Thai:
            {
                name = Data.Name_TH;
                if (Data.Alias_TH.Length > 0)
                {
                     name += ", " + _data.Alias_TH;
                }
            }
            break;

            case LocalizationManager.Language.English:
            default:
            {
                name = Data.Name_EN;
                if (Data.Alias_EN.Length > 0)
                {
                    name += ", " + _data.Alias_EN;
                }
            }
            break;
        }

        NameText.text = name;        
        NameText.font = FontManager.GetFont(LocalizationManager.CurrentLanguage);
    }
    
    private void SetTypeLabel()
    {
        string typeName = "";
        switch (LocalizationManager.CurrentLanguage)
        {
            case LocalizationManager.Language.Thai:
            {
                typeName = Data.TypeData.TypeName_TH;
            }
            break;

            case LocalizationManager.Language.English:
            default:
            {
                typeName = Data.TypeData.TypeName_EN;
            }
            break;
        }

        TypeText.font = FontManager.GetFont(LocalizationManager.CurrentLanguage);
        TypeText.text = typeName;
        

        string typeSubfixText = "";
        if (Data.SubTypeDataList.Count > 0)
        {
            foreach (CardSubTypeData subTypeData in Data.SubTypeDataList)
            {
                if (string.Compare(subTypeData.CardSubTypeID, "ST0000") != 0) // not "None" subType
                {
                    string subTypeName = "";
                    switch (LocalizationManager.CurrentLanguage)
                    {
                        case LocalizationManager.Language.Thai:
                        {
                            subTypeName = subTypeData.SubTypeName_TH;
                        }
                        break;

                        case LocalizationManager.Language.English:
                        default:
                        {
                            subTypeName = subTypeData.SubTypeName_EN;
                        }
                        break;
                    }

                    typeSubfixText += " " + subTypeName;
                }
            }
        }

        if (Data.JobDataList.Count > 0)
        {
            foreach (CardJobData jobData in Data.JobDataList)
            {
                if (string.Compare(jobData.CardJobID, "J0000") != 0) // not "None" job
                {
                    string jobName = "";
                    switch (LocalizationManager.CurrentLanguage)
                    {
                        case LocalizationManager.Language.Thai:
                        {
                            jobName = jobData.JobName_TH;
                        }
                        break;

                        case LocalizationManager.Language.English:
                        default:
                        {
                            jobName = jobData.JobName_EN;
                        }
                        break;
                    }

                    typeSubfixText += " " + jobName;
                }
            }
        }

        // if have subfix text
        if (typeSubfixText.Length > 0)
        {
            TypeText.text += " -" + typeSubfixText;
        }
    }
    
    private void SetDescription()
    {
        string description = "";
        switch (LocalizationManager.CurrentLanguage)
        {
            case LocalizationManager.Language.Thai:
            {
                description = Data.DescriptionText_TH;
            }
            break;

            case LocalizationManager.Language.English:
            default:
            {
                description = Data.DescriptionText_EN;
            }
            break;
        }
        
        //Description.text = "[000000]" + description + "[-]"; 
        
        Description.text = description; 
        Description.font = FontManager.GetFont(LocalizationManager.CurrentLanguage);
    }
    
    private void SetArtistNameLabel()
    {
        switch (LocalizationManager.CurrentLanguage)
        {
            case LocalizationManager.Language.Thai:
            {
                ArtistText.text = Data.ImageData.ArtistName_TH;
            }
            break;

            case LocalizationManager.Language.English:
            default:
            {
                ArtistText.text = Data.ImageData.ArtistName_EN;
            }
            break;
        }
        
        //ArtistText.font = FontManager.GetFont(LocalizationManager.CurrentLanguage);
    }
    
    private void SetFrame()
    {
        string fullArtSuffix = "";
        if(Data.IsCardSubType(CardSubTypeData.CardSubType.Unique))
        {
            fullArtSuffix = "F"; // Full Art
        }
        
        string spellSuffix = "";
        if(_isNoStat)
        {
            spellSuffix = "S"; // Spell
        }
        
        string filePath = string.Format("{0}{1}{2}_{3}"
            , Data.ClanData.CardClanID
            , fullArtSuffix
            , spellSuffix
            , Size.ToString().ToUpper()
        );
        
        switch(Size)
        {
            case CardUISize.L:  CardFrame.sprite = CardAtlas_L.GetSprite(filePath); break;
            case CardUISize.M:  CardFrame.sprite = CardAtlas_M.GetSprite(filePath); break;
            case CardUISize.S:  CardFrame.sprite = CardAtlas_S.GetSprite(filePath); break;
        }
    }
    
    private void SetCardImage()
    {
        string filePath = string.Format("{0}_{1}"
            , Data.ImageData.CardImageID
            , Size.ToString().ToUpper()
        );
        
        switch(Size)
        {
            case CardUISize.L:  CardImage.sprite = CardAtlas_L.GetSprite(filePath); break;
            case CardUISize.M:  CardImage.sprite = CardAtlas_M.GetSprite(filePath); break;
            case CardUISize.S:  CardImage.sprite = CardAtlas_S.GetSprite(filePath); break;
        }
    }
    
    private void SetCardSymbol()
    {
        string filePath = string.Format("{0}_{1}"
            , Data.SymbolData.SymbolImageFile
            , Size.ToString().ToUpper()
        );
                
        switch(Size)
        {
            case CardUISize.L:  RarityImage.sprite = CardAtlas_L.GetSprite(filePath); break;
            case CardUISize.M:  RarityImage.sprite = CardAtlas_M.GetSprite(filePath); break;
            case CardUISize.S:  RarityImage.sprite = CardAtlas_S.GetSprite(filePath); break;
        }
    }

    public void BindOnClickCallback(OnClickEvent callback)
    {
        _onClickCallback += callback;
    }

    public void UnbindOnClickCallback(OnClickEvent callback)
    {
        _onClickCallback -= callback;
    }

    public void RemoveAllOnClickCallback()
    {
        _onClickCallback = null;
    }

    private void OnClick()
    {
        //Debug.Log("FullCardUI: OnClick");

        if (_onClickCallback != null)
        {
            _onClickCallback.Invoke(this);
        }
    }


}
