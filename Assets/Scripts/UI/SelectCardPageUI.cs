﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class SelectCardPageUI : MonoBehaviour 
{
	public delegate void OnConfirmEvent(List<int> uniqueCardIDList);
	public delegate void OnCancelEvent();
    
    private float _timer = 0.0f;
    private bool _isCanCancel = false;
    private bool _isHaveTimeout = false;
    private bool _isPauseTimeout = false;

	private OnConfirmEvent _onConfirmCallback = null;
	private OnCancelEvent _onCancelCallback = null;

	public GameObject Topic;
	public UILabel TopicLabel;
	public UILabel HeaderLabel;
	public UILabel PageLabel;

	public UIButton NextButton;
	public UIButton PrevButton;
	public UIButton ConfirmButton;
	public UIButton CancelButton;
    
    public UISlider TimeSlider;

	public List<SelectCardGrid> SelectCardGridList;

	private List<UniqueCardData> _dataList;
	private List<int> _selectIDList;
	private int _selectNum;

	private int _currentPageIndex = 0;

	// Use this for initialization
	void Start () 
	{
		//NGUITools.SetActive(gameObject, false);
	}
	
	// Update is called once per frame
	void Update () 
	{
	    if(_isHaveTimeout)
        {
            _timer -= Time.deltaTime;
            UpdateTimeSlider();
            if(_timer < 0.0f)
            {
                // timeout.                    
                _isHaveTimeout = false;
                AutoSelectOrCancelSelectCard();
            }
        }
	}

	public void ShowUI(string topicText, string headerText, List<UniqueCardData> cardDataList, int selectNum, OnConfirmEvent onConfirmCallback, OnCancelEvent onCancelCallback, bool isHaveTimeout, bool isCanCancel = true)
	{
		//Debug.Log("SelectCardGridUI: Show UI");

		_selectIDList = new List<int>();

		TopicLabel.text = topicText;
		if(topicText != null && topicText.Length > 0)
		{
			Topic.SetActive(true);
		}
		else
		{
			Topic.SetActive(false);
		}

		HeaderLabel.text = headerText;
		_selectNum = selectNum;
		_onConfirmCallback = onConfirmCallback;
		_onCancelCallback = onCancelCallback;

		_dataList = cardDataList;

		// Bind button onclick event.
		EventDelegate delConfirm = new EventDelegate(this, "OnConfirmClick");
		EventDelegate.Set(ConfirmButton.onClick, delConfirm);

		EventDelegate delCancel = new EventDelegate(this, "OnCancelClick");
		EventDelegate.Set(CancelButton.onClick, delCancel);

		/*
		if(selectNum > 0)
		{
			ConfirmButton.gameObject.SetActive(true);
			ConfirmButton.isEnabled = false;
		}
		else
		{
			ConfirmButton.gameObject.SetActive(false);
		}
		*/
		ConfirmButton.gameObject.SetActive(false);

        _isHaveTimeout = (BattleManager.Instance.IsHaveTimeout && isHaveTimeout);
        _isCanCancel = isCanCancel;
        _timer = BattleManager.Instance.PopupDecisionTime;
        _isPauseTimeout = _isHaveTimeout;
        if(_isPauseTimeout)
        {
            //BattleManager.Instance.SetPauseTimeout(true);
            if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
            {
                KOSNetwork.GamePlayer.localPlayer.RequestPauseTimer(true, BattleManager.Instance.TurnTimer);
            }
            else
            {
                BattleManager.Instance.SetPauseTimeout(true);
            }
            
            TimeSlider.gameObject.SetActive(true);
        }
        else
        {
            TimeSlider.gameObject.SetActive(false);
        }
        
		if(isCanCancel || selectNum <= 0)
		{
			CancelButton.gameObject.SetActive(true);
		}
		else
		{
			CancelButton.gameObject.SetActive(false);
		}

		// Show UI
		//ResetScrollView();
		NGUITools.SetActive(gameObject, true);
		BattleUIManager.RequestBringForward(this.gameObject);

		foreach(SelectCardGrid selectCardGrid in SelectCardGridList)
		{
			selectCardGrid.BindOnClickCallback(this.OnSelectCard);
		}

		_currentPageIndex = 0;
		ShowCurrentPage();
	}

	public void HideUI()
	{
		//Debug.Log("SelectCardPageUI: HideUI");
        if(_isPauseTimeout)
        {
            //BattleManager.Instance.SetPauseTimeout(false);
            if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
            {
                KOSNetwork.GamePlayer.localPlayer.RequestPauseTimer(false, BattleManager.Instance.TurnTimer);
            }
            else
            {
                BattleManager.Instance.SetPauseTimeout(false);
            }
        }

		//RemoveAllCard();
		foreach(SelectCardGrid selectCardGrid in SelectCardGridList)
		{
			selectCardGrid.BindOnClickCallback(null);
		}

		NGUITools.SetActive(gameObject, false);
	}

	private void ShowCurrentPage()
	{
		ShowPage(_currentPageIndex);
	}

	private void ShowPage(int pageIndex)
	{
		int numPerPage = SelectCardGridList.Count;
		int index = (pageIndex * numPerPage);

		foreach(SelectCardGrid selectCardGrid in SelectCardGridList)
		{
			if(_dataList != null && index < _dataList.Count)
			{
				selectCardGrid.SetUniqueCardData(_dataList[index]);

				bool isFound = false;
				foreach(int id in _selectIDList)
				{
					if(id == _dataList[index].UniqueCardID)
					{
						isFound = true;
						break;
					}
				}
				selectCardGrid.SetSelecting(isFound);

				selectCardGrid.gameObject.SetActive(true);
				BattleUIManager.RequestBringForward(selectCardGrid.gameObject);
			}
			else
			{
				selectCardGrid.gameObject.SetActive(false);
			}
			++index;
		}

		int maxPageIndex = 0;
		if(_dataList != null && _dataList.Count > 0)
		{
			maxPageIndex = Mathf.CeilToInt((float)_dataList.Count / (float)numPerPage) - 1;
		}
		PageLabel.text = (pageIndex + 1).ToString() + "/" + (maxPageIndex + 1).ToString();

		if(pageIndex == 0)
		{
			NGUITools.SetActive(PrevButton.gameObject, false);
		}
		else
		{
			NGUITools.SetActive(PrevButton.gameObject, true);
			BattleUIManager.RequestBringForward(PrevButton.gameObject);
		}

		if(pageIndex == maxPageIndex)
		{
			NGUITools.SetActive(NextButton.gameObject, false);
		}
		else
		{
			NGUITools.SetActive(NextButton.gameObject, true);
			BattleUIManager.RequestBringForward(NextButton.gameObject);
		}

	}

	public void ShowNextPage()
	{
		_currentPageIndex++;
		ShowCurrentPage();
	}

	public void ShowPrevPage()
	{
		_currentPageIndex--;
		ShowCurrentPage();
	}
		
	public void OnSelectCard(SelectCardGrid selectCardGrid)
	{
		//Debug.Log("SelectCardGridUI: OnSelectCard");

		if(_selectNum > 0)
		{
			bool isFound = false;
			foreach(int uniqueCardID in _selectIDList)
			{
				if (uniqueCardID == selectCardGrid.UniqueID)
				{
					// already in list
					isFound = true;
					break;
				}
			}

			//Debug.Log(isFound);

			if (isFound)
			{
				_selectIDList.Remove(selectCardGrid.UniqueID);

				SoundManager.PlayEFXSound(BattleController.Instance.SelectSound);
				selectCardGrid.SetSelecting(false);
			}
			else
			{
				if(_selectIDList.Count < _selectNum)
				{
					_selectIDList.Add(selectCardGrid.UniqueID);

					SoundManager.PlayEFXSound(BattleController.Instance.SelectSound);
					selectCardGrid.SetSelecting(true);
				}
			}

			if (_selectIDList.Count == _selectNum || _selectIDList.Count == _dataList.Count)
			{
				// Enable confirm button
				ConfirmButton.gameObject.SetActive(true);
				ConfirmButton.isEnabled = true;
			}
			else
			{
				// Disable confirm button
				ConfirmButton.gameObject.SetActive(false);
				ConfirmButton.isEnabled = false;
			}
		}
	}

	public void OnConfirmClick()
	{
		if(_dataList != null)
		{
			if (_selectIDList.Count == _selectNum || _selectIDList.Count == _dataList.Count)
			{
				HideUI();

				if (_onConfirmCallback != null)
				{
					_onConfirmCallback.Invoke(_selectIDList);
				}
			}
		}
	}

	public void OnCancelClick()
	{
		HideUI();

		if (_onCancelCallback != null)
		{
			_onCancelCallback.Invoke();
		}
	}
    
    public void AutoSelectOrCancelSelectCard()
    {
        if(_isCanCancel)
        {
            // if can cancel, will cancel
            Debug.Log("SelectCardPageUI/AutoSelectOrCancelSelectCard: Cancel");    
            
            OnCancelClick();
        }
        else
        {
            Debug.Log("SelectCardPageUI/AutoSelectOrCancelSelectCard: Auto Select");    
            
            _selectIDList = new List<int>();
            
            foreach(UniqueCardData card in _dataList)
            {
                _selectIDList.Add(card.UniqueCardID);
                if(_selectIDList.Count >= _selectNum)
                {
                    break;
                }
            }
        
            OnConfirmClick();
        }
    }
    
    private void UpdateTimeSlider()
    {
        if(TimeSlider != null)
        {
            float ratio = _timer/BattleManager.Instance.PopupDecisionTime;
            
            TimeSlider.value = Mathf.Clamp(ratio, 0.0f, 1.0f);
        }
    }
}
