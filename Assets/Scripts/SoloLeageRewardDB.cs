﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;

public class SoloLeagueRewardDBData 
{
	public enum RewardTypeEnum
	{
		  Card
		, PackNormal
		, PackPremium
	}

	// Use this for initialization
	#region Public Properties
	public string RewardID;
	public RewardTypeEnum RewardType;
	public string RewardKey;
	public string ShopID;
	public string RewardText_Eng;
	public string RewardText_Thai;
	public int	  DiamondUse;
	public int    LevelUse;
	public int    MinimumLevel;
	public int    PositionIndex;
	public string RewardOccur;
	#endregion

    
	#region Constructor
	public SoloLeagueRewardDBData ()
	{
		RewardID = "";
		RewardType = RewardTypeEnum.Card;
		RewardKey = "";
		ShopID = "";
		RewardText_Eng = "";
		RewardText_Thai = "";
		DiamondUse = 0;
		LevelUse = 0;
		MinimumLevel = 0;
		PositionIndex = 0;
		RewardOccur = "";
	}

	public SoloLeagueRewardDBData (string rewardId,
							  string rewardType,
							  string rewardKey,
						      string shopID,
							  string rewardText_Eng,
							  string rewardText_Thai,
						      int diamondUse,
							  int levelUse,
							  int minimumLevel,
							  int positionIndex,
							  string rewardOccur)
	{
	    RewardID = rewardId;

		switch (rewardType.Trim()) 
		{
			case "Card":
			case "CARD":
			case "card":
			{
				RewardType = RewardTypeEnum.Card;
			}
			break;

			case "PackNormal":
			case "PACKNORMAL":
			case "packnormal":
			{
				RewardType = RewardTypeEnum.PackNormal;
			}
			break;

			case "PackPremium":
			case "PACKPREMIUM":
			case "packpremium":
			{
				RewardType = RewardTypeEnum.PackPremium;
			}
			break;


			default:
			{
				Debug.LogError ("SoloLeagueRewardDBData: Can't parse RewardType.");
				RewardType = RewardTypeEnum.Card;
			}
			break;
		}

	    RewardKey = rewardKey;
		ShopID = shopID;
		RewardText_Eng = rewardText_Eng;
		RewardText_Thai = rewardText_Thai;
		DiamondUse = diamondUse;
		LevelUse = levelUse;
		MinimumLevel = minimumLevel;
		PositionIndex = positionIndex;
		RewardOccur = rewardOccur;
   } 


	public SoloLeagueRewardDBData (SoloLeagueRewardDBData soloLeagueRewardDBData)
	{
		RewardID = soloLeagueRewardDBData.RewardID;
		RewardType = soloLeagueRewardDBData.RewardType;
		RewardKey = soloLeagueRewardDBData.RewardKey; 
		ShopID = soloLeagueRewardDBData.ShopID;
		RewardText_Eng = soloLeagueRewardDBData.RewardText_Eng;
		RewardText_Thai = soloLeagueRewardDBData.RewardText_Thai;
		DiamondUse = soloLeagueRewardDBData.DiamondUse; 
		LevelUse = soloLeagueRewardDBData.LevelUse;
		MinimumLevel = soloLeagueRewardDBData.MinimumLevel;
		PositionIndex = soloLeagueRewardDBData.PositionIndex;
		RewardOccur = soloLeagueRewardDBData.RewardOccur;
	}
	#endregion

	#region Method
	public string GetDebugPrintText()
	{
		string resultText;
		resultText = string.Format(
			"REWARDID: {0}\nREWARDTYPE: {1}\nREWARDKEY: {2}\nSHOP_ID: {3}\nREWARDTEXT_ENG: {4}\nREWARDTEXT_THAI: {5}\nDIAMONDUSE: {6}\nLEVELUSE: {7}\nMINIMUMLEVEL: {8}\nPOSITIONINDEX: {9}\nREWARDOCCUR: {10}"
			, RewardID
			, RewardType
			, RewardKey
			, ShopID
			, RewardText_Eng
			, RewardText_Thai
			, DiamondUse.ToString()
			, LevelUse.ToString()
			, MinimumLevel.ToString()
			, PositionIndex.ToString()
			, RewardOccur
		);

		return resultText;
	}
	public void DebugPrintData()
	{
		Debug.Log(GetDebugPrintText());
	}
	#endregion
}

public class SoloLeagueRewardDB
{
		#region Enums
		private enum REWARDDBIndex : int
		{
			REWARDID = 0
			, REWARDTYPE
		    , REWARDKEY
		    , SHOPID
		    , REWARDTEXT_ENG
		    , REWARDTEXT_THAI
		    , DIAMONDUSE
		    , LEVELUSE
		    , MINIMUMLEVEL
		    , POSITIONINDEX
		    , REWARDOCCUR

		}
		#endregion
	#region Private Properties
	private static Dictionary<string, SoloLeagueRewardDBData> _rewardDBList = null;
	private static bool _isLoadDB = false;
	#endregion

	#region Methods
	public static bool LoadDB()
	{

		TextAsset csvFile = Resources.Load<TextAsset>("Databases/SoloDB - REWARDDB");
		if(csvFile != null)
		{
			List<List<string>> rawDBList;
			bool isSuccess = CSVReader.ReadCSV(csvFile, out rawDBList);
			if (isSuccess)
			{
				_rewardDBList = new Dictionary<string, SoloLeagueRewardDBData>();

				foreach(List<string> record in rawDBList)
				{
					SoloLeagueRewardDBData data = new SoloLeagueRewardDBData(
						record[(int)REWARDDBIndex.REWARDID]
						, record[(int)REWARDDBIndex.REWARDTYPE]
						, record[(int)REWARDDBIndex.REWARDKEY]
						, record[(int)REWARDDBIndex.SHOPID]
						, record[(int)REWARDDBIndex.REWARDTEXT_ENG]
						, record[(int)REWARDDBIndex.REWARDTEXT_THAI]
						, int.Parse(record[(int)REWARDDBIndex.DIAMONDUSE])
						, int.Parse(record[(int)REWARDDBIndex.LEVELUSE])
						, int.Parse(record[(int)REWARDDBIndex.MINIMUMLEVEL])
						, int.Parse(record[(int)REWARDDBIndex.POSITIONINDEX])
						, record[(int)REWARDDBIndex.REWARDOCCUR]
					 );
					_rewardDBList.Add(data.RewardID, data);
				}
				Resources.UnloadAsset(csvFile);
				_isLoadDB = true;
				return true;
			}
		}

		_isLoadDB = false;
		return false;
	}
	public static bool GetData(string rewardID, out SoloLeagueRewardDBData data)
	{
		if(!_isLoadDB)
		{
			LoadDB();
		}

		return (_rewardDBList.TryGetValue(rewardID, out data));
	}

	public static void GetAllData(out List<SoloLeagueRewardDBData> dataList)
	{
		if(!_isLoadDB)
		{
			LoadDB();
		}

		dataList = new List<SoloLeagueRewardDBData>();

		foreach (KeyValuePair<string, SoloLeagueRewardDBData> data in _rewardDBList)
		{
			dataList.Add(data.Value);
		}
	}

	public static void DebugPrintData()
	{
		StringBuilder sb = new StringBuilder();

		foreach (KeyValuePair<string, SoloLeagueRewardDBData> data in _rewardDBList)
		{
			sb.Append(data.Value.GetDebugPrintText());
			sb.Append("\n\n");
		}
		Debug.Log(sb.ToString());
	}
	#endregion

}
