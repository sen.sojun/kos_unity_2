﻿using UnityEngine;
using System.Collections;
using GameAnalyticsSDK;

public class AnalyticTrackButton : MonoBehaviour {

	public string AnalyticKey;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnClick()
	{
		GameAnalytics.NewDesignEvent (AnalyticKey);
	}
		
}
