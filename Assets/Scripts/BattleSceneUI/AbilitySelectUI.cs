﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AbilitySelectUI : MonoBehaviour 
{
	public List<AbilitySelectButton> ButtonList;

    private bool _isShow = false;
	private BattleCardData _battleCardData = null;
    
    public bool IsShow { get { return _isShow; } }

	// Use this for initialization
	void Start () 
	{
		//NGUITools.SetActive(gameObject, false);
	}
	
	// Update is called once per frame
	void Update () 
	{
	}

	public void ShowUI(BattleCardData battleCardData)
	{
		_battleCardData = battleCardData;

		List<AbilityData> abilityList;
		bool isFound = _battleCardData.GetCanUseAbilityList(out abilityList);
		if(isFound)
		{
			if(abilityList.Count == 1)
			{
				OnClickButton(abilityList[0].AbilityIndex);
				return;
			}

			int buttonIndex = 0;

			for(int index = 0; index < abilityList.Count; ++index)
			{
				AbilitySelectButton button = ButtonList[buttonIndex++];

				button.ButtonIndex = abilityList[index].AbilityIndex;

				string description = "";
				switch(LocalizationManager.CurrentLanguage)
				{
					case LocalizationManager.Language.Thai:
					{
						description = abilityList[index].Description_TH;
					}
					break;

					case LocalizationManager.Language.English:
					default:
					{
						description = abilityList[index].Description_EN;
					}
					break;
				}

				button.SetButtonLabel(description);

				/*
				EventDelegate ed = new EventDelegate();
				ed.parameters[0].value = button.ButtonIndex;
				ed.Set(this, "OnClickButton");
				button.GetComponent<UIButton>().onClick.Add(ed);
				*/

				NGUITools.SetActive(button.gameObject, true);
			}

			for(int index = buttonIndex; index < ButtonList.Count; ++index)
			{
				NGUITools.SetActive(ButtonList[index].gameObject, false);
			}
				
			NGUITools.SetActive(gameObject, true);
			BattleUIManager.RequestBringForward(gameObject, true);
            _isShow = true;
		}
		else
		{
			Debug.LogError("AbilitySelectUI/ShowUI: ability not found.");
			HideUI();
		}
	}

	public void HideUI()
	{
		NGUITools.SetActive(gameObject, false);
        _isShow = false;
	}
    
    public void CloseUI()
    {
        OnClickButton(0);
    }

	public void OnClickButton(int abilityIndex)
	{
		BattleManager.Instance.PopupUseAbilityBattleCard(_battleCardData.BattleCardID, abilityIndex);
		HideUI();
	}
}
