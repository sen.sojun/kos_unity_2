﻿using UnityEngine;
using System.Collections;


public class FindFirstOption : MonoBehaviour 
{
	public enum FindFirstOptionType : int
	{
		  Rock		= 0
		, Scissors	= 1
		, Paper		= 2
	}

	public enum FindFirstOptionResult : int
	{	
		  Lose 	= -1
		, Draw	= 0
		, Win	= 1
	}
		
	public FindFirstOptionType OptionType;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public static FindFirstOptionResult FindFirstOptionCompare(FindFirstOptionType op1, FindFirstOptionType op2)
	{
		if(op1 != op2)
		{
			if(op1 == FindFirstOptionType.Rock)
			{
				if(op2 == FindFirstOptionType.Scissors) 	 return FindFirstOptionResult.Win;
				else if(op2 == FindFirstOptionType.Paper)	 return FindFirstOptionResult.Lose;
			}
			else if(op1 == FindFirstOptionType.Scissors)
			{
				if(op2 == FindFirstOptionType.Rock) 		 return FindFirstOptionResult.Lose;
				else if(op2 == FindFirstOptionType.Paper)	 return FindFirstOptionResult.Win;
			}
			else if(op1 == FindFirstOptionType.Paper)
			{
				if(op2 == FindFirstOptionType.Rock) 		 return FindFirstOptionResult.Win;
				else if(op2 == FindFirstOptionType.Scissors) return FindFirstOptionResult.Lose;
			}
		}

		return FindFirstOptionResult.Draw;
	}

	public static FindFirstOptionType RandomOption()
	{
		int r = Random.Range(0, 3); // [0-3)

		FindFirstOptionType optionType = (FindFirstOptionType)r;

		return optionType;
	}

	/*
	void OnClick()
	{
		Debug.Log("Click " + OptionType.ToString());
	}
	*/
}
