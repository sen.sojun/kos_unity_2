﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class ProfileUIManager : MonoBehaviour
{
    #region Private Properties
    private BattleManager _battleManager = null;
    #endregion

    #region Public Properties
    public ProfileUI EnemyProfileUI;
    public ProfileUI PlayerProfileUI;

    public UIButton SettingsButton;
    
    private int MarginiPhoneX = 127;
    #endregion
    
    #region Start
    // Use this for initialization
	private void Start () 
    {
        CheckIPhoneXMargin();
	}
    #endregion

    #region OnDestroy
    void OnDestory()
    {
        if (_battleManager != null)
        {
            _battleManager.UnSubscribePlayersInfoUpdateEvent(SetProfiles);
        }
    }
    #endregion

    #region Update
    // Update is called once per frame
	private void Update () 
    {
	}
    #endregion

    #region Methods
    private void CheckIPhoneXMargin()
    {
        #if UNITY_IOS || UNITY_EDITOR
        double ratio = (double)Screen.width/(double)Screen.height;
        //double iPhoneRatio = 2436.0/1125.0;
        
        //Debug.Log("Ratio : " + ratio.ToString());
        //Debug.Log("Ratio X : " + iPhoneRatio.ToString());
        
        if(    (UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhoneX)
            || (ratio >= 2.1650 && ratio <=  2.1697)
        )
        {
            // iPhone X
            Debug.Log("You are iPhone X.");
            UIWidget widget = GetComponent<UIWidget>();
            widget.leftAnchor.absolute =  widget.leftAnchor.absolute + MarginiPhoneX;
            widget.rightAnchor.absolute =  widget.rightAnchor.absolute + MarginiPhoneX;
        }
        #endif
    }
    
    public void SetBattleManager(BattleManager battleManager)
    {
        _battleManager = battleManager;
        _battleManager.SubscribePlayersInfoUpdateEvent(SetProfiles);
    }

    private void SetProfile(DisplayPlayerIndex displayPlayerIndex, PlayerBattleData playerBattleData)
	{
		//Debug.Log(displayPlayerIndex);

        switch (displayPlayerIndex)
		{
            case DisplayPlayerIndex.Player:
			{
				PlayerProfileUI.SetProfile(playerBattleData);
			}
			break;

            case DisplayPlayerIndex.Enemy:
			{
				EnemyProfileUI.SetProfile(playerBattleData);
				//	EnemyProfileUI.SetProfile(playerBattleData,"enemy");
			}
			break;
		}
	}

    public void OnClickSettingsButton()
    {
        //Debug.Log("Settings Clicked!");

		BattleManager.Instance.GoToRetry();
    }

    public void SetProfiles(PlayerBattleData[] playersBattleData)
    {
        if (_battleManager.GetPlayer(PlayerIndex.One).IsLocalPlayer()) 
        {
            // I'am 1
            SetProfile(DisplayPlayerIndex.Player, playersBattleData[(int)PlayerIndex.One]);
            SetProfile(DisplayPlayerIndex.Enemy, playersBattleData[(int)PlayerIndex.Two]);
        }
        else 
        {
            // I'am 2
            SetProfile(DisplayPlayerIndex.Player, playersBattleData[(int)PlayerIndex.Two]);
            SetProfile(DisplayPlayerIndex.Enemy, playersBattleData[(int)PlayerIndex.One]);
        }
    }
    #endregion
}
