﻿using UnityEngine;
using System.Collections;

public class SelectCardGrid : MonoBehaviour 
{
    public delegate void OnClickEvent(SelectCardGrid selectCardGrid);

    private UniqueCardData _uniqueCardData;
    private bool _isSelect = false;
    private OnClickEvent _onClickEvent = null;

    public FullCardUI FullCard;
	public GameObject SelectFrame;

    public int UniqueID
    {
        get { return _uniqueCardData.UniqueCardID; }
    }
		
	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
    /*
	void Update () {
	
	}
    */

    public void SetUniqueCardData(UniqueCardData uniqueCardData)
    {
        _uniqueCardData = uniqueCardData;
        FullCard.SetCardData(_uniqueCardData.PlayerCardData);

		FullCard.RemoveAllOnClickCallback();
		FullCard.BindOnClickCallback(OnClickSelectCardGrid);

		BattleUIManager.RequestBringForward(SelectFrame.gameObject);
		FullCard.RequestBringForward();
    }

    public void BindOnClickCallback(OnClickEvent onClickEvent)
    {
        _onClickEvent = onClickEvent;
    }

	public void OnClickSelectCardGrid(FullCardUI fullCardUI)
    {
		//Debug.Log("SelectCardGrid: OnClick");

        if (_onClickEvent != null)
        {
            _onClickEvent.Invoke(this);
        }
    }

    public void SetSelecting(bool isSelect)
    {
        if(_isSelect != isSelect)
        {
            _isSelect = isSelect;
        }

		NGUITools.SetActive(SelectFrame, _isSelect);

		BattleUIManager.RequestBringForward(SelectFrame.gameObject);
		BattleUIManager.RequestBringForward(FullCard.gameObject);
    }
}
