﻿using UnityEngine;
using System.Collections;

public enum LocalBattleZoneIndex : int
{
	  PlayerBase    = 0
	, Battlefield   = 1
	, EnemyBase     = 2
}

public class FieldUIManager : MonoBehaviour 
{
    public delegate void OnClickBattleCardEvent(int battleCardID);

	private BattleManager _battleManager = null;

    public FieldUI EnemyBase;
    public FieldUI Battlefield;
    public FieldUI PlayerBase;

    public OnClickBattleCardEvent _OnClickBattleCard;

	// Use this for initialization
	void Start () 
    {	
	}
	
	// Update is called once per frame
    /*
	void Update () 
    {
	}
    */

	public void SetBattleManager(BattleManager battleManager)
	{
		_battleManager = battleManager;
	}

	public bool CreateBattleCardUI(BattleCardData battleCard, BattleZoneIndex battleZoneIndex, BattleCardUI.OnClickBattleCard onClickBattleCardCallback)
	{
		IBattleControlController localPlayer = null;
		bool isFound = _battleManager.GetLocalPlayer(out localPlayer);

		if(isFound)
		{
			bool isMeP1 = (localPlayer.GetPlayerIndex() == PlayerIndex.One);
            bool isMyCard = (localPlayer.GetPlayerIndex() == battleCard.OwnerPlayerIndex);
				
			FieldUI targetField;
			switch(battleZoneIndex)
			{
				case BattleZoneIndex.BaseP1:
				{
					if(isMeP1)
					{
						targetField = PlayerBase;
					}
					else
					{
						targetField = EnemyBase;
					}
				}
				break;

				case BattleZoneIndex.BaseP2:
				{
					if(isMeP1)
					{
						targetField = EnemyBase;
					}
					else
					{
						targetField = PlayerBase;
					}
				}
				break;

				case BattleZoneIndex.Battlefield:
				default:
				{
					targetField = Battlefield;
				}
				break;
			}

			FieldUI.FieldSide targetSide;
			if(isMyCard)
			{
				targetSide = FieldUI.FieldSide.Player;
			}
			else
			{
				targetSide = FieldUI.FieldSide.Enemy;
			}

            targetField.CreateCard(battleCard, targetSide, onClickBattleCardCallback);

			return true;
		}

		return false;
	}
		
	public bool CreateDummyBattleCardUI(BattleZoneIndex battleZoneIndex, PlayerIndex ownerPlayer)
	{
		bool isMeP1 = (ownerPlayer == PlayerIndex.One);
		bool isMyCard = (ownerPlayer == PlayerIndex.One);

		FieldUI targetField;
		switch(battleZoneIndex)
		{
			case BattleZoneIndex.BaseP1:
			{
				if(isMeP1)
				{
					targetField = PlayerBase;
				}
				else
				{
					targetField = EnemyBase;
				}
			}
			break;

			case BattleZoneIndex.BaseP2:
			{
				if(isMeP1)
				{
					targetField = EnemyBase;
				}
				else
				{
					targetField = PlayerBase;
				}
			}
			break;

			case BattleZoneIndex.Battlefield:
			default:
			{
				targetField = Battlefield;
			}
			break;
		}

		FieldUI.FieldSide targetSide;
		if(isMyCard)
		{
			targetSide = FieldUI.FieldSide.Player;
		}
		else
		{
			targetSide = FieldUI.FieldSide.Enemy;
		}

		targetField.CreateDummyCard(targetSide);

		return true;
	}

	public bool GetBattleCardUI(int battleCardID, out BattleCardUI battleCard)
	{
		bool isFound = false;

		isFound = Battlefield.GetBattleCard(battleCardID, out battleCard);
		if(isFound) return true;

		isFound = EnemyBase.GetBattleCard(battleCardID, out battleCard);
		if(isFound) return true;

		isFound = PlayerBase.GetBattleCard(battleCardID, out battleCard);
		if(isFound) return true;

		battleCard = null;
		return false;
	}

    public bool MoveBattleCardUI(int battleCardID, BattleZoneIndex targetZone)
    {
        // Find target battle card.
        BattleCardUI battleCard = null;

        bool isFound = GetBattleCardUI(battleCardID, out battleCard);
        if (!isFound)
        {
            Debug.LogError("FieldUIManager/MoveBattleCard: Not found battle card UI.");
            return false;
        }
        else
        {
			return MoveBattleCardUI(battleCard, battleCardID, targetZone);
        }
    }

	public bool MoveBattleCardUI(BattleCardUI battleCardUI, int battleCardID, BattleZoneIndex targetZone)
    {
        // Find local player.
        IBattleControlController localPlayer = null;
        bool isFoundPlayer = _battleManager.GetLocalPlayer(out localPlayer);
        if (!isFoundPlayer)
        {
            Debug.LogError("FieldUIManager/MoveBattleCard: Not found local player.");
            return false;
        }

        // Find current field.
        FieldUI currentField = battleCardUI.CurrentField;
        if (currentField == null)
        {
            Debug.LogError("FieldUIManager/MoveBattleCard: Not found battle card current field.");
            return false;
        }

        // Find target field.
        FieldUI targetField = null;
        switch (targetZone)
        {
            case BattleZoneIndex.BaseP1:
                {
                    if (localPlayer.GetPlayerIndex() == PlayerIndex.One)
                    {
                        // local is one
                        targetField = PlayerBase;
                    }
                    else
                    {
                        targetField = EnemyBase;
                    }
                }
                break;

            case BattleZoneIndex.Battlefield:
                {
                    targetField = Battlefield;
                }
                break;

            case BattleZoneIndex.BaseP2:
                {
                    if (localPlayer.GetPlayerIndex() == PlayerIndex.One)
                    {
                        // local is one
                        targetField = EnemyBase;
                    }
                    else
                    {
                        targetField = PlayerBase;
                    }
                }
                break;
        }

        if (targetField == null)
        {
            Debug.LogError("FieldUIManager/MoveBattleCard: Not found target field.");
            return false;
        }

        // Try move card from current field to target field.
        // If success return true, else return false.
		bool isSuccess = currentField.GiveBattleCardTo(targetField, battleCardID);
        return isSuccess;
    }

	public bool DestroyBattleCardUI(int battleCardID)
	{
		bool isSuccess = false;

		isSuccess = Battlefield.DestroyCard(battleCardID);
		if(isSuccess) return true;

		isSuccess = EnemyBase.DestroyCard(battleCardID);
		if(isSuccess) return true;

		isSuccess = PlayerBase.DestroyCard(battleCardID);
		if(isSuccess) return true;

		return false;
	}

	public void SetAllBattleCardUIClickable(bool isClickable)
	{
		Battlefield.SetAllBattleCardUIClickable(isClickable);
		EnemyBase.SetAllBattleCardUIClickable(isClickable);
		PlayerBase.SetAllBattleCardUIClickable(isClickable);
	}

	public bool SetBattleCardUIClickable(int battleCardID, bool isClickable)
	{
		BattleCardUI battleCard;
		bool isFound = GetBattleCardUI(battleCardID, out battleCard);
		if(isFound)
		{
			battleCard.IsClickable = isClickable;
			if(isClickable)
			{
				battleCard.ShowTargetMark(true);
			}
			return true;
		}

		return false;
	}
}
