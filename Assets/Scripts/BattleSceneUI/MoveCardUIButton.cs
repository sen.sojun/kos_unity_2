﻿using UnityEngine;
using System.Collections;

public class MoveCardUIButton : MonoBehaviour 
{
	public LocalBattleZoneIndex MoveZoneIndex;
	public MoveCardUI MoveCardUI;
	public UISprite Arrow;

	// Use this for initialization
	void Start () 
	{
	}

	/*
	// Update is called once per frame
	void Update () {
	
	}
	*/

	void OnClick()
	{
		this.MoveCardUI.OnSelectMoveField(MoveZoneIndex);
	}
}
