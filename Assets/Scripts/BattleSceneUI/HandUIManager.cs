﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HandUIManager : MonoBehaviour
{
    #region Private Poperties
    private List<HandCardUI> _handCardList;

    /*
	private List<PlayerCardData> _createQueue;
	private List<PlayerCardData> _currentHand;
    */

	private BattleManager _battleManager;
	private IBattleControlController _localPlayer;
	private bool _isShowPreview = false;

    private static int _handCardIndex = 0; // 1, 2, 3, 4, ...
    #endregion

    #region Public Poperties
	public FullCardPreviewUI FullCardPreview;
	public GameObject HandCardPrefab;


    public float CardOffset = 10.0f; // Offset between card.
	public bool IsShowPreview
	{
		get { return _isShowPreview; }
	}
    //public float CardWidth = 360.0f;
    #endregion

	void Awake()
	{
        _handCardList = new List<HandCardUI>();
	}

    #region Start
    // Use this for initialization
	void Start () 
    {
	}
    #endregion

    /*
    #region Update
    // Update is called once per frame
	void Update () 
    {
    }
    #endregion
    */

    #region Methods
	public void SetBattleManager(BattleManager battleManager)
	{
		_battleManager = battleManager;

		_battleManager.SubscribePlayersInfoUpdateEvent(this.UpdatePlayersInfo);
	}

	private void UpdatePlayersInfo(PlayerBattleData[] playersData)
	{
		UpdateHandCardUI();
	}
		
    /*
	public void OnDrawCard(PlayerCardData playerCardData)
	{
		AddCard(playerCardData);
	}

    public void AddCard(PlayerCardData playerCardData)
    {
		_createQueue.Add(playerCardData);
	}
    */

	public void CreateHandCard(UniqueCardData uniquePlayerCardData, HandCardUI.OnClickHandCard onClickHandCardCallback)
	{
		GameObject newHandCard = NGUITools.AddChild(gameObject, HandCardPrefab);
        
		newHandCard.transform.parent = this.transform;
        newHandCard.transform.localScale = Vector3.one;

		HandCardUI handCardUI = newHandCard.GetComponent<HandCardUI>();
		handCardUI.SetCardData(uniquePlayerCardData);
		handCardUI.SetHandUIManager(this);
        handCardUI.SetOnClickCallback(onClickHandCardCallback);
		handCardUI.SetShowCardPreviewCallback(
			  this.ShowFullCardPreview
			, this.HideFullCardPreview
		);

        _handCardList.Add(handCardUI);

        Reposition();

		BattleUIManager.RequestBringForward(newHandCard.gameObject, false);

		UpdateHandCardUI();
    }

    public HandCardUI GetCardByID(int uniqueCardID)
    {
        if (_handCardList.Count > 0)
        {
            for (int index = 0; index < _handCardList.Count; ++index)
            {
				if (_handCardList[index].UniqueCardID == uniqueCardID)
                {
                    return _handCardList[index]; 
                }
            }
        }

        return null;
    }

	public bool RemoveCardByID(int uniqueCardID)
	{
		if (_handCardList.Count > 0)
		{
			for (int index = 0; index < _handCardList.Count; ++index)
			{
				if (_handCardList[index].UniqueCardID == uniqueCardID)
				{
					return RemoveCard(index); 
				}
			}
		}

		return false;
	}

    public bool RemoveCard(int index)
    {
        if (index >= 0 && index < _handCardList.Count)
        {
            _handCardList.RemoveAt(index);
            Reposition();
			UpdateHandCardUI();

            return true;
        }
        
        return false;
    }

	public bool DestroyCardByID(int uniqueCardID)
	{
		if (_handCardList.Count > 0)
		{
			for (int index = 0; index < _handCardList.Count; ++index)
			{
				if (_handCardList[index].UniqueCardID == uniqueCardID)
				{
					return DestroyCard(index); 
				}
			}
		}

		return false;
	}

    public bool DestroyCard(int index)
    {
        if (index >= 0 && index < _handCardList.Count)
        {
            GameObject obj = _handCardList[index].gameObject;
            _handCardList.RemoveAt(index);
            Destroy(obj);

            Reposition();
			UpdateHandCardUI();

            return true;
        }

        return false;
    }

    public void DestroyAllCard()
    {
        if (_handCardList.Count > 0)
        {
            while (_handCardList.Count > 0)
            {
                GameObject obj = _handCardList[0].gameObject;
                _handCardList.RemoveAt(0);
                Destroy(obj);
            }
        }
    }

	public void SetAllHandCardClickable(bool isClickable)
	{
		if (_handCardList.Count > 0)
		{
			for (int index = 0; index < _handCardList.Count; ++index)
			{
				_handCardList[index].IsClickable = isClickable;
			}
		}
	}

	public void ShowFullCardPreview(PlayerCardData playerCardData)
	{
		FullCardPreview.ShowUI(playerCardData);
		_isShowPreview = true;
	}

	public void HideFullCardPreview()
	{
		FullCardPreview.HideUI();
		_isShowPreview = false;
	}

    private void Reposition()
    {
        if (_handCardList.Count <= 0) return; // Ignore empty hand. 

		float _cardWidth = HandCardPrefab.GetComponent<UIWidget>().width;
		float _cardHeight = HandCardPrefab.GetComponent<UIWidget>().height;

        float overallHandSize = (_handCardList.Count * _cardWidth) + ((_handCardList.Count - 1) * CardOffset);
		float centerOffset = (overallHandSize * 0.5f) - (_cardWidth * 0.5f);
        //Debug.Log(centerOffset);

        for (int index = 0; index < _handCardList.Count; ++index)
        {
            _handCardList[index].transform.localPosition = new Vector3((_cardWidth * index) + (CardOffset * index) - centerOffset - (_cardWidth * 0.5f), (_cardHeight * 0.5f), 0);
        }
    }

    public static int RequestHandCardID()
    {
        return ++_handCardIndex;
    }

	public void UpdateHandCardUI()
	{
		for (int index = 0; index < _handCardList.Count; ++index)
		{
			_handCardList[index].UpdateCanUse();
		}
	}
    #endregion
}
