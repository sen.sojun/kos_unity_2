﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FieldUI : MonoBehaviour
{
	public enum FieldSide
	{
		  Player
		, Enemy
	}

    #region Private Properties
    private List<BattleCardUI> _playerCardObjList; // list of player card object
	private List<BattleCardUI> _enemyCardObjList; // list of enemy card object

	private float _currentWidth = 0.0f;
	private int _tabIndex = 0;
	private bool _isOverflow = false;
    #endregion

    #region Public Properties
	public UIPanel ClippingPanel;
	public GameObject Content;
	public UIButton ShiftLeftButton;
	public UIButton ShiftRightButton;

	public GameObject BattleCardPrefab;

	public float CardOffset = 100.0f; // Offset between card.
	public float CardWidth = 360.0f;
	public float BorderWidth = 10.0f; // Border of card list.

	public FieldUI NextField = null;
    public FieldUI PrevField = null;

	private Vector3 ContentLocalPos
	{
		get
		{ 
			return Content.transform.localPosition; 
		}
		set
		{
			Content.transform.localPosition = value;

			// Redraw it clipping panel
			ClippingPanel.Invalidate(true);
		}
	}

	public float ContentMinPosX
	{
		get
		{
			if(_isOverflow)
			{
				return ContentMaxPosX - (_currentWidth - MinSize.x);
			}

			return ContentMaxPosX;
		}
	}

	public float ContentMaxPosX
	{
		get
		{
			return ShiftLeftButton.GetComponent<UISprite>().localSize.x;
		}
	}

	public Vector2 MinSize
	{
		get 
		{
			return ClippingPanel.GetViewSize();
		}
	}
    #endregion


    #region Start
    // Use this for initialization
	void Start () 
    {
		_playerCardObjList = new List<BattleCardUI>(); // initialization
		_enemyCardObjList = new List<BattleCardUI>();  // initialization
	}
    #endregion

    #region Update
    // Update is called once per frame
	void Update () 
    {
		UpdateContentPosition();
	}
    #endregion

    #region Methods
	public void CreateCard(BattleCardData battleCardData, FieldSide fieldSide, BattleCardUI.OnClickBattleCard onClickCallback)
	{
		List<BattleCardUI> targetSide = null;

		switch(fieldSide)
		{
			case FieldSide.Player:
			{
				targetSide = _playerCardObjList;
			}
			break;

			case FieldSide.Enemy:
			{
				targetSide = _enemyCardObjList;
			}
			break;
		}

		bool isCanCreate = true;

		if(targetSide.Count > 0)
		{
			for(int index = 0; index < targetSide.Count; ++index)
			{
				BattleCardData showBattleCardData = targetSide[index].ShowBattleCardData;
				if(showBattleCardData.TypeData == CardTypeData.CardType.Structure)
				{
					if(showBattleCardData.PlayerCardData.PlayerCardID == battleCardData.PlayerCardData.PlayerCardID)
					{
						// Same
						targetSide[index].AddBattleCard(battleCardData);
						isCanCreate = false;
						break;
					}
				}
			}
		}

		if(isCanCreate)
		{
			CreateCardUI(battleCardData, fieldSide, onClickCallback);
		}
	}

	private void CreateCardUI(BattleCardData battleCardData, FieldSide fieldSide, BattleCardUI.OnClickBattleCard onClickCallback)
	{
		GameObject newBattleCard = Instantiate(BattleCardPrefab) as GameObject;
		BattleCardUI newBattleCardManager = newBattleCard.GetComponent<BattleCardUI>();
		newBattleCardManager.SetInitData(battleCardData, fieldSide, this);
		newBattleCardManager.SetOnClickCallback(onClickCallback);

		newBattleCard.transform.SetParent(Content.transform);
		newBattleCardManager.transform.localScale = Vector3.one;
		NGUITools.MarkParentAsChanged(newBattleCard.gameObject);

		switch(fieldSide) 
		{  
		case FieldSide.Enemy:
			{
				// Enemy
				_enemyCardObjList.Add(newBattleCardManager);
			}
			break;

		case FieldSide.Player:
			{  
				// Player
				_playerCardObjList.Add(newBattleCardManager);
			}
			break;
		}

		Reposition();

		BattleUIManager.RequestBringForward(newBattleCardManager.gameObject, false);
		//NGUITools.ImmediatelyCreateDrawCalls(newBattleCardManager.gameObject);
	}
		
	public void CreateDummyCard(FieldSide fieldSide)
	{
		GameObject newBattleCard = Instantiate(BattleCardPrefab) as GameObject;
		BattleCardUI newBattleCardManager = newBattleCard.GetComponent<BattleCardUI>();

		newBattleCard.transform.parent = Content.transform;
		newBattleCardManager.transform.localScale = Vector3.one;
		newBattleCardManager.transform.localPosition = Vector3.zero;
		NGUITools.MarkParentAsChanged(newBattleCard.gameObject);

		switch(fieldSide) 
		{  
			case FieldSide.Enemy:
			{
				// Enemy
				_enemyCardObjList.Add(newBattleCardManager);
			}
			break;

			case FieldSide.Player:
			{  
				// Player
				_playerCardObjList.Add(newBattleCardManager);
			}
			break;
		}

		Reposition();

		BattleUIManager.RequestBringForward(newBattleCardManager.gameObject, false);
	}

	/*
	public bool DestroyCard(BattleCardUI battleCardUI)
    {
		return DestroyCard(battleCardUI.BattleCardID);
    }
    */

    public bool DestroyCard(int battleCardID)
    {
        // Find in _enemyCardObjList
        for (int index = 0; index < _enemyCardObjList.Count; ++index)
        {
			if (_enemyCardObjList[index].IsHasBattleID(battleCardID))
            {
				BattleCardUI ui = _enemyCardObjList[index];
				bool isSuccess = ui.RemoveBattleCard(battleCardID);
				if(isSuccess)
				{
					if(ui.CardCount <= 0)
					{
						// Empty.
						_enemyCardObjList.RemoveAt(index);
						Destroy(ui.GameObject);
						Reposition();
					}

					return true; // return true is successful destroy.
				}
            }
        }

        // Find in _playerCardObjList
        for (int index = 0; index < _playerCardObjList.Count; ++index)
        {
			if (_playerCardObjList[index].IsHasBattleID(battleCardID))
            {
				BattleCardUI ui = _playerCardObjList[index];
				bool isSuccess = ui.RemoveBattleCard(battleCardID);
				if(isSuccess)
				{
					if(ui.CardCount <= 0)
					{
						// Empty.
						_playerCardObjList.RemoveAt(index);
						Destroy(ui.GameObject);
						Reposition();
					}

					return true; // return true is successful destroy.
				}
            }
        }

        return false; // return false is not successful destroy.
    }
		
	private void AddCard(BattleCardUI battleCardUI, bool isEnemy)
    {
		List<BattleCardUI> targetSide = null;

		switch(battleCardUI.Side)
		{
		case FieldSide.Player:
			{
				targetSide = _playerCardObjList;
			}
			break;

		case FieldSide.Enemy:
			{
				targetSide = _enemyCardObjList;
			}
			break;
		}

		bool isCanAdd = true;

		if(targetSide.Count > 0)
		{
			for(int index = 0; index < targetSide.Count; ++index)
			{
				BattleCardData showBattleCardData = targetSide[index].ShowBattleCardData;

				if(showBattleCardData.TypeData == CardTypeData.CardType.Structure)
				{
					if(showBattleCardData.PlayerCardData.PlayerCardID == battleCardUI.ShowBattleCardData.PlayerCardData.PlayerCardID)
					{
						// Same
						targetSide[index].AddBattleCard(battleCardUI.ShowBattleCardData);
						Destroy(battleCardUI.gameObject);
						isCanAdd = false;
						break;
					}
				}
			}
		}

		if(isCanAdd)
		{
			if (isEnemy)
			{
				// Enemy
				battleCardUI.GameObject.transform.parent = Content.transform;
				_enemyCardObjList.Add(battleCardUI);
			}
			else
			{
				// Player
				battleCardUI.GameObject.transform.parent = Content.transform;
				_playerCardObjList.Add(battleCardUI);
			}

			NGUITools.MarkParentAsChanged(battleCardUI.GameObject);
			battleCardUI.SetCurrentField(this);
			Reposition();
		}

		//NGUITools.ImmediatelyCreateDrawCalls(battleCardUI.gameObject);        
    }
		
	/*
    private bool RemoveCard(BattleCardUI battleCardUI)
    {
        return RemoveCard(battleCardUI.BattleCardID);
    }
    */

	private bool RemoveCard(int battleCardID, out BattleCardUI battleCardUI)
    {
        // Find in _enemyCardObjList
        for (int index = 0; index < _enemyCardObjList.Count; ++index)
        {
			if (_enemyCardObjList[index].IsHasBattleID(battleCardID))
            {
				BattleCardUI ui = _enemyCardObjList[index];
				if(ui.CardCount > 1)
				{
					ui.RemoveBattleCard(battleCardID);

					battleCardUI = null;
					return true; // remove only data.
				}
				else if(ui.CardCount == 1)
				{
					_enemyCardObjList.RemoveAt(index);
					Reposition();

					battleCardUI = ui;
					return true; // remove UI.
				}
            }
        }

        // Find in _playerCardObjList
        for (int index = 0; index < _playerCardObjList.Count; ++index)
        {
			if (_playerCardObjList[index].IsHasBattleID(battleCardID))
            {
				BattleCardUI ui = _playerCardObjList[index];
				if(ui.CardCount > 1)
				{
					ui.RemoveBattleCard(battleCardID);

					battleCardUI = null;
					return true; // remove only data.
				}
				else if(ui.CardCount == 1)
				{
					_playerCardObjList.RemoveAt(index);
					Reposition();

					battleCardUI = ui;
					return true; // remove UI.
				}
            }
        }

		battleCardUI = null;
        return false; // return false is not successful remove.
    }

	public bool GetBattleCard(int battleCardID, out BattleCardUI battleCard)
    {
        // Find in _enemyCardObjList
        for (int index = 0; index < _enemyCardObjList.Count; ++index)
        {
			if (_enemyCardObjList[index].IsHasBattleID(battleCardID))
            {
				// Found in enemy side.
				battleCard = _enemyCardObjList[index];
                return true; 
            }
        }

        // Find in _playerCardObjList
        for (int index = 0; index < _playerCardObjList.Count; ++index)
        {
			if (_playerCardObjList[index].IsHasBattleID(battleCardID))
            {
				// Found in player side.
				battleCard = _playerCardObjList[index];
                return true;
            }
        }

		battleCard = null;
        return false; // return false if not found.
    }

    public bool GiveBattleCardTo(FieldUI targetField, int battleCardID)
    {
        BattleCardUI battleCard;
        bool isFound = GetBattleCard(battleCardID, out battleCard);
        if (isFound)
        {
			FieldSide side = battleCard.Side;
			BattleCardUI.OnClickBattleCard callback = battleCard.OnClickBattleCardCallback;

			BattleUIManager.RequestBringForward(battleCard.GameObject, false);
			//BattleUIManager.RequestBringForward(BattleController.Instance.BattleUIManager.PopupPanel.gameObject);
			NGUITools.AdjustDepth(BattleController.Instance.BattleUIManager.PopupPanel.gameObject, 1001);

            // Remove card from local battle card list.
			BattleCardUI removeBattleCard = null;
			bool isRemove = RemoveCard(battleCardID, out removeBattleCard);
			if(isRemove)
			{
				if(removeBattleCard != null)
				{
		            // Give UI.
					targetField.AddCard(removeBattleCard, (battleCard.Side == FieldSide.Enemy));
					SoundManager.PlayEFXSound(BattleController.Instance.MoveSound);
				}
				else
				{
					// Give just data.
					BattleCardData battleCardData;
					bool isSuccess = BattleManager.Instance.FindBattleCard(battleCardID, out battleCardData);
					if(isSuccess)
					{
						targetField.CreateCard(battleCardData, side, callback);
					}
					SoundManager.PlayEFXSound(BattleController.Instance.MoveSound);
				}

	            return true;
			}
        }
        return false;
    }

	public void SetAllBattleCardUIClickable(bool isClickable)
	{
		// In _enemyCardObjList
		for (int index = 0; index < _enemyCardObjList.Count; ++index)
		{
			_enemyCardObjList[index].IsClickable = isClickable;
		}

		// In _playerCardObjList
		for (int index = 0; index < _playerCardObjList.Count; ++index)
		{
			_playerCardObjList[index].IsClickable = isClickable;
		}
	}
		
	private void UpdateContentPosition()
	{
		if(_isOverflow)
		{
			if(Content.transform.localPosition.x >= ContentMaxPosX)
			{
				// at left
				if(ShiftLeftButton.gameObject.activeSelf) 
				{
					ShiftLeftButton.gameObject.SetActive(false);
				}
			}
			else 
			{
				if(!ShiftLeftButton.gameObject.activeSelf) 
				{
					ShiftLeftButton.gameObject.SetActive(true);
				}
			}

			if(Content.transform.localPosition.x <= ContentMinPosX)
			{
				// at right
				if(ShiftRightButton.gameObject.activeSelf) 
				{
					ShiftRightButton.gameObject.SetActive(false);
				}
			}
			else
			{
				if(!ShiftRightButton.gameObject.activeSelf) 
				{
					ShiftRightButton.gameObject.SetActive(true);
				}
			}
		}
		else
		{
			if(ShiftLeftButton.gameObject.activeSelf) 
				ShiftLeftButton.gameObject.SetActive(false);
			
			if(ShiftRightButton.gameObject.activeSelf) 
				ShiftRightButton.gameObject.SetActive(false);
		}
	}

	public void OnGoLeft()
	{
		if((Content.transform.localPosition.x + MinSize.x) < ContentMaxPosX)
		{
			// Can shift
			this.ContentLocalPos = new Vector3(Content.transform.localPosition.x + MinSize.x, 0.0f, 0.0f);
		}
		else
		{
			// End 
			//Debug.Log(ContentMaxPosX);

			this.ContentLocalPos = new Vector3(ContentMaxPosX, 0.0f, 0.0f);
			ShiftLeftButton.gameObject.SetActive(false);
		}
	}

	public void OnGoRight()
	{
		if((Content.transform.localPosition.x - MinSize.x) > ContentMinPosX)
		{
			//Debug.Log((Content.transform.localPosition.x - MinSize.x) + " " + ContentMinPosX);

			// Can shift
			this.ContentLocalPos = new Vector3(Content.transform.localPosition.x - MinSize.x, 0.0f, 0.0f);
		}
		else
		{
			// End 
			//Debug.Log(ContentMinPosX);

			this.ContentLocalPos = new Vector3(ContentMinPosX, 0.0f, 0.0f);
			ShiftRightButton.gameObject.SetActive(false);
		}
	}

	public void Reposition()
	{
		float num = _playerCardObjList.Count + _enemyCardObjList.Count;
		_currentWidth = (CardWidth * num) + (CardOffset * (num - 1.0f)) + (BorderWidth * 2.0f);

		/*
		Content.width = Mathf.CeilToInt(_currentWidth);
		Content.height = Mathf.CeilToInt(MinSize.y);
		*/

		int cardIndex = 0;

		if(_currentWidth <= MinSize.x)
		{
			// Not overflow

			// Player
			for (int index = 0; index < _playerCardObjList.Count; ++index) 
			{
				// Left Side
				float x_offset = (CardWidth * 0.5f) + (CardWidth * cardIndex) + (CardOffset * cardIndex) + BorderWidth;
				_playerCardObjList[index].GameObject.transform.localPosition = new Vector3(x_offset, 0, 0);

				++cardIndex;
			}

			// Enemy
			for (int index = 0; index < _enemyCardObjList.Count; ++index) 
			{
				// Right Side
				float x_offset = (CardWidth * 0.5f) + (CardWidth * index) + (CardOffset * index) + BorderWidth;
				_enemyCardObjList[index].GameObject.transform.localPosition = new Vector3(MinSize.x - x_offset, 0, 0);
			}

			this.ContentLocalPos = new Vector3(ContentMaxPosX, 0.0f, 0.0f);


			ShiftLeftButton.gameObject.SetActive(false);
			ShiftRightButton.gameObject.SetActive(false);

			_isOverflow = false;

		}
		else
		{
			// Overflow
			bool isLeft = false;

			// Player
			for (int index = 0; index < _playerCardObjList.Count; ++index) 
			{
				// Left Side
				float x_offset = (CardWidth * 0.5f) + (CardWidth * cardIndex) + (CardOffset * cardIndex) + BorderWidth;
				_playerCardObjList[index].GameObject.transform.localPosition = new Vector3(x_offset, 0, 0);

				++cardIndex;
				isLeft = true;
			}

			// Enemy
			for (int index = _enemyCardObjList.Count - 1; index >= 0; --index) 
			{
				// Left Side
				float x_offset = (CardWidth * 0.5f) + (CardWidth * cardIndex) + (CardOffset * cardIndex) + BorderWidth;
				_enemyCardObjList[index].GameObject.transform.localPosition = new Vector3(x_offset, 0, 0);

				++cardIndex;
			}

			if(isLeft)
			{
				this.ContentLocalPos = new Vector3(ContentMaxPosX, 0.0f, 0.0f);
			}
			else
			{
				this.ContentLocalPos = new Vector3(ContentMinPosX, 0.0f, 0.0f);
			}

			_isOverflow = true;
		}
	}
    #endregion
}
	