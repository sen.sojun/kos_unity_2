﻿using UnityEngine;
using System.Collections;

public class FindFirstUIManager : MonoBehaviour 
{
    private BattleManager _battleManager = null;

    public FindFirstOptionUIManager FindFirstOptionUIManager;
    public FindFirstResultUIManager FindFirstResultUIManager;
	public FindFirstPlayUIManager FindFirstPlayUIManager;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

    public void SetBattleManager(BattleManager battleManager)
    {
        _battleManager = battleManager;
    }

	public void ShowOptionUI(FindFirstOptionUIManager.OnSelectedOption callback)
    {
		FindFirstOptionUIManager.ShowUI(callback);
    }

    public void ShowResultUI(FindFirstOption.FindFirstOptionType p1OptionType, FindFirstOption.FindFirstOptionType p2OptionType, FindFirstResultUIManager.OnFinishShowCallback callback)
    {
        if (_battleManager != null)
        {
            FindFirstOption.FindFirstOptionType meOptionType;
            FindFirstOption.FindFirstOptionType enemyOptionType;

            if (_battleManager.GetPlayer(PlayerIndex.One).IsLocalPlayer())
            {
                meOptionType = p1OptionType;
                enemyOptionType = p2OptionType;
            }
            else
            {
                meOptionType = p2OptionType;
                enemyOptionType = p1OptionType;
            }

            FindFirstResultUIManager.ShowUI(meOptionType, enemyOptionType, callback);
        }
    }

	public void ShowPlayUI(bool isActivable, FindFirstPlayUIManager.OnSelectedFindFirstPlay callback = null)
	{
		this.FindFirstPlayUIManager.ShowUI(isActivable, callback);
	}

	public void ShowPlayResultUI(FindFirstPlay.FindFirstPlayType playType, FindFirstPlayUIManager.OnFinishShowPlayResult callback)
	{
		this.FindFirstPlayUIManager.ShowResult(playType, callback);
	}
}
