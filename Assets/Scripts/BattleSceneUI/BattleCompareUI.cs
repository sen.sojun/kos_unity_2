﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BattleCompareUI : MonoBehaviour 
{
	public delegate void OnFinishBattle(int atkBattleID, int targetBattleID);

	public enum BattleSide
	{
		  Attacker
		, Target
	}

	public enum ShowMode
	{
		  Text
		, Destroy
		, Slash
	}

	public struct PerformAtkAction
	{
		public BattleSide Side;
		public ShowMode Mode;
		public string PopupText;
		public bool isShow;
	};

	public struct PerformAtkStep
	{
		public List<PerformAtkAction> ActionList;
	};

	public BattleCompareCardUI PlayerCard;
	public BattleCompareCardUI EnemyCard;
	private bool _isAttackerIsLocal;

	private BattleCardData _attacker = null;
	private BattleCardData _target = null;
	private UIWidget _widget = null;

	private bool _isStartShow = false;

	private OnFinishBattle _onFinishBattle = null;

	private float _timer = 0.0f;
	private float _waitStepTime = 0.75f;

	private List<PerformAtkStep> _stepList;

	void Awake()
	{
		//ClearPopupText();
		//NGUITools.SetActive(gameObject, false);
	}
	// Use this for initialization
	/*
	void Start () {
	
	}
	*/
		
	// Update is called once per frame
	void Update () 
	{
		if(_isStartShow)
		{
			_timer += Time.deltaTime;
			if(_timer >= _waitStepTime)
			{
				_timer -= _waitStepTime;
				OnShowStep();
			}
		}
	}
		
	public void ShowUI(BattleCardData attacker, BattleCardData target, OnFinishBattle onFinishBattleCallback)
	{
        ClearPopupText();
    
		//_stepList = new List<PerformAtkStep>();
		_attacker = attacker;
		_target = target;

		// Set show fullcard value
		if (BattleManager.Instance.IsLocalPlayer (_attacker.OwnerPlayerIndex))
		{
			// Attacker is local.
			PlayerCard.SetCardData(_attacker.PlayerCardData);
			EnemyCard.SetCardData(_target.PlayerCardData);
			_isAttackerIsLocal = true;
		} 
		else
		{
			// Target is local.
			PlayerCard.SetCardData(_target.PlayerCardData);
			EnemyCard.SetCardData(_attacker.PlayerCardData);
			_isAttackerIsLocal = false;
		}

		_onFinishBattle = onFinishBattleCallback;

		/*
		Attacker.ShowUI();
		Target.ShowUI();
		*/

		NGUITools.SetActive(gameObject, true);

		BattleUIManager.RequestBringForward(gameObject, true);

		_widget = gameObject.GetComponent<UIWidget>();
		if(_widget != null)
		{
			NGUITools.AdjustDepth(_widget.panel.gameObject, 1001);
		}
			
		/*
		NGUITools.BringForward(gameObject);
		NGUITools.BringForward(gameObject.GetComponent<UIWidget>().panel.gameObject);
		*/

		_isStartShow = false;
	}

	public void SetStepList(List<PerformAtkStep> stepList)
	{
		_stepList = stepList;

		_timer = 0.0f;
		_isStartShow = true;
	}

	private void OnShowStep()
	{
		if(_stepList.Count > 0)
		{
			PerformAtkStep step = _stepList[0];

			foreach(PerformAtkAction action in step.ActionList)
			{
				switch(action.Mode)
				{
					case ShowMode.Text:	 	ShowPopupText(action.Side, action.PopupText); break;
					case ShowMode.Destroy:	ShowDestroy(action.Side, action.isShow); break;
					case ShowMode.Slash: 	ShowPopupSlash(action.Side, action.isShow); break;
				}
			}

			_stepList.RemoveAt(0);
		}	
		else
		{
			HideUI();
		}
	}
		
	public static PerformAtkAction CreateDamageAction(BattleSide side, int damage, PassiveIndex attackerPassiveIndex)
	{
		PerformAtkAction action;
		action.Side = side;
		action.Mode = ShowMode.Text;
		action.isShow = false;

		if(attackerPassiveIndex == PassiveIndex.PassiveType.Knockback)
		{
			action.PopupText = (-damage).ToString() + " " + Localization.Get("BATTLE_COMPARE_KNOCKBACK_TEXT");
		}
		else if(attackerPassiveIndex == PassiveIndex.PassiveType.Piercing)
		{
			action.PopupText = (-damage).ToString() + " " + Localization.Get("BATTLE_COMPARE_PIERCING_TEXT");
		}
		else
		{
			action.PopupText = (-damage).ToString();
		}

		return action;
	}

	public static PerformAtkAction CreateEffectTextAction(BattleSide side, string effectText)
	{
		PerformAtkAction action;
		action.Side = side;
		action.Mode = ShowMode.Text;
		action.PopupText = effectText;
		action.isShow = false;

		return action;
	}

	public static PerformAtkAction CreateDestroyAction(BattleSide side, bool isShow)
	{
		PerformAtkAction action;
		action.Side = side;
		action.Mode = ShowMode.Destroy;
		action.PopupText = "destroy";
		action.isShow = isShow;

		return action;
	}

	public static PerformAtkAction CreateSlashEffectAction(BattleSide side, bool isShow)
	{
		PerformAtkAction action;
		action.Side = side;
		action.Mode = ShowMode.Slash;
		action.PopupText = "slash";
		action.isShow = isShow;

		return action;
	}

	private void ShowPopupText(BattleSide side, string popupText)
	{
		//Debug.Log ("ShowPopupText");

		switch(side)
		{
			case BattleSide.Attacker:
			{
				if (_isAttackerIsLocal)
				{
					PlayerCard.ShowPopupText (popupText);

					//Debug.Log ("PlayerCard Slash false");
					PlayerCard.ShowSlash (false);
				} 
				else
				{
					EnemyCard.ShowPopupText(popupText);

					//Debug.Log ("EnemyCard Slash false");
					EnemyCard.ShowSlash(false);
				}
			}
			break;

			case BattleSide.Target:
			{
				if (_isAttackerIsLocal)
				{
					EnemyCard.ShowPopupText(popupText);

					//Debug.Log ("EnemyCard Slash false");
					EnemyCard.ShowSlash(false);
				} 
				else
				{
					PlayerCard.ShowPopupText (popupText);

					//Debug.Log ("PlayerCard Slash false");
					PlayerCard.ShowSlash (false);
				}
			}
			break;
		}
	}

	private void ShowPopupText(bool IsPlayerCard, string popupText)
	{
		if (IsPlayerCard)
		{
			PlayerCard.ShowPopupText (popupText);

			//Debug.Log ("PlayerCard Slash false");
			PlayerCard.ShowSlash (false);
		} 
		else
		{
			EnemyCard.ShowPopupText(popupText);

			//Debug.Log ("EnemyCard Slash false");
			EnemyCard.ShowSlash(false);
		}
	}

	private void ShowDestroy(BattleSide side, bool isShow)
	{
		switch(side)
		{
			case BattleSide.Attacker:
			{
				if (_isAttackerIsLocal)
				{
					PlayerCard.ShowDestroy(isShow);

					Debug.Log ("PlayerCard Slash false");
					PlayerCard.ShowSlash(false);
				} 
				else
				{
					EnemyCard.ShowDestroy(isShow);

					Debug.Log ("EnemyCard Slash false");
					EnemyCard.ShowSlash(false);
				}
			}
			break;

			case BattleSide.Target:
			{
				if (_isAttackerIsLocal)
				{
					EnemyCard.ShowDestroy(isShow);

					//Debug.Log ("EnemyCard Slash false");
					EnemyCard.ShowSlash(false);
				} 
				else
				{
					PlayerCard.ShowDestroy(isShow);

					//Debug.Log ("PlayerCard Slash false");
					PlayerCard.ShowSlash(false);
				}
			}
			break;
		}
	}

	private void ShowPopupSlash(BattleSide side, bool isShow)
	{
		//Debug.Log ("ShowPopupSlash");

		switch(side)
		{
			case BattleSide.Attacker:
			{
				if (_isAttackerIsLocal)
				{
					ShowPopupText (true, "");

					//Debug.Log ("PlayerCard Slash " + isShow);
					PlayerCard.ShowSlash(isShow);
				} 
				else
				{
					ShowPopupText (false, "");

					//Debug.Log ("EnemyCard Slash " + isShow);
					EnemyCard.ShowSlash(isShow);
				}
			}
			break;

			case BattleSide.Target:
			{
				if (_isAttackerIsLocal)
				{
					ShowPopupText (false, "");

					//Debug.Log ("EnemyCard Slash " + isShow);
					EnemyCard.ShowSlash(isShow);
				} 
				else
				{
					ShowPopupText (true, "");

					//Debug.Log ("PlayerCard Slash " + isShow);
					PlayerCard.ShowSlash(isShow);
				}
			}
			break;
		}
	}

	private void ClearPopupText()
	{
		ShowPopupText(BattleSide.Attacker, "");
		ShowPopupText(BattleSide.Target, "");
	}

	private void ClearPopupSlash()
	{
		PlayerCard.ShowSlash(false);
		EnemyCard.ShowSlash(false);
	}

	private void HideUI()
	{
		_isStartShow = false;
	
		ClearPopupText();
		ClearPopupSlash();

		_widget = null;
		NGUITools.SetActive(gameObject, false);

		if(_onFinishBattle != null)
		{
			_onFinishBattle.Invoke(_attacker.BattleCardID, _target.BattleCardID);
		}
	}
}

