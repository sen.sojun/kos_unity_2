﻿using UnityEngine;
using System.Collections;

public class BattleCompareCardUI : MonoBehaviour 
{
	public GameObject SlashTexture;
	public UILabel PopupLabel;
	public FullCardUI FullCard;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	/*
	void Update () 
	{
	}
	*/

	public void ShowUI()
	{
		//BattleUIManager.RequestBringForward(FullCard.gameObject);

		//NGUITools.ImmediatelyCreateDrawCalls(FullCard.gameObject);
	}

	public void SetCardData(PlayerCardData data)
	{
		FullCard.SetCardData(data);
        FullCard.RequestBringForward();
	}

	public void ShowPopupText(string showText)
	{
		PopupLabel.text = showText;
		BattleUIManager.RequestBringForward(PopupLabel.gameObject, false);
	}

	public void ShowDestroy(bool isShow)
	{
		
		if(isShow)
		{
			PopupLabel.text = Localization.Get("BATTLE_COMPARE_DESTROYED");
			//SoundManager.PlayEFXSound(BattleController.Instance.DestroySound);
			BattleUIManager.RequestBringForward(PopupLabel.gameObject, false);
		}
		else
		{
			PopupLabel.text = "";
		}
	}

	public void ShowSlash(bool isShow)
	{
		SlashTexture.SetActive(isShow);
		if(isShow)
		{
			SoundManager.PlayEFXSound(BattleController.Instance.AttackSound);
		}
	}
}
