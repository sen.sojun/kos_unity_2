﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class ProfileUI : MonoBehaviour 
{
	private string _defaultImageName = "white_10x10";

    public UISprite PlayerImageSprite;

    public UILabel PlayerNameLabel;

    public UILabel HitPointLabel;
    public UILabel HandCardLabel;
    public UILabel GoldLabel;
    public UILabel DeckCardLabel;

    public UIButton GraveyardButton;
    public UIButton RemoveZoneButton;

	private PlayerIndex _playerIndex;

	private int _gold;
	private int _hp;

	private Tween _goldTween = null;
	private Tween _hpTween = null;

	// Use this for initialization
	private void Start () 
    {
	}
	
	// Update is called once per frame
	private void Update () 
    {
	
	}

	public void SetProfile(PlayerBattleData playerBattleData)
    {
		_playerIndex = playerBattleData.PlayerIndex;

        SetPlayerName(playerBattleData.Name);
		SetPlayerImage(playerBattleData.ImageName);
		//SetPlayerImage("01");                        //delete after use
        SetHitPoint(playerBattleData.HitPoint);
        SetHand(playerBattleData.HandCount);
        SetGold(playerBattleData.Gold);
        SetDeckCard(playerBattleData.Deck.Count);
    }

	public void SetProfile(PlayerBattleData playerBattleData,string playerType)
    {
        _playerIndex = playerBattleData.PlayerIndex;

        SetPlayerName(playerBattleData.Name);
        SetPlayerImage(playerBattleData.ImageName);
        SetPlayerImage("01");                        //delete after use
        SetHitPoint(playerBattleData.HitPoint);
        SetHand(playerBattleData.HandCount);
        SetGold(playerBattleData.Gold);
        SetDeckCard(playerBattleData.Deck.Count);
    }

		
	public void SetPlayerImage(string imageName)
	{
        string spriteName = "";
        switch(imageName)
        {
            case "01":
            {
                // Davox
				spriteName = "NPC_Davox_b01";
            }
            break;
            
            case "02":
            {
                // Ethan
                spriteName = "NPC_Ethan_b01";
            }
            break;
            
            case "03":
            {
                // Lakuras
                spriteName = "NPC_Lakuras_b01";
            }
            break;
            
            case "04":
            {
                // Seraph
                spriteName = "NPC_Seraph_b01";
            }
            break;
            
            case "05":
            {
                // Marron
                spriteName = "NPC_Marron_b01";
            }
            break;
            
            case "06":
            {
                // Kano
                spriteName = "NPC_Kano_b01";
            }
            break;

            case "07":
            {
                // The Spector
                spriteName = "07";
            }
            break;

            case "08":
            {
                // Psychicsoldier
                spriteName = "08";
            }
            break;

            case "09":
            {
                // Mutantsoldier
                spriteName = "09";
            }
            break;

            case "10":
            {
                // Angelofmind
                spriteName = "10";
            }
            break;

            case "11":
            {
                // Lightinfantry
                spriteName = "11";
            }
            break;

            case "12":
            {
                // Kanopet
                spriteName = "12";
            }
            break;



        
            default:
            {
                spriteName = "NPC_Spector_b01";
            }
            break;
        }
    
		if(PlayerImageSprite.atlas.GetSprite(spriteName) != null)
		{
			PlayerImageSprite.spriteName = spriteName;
		}
		else
		{
			PlayerImageSprite.spriteName = _defaultImageName;
		}
	}

    public void SetPlayerName(string playerName)
    {
		PlayerNameLabel.text = playerName.ToUpper();
    }

    public void SetHitPoint(int hitPoint)
    {
		if(_hp != hitPoint)
		{
			_hp = hitPoint;
			HitPointLabel.text = hitPoint.ToString();

			if(_hpTween == null)
			{
				_hpTween = HitPointLabel.transform.DOShakeScale(0.5f);
				_hpTween.SetAutoKill(false);
				_hpTween.Play();
			}
			else
			{
				_hpTween.Restart();
			}
		}
    }

    public void SetHand(int handCardAmount)
    {
        HandCardLabel.text = handCardAmount.ToString();
    }

    public void SetGold(int goldCoin)
    {
		if(_gold != goldCoin)
		{
			_gold = goldCoin;
			GoldLabel.text = string.Format("{0:n0}", _gold);

			if(_goldTween == null)
			{
				_goldTween = GoldLabel.transform.DOShakeScale(0.5f);
				_goldTween.SetAutoKill(false);
				_goldTween.Play();
			}
			else
			{
				_goldTween.Restart();
			}
		}
    }

    public void SetDeckCard(int deckCardAmount)
    {
        DeckCardLabel.text = deckCardAmount.ToString();
    }

    public void OnClickGraveyardButton()
    {
        //Debug.Log("Graveyard Clicked");

		BattleManager.Instance.ShowGraveyard(_playerIndex);
    }

    public void OnClickRemoveButton()
    {
        //Debug.Log("RemoveZone Clicked");

		BattleManager.Instance.ShowRemoveZone(_playerIndex);
    }
}
