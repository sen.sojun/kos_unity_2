﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SelectInitCardUIManager : MonoBehaviour 
{
	public string SelectBaseHeaderText = "Select Base Card";
	public string SelectLeaderHeaderText = "Select Leader Card";

	public SelectCardUI SelectBaseCardUI;
	public SelectCardUI SelectLeaderCardUI;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	/*
	void Update () 
	{
	
	}
	*/

	public void ShowSelectBaseUI(List<UniqueCardData> baseCardList, SelectCardUI.OnConfirmSelectCard onConfirmSelectCardCallback)
	{
		this.SelectBaseCardUI.ShowUI(baseCardList, onConfirmSelectCardCallback, SelectBaseHeaderText);
	}

	public void ShowSelectLeaderUI(List<UniqueCardData> leaderCardList, SelectCardUI.OnConfirmSelectCard onConfirmSelectCardCallback)
	{
		this.SelectLeaderCardUI.ShowUI(leaderCardList, onConfirmSelectCardCallback, SelectLeaderHeaderText);
	}
}
