﻿using UnityEngine;
using System.Collections;

public class FindFirstPlayUIManager : MonoBehaviour 
{
	public delegate void OnSelectedFindFirstPlay(FindFirstPlay.FindFirstPlayType playType);
	public delegate void OnFinishShowPlayResult();

	private bool __isActivable = false;
	private bool _isShow = false;

	private bool _isShowResult = false;
	private float _timer = 0.0f;
	private float _showResultTime = 1.5f;

	private OnSelectedFindFirstPlay _onSelectedFindFirstPlay = null;
	private OnFinishShowPlayResult _onFinishShowPlayResult = null;

	private bool _isActivable
	{
		get { return __isActivable; }
		set 
		{
			__isActivable = value;
			SetButtonActive();
		}
	}

	public UIButton PlayFirstButton;
	public UIButton DrawFirstButton;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (_isShow && _isShowResult)
		{
			_timer += Time.deltaTime;
			if (_timer >= _showResultTime)
			{
				HideUI(); 
			}
		}
	}

	private void SetButtonActive()
	{
		PlayFirstButton.isEnabled = _isActivable;
		DrawFirstButton.isEnabled = _isActivable;
	}

	public void ShowUI(bool isActivable, OnSelectedFindFirstPlay onSelectedFindFirstPlay = null)
	{
		_isActivable = isActivable;

		if(BattleManager.Instance.IsTutorial)
		{
			DrawFirstButton.isEnabled = false;
		}

		_onSelectedFindFirstPlay = onSelectedFindFirstPlay;

		StartShowUI();
	}
		
	private void StartShowUI()
	{
		NGUITools.SetActiveSelf(PlayFirstButton.gameObject, true);
		NGUITools.SetActiveSelf(DrawFirstButton.gameObject, true);

		if(BattleManager.Instance.IsTutorial)
		{
			Transform glow = PlayFirstButton.transform.Find("Glow");
			glow.gameObject.SetActive(true);

			Transform arrow = PlayFirstButton.transform.Find("Arrow");
			arrow.gameObject.SetActive(true);
		}
		else
		{
			Transform glow = PlayFirstButton.transform.Find("Glow");
			glow.gameObject.SetActive(false);

			Transform arrow = PlayFirstButton.transform.Find("Arrow");
			arrow.gameObject.SetActive(false);
		}

		NGUITools.SetActive(gameObject, true);
		BattleUIManager.RequestBringForward(this.gameObject);

		_isShow = true;
	}

	public void OnSelectPlay(FindFirstPlay.FindFirstPlayType playType)
	{
		//Debug.Log(playType.ToString());
		SoundManager.PlayEFXSound(BattleController.Instance.ButtonSound);
		_isActivable = false;

		if(_onSelectedFindFirstPlay != null)
		{
			_onSelectedFindFirstPlay(playType);
		}
	}

	public void ShowResult(FindFirstPlay.FindFirstPlayType resultPlayType, OnFinishShowPlayResult _onFinishShowPlayResultCallback)
	{
		if(_isShow)
		{
			_onFinishShowPlayResult = _onFinishShowPlayResultCallback;

			if(_isActivable) _isActivable = false;

			switch(resultPlayType)
			{
				case FindFirstPlay.FindFirstPlayType.PlayFirst:
				{
					NGUITools.SetActive(PlayFirstButton.gameObject, true);
					NGUITools.SetActive(DrawFirstButton.gameObject, false);

					if(BattleManager.Instance.IsTutorial)
					{
						Transform glow = PlayFirstButton.transform.Find("Glow");
						glow.gameObject.SetActive(false);

						Transform arrow = PlayFirstButton.transform.Find("Arrow");
						arrow.gameObject.SetActive(false);
					}
				}
				break;

				case FindFirstPlay.FindFirstPlayType.DrawFirst:
				{
					NGUITools.SetActive(PlayFirstButton.gameObject, false);
					NGUITools.SetActive(DrawFirstButton.gameObject, true);
				}
				break;
			}

			_timer = 0.0f;
			_isShowResult = true;
		}
		else
		{
			Debug.LogError("FindFirstChooseUIManager/ShowResult: Error try to show result while not show UI.");
		}
	}

	private void HideUI()
	{
		StartHideUI();
	}

	private void StartHideUI()
	{
		if(_onFinishShowPlayResult != null)
		{
			_onFinishShowPlayResult.Invoke();
		}

		NGUITools.SetActive(gameObject, false);
		_isShow = false;
		_isShowResult = false;
	}
}
