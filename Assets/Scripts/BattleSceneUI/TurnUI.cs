﻿using UnityEngine;
using System.Collections;

public class TurnUI : MonoBehaviour 
{
    #region Private Properties
    private BattleManager _battleManager = null;
    #endregion

    public UILabel TurnLabel;

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
    /*
	void Update () 
    {
	
	}
    */

    #region OnDestroy
    void OnDestory()
    {
        if (_battleManager != null)
        {
            _battleManager.UnSubscribeTurnIndexUpdateEvent(SetTurn);
        }
    }
    #endregion

    public void SetBattleManager(BattleManager battleManager)
    {
        _battleManager = battleManager;
        _battleManager.SubscribeTurnIndexUpdateEvent(SetTurn, true);
    }

    public void SetTurn(int turnIndex)
    {
		TurnLabel.text = string.Format(Localization.Get("BATTLE_TURN_INDEX"), turnIndex);
    }
}
