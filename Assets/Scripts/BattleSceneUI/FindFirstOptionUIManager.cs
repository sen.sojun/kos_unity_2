﻿using UnityEngine;
using System.Collections;

public class FindFirstOptionUIManager : MonoBehaviour 
{
    public delegate void OnSelectedOption(FindFirstOption.FindFirstOptionType optionType);

    private OnSelectedOption OnSelectedOptionCallback = null;

    private FindFirstOption.FindFirstOptionType _currentOptionType;
    private bool isShow = false;

    public bool IsShow
    {
        get { return isShow; }
    }

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
		
	}

    public void ShowUI(OnSelectedOption onSelectedOptionCallback)
    {
        SetOnSelectedOptionCallback(onSelectedOptionCallback);
        StartShowUI();
    }

    private void StartShowUI()
    {
		NGUITools.SetActiveSelf(gameObject, true);
		BattleUIManager.RequestBringForward(this.gameObject);

        isShow = true;
    }

    private void HideUI()
    {
        StartHideUI();
    }

    private void StartHideUI()
    {
		NGUITools.SetActive(gameObject, false);
        isShow = false;
    }

    private void SetOnSelectedOptionCallback(OnSelectedOption callback)
	{
		OnSelectedOptionCallback = callback;
	}

	private void ClearSelectedOptionCallback()
	{
		OnSelectedOptionCallback = null;
	}

	public void OnSelectFindFirstOption(FindFirstOption.FindFirstOptionType optionType)
	{
		HideUI();
		_currentOptionType = optionType;

		//Debug.Log("OnSelectOption " + _currentOptionType.ToString());

		if(OnSelectedOptionCallback != null)
		{
			OnSelectedOptionCallback(optionType);
		}
	}
}
