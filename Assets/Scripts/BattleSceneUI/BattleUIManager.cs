﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GamePlayerUI;

#region DisplayProfileIndex Enum
public enum DisplayPlayerIndex
{
    Player
  , Enemy
}
#endregion

public class BattleUIManager : MonoBehaviour 
{
    private BattleManager _battleManager;

	#region Public Properties
    public GameObject BattleManagerController;

	public UIButton NextPhaseButton;
	public UISprite NextPhaseArrow;
	public BattleBGUI BattleBGUI;
	public UIPanel PopupPanel;

	public ProfileUIManager ProfileUIManager;
	public PhaseUIManager PhaseUIManager;
	public TurnUI TurnUIManager;

	public HandUIManager HandUIManager;
	public FieldUIManager FieldUIManager;

    public CardPopupMenuUI CardPopupMenuUI;
    public YesNoPopupUI YesNoPopupUI;
	public FullCardInfoUI FullCardInfoUI;
	public MoveCardUI MoveCardUI;

	public SelectCardPageUI SelectCardPageUI;
	public SelectBattleCardUI SelectBattleCardUI;

	public ShowPhasePopupUI ShowPhasePopupUI;
	public GameResultUI GameResultUI;
	public GameSoloResult GameSoloResult;  //+vit01062017
	public BattleCompareUI BattleCompareUI;
	public ShowUseAbilityUI ShowUseAbilityUI;

	public AbilitySelectUI AbilitySelectUI;
	public SuggestText SuggestText;

	public PopupBoxUI PopupBoxUI;

    //------------

    // RPS
    public RPSSelectUI RPSSelectUI;
    public RPSWaitUI RPSWaitUI;
    public RPSResultUI RPSResultUI;

    // Play Order
    public PlayOrderSelectUI PlayOrderSelectUI;
    public PlayOrderResultUI PlayOrderResultUI;
    
    // Select Card
    public SelectCardListUI SelectCardListUI;
    
    // Wait UI
    public WaitingUI WaitingUI;
    
    // Loading UI
    public LoadingUI LoadingUI;
    
    //------------

	//add vit
//	public GameObject BattlePanel;
//	public GameObject PopUpPanel;
//	public GameObject CardListPanel;
//	public TutorialWindow TutorialWindow;


//	public List<Texture> TutorialBaseLeaderImageList;
//	//public List<Texture> TutorialLeaderImageList;
//	public List<Texture> TutorialResouceImageList;
//	public List<Texture> TutorialHandImageList;
//	public List<Texture> TutorialEventImageList;
//	public List<Texture> TutorialFieldImageList;
//	public List<Texture> TutorialFightImageList;
//	public List<Texture> TutorialZoneImageList;
//	public List<Texture> TutorialPhaseImageList;
	//end add vit

	#endregion

	#region Start
	// Use this for initialization
	void Start () 
    {
	}
	#endregion

	void Init()
	{
		
	}

	#region Update
	// Update is called once per frame
	void Update () 
	{
	
	}
	#endregion
		
	#region Methods
    public void SetBattleManager(BattleManager battleManager)
    {
        _battleManager = battleManager;

		ProfileUIManager.SetBattleManager(_battleManager);   
		PhaseUIManager.SetBattleManager(_battleManager);
		TurnUIManager.SetBattleManager(_battleManager);

		HandUIManager.SetBattleManager(_battleManager);
		FieldUIManager.SetBattleManager(_battleManager);

		MoveCardUI.SetBattleManager(_battleManager);
    }
	#endregion

//	//add by vit27052016
//    #region ShowTutorialFirstTime
//	public void ShowTutorial(List<Texture> tutorialImageList)
//	{
//		Debug.Log (tutorialImageList.Count);
//		TutorialWindow.enabled = true;
//		TutorialWindow.ShowTutorial (tutorialImageList);
//	}
//	#endregion
//	//end add by vit27052016


	#region FindFirst Methods
    public void ShowSelectRPSUI(RPSSelectUI.OnClickRPSOption onSelectRPS)
	{
        this.RPSSelectUI.ShowUI(onSelectRPS);
	}

    public void ShowWaitRPSUI(RPSType selectedRPS)
    {
        this.RPSWaitUI.ShowUI(selectedRPS);
    }

    public void ShowResultRPSUI(RPSType selectedRPS_me, RPSType selectedRPS_enemy, RPSResultUI.OnFinishShowResult onFinishShowCallback)
    {
        this.RPSWaitUI.HideUI();

        //Debug.Log("Show FindFirstResult UI.");
        this.RPSResultUI.ShowUI(selectedRPS_me, selectedRPS_enemy, onFinishShowCallback);
    }

    public void ShowPlayOrderSelectUI(bool isActivable, PlayOrderSelectUI.OnClickPlayOrderOption onSelectFindFirstPlayCallback = null)
	{
		//Debug.Log("Show FindFirstPlay UI.");
        this.PlayOrderSelectUI.ShowUI(isActivable, onSelectFindFirstPlayCallback);
	}

    public void ShowPlayOrderResultUI(PlayOrderType playType, PlayOrderResultUI.OnFinishShowResult onFinishShowPlayResultCallback)
	{
        this.PlayOrderSelectUI.HideUI();

		//Debug.Log("Show FindFirstPlayResult UI.");
        this.PlayOrderResultUI.ShowUI(playType, onFinishShowPlayResultCallback);
	}
	#endregion
    
    public void ShowSelectCardListUI(string headerTextKey, List<UniqueCardData> cardList, SelectCardListUI.OnConfirmSelectCard onSelectCallback)
    {
        SelectCardListUI.ShowUI(headerTextKey, cardList, onSelectCallback);
    }
    
    public void ShowWaitUI()
    {
    }

    public void ShowHandCardPopupMenuUI(List<CardPopupMenuUI.CardPopupActionType> actionList, HandCardUI handCardUI, CardPopupMenuUI.OnSelectActionCallback onSelectCallback)
    { 

    }

    public void ShowBattleCardPopupMenuUI(List<CardPopupMenuUI.CardPopupActionType> actionList, BattleCardUI battleCardUI, CardPopupMenuUI.OnSelectActionCallback onSelectCallback)
    {

    }

    public void ShowYesNoPopupUI(string descriptionText, YesNoPopupUI.OnSelectYes onYes, YesNoPopupUI.OnSelectNo onNo, bool isHaveTimeout)
    {
        //Debug.Log("Show YesNo UI. " + descriptionText);
        this.YesNoPopupUI.ShowUI(descriptionText, onYes, onNo, isHaveTimeout);
    }

	public void ShowFullCardInfoUI(UniqueCardData uniqueCardData)
	{
		//Debug.Log("Show FullCard Info UI.");
		this.FullCardInfoUI.ShowUI(uniqueCardData);
	}

	public void ShowMoveCardUI(BattleCardData battleCardData, bool isHaveTimeout, bool isCanCancel, MoveCardUI.OnSelectedMove onSelectedMove, MoveCardUI.OnCancelMove onCancelMove)
	{
		//Debug.Log("Show Move Card UI.");
		this.MoveCardUI.ShowUI(battleCardData, isHaveTimeout, isCanCancel, onSelectedMove, onCancelMove);
	}

	public bool IsShowMoveCardUI()
	{
		//Debug.Log("Show Move Card UI.");
		return this.MoveCardUI.IsShowUI;
	}

	public void ShowSelectCardPageUI(string topicText, string headerText, List<UniqueCardData> cardDataList, int selectNum, SelectCardPageUI.OnConfirmEvent onConfirmCallback, SelectCardPageUI.OnCancelEvent onCancelCallback, bool isHaveTimeout, bool isCanCancel = true)
	{
		//Debug.Log("Show Select Card Page UI.");
        if(cardDataList == null || cardDataList.Count <= 0)
        {
            // Empty
            if(!isCanCancel)
            {
                onConfirmCallback.Invoke(new List<int>());
            }
            else
            {
                onCancelCallback();
            }
            return;
        }
        
		this.SelectCardPageUI.ShowUI(
			  topicText
			, headerText
			, cardDataList
			, selectNum
			, onConfirmCallback
			, onCancelCallback
            , isHaveTimeout
			, isCanCancel
		);
	}

	public void ShowSelectBattleCardUI(SelectBattleCardUI.OnCancelSelectTarget onCancel)
	{
		this.SelectBattleCardUI.ShowUI(onCancel);
	}

	public void ShowPhaseUI(bool isMe, DisplayBattlePhase displayBattlePhase, ShowPhasePopupUI.OnFinishShowEvent onFinishShowCallback)
	{
		//Debug.Log("Show Phase Popup UI.");
		this.ShowPhasePopupUI.ShowUI(isMe, displayBattlePhase, onFinishShowCallback);
	}

	public void ShowGameResult(GameResultUI.GameResultType resultType, int winCount, int rewardSilverCoin, PlayerCardData rewardCard, GameResultUI.OnChooseMainMenu onMainMenu, GameResultUI.OnChooseRetry onRetry)
	{
		//Debug.Log("Show Game Result UI.");			
		this.GameResultUI.ShowUI(resultType, winCount, rewardSilverCoin, rewardCard, onMainMenu, onRetry);
	}
    
    public void ShowPVPResult(GameResultUI.GameResultType resultType, int rewardSilverCoin, GameResultUI.OnChooseMainMenu onMainMenu)
    {
        //Debug.Log("Show Game PVP Result UI.");            
        this.GameResultUI.ShowPVPUI(resultType, rewardSilverCoin, onMainMenu);
    }

	public void ShowGameSoloResult(GameResultUI.GameResultType resultType,
		int exp, 
		int diamond,
		int newLevel, 
		bool isLevelUp,
		GameSoloResult.OnChooseDone onMainMenu)
	{

		//		resultType
		//		, exp
		//		, diamond
		//		, newLv
		//		, isLevelUp
		//		, GoToMainMenu
		//Debug.Log("Show Game Result UI.");			
		this.GameSoloResult.ShowUI(resultType, exp, diamond, newLevel,isLevelUp, onMainMenu);
	}

	public void ShowAbilitySelectUI(BattleCardData battleCardData)
	{
		//Debug.Log("Show Ability Select UI.");
		this.AbilitySelectUI.ShowUI(battleCardData);
	}

	public void ShowLoading(bool isShow)
	{
		if(isShow)
		{
			this.LoadingUI.ShowUI();
		}
		else
		{
			this.LoadingUI.HideUI();
		}
	}

    public void ShowWaiting(bool isShow, bool isKeepOnTop = false)
    {
        if(isShow)
        {
            this.WaitingUI.ShowUI(isKeepOnTop);
        }
        else
        {
            this.WaitingUI.HideUI();
        }
    }

	public void ShowNextPhaseArrow(bool isShow)
	{
		NextPhaseArrow.gameObject.SetActive(isShow);
		if(isShow)
		{
			BattleUIManager.RequestBringForward(NextPhaseArrow.gameObject, false);
		}
	}

	#region BattleCardUI Properties
	public enum BattleCardUIClickState
	{
		  Normal
		, SelectTarget
	}
		
	BattleCardUIClickState _battleCardUIClickState = BattleCardUIClickState.Normal;
	#endregion

	#region BattleCardUI Methods
	/*
	public void OnClickBattleCard(BattleCardUI battleCardUI)
	{
		switch(_battleCardUIClickState)
		{
			case BattleCardUIClickState.Normal:
			{
				_battleManager.OnClickBattleCard(battleCardUI);
			}
			break;

			case BattleCardUIClickState.SelectTarget:
			{
				
			}
			break;
		}
	}
	*/

	#endregion

	public static void RequestBringForward(GameObject go, bool isIncludeParentPanel = true)
	{
		if(go != null)
		{
			//Debug.Log(go.name);

			UIPanel panel = go.GetComponent<UIPanel>();
			if(panel != null)
			{
				// Panel
				//Debug.Log("Panel");

				NGUITools.BringForward(go);
			}
			else
			{
				// Widget
				panel = NGUITools.FindInParents<UIPanel>(go);

				UIWidget[] widgets = go.GetComponentsInChildren<UIWidget>(true);

				for (int i = 0, imax = widgets.Length; i < imax; ++i)
				{
					UIWidget w = widgets[i];

					// init widget panel.
					if (w.panel == null || w.panel != panel)
					{
						w.panel = panel;

						//if(!w.gameObject.activeSelf) continue;

						NGUITools.MarkParentAsChanged(w.gameObject);
					}
				}

				/*
				{
					UIWidget widget = go.GetComponent<UIWidget>();

					if(widget.panel == null) 
					{
						//Debug.Log("Null Panel");

						//panel = NGUITools.FindInParents<UIPanel>(go);

						UIWidget[] parentsWidget = go.GetComponentsInParent<UIWidget>(true);

						if(parentsWidget.Length > 0)
						{
							bool isFound = false;
							foreach (UIWidget parentWidget in parentsWidget) 
							{
								if(parentWidget.panel != null)
								{
									widget.panel = parentWidget.panel;
									isFound = true;
									break;
								}
							}

							if(!isFound)
							{
								UIPanel[] parentsPanel = widget.GetComponentsInParent<UIPanel>(true);
								if(parentsPanel.Length > 0)
								{
									widget.panel = parentsPanel[0];
								}
							}
						}
					}
				}
				*/

				if(isIncludeParentPanel) 
				{
					//Debug.Log("Parent Panel: " + widget.panel.name);

					UIWidget widget = go.GetComponent<UIWidget>();
					if(widget != null)
					{
						if(widget.panel != null)
						{
							NGUITools.BringForward(widget.panel.gameObject);
						}
						else
						{
							//Debug.LogError("Not found panel.");
						}
					}
					/*
					if(widget.panel != null && NGUITools.GetActive(widget.panel.gameObject))
					{
						//NGUITools.ImmediatelyCreateDrawCalls(widget.panel.gameObject);
					}
					*/
				}

				//Debug.Log("GO: " + go.name);
					
				NGUITools.BringForward(go);

				/*
				if(go != null)
				{
					//NGUITools.ImmediatelyCreateDrawCalls(go);
				}
				*/
			}
		}
	} 
}
