﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FullCardInfoUI : MonoBehaviour 
{
	private UniqueCardData _uniqueCardData = null;
	private BattleCardData _battleCardData = null;
	private bool _IsShow = false;

	public FullCardUI FullCardUI;

	public List<FullCardInfoButton> ButtonList;

	public bool IsShow {get { return _IsShow; } }

	// Use this for initialization
	void Start () 
	{
		//NGUITools.SetActive(gameObject, false);
	}
	
	// Update is called once per frame
	/*
	void Update () 
	{
	}
	*/

	#region Methods
	public void ShowUI(UniqueCardData uniqueCardData)
	{
		_uniqueCardData = uniqueCardData;
		_battleCardData = null;

		FullCardUI.SetCardData(uniqueCardData.PlayerCardData);

		StartShowUI();
	}

	public void ShowUI(BattleCardData battleCardData)
	{
		_uniqueCardData = null;
		_battleCardData = battleCardData;

		FullCardUI.SetCardData(battleCardData.PlayerCardData);

		StartShowUI();
	}

	private void StartShowUI()
	{			
		if(_uniqueCardData != null)
		{
			List<CardPopupMenuUI.CardPopupActionType> actionList;
			bool isActionable = BattleManager.Instance.GetHandCardActionableList(_uniqueCardData.UniqueCardID, out actionList);

			int buttonIndex = 0;
			if(isActionable)
			{
				for(int index = 0; index < actionList.Count; ++index)
				{
					if(actionList[index] != CardPopupMenuUI.CardPopupActionType.Info)
					{
						FullCardInfoButton button = ButtonList[buttonIndex++];	
						button.ActionType = actionList[index];
						button.SetButtonLabel(GetActionText(actionList[index]));
						button.SetInteractable(true);
						NGUITools.SetActive(button.gameObject, true);
					}
				}
			}

			// Back Button
			if (buttonIndex < ButtonList.Count)
			{
				FullCardInfoButton backButton = ButtonList [buttonIndex++];	
				backButton.ActionType = CardPopupMenuUI.CardPopupActionType.Back;
				backButton.SetButtonLabel (GetActionText(CardPopupMenuUI.CardPopupActionType.Back));
				backButton.SetInteractable (true);
				NGUITools.SetActive (backButton.gameObject, true);
			}

			for(int index = buttonIndex; index < ButtonList.Count; ++index)
			{
				NGUITools.SetActive(ButtonList[index].gameObject, false);
			}

		}
		else if(_battleCardData != null)
		{
			List<CardPopupMenuUI.CardPopupActionType> actionList;
			bool isActionable = BattleManager.Instance.GetBattleCardActionableList(_battleCardData.BattleCardID, out actionList);

			int buttonIndex = 0;
			if(isActionable)
			{
				for(int index = 0; index < actionList.Count; ++index)
				{
					if(actionList[index] != CardPopupMenuUI.CardPopupActionType.Info)
					{
						FullCardInfoButton button = ButtonList[buttonIndex++];	
						button.ActionType = actionList[index];
						button.SetButtonLabel(GetActionText(actionList[index]));
						button.SetInteractable(true);
						NGUITools.SetActive(button.gameObject, true);
					}
				}
			}

			// Back Button
			if (buttonIndex < ButtonList.Count)
			{
				FullCardInfoButton backButton = ButtonList [buttonIndex++];	
				backButton.ActionType = CardPopupMenuUI.CardPopupActionType.Back;
				backButton.SetButtonLabel (GetActionText(CardPopupMenuUI.CardPopupActionType.Back));
				backButton.SetInteractable (true);
				NGUITools.SetActive (backButton.gameObject, true);
			}

			for(int index = buttonIndex; index < ButtonList.Count; ++index)
			{
				NGUITools.SetActive(ButtonList[index].gameObject, false);
			}
		}
		else
		{
			Debug.LogError("FullCardInfoUI/StartShowUI: battleCardData and uniqueCardData both null.");
			return;
		}

		//NGUITools.SetActiveSelf(gameObject, true);
		NGUITools.SetActive(gameObject, true);
		BattleUIManager.RequestBringForward(gameObject);
		_IsShow = true;

		FullCardUI.RequestBringForward();
	}

	private string GetActionText(CardPopupMenuUI.CardPopupActionType actionType)
	{
		string text = "";
		switch (actionType)
		{
			case CardPopupMenuUI.CardPopupActionType.Info: 		text = Localization.Get("BATTLE_CARD_POPUP_ACTION_INFO"); break;
			case CardPopupMenuUI.CardPopupActionType.Summon:	text = Localization.Get("BATTLE_CARD_POPUP_ACTION_SUMMON"); break;
			case CardPopupMenuUI.CardPopupActionType.Facedown: 	text = Localization.Get("BATTLE_CARD_POPUP_ACTION_FACEDOWN"); break;
			case CardPopupMenuUI.CardPopupActionType.Equip: 	text = Localization.Get("BATTLE_CARD_POPUP_ACTION_EQUIP"); break;
			case CardPopupMenuUI.CardPopupActionType.Use: 		text = Localization.Get("BATTLE_CARD_POPUP_ACTION_USE"); break;
			case CardPopupMenuUI.CardPopupActionType.Discard: 	text = Localization.Get("BATTLE_CARD_POPUP_ACTION_DISCARD"); break;
			case CardPopupMenuUI.CardPopupActionType.Attack: 	text = Localization.Get("BATTLE_CARD_POPUP_ACTION_ATTACK"); break;
			case CardPopupMenuUI.CardPopupActionType.Move: 		text = Localization.Get("BATTLE_CARD_POPUP_ACTION_MOVE"); break;
			case CardPopupMenuUI.CardPopupActionType.Ability: 	text = Localization.Get("BATTLE_CARD_POPUP_ACTION_ABILITY"); break;
			case CardPopupMenuUI.CardPopupActionType.Back: 		text = Localization.Get("BATTLE_CARD_POPUP_ACTION_BACK"); break;

			case CardPopupMenuUI.CardPopupActionType.None:
			default:
				text = Localization.Get("BATTLE_CARD_POPUP_ACTION_NONE"); break;
		}

		return text;
	}

	public void HideUI()
	{
		_IsShow = false;
		NGUITools.SetActive(gameObject, false);
	}

	public bool IsShowCard(string playerCardID)
	{
		if (IsShow)
		{
			if (_uniqueCardData != null && string.Compare (_uniqueCardData.PlayerCardData.PlayerCardID, playerCardID) == 0)
			{
				//Debug.Log ("Info HandCard");
				return true;
			}

			if (_battleCardData != null && string.Compare (_battleCardData.PlayerCardData.PlayerCardID, playerCardID) == 0)
			{
				//Debug.Log ("Info BattleCard");
				return true;
			}
		}

		return false;
	}
    
    public void CloseUI()
    {
        HideUI();
    }
	#endregion

	#region Button Event
	public void OnClickButton(CardPopupMenuUI.CardPopupActionType actionType)
	{
		if (actionType != CardPopupMenuUI.CardPopupActionType.Back)
		{

			if (_uniqueCardData != null)
			{
				BattleManager.Instance.OnActionHandCard (
					actionType
				, _uniqueCardData.UniqueCardID
				);
			} else if (_battleCardData != null)
			{
				BattleManager.Instance.OnSelectActionBattleCard (
					actionType
				, _battleCardData.BattleCardID
				);
			}
		}

		HideUI();
	}
	#endregion
}
