﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(UIWidget))]
public class SelectCardUI : MonoBehaviour 
{
	public delegate void OnConfirmSelectCard(UniqueCardData selectedPlayerCard);

	private List<UniqueCardData> _cardDataList;
	private List<SelectCardButton> _selectCardButtonList;
	private int _selectedIndex = -1;

	private OnConfirmSelectCard _onConfirmSelectCard;

	public UIScrollView ScrollView;
	public UIGrid UIGrid;
	public UILabel HeaderLabel;
	public FullCardUI FullCardPreview;

	public GameObject SelectCardButtonPrefab;

	// Use this for initialization
	void Start () 
	{
		/*
		List<PlayerCardData> _stashList;
		bool isSuccess = PlayerSave.GetPlayerCardStash(out _stashList);

		if(isSuccess)
		{
			SetCardDataList(_stashList);
		}
		*/
	}
	
	// Update is called once per frame
	/*
	void Update () 
	{
	}
	*/

	public void OnEnable()
	{
		ScrollView.ResetPosition();
		//ScrollView.UpdatePosition();
	}

	public void ShowUI(List<UniqueCardData> cardDataList, OnConfirmSelectCard callback, string headerText = "Select Card")
	{
		//Debug.Log("Deck:" + cardDataList.Count.ToString());

		SetCardDataList(cardDataList);
		SetHeaderLabel(headerText);
		_onConfirmSelectCard = callback;

		NGUITools.SetActive(gameObject, true);

		if(BattleManager.Instance.IsTutorial)
		{
			Transform confirmButton = transform.Find("ConfirmButton");
			if(confirmButton != null)
			{
				Transform arrow = confirmButton.Find("Arrow");
				if(arrow != null)
				{
					arrow.gameObject.SetActive(true);
				}
			}
		}
		else
		{
			Transform confirmButton = transform.Find("ConfirmButton");
			if(confirmButton != null)
			{
				Transform arrow = confirmButton.Find("Arrow");
				if(arrow != null)
				{
					arrow.gameObject.SetActive(false);
				}
			}
		}

		BattleUIManager.RequestBringForward(this.gameObject);
	}

	private void SetHeaderLabel(string headerText)
	{
		HeaderLabel.text = headerText;
	}

	private void SetCardDataList(List<UniqueCardData> cardDataList)
	{
		_selectedIndex = -1;
		_cardDataList = new List<UniqueCardData>(cardDataList);

		if(_selectCardButtonList != null)
		{
			if(_selectCardButtonList.Count > 0)
			{
				//Debug.Log("Destroy object");
				while(_selectCardButtonList.Count > 0)
				{
					GameObject obj = _selectCardButtonList[0].gameObject;
					_selectCardButtonList.RemoveAt(0);
					Destroy(obj);
				}
			}
			_selectCardButtonList.Clear();
		}
		else
		{
			_selectCardButtonList = new List<SelectCardButton>();
		}

		GenerateDisplayList();
	}

	private void GenerateDisplayList()
	{
		ScrollView.ResetPosition();
		ScrollView.UpdatePosition();

		for(int index = 0; index < _cardDataList.Count; ++index)
		{
			GameObject selectCardButtonObj = Instantiate(SelectCardButtonPrefab) as GameObject;
			SelectCardButton selectCardButton = selectCardButtonObj.GetComponent<SelectCardButton>();

			// Init value
			string cardName = "";
			switch (LocalizationManager.CurrentLanguage)
			{
				case LocalizationManager.Language.Thai:
				{
					cardName = _cardDataList[index].PlayerCardData.Name_TH;
				}
				break;

				case LocalizationManager.Language.English:
				default:
				{
					cardName = _cardDataList[index].PlayerCardData.Name_EN;
				}
				break;
			}

			selectCardButton.SetCardName(cardName);
			selectCardButton.SetIndex(index);
			selectCardButton.SetHighlight(false);
			selectCardButton.SetSelectCardUI(this);

			// Set hierarchy
			selectCardButton.transform.parent = UIGrid.transform;
			selectCardButton.transform.localScale = new Vector3(1, 1, 1);

			_selectCardButtonList.Add(selectCardButton);
		}

		this.UIGrid.transform.localPosition = Vector3.zero;
			
		this.UIGrid.Reposition();

		// Init at zero index.
		OnSelectCard(0);
	}

	public void OnSelectCard(int index)
	{
		//Debug.Log("OnSelectCard:" + index.ToString());

		if(_selectCardButtonList.Count > 0)
		{
			if(_selectedIndex >= 0)
			{
				// Remove prev-highlight.
				_selectCardButtonList[_selectedIndex].SetHighlight(false);
			}

			// Set new index highlight.
			_selectedIndex = index;
			_selectCardButtonList[_selectedIndex].SetHighlight(true);

			// Set preview card.
			DisplayFullCard(_cardDataList[_selectedIndex]);
		}
	}

	private void DisplayFullCard(UniqueCardData uniqueCardData)
	{
		FullCardPreview.SetCardData(uniqueCardData.PlayerCardData);
	}

	public void OnClickConfirm()
	{
		if(_selectedIndex >= 0)
		{
			HideUI();

			if(_onConfirmSelectCard != null)
			{
				_onConfirmSelectCard.Invoke(_cardDataList[_selectedIndex]);
			}
		}
		else
		{
			Debug.LogError("SelectCardUI/OnClickConfirm: Select empty card.");
		}

	}

	private void HideUI()
	{
		ScrollView.ResetPosition();
		ScrollView.UpdatePosition();
		ScrollView.currentMomentum = Vector3.zero;

		if(_selectCardButtonList.Count > 0)
		{
			//Debug.Log("Start Destroy object");
			while(_selectCardButtonList.Count > 0)
			{
				//Debug.Log("Destroy object");
				GameObject obj = _selectCardButtonList[0].gameObject;
				_selectCardButtonList.RemoveAt(0);
				Destroy(obj);
			}
		}
		this.UIGrid.transform.localPosition = Vector3.zero;
		//ScrollView.ResetPosition();

		NGUITools.SetActive(gameObject, false);
	}
}
