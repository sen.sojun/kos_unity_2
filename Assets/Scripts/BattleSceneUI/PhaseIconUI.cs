﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class PhaseIconUI : MonoBehaviour 
{
	public enum PlayerSide
	{
		  Player
		, Enemy
	}

	public UISprite PlayerInnerIcon;
	public UISprite EnemyInnerIcon;

	public UISprite Icon;

	public UISprite PlayerBG;
	public UISprite EnemyBG;

	public UILabel label;

	private Tween _tweenPlayer;
	private Tween _tweenEnemy;

	// Use this for initialization
	void Start () {
	
	}

	/*
	// Update is called once per frame
	void Update () {
	
	}
	*/

	public void SetShowIcon(bool isShow, PlayerSide side)
	{
		if(side == PlayerSide.Player)
		{
			PlayerInnerIcon.gameObject.SetActive(isShow);
			PlayerBG.gameObject.SetActive(isShow);

			EnemyInnerIcon.gameObject.SetActive(false);
			EnemyBG.gameObject.SetActive(false);

			if(isShow)
			{
				BattleUIManager.RequestBringForward(PlayerBG.gameObject, false);
				BattleUIManager.RequestBringForward(Icon.gameObject, false);
				BattleUIManager.RequestBringForward(PlayerInnerIcon.gameObject, false);
				BattleUIManager.RequestBringForward(label.gameObject, false);
			}
		}
		else
		{
			PlayerInnerIcon.gameObject.SetActive(false);
			PlayerBG.gameObject.SetActive(false);

			EnemyInnerIcon.gameObject.SetActive(isShow);
			EnemyBG.gameObject.SetActive(isShow);

			if(isShow)
			{
				BattleUIManager.RequestBringForward(EnemyBG.gameObject, false);
				BattleUIManager.RequestBringForward(Icon.gameObject, false);
				BattleUIManager.RequestBringForward(EnemyInnerIcon.gameObject, false);
				BattleUIManager.RequestBringForward(label.gameObject, false);
			}
		}
	}
}
