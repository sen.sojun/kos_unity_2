﻿using UnityEngine;
using System.Collections;

public class MoveCardUI : MonoBehaviour 
{
	public delegate void OnSelectedMove(BattleCardData battleCardData, LocalBattleZoneIndex moveFieldIndex);
    public delegate void OnCancelMove();

    private float _timer = 0.0f;
    private bool _isCanCancel = false;
    private bool _isHaveTimeout = false;
    private bool _isPauseTimeout = false;

	private BattleCardData _battleCardData;
	private OnSelectedMove _onSelectedMoveField = null;
    private OnCancelMove _onCancelMove = null;
	private BattleManager _battleManager = null;

	public MoveCardUIButton PlayerBaseButton;
	public MoveCardUIButton BattlefieldButton;
	public MoveCardUIButton EnemyBaseButton;

	private bool _isShowUI = false;
	public bool IsShowUI { get { return _isShowUI; } }

	// Use this for initialization
	void Start () 
	{
		//NGUITools.SetActiveSelf(gameObject, false);
	}
	
	// Update is called once per frame

	void Update () 
    {
	    if(_isHaveTimeout)
        {
            _timer -= Time.deltaTime;
            if(_timer < 0.0f)
            {
                // timeout.                    
                _isHaveTimeout = false;
                AutoSelectOrCancelMove();
            }
        }
	}

	public void SetBattleManager(BattleManager battleManager)
	{
		_battleManager = battleManager;
	}

	public void ShowUI(BattleCardData battleCardData, bool isHaveTimeout, bool isCanCancel, OnSelectedMove onSelectedMove, OnCancelMove onCancelMove)
	{
		_battleCardData = battleCardData;
		_onSelectedMoveField = onSelectedMove;
        _onCancelMove = onCancelMove;

		switch(_battleCardData.CurrentZone)
		{
			case BattleZoneIndex.BaseP1: 
			case BattleZoneIndex.BaseP2: 
			{
				PlayerBaseButton.gameObject.SetActive(false);
				BattlefieldButton.gameObject.SetActive(true);
				EnemyBaseButton.gameObject.SetActive(false);

				if(BattleManager.Instance.IsTutorial)
				{
					BattlefieldButton.Arrow.gameObject.SetActive(true);

					PlayerBaseButton.Arrow.gameObject.SetActive(false);
					EnemyBaseButton.Arrow.gameObject.SetActive(false);
				}
			}
			break;

			case BattleZoneIndex.Battlefield:
			{
				PlayerBaseButton.gameObject.SetActive(true);
				BattlefieldButton.gameObject.SetActive(false);
				EnemyBaseButton.gameObject.SetActive(true);

				if(BattleManager.Instance.IsTutorial)
				{
					BattlefieldButton.Arrow.gameObject.SetActive(false);

					PlayerBaseButton.Arrow.gameObject.SetActive(true);
					EnemyBaseButton.Arrow.gameObject.SetActive(true);
				}
			}
			break;
		}
        
        _isHaveTimeout = (BattleManager.Instance.IsHaveTimeout && isHaveTimeout);
        _isCanCancel = isCanCancel;
        _timer = BattleManager.Instance.PopupDecisionTime;
        _isPauseTimeout = _isHaveTimeout;
        if(_isPauseTimeout)
        {
            if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
            {
                KOSNetwork.GamePlayer.localPlayer.RequestPauseTimer(true, BattleManager.Instance.TurnTimer);
            }
            else
            {
                BattleManager.Instance.SetPauseTimeout(true);
            }
        }

		StartShowUI();
		_isShowUI = true;
	}

	private void StartShowUI()
	{
		NGUITools.SetActiveSelf(gameObject, true);
		BattleUIManager.RequestBringForward(gameObject);
	}

	public void OnSelectMoveField(LocalBattleZoneIndex moveFieldIndex)
	{
		//Debug.Log("Selected field: " + moveFieldIndex.ToString());
		HideUI();

		if(_onSelectedMoveField != null)
		{
			_onSelectedMoveField.Invoke(_battleCardData, moveFieldIndex);
		}
	}
    
    public void OnCancel()
    {
        HideUI();
    
        if(_onCancelMove != null)
        {
            _onCancelMove.Invoke();
        }
    }

	public void HideUI()
	{
        if(_isPauseTimeout)
        {
            if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
            {
                KOSNetwork.GamePlayer.localPlayer.RequestPauseTimer(false, BattleManager.Instance.TurnTimer);
            }
            else
            {
                BattleManager.Instance.SetPauseTimeout(false);
            }
        }
		NGUITools.SetActive(gameObject, false);
		_isShowUI = false;
	}
    
    public void AutoSelectOrCancelMove()
    {
        if(_isCanCancel)
        {
            // if can cancel, will cancel
            Debug.Log("MoveCardUI/AutoSelectOrCancelMove: Cancel");    
            OnCancel();
        }
        else
        {
            Debug.Log("MoveCardUI/AutoSelectOrCancelMove: Auto Select");    
            
            switch(_battleCardData.CurrentZone)
            {
                case BattleZoneIndex.BaseP1: 
                case BattleZoneIndex.BaseP2: 
                {
                    OnSelectMoveField(LocalBattleZoneIndex.Battlefield);
                }
                break;
    
                case BattleZoneIndex.Battlefield:
                {
                    OnSelectMoveField(LocalBattleZoneIndex.EnemyBase);
                }
                break;
            }
        }
    }
}
