﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitingUI : MonoBehaviour 
{
    bool _isKeepOnTop = false;
    
    /*
	// Use this for initialization
	void Start () 
    {	
	}
    */
	
	// Update is called once per frame
	void Update () 
    {
	    if(gameObject.activeSelf && _isKeepOnTop)
        {
            BattleUIManager.RequestBringForward(gameObject);
        }	
	}

    public void ShowUI(bool isKeepOnTop = false)
    {
        _isKeepOnTop = isKeepOnTop;
    
        gameObject.SetActive(true);
        BattleUIManager.RequestBringForward(gameObject);
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }
}
