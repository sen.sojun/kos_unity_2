﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PhaseIndicatorUI : MonoBehaviour 
{
	public List<PhaseIconUI> PhaseIconList;

	// Use this for initialization
	void Awake()
	{
		SetPhase(DisplayBattlePhase.GameSetup);
	}

	/*
	void Start () 
	{
	}
	*/

	/*
	// Update is called once per frame
	void Update () {
	
	}
	*/

	public void SetPhase(DisplayBattlePhase displayPhase)
	{
		int showIndex = 0;

		switch(displayPhase)
		{
			case DisplayBattlePhase.GameSetup:
			case DisplayBattlePhase.Begin:
			{
				showIndex = 0;
			}
			break;

			case DisplayBattlePhase.Draw:
			{
				showIndex = 1;
			}
			break;

			case DisplayBattlePhase.PreBattle:
			{
				showIndex = 2;
			}
			break;

			case DisplayBattlePhase.Battle:
			{
				showIndex = 3;
			}
			break;

			case DisplayBattlePhase.PostBattle:
			{
				showIndex = 4;
			}
			break;

			case DisplayBattlePhase.End:
			{
				showIndex = 5;
			}
			break;
		}

		IBattleControlController localPlayer;
		bool isFound = BattleManager.Instance.GetLocalPlayer(out localPlayer);
		if(isFound)
		{
			PhaseIconUI.PlayerSide side;
			if(localPlayer.GetPlayerIndex() == BattleManager.Instance.CurrentActivePlayerIndex)
			{
				side = PhaseIconUI.PlayerSide.Player;
			}
			else
			{
				side = PhaseIconUI.PlayerSide.Enemy;
			}

			for(int index = 0; index < PhaseIconList.Count; ++index)
			{
				if(index == showIndex)
				{
					PhaseIconList[index].SetShowIcon(true, side);
				}
				else
				{
					PhaseIconList[index].SetShowIcon(false, side);
				}
			}
		}
		else
		{
			for(int index = 0; index < PhaseIconList.Count; ++index)
			{
				if(index == showIndex)
				{
					PhaseIconList[index].SetShowIcon(true, PhaseIconUI.PlayerSide.Player);
				}
				else
				{
					PhaseIconList[index].SetShowIcon(false, PhaseIconUI.PlayerSide.Player);
				}
			}
		}
	}
}
