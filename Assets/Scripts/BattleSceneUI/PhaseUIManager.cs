﻿using UnityEngine;
using System.Collections;

public class PhaseUIManager : MonoBehaviour 
{
    #region Private Properties
    private BattleManager _battleManager = null;
    #endregion

	public PhaseIndicatorUI PhaseIndicatorUI;
    //public UIButton NextPhaseButton;

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
    /*
	void Update () 
    {
	
	}
    */

    #region OnDestroy
    void OnDestory()
    {
        if (_battleManager != null)
        {
            _battleManager.UnSubscribeBattlePhaseUpdateEvent(SetBattlePhase);
        }
    }
    #endregion

    public void SetBattleManager(BattleManager battleManager)
    {
        _battleManager = battleManager;
        _battleManager.SubscribeBattlePhaseUpdateEvent(SetBattlePhase, true);
    }

	public void SetDisplayPhase(DisplayBattlePhase phase)
    {
		PhaseIndicatorUI.SetPhase(phase);
    }

    public void SetBattlePhase(BattlePhase phase)
    {
        SetDisplayPhase(BattleManager.ToDisplayBattlePhase(phase));
    }
}
