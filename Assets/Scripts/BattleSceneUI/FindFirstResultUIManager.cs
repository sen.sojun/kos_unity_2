﻿using UnityEngine;
using System.Collections;

public class FindFirstResultUIManager : MonoBehaviour 
{
    public delegate void OnFinishShowCallback();

    private OnFinishShowCallback _onFinishShowCallback = null;

    private float _showTime = 1.5f;
    private float _timer = 0.0f;
    private bool _isShow = false;

    public UILabel ResultLabel;
    public FindFirstResultUI MeFindFirstResultUI;
    public FindFirstResultUI EnemyFindFirstResultUI;

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (_isShow)
        {
            _timer += Time.deltaTime;
            if (_timer >= _showTime)
            {
                HideUI(); 
            }
        }
	}

    public void ShowUI(FindFirstOption.FindFirstOptionType meOptionType, FindFirstOption.FindFirstOptionType enemyOptionType, OnFinishShowCallback callback)
    {
        switch (FindFirstOption.FindFirstOptionCompare(meOptionType, enemyOptionType))
        {
            case FindFirstOption.FindFirstOptionResult.Win: ResultLabel.text = Localization.Get("BATTLE_FINDFIRST_RESULT_WIN"); break;
            case FindFirstOption.FindFirstOptionResult.Lose: ResultLabel.text = Localization.Get("BATTLE_FINDFIRST_RESULT_LOSE"); break;
            case FindFirstOption.FindFirstOptionResult.Draw: ResultLabel.text = Localization.Get("BATTLE_FINDFIRST_RESULT_DRAW"); break;
        }

        MeFindFirstResultUI.SetFindFirstOptionResult(meOptionType);
        EnemyFindFirstResultUI.SetFindFirstOptionResult(enemyOptionType);

        _timer = 0.0f;
        _onFinishShowCallback = callback;

        StartShowUI();
    }

    private void StartShowUI()
    {
		NGUITools.SetActiveSelf(gameObject, true);

		BattleUIManager.RequestBringForward(this.gameObject);

        _isShow = true;
    }

    private void HideUI()
    {
        StartHideUI();
    }

    private void StartHideUI()
    {
        if (_onFinishShowCallback != null)
        {
            _onFinishShowCallback.Invoke();
        }

		NGUITools.SetActive(gameObject, false);
        _isShow = false;
    }
}
