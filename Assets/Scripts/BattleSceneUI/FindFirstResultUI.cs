﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(UISprite))]
public class FindFirstResultUI : MonoBehaviour 
{
	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	/*
	void Update () 
    {
	
	}
	*/

    public void SetFindFirstOptionResult(FindFirstOption.FindFirstOptionType optionType)
    {
		switch(optionType)
		{
			case FindFirstOption.FindFirstOptionType.Rock:
			{
				GetComponent<UISprite>().spriteName = "Rock";
			}
			break;

			case FindFirstOption.FindFirstOptionType.Scissors:
			{
				GetComponent<UISprite>().spriteName = "Scissors";
			}
			break;

			case FindFirstOption.FindFirstOptionType.Paper:
			{
				GetComponent<UISprite>().spriteName = "Paper";
			}
			break;
		}
    }
}
