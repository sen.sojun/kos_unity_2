﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(UIButton))]
public class SelectCardButton : MonoBehaviour 
{
	private int _index = -1;
	private bool _isHighlight = false;

	private UIButton _button = null;
	private SelectCardUI _selectCardUI = null;

	public UILabel NameLabel;

	// Use this for initialization
	void Start () 
	{	
		_button = GetComponent<UIButton>();
	}
	
	// Update is called once per frame
	void Update () 
	{	
		if(!_button.isEnabled)
		{
			NameLabel.color = Color.black;
		}
		else
		{
			NameLabel.color = Color.white;
		}
	}

	public void SetSelectCardUI(SelectCardUI selectCardUI)
	{
		_selectCardUI = selectCardUI;
	}

	public void SetIndex(int index)
	{
		_index = index;
	}

	public void SetCardName(string name)
	{
		NameLabel.text = name;
	}

	void OnClick()
	{
		_selectCardUI.OnSelectCard(_index);
	}

	public void SetHighlight(bool isHighlight)
	{
		_isHighlight = isHighlight;
		UpdateHighlight();
	}

	private void UpdateHighlight()
	{
		//Debug.Log("SelectCard[" + _index.ToString() + "]: " + _isHighlight);

		GetComponent<UIButton>().isEnabled = !_isHighlight;
	}
}
