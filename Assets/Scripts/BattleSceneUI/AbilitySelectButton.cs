﻿using UnityEngine;
using System.Collections;

public class AbilitySelectButton : MonoBehaviour 
{
	public int ButtonIndex = -1;
	public UILabel ButtonLabel;

	/*
	// Use this for initialization
	void Start () 
	{
	}
	*/

	/*
	// Update is called once per frame
	void Update () 
	{
	}
	*/

	public void SetButtonLabel(string text)
	{
		ButtonLabel.text = text;
	}
}
