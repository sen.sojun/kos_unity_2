﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(UIButton))]
public class FullCardInfoButton : MonoBehaviour 
{
	public CardPopupMenuUI.CardPopupActionType ActionType;
	public UILabel ButtonLabel;

	/*
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	*/

	public void SetButtonLabel(string text)
	{
		ButtonLabel.text = text;
	}

	public void SetInteractable(bool isInteractable)
	{
		GetComponent<UIButton>().isEnabled = isInteractable;
	}
}
