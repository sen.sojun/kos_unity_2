﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameResultUI : MonoBehaviour 
{
	#region Delagates
	public delegate void OnChooseMainMenu();
	public delegate void OnChooseRetry();
	#endregion

	#region Enums
	public enum GameResultType : int
	{	
		  Lose 	= -1
		, Draw	= 0
		, Win	= 1
	}
	#endregion

	#region Properties
	private OnChooseMainMenu _onChooseMainMenu = null;
	private OnChooseRetry _onChooseRetry = null;

	public ResultUI ResultUI;
    public ResultUI ResultPVPUI;
	public RewardUI RewardUI;

	private GameResultType _result;
	private int _rewardSilverCoin;
	private PlayerCardData _rewardCard = null;
	#endregion

	// Use this for initialization
	void Start () 
	{
		//NGUITools.SetActive(gameObject, false);
	}
		
	// Update is called once per frame
	void Update () 
	{
	}

	public void ShowUI(GameResultType gameResult, int winCount, int rewardSilverCoin, PlayerCardData rewardCard, OnChooseMainMenu onChooseMainMenu, OnChooseRetry onChooseRetry)
	{
		_result = gameResult;
		_rewardSilverCoin = rewardSilverCoin;
		_rewardCard = rewardCard;

		_onChooseMainMenu = onChooseMainMenu;
		_onChooseRetry = onChooseRetry;

		NGUITools.SetActive(gameObject, true);
		BattleUIManager.RequestBringForward(gameObject);

		ResultUI.ShowUI(_result, _rewardSilverCoin, winCount, ShowResultFinish);
	}
    
    public void ShowPVPUI(GameResultType gameResult, int rewardSilverCoin, OnChooseMainMenu onChooseMainMenu)
    {
        _result = gameResult;
        _rewardSilverCoin = rewardSilverCoin;
        _rewardCard = null;

        _onChooseMainMenu = onChooseMainMenu;
        _onChooseRetry = null;

        NGUITools.SetActive(gameObject, true);
        BattleUIManager.RequestBringForward(gameObject);

        ResultPVPUI.ShowPVPUI(_result, _rewardSilverCoin, ShowResultFinish);
    }

	private void ShowResultFinish()
	{
		if(_result == GameResultType.Win && _rewardCard != null)
		{
			ResultUI.HideUI();
			RewardUI.ShowUI(_rewardCard);
		}
		else
		{
			// Show result
			ResultUI.ShowButton(true);
		}
	}

	public void OnClickMainMenu()
	{
		if(_onChooseMainMenu != null)
		{
			_onChooseMainMenu.Invoke();
		}
	}

	public void OnClickRetry()
	{
		if(_onChooseRetry != null)
		{
			_onChooseRetry.Invoke();
		}
	}
}
