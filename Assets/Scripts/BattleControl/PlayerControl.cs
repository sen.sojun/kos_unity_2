﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerControl : BattleControl
{

	#region Constructors
    public PlayerControl(IBattleControlController battleController)
        : base(battleController)
	{
	}
	#endregion

	#region Methods
	public void OnAwake()
	{
		
	}

	public void OnStart()
	{
		
	}

	public void OnUpdate() 
	{

	}
	#endregion

	#region BattleControl Methods
	public override void SetBattlePhase(BattlePhase battlePhase)
	{
		_currentBattlePhase = battlePhase;

		switch(_currentBattlePhase)
		{
			case BattlePhase.BeginGameSetup:
			case BattlePhase.SetupData:
			case BattlePhase.WaitSetupData:
			{
				// do nothing.
			}
			break;

            case BattlePhase.StartRPSSelect:
			{
			}
			break;

            case BattlePhase.WaitRPSSelect:
			{
				StartSelectFindFirstOption();
			}
			break;

			case BattlePhase.StartPlayOrderSelect:
			{
			}
			break;

            case BattlePhase.WaitPlayOrderSelect:
			{
				StartSelectFindFirstPlay();
			}
			break;
			
			case BattlePhase.SetupBase:
			{

			}
			break;

			case BattlePhase.WaitSetupBase:
			{
				StartSetupBase();
			}
			break;

			case BattlePhase.SetupLeader:
			{

			}
			break;

			case BattlePhase.WaitSetupLeader:
			{
				StartSetupLeader();
			}
			break;

			case BattlePhase.InitDrawCard:
			{

			}
			break;

            case BattlePhase.InitReHand:
            { 
                
            }
            break;
            
            case BattlePhase.WaitReHand:
            {
                StartReHand();
            }
            break;

			case BattlePhase.EndGameSetup:
			{
				
			}
			break;


			case BattlePhase.BeginBegin:
			{

			}
			break;

			case BattlePhase.EndBegin:
			{

			}
			break;

			case BattlePhase.BeginInactive:
			{

			}
			break;

			case BattlePhase.EndInactive:
				{

				}
				break;


			case BattlePhase.BeginReiterate:
				{

				}
				break;

			case BattlePhase.EndReiterate:
				{

				}
				break;


			case BattlePhase.BeginDraw:
				{

				}
				break;

			case BattlePhase.EndDraw:
				{

				}
				break;


			case BattlePhase.BeginPreBattle:
				{

				}
				break;

			case BattlePhase.EndPreBattle:
				{

				}
				break;


			case BattlePhase.BeginBattle:
				{

				}
				break;

			case BattlePhase.EndBattle:
				{

				}
				break;


			case BattlePhase.BeginPostBattle:
				{

				}
				break;

			case BattlePhase.EndPostBattle:
				{

				}
				break;


			case BattlePhase.BeginEnd:
				{

				}
				break;

			case BattlePhase.EndEnd:
				{

				}
				break;
		}
	}

	public override void StartSelectDealDamage(
		  List<BattleCardData> selectableList
		, int damage
		, int num
		, BattleManager.OnFinishDealDamage onFinish
		, BattleManager.OnCancelDealDamage onCancel
        , bool isHaveTimeout
        , bool isCanCancel
	)
	{
		//Debug.Log("PlayerControl: StartSelectDealDamage");
		BattleManager.Instance.ShowSelectTargetUI(
			string.Format(Localization.Get("BATTLE_SELECT_CARD_DEAL_DAMAGE"), num, damage)
            , this.PlayerIndex
			, selectableList
			, num
			, new BattleManager.OnFinishSelectTargetCallback(onFinish)
			, () =>
			{
				if(onCancel != null)
				{
					onCancel.Invoke();
				}
			}
            , isHaveTimeout
			, isCanCancel
		);
	}

	public override void StartSelectBackToOwnerHand(
		  List<BattleCardData> selectableList
		, int num
		, BattleManager.OnFinishBackToOwnerHand onFinish
		, BattleManager.OnCancelBackToOwnerHand onCancel
        , bool isHaveTimeout
        , bool isCanCancel
	)
	{
		BattleManager.Instance.ShowSelectTargetUI(
			string.Format(Localization.Get("BATTLE_SELECT_CARD_BACK_TO_HAND"), num)
            , this.PlayerIndex
			, selectableList
			, num
			, new BattleManager.OnFinishSelectTargetCallback(onFinish)
			, new BattleManager.OnCancelSelectTargetCallback(onCancel)
            , isHaveTimeout
			, isCanCancel
		);
	}

	public override void StartSelectTarget(
		  string headerText
		, List<BattleCardData> selectableList
		, int num
		, BattleManager.OnFinishSelectTargetCallback onFinish
		, BattleManager.OnCancelSelectTargetCallback onCancel
        , bool isHaveTimeout
		, bool isCanCancel
	)
	{
		BattleManager.Instance.ShowSelectTargetUI(
			  headerText
            , this.PlayerIndex
			, selectableList
			, num
			, onFinish
			, onCancel
            , isHaveTimeout
			, isCanCancel
		);
	}


	#endregion
}
