﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GamePlayerUI;

public class PlayerController : MonoBehaviour , IBattleControlController
{
	#region Private Properties
	private BattleController _battleController = null;
	private BattleUIManager _battleUIManager = null;

	private PlayerControl _playerControl = null;
	#endregion

	#region Awake
	void Awake()
	{
		// Find BattleController
		_battleController = GameObject.FindObjectOfType<BattleController>();
		// Find BattleUIManager
		_battleUIManager = GameObject.FindObjectOfType<BattleUIManager>();

		_playerControl = new PlayerControl(this);
		_playerControl.OnAwake();
	}
	#endregion

	#region Start
	// Use this for initialization
	void Start () 
	{
		_playerControl.OnStart();
	}
	#endregion

	#region Update
	// Update is called once per frame
	void Update () 
	{
		_playerControl.OnUpdate();
	}
	#endregion

    #region IBattleControlController Methods
    public bool IsLocalPlayer()
    {
        if (KOSNetwork.NetworkSetting.IsNetworkEnabled)
        {
            if (KOSNetwork.GamePlayer.localPlayer.playerIndex == GetPlayerIndex())
            {
                return true;
            }
            else
            {
                return false;   
            }
        }
        else
        {
            return true;
        }
    }

	public bool IsMyTurn()
	{
		return (this.GetPlayerIndex() == _battleController.BattleManager.CurrentActivePlayerIndex);
	}

	public PlayerIndex GetPlayerIndex()
	{
		return _playerControl.PlayerIndex;
	}

	public PlayerBattleData GetPlayerBattleData()
	{
		// Get Player Battle Data.
		return _battleController.BattleManager.PlayersData[(int)GetPlayerIndex()];
	}

    public void SetPlayerIndex(PlayerIndex playerIndex)
	{
		if(_playerControl != null)
		{
			_playerControl.SetPlayerIndex(playerIndex);
		}
		else
		{
			Debug.LogError("PlayerController/SetPlayerIndex: _playerControl is null.");
		}
	}

	public virtual void SetBattlePhase(BattlePhase battlePhase)
	{
		if(_playerControl != null)
		{  
			_playerControl.SetBattlePhase(battlePhase);
            //Debug.Log(gameObject.name + "/Phase : " + _playerControl.CurrentBattlePhase.ToString());
		}
		else
		{
			Debug.LogError("PlayerController/SetBattlePhase: _playerControl is null.");
		}
	}

	public void StartSelectRPS()
    {
        _battleUIManager.ShowSelectRPSUI(this.OnSelectRPS);
    }
    public void OnSelectRPS(RPSType rpsType)
    {
        // Network Bridge
        if (KOSNetwork.NetworkSetting.IsNetworkEnabled)
            KOSNetwork.GamePlayer.localPlayer.RequestSelectRPS(rpsType);
		else
            _battleController.BattleManager.OnSelectRPS(_playerControl.PlayerIndex, rpsType);
    }

    public void SetRPSResult(RPSResult optionResult)
	{
		_playerControl.SetRPSResult(optionResult);
	}

	public void StartSelectPlayOrder()
	{
        if (this.IsLocalPlayer())
        {
            if (_playerControl.OptionResult == RPSResult.Win)
            {
                _battleUIManager.ShowPlayOrderSelectUI(true, OnSelectFindFirstPlay);
            }
            else
            {
                _battleUIManager.ShowPlayOrderSelectUI(false);
            }
        }
	}
    public void OnSelectFindFirstPlay(PlayOrderType playType)
	{
        // Network Bridge
        if (KOSNetwork.NetworkSetting.IsNetworkEnabled)
        	KOSNetwork.GamePlayer.localPlayer.RequestSelectFindFirstPlay(playType);
		else
			_battleController.BattleManager.OnSelectPlayOrder(playType);
	}
		
	public void StartSetupBase()
	{
        if(BattleManager.Instance.IsLocalPlayer(this.GetPlayerIndex()))
        {
            // Find all base card in deck.
    		List<UniqueCardData> allBaseCardList;
    		bool isFound = _battleController.BattleManager.PlayersData[(int)_playerControl.PlayerIndex].Deck.FindAll(
    			  new CardTypeData(CardTypeData.EnumCardTypeToID(CardTypeData.CardType.Base))
    			, out allBaseCardList
    		);
    
    		// Filter duplicate base card.
            List<UniqueCardData> baseCardList = new List<UniqueCardData>();
    		foreach (UniqueCardData card_1 in allBaseCardList)
    		{
    			bool isSame = false;
    			foreach(UniqueCardData card_2 in baseCardList)
    			{
    				if (card_1.PlayerCardData == card_2.PlayerCardData)
    				{
    					isSame = true;
    					break;
    				}
    			}   
    			if(!isSame)
    			{
    				baseCardList.Add(card_1);
    			}
    		}
    
    		if(isFound && baseCardList.Count > 0)
    		{
    		    _battleUIManager.ShowSelectCardListUI("BATTLE_SELECT_BASE_HEADER", baseCardList, this.OnSelectBase);
    		}
    		else
    		{
    			Debug.LogError("PlayerController/StartSetupBase: not found any base card in deck.");
    		}
        }
	}

	public void OnSelectBase(UniqueCardData baseCard)
	{
        // Network Bridge
        if (KOSNetwork.NetworkSetting.IsNetworkEnabled)
            KOSNetwork.GamePlayer.localPlayer.RequestSelectBase(baseCard.UniqueCardID, baseCard.PlayerCardData.PlayerCardID, baseCard.CurrentZone);
        else
            _battleController.BattleManager.OnSelectBase(this, baseCard);
	}

	public void StartSetupLeader()
	{
        if(BattleManager.Instance.IsLocalPlayer(this.GetPlayerIndex()))
        {
            // Find all leader card in deck.
    		List<UniqueCardData> allLeaderCardList;
    		bool isFound = _battleController.BattleManager.PlayersData[(int)_playerControl.PlayerIndex].Deck.FindAll(
                  new CardSubTypeData(CardSubTypeData.EnumCardSubTypeToID(CardSubTypeData.CardSubType.Unique)) // CardSubType Unique
    			, out allLeaderCardList
    		);
    
    		// Filter duplicate leader card.
    		List<UniqueCardData> leaderCardList = new List<UniqueCardData>();
    		foreach (UniqueCardData card_1 in allLeaderCardList)
    		{
    			bool isSame = false;
    			foreach(UniqueCardData card_2 in leaderCardList)
    			{
    				if (card_1.PlayerCardData == card_2.PlayerCardData)
    				{
    					isSame = true;
    					break;
    				}
    			}
    			if(!isSame)
    			{
    				leaderCardList.Add (card_1);
    			}
    		}
    
            if (isFound && leaderCardList.Count > 0)
    		{
                _battleUIManager.ShowSelectCardListUI("BATTLE_SELECT_LEADER_HEADER", leaderCardList, this.OnSelectLeader);
    		}
    		else
    		{
    			Debug.LogError("PlayerController/StartSetupLeader: not found any leader card in deck.");
    		}
        }
	}

	public void OnSelectLeader(UniqueCardData leaderCard)
	{
        // Network Bridge
        if (KOSNetwork.NetworkSetting.IsNetworkEnabled)
            KOSNetwork.GamePlayer.localPlayer.RequestSelectLeader(leaderCard.UniqueCardID, leaderCard.PlayerCardData.PlayerCardID, leaderCard.CurrentZone);
        else
            _battleController.BattleManager.OnSelectLeader(this, leaderCard);
    }

    public void StartReHand()
    {
        _battleUIManager.YesNoPopupUI.ShowUI(
            "Do you want to Muligan?\n(Renew your hands.)"
            , OnReHand
            , OnSkipReHand
            , true
       );
    }

    public void OnSelectReHand(bool isReHand)
    {
        // Network Bridge
        if (KOSNetwork.NetworkSetting.IsNetworkEnabled)
            KOSNetwork.GamePlayer.localPlayer.RequestSelectReHand(isReHand);
        else
            _battleController.BattleManager.OnSelectReHand(this, isReHand);
    }

    private void OnReHand()
    {
        OnSelectReHand(true);
    }

    private void OnSkipReHand()
    {
        OnSelectReHand(false);
    }

	public void RequestNextPhase()
	{
		_battleController.BattleManager.RequestNextPhase(
              this.GetPlayerIndex()
            , BattleManager.Instance.Turn
            , BattleManager.Instance.CurrentBattlePhase
        );
	}

	public void StartDiscard(int discardNum, SelectCardPageUI.OnConfirmEvent onConfirmCallback, SelectCardPageUI.OnCancelEvent onCancelCallback, bool isHaveTimeout, bool isCanCancel)
    {
        List<UniqueCardData> handCardList = _battleController.BattleManager.PlayersData[(int)_playerControl.PlayerIndex].GetZoneDataList(CardZoneIndex.Hand);
        if (handCardList != null && handCardList.Count > 0)
        {
			string numberText = "";
			string cardPostfixText = "";
			switch(LocalizationManager.CurrentLanguage)
			{
				case LocalizationManager.Language.Thai:
				{
					numberText = discardNum.ToString ();
					cardPostfixText = "";
				}
				break;

				case LocalizationManager.Language.English:
				default:
				{
					if (discardNum == 1)
					{
						numberText = "a";
						cardPostfixText = "";
					}
					else
					{
						numberText = discardNum.ToString ();
						cardPostfixText = "s";
					}
				}
				break;
			}

			string headerText = string.Format (
				Localization.Get ("BATTLE_ASK_DISCARD_CARD")
				, numberText
				, cardPostfixText
			);
				
			_battleController.BattleManager.OnShowSelectCardPageUI(
				  ""
                , headerText
                , handCardList
                , discardNum
                , onConfirmCallback
                , onCancelCallback
                , isHaveTimeout
                , isCanCancel
            );
        }
        else
        {
            Debug.LogError("PlayerController/StartDiscard: not found any hand card.");
        }
    }

	public void StartSelectHandCard(
		  string headerText
		, List<UniqueCardData> selectCardList
		, int selectNum
		, BattleManager.OnFinishSelectHandCardCallback onFinish
		, BattleManager.OnCancelSelectHandCardCallback onCancel
        , bool isHaveTimeout
		, bool isCanCancel
	)
	{
		_battleController.BattleManager.OnShowSelectCardPageUI(
			  ""
			, headerText
			, selectCardList
			, selectNum
			, new SelectCardPageUI.OnConfirmEvent(onFinish)
			, new SelectCardPageUI.OnCancelEvent(onCancel)
            , isHaveTimeout
			, isCanCancel
		);
	}

	public void StartSelectUniqueCard(
		string headerText
		, List<UniqueCardData> selectCardList
		, int selectNum
		, BattleManager.OnFinishSelectUniqueCardCallback onFinish
		, BattleManager.OnCancelSelectUniqueCardCallback onCancel
        , bool isHaveTimeout
		, bool isCanCancel
	)
	{
		_battleController.BattleManager.OnShowSelectCardPageUI(
			  ""
			, headerText
			, selectCardList
			, selectNum
			, new SelectCardPageUI.OnConfirmEvent(onFinish)
			, new SelectCardPageUI.OnCancelEvent(onCancel)
            , isHaveTimeout
			, isCanCancel
		);
	}

	public void StartAskYesNo(
		string question
		, YesNoPopupUI.OnSelectYes onSelectYes
		, YesNoPopupUI.OnSelectNo onSelectNo
        , bool isHaveTimeout
	)
	{
		BattleManager.Instance.ShowAskYesNoUI(question, onSelectYes, onSelectNo, isHaveTimeout);
	}

	public void StartSelectDealDamage(
		  List<BattleCardData> selectableList
		, int damage
		, int num
		, BattleManager.OnFinishDealDamage onFinish
		, BattleManager.OnCancelDealDamage onCancel //= null
        , bool isHaveTimeout
        , bool isCanCancel
	)
	{
		_playerControl.StartSelectDealDamage(selectableList, damage, num, onFinish, onCancel, isHaveTimeout, isCanCancel);
	}

	public void StartSelectBackToOwnerHand(
		  List<BattleCardData> selectableList
		, int num
		, BattleManager.OnFinishBackToOwnerHand onFinish
		, BattleManager.OnCancelBackToOwnerHand onCancel
        , bool isHaveTimeout
        , bool isCanCancel
	)
	{
		_playerControl.StartSelectBackToOwnerHand(selectableList, num, onFinish, onCancel, isHaveTimeout, isCanCancel);
	}

	public void StartSelectTarget(
		  string headerText
		, List<BattleCardData> selectableList
		, int num
		, BattleManager.OnFinishSelectTargetCallback onFinish
		, BattleManager.OnCancelSelectTargetCallback onCancel
        , bool isHaveTimeout
		, bool isCanCancel
	)
	{
		_playerControl.StartSelectTarget(headerText, selectableList, num, onFinish, onCancel, isHaveTimeout, isCanCancel);
	}
    #endregion
}
