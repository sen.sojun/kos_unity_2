﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GamePlayerUI;

public class BotController : MonoBehaviour, IBattleControlController 
{
	#region Private Properties
	private BattleController _battleController = null;
	private BattleUIManager _battleUIManager = null;

	private BotControl _botControl = null;
	#endregion

	#region Awake
	void Awake()
	{
		// Find BattleController
		_battleController = GameObject.FindObjectOfType<BattleController>();
		// Find BattleUIManager
		_battleUIManager = GameObject.FindObjectOfType<BattleUIManager>();

		_botControl = new BotControl(this);
		_botControl.OnAwake();
	}
	#endregion

	#region Start
	// Use this for initialization
	void Start () 
	{
		_botControl.OnStart();
	}
	#endregion

	#region Update
	// Update is called once per frame
	void Update () 
	{
		if(_botControl != null)
		{
			_botControl.OnUpdate();
		}
	}
	#endregion

	#region Methods
	public void SetBotControl(BotControl bot)
	{
		//Debug.Log("Set Bot: " + bot.BotName);
		_botControl = bot;
	}
	#endregion

    #region IBattleControlController Methods
    public bool IsLocalPlayer()
    {
        return false;
    }

	public bool IsMyTurn()
	{
		return (this.GetPlayerIndex() == _battleController.BattleManager.CurrentActivePlayerIndex);
	}

	public PlayerIndex GetPlayerIndex()
	{
		return _botControl.PlayerIndex;
	}

	public PlayerBattleData GetPlayerBattleData()
	{
		// Get Player Battle Data.
		return _battleController.BattleManager.PlayersData[(int)GetPlayerIndex()];
	}

    public void SetPlayerIndex(PlayerIndex playerIndex)
    {
        if (_botControl != null)
        {
            _botControl.SetPlayerIndex(playerIndex);
        }
        else
        {
            Debug.LogError("BotController/SetBattlePhase: _botControl is null.");
        }
    }

    public virtual void SetBattlePhase(BattlePhase battlePhase)
    {
        if (_botControl != null)
        {
            _botControl.SetBattlePhase(battlePhase);
            //Debug.Log(gameObject.name + "/Phase : " + _botControl.CurrentBattlePhase.ToString());
        }
        else
        {
            Debug.LogError("BotController/SetBattlePhase: _botControl is null.");
        }
    }

	public void StartSelectRPS()
    {
		// do nothing
    }

    public void OnSelectRPS(RPSType optionType)
    {
		_battleController.BattleManager.OnSelectRPS(_botControl.PlayerIndex, optionType);
    }

    public void SetRPSResult(RPSResult optionResult)
	{
		_botControl.SetRPSResult(optionResult);
	}

	public void StartSelectPlayOrder()
	{
		// do nothing.
	}

    public void OnSelectFindFirstPlay(PlayOrderType playType)
	{
		_battleController.BattleManager.OnSelectPlayOrder(playType);
	}

	public void StartSetupBase()
	{
		// do nothing.
	}

	public void OnSelectBase(UniqueCardData baseCard)
	{
		_battleController.BattleManager.OnSelectBase(this, baseCard);
	}

	public void StartSetupLeader()
	{
		// do nothing.
	}

	public void OnSelectLeader(UniqueCardData leaderCard)
	{
		_battleController.BattleManager.OnSelectLeader(this, leaderCard);
	}

    public void StartReHand()
    {
        // do nothing.
    }

    public void OnSelectReHand(bool isReHand)
    {
        _battleController.BattleManager.OnSelectReHand(this, isReHand);
    }

	public void RequestNextPhase()
	{
		_battleController.BattleManager.RequestNextPhase(
              this.GetPlayerIndex()
            , BattleManager.Instance.Turn
            , BattleManager.Instance.CurrentBattlePhase
        );
	}

	public void StartDiscard(int discardNum, SelectCardPageUI.OnConfirmEvent onConfirmCallback, SelectCardPageUI.OnCancelEvent onCancelCallback, bool isHaveTimeout, bool isCanCancel)
    {
        List<UniqueCardData> handCardList = _battleController.BattleManager.PlayersData[(int)_botControl.PlayerIndex].GetZoneDataList(CardZoneIndex.Hand);
        if (handCardList != null)
        {
            List<int> discardIDList = new List<int>();
            foreach (UniqueCardData card in handCardList)
            {
                discardIDList.Add(card.UniqueCardID);
                if (discardIDList.Count >= discardNum)
                {
                    break;
                }
            }

            onConfirmCallback.Invoke(discardIDList);
        }
        else
        {
            Debug.LogError("BotController/StartDiscard: not found any hand card.");
        }
    }

	public void StartSelectHandCard(
		  string headerText
		, List<UniqueCardData> selectableList
		, int selectNum
		, BattleManager.OnFinishSelectHandCardCallback onFinish
		, BattleManager.OnCancelSelectHandCardCallback onCancel
        , bool isHaveTimeout
		, bool isCanCancel
	)
	{
		List<int> selectIDList = new List<int>();
		foreach (UniqueCardData card in selectableList)
		{
			selectIDList.Add(card.UniqueCardID);
			if (selectIDList.Count >= selectNum)
			{
				break;
			}
		}

		onFinish.Invoke(selectIDList);
	}

	public void StartSelectUniqueCard(
		  string headerText
		, List<UniqueCardData> selectableList
		, int selectNum
		, BattleManager.OnFinishSelectUniqueCardCallback onFinish
		, BattleManager.OnCancelSelectUniqueCardCallback onCancel
        , bool isHaveTimeout
		, bool isCanCancel
	)
	{
		List<int> selectIDList = new List<int>();
        if(selectableList != null && selectableList.Count > 0)
        {
    		foreach (UniqueCardData card in selectableList)
    		{
    			selectIDList.Add(card.UniqueCardID);
    			if (selectIDList.Count >= selectNum)
    			{
    				break;
    			}
    		}
        }

		onFinish.Invoke(selectIDList);
	}

	public void StartAskYesNo(
		  string question
		, YesNoPopupUI.OnSelectYes onSelectYes
		, YesNoPopupUI.OnSelectNo onSelectNo
        , bool isHaveTimeout
	)
	{
		if(onSelectYes != null)
		{
			onSelectYes.Invoke();
		}
		else
		{
			onSelectNo.Invoke();
		}
	}

	public void StartSelectDealDamage(
		  List<BattleCardData> selectableList
		, int damage
		, int num
		, BattleManager.OnFinishDealDamage onFinish
		, BattleManager.OnCancelDealDamage onCancel //= null
        , bool isHaveTimeout
        , bool isCanCancel
	)
	{
		_botControl.StartSelectDealDamage(selectableList, damage, num, onFinish, onCancel, isHaveTimeout, isCanCancel);
	}

	public void StartSelectBackToOwnerHand(
		List<BattleCardData> selectableList
		, int num
		, BattleManager.OnFinishBackToOwnerHand onFinish
		, BattleManager.OnCancelBackToOwnerHand onCancel
        , bool isHaveTimeout
        , bool isCanCancel
	)
	{
		_botControl.StartSelectBackToOwnerHand(selectableList, num, onFinish, onCancel, isHaveTimeout, isCanCancel);
	}

	public void StartSelectTarget(
		  string headerText
		, List<BattleCardData> selectableList
		, int num
		, BattleManager.OnFinishSelectTargetCallback onFinish
		, BattleManager.OnCancelSelectTargetCallback onCancel
        , bool isHaveTimeout
		, bool isCanCancel
	)
	{
		_botControl.StartSelectTarget(headerText, selectableList, num, onFinish, onCancel, isHaveTimeout, isCanCancel);
	}
    #endregion
}
