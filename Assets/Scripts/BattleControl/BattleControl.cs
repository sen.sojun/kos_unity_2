﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GamePlayerUI;

public interface IBattleControlController
{
    bool IsLocalPlayer();
	bool IsMyTurn();
	PlayerIndex GetPlayerIndex();
	PlayerBattleData GetPlayerBattleData();

	void SetPlayerIndex(PlayerIndex playerIndex);
	void SetBattlePhase(BattlePhase battlePhase);

	void StartSelectRPS();
    void OnSelectRPS(RPSType optionType);

    void SetRPSResult(RPSResult optionResult);

	void StartSelectPlayOrder();
    void OnSelectFindFirstPlay(PlayOrderType playType);
	//void StartFindFirstPlayResult();

	void StartSetupBase();
	void OnSelectBase(UniqueCardData baseCard);

	void StartSetupLeader();
	void OnSelectLeader(UniqueCardData leaderCard);

    void StartReHand();
    void OnSelectReHand(bool isReHand);

	void RequestNextPhase();

	void StartDiscard(
		  int discardNum
		, SelectCardPageUI.OnConfirmEvent onConfirmCallback
		, SelectCardPageUI.OnCancelEvent onCancelCallback
        , bool isHaveTimeout
		, bool isCanCancel
	);

	void StartSelectDealDamage(
		  List<BattleCardData> selectableList
		, int damage
		, int num
		, BattleManager.OnFinishDealDamage onFinish
		, BattleManager.OnCancelDealDamage onCancel //= null
        , bool isHaveTimeout
        , bool isCanCancel
	);

	void StartSelectBackToOwnerHand(
		  List<BattleCardData> selectableList
		, int num
		, BattleManager.OnFinishBackToOwnerHand onFinish
		, BattleManager.OnCancelBackToOwnerHand onCancel //= null
        , bool isHaveTimeout
        , bool isCanCancel //= false
	);

	void StartSelectTarget(
		  string headerText
		, List<BattleCardData> selectableList
		, int num
		, BattleManager.OnFinishSelectTargetCallback onFinish
		, BattleManager.OnCancelSelectTargetCallback onCancel //= null
        , bool isHaveTimeout
		, bool isCanCancel //= false
	);

	void StartSelectHandCard(
		  string headerText
		, List<UniqueCardData> selectableList
		, int selectNum
		, BattleManager.OnFinishSelectHandCardCallback onFinish
		, BattleManager.OnCancelSelectHandCardCallback onCancel
        , bool isHaveTimeout
		, bool isCanCancel //= false
	);

	void StartSelectUniqueCard(
		  string headerText
		, List<UniqueCardData> selectableList
		, int selectNum
		, BattleManager.OnFinishSelectUniqueCardCallback onFinish
		, BattleManager.OnCancelSelectUniqueCardCallback onCancel
        , bool isHaveTimeout
		, bool isCanCancel //= false
	);

	void StartAskYesNo(
		  string question
		, YesNoPopupUI.OnSelectYes onSelectYes
		, YesNoPopupUI.OnSelectNo onSelectNo
        , bool isHaveTimeout
	);

	/*
	void StartSelectTarget(
		  List<BattleCardData> targetList
		, int targetNum
		, BattleManager.OnFinishSelectTargetCallback onFinish
		, BattleManager.OnCancelSelectTargetCallback onCancel = null
	);
	*/
}

public abstract class BattleControl
{
	#region Protected Properties
	protected BattlePhase _currentBattlePhase;
	protected PlayerIndex _playerIndex;
    protected IBattleControlController _battleController;
    protected GamePlayerUI.RPSResult _optionResult;
	#endregion

	#region Public Properties
	public BattlePhase CurrentBattlePhase
	{
		get { return _currentBattlePhase; }
	}
	public PlayerIndex PlayerIndex
	{
		get { return _playerIndex; }
	}
    public IBattleControlController BattleController
    {
        get { return _battleController; }
    }
    public GamePlayerUI.RPSResult OptionResult
	{
		get { return _optionResult; }
	}
	#endregion

	#region Constructors
    protected BattleControl(IBattleControlController battleController)
	{
        _battleController = battleController;

		_currentBattlePhase = BattlePhase.BeginGameSetup;
	}
	#endregion

	#region Methods
	public virtual void SetPlayerIndex(PlayerIndex playerIndex)
	{
		_playerIndex = playerIndex;
	}

	public virtual void SetBattlePhase(BattlePhase battlePhase)
	{
		_currentBattlePhase = battlePhase;
	}

    public virtual void SetRPSResult(GamePlayerUI.RPSResult optionResult)
	{
		_optionResult = optionResult;
	}
	#endregion

    #region FindFirst Phase Methods
	virtual protected void StartSelectFindFirstOption()
    {
		this.BattleController.StartSelectRPS();
    }

    virtual protected void OnSelectFindFirstOption(RPSType optionType)
    {
		this.BattleController.OnSelectRPS(optionType);
    }

	virtual protected void StartSelectFindFirstPlay()
	{
		this.BattleController.StartSelectPlayOrder();
	}

    virtual protected void OnSelectFindFirstPlay(PlayOrderType playType)
	{
		this.BattleController.OnSelectFindFirstPlay(playType);
	}
    #endregion

	virtual protected void StartSetupBase()
    {
		this.BattleController.StartSetupBase();
    }

	virtual protected void OnSelectBase(UniqueCardData baseCard)
	{
		this.BattleController.OnSelectBase(baseCard);
	}

	virtual protected void StartSetupLeader()
    {
		this.BattleController.StartSetupLeader();
    }

	virtual protected void OnSelectLeader(UniqueCardData leaderCard)
	{
		this.BattleController.OnSelectLeader(leaderCard);
	}

    virtual protected void StartReHand()
    {
        this.BattleController.StartReHand();
    }

    virtual protected void OnSelectReHand(bool isReHand)
    {
        this.BattleController.OnSelectReHand(isReHand);
    }

	virtual protected void RequestNextPhase()
	{
		this.BattleController.RequestNextPhase();
	}

	virtual protected void StartDiscard(int discardNum, SelectCardPageUI.OnConfirmEvent onConfirmCallback, SelectCardPageUI.OnCancelEvent onCancelCallback, bool isHaveTimeout, bool isCanCancel)
    {
        this.BattleController.StartDiscard(discardNum, onConfirmCallback, onCancelCallback, isHaveTimeout, isCanCancel);
    }

	/*
	virtual protected void StartSelectTarget(
		  List<BattleCardData> targetList
		, int targetNum
		, BattleManager.OnFinishSelectTargetCallback onConfirmCallback
		, BattleManager.OnCancelSelectTargetCallback onCancelCallback = null
	)
	{
		this.BattleController.StartSelectTarget(
			  targetList
			, targetNum
			, onConfirmCallback
			, onCancelCallback
		);
	}
	*/

	public virtual void StartSelectDealDamage(
		List<BattleCardData> selectableList
		, int damage
		, int num
		, BattleManager.OnFinishDealDamage onFinish
		, BattleManager.OnCancelDealDamage onCancel //= null
        , bool isHaveTimeout
        , bool isCanCancel
	)
	{
		Debug.Log("BattleControl: StartSelectDealDamage");

		List<int> targetIDList = new List<int>();
		foreach(BattleCardData card in selectableList)
		{
			targetIDList.Add(card.BattleCardID);
			if(targetIDList.Count == num || targetIDList.Count == selectableList.Count)
			{
				break;
			}
		}

		onFinish.Invoke(targetIDList);
	}

	public virtual void StartSelectBackToOwnerHand(
		List<BattleCardData> selectableList
		, int num
		, BattleManager.OnFinishBackToOwnerHand onFinish
		, BattleManager.OnCancelBackToOwnerHand onCancel
        , bool isHaveTimeout
        , bool isCanCancel
	)
	{
		List<int> targetIDList = new List<int>();
		foreach(BattleCardData card in selectableList)
		{
			targetIDList.Add(card.BattleCardID);
			if(targetIDList.Count == num || targetIDList.Count == selectableList.Count)
			{
				break;
			}
		}

		onFinish.Invoke(targetIDList);
	}

	public virtual void StartSelectTarget(
		  string headerText
		, List<BattleCardData> selectableList
		, int num
		, BattleManager.OnFinishSelectTargetCallback onFinish
		, BattleManager.OnCancelSelectTargetCallback onCancel
        , bool isHaveTimeout
		, bool isCanCancel
	)
	{
		List<int> targetIDList = new List<int>();
		foreach(BattleCardData card in selectableList)
		{
			targetIDList.Add(card.BattleCardID);
			if(targetIDList.Count == num || targetIDList.Count == selectableList.Count)
			{
				break;
			}
		}

		onFinish.Invoke(targetIDList);
	}

	public virtual void StartSelectHandCard(
		  string headerText
		, List<UniqueCardData> selectableList
		, int selectNum
		, BattleManager.OnFinishSelectHandCardCallback onFinish
		, BattleManager.OnCancelSelectHandCardCallback onCancel
        , bool isHaveTimeout
		, bool isCanCancel = false
	)
	{
		this.BattleController.StartSelectHandCard(
			  headerText
			, selectableList
			, selectNum
			, onFinish
			, onCancel
            , isHaveTimeout
			, isCanCancel
		);
	}

	public virtual void StartSelectUniqueCard(
		string headerText
		, List<UniqueCardData> selectableList
		, int selectNum
		, BattleManager.OnFinishSelectUniqueCardCallback onFinish
		, BattleManager.OnCancelSelectUniqueCardCallback onCancel
        , bool isHaveTimeout
		, bool isCanCancel = false
	)
	{
		this.BattleController.StartSelectUniqueCard(
			  headerText
			, selectableList
			, selectNum
			, onFinish
			, onCancel
            , isHaveTimeout
			, isCanCancel
		);
	}

	public virtual void StartAskYesNo(
		string question
		, YesNoPopupUI.OnSelectYes onSelectYes
		, YesNoPopupUI.OnSelectNo onSelectNo
        , bool isHaveTimeout
	)
	{
		this.BattleController.StartAskYesNo(
			question
			, onSelectYes
			, onSelectNo
            , isHaveTimeout
		);
	}
}
