﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GamePlayerUI;

public class BotControl : BattleControl 
{
	#region Enums
	public enum ActionState
	{
		  Waiting
		, Playing
		, Moving
		, Attacking
	}

	public enum BotActionState : int
	{
		  Init
		, Action
		, Finish
	}
	#endregion

	#region Delegates
	public delegate bool PlayAction();
	#endregion

	#region Protected Properties
	protected float _timer = 0.0f;
	protected float _waitTime = 0.75f;
	protected string _botName;
	protected bool _isPlayAction = false;
	protected ActionState _actionState = ActionState.Waiting;

	protected BotActionState _playActionState = BotActionState.Init;
	protected BotActionState _moveActionState = BotActionState.Init;
	protected BotActionState _attackActionState = BotActionState.Init;
	protected List<PlayAction> _botPlayActionList = null;
	#endregion

	#region Public Properties
	public string BotName
	{
		get {return _botName; }
	}
	#endregion

	#region Constructors
    public BotControl(IBattleControlController battleController)
        : base(battleController)
	{
		_botName = "Dummy Bot";
		_botPlayActionList = new List<PlayAction>();
	}
	#endregion

	#region Methods
	public void OnAwake()
	{
		
	}

	public void OnStart () 
	{
	
	}

	public void OnUpdate () 
	{
        if (_actionState == ActionState.Waiting) return; // waiting...

        if (!BattleManager.Instance.IsPlayerCanAction) return; // Wait until can command action.

		_timer -= Time.deltaTime;
		if(_timer <= 0.0f)
		{
			_timer = _waitTime;

		    switch(_actionState)
		    {
			    case ActionState.Playing:
			    {
				    OnPlay();
			    }
			    break;

			    case ActionState.Moving:
			    {
				     OnMove();
			    }
			    break;

			    case ActionState.Attacking:
			    {
				    OnAttackCard();
			    }
			    break;
		    }
        }
	}
	#endregion

	#region Play Card Methods
	private void OnStartPlay()
	{
		_actionState = ActionState.Playing;
		_playActionState = BotActionState.Init;
		_botPlayActionList.Clear();

		_timer = _waitTime;
	}

	private void OnPlay()
	{
		switch(_playActionState)
		{
			case BotActionState.Init:
			{
				InitPlay();
				_playActionState = BotActionState.Action;

				OnPlay ();
			}
			break;

			case BotActionState.Action:
			{
				bool isAction = false;
				while(this._botPlayActionList.Count > 0)
				{
					PlayAction botAction = _botPlayActionList[0];
					_botPlayActionList.RemoveAt(0);

					if(botAction != null)
					{
						isAction = botAction.Invoke();
						if(isAction)
						{
							_isPlayAction = true;
							break;	
						}
					}
				}
					
				if(this._botPlayActionList.Count <= 0)
				{
					if(_isPlayAction)
					{
						// Next round.
						_playActionState = BotActionState.Init;
						if(!isAction)
						{
							OnPlay();
						}
					}
					else
					{
						_playActionState = BotActionState.Finish;
						if (!isAction)
						{
							OnStartMove ();
						} 
					}
				}
			}
			break;

			case BotActionState.Finish:
			{
				OnStartMove();
			}
			break;
		}
	}

	private void InitPlay()
	{
		bool isPlayResourceFirst = true;
		_isPlayAction = false;

		// Resource
		List<BattleCardData> resourceList;
		FindInBattlefield(CardSubTypeData.CardSubType.Resource, out resourceList);
		if(resourceList != null && resourceList.Count >= 5)	
		{
			isPlayResourceFirst = false;
		}
		else
		{
			isPlayResourceFirst = true;
		}

		// Resource
		if(isPlayResourceFirst)
		{
			_botPlayActionList.Add(() => this.OnPlaySubType(CardSubTypeData.CardSubType.Resource));
		}

		// Unit
		_botPlayActionList.Add(() => this.OnPlayCardType(CardTypeData.CardType.Unit));
		// Event
		_botPlayActionList.Add(() => this.OnPlayCardType(CardTypeData.CardType.Event));
		// Technic
		_botPlayActionList.Add(() => this.OnPlayCardType(CardTypeData.CardType.Technic));
		// Structure
		_botPlayActionList.Add(() => this.OnPlayCardType(CardTypeData.CardType.Structure));
		// Item
		_botPlayActionList.Add(() => this.OnPlayCardType(CardTypeData.CardType.Item));

		// Resource
		if(!isPlayResourceFirst)
		{
			_botPlayActionList.Add(() => this.OnPlaySubType(CardSubTypeData.CardSubType.Resource));
		}

		// Leader
		_botPlayActionList.Add(this.OnPlayLeader);
	}
	#endregion

	#region Move Card Methods
	private void OnStartMove()
	{
		_actionState = ActionState.Moving;
		_moveActionState = BotActionState.Init;

		OnMove ();
	}

	private void OnMove()
	{
		switch(_moveActionState)
		{
			case BotActionState.Init:
			{
				InitMove();
				_moveActionState = BotActionState.Action;
				OnMove ();
				_timer = _waitTime;
			}
			break;

			case BotActionState.Action:
			{
				if(!ActionMove())
				{
					_moveActionState = BotActionState.Finish;
					_actionState = ActionState.Waiting;

					// Next Phase
					RequestNextPhase();
				}
			}
			break;

			case BotActionState.Finish:
			{
			}
			break;
		}
	}

	private void InitMove()
	{
	}

	private bool ActionMove()
	{
		List<BattleCardData> resultList;
		GetAllBattleCard(out resultList);

		if(resultList != null && resultList.Count > 0)
		{
			foreach(BattleCardData battleCard in resultList)
			{
				BattleZoneIndex forwardZoneIndex;
				if(battleCard.IsCanMoveForward(out forwardZoneIndex))
				{
					if(!battleCard.IsICanKill())
					{
						BattleManager.Instance.RequestMoveBattleCard(battleCard.BattleCardID, forwardZoneIndex, true);

						return true;
					}
				}
			}
		}

		return false;
	}
	#endregion

	#region Attack Card Methods
	private void OnStartAttackCard()
	{
		_actionState = ActionState.Attacking;
		_attackActionState = BotActionState.Init;
		//_timer = 0.0f;

		OnAttackCard ();
	}

	private void OnAttackCard()
	{
		switch(_attackActionState)
		{
			case BotActionState.Init:
			{
				//InitAttackCard();
				_attackActionState = BotActionState.Action;
				_timer = _waitTime;
			}
			break;

			case BotActionState.Action:
			{
				if(!ActionAttack())
				{
					_attackActionState = BotActionState.Finish;

					// Next Phase
					RequestNextPhase();
				}
			}
			break;

			case BotActionState.Finish:
			{
				_actionState = ActionState.Waiting;
			}
			break;
		}
	}
		
	private bool ActionAttack()
	{
		if(BattleManager.Instance.IsPerformBattle)
		{
			// Wait until preform attack finish.
			return true;
		}

		List<BattleCardData> resultList;
		GetAllBattleCard(out resultList);

		foreach(BattleCardData attacker in resultList)
		{
			List<BattleCardData> atkableList;
			if(BattleManager.Instance.IsCanAttack(attacker.BattleCardID,  out atkableList))
			{
				//Debug.Log("Target: " + atkableList.Count);

				if(atkableList.Count > 0)
				{
					for(int index = 0; index < atkableList.Count; ++index)
					{
						BattleCardData target = atkableList[index];

						if(attacker.IsCanAttackSuccess(target) && attacker.IsICanKillIt(target))
						{
							//Debug.Log(attacker.Name + " attack to " + target.Name);

							BattleManager.Instance.RequestAttackBattleCard(
								  attacker.BattleCardID
								, target.BattleCardID
                                , true
							);
								
							return true;
						}
					}

					// not attack
					for(int index = 0; index < atkableList.Count; ++index)
					{
						BattleCardData target = atkableList[index];

						if(attacker.IsCanAttackSuccess(target) && target.TypeData == CardTypeData.CardType.Base)
						{
							BattleManager.Instance.RequestAttackBattleCard(
								attacker.BattleCardID
								, target.BattleCardID
                                , true
							);

							return true;
						}
					}
				}
			}
		}

		return false;
	}
	#endregion

	#region BattleControl Methods
	public override void SetBattlePhase(BattlePhase battlePhase)
	{
		_currentBattlePhase = battlePhase;

		switch(_currentBattlePhase)
		{
			case BattlePhase.BeginGameSetup:
			case BattlePhase.SetupData:
			case BattlePhase.WaitSetupData:
			{
				// do nothing.
			}
			break;

            case BattlePhase.StartRPSSelect:
			{
			}
			break;

            case BattlePhase.WaitRPSSelect:
			{
				StartSelectFindFirstOption();
			}
			break;

            case BattlePhase.StartPlayOrderSelect:
			{
			}
			break;

            case BattlePhase.WaitPlayOrderSelect:
			{
				StartSelectFindFirstPlay();
			}
			break;

			case BattlePhase.SetupBase:
			{

			}
			break;

			case BattlePhase.WaitSetupBase:
			{
				StartSetupBase();
			}
			break;

			case BattlePhase.SetupLeader:
			{

			}
			break;

			case BattlePhase.WaitSetupLeader:
			{
				StartSetupLeader();
			}
			break;

			case BattlePhase.InitDrawCard:
			{

			}
			break;

            case BattlePhase.InitReHand:
            { 

            }
            break;

            case BattlePhase.WaitReHand:
            {
                StartReHand();
            }
            break;

			case BattlePhase.EndGameSetup:
			{

			}
			break;


			case BattlePhase.BeginBegin:
			{

			}
			break;

			case BattlePhase.BeginInactive:
			{

			}
			break;

			case BattlePhase.EndInactive:
			{

			}
			break;

			case BattlePhase.BeginReiterate:
			{

			}
			break;

			case BattlePhase.EndReiterate:
			{

			}
			break;

			case BattlePhase.EndBegin:
			{

			}
			break;

			case BattlePhase.BeginDraw:
			{

			}
			break;

			case BattlePhase.EndDraw:
			{

			}
			break;

			case BattlePhase.BeginPreBattle:
			{

			}
			break;

			case BattlePhase.PreBattle:
			{
				if(this._battleController.IsMyTurn())
				{
					OnStartPlay();
				}
			}
			break;

			case BattlePhase.EndPreBattle:
			{

			}
			break;


			case BattlePhase.BeginBattle:
			{

			}
			break;

			case BattlePhase.Battle:
			{
				if(this._battleController.IsMyTurn())
				{
					OnStartAttackCard();
				}
			}
			break;

			case BattlePhase.EndBattle:
			{

			}
			break;
	
			case BattlePhase.BeginPostBattle:
			{

			}
			break;

			case BattlePhase.PostBattle:
			{
				if(this._battleController.IsMyTurn())
				{
					OnStartMove();
				}
			}
			break;

			case BattlePhase.EndPostBattle:
			{

			}
			break;

			case BattlePhase.BeginEnd:
			{

			}
			break;

			case BattlePhase.EndEnd:
			{

			}
			break;
		}
	}
	#endregion

	#region FindFirst Phase Methods
	override protected void StartSelectFindFirstOption()
	{
		// Random FindFirstOption
        RPSType optionType = GameUtility.RandomRPS();
		OnSelectFindFirstOption(optionType);
	}

	override protected void StartSelectFindFirstPlay()
	{
        if(OptionResult == RPSResult.Win)
		{
			// Random FindFirstPlay
            PlayOrderType playType = PlayOrderType.PlayFirst;
			OnSelectFindFirstPlay(playType);
		}
	}
	#endregion

	override protected void StartSetupBase()
	{
		// Random base card in deck.
		List<UniqueCardData> baseCardList;
		bool isFound = _battleController.GetPlayerBattleData().Deck.FindAll(
              new CardTypeData(CardTypeData.EnumCardTypeToID(CardTypeData.CardType.Base))
			, out baseCardList
		);

		if(isFound)
		{
			int r_index = Random.Range(0, baseCardList.Count); // Random select base

			//Debug.Log("Bot Base:" + baseCardList[r_index].PlayerCardData.Name);
			OnSelectBase(baseCardList[r_index]);
		}
		else
		{
			Debug.LogError("BotControl/StartSetupBase: not found any base card in deck.");
		}
	}

	override protected void StartSetupLeader()
	{
		// Random leader card in deck.
		List<UniqueCardData> leaderCardList;
		bool isFound = _battleController.GetPlayerBattleData().Deck.FindAll(
              new CardSubTypeData(CardSubTypeData.EnumCardSubTypeToID(CardSubTypeData.CardSubType.Unique))
			, out leaderCardList
		);

		if(isFound)
		{
			//int r_index = Random.Range(0, leaderCardList.Count); // Random select leader

			//Debug.Log("Bot Leader:" + leaderCardList[r_index].PlayerCardData.Name);
			int selectIndex = 0;

			string npcID = BattleManager.Instance.PlayersData[(int)this.PlayerIndex].ID;
			NPCDBData npcData;
			bool isSuccess = NPCDB.GetData(npcID, out npcData);
			bool isMatch = false;
			if (isSuccess)
			{
				
				for (int index = 0; index < leaderCardList.Count; ++index)
				{
					if (string.Compare (leaderCardList [index].PlayerCardData.PlayerCardID, npcData.RewardCardID) == 0)
					{
						selectIndex = index;
						isMatch = true;
						break;
					}
				}
			} 

			if(!isMatch)
			{
				selectIndex = Random.Range(0, leaderCardList.Count); // Random select leader
			}

			OnSelectLeader(leaderCardList[selectIndex]);
		}
		else
		{
			Debug.LogError("BotControl/StartSetupLeader: not found any leader card in deck.");
		}
	}

    override protected void StartReHand()
    {
        // Not re-hand.
        OnSelectReHand(false);
    }
		
	protected bool OnPlayCardType(CardTypeData.CardType cardType)
	{
		//Debug.Log("Bot: OnPlayCardType " + cardType.ToString());

		List<UniqueCardData> handCardList;
		if(FindInHand(cardType, out handCardList))
		{
			if(handCardList != null && handCardList.Count > 0)
			{
				foreach(UniqueCardData handCard in handCardList)
				{
					bool isCanUse = BattleManager.Instance.IsCanUseGold(this.PlayerIndex, handCard.PlayerCardData.Cost);
					if(isCanUse)
					{
						switch(cardType)
						{
							case CardTypeData.CardType.Base:
							case CardTypeData.CardType.Structure:
							case CardTypeData.CardType.Unit: 
							{
								BattleManager.Instance.RequestSummonUniqueCard(handCard.UniqueCardID, true);
								return true;
							}
								
							case CardTypeData.CardType.Technic:
							case CardTypeData.CardType.Event:
							{
								if(!handCard.IsInitAbility)
								{
									handCard.InitAbility();
								}

								if(handCard.IsCanUse())
								{
									BattleManager.Instance.RequestUseHandCard(handCard.UniqueCardID, true);
									return true;
								}
								else
								{
									return false;
								}
							}
						
						}
					}
				}
			}	
		}
		return false;
	}

	protected bool OnPlaySubType(CardSubTypeData.CardSubType subType)
	{
		//Debug.Log("Bot: OnPlaySubType " + subType.ToString());

		List<UniqueCardData> handCardList;
		if(FindInHand(subType, out handCardList))
		{
			if(handCardList != null && handCardList.Count > 0)
			{
				foreach(UniqueCardData handCard in handCardList)
				{
					bool isCanUse = BattleManager.Instance.IsCanUseGold(this.PlayerIndex, handCard.PlayerCardData.Cost);
					if(isCanUse)
					{
						//Summon card
						BattleManager.Instance.RequestSummonUniqueCard(handCard.UniqueCardID, true);
						return true;
					}
				}
			}	
		}
		return false;
	}

	protected bool OnPlayLeader()
	{
		//Debug.Log("Bot: OnPlayLeader");

		List<BattleCardData> cardList;
		this.BattleController.GetPlayerBattleData().GetAllSummonedCard(out cardList);

		if(cardList != null && cardList.Count > 0)
		{
			foreach(BattleCardData card in cardList)
			{
				if(card.IsLeader)
				{
					if(card.IsFaceDown)
					{
						bool isCanUse = BattleManager.Instance.IsCanUseGold(this.PlayerIndex, card.PlayerCardData.Cost);
						if(isCanUse)
						{
							//Summon card
							BattleManager.Instance.RequestFaceupBattleCard(card.BattleCardID, true);
							return true;
						}
					}
					else
					{
						break;
					}
				}
			}
		}

		return false;
	}

	protected bool FindAndPlayCard(CardTypeData.CardType cardType)
	{
		bool isAction = false;

		// Check Resource.
		List<UniqueCardData> resultList;
		bool isFound = FindInHand(cardType, out resultList);
		if(isFound)
		{	
			// Has Resource.
			foreach(UniqueCardData card in resultList)
			{
				bool isCanUse = BattleManager.Instance.IsCanUseGold(this.PlayerIndex, card.PlayerCardData.Cost);
				if(isCanUse)
				{
					//Summon card
					BattleManager.Instance.PopupRequestSummonHandCard(card.UniqueCardID);
					//Debug.Log("Play: " + card.PlayerCardData.Name + " (-" + card.PlayerCardData.Cost.ToString() + "G.): " + this.BattleController.GetPlayerBattleData().Gold + "G.");
					isAction = true;
					return isAction;
				}
			}
		}

		return isAction;
	}

	protected bool FindAndPlayCard(CardSubTypeData.CardSubType cardSubType)
	{
		bool isAction = false;

		// Check Resource.
		List<UniqueCardData> resultList;
		bool isFound = FindInHand(cardSubType, out resultList);
		if(isFound)
		{	
			// Has Resource.
			foreach(UniqueCardData card in resultList)
			{
				bool isCanUse = BattleManager.Instance.IsCanUseGold(this.PlayerIndex, card.PlayerCardData.Cost);
				if(isCanUse)
				{
					//Summon card
					BattleManager.Instance.PopupRequestSummonHandCard(card.UniqueCardID);
					//Debug.Log("Play: " + card.PlayerCardData.Name + " (-" + card.PlayerCardData.Cost.ToString() + "G.): " + this.BattleController.GetPlayerBattleData().Gold + "G.");
					isAction = true;
					return isAction;
				}
			}
		}

		return isAction;
	}
		
	protected bool FindInHand(CardTypeData.CardType cardType, out List<UniqueCardData> resultList)
	{
		return this.BattleController.GetPlayerBattleData().FindUniqueCardIn(cardType, CardZoneIndex.Hand, out resultList);
	}

	protected bool FindInHand(CardSubTypeData.CardSubType cardSubType, out List<UniqueCardData> resultList)
	{
		return this.BattleController.GetPlayerBattleData().FindUniqueCardIn(cardSubType, CardZoneIndex.Hand, out resultList);
	}

	protected bool FindInBattlefield(CardTypeData.CardType cardType, out List<BattleCardData> resultList)
	{
		List<BattleCardData> cardList;
		this.BattleController.GetPlayerBattleData().GetAllSummonedCard(out cardList);

		if(cardList != null && cardList.Count > 0)
		{
			resultList = new List<BattleCardData>();
			foreach(BattleCardData card in cardList)
			{
				if(card.TypeData == cardType)
				{
					resultList.Add(card);
				}
			}

			if(resultList != null && resultList.Count > 0)
			{
				return true;
			}
		}

		resultList = null;
		return false;
	}

	protected bool FindInBattlefield(CardSubTypeData.CardSubType cardSubType, out List<BattleCardData> resultList)
	{
		List<BattleCardData> cardList;
		this.BattleController.GetPlayerBattleData().GetAllSummonedCard(out cardList);

		if(cardList != null && cardList.Count > 0)
		{
			resultList = new List<BattleCardData>();
			foreach(BattleCardData card in cardList)
			{
				foreach(CardSubTypeData subType in card.SubTypeDataList)
				{
					if(subType == cardSubType)
					{
						resultList.Add(card);
						break;
					}
				}
			}

			if(resultList != null && resultList.Count > 0)
			{
				return true;
			}
		}

		resultList = null;
		return false;
	}

	protected void GetAllBattleCard(out List<BattleCardData> resultList)
	{
		this.BattleController.GetPlayerBattleData().GetAllSummonedCard(out resultList);
	}		
}
