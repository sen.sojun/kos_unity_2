﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SinglePlayerUI : MonoBehaviour {

    // Use this for initialization
    public List<Image> ForFirstFlowCover;
    public PopupBoxUINew PopUpBoxUINew;

    #region Tutorial
    public List<string> PopUpMessageList;
    public List<string> PopUpButtonList;
    public Text Version;
    //private bool testBool = false; 
    private string Tutorial4 = "T0004"; 
    private string Tutorial5 = "T0005"; 
  
    public List<Image> FirstFlowButtonToHide;
    public bool forcePlayerFirstTime = false;
    public Image imgHighlight;
    //for tutorial database
    private List<TutorialData> _tutorialDataList = null;
    #endregion


    #region Popup UI Methods
    public void ShowErrorVersionPopUp(string header, string message, string buttonText)
    {
        PopUpBoxUINew.ShowMessageUI(
             message
           , buttonText
           , () =>
           {
               {
                   this.ClosePopUpUI();
               }
           }
           , false
       );
    }

    public void ShowPopupUI(int index)
    {
        TutorialData data = _tutorialDataList[index++];

        string title = "";
        string message = "";
        string buttonText = "";
        if (LocalizationManager.IsCurrentLanguage(LocalizationManager.Language.Thai))
        {
            title = data.TitleTh;
            message = data.TextTh;
        }
        else
        {
            title = data.TitleEng;
            message = data.TextEng;
        }

        buttonText = Localization.Get(data.ButtonKey);

        if (string.Compare(data.DisplayMode, "DIALOG") == 0)
        {
            PopUpBoxUINew.ShowMessageUI(
                  message
                , buttonText
                , () =>
                {
                    if (index < _tutorialDataList.Count)
                    {
                        ShowPopupUI(index);
                    }
                    else
                    {
                        this.ClosePopUpUI();
                    }
                }
                , false
            );
        }
        else
        {
            PopUpBoxUINew.ShowImageUI(
                  data.ImagePath
                , title
                , data.PageText
                , message
                , buttonText
                , () =>
                {
                    if (index < _tutorialDataList.Count)
                    {
                        ShowPopupUI(index);
                    }
                    else
                    {
                        this.ClosePopUpUI();
                    }
                }
                , false
            );
        }

        PopUpBoxUINew.gameObject.SetActive(true);
    }


    public void ClosePopUpUI()
    {
        PopUpBoxUINew.gameObject.SetActive(false);
    }
    #endregion

   
    public List<TutorialData> LoadFirstFlowTutorial(string tutorialCode, bool isFirstFlow = true)
   {
    // Load Tutorial1
    List<TutorialData> tutorialList = new List<TutorialData>();

    List<TutorialDBData> dataList;
    bool _isSuccess = TutorialDB.GetDataByCode(tutorialCode, out dataList);
    if (_isSuccess)
    {
        List<TutorialDBData> showList = new List<TutorialDBData>();

        // Filter not show in first flow.
        foreach (TutorialDBData data in dataList)
        {
            if (isFirstFlow)
            {
                Debug.Log("data = " + data.TextEng);
                if (data.ShowInFirstFlow > 0)
                {
                    showList.Add(data);
                }
            }
            else
            {
                showList.Add(data);
            }
        }

        int page = 0;
        foreach (TutorialDBData data in showList)
        {
            TutorialData tutorialData = new TutorialData(data, (++page).ToString() + "/" + showList.Count.ToString());
            tutorialList.Add(tutorialData);
        }
    }

    return tutorialList;
}


	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ShowUI()
    {
        bool firstTime = false;
        TopBarManager.Instance.SetUpBar(
              true
            , Localization.Get("BATTLE_SINGLE_TITLE") //"SINGLE PLAYER"
            , MenuManagerV2.Instance.ShowBattleUI);

        gameObject.SetActive(true);

        //PlayerSave.SavePlayerFirstTime(true);
        if (PlayerSave.IsPlayerFirstTime() || forcePlayerFirstTime == true)
        {
            firstTime = true;
            imgHighlight.gameObject.SetActive(firstTime);

            for (int i = 0; i < ForFirstFlowCover.Count; ++i)
            {
                ForFirstFlowCover[i].gameObject.SetActive(firstTime);
            }

            // Load first time tutorial
            _tutorialDataList = new List<TutorialData>();

            // Load Tutorial1
            _tutorialDataList.AddRange(LoadFirstFlowTutorial(Tutorial4, true));

            // Load Tutorial2
            _tutorialDataList.AddRange(LoadFirstFlowTutorial(Tutorial5, true));

            ShowPopupUI(0);
        }
        else
        {
            firstTime = false;
            imgHighlight.gameObject.SetActive(false);
            for (int i = 0; i < ForFirstFlowCover.Count; ++i)
            {
                        ForFirstFlowCover[i].gameObject.SetActive(firstTime);
            }

        }

      
    }



    public void HideUI()
    {
        gameObject.SetActive(false);
    }

}
