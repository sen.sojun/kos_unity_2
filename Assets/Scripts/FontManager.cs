﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FontManager
{
    #region Private Properties
    private static string _fontPath = "Fonts/";
    private static Font[] _fonts = null;
    #endregion

    #region Methods
    public static Font GetFont(LocalizationManager.Language language)
    {
        if(_fonts == null || _fonts[(int)language] == null)
    
        LoadFont();
    
        return _fonts[(int)language];
    }
    
    private static void LoadFont()
    {
        _fonts = new Font[System.Enum.GetValues(typeof(LocalizationManager.Language)).Length];
        
        _fonts[(int)LocalizationManager.Language.English] = Resources.Load<Font>(_fontPath + "Kanit-Bold");
        _fonts[(int)LocalizationManager.Language.Thai] = Resources.Load<Font>(_fontPath + "Pridi-500");
    }
    #endregion
}
