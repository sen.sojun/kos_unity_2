﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LoadingUI : MonoBehaviour 
{
	// Use this for initialization
	void Start () 
	{
		//HideUI();
	}
	
	// Update is called once per frame
	void Update () 
	{
	}

	public void ShowUI()
	{
		gameObject.SetActive(true);
		BattleUIManager.RequestBringForward(gameObject);
	}

	public void HideUI()
	{
		gameObject.SetActive(false);
	}
}
