﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class NPCDetailUI : MonoBehaviour 
{
	public MenuManager MenuManager;

	public UISprite NPCImage;

	public UILabel NameLabel;
	public UILabel DiffLabel;
	public UILabel WinLabel;
	public UILabel DescriptionLabel;

	public UIButton StartButton;
	public UIButton CloseButton;

	private NPCData _npcData;
	public bool IsPlayFirstTime { get { return (MenuManager.ForcePlayFirstTime || PlayerSave.IsPlayerFirstTime()); } }

	// Use this for initialization
	void Start () 
	{
	
	}

	/*
	// Update is called once per frame
	void Update () 
	{
	
	}
	*/

	public void ShowUI(string npcID)
	{
		_npcData = new NPCData(npcID);
        //Debug.Log(_npcData.NPCID);

        string name = "";
        string description = "";
		string deckStyle = "";
        switch (LocalizationManager.CurrentLanguage)
        {
            case LocalizationManager.Language.Thai:
            {
                name = _npcData.Name_TH;
                description = _npcData.Description_TH;
				deckStyle = _npcData.DeckStyle_TH;
            }
            break;

            case LocalizationManager.Language.English:
            default:
            {
                name = _npcData.Name_EN;
                description = _npcData.Description_EN;
				deckStyle = _npcData.DeckStyle_EN;
            }
            break;
        }

        NameLabel.text = name;
		NPCImage.spriteName = _npcData.ImageName;
		DiffLabel.text = deckStyle;

		WinLabel.text = Localization.Get("NPC_DETAIL_WIN") + ": " + PlayerSave.GetWinCount(_npcData.NPCID).ToString() + " " + Localization.Get("NPC_DETAIL_WIN_TIMES");
        DescriptionLabel.text = description;

        //hide close button if play first time
        CloseButton.gameObject.SetActive(!IsPlayFirstTime);

		gameObject.SetActive(true);
		BattleUIManager.RequestBringForward(gameObject);
	}

	public void OnStartClicked()
	{
		MatchMakingManager mmm = GameObject.FindObjectOfType<MatchMakingManager>();

		bool isPlayFirstTime = (MenuManager.ForcePlayFirstTime || PlayerSave.IsPlayerFirstTime());
		if(IsPlayFirstTime)
		{
			bool isSuccess = mmm.CreateTutorialData(_npcData);
			if(isSuccess)
			{
				// Load BattleScene
				PlayerSave.SavePlayFromSoloLeague (false); //+vit13052017
                KOSNetwork.NetworkSetting.IsNetworkEnabled = false;
				MenuManager.LoadScene("NetworkBattleScene");
			}
		}
		else
		{
			bool isSuccess = mmm.CreateVSBotData(_npcData, 1);
			if(isSuccess)
			{
				// Load BattleScene
				PlayerSave.SavePlayFromSoloLeague (false); //+vit13052017
                KOSNetwork.NetworkSetting.IsNetworkEnabled = false;
				MenuManager.LoadScene("NetworkBattleScene");
			}
		}
	}

	public void ShowNPCReward()
	{
		//Debug.Log(_npcData.NPCID);
		MenuManager.ShowNPCReward(_npcData.NPCID);
	}

	public void HideUI()
	{
		gameObject.SetActive(false);
	}
}
