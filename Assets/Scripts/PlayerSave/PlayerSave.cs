﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerSave 
{
    public enum SaveIndex
    {
          Deck
        , CardStash
        , SilverCoin
        , GoldCoin
        , CardCollection
        , PlayerFirstTime
        , WinCount
        , RatingAtTitle
        , RatingAtBattle
        , RatingShownFinish
        , SoundMusicVolume
        , SoundEffectVolume
        , DeckEditorFirstTime
        , Language
        , Got500SilverFirstTime
        , PlayerDiamond
        , PlayerExp
        , NpcSoloLevel
        , SoloWin
        , SoloLose
        , SoloDraw
        , PlayFromSoloLeague
        , NotificationRun
        , NotificationTime
        , PlayerEmail
        , PlayerPassword
        , PlayerName
        , PlayerUid
        , PlayerAccountType
        , PlayerAvatar
        , BuildVersion
        , GuestNeedRegister
    }

	private static string _deckKey = "playerDeck";
	private static string _cardStashKey = "playerCardStash";
	private static string _silverCoinKey = "playerSilverCoin";
	private static string _goldCoinKey = "playerGoldCoin";
	private static string _cardCollection = "playerCardCollection";
	private static string _playerFirstTimeKey = "playerFirstTime";
	private static string _winCountKey = "playerWinCount";

    private static string _autoLoginKey = "playerAutoLogin";
    private static string _submitGameDataKey = "_submitGameData";
    private static string _mergeStashDeckKey = "_mergeStashDeck";

	private static string _ratingAtTitle = "ratingAtTitle";
	private static string _ratingAtBattle = "ratingAtBattle";
	private static string _ratingShownFinish = "ratingShownFinish";

	private static string _soundMusicVolumeKey = "Sound_MusicVolume";
	private static string _soundEffectVolumeKey = "Sound_EffectVolume";

	private static string _deckEditorFirstTimeKey = "DeckEditorFirstTime";
    private static string _languageKey = "GameLanguageKey";

	private static string _got500SilverFirstTimeKey = "Got500SilverFirstTime";

	private static string _playerDiamondKey = "PlayerDiamond";
	private static string _playerExpKey = "PlayerExp";
	private static string _playerLevelKey = "PlayerLevel";
	private static string _npcSoloLevelKey = "NpcSoloLevel";
	private static string _soloWinKey = "SoloWin";
	private static string _soloLoseKey = "SoloLose";
	private static string _soloDrawKey = "SoloDraw";
	private static string _playFromSoloLeagueKey = "PlayFromSoloLeague";
	private static string _deckEditorFromSoloLoeagueKey = "DeckEditorFromSoloLeague";
	private static string _shopFromSoloLoeagueKey = "ShopFromSoloLeague";

	private static string _notificationRun = "NotificationRun";
    private static string _notificationTime = "NotificationTime";

    private static string _playerEmailKey = "PlayerEmail";
    private static string _playerPasswordKey = "PlayerPassword";
    private static string _playerNameKey = "PlayerName";
    private static string _playerUidKey = "PlayerUid";
    private static string _playerAccountTypeKey = "PlayerAccountType";
    private static string _playerAvatarKey = "PlayerAvatar";
    private static string _buildVersionKey = "KOSBuildVersion";
    private static string _guestNeedRegisterKey = "GuestNeedRegister";

	public static void ClearAllSave()
	{
		PlayerPrefs.DeleteAll();
	}

	public static bool ClearSave(SaveIndex saveIndex)
	{
		switch (saveIndex) 
		{
			case SaveIndex.Deck:
			{
				PlayerPrefs.DeleteKey (_deckKey);
				return true;
			}

			case SaveIndex.CardStash:
			{
				PlayerPrefs.DeleteKey (_cardStashKey);
				return true;
			}

			case SaveIndex.SilverCoin:
			{
				PlayerPrefs.DeleteKey (_silverCoinKey);
				return true;
			}

			case SaveIndex.GoldCoin:
			{
				PlayerPrefs.DeleteKey (_goldCoinKey);
				return true;
			}

			case SaveIndex.CardCollection:
			{
				PlayerPrefs.DeleteKey (_cardCollection);
				return true;
			}

			case SaveIndex.PlayerFirstTime:
			{
				PlayerPrefs.DeleteKey (_playerFirstTimeKey);
				return true;
			}

			case SaveIndex.WinCount:
			{
				List<NPCDBData> dataList;
				NPCDB.GetAllData(out dataList);

				if(dataList != null && dataList.Count > 0)
				{
					foreach(NPCDBData data in dataList)
					{
						string key = _winCountKey + "_" + data.NPCID;
						if(PlayerPrefs.HasKey(key))
						{
							PlayerPrefs.DeleteKey (key);
						}
					}
				}
				return true;
			}
      
			case SaveIndex.RatingAtTitle:
			{
				PlayerPrefs.DeleteKey (_ratingAtTitle);
				return true;
			}

			case SaveIndex.RatingAtBattle:
			{
				PlayerPrefs.DeleteKey (_ratingAtBattle);
				return true;
			}

			case SaveIndex.RatingShownFinish:
			{
				PlayerPrefs.DeleteKey (_ratingShownFinish);
				return true;
			}

			case SaveIndex.SoundMusicVolume:
			{
				PlayerPrefs.DeleteKey (_soundMusicVolumeKey);
				return true;
			}

			case SaveIndex.SoundEffectVolume:
			{
				PlayerPrefs.DeleteKey (_soundEffectVolumeKey);
				return true;
			}

		    case SaveIndex.DeckEditorFirstTime:
			{
				PlayerPrefs.DeleteKey (_deckEditorFirstTimeKey);
				return true;
			}

            case SaveIndex.Language:
            {
                PlayerPrefs.DeleteKey(_languageKey);
                return true;
            }

			//for solo league
		    case SaveIndex.PlayerDiamond:
			{
				PlayerPrefs.DeleteKey (_playerDiamondKey);
				return true;
			}

			//for solo league
		    case SaveIndex.PlayerExp:
			{
				PlayerPrefs.DeleteKey (_playerExpKey);
				return true;
			}
				
			//for solo league
		    case SaveIndex.NpcSoloLevel:
			{
				PlayerPrefs.DeleteKey (_playerLevelKey);
				return true;
			}

			//for solo league
		    case SaveIndex.SoloWin:
			{
				PlayerPrefs.DeleteKey (_soloWinKey);
				return true;
			}

			//for solo league
		    case SaveIndex.SoloLose:
			{
				PlayerPrefs.DeleteKey (_soloLoseKey);
				return true;
			}

			//for solo league
		    case SaveIndex.SoloDraw:
			{
				PlayerPrefs.DeleteKey (_soloDrawKey);
				return true;
			}

			//for solo league
		    case SaveIndex.PlayFromSoloLeague:
			{
				PlayerPrefs.DeleteKey (_playFromSoloLeagueKey);
				return true;
			}

			//for notification
		    case SaveIndex.NotificationRun:
			{
				PlayerPrefs.DeleteKey (_notificationRun);
				return true;
			}

            case SaveIndex.NotificationTime:
                {
                    PlayerPrefs.DeleteKey(_notificationTime);
                    return true;
                }
        
            case SaveIndex.PlayerEmail:
                {
                    PlayerPrefs.DeleteKey(_playerEmailKey);
                    return true;
                }
            
            case SaveIndex.PlayerPassword:
                {
                    PlayerPrefs.DeleteKey(_playerPasswordKey);
                    return true;
                }

            case SaveIndex.PlayerAccountType:
                {
                    PlayerPrefs.DeleteKey(_playerAccountTypeKey);
                    return true;
                }

            case SaveIndex.PlayerUid:
                {
                    PlayerPrefs.DeleteKey(_playerUidKey);
                    return true;
                }

            case SaveIndex.PlayerAvatar:
                {
                    PlayerPrefs.DeleteKey(_playerAvatarKey);
                    return true;
                }
                
            case SaveIndex.BuildVersion:
                {
                    PlayerPrefs.DeleteKey(_buildVersionKey);
                    return true;
                }

            case SaveIndex.GuestNeedRegister:
                {
                    PlayerPrefs.DeleteKey(_guestNeedRegisterKey);
                    return true;
                }
         
            //break;
            default:
			return false;
				//return false;
		}

	}

    public static bool IsAutoLogin()
    {
        int index = PlayerPrefs.GetInt(_autoLoginKey, 0);

        return (index != 0);
    }

    public static void SetAutoLogin(bool isAutoLogin)
    {
        PlayerPrefs.SetInt(_autoLoginKey, isAutoLogin ? 1 : 0);
    }

    public static bool IsSubmitGameData()
    {
        int index = PlayerPrefs.GetInt(_submitGameDataKey, 0);
        
        return (index != 0);
    }
    
    public static void SetSubmitGameData(bool isSubmit)
    {
        PlayerPrefs.SetInt(_submitGameDataKey, isSubmit ? 1 : 0);
    }

    public static bool IsMergeStashDeck()
    {
        int index = PlayerPrefs.GetInt(_mergeStashDeckKey, 0);

        return (index != 0);
    }

    public static void SetMergeStashDeck(bool isMerge)
    {
        PlayerPrefs.SetInt(_mergeStashDeckKey, isMerge ? 1 : 0);
    }

	public static void GenerateDummySave(int dummySilverCoin = 10, int maxDeckSize = 40)
	{
		bool isSuccess = false;
		List<CardDBData> rawDataList;

		isSuccess = CardDB.GatAllData(out rawDataList);
		if(isSuccess)
		{
			// Generate dummy card list.
			List<PlayerCardData> dummyCardStashList = new List<PlayerCardData>();
			List<CardData> dummyCardCollectionList = new List<CardData>();
			DeckData dummyDeck = new DeckData();

			foreach(CardDBData cardDBData in rawDataList)
			{
				CardData dummyCardData = new CardData (cardDBData.CardID);
				PlayerCardData dummyPlayerCardData = new PlayerCardData(dummyCardData);

				if ((Random.Range (0.0f, 1.0f) > 0.5f)) {
					dummyCardCollectionList.Add (dummyCardData);
				}
				dummyCardStashList.Add(dummyPlayerCardData);

				if(dummyDeck.Count < maxDeckSize)
				{
					dummyDeck.Add(dummyPlayerCardData);
				}
			}

			// Save to deck.
			SavePlayerDeck(dummyDeck);

			// Save to card stash.
			SavePlayerCardStash(dummyCardStashList);

			// Save to silver coin.
			SavePlayerSilverCoin(dummySilverCoin);

			// Save to card collection.
			SavePlayerCardCollection(dummyCardCollectionList);

			//for solo league
			SavePlayerExp(0);

			//for solo leage
			SavePlayerDiamond(10);

			//for solo league
			SaveSoloWin(0);

			//for solo league
			SaveSoloLose(0);

			//for solo league
			SaveSoloDraw(0);

			//for solo league
			SavePlayFromSoloLeague (false);

			//for notification
			SaveNotificationRun(false);
		}
		else
		{
			Debug.LogError("PlayerSave/GenerateDummySave: CardDB.GatAllData failed.");
		}
	}
		
	public static void GenerateFirstDeckSave(int dummySilverCoin = 10, int maxDeckSize = 40)
	{
		bool isSuccess = false;
		List<CardDBData> rawDataList;

		isSuccess = CardDB.GatAllData(out rawDataList);
		if(isSuccess)
		{
			// Generate dummy card list.
			List<PlayerCardData> dummyCardStashList = new List<PlayerCardData>();
			List<CardData> dummyCardCollectionList = new List<CardData>();
			DeckData dummyDeck = new DeckData();

			foreach(CardDBData cardDBData in rawDataList)
			{
				CardData dummyCardData = new CardData (cardDBData.CardID);
				PlayerCardData dummyPlayerCardData = new PlayerCardData(dummyCardData);

				if ((Random.Range (0.0f, 1.0f) > 0.5f)) {
					dummyCardCollectionList.Add (dummyCardData);
				}
				dummyCardStashList.Add(dummyPlayerCardData);

				if(dummyDeck.Count < maxDeckSize)
				{
					dummyDeck.Add(dummyPlayerCardData);
				}
			}

			// Save to deck.
			SavePlayerDeck(dummyDeck);

			// Save to card stash.
			SavePlayerCardStash(dummyCardStashList);

			// Save to silver coin.
			SavePlayerSilverCoin(dummySilverCoin);

			// Save to card collection.
			SavePlayerCardCollection(dummyCardCollectionList);

			//for solo league
			SavePlayerExp(0);

			//for solo leage
			SavePlayerDiamond(0);

			//for solo league
			SaveNpcSoloLevel(1);

			//for solo league
			SaveSoloWin(0);

			//for solo league
			SaveSoloLose(0);

			//for solo league
			SaveSoloDraw(0);

			//for solo league
			SavePlayFromSoloLeague (false);
		
		}
		else
		{
			Debug.LogError("PlayerSave/GenerateDummySave: CardDB.GatAllData failed.");
		}
	}
		
	public static void SavePlayerDeck(DeckData deck)
	{
		List<string> deckCardIDList = new List<string>();

        deck.Sort();

		for(int index = 0; index < deck.Count; ++index)
		{
			deckCardIDList.Add(deck.GetCardAt(index).PlayerCardData.PlayerCardID);
		}

		PlayerPrefsX.SetStringArray(_deckKey, deckCardIDList.ToArray());
	}

	public static void SavePlayerCardStash(List<PlayerCardData> cardStash)
	{
		List<string> stashCardIDList = new List<string>();

        cardStash = GameUtility.SortCard(cardStash);

		foreach(PlayerCardData playerCardData in cardStash)
		{
			stashCardIDList.Add(playerCardData.CardID);
		}

		PlayerPrefsX.SetStringArray(_cardStashKey, stashCardIDList.ToArray());
	}

	public static bool SavePlayerCardStash(string cardID)
	{
		List<PlayerCardData> stash;
		bool isSuccess = PlayerSave.GetPlayerCardStash(out stash);
		if(isSuccess)
		{
			stash.Add(new PlayerCardData(cardID));
			PlayerSave.SavePlayerCardStash (stash);

			return true;
		}
		else
		{
			stash = new List<PlayerCardData>();
			stash.Add(new PlayerCardData(cardID));
			PlayerSave.SavePlayerCardStash (stash);

			return true;
		}

		return false;
	}

	public static void SavePlayerCardCollection(List<CardData> cardCollection)
	{
		List<string> collectionCardIDList = new List<string>();

		foreach(CardData cardData in cardCollection)
		{
			collectionCardIDList.Add(cardData.CardID);
		}

		PlayerPrefsX.SetStringArray(_cardCollection, collectionCardIDList.ToArray());
	}

	//when get new card ,save to card collection
	//--16062016
	public static bool SavePlayerCardCollection(string cardID) //List<CardData> cardCollection)
	{
		CardData card = new CardData (cardID);

		List<CardData> collectionList;
		bool isSuccess = PlayerSave.GetPlayerCardCollection(out collectionList);
		if (isSuccess) 
		{
			foreach (CardData cardData in collectionList) 
			{
				if (string.Compare (cardData.CardID, card.CardID) == 0)
					return false;
			}

			collectionList.Add (card);
			PlayerSave.SavePlayerCardCollection (collectionList);
			return true;
		} 
		else 
		{
			collectionList.Add (card);
			PlayerSave.SavePlayerCardCollection (collectionList);
			return true;
		}
	}

	public static void SavePlayerSilverCoin(int silverCoin)
	{
		if(silverCoin >= 0)
		{
			PlayerPrefs.SetInt(_silverCoinKey, silverCoin);
		}
		else
		{
			Debug.LogError("PlayerSave/SavePlayerSilverCoin: silverCoin < 0.");
		}
	}

	public static void SavePlayerGoldCoin(int goldCoin)
	{
		if(goldCoin >= 0)
		{
			PlayerPrefs.SetInt(_goldCoinKey, goldCoin);
		}
		else
		{
			Debug.LogError("PlayerSave/SavePlayerGoldCoin: goldCoin < 0.");
		}
	}

	//for solo league
	public static void SavePlayerExp(int playerExp)
	{
		if(playerExp >= 0)
		{
			PlayerPrefs.SetInt(_playerExpKey, playerExp);
		}
		else
		{
			Debug.LogError("PlayerSave/SavePlayerExp: playerExp < 0.");
		}
	}

	//for solo league
	public static void SaveNpcSoloLevel(int npcLevel)
	{
		if(npcLevel >= 0)
		{
			PlayerPrefs.SetInt(_npcSoloLevelKey, npcLevel);
		}
		else
		{
			Debug.LogError("PlayerSave/SaveNpcSoloLevel: npcSoloLevel < 0.");
		}
	}

	//for solo league
	public static void SavePlayerDiamond(int playerDiamond)
	{
		if(playerDiamond >= 0)
		{
			PlayerPrefs.SetInt(_playerDiamondKey, playerDiamond);
		}
		else
		{
			Debug.LogError("PlayerSave/SavePlayerDiamond: playerDiamond < 0.");
		}
	}

	//for solo league
	public static void SaveSoloWin(int soloWin)
	{
		if(soloWin >= 0)
		{
			PlayerPrefs.SetInt(_soloWinKey, soloWin);
		}
		else
		{
			Debug.LogError("PlayerSave/SaveSoloWin: soloWin < 0.");
		}
	}

	//for solo league
	public static void SaveSoloLose(int soloLose)
	{
		if(soloLose >= 0)
		{
			PlayerPrefs.SetInt(_soloWinKey, soloLose);
		}
		else
		{
			Debug.LogError("PlayerSave/SaveSoloLose: soloLose < 0.");
		}
	}

	//for solo league
	public static void SaveSoloDraw(int soloDraw)
	{
		if(soloDraw >= 0)
		{
			PlayerPrefs.SetInt(_soloDrawKey, soloDraw);
		}
		else
		{
			Debug.LogError("PlayerSave/SaveSoloDraw: soloDraw < 0.");
		}
	}

	//for solo league
	//18019

    //for firebase
    public static void SavePlayerEmail(string playerEmail)
    {
        if(playerEmail != "")
        {
            PlayerPrefs.SetString(_playerEmailKey,playerEmail);
        }
        else
        {
            Debug.LogError("PlayerSave/SaveServer: invalid email");
        }
    }

    public static void SavePlayerPassword(string playerPassword)
    {
        if (playerPassword != "")
        {
            PlayerPrefs.SetString(_playerPasswordKey, playerPassword);
        }
        else
        {
            Debug.LogError("PlayerSave/SaveServer: invalid password");
        }  
    }

    //for firebase
    public static void SavePlayerName(string playerName)
    {
        if (playerName != "")
        {
            PlayerPrefs.SetString(_playerNameKey, playerName);
        }
        else
        {
            Debug.LogError("PlayerSave/SaveServer: invalid player name");
        }
    }

    //for firebase
    public static void SavePlayerAccountType(AccountType accountType)
    {
        switch(accountType)
        {
            case AccountType.Guest:
                PlayerPrefs.SetInt(_playerAccountTypeKey, 1);
                break;
        
            case AccountType.KOS:
                PlayerPrefs.SetInt(_playerAccountTypeKey, 2);
                break;
        
            case AccountType.New:
            default:
                PlayerPrefs.SetInt(_playerAccountTypeKey, 0);
                break;
        }
    }

    //for firebase
    public static void SavePlayerUid(string playerUid)
    {
        if (playerUid != "")
        {
            PlayerPrefs.SetString(_playerUidKey, playerUid);
        }
        else
        {
            Debug.LogError("PlayerSave/SaveServer: invalid player Uid ");
        }
    }

    //for firebase
    public static void SavePlayerAvatar(string playerAvatar)
    {
        if (playerAvatar != "")
        {
            PlayerPrefs.SetString(_playerAvatarKey, playerAvatar);
        }
        else
        {
            Debug.LogError("PlayerSave/SaveServer: invalid player Avatar ");
        }
    }

	public static void SavePlayFromSoloLeague(bool isPlayFromSoloLeague)
	{
		PlayerPrefs.SetInt(_playFromSoloLeagueKey, ((isPlayFromSoloLeague) ? 1 : 0));
	}

	public static void SaveDeckFromSoloLeague(bool isDeckFromSoloLeague)
	{
		PlayerPrefs.SetInt(_deckEditorFromSoloLoeagueKey, ((isDeckFromSoloLeague) ? 1 : 0));
	}

	public static void SaveShopFromSoloLeague(bool isShopFromSoloLeague)
	{
		PlayerPrefs.SetInt(_shopFromSoloLoeagueKey, ((isShopFromSoloLeague) ? 1 : 0));
	}
//	public static void SaveSoloLeagueFirstTime(bool isSoloFirstTime)
//	{
//		PlayerPrefs.SetInt(_playFromSoloLeagueKey, ((isSoloFirstTime) ? 1 : 0));
//	}

	//for notification
	//03102017
	public static void SaveNotificationRun(bool isNotificationRun)
	{
		PlayerPrefs.SetInt (_notificationRun, ((isNotificationRun) ? 1 : 0));
	}

    public static void SaveNotificationTime(string notiTimeSpan)
    {
        PlayerPrefs.SetString(_notificationTime, notiTimeSpan);
    }


	public static void SavePlayerFirstTime(bool isPlayFirstTime)
	{
        Debug.Log("SavePlayerFirstTime");
    
		PlayerPrefs.SetInt(_playerFirstTimeKey, ((isPlayFirstTime) ? 1 : 0));
	}

    public static void SaveGuestNeedLogin(bool isGuestNeedLogin)
    {
        Debug.Log("GuestNeedLogin");

        PlayerPrefs.SetInt(_guestNeedRegisterKey, ((isGuestNeedLogin) ? 1 : 0));
    }

	public static void SaveRatingAtTitle(int iCount)
	{
		PlayerPrefs.SetInt(_ratingAtTitle,iCount);
	}

	public static void SaveRatingAtBattle(int iCount)
	{
		PlayerPrefs.SetInt(_ratingAtBattle,iCount);
	}

	public static void SaveRatingShownFinish(bool isRatingShownFinish)
	{
		PlayerPrefs.SetInt(_ratingShownFinish, ((isRatingShownFinish) ? 1 : 0));
	}
		
	public static bool GetPlayerDeck(out DeckData deck)
	{
		deck = new DeckData();

		if(PlayerPrefs.HasKey(_deckKey))
		{
			string[] deckCardIDList = PlayerPrefsX.GetStringArray(_deckKey);

			foreach(string deckCardID in deckCardIDList)
			{
				deck.Add(new PlayerCardData(deckCardID));
			}

			return true;
		}
	
		return false;
	}

	public static bool GetPlayerCardStash(out List<PlayerCardData> cardStash)
	{
		cardStash = new List<PlayerCardData>();

		if(PlayerPrefs.HasKey(_cardStashKey))
		{
			string[] stashCardIDList = PlayerPrefsX.GetStringArray(_cardStashKey);

			foreach(string stashCardID in stashCardIDList)
			{
				cardStash.Add(new PlayerCardData(stashCardID));
			}

			return true;
		
		}

        return true;
	}

	public static bool GetPlayerCardCollection(out List<CardData> cardCollection)
	{
		cardCollection = new List<CardData>();

		if(PlayerPrefs.HasKey(_cardCollection))
		{
			string[] cardCollectionIDList = PlayerPrefsX.GetStringArray(_cardCollection);

			foreach(string cardCollectionID in cardCollectionIDList)
			{
				cardCollection.Add(new  CardData(cardCollectionID));
			}

			return true;

		}

        return true;
	}
		
	public static int GetPlayerSilverCoin()
	{
		if(PlayerPrefs.HasKey(_silverCoinKey))
		{
			int silverCoin = PlayerPrefs.GetInt(_silverCoinKey);

			if(silverCoin < 0)
			{
				silverCoin = 0;
				SavePlayerSilverCoin(silverCoin);
			}
		
			return silverCoin;
		}

		return 0;
	}

	public static int GetPlayerGoldCoin()
	{
		if(PlayerPrefs.HasKey(_goldCoinKey))
		{
			int goldCoin = PlayerPrefs.GetInt(_goldCoinKey);

			if(goldCoin < 0)
			{
				goldCoin = 0;
				SavePlayerGoldCoin(goldCoin);
			}

			return goldCoin;
		}

		return 0;
	}


	//for solo league
	public static int GetPlayerExp()
	{
		if(PlayerPrefs.HasKey(_playerExpKey))
		{
			int playerExp = PlayerPrefs.GetInt(_playerExpKey);

			if(playerExp < 0)
			{
				playerExp = 0;
				SavePlayerExp(playerExp);
			}

			return playerExp;
		}

		return 0;
	}

	//for solo league
	public static int GetPlayerLevel()
	{
		return PlayerLevelDB.GetLevel (PlayerSave.GetPlayerExp());
	}

	//for solo League
	public static int GetPlayerExpByLevel(int iLevel)
	{
		int iExp = 0;
		return  PlayerLevelDB.GetExpCom (iLevel);
		//return PlayerLevelDB.GetLevel (iExp);

	}

	//for solo league
	public static int GetNpcSoloLevel()
	{
		if(PlayerPrefs.HasKey(_npcSoloLevelKey))
		{
			int npcSoloLevel = PlayerPrefs.GetInt(_npcSoloLevelKey);

			if(npcSoloLevel < 0)
			{
				npcSoloLevel = 1;
				SaveNpcSoloLevel(npcSoloLevel);
			}

			return npcSoloLevel;
		}

		return 0;
	}
		
	//for solo league
	public static int GetPlayerDiamond()
	{
		if(PlayerPrefs.HasKey(_playerDiamondKey))
		{
			int playerDiamond = PlayerPrefs.GetInt(_playerDiamondKey);

			if(playerDiamond < 0)
			{
				playerDiamond = 0;
				SavePlayerDiamond(playerDiamond);
			}

			return playerDiamond;
		}

		return 0;
	}


	//for solo league
	public static int GetSoloWin()
	{
		if(PlayerPrefs.HasKey(_soloWinKey))
		{
			int soloWin = PlayerPrefs.GetInt(_soloWinKey);

			if(soloWin < 0)
			{
				soloWin = 0;
				SaveSoloWin(soloWin);
			}

			return soloWin;
		}

		return 0;
	}

	//for solo league
	public static int GetSoloLose()
	{
		if(PlayerPrefs.HasKey(_soloLoseKey))
		{
			int soloLose = PlayerPrefs.GetInt(_soloLoseKey);

			if(soloLose < 0)
			{
				soloLose = 0;
				SaveSoloLose(soloLose);
			}

			return soloLose;
		}

		return 0;
	}

	//for solo league
	public static int GetSoloDraw()
	{
		if(PlayerPrefs.HasKey(_soloDrawKey))
		{
			int soloDraw = PlayerPrefs.GetInt(_soloDrawKey);

			if(soloDraw < 0)
			{
				soloDraw = 0;
				SaveSoloDraw(soloDraw);
			}

			return soloDraw;
		}

		return 0;
	}

    //for firebase
    public static string GetPlayerUid()
    {
        #if UNITY_EDITOR
        return "Jyozah67jJU2QJczk7Sp7cRpG4m1";
        #endif
    
        if (PlayerPrefs.HasKey(_playerUidKey))
        {
            return PlayerPrefs.GetString(_playerUidKey);
        }

        return "";
    }

    //for firebase
    public static string GetPlayerEmail()
    {
        if (PlayerPrefs.HasKey(_playerEmailKey))
        {
            return PlayerPrefs.GetString(_playerEmailKey);
        }

        return "";
    }

    //for firebase
    public static string GetPlayerPassword()
    {
        if (PlayerPrefs.HasKey(_playerPasswordKey))
        {
            return PlayerPrefs.GetString(_playerPasswordKey);
        }

        return "";
    }

    public static string GetPlayerName()
    {
        return PlayerPrefs.GetString(_playerNameKey, "Player");
    }

    public static AccountType GetPlayerAccountType()
    {
        int index = PlayerPrefs.GetInt(_playerAccountTypeKey, 0);
        
        switch(index)
        {
            case 1:
            {
                return AccountType.Guest;
            }
            
            case 2:
            {
                return AccountType.KOS;
            }
        
            case 0:
            default:
            {
                return AccountType.New;
            }
        }
    }

    public static string GetPlayerAvatar()
    {
        return PlayerPrefs.GetString(_playerAvatarKey, "01");  //default 01 davox
    }

	//for solo league
	public static bool IsFromSoloLeague()
	{
		//Debug.Log ("test isfromsololeage = " + PlayerPrefs.HasKey (_playFromSoloLeagueKey) + " int " + PlayerPrefs.GetInt(_playFromSoloLeagueKey));
		if(PlayerPrefs.HasKey(_playFromSoloLeagueKey))
		{
			return (PlayerPrefs.GetInt(_playFromSoloLeagueKey) > 0);
		}

		return false;
	}

	//for solo league
	//private static string _deckEditorFromSoloLoeagueKey = "DeckEditorFromSoloLeague";
	//private static string _ShopFromSoloLoeagueKey = "ShopFromSoloLeague";
	public static bool IsDeckEditorFromSoloLeague()
	{
		//Debug.Log ("Deck editor from sololeague = " + PlayerPrefs.HasKey (_deckEditorFromSoloLoeagueKey) + "int " + PlayerPrefs.GetInt (_deckEditorFromSoloLoeagueKey));
		if (PlayerPrefs.HasKey (_deckEditorFromSoloLoeagueKey)) 
		{
			return (PlayerPrefs.GetInt (_deckEditorFromSoloLoeagueKey) > 0);

		}
		return false;

	}

	//for solo league
	public static bool IsShopFromSoloLeague()
	{
		//Debug.Log ("Shop ui from sololeague = " + PlayerPrefs.HasKey (_shopFromSoloLoeagueKey) + "int " + PlayerPrefs.GetInt (_shopFromSoloLoeagueKey));
		if (PlayerPrefs.HasKey (_shopFromSoloLoeagueKey)) 
		{
			return (PlayerPrefs.GetInt (_shopFromSoloLoeagueKey) > 0);

		}
		return false;

	}

	//for notification
	public static bool IsNotificationRun()
	{
		if (PlayerPrefs.HasKey (_notificationRun))
		{
			return (PlayerPrefs.GetInt(_notificationRun) > 0);
		}
		return false;
	}

    public static string GetNotificationDateTime()
    {
        if (PlayerPrefs.HasKey(_notificationTime))
        {
            return (PlayerPrefs.GetString(_notificationTime));
        }
        return "";
    }

//	public static bool IsRatingShownFinish()
//	{
//		if(PlayerPrefs.HasKey(_ratingShownFinish))
//		{
//			Debug.Log ("PlayerPrefs.GetInt(_ratingShownFinish) " + PlayerPrefs.GetInt (_ratingShownFinish));
//			return (PlayerPrefs.GetInt(_ratingShownFinish) > 0);
//		}
//
//		Debug.Log ("PlayerPrefs.GetInt(_ratingShownFinish) " + PlayerPrefs.GetInt (_ratingShownFinish));
//		return false;
//	}


//	public static bool IsPlaySoloLeagueFirstTime()
//	{
//		if(PlayerPrefs.HasKey(_playFromSoloLeagueKey))
//		{
//			return (PlayerPrefs.GetInt(_playerFirstTimeKey) > 0);
//		}
//
//		return true;
//	}


	public static bool IsPlayerFirstTime()
	{
		if(PlayerPrefs.HasKey(_playerFirstTimeKey))
		{
            //Debug.Log ("PlayerPrefs.GetInt(_playerFirstTimeKey) " + PlayerPrefs.GetInt (_playerFirstTimeKey));
			return (PlayerPrefs.GetInt(_playerFirstTimeKey) > 0);
		}
        
		return true;
	}

    public static bool isGuestNeedLogin()
    {
        if(PlayerPrefs.HasKey(_guestNeedRegisterKey))
        {
            return (PlayerPrefs.GetInt(_guestNeedRegisterKey) > 0);
        }

        return true;
    }

	public static int GetWinCount(string npcID)
	{
		string key = _winCountKey + "_" + npcID;

		return (PlayerPrefs.GetInt(key, 0));
	}

	public static void SetWinCount(string npcID, int winCount)
	{
		string key = _winCountKey + "_" + npcID;

		PlayerPrefs.SetInt(key, winCount);
	}

	public static bool IsRatingShownFinish()
	{
		if(PlayerPrefs.HasKey(_ratingShownFinish))
		{
			//Debug.Log ("PlayerPrefs.GetInt(_ratingShownFinish) " + PlayerPrefs.GetInt (_ratingShownFinish));
			return (PlayerPrefs.GetInt(_ratingShownFinish) > 0);
		}

		//Debug.Log ("PlayerPrefs.GetInt(_ratingShownFinish) " + PlayerPrefs.GetInt (_ratingShownFinish));
		return false;
	}

	public static int GetRatingAtTitle()
	{
		return PlayerPrefs.GetInt(_ratingAtTitle, 0);
	}

	public static int GetRatingAtBattle()
	{
		if(PlayerPrefs.HasKey(_ratingAtBattle))
		{
			int ratingAtBattle = PlayerPrefs.GetInt(_ratingAtBattle);

			if(ratingAtBattle < 0)
			{
				ratingAtBattle = 0;
			}

			return ratingAtBattle;
		}

		return 0;
	}

	public static float GetMusicVolume()
	{
		return PlayerPrefs.GetFloat(_soundMusicVolumeKey, SoundManager.MusicVolumeDefault);
	}
	public static void SaveMusicVolume(float volume)
	{
		PlayerPrefs.SetFloat(_soundMusicVolumeKey, volume);
	}

	public static float GetEffectVolume()
	{
		return PlayerPrefs.GetFloat(_soundEffectVolumeKey, SoundManager.EffectVolumeDefault);
	}
	public static void SaveEffectVolume(float volume)
	{
		PlayerPrefs.SetFloat(_soundEffectVolumeKey, volume);
	}

	public static void SaveDeckEditorFirstTime(bool isPlayFirstTime)
	{
		PlayerPrefs.SetInt(_deckEditorFirstTimeKey, ((isPlayFirstTime) ? 1 : 0));
	}
		
	public static bool IsDeckEditorFirstTime()
	{
		if(PlayerPrefs.HasKey(_deckEditorFirstTimeKey))
		{
			return (PlayerPrefs.GetInt(_deckEditorFirstTimeKey) > 0);
		}

		return true;
	}

    public static LocalizationManager.Language GetLanguage()
    {
        if (PlayerPrefs.HasKey(_languageKey))
        {
            int languageIndex = PlayerPrefs.GetInt(_languageKey);
            //Debug.Log("GetLanguage:" + languageIndex);

            return (LocalizationManager.Language)languageIndex;
        }
        else
        {
            //Debug.Log("GetLanguage: Not found.");
            SystemLanguage systemLanguage = Application.systemLanguage;
            switch (systemLanguage)
            {
                case SystemLanguage.Thai: return LocalizationManager.Language.Thai;

                case SystemLanguage.English:
                default: return LocalizationManager.Language.English;
            }
        }
    }

    public static void SaveLanguage(LocalizationManager.Language language)
    {
        //Debug.Log("SaveLanguage: " + language);
        PlayerPrefs.SetInt(_languageKey, (int)language);
    }

	public static void SaveGot500SilverFirstTime(bool isGot)
	{
		PlayerPrefs.SetInt(_got500SilverFirstTimeKey, (isGot)? 1:0 );
	}

	public static bool GetGot500SilverFirstTime()
	{
		if(PlayerPrefs.HasKey(_got500SilverFirstTimeKey))
		{
			int got500SilverFirstTime = PlayerPrefs.GetInt(_got500SilverFirstTimeKey);

			if (got500SilverFirstTime <= 0)
			{
				return false;
			} 
			else 
			{
				return true;
			}


		}

		return false;
	}

    public static string GetBuildVersion()
    {
		return BuildVersion.GetBuildTime();
    }
}