﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoloLeageRewardFinalButtonConfirmUI : MonoBehaviour {

	//public UILabel LevelFromLabel;
	public UILabel LevelUseLabel;
	//public UILabel DiamondFromLabel;
	public UILabel DiamondUseLabel;

	public string levelFrom;
	public string diamondFrom;
	public string cardID;
	public string packID;

	private string _cardID;
	private string _packID;

	private int _buttonRankIndex;
	//private RewardTypeEnum RewardType;
	public int ButtonRankIndex { get { return _buttonRankIndex; } }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


}
