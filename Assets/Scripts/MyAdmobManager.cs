﻿using UnityEngine;
using UnityEngine.Events;
using System;
using System.Collections;
using System.Collections.Generic;

//using GoogleMobileAds.Api;
#if UNITY_ADS
using UnityEngine.Advertisements;
#endif

public class MyAdmobManager : MonoBehaviour
{
    public enum ADSState
    {
          Unload
        , Loading
        , Waiting
        , Loaded
        , Playing
    };

    public delegate void OnShowRewardedVideoComplete(bool isSuccess);

    private static MyAdmobManager _instance = null;
    public static MyAdmobManager Instance
    {
        get
        {
            if (_instance == null)
            {
                MyAdmobManager obj = GameObject.FindObjectOfType<MyAdmobManager>();
                if (obj != null)
                {
                    _instance = obj;
                }
                else
                {
                    GameObject prefab = Resources.Load("MyAdmob/MyAdmobManager") as GameObject;

                    if (prefab != null)
                    {
                        GameObject myAdmobObj = GameObject.Instantiate(prefab);
                        myAdmobObj.name = "MyAdmobManager";
                        _instance = myAdmobObj.GetComponent<MyAdmobManager>();
                    }
                }
            }

            return _instance;
        }
    }

    private static Action<bool> _onShowRewardedVideoComplete = null;
    private static bool _isAdsComplete = false;
    private static string _message = "";
    private static float WaitRetryTime = 5.0f;

    public static string AppID_UnityAds
    {
        get
        {
            #if UNITY_ANDROID
            return "1253334";
            #elif UNITY_IOS
            return "1253335";
            #else
            return "";
            #endif
        }
    }
    
    public static string AppID_Admob
    {
        get
        {
            #if UNITY_ANDROID
            return "ca-app-pub-4889062171646218~4173977441";
            #elif UNITY_IOS
            return "ca-app-pub-4889062171646218~5761643488";
            #else
            return "";
            #endif
        }
    }
    
    public static string AdmobSilverRewardedVideoAdUnitID
    {
        get 
        {
            #if UNITY_ANDROID
            return "ca-app-pub-4889062171646218/5588073413";            
            #elif UNITY_IOS
            return "ca-app-pub-4889062171646218/8715109883";
            #else
            return "";
            #endif
        }
    }

    private List<string> _logList = null;
    public LogText LogText;
    private bool _isPrintLog = false;

    public static bool IsRewardedVideoReady
    {
        get
        {
            bool isReady = false;
            #if (UNITY_IPHONE || UNITY_ANDROID)

            #if UNITY_ADS
            // Unity Ads
            isReady |= Advertisement.IsReady("rewardedVideo");
            #endif
            
            // Admob
            /*
            if(_rewardBasedVideo != null)
            {
                isReady |= _rewardBasedVideo.IsLoaded();
            }
            */

            #endif

            return isReady;
        }
    }

    void Awake()
    {
        DontDestroyOnLoad(gameObject);

        if (_instance == null)
        {
            _instance = this;

            // Start Preload
            PreloadRewardedVideo();
        }
    }

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    private static void SetGameAudio(bool isMute)
    {
        var f = FindObjectsOfType<AudioSource>();
        var l = new List<AudioSource>();

        if (f != null)
        {
            foreach (var a in f)
            {
                if (a.isPlaying)
                {
                    l.Add(a);
                    a.mute = isMute;
                }
            }
        }
    }

    public static void PrintLog(string text)
    {
        if (_instance != null)
        {
            _instance._PrintLog(text);
        }
    }

    public void _PrintLog(string text)
    {
        /*
        if (this.LogText != null)
        {
            this.LogText.PrintLog(text);
        }
        */

        AddLogText(text);

        Debug.Log(text);
    }

    #region Methods
    public static void PreloadRewardedVideo()
    {
        InitRewardedVideo_UnityAds();
        //InitRewardedVideo_Admob();
    }

    public static void ShowRewardedVideo(Action<bool> callback = null)
    {
        if (IsRewardedVideoReady)
        {
            #if UNITY_ADS
            // Unity Ads
            if (Advertisement.IsReady("rewardedVideo"))
            {
                ShowRewardedVideo_UnityAds(callback);
                return;
            }
            else
            {
                PrintLog("Unity Ads not ready.");
                InitRewardedVideo_UnityAds();
            }
            #endif
            
            // Admob
            /*
            if(_rewardBasedVideo != null && _rewardBasedVideo.IsLoaded())
            {
                ShowRewardedVideo_Admob(callback);
                return;
            }
            else
            {
                PrintLog("Admob not ready.");
                InitRewardedVideo_Admob();
            }
            */
        }
        
        if(callback != null)
        {
            callback(false);
        }
    }
    #endregion

    #region UnityAds
    public static void InitRewardedVideo_UnityAds()
    {
        #if UNITY_ADS
        Advertisement.Initialize(AppID_UnityAds);
        #endif
    }

    public static void ShowRewardedVideo_UnityAds(Action<bool> callback = null)
    {
        #if UNITY_ADS
         if (Advertisement.IsReady("rewardedVideo"))
        {
            PrintLog("Showing Unity Ads...");

            _onShowRewardedVideoComplete = callback;

            var options = new ShowOptions { resultCallback = HandleShowUnityAdsResult };
            Advertisement.Show("rewardedVideo", options);
            return;
        }
        else
        {
            _onShowRewardedVideoComplete = null;

            if (callback != null)
            {
                callback.Invoke(false);
            }

            InitRewardedVideo_UnityAds();
        }
        #else
        if (callback != null)
        {
            callback.Invoke(false);
        }
        #endif
    }

    #if UNITY_ADS
    private static void HandleShowUnityAdsResult(ShowResult result)
    {
        string logText = "HandleShowUnityAdsResult: " + result.ToString();
        PrintLog(logText);
        //SetGameAudio (false);

        switch (result)
        {
            case ShowResult.Finished:
            {
                if (_onShowRewardedVideoComplete != null)
                {
                    _onShowRewardedVideoComplete(true);
                }
            }
            break;

            default:
            {
                if (_onShowRewardedVideoComplete != null)
                {
                    _onShowRewardedVideoComplete(false);
                }
            }
            break;
        }
    }
    #endif
    #endregion
           
    #region Admob
    /*
    private static RewardBasedVideoAd _rewardBasedVideo = null;
    private static bool _isLoadingRewardedVideo_Admob = false;
    private static bool _isRewarded = false;
    
    private static void Init_Admob()
    {
        MobileAds.Initialize(AppID_Admob);
        
        _rewardBasedVideo = RewardBasedVideoAd.Instance;
        
        // Called when an ad request has successfully loaded.
        _rewardBasedVideo.OnAdLoaded += HandleRewardBasedVideoLoaded;
        
        // Called when an ad request failed to load.
        _rewardBasedVideo.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
        
        // Called when an ad is shown.
        _rewardBasedVideo.OnAdOpening += HandleRewardBasedVideoOpened;
        
        // Called when the ad starts to play.
        _rewardBasedVideo.OnAdStarted += HandleRewardBasedVideoStarted;
        
        // Called when the user should be rewarded for watching a video.
        _rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
        
        // Called when the ad is closed.
        _rewardBasedVideo.OnAdClosed += HandleRewardBasedVideoClosed;
        
        // Called when the ad click caused the user to leave the application.
        _rewardBasedVideo.OnAdLeavingApplication += HandleRewardBasedVideoLeftApplication;
        
    }
    
    private static void InitRewardedVideo_Admob()
    {
        if(!_isLoadingRewardedVideo_Admob)
        {
            // Create an empty ad request.
            AdRequest request = new AdRequest.Builder().Build();
            
            if(_rewardBasedVideo == null)
            {
                Init_Admob();
            }
            
            // Load the rewarded video ad with the request.
            _rewardBasedVideo.LoadAd(request, AdmobSilverRewardedVideoAdUnitID);
            _isLoadingRewardedVideo_Admob = true;
            _isRewarded = false;
        }
    }
    
    public static void ShowRewardedVideo_Admob(Action<bool> callback = null)
    {
        if(_rewardBasedVideo != null && _rewardBasedVideo.IsLoaded())
        {
            _onShowRewardedVideoComplete = callback;
            _rewardBasedVideo.Show();
            return;
        }
        else
        {
            if (callback != null)
            {
                callback.Invoke(false);
            }
            
            InitRewardedVideo_Admob();
        }
    }
    
    private static void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
    {
        Debug.Log("HandleRewardBasedVideoLoaded event received");
        
        _isLoadingRewardedVideo_Admob = false;
    }

    private static void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        Debug.Log("HandleRewardBasedVideoFailedToLoad event received with message: " + args.Message);
        
        _isLoadingRewardedVideo_Admob = false;
        _instance.ReloadAds();
    }

    private static void HandleRewardBasedVideoOpened(object sender, EventArgs args)
    {
        Debug.Log("HandleRewardBasedVideoOpened event received");
    }

    private static void HandleRewardBasedVideoStarted(object sender, EventArgs args)
    {
        Debug.Log("HandleRewardBasedVideoStarted event received");
    }

    private static void HandleRewardBasedVideoClosed(object sender, EventArgs args)
    {
        Debug.Log("HandleRewardBasedVideoClosed event received");
        
        if(_onShowRewardedVideoComplete != null)
        {
            _onShowRewardedVideoComplete.Invoke(_isRewarded);
        }
        
        _isRewarded = false;       
        _instance.ReloadAds();
    }

    private static void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        string type = args.Type;
        double amount = args.Amount;
        Debug.Log("HandleRewardBasedVideoRewarded event received for "+ amount.ToString() + " " + type);
        
        _isRewarded = true;        
    }

    private static void HandleRewardBasedVideoLeftApplication(object sender, EventArgs args)
    {
        Debug.Log("HandleRewardBasedVideoLeftApplication event received");
    }
    */
    #endregion
    

    private void ReloadAds()
    {
        StartCoroutine(this._ReloadAds());
    }

    private IEnumerator _ReloadAds()
    {
        yield return new WaitForSeconds(WaitRetryTime);

        MyAdmobManager.PreloadRewardedVideo();
    }

    private void AddLogText(string text)
    {
        if (_logList == null)
        {
            _logList = new List<string>();
        }

        _logList.Add(text);

        if (_logList.Count > 40)
        {
            _logList.RemoveAt(0);
        }
    }

    private void OnGUI()
    {
        if(_isPrintLog)
        {
            float width = (float) Screen.width * 0.5f;
            float height = (float) Screen.height;

            string displayText = "";

            if(_logList != null && _logList.Count > 0)
            foreach (string log in _logList)
            {
                if (displayText.Length > 0)
                {
                    displayText += ("\n" + log);
                }
                else
                {
                    displayText += log;
                }
            }

            GUI.Label(
                  new Rect(0.0f, 0.0f, width, height)
                , displayText
            );
        }
    }
}
