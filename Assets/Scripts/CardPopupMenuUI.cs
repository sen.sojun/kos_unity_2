﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(UIWidget))]
public class CardPopupMenuUI : MonoBehaviour 
{
    public enum CardPopupActionType : int
    {
          None = 0
        , Info
        , Summon
        , Facedown
        , Equip
        , Use
        , Discard
        , Attack
        , Move
        , Ability
		, Back
    }

	public delegate void OnSelectActionCallback(CardPopupActionType action, int id);
    public delegate void OnCloseActionCallback();

    private List<ActionButton> _myActionButtonList;

    private int _buttonWidth;
    private int _buttonHeight;
    private Vector3 _pivotOffset;

    private OnSelectActionCallback _onSelectActionCallback = null;
	private OnCloseActionCallback _onCloseActionCallback = null;
    private GameObject _selectedObj;
	private int _id;
    private bool _isInit = false;
    private bool _isShow = false;

    public float ButtonOffset = 10.0f;
    public float WindowBorder = 20.0f;
    public GameObject ActionButtonPrefab;
    
    public bool IsShow { get {return _isShow; } }

	// Use this for initialization
	void Start () 
    {
        //Init();

        // first hide
        //NGUITools.SetActive(gameObject, false);
	}
	
	// Update is called once per frame
    /*
	void Update () 
    {
	}
    */

    void OnEnable()
    {
        //Init();
    }

    void OnDisable()
    {
 
    }

    public void Init()
    {
        UIWidget actionButtonWidget = ActionButtonPrefab.GetComponent<UIWidget>();

        _buttonWidth = actionButtonWidget.width;
        _buttonHeight = actionButtonWidget.height;

        _myActionButtonList = new List<ActionButton>();
        _pivotOffset = Vector3.zero;
        
        _isInit = true;
    }

	public void ShowUI(List<CardPopupActionType> actionTypeList, int id, GameObject selectedObj, Vector3 position, OnSelectActionCallback selectCallback, OnCloseActionCallback closeCallback, UIWidget.Pivot pivot = UIWidget.Pivot.TopLeft)
    {
        if(_isInit)
        {
            ClearUI();
        }
        else
        {
            Init();
        }
        
        _selectedObj = selectedObj;
		_id = id;
		_onSelectActionCallback = selectCallback;
		_onCloseActionCallback = closeCallback;
        //BattleUIManager.RequestBringForward(gameObject);

        CreateActionList(actionTypeList);
        SetPopupPosition(position, pivot);

        NGUITools.SetActive(gameObject, true);
		BattleUIManager.RequestBringForward(gameObject);
        _isShow = true;
    }

    public void SetPopupPosition(Vector3 position, UIWidget.Pivot pivot)
    {
        UIWidget widget = GetComponent<UIWidget>();
       
        switch(pivot)
        {
            case UIWidget.Pivot.TopLeft:
            {
                _pivotOffset = new Vector3(WindowBorder, -WindowBorder, 0.0f);
            }
            break;

            case UIWidget.Pivot.Top:
            {
                _pivotOffset = new Vector3(-(_buttonWidth * 0.5f), -WindowBorder, 0.0f);
            }
            break;

            case UIWidget.Pivot.TopRight:
            {
                _pivotOffset = new Vector3(-(_buttonWidth + WindowBorder), -WindowBorder, 0.0f);
            }
            break;

            case UIWidget.Pivot.Left:
            {
                _pivotOffset = new Vector3( WindowBorder, (widget.height * 0.5f) - WindowBorder, 0.0f);
            }
            break;

            case UIWidget.Pivot.Center:
            {
                _pivotOffset = new Vector3(-(_buttonWidth * 0.5f), (widget.height * 0.5f) - WindowBorder, 0.0f);
            }
            break;

            case UIWidget.Pivot.Right:
            {
                _pivotOffset = new Vector3(-(_buttonWidth + WindowBorder), (widget.height * 0.5f) - WindowBorder, 0.0f);
            }
            break;

            case UIWidget.Pivot.BottomLeft:
            {
                _pivotOffset = new Vector3(WindowBorder, widget.height - WindowBorder, 0.0f);
            }
            break;

            case UIWidget.Pivot.Bottom:
            {
                _pivotOffset = new Vector3(-(_buttonWidth * 0.5f), widget.height - WindowBorder, 0.0f);
            }
            break;

            case UIWidget.Pivot.BottomRight:
            {
                _pivotOffset = new Vector3(-(_buttonWidth + WindowBorder), widget.height - WindowBorder, 0.0f);
            }
            break;

            default:
            {
                _pivotOffset = Vector3.zero;
            }
            break;
        }

        widget.pivot = pivot;
        transform.position = position;

        Reposition();
    }

    private void CreateActionList(List<CardPopupActionType> actionTypeList)
    {
        foreach (CardPopupActionType actionType in actionTypeList)
        {
            CreateActionButton(actionType);
        }

        // Set new window size.
        ResizeWindow();
    }

    private void CreateActionButton(CardPopupActionType actionType)
    {
        GameObject actionButtonObj = NGUITools.AddChild(gameObject, ActionButtonPrefab);
        ActionButton actionButton = actionButtonObj.GetComponent<ActionButton>();

        // add to child.
        actionButtonObj.transform.localScale = Vector3.one;

        // Init action button
        actionButton.SetActionButton((int)actionType, actionType, this.OnClickActionButton);

        // add to actionButtonList
        _myActionButtonList.Add(actionButton);

        BattleUIManager.RequestBringForward(actionButtonObj);
    }

    private void Reposition()
    {
        // reposition all action button.
        // horizontal from top to buttom
        for (int index = 0; index < _myActionButtonList.Count; ++index)
        {
            _myActionButtonList[index].transform.localPosition = new Vector3(
                  _pivotOffset.x
                , _pivotOffset.y - (_buttonHeight * index) - (ButtonOffset * index)
                , 0.0f
            );
        }
    }

    private void ResizeWindow()
    {
        // Set new BG size.
        UIWidget widget = GetComponent<UIWidget>();

        int maxWidth = Mathf.CeilToInt(_buttonWidth + (WindowBorder * 2.0f));
        int maxHeight = Mathf.CeilToInt((_buttonHeight * _myActionButtonList.Count) + (ButtonOffset * (_myActionButtonList.Count - 1)) + (WindowBorder * 2.0f));

        widget.width = maxWidth;
        widget.height = maxHeight;
    }

    public void OnClickActionButton(int actionID)
    {
        if (_onSelectActionCallback != null && actionID > 0)
        {
			SoundManager.PlayEFXSound(BattleController.Instance.ButtonSound);
			_onSelectActionCallback.Invoke((CardPopupActionType)actionID, _id);
        }

        HideUI();
    }

	public void CloseUI()
	{
		if(_onCloseActionCallback != null)
		{
			_onCloseActionCallback.Invoke();
		}

		HideUI();
	}

    private void HideUI()
    {
        ClearUI();
        NGUITools.SetActive(gameObject, false);
        _isShow = false;
    }

    private void ClearUI()
    {
        while (_myActionButtonList.Count > 0)
        {
            GameObject obj = _myActionButtonList[0].gameObject;
            _myActionButtonList.RemoveAt(0);

            Destroy(obj);
        }

        _myActionButtonList.Clear();
        _selectedObj = null;
        _onSelectActionCallback = null;
    }
}
