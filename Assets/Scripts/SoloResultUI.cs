﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoloResultUI : MonoBehaviour
{

	#region Delegates
	public delegate void OnShowFinish();
	public delegate void OnChooseDone();
	#endregion
	public UILabel ResultLabel;
	public UILabel WinLabel;
	public UILabel DiamondLabel;
	public UIButton MainMenuButton;

	public GameObject WinFrame;

	public float ShowTime = 1.5f;

	private OnShowFinish _onShowFinish = null;
	private OnChooseDone _onChooseDone = null;

	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
	}

	public void ShowUI(GameResultUI.GameResultType gameResult, 
		               int expGet, 
		               int rewardDiamond, 
		               int levelUp,
		               bool isLevelUp,  
		               //OnChooseDone onChooseDone)
		               OnShowFinish onShowFinish)
	{
		//_onChooseDone = onChooseDone;
		_onShowFinish = onShowFinish;

		switch(gameResult)
		{
		 case GameResultUI.GameResultType.Lose: 
			{ 
				ResultLabel.text = Localization.Get("BATTLE_SOLO_RESULT_LOSE"); 
			}
			break;

		 case GameResultUI.GameResultType.Draw: 
			{ 
				ResultLabel.text = Localization.Get("BATTLE_SOLO_RESULT_DRAW"); 
			}
			break;

		 case GameResultUI.GameResultType.Win: 
			{ 
				ResultLabel.text = Localization.Get("BATTLE_SOLO_RESULT_WIN"); 
			}
			break;
		}

		{
			WinFrame.SetActive(true);
			WinLabel.text = string.Format (Localization.Get ("BATTLE_SOLO_GET_EXP"), expGet.ToString ());
			string text = string.Format(Localization.Get("BATTLE_SOLO_GET_DIAMOND"),rewardDiamond.ToString());
			//WinLabel.text += "\n" + text;
			DiamondLabel.text = text;
		}

		ShowButton(false);
		NGUITools.SetActive(gameObject, true);
		BattleUIManager.RequestBringForward(gameObject);

		StartCoroutine(WaitShowFinish(ShowTime));
	}

	private IEnumerator WaitShowFinish(float waitTime)
	{
		yield return new WaitForSeconds(waitTime);

		ShowFinish();
	}

	private void ShowFinish()
	{
		if(_onShowFinish != null)
		{
			_onShowFinish.Invoke();
		}	
	}

	public void HideUI()
	{
		NGUITools.SetActive(gameObject, false);
	}

	public void ShowButton(bool isShow)
	{
		MainMenuButton.gameObject.SetActive(isShow);
//		RetryButton.gameObject.SetActive(isShow);
	}

}
