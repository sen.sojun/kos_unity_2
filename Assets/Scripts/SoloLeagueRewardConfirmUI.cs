﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoloLeagueRewardConfirmUI : MonoBehaviour {

	//public UILabel RewardTitle;
	public UILabel LevelFromLabel;
	public UILabel LevelUseLabel;
	public UILabel DiamondFromLabel;
	public UILabel DiamondUseLabel;
	public GameObject NotEnoughUI;

	private string _cardID;
	private string _packID;

//	private string cardID;
//	private string packID;

//	public FullCardUI CardUI;
//	public UISprite PackImgUI;

	public enum RewardTypeEnum
	{
		Card
		, Pack
	}


	// Use this for initialization
	void Start () 
	{

	}

	// Update is called once per frame
	void Update () 
	{

	}


	public void ShowUI2(string levelFrom,string levelUse,
		               string diamondFrom,string diamondUse)
	{
		LevelFromLabel.text = levelFrom;
		LevelUseLabel.text = "" + levelUse;
		DiamondFromLabel.text = diamondFrom;
		DiamondUseLabel.text = "" + diamondUse;

		NGUITools.BringForward(gameObject);
		gameObject.SetActive (true);
	}

	public void HideUI()
	{
		gameObject.SetActive (false);
	}



//	public void ShowUI(SoloLeagueRewardDBData data)
//	{
//		int iFromLevel = 0;
//		int iFromDiamond = 0;
//		int iCostLevel = 0;
//		int iCostDiamond = 0;
//		int iCurrExp = 0;
//
//		int iDestinationLevelExp = 0;
//
//		int iNextLevel = 0;   //next level from now
//		int iDestLevel = 0;   //new level after swap
//		int iNextFromDestLevel = 0;
//
//		//show from diamond
//		iFromDiamond = PlayerSave.GetPlayerDiamond ();
//
//		//get destination diamond
//		iCostDiamond = iFromDiamond - data.DiamondUse;
//
//		//get currexp
//		iCurrExp = PlayerSave.GetPlayerExp ();
//		Debug.Log ("now player's exp = " + iCurrExp.ToString ());
//
//		//show from level
//		iFromLevel = PlayerSave.GetPlayerLevel ();
//
//		iDestinationLevelExp = PlayerSave.GetPlayerExp ();
//
//		//get destination level
//		iCostLevel = iFromLevel - data.LevelUse;
//		iNextLevel = iFromLevel + 1;
//		iNextFromDestLevel = iCostLevel + 1;
//
//		iDestinationLevelExp = getNewExp (iCurrExp,iFromLevel, iNextLevel, iCostLevel, iNextFromDestLevel);
//
//
//		LevelFromLabel.text = iFromLevel.ToString();
//		LevelUseLabel.text = "> " + iCostLevel.ToString();
//		DiamondFromLabel.text = iFromDiamond.ToString();
//		DiamondUseLabel.text = "> " + iCostDiamond.ToString();
////
////		_cardID = cardID;
////		_packID = packID;
//		//BattleUIManager.RequestBringForward(gameObject);
//		NGUITools.BringForward(gameObject);
//		gameObject.SetActive (true);
//	}
//
//
//	public int getNewExp(int iCurrExp ,int iFromLevel,int iNextLevel,
//		int iCostLevel,int iNextDestLevel)
//	{
//		int iCurrLevelExp = 0;
//		int iNextLevelExp = 0;
//		int iCostLevelExp = 0;
//		int iNextDestLevelExp = 0;
//
//		int iDeltaD = 0;
//		int iSpaceFromNextToCurr = 0;
//
//		float fPercent = 0;
//		float fToAddNewExp = 0;
//		int iNewExp = 0;
//
//
//
//		iCurrLevelExp = PlayerSave.GetPlayerExpByLevel (iFromLevel);
//		iNextLevelExp = PlayerSave.GetPlayerExpByLevel (iNextLevel);
//		iCostLevelExp = PlayerSave.GetPlayerExpByLevel (iCostLevel);
//		iNextDestLevelExp = PlayerSave.GetPlayerExpByLevel (iNextDestLevel);
//
//		Debug.Log ("curr level's exp = " + iCurrLevelExp);
//		Debug.Log ("next level's exp = " + iNextLevelExp);
//		Debug.Log ("cost level's exp = " + iCostLevelExp);
//		Debug.Log ("cost level's next exp = " + iNextDestLevelExp);
//
//		iDeltaD = iCurrExp - iCurrLevelExp;
//		iSpaceFromNextToCurr = iNextLevelExp - iCurrLevelExp;
//		fPercent = (float)iDeltaD / (float)iSpaceFromNextToCurr;
//
//		iSpaceFromNextToCurr = iNextDestLevelExp - iCostLevelExp;
//
//		fToAddNewExp = iSpaceFromNextToCurr * fPercent;
//		iNewExp = Mathf.RoundToInt(fToAddNewExp) + iCostLevelExp;
//
//		Debug.Log ("new exp after purchase = " + iNewExp);
//		return iNewExp;
//	
//	}


//	private bool AddCardToStash(string playerCardID)
//	{
//		List<PlayerCardData> stash;
//		bool isSuccess = PlayerSave.GetPlayerCardStash(out stash);
//		if(isSuccess)
//		{
//			stash.Add(new PlayerCardData(playerCardID));
//			PlayerSave.SavePlayerCardStash (stash);
//
//			return true;
//		}
//		else
//		{
//			stash = new List<PlayerCardData>();
//			stash.Add(new PlayerCardData(playerCardID));
//			PlayerSave.SavePlayerCardStash (stash);
//
//			return true;
//		}
//
//		return false;
//	}

}
