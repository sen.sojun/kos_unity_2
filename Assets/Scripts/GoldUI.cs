﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldUI : MonoBehaviour {

	public enum ShopMode
	{
		SilverMode,
		GoldMode
	}

	#region Private Properties
	private Dictionary<string, ShopPackData> _shopList;
	private ShopMode _mode;
	private int _goldIndex;
	#endregion

	#region Public Properties
	public GoldCoinUI GoldCoinUI;

	public ShopConfirmationUI ConfirmPurchaseUI;
//	public UILabel LabelConfirmation;
//	public GameObject NotEnoughUI;
//	public ShopCardResultUI ShopCardResultUI;
	public GameObject OpenGoldUI;
//	public GameObject ShopPackUI;
//	public GameObject ShopStoreFrontUI;
//	public GameObject ShopInfoUI;

	public AdsMessageUI ResultUI;

//	public int NormalPrice =  150; // 150 silver
//	public int GoldPrice = 15; //15 gold
	int amountOfGold = 0;

	#endregion

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void onClickBuyGold (int index)
	{
		Debug.Log (index);
		_goldIndex = index;

		string phaseText = "";
		string headText = "";

		string amountOfDollarText = "";
		string amountOfGoldText = "";


		switch (_goldIndex) 
		{
		case 0:
			amountOfDollarText = "$1.99";
			amountOfGoldText = "15";
			amountOfGold = 15;
			break;
		case 1:
			amountOfDollarText = "$4.99";
			amountOfGoldText = "30";
			amountOfGold = 30;
			break;
		case 2:
			amountOfDollarText = "$9.99";
			amountOfGoldText = "105";
			amountOfGold = 105;
			break;
		case 3:
			amountOfDollarText = "$24.99";
			amountOfGoldText = "375";
			amountOfGold = 375;
			break;
		case 4:
			amountOfDollarText = "$39.99";
			amountOfGoldText = "750";
			amountOfGold = 750;
			break;
		}

		Debug.Log (index);

		phaseText = string.Format (Localization.Get ("SHOP_GOLD_BUY_CONFIRMATION")
			, amountOfGoldText, amountOfDollarText 
		);

		headText = string.Format (Localization.Get ("SHOP_GOLD_BUY_CONGRATULATION_HEAD"));

		Debug.Log ("descr = " + amountOfDollarText + " " + amountOfGoldText + " pahse = " + phaseText + " head = " + headText);
		OpenGoldUI.SetActive (false);
		ConfirmPurchaseUI.ShowUI (headText,phaseText,this.ConfirmAddGold, ConfirmPurchaseUI.HideUI,true,true,false);
		//ConfirmPurchaseUI.ShowUI (headText,descText, this.ConfirmBuyStarterSet_5, ConfirmPurchaseUI.HideUI,true,true,false);
		//ConfirmPurchaseUI.ShowUI (headText,phaseText, ConfirmPurchaseUI.HideUI, ConfirmPurchaseUI.HideUI,true,true,false);


	}

	public void ShowUI()
	{
		gameObject.SetActive (true);
	}

	public void HideUI()
	{
		gameObject.SetActive (false);
	}

	public void ShowGoldPriceListUI()
	{
		//ShopStoreFrontUI.SetActive (false);
		OpenGoldUI.SetActive (true);
		BattleUIManager.RequestBringForward (OpenGoldUI);
	}

	public void HideGoldPriceListUI()
	{
		Debug.Log ("close gold price");
		OpenGoldUI.SetActive (false);
		//ShopStoreFrontUI.SetActive (true);
	}

	public void ConfirmAddGold()
	{
		AddGoldCoin (amountOfGold);
	}

	public void AddGoldCoin(int addGoldCoin)
	{
		int goldCoin = PlayerSave.GetPlayerGoldCoin ();

		goldCoin += addGoldCoin;

		// save add coin.
		PlayerSave.SavePlayerGoldCoin(goldCoin);

		GoldCoinUI.UpdateGoldCoin ();

		string logText = string.Format(
			Localization.Get("SHOP_GOLD_BUY_CONGRATULATION")
			, addGoldCoin
		);
		ShowResultMsg (logText);
	}


	public void ShowResultMsg(string text)
	{
		ResultUI.ShowUI (text);
	}

	public void HideResultMsg()
	{
		ResultUI.HideUI ();
	}

}
