﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour
{
	#region Enums
	public enum SoundType
	{
		  Music
		, Effect
	}
	#endregion

	#region Private Properties
	private static GameObject _bgmObj = null;
	private static AudioSource _bgmSource = null;
	private static float _musicVolume;
	private static float _effectVolume;
	private static bool _isLoad = false;
	#endregion

	#region Public Properties
	public static float MusicVolume { get { if(!_isLoad) Load(); return _musicVolume; } }
	public static float EffectVolume { get { if(!_isLoad) Load(); return _effectVolume; } }

	public static float MusicVolumeDefault { get { return 0.25f; } }
	public static float EffectVolumeDefault { get { return 1.0f; } }
	#endregion

	#region Methods
	public static void Load()
	{
		_musicVolume = PlayerSave.GetMusicVolume();
		_effectVolume = PlayerSave.GetEffectVolume();
		_isLoad = true;
	}

	public static void SetMusicVolume(float musicVolume)
	{
		_musicVolume = musicVolume;
		PlayerSave.SaveMusicVolume(_musicVolume);

		if(_bgmSource != null)
		{
			_bgmSource.volume = MusicVolume;
		}
	}

	public static void SetEffectVolume(float effectVolume)
	{
		_effectVolume = effectVolume;
		PlayerSave.SaveEffectVolume(_effectVolume);
	}

	public static void PlayEFXSound(AudioClip clip)
	{
        
		NGUITools.PlaySound(clip, EffectVolume);
	}

	public static void PlayBGM(AudioClip clip)
	{
        //Debug.Log("PlayBGM...");

		if(_bgmSource != null && _bgmObj != null)
		{
			_bgmSource.Stop();
			_bgmSource.clip = clip;
			_bgmSource.loop = true;
			_bgmSource.volume = MusicVolume;
		}
		else
		{
			_bgmObj = new GameObject();
			_bgmSource = _bgmObj.AddComponent<AudioSource>();
			_bgmObj.name = "BGM";

			_bgmSource.clip = clip;
			_bgmSource.loop = true;
			_bgmSource.volume = MusicVolume;

			DontDestroyOnLoad(_bgmObj);
		}

		_bgmSource.Play();
	}

	public static void StopBGM()
	{
		if(_bgmSource != null && _bgmObj != null)
		{
			_bgmSource.Stop();
			Destroy(_bgmObj);

			_bgmObj = null;
			_bgmSource = null;
		}
	}

	public static bool IsPlayBGM(AudioClip clip)
	{
		if(_bgmSource != null)
		{
			if(_bgmSource.clip == clip)
			{
				return _bgmSource.isPlaying;
			}
		}

		return false;
	}

    static AudioListener mListener;
    static float mLastTimestamp = 0f;
    static AudioClip mLastClip;
    
    static private AudioSource PlaySound (AudioClip clip, float volume, float pitch)
    {
        float time = RealTime.time;
        if (mLastClip == clip && mLastTimestamp + 0.1f > time) return null;

        mLastClip = clip;
        mLastTimestamp = time;

        if (clip != null && volume > 0.01f)
        {
            if (mListener == null || !NGUITools.GetActive(mListener))
            {
                AudioListener[] listeners = GameObject.FindObjectsOfType(typeof(AudioListener)) as AudioListener[];

                if (listeners != null)
                {
                    for (int i = 0; i < listeners.Length; ++i)
                    {
                        if (NGUITools.GetActive(listeners[i]))
                        {
                            mListener = listeners[i];
                            break;
                        }
                    }
                }

                if (mListener == null)
                {
                    Camera cam = Camera.main;
                    if (cam == null) cam = GameObject.FindObjectOfType(typeof(Camera)) as Camera;
                    if (cam != null) mListener = cam.gameObject.AddComponent<AudioListener>();
                }
            }

            if (mListener != null && mListener.enabled && NGUITools.GetActive(mListener.gameObject))
            {
#if UNITY_4_3 || UNITY_4_5 || UNITY_4_6 || UNITY_4_7
                AudioSource source = mListener.audio;
#else
                AudioSource source = mListener.GetComponent<AudioSource>();
#endif
                if (source == null) source = mListener.gameObject.AddComponent<AudioSource>();
#if !UNITY_FLASH
                source.priority = 50;
                source.pitch = pitch;
#endif
                source.PlayOneShot(clip, volume);
                return source;
            }
        }
        return null;
    }
	#endregion
}
