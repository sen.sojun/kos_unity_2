﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class SoundPlayer : MonoBehaviour 
{
	public AudioClip SoundClip;
	public SoundManager.SoundType SoundType;
	public bool IsLoop = false;

	void Awake()
	{
		if(SoundType == SoundManager.SoundType.Music)
		{
			Play();
		}
	}

	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
	}

	public void Play()
	{
		AudioSource audioSource = GetComponent<AudioSource>();

		audioSource.clip = SoundClip;
		switch(SoundType)
		{
			case SoundManager.SoundType.Music: audioSource.volume = SoundManager.MusicVolume; break;
			case SoundManager.SoundType.Effect: audioSource.volume = SoundManager.EffectVolume; break;
		}
		audioSource.loop = IsLoop;

		audioSource.Play();
	}
}
