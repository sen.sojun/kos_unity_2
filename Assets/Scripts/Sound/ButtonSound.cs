﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ButtonSound : MonoBehaviour
{
    private Button _button;
    
    public AudioClip Clip;

    /*
	// Use this for initialization
	void Start () 
    {	
	}
	
	// Update is called once per frame
	void Update () 
    {
	}
    */

    public void OnEnable()
    {
        Init();
    }
    
    public void Init()
    {
        _button = GetComponent<Button>();
        
        _button.onClick.RemoveListener(this.PlaySound);
        _button.onClick.AddListener(this.PlaySound);
    }

    public void PlaySound()
    {
        if(Clip != null)
        {
            SoundManager.PlayEFXSound(Clip);  
        }
    }
}
