﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupImageBoxUINew : MonoBehaviour
{

    public Image ImageTexture;
    public Text MessageLabel;
    public Text TutorialTitle;
    public Text TutorialPage;
    public Button Button;
    public Text ButtonLabel;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ShowUI(string imageFilePath, string title, string page, string message, string buttonText = "OK", bool isKey = true)
    {
        Texture imageTexture = Resources.Load(imageFilePath, typeof(Texture)) as Texture;

        if (imageTexture != null)
        {
            Texture2D texture2D = imageTexture as Texture2D;
            Sprite mySprite = Sprite.Create(texture2D, new Rect(0.0f, 0.0f, texture2D.width, texture2D.height), new Vector2(0.5f, 0.5f), 100.0f);

            ImageTexture.sprite = mySprite;
            Resources.UnloadUnusedAssets();
            //Resources.UnloadAsset(imageTexture);
        }

        if (isKey)
        {
            MessageLabel.GetComponent<UILocalize>().key = message;
            ButtonLabel.GetComponent<UILocalize>().key = buttonText;
            TutorialTitle.GetComponent<UILocalize>().key = title;
            TutorialPage.text = page;
            ButtonLabel.GetComponent<UILocalize>().key = buttonText;
        }
        else
        {
            MessageLabel.text = message;
            ButtonLabel.text = buttonText;
            TutorialTitle.text = title;
            TutorialPage.text = page;
            ButtonLabel.text = buttonText;

            //MessageLabel.GetComponent<UILocalize>().value = message;
            //ButtonLabel.GetComponent<UILocalize>().value = buttonText;
            //TutorialTitle.GetComponent<UILocalize>().value = title;
            //TutorialPage.text = page;
            //ButtonLabel.GetComponent<UILocalize>().value = buttonText;
        }

        gameObject.SetActive(true);
        BattleUIManager.RequestBringForward(gameObject);
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }
}
