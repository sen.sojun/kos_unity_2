﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSoloResult : MonoBehaviour
{
	#region Delagates
	public delegate void OnChooseDone();
	#endregion

	#region Properties
	private OnChooseDone _onChooseDone = null;

	public SoloResultUI ResultUI;
	public SoloRewardUI RewardUI;

	private GameResultUI.GameResultType _result;
	private int _rewardDiamond;
	private int _gainexp;
	private int _level;
	private bool _isLevelUp;
	private PlayerCardData _rewardCard = null;
	#endregion

	// Use this for initialization
	void Start () 
	{
		//NGUITools.SetActive(gameObject, false);
	}

	// Update is called once per frame
	void Update () 
	{
	}

	public void ShowUI(GameResultUI.GameResultType gameResult,
		               int expCount, 
		               int rewardDiamond, 
					   int level, 
		               bool isLevelUp,
		               OnChooseDone onChooseDone)
	{
		_result = gameResult;
		_rewardDiamond = rewardDiamond;
		_gainexp = expCount;
		_level = level;
		_isLevelUp = isLevelUp;
		//_rewardCard = rewardCard;
		_onChooseDone = onChooseDone;

		NGUITools.SetActive(gameObject, true);
		BattleUIManager.RequestBringForward(gameObject);

		if (!isLevelUp) 
		{
			ResultUI.ShowUI (
				_result
				, _gainexp
				, _rewardDiamond
				,  _level
				, _isLevelUp 
				, this.ShowSoloResultFinish);
		}
		else
		{
			RewardUI.ShowUI (
				_result
				, _gainexp
				, _rewardDiamond
				,  _level
				, _isLevelUp 
				, this.ShowSoloResultFinish);
		}

		//ResultUI.ShowUI(_result, _rewardSilverCoin, winCount, ShowResultFinish);
	}

	private void ShowSoloResultFinish()
	{
		if(_result == GameResultUI.GameResultType.Win && _rewardCard != null)
		{
			ResultUI.HideUI();
			//RewardUI.HideUI();
			//RewardUI.ShowUI(_rewardCard);
		}
		else
		{
			// Show result
			ResultUI.ShowButton(true);
			RewardUI.ShowButton (true);
		}
	}

	public void OnClickDone()
	{
		if(_onChooseDone != null)
		{
			_onChooseDone.Invoke();
		}
	}

}
