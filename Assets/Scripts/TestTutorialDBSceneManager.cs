﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameAnalyticsSDK;

public class TestTutorialDBSceneManager : MonoBehaviour 
{
    public PopupBoxUI PopupBoxUI;
    public UILabel IndexLabel;

    private List<TutorialDBData> _tutorialDataList = null;
	private int _currentIndex = 0;

	//string _tutorialCode = "T0001";

	// Use this for initialization
	void Start () 
	{
        Init();
	}

    void Init()
    {
        _currentIndex = 0;

        bool isSuccess = TutorialDB.GetAllData(out _tutorialDataList);
        if (isSuccess)
        {
            ShowCurrentData();
        }
        else
        {
            Debug.LogError("TestDBSceneScript/Init: TutorialDB.GatAllData Failed!!");
        }
    }

    /*
	// Update is called once per frame
	void Update () 
	{

	}
    */

	public void ShowNextCard()
	{
		//Debug.Log("Show Next Card");
        ++_currentIndex;

        if (_currentIndex >= _tutorialDataList.Count)
		{
            _currentIndex = 0;
		}

        IndexLabel.text = (_currentIndex + 1).ToString();

        ShowCurrentData();
	}

	public void ShowPrevCard()
	{
		//Debug.Log("Show Prev Card");
        --_currentIndex;

        if (_currentIndex < 0)
		{
            _currentIndex = _tutorialDataList.Count - 1;
		}

        IndexLabel.text = (_currentIndex + 1).ToString();

        ShowCurrentData();
	}

    public void ShowCurrentData()
    {
        ShowData(_currentIndex);
    }

	public void ShowData(int index)
	{
        if (_tutorialDataList != null && _tutorialDataList.Count > index)
		{
            TutorialDBData data = _tutorialDataList[index];
            //tutorialData.DebugPrintData();

			string title = "";
			string message = "";
			string buttonText = "";
			if(LocalizationManager.IsCurrentLanguage(LocalizationManager.Language.Thai))
			{
				title = data.TitleTh;
				message = data.TextTh;
			}
			else
			{
				title = data.TitleEng;
				message = data.TextEng;
			}

			buttonText = Localization.Get(data.ButtonKey);

			if(string.Compare(data.DisplayMode, "DIALOG") == 0)
			{
				PopupBoxUI.ShowMessageUI(
					  message
					, buttonText
				);
			} 
			else 
			{
                PopupBoxUI.ShowImageUI(
					  data.ImagePath
					, title
					, ""
					, message
					, buttonText
				);
            }	
		}
	}
}
