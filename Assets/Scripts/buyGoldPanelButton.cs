﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buyGoldPanelButton : MonoBehaviour {
	public GoldUI GoldUI;
	public int GoldButtonIndex;

	public delegate void onBuyGoldButton(int index);
	private onBuyGoldButton _onBuyGoldButton = null;

	public void BindOnBuyGoldButton(onBuyGoldButton callback)
	{
		_onBuyGoldButton = callback;
	}


	void OnClick () {
		if (_onBuyGoldButton != null)
		{
			_onBuyGoldButton.Invoke (GoldButtonIndex);
		}
	}

	void Awake()
	{
		BindOnBuyGoldButton (GoldUI.onClickBuyGold);
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
