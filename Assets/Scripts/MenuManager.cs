﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System;
using Firebase.Auth;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour 
{
	private enum eAudioClips : int
	{
		  mainmenu = 0
		, campaign
		, jinglebell
	}

	public PopupBoxUI popupBoxUI;

	public GameObject MainMenuUI;
	public GameObject CampaignMenuUI;
	public TutorialMenuUI TutorialMenuUI;
	public GameObject SettingUI;
	public GameObject CreditUI; 
	public NPCDetailUI NPCDetailUI;
	public NPCRewardUI NPCRewardUI;
	public LoadingUI LoadingUI;
	public SoloLeagueManager SoloLeagueManager;
	public AnnouncementUI AnnounceUI;
	public ShopUI ShopUI;
    //	public GameObject SoloLeagueUI;

    public GameObject ResultUI;

	public GameObject GoldCoinUI;
	public GameObject SilvercoinUI;

	public List<AudioClip> BGMClipList;

	public List<UILabel> MenuLabelList;
	public List<UIButton> MenuButtonList;

	public List<string> PopUpMessageList;
	public List<string> PopUpButtonList;

    //public Text ResultText;

	//private bool testBool = false; 
	private string Tutorial1 = "T0001"; //First 3 Message before campaign
	private string Tutorial2 = "T0002"; //get first deck message
	private string Tutorial3 = "T0003"; //tell to open Campaign  
	private string Tutorial4 = "T0004"; //in Campaign Message
	private string Tutorial5 = "T0005"; //tell to select davox

	public bool ForcePlayFirstTime = false;

	//for tutorial database
	private List<TutorialData> _tutorialDataList = null;
    private string userID;
    //private int _currentIndex = 0;

    // Use this for initialization
    static Dictionary<string, int> card = new Dictionary<string, int>();
    StashFirebaseData stashdb;
    private AccountType _accType;

	void Start() 
	{
        _accType = PlayerSave.GetPlayerAccountType();
        userID = PlayerSave.GetPlayerUid();
        stashdb = new StashFirebaseData(userID, card);

        //RequestCacheData();
//		if (AnnouncementUI.IsShowAnnouncement) 
//		{
//			AnnounceUI.ShowAnnouncementUI();
//		}
        if (_accType != AccountType.KOS)
        {
            ShowMainMenu();
        }
        else
        {
            RequestCacheData(); 
        }
		//ShowMainMenu();
       // ResultText.text = "Start Main menu...";


	}

    public void RequestCacheData()
    {
       // ResultText.text = "Start Cache data...";
        KOSServer.Instance.CacheData(this.OnCacheCompleted, this.OnCacheFailed);
    }

    public void OnCacheCompleted()
    {
        try
        {
       // ResultText.text = "Cache Successful.";

        KOSServer.Instance.CacheDataStash(this.OnCacheStashCompleted,this.OnCacheFailed);


            //text = string.Format("Cache Successful.\nUID : {0}\nSilver Coin : {1}\njson : {2}",
            //    data.uid,
            //    data.UsersilverCoin,
            //    data.UserName
            //);
            //ResultText.text = text;
            //AddSilverAmt.text = data.UsersilverCoin.ToString();
            //silverCoin = data.UsersilverCoin;

            //find cardID C0001 if not found just add

        }
        catch (SystemException e)
        {
            Debug.Log(e.Message);
           // ResultText.text = e.Message;
        }
    }

    public void OnCacheFailed(string errorMessage)
    {
        Debug.Log(errorMessage);
       // ResultText.text = errorMessage;
    }


    public void OnCacheStashCompleted()
    {
    string text = "";
    bool foundCard = false;
        string testcard = "C0099";
    int currQty = 1;

        try
        {
            Debug.Log("on stash cashed");
            //ResultText.text = "Cache stash Successful.";
            Debug.Log("on stash cashed successfully");

            stashdb = KOSServer.Instance.StashData;

            Debug.Log("stashDB");

            //ResultText.text = "uid =  " + stashdb.userID;

            Debug.Log("uid =  " + stashdb.UID);

            foundCard = stashdb.Card.ContainsKey(testcard);

            //ResultText.text = "found ? " + foundCard;

            if (foundCard)
            {
                currQty = stashdb.Card[testcard];
                ++currQty;
            }
            else
            {
                stashdb.Card.Add(testcard, 1);
            }

            KOSServer.UpdateStashData(stashdb, testcard, currQty);
        }

        catch (SystemException e)
        {
            Debug.Log("cash stash erro " + e.Message);
            //ResultText.text = "cash stash erro " + e.Message;
        }
    }

	public void CloseAnnouncement()
	{
		AnnounceUI.gameObject.SetActive (false);
	}

	// Update is called once per frame
	void Update () 
	{
	}
		
	#region Popup UI Methods
	public void ShowPopupUI(int index)
	{
		TutorialData data = _tutorialDataList[index++];

		string title = "";
		string message = "";
		string buttonText = "";
		if(LocalizationManager.IsCurrentLanguage(LocalizationManager.Language.Thai))
		{
			title = data.TitleTh;
			message = data.TextTh;
		}
		else
		{
			title = data.TitleEng;
			message = data.TextEng;
		}

		buttonText = Localization.Get(data.ButtonKey);

		if(string.Compare(data.DisplayMode, "DIALOG") == 0)
		{
			popupBoxUI.ShowMessageUI(
				  message
				, buttonText
				, () =>
				{
					if(index < _tutorialDataList.Count)
					{
						ShowPopupUI(index);
					}
					else
					{
						this.ClosePopUpUI();
					}
				}
				, false
			);
		}
		else
		{
			popupBoxUI.ShowImageUI(
				  data.ImagePath
				, title
				, data.PageText
				, message
				, buttonText
				, () =>
				{
					if(index < _tutorialDataList.Count)
					{
						ShowPopupUI(index);
					}
					else
					{
						this.ClosePopUpUI();
					}
				}
				, false
			);
		}

		popupBoxUI.gameObject.SetActive(true);
		BattleUIManager.RequestBringForward(popupBoxUI.gameObject);
	}

	public void ClosePopUpUI()
	{
		popupBoxUI.gameObject.SetActive(false);
	}
	#endregion
		
	#region Show UI Methods
	public void ShowMainMenu()
	{
		MainMenuUI.SetActive(true);

		// Hide all ui.
		CampaignMenuUI.SetActive(false);
		TutorialMenuUI.HideUI();
		SettingUI.SetActive(false);
		CreditUI.SetActive(false);
		SilvercoinUI.SetActive (true);
		GoldCoinUI.SetActive (true);

		SoloLeagueManager.HideSoloLeague ();
		//SoloLeagueUI.SetActive (false);
		ShopUI.HideUI();
		NPCDetailUI.HideUI();

        //Debug.Log(string.Format("IsPlayerFirstTime: {0}", PlayerSave.IsPlayerFirstTime()));
        
		if (ForcePlayFirstTime || PlayerSave.IsPlayerFirstTime()) 
		{
			// First time main menu.
			for (int index = 0; index < MenuButtonList.Count; ++index) 
			{
				if (index == 0) 
				{ 
					// Campaign Button
					MenuLabelList [index].color = Color.yellow;
					MenuButtonList [index].normalSprite = "Button_Green";
					MenuButtonList [index].isEnabled = true;
				} 
				else 
				{
					// Other Button
					MenuLabelList[index].color = Color.gray;
					Transform glow = MenuButtonList [index].transform.Find("Glow");
					glow.gameObject.SetActive(false);
					MenuButtonList [index].isEnabled = false;
				}
			}

			// Load first time tutorial
			_tutorialDataList = new List<TutorialData>();

			// Load Tutorial1
			_tutorialDataList.AddRange(LoadFirstFlowTutorial(Tutorial1, true));

			// Load Tutorial2
			_tutorialDataList.AddRange(LoadFirstFlowTutorial(Tutorial2, true));

			// Load Tutorial3
			_tutorialDataList.AddRange(LoadFirstFlowTutorial(Tutorial3, true));
				
			// Load default deck.
			DeckPackData pack = new DeckPackData ("D0001");



			// Save to player local save.
			PlayerSave.SavePlayerDeck (pack.DeckData);


			//	Update new card to card collection to default.
			for (int index = 0; index < pack.DeckData.Count; ++index) 
			{
				// Save new card to card collection.
				PlayerSave.SavePlayerCardCollection (pack.DeckData.GetCardAt (index).PlayerCardData.CardData.CardID);
			}
				
			ShowPopupUI (0);	
		}
		else 
		{
			// Normal State
			if (AnnouncementUI.IsShowAnnouncement) 
			{
				AnnounceUI.ShowAnnouncementUI();
			}
			for (int index = 0; index < MenuButtonList.Count; ++index) 
			{
				MenuLabelList [index].color = Color.white;
				MenuButtonList [index].isEnabled = true;
			} 
		}

		// Play main menu song.
		//AudioClip mainMenuClip = BGMClipList[(int)eAudioClips.jinglebell]; //-vit11012017
		AudioClip mainMenuClip = BGMClipList[(int)eAudioClips.mainmenu]; //+vit11012017
		if(!SoundManager.IsPlayBGM(mainMenuClip))
		{
			SoundManager.PlayBGM(mainMenuClip);
		}


		//start of insertion vit 13052017
		//PlayerSave.SavePlayFromSoloLeague (true); //try

		bool isPlayFromSoloLeage = PlayerSave.IsFromSoloLeague ();

		//Debug.Log (" sololeage ?" + isPlayFromSoloLeage);
		if (isPlayFromSoloLeage)
		{
			//Debug.Log ("From sololeage" + PlayerSave.IsFromSoloLeague ());
			ShowSololeagueMenu ();

		}


		//PlayerSave.SavePlayFromSoloLeague (false); //reset sololeage
		//isPlayFromSoloLeage = PlayerSave.IsFromSoloLeague ();
		//Debug.Log (" out sololeage ?" + isPlayFromSoloLeage);

		//end of insertion vit 13052017


		//start of insertion vit 27072017
		bool isDeckFromSoloLeage = PlayerSave.IsDeckEditorFromSoloLeague();

		//Debug.Log (" deck sololeage ?" + isPlayFromSoloLeage);
		if (isDeckFromSoloLeage)
		{
			PlayerSave.SaveDeckFromSoloLeague (false);
			//Debug.Log ("From sololeage" + PlayerSave.IsFromSoloLeague ());
			ShowSololeagueMenu ();

		}

		//end of insertion vit 27072017

		//		PlayerSave.SavePlayFromSoloLeague (false);
		//		isPlayFromSoloLeage = PlayerSave.IsFromSoloLeague ();
		//
		//		if (!isPlayFromSoloLeage)
		//		{
		//			Debug.Log ("out From sololeage" + PlayerSave.IsFromSoloLeague ());
		//		}
	}

	public List<TutorialData> LoadFirstFlowTutorial(string tutorialCode, bool isFirstFlow = true)
	{
		// Load Tutorial1
		List<TutorialData> tutorialList = new List<TutorialData>();

		List<TutorialDBData> dataList;
		bool _isSuccess = TutorialDB.GetDataByCode (tutorialCode, out dataList);
		if (_isSuccess) 
		{
			List<TutorialDBData> showList = new List<TutorialDBData>();

			// Filter not show in first flow.
			foreach(TutorialDBData data in dataList)
			{
				if(isFirstFlow)
				{
					if(data.ShowInFirstFlow > 0)
					{
						showList.Add(data);
					}
				}
				else
				{
					showList.Add(data);
				}
			}

			int page = 0;
			foreach(TutorialDBData data in showList)
			{
				TutorialData tutorialData = new TutorialData(data, (++page).ToString() + "/" + showList.Count.ToString());
				tutorialList.Add(tutorialData);
			}
		}

		return tutorialList;
	}

    public void addCardDemo()
    {
        List<CardDBData> _database;
        List<PlayerCardData> cardList = new List<PlayerCardData>();
        List<CardData> cardColloectList = new List<CardData>();

        bool isSuccess = CardDB.GatAllData(out _database);
        if (isSuccess)
        {
            for (int i = 0; i < _database.Count;++i)
            {
                string id = _database[i].CardID;
                CardData collectCard = new CardData(id);
                PlayerCardData card = new PlayerCardData(collectCard);
                cardColloectList.Add(collectCard);
                cardList.Add(card);
            }
            Debug.Log("card count = " + cardList.Count);

            PlayerSave.SavePlayerCardStash(cardList);
            PlayerSave.SavePlayerCardCollection(cardColloectList);
        }
    }

	public void ShowCampaign()
	{
		MainMenuUI.SetActive (false);
		TutorialMenuUI.HideUI();
		SettingUI.SetActive (false);
		CreditUI.SetActive (false);
		SilvercoinUI.SetActive (true);
		GoldCoinUI.SetActive (false);
		//SoloLeagueUI.SetActive (false);
		SoloLeagueManager.HideSoloLeague ();
		NPCDetailUI.HideUI();

		CampaignMenuUI.GetComponent<CampaignUI>().ShowUI();
		CampaignMenuUI.SetActive (true);

		if (ForcePlayFirstTime || PlayerSave.IsPlayerFirstTime()) 
		{
			_tutorialDataList = new List<TutorialData> ();

			_tutorialDataList.AddRange(LoadFirstFlowTutorial(Tutorial4, true));

			_tutorialDataList.AddRange(LoadFirstFlowTutorial(Tutorial5, true));

			ShowPopupUI (0);
		}

		// Play campaign song.
		AudioClip campaignClip = BGMClipList[(int)eAudioClips.campaign];
		if(!SoundManager.IsPlayBGM(campaignClip))
		{
			SoundManager.PlayBGM(campaignClip);
		}
	}
		
	public void ShowTutorialMenu()
	{
		TutorialMenuUI.ShowUI();

		// Hide all ui.
		MainMenuUI.SetActive(false);
		CampaignMenuUI.SetActive (false);
		SettingUI.SetActive (false);
		ShopUI.HideUI();
		NPCDetailUI.HideUI();
		SilvercoinUI.SetActive (true);
		GoldCoinUI.SetActive (false);
		//SoloLeagueUI.SetActive (false);
		SoloLeagueManager.HideSoloLeague ();
	}

	public void ShowSololeagueMenu()
	{
		//SoloLeagueUI.SetActive (true);
		SoloLeagueManager.ShowDetail();

		// Hide all ui.
		MainMenuUI.SetActive(false);
		CampaignMenuUI.SetActive (false);
		SettingUI.SetActive (false);
		ShopUI.HideUI();
		NPCDetailUI.HideUI();
		SilvercoinUI.SetActive (true);
		GoldCoinUI.SetActive (false);
		TutorialMenuUI.HideUI ();
	}
		

	public void ShowCredit()
	{
		CreditUI.SetActive (true);

		// Hide all ui.
		MainMenuUI.SetActive(false);
		CampaignMenuUI.SetActive (false);
		SettingUI.SetActive (false);
		ShopUI.HideUI();
		TutorialMenuUI.HideUI();
		NPCDetailUI.HideUI();
		SilvercoinUI.SetActive (true);
		GoldCoinUI.SetActive (false);
		//SoloLeagueUI.SetActive (false);
		SoloLeagueManager.HideSoloLeague ();
	}

	public void ShowShop()
	{
		ShopUI.ShowUI();

		// Hide all ui.
		MainMenuUI.SetActive(false);
		CampaignMenuUI.SetActive (false);
		SettingUI.SetActive (false);
		CreditUI.SetActive (false);
		TutorialMenuUI.HideUI();
		NPCDetailUI.HideUI();
		SilvercoinUI.SetActive (true);
		GoldCoinUI.SetActive (true);
		//SoloLeagueUI.SetActive (false);
		SoloLeagueManager.HideSoloLeague ();
	}
		
	public void onCloseShop()
	{
		bool isShopFromSoloLeage = PlayerSave.IsShopFromSoloLeague ();

		ShopUI.HideUI ();
		Debug.Log (" shop sololeage ?" + isShopFromSoloLeage);

		if (isShopFromSoloLeage)
		{
			PlayerSave.SaveShopFromSoloLeague (false);
			Debug.Log ("From sololeage" + PlayerSave.IsFromSoloLeague ());
			ShowSololeagueMenu ();
		}
		else 
		{

		  MainMenuUI.SetActive (true);
		}
	}

	public void onCloseSoloLeague()
	{
		PlayerSave.SavePlayFromSoloLeague (false); //reset state back to normal
		SoloLeagueManager.HideSoloLeague ();
		//SoloLeagueUI.SetActive (false);
		MainMenuUI.SetActive (true);
	}

	public void ShowSetting()
	{
		SettingUI.SetActive (true);

		// Hide all ui.
		MainMenuUI.SetActive(false);
		CampaignMenuUI.SetActive (false);
		ShopUI.HideUI ();
		TutorialMenuUI.HideUI();
		NPCDetailUI.HideUI();
	}

	public void ShowLoading()
	{
		LoadingUI.ShowUI();
	}
	#endregion
				
	#region Main Menu Button Methods
	public void OnClickCampaign()
	{
		ShowCampaign();
	}

	public void OnClickDeck()
	{
		LoadScene("DeckEditorScene");
	}

	public void OnClickDeckFromSoloLeague()
	{
		PlayerSave.SaveDeckFromSoloLeague (true);
		LoadScene("DeckEditorScene");
	}

    public void OnClickLobby()
    {
        KOSNetwork.NetworkSetting.IsNetworkEnabled = true;
        LoadScene("LobbyScene");
    }
    
    public void OnClickHon()
    {
        LoadScene("TestDBScene2");
    }

	public void OnClickTutorial()
	{
		ShowTutorialMenu();
	}

	public void OnClickShop()
	{
		//LoadScene("ShopScene");
		ShowShop ();
	}

	public void OnClickShopFromSoloLeague()
	{
		PlayerSave.SaveShopFromSoloLeague (true);
		ShowShop ();
	}

	public void OnClickCollection()
	{
		LoadScene("CardCollectionScene");
	}

	public void OnClickSetting()
	{
		ShowSetting();
	}

	public void OnClickCredit()
	{
		ShowCredit();
	}

	public void OnClickFeedback()
	{
		string email = "hello@koscardgame.com";
		string subject = MyEscapeURL("Feedback for KOS");
		string body = MyEscapeURL("");
		Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);
	}

	string MyEscapeURL (string url)
	{
		return WWW.EscapeURL(url).Replace("+","%20");
	}
	#endregion

	#region Campaign Button Methods
	public void OnClickNPC(string npcID)
	{
		ShowNPCDetail(npcID);
	}

	public void CloseNPC()
	{
		HideNPCDetail();
	}

	public void ShowNPCDetail(string npcID)
	{
		CampaignMenuUI.SetActive (true);

		MainMenuUI.SetActive(false);
		TutorialMenuUI.HideUI();
		SettingUI.SetActive (false);
		CreditUI.SetActive (false);

		NPCDetailUI.ShowUI(npcID);
	}

	public void HideNPCDetail()
	{
		CampaignMenuUI.SetActive (true);

		MainMenuUI.SetActive(false);
		TutorialMenuUI.HideUI();
		SettingUI.SetActive (false);
		NPCDetailUI.HideUI();
	}

	public void ShowNPCReward(string npcID)
	{
		//Debug.Log(npcID);

		NPCData npcData = new NPCData(npcID);
		//Debug.Log(npcData.RewardCardID);

		PlayerCardData rewardCard = new PlayerCardData(new CardData(npcData.RewardCardID));

		NPCRewardUI.ShowUI(rewardCard);
	}
	#endregion

	public void LoadScene(string sceneName)
	{
		StartCoroutine(LoadingScene(sceneName));
	}

	IEnumerator LoadingScene(string sceneName)
	{
		ShowLoading();
		yield return new WaitForSeconds(0.01f);
		SceneManager.LoadSceneAsync(sceneName);
	}

	public void ResetAll()
	{
		// Clear save.
		PlayerSave.ClearAllSave();

		// Reload current scene.
		//LoadScene(SceneManager.GetActiveScene().name);

		// Load title scene.
		LoadScene("TitleScene");
	}
		
	public void ResetDefault()
	{
		// Clear save.
		PlayerSave.ClearAllSave();

		// Load default deck.
		DeckPackData pack = new DeckPackData ("D0001");

		// Save to player local save.
		PlayerSave.SavePlayerDeck (pack.DeckData);

		// Update new card to card collection to default.
		for(int index = 0; index < pack.DeckData.Count; ++index)
		{
			// Save new card to card collection.
			PlayerSave.SavePlayerCardCollection (pack.DeckData.GetCardAt(index).PlayerCardData.CardData.CardID);
		}

		// Finished first time.
		PlayerSave.SavePlayerFirstTime(false);
        PlayerSave.SaveGot500SilverFirstTime (true);

		// Reset coin.
		PlayerSave.SavePlayerSilverCoin(500);
		PlayerSave.SavePlayerGoldCoin(0);

		//Solo League
        PlayerSave.SavePlayFromSoloLeague(false);
        PlayerSave.SaveSoloDraw(0);
        PlayerSave.SaveSoloWin(0);
        PlayerSave.SaveSoloLose(0);
        PlayerSave.SavePlayerExp(0);
        PlayerSave.SavePlayerDiamond(0);


		// Reload current scene.
		//LoadScene(SceneManager.GetActiveScene().name);

		// Load title scene.
		LoadScene("TitleScene");
	}

    public void ResetTestProfile()
    {
        // Clear save.
        PlayerSave.ClearAllSave();

        // Load default deck.
        DeckPackData pack = new DeckPackData ("D0001");

        // Save to player local save.
        PlayerSave.SavePlayerDeck (pack.DeckData);

        // Update new card to card collection to default.
        for(int index = 0; index < pack.DeckData.Count; ++index)
        {
            // Save new card to card collection.
            PlayerSave.SavePlayerCardCollection (pack.DeckData.GetCardAt(index).PlayerCardData.CardData.CardID);
        }
        
        // Add all card to stash.
        List<CardDBData> cardList;
        List<PlayerCardData> stashList = new List<PlayerCardData>();
        {
            bool isSuccess = CardDB.GatAllData(out cardList);
            if(isSuccess)
            {
                foreach(CardDBData card in cardList)
                {
                    PlayerCardData playerCard = new PlayerCardData(card.CardID);
                    stashList.Add(playerCard);
                }
                
                PlayerSave.SavePlayerCardStash(stashList);
            }
        }
        
        // Update stash card to card collection.
        for(int index = 0; index < stashList.Count; ++index)
        {
            // Save stash card to card collection.
            PlayerSave.SavePlayerCardCollection (stashList[index].CardData.CardID);
        }
          
        // Finished first time.
        PlayerSave.SavePlayerFirstTime(false);
        PlayerSave.SaveGot500SilverFirstTime (true);

        // Reset coin.
        PlayerSave.SavePlayerSilverCoin(99990);
        PlayerSave.SavePlayerGoldCoin(99990);

         //Solo League
        PlayerSave.SavePlayFromSoloLeague(false);
        PlayerSave.SaveSoloDraw(0);
        PlayerSave.SaveSoloWin(0);
        PlayerSave.SaveSoloLose(0);
        PlayerSave.SavePlayerExp(0);
        PlayerSave.SavePlayerDiamond(0);

        // Reload current scene.
        //LoadScene(SceneManager.GetActiveScene().name);

        // Load title scene.
        LoadScene("TitleScene");
    }

	public void ResetDemo_1()
	{
		// Clear save.
		PlayerSave.ClearAllSave();

		// Load default deck.
		DeckPackData pack = new DeckPackData ("D0008");
		Debug.Log (pack.DeckData.Count);

		// Save to player local save.
		PlayerSave.SavePlayerDeck (pack.DeckData);

		// Update new card to card collection to default.
		for(int index = 0; index < pack.DeckData.Count; ++index)
		{
			// Save new card to card collection.
			PlayerSave.SavePlayerCardCollection (pack.DeckData.GetCardAt(index).PlayerCardData.CardData.CardID);
		}

		// Update all card in card collection.
		List<CardDBData> cardList;
		CardDB.GatAllData (out cardList);
		if (cardList != null && cardList.Count > 0) 
		{
			foreach (CardDBData data in cardList) 
			{
				PlayerSave.SavePlayerCardCollection (data.CardID);
			}
		}

		// Finished first time.
		PlayerSave.SavePlayerFirstTime(false);

		// Reset coin.
		PlayerSave.SavePlayerSilverCoin(1000);
		PlayerSave.SavePlayerGoldCoin(1000);

		// Reload current scene.
		//LoadScene(SceneManager.GetActiveScene().name);

		// Load title scene.
		LoadScene("TitleScene");
	}
}
