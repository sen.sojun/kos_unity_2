﻿using UnityEngine;
using System.Collections;

public class GoldCoinUI : MonoBehaviour 
{
	#region Private
	private int __currentGoldCoin = 0;

	private int _currentGoldCoin
	{
		get { return __currentGoldCoin; }
		set 
		{
			__currentGoldCoin = value;

			UpdateGoldCoinLabel ();
		}
	}
	#endregion

	#region Public Inspector Properties
	public UILabel labelGoldCoin;
	#endregion

	/*
	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
	}
	*/

	void OnEnable ()
	{
		_currentGoldCoin = PlayerSave.GetPlayerGoldCoin();
	}

	public void UpdateGoldCoin()
	{
		_currentGoldCoin = PlayerSave.GetPlayerGoldCoin();
	}

	private void UpdateGoldCoinLabel()
	{
		labelGoldCoin.text = "" + _currentGoldCoin.ToString ("##,###,##0") + "";
	}
}
