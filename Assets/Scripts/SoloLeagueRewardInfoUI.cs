﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoloLeagueRewardInfoUI : MonoBehaviour {
	public UILabel RewardTitle;
	public UILabel LevelUseLabel;
	public UILabel DiamondUseLabel;
	public FullCardUI CardUI;
	public UISprite PackImgUI;


	private string _cardID;
	private string _packID;

	public enum RewardTypeEnum
	{
		Card
	  , Pack
	}


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ShowUI(SoloLeagueRewardDBData data)
	{
		
		string titleText = "";

		DiamondUseLabel.text = data.DiamondUse.ToString ();
		LevelUseLabel.text = "LV. " + data.LevelUse.ToString ();

		switch (LocalizationManager.CurrentLanguage)
		{
			case LocalizationManager.Language.Thai:
				{
					titleText  = data.RewardText_Thai;
				}
				break;

			case LocalizationManager.Language.English:
			default:
				{
					titleText = data.RewardText_Eng;
				}
				break;
		}

		RewardTitle.text = titleText;


		switch(data.RewardType)
		{
			case SoloLeagueRewardDBData.RewardTypeEnum.Card:
				{
					CardUI.SetCardData (new PlayerCardData(data.RewardKey));

					CardUI.gameObject.SetActive (true);
				    _cardID = data.RewardKey;
				    //Debug.Log ("card id " + _cardID);
					PackImgUI.gameObject.SetActive (false);
				}
				break;

			case SoloLeagueRewardDBData.RewardTypeEnum.PackNormal:
				{
					PackImgUI.spriteName = data.RewardKey;
				    _packID = data.RewardKey;
				    //Debug.Log ("card id " + _packID);
				 	CardUI.gameObject.SetActive (false);
					PackImgUI.gameObject.SetActive (true);
				}
				break;
		   case SoloLeagueRewardDBData.RewardTypeEnum.PackPremium:
			 	{
					PackImgUI.spriteName = data.RewardKey;
					_packID = data.RewardKey;
					//Debug.Log ("card id " + _packID);
					CardUI.gameObject.SetActive (false);
					PackImgUI.gameObject.SetActive (true);
				}
				break;
		}

		NGUITools.BringForward(gameObject);
		gameObject.SetActive (true);
 
	}

	public void HideUI()
	{
		gameObject.SetActive (false);
	}



}
