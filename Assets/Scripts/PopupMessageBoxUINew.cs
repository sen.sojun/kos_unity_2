﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class PopupMessageBoxUINew : MonoBehaviour {

    public Text MessageLabel;
    public Button Button;
    public Text ButtonLabel;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void ShowUI(string message, string buttonText = "OK", bool isKey = true)
    {
        if (isKey)
        {

			MessageLabel.text  = Localization.Get(message);
			//MessageLabel.GetComponent<UILocalize>().key = message;
            //ButtonLabel.GetComponent<UILocalize>().key = buttonText;
			ButtonLabel.text = Localization.Get(buttonText);
        }
        else
        {
			MessageLabel.text = Localization.Get(message);
            //MessageLabel.GetComponent<UILocalize>().key = message;
            //ButtonLabel.GetComponent<UILocalize>().key = buttonText;
            ButtonLabel.text = Localization.Get(buttonText);
            
			MessageLabel.text = message;
			ButtonLabel.text = buttonText;
            //MessageLabel.GetComponent<UILocalize>().value = message;
            //ButtonLabel.GetComponent<UILocalize>().value = buttonText;
        }

        gameObject.SetActive(true);
        //BattleUIManager.RequestBringForward(gameObject);
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }
}
