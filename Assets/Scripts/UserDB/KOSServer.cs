﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Purchasing;
//using UnityEngine.Purchasing.Security;
using System;
using System.Text;

using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using Firebase.Auth;

public enum AccountType
{
      New = 0
    , Guest
    , KOS
}

public class KOSServer
{
    private static KOSServer _instance = null;
    public static KOSServer Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new KOSServer();
                _auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
            }

            return _instance;
        }
    }

    private static string _url = "https://koscardgame.firebaseio.com/";

    public delegate void OnSignUpCompleted(string email);
    public delegate void OnSignUpFailed(string errorMessage);

    public delegate void OnLoginCompleted();
    public delegate void OnLoginFailed(string errorMessage);

    public delegate void OnCacheCompleted();
    public delegate void OnCacheFailed(string errorMessage);

    public delegate void OnGetVersionCompleted(bool isSuccess, string version, bool isCheckVersion);

    public delegate void OnSaveReceiptCompleted(bool isSuccess);

    private static FirebaseAuth _auth;
    private FirebaseDatabase _database;
    private DatabaseReference _firebaseRef;
    private bool _isLogin = false;
    private bool _isProcessSignUp = false;
    private bool _isProcessLogin = false;
    private bool _isProcessCache = false;
    private bool _isProcessPassword = false;

    private bool _isCacheData = false;
    private bool _isCacheStash = false;
    private bool _isCacheDeck = false;

    private OnSignUpCompleted _onSignUpCompleted;
    private OnSignUpFailed _onSignUpFailed;
    private OnLoginCompleted _onLoginCompleted;
    private OnLoginFailed _onLoginFailed;

    private OnSaveReceiptCompleted _onSaveReceiptCompleted;

    private UserDBData _data;
    private StashFirebaseData _stashData;
    private DeckData _deckData;

    public bool IsLogin { get { return _isLogin; } }
    public bool IsCache { get { return _isCacheData && _isCacheStash && _isCacheDeck; } }
    public bool IsProcessSignUp { get { return _isProcessSignUp; } }
    public bool IsProcessLogin { get { return _isProcessLogin; } }
    public bool IsProcessCache { get { return _isProcessCache; } }
    public UserDBData PlayerData { get { return _data; } }
    public StashFirebaseData StashData { get { return _stashData; } }
    public DeckData DeckData { get { return _deckData; } }

    public enum DataTable
    {
        Player,
        Stash,
        Deck,
        Receipts,
        Matchlog
    }

    public bool IsKOSAccount()
    {
        if (PlayerSave.GetPlayerAccountType() == AccountType.KOS)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private static string GetTableKey(DataTable dataTable)
    {
        switch (dataTable)
        {
            case DataTable.Player: return "player";
            case DataTable.Stash: return "player_stash";
            case DataTable.Deck: return "player_deck";
            case DataTable.Receipts: return "receipts";
            case DataTable.Matchlog: return "match_log";
        }

        return "";
    }

    public void SignUp(string email, string password, string username, OnSignUpCompleted onSignUpCompleted = null, OnSignUpFailed onSignUpFailed = null)
    {
        if (_isProcessSignUp)
        {
            string errorTxt = "SignUp Failed: Signup is on process.";
            Debug.Log(errorTxt);
            if (onSignUpFailed != null)
            {
                onSignUpFailed.Invoke(errorTxt);
            }
            return;
        }

        _isProcessSignUp = true;
        _onSignUpCompleted = onSignUpCompleted;
        _onSignUpFailed = onSignUpFailed;

        KOSServer._auth.CreateUserWithEmailAndPasswordAsync(email, password).ContinueWith(task =>
        {
            _isProcessSignUp = false;

            if (task.IsCanceled)
            {
                //string errorTxt = "Signup failed: Signup has been canceled.";
                string errorTxt = Localization.Get("TITLE_SIGNUP_MESSAGE_FAILED");
                Debug.Log(errorTxt);
                if (_onSignUpFailed != null)
                {
                    _onSignUpFailed.Invoke(errorTxt);

                }
                return;
            }
            else if (task.IsFaulted)
            {
                //string errorTxt = "Signup failed: Error: " + task.Exception;
                string errorTxt = Localization.Get("TITLE_SIGNUP_MESSAGE_FAILED");
                Debug.Log(errorTxt);
                if (_onSignUpFailed != null)
                {
                    _onSignUpFailed.Invoke(errorTxt);

                }
                return;
            }
            else if (task.IsCompleted)
            {
                FirebaseUser newUser = task.Result; // Firebase user has been created.
                
                // Print Log
                {
                    string log = "";
                    log += string.Format("Firebase signup successfully: {0} ({1})", newUser.DisplayName, newUser.UserId);
                    log += string.Format("IsPlayerFirstTime : {0}", PlayerSave.IsPlayerFirstTime());                 
                    ScreenDubugLog.Instance.Print(log);
                }
                
                DeckData deckData = null;

                if (PlayerSave.IsPlayerFirstTime() || PlayerSave.IsSubmitGameData())
                {
                    // Nerver play KOS before.
                    PlayerSave.ClearAllSave();

                    PlayerSave.SavePlayerFirstTime(true);
                    PlayerSave.SavePlayerAvatar("07"); //"01"

                    PlayerSave.SavePlayerGoldCoin(0);
                    PlayerSave.SavePlayerSilverCoin(500);
                    PlayerSave.SavePlayerDiamond(0);
                    PlayerSave.SaveGot500SilverFirstTime(true);

                    PlayerSave.SavePlayerExp(0);

                    PlayerSave.SetWinCount("NPC0001", 0);
                    PlayerSave.SetWinCount("NPC0002", 0);
                    PlayerSave.SetWinCount("NPC0003", 0);
                    PlayerSave.SetWinCount("NPC0004", 0);
                    PlayerSave.SetWinCount("NPC0005", 0);
                    PlayerSave.SetWinCount("NPC0006", 0);

                    PlayerSave.SaveSoloWin(0);
                    PlayerSave.SaveSoloDraw(0);
                    PlayerSave.SaveSoloLose(0);

                    PlayerSave.SavePlayFromSoloLeague(false);

                    PlayerSave.SavePlayerUid(newUser.UserId);
                    PlayerSave.SavePlayerName(KOSServer.GetNewPlayerNameUniqueID());
                    PlayerSave.SavePlayerEmail(newUser.Email);
                    PlayerSave.SavePlayerPassword(password);

                    // Load default deck.
                    DeckPackData pack = new DeckPackData("D0001");
                    PlayerSave.SavePlayerDeck(pack.DeckData);

                    // Save stash.
                    List<PlayerCardData> stashDeck = new List<PlayerCardData>();
                    for (int index = 0; index < pack.DeckData.Count; ++index)
                    {
                        stashDeck.Add(pack.DeckData.GetCardAt(index).PlayerCardData);
                    }
                    PlayerSave.SavePlayerCardStash(stashDeck);
                    PlayerSave.SetSubmitGameData(true);
                }
                else
                {
                    // Already play or guest.
                    PlayerSave.SavePlayerUid(newUser.UserId);
                    PlayerSave.SavePlayerName(KOSServer.GetNewPlayerNameUniqueID());
                    PlayerSave.SavePlayerEmail(newUser.Email);
                    PlayerSave.SavePlayerPassword(password);

                    // Merge stash deck.
                    if (!PlayerSave.IsMergeStashDeck())
                    {
                        List<PlayerCardData> stashList;
                        PlayerSave.GetPlayerCardStash(out stashList);
                        PlayerSave.GetPlayerDeck(out deckData);
                        List<PlayerCardData> stashDeck = new List<PlayerCardData>(stashList);
                        if (deckData != null && deckData.Count > 0)
                        {
                            // Merge deck into stash.
                            for (int index = 0; index < deckData.Count; ++index)
                            {
                                stashDeck.Add(deckData.GetCardAt(index).PlayerCardData);
                            }
                        }
                        PlayerSave.SavePlayerCardStash(stashDeck);
                        PlayerSave.SetMergeStashDeck(true);
                    }
                    PlayerSave.SetSubmitGameData(true);
                }

                UserDBData user = new UserDBData(
                      PlayerSave.GetPlayerUid()
                    , PlayerSave.GetPlayerName()
                    , PlayerSave.GetPlayerEmail()
                    , PlayerSave.GetPlayerAvatar()
                    , PlayerSave.IsPlayerFirstTime() ? "O" : "X" // First Flow
                    , PlayerSave.GetPlayerLevel()
                    , PlayerSave.GetPlayerExp()
                    , PlayerSave.GetPlayerSilverCoin()
                    , PlayerSave.GetPlayerGoldCoin()
                    , PlayerSave.GetPlayerDiamond()
                    , PlayerSave.GetSoloWin()
                    , PlayerSave.GetSoloDraw()
                    , PlayerSave.GetSoloLose()
                    , PlayerSave.GetWinCount("NPC0001")
                    , PlayerSave.GetWinCount("NPC0002")
                    , PlayerSave.GetWinCount("NPC0003")
                    , PlayerSave.GetWinCount("NPC0004")
                    , PlayerSave.GetWinCount("NPC0005")
                    , PlayerSave.GetWinCount("NPC0006")
                );

                
                // Print Log
                {
                    string log = "";
                    log += string.Format("WriteUserData : ", user.GetDebugPrintText());              
                    ScreenDubugLog.Instance.Print(log);
                }

                // Update player data.
                KOSServer.WriteUserData(
                      user
                    , delegate()
                    {
                        // Complete
                        
                        // Update deck.
                        //DeckData deckData1 = null;   //-vit change deckData1 to deckData
                        PlayerSave.GetPlayerDeck(out deckData);
                        
                        // Print Log
                        {
                            string log = "";
                            log += string.Format("WriteNewDeck : ", deckData.ToString());              
                            ScreenDubugLog.Instance.Print(log);
                        }
                
                        KOSServer.WriteNewDeck(
                              deckData
                            , delegate()
                            {
                                // Completed
                                
                                // Update stash.
                                List<PlayerCardData> stash1;
                                PlayerSave.GetPlayerCardStash(out stash1);
                                Dictionary<string, int> stashData = ConvertCardListToDict(stash1);
                                
                                // Print Log
                                {
                                    string log = "";
                                    log += string.Format("WriteNewStash : ", stashData.ToString());              
                                    ScreenDubugLog.Instance.Print(log);
                                }
                                                               
                                KOSServer.WriteNewStash(
                                      stashData
                                    , delegate()
                                    {
                                        // Completed

                                        // Update deck to card collection.
                                        for (int index = 0; index < deckData.Count; ++index)
                                        {
                                            PlayerSave.SavePlayerCardCollection(deckData.GetCardAt(index).PlayerCardData.CardData.CardID);
                                        }

                                        // Update stash to card collection.
                                        for (int index = 0; index < stash1.Count; ++index)
                                        {
                                            PlayerSave.SavePlayerCardCollection(stash1[index].CardData.CardID);
                                        }

                                        PlayerSave.SetAutoLogin(true);

                                        if (_onSignUpCompleted != null)
                                        {
                                            _onSignUpCompleted.Invoke(PlayerSave.GetPlayerEmail()); //user.Useremail);
                                        }
                                    }
                                    , delegate()
                                    {
                                        // Failed
                                        
                                        //string errorTxt = "SignUp Failed: Unknown error.";
                                        string errorTxt = Localization.Get("TITLE_SIGNUP_MESSAGE_FAILED");
                                        Debug.Log(errorTxt + "Failed to write new stash.");
                                        if (_onSignUpFailed != null)
                                        {
                                            _onSignUpFailed.Invoke(errorTxt);
                        
                                        }
                                    }
                                );
                            }
                            , delegate()
                            {
                                // Failed
                                
                                //string errorTxt = "SignUp Failed: Unknown error.";
                                string errorTxt = Localization.Get("TITLE_SIGNUP_MESSAGE_FAILED");
                                Debug.Log(errorTxt + "Failed to write new deck.");
                                if (_onSignUpFailed != null)
                                {
                                    _onSignUpFailed.Invoke(errorTxt);
                
                                }
                            }
                        );
                    } 
                    , delegate()
                    {
                        // Failed
                        
                        //string errorTxt = "SignUp Failed: Unknown error.";
                        string errorTxt = Localization.Get("TITLE_SIGNUP_MESSAGE_FAILED");
                        Debug.Log(errorTxt + "Failed to write new user data.");
                        if (_onSignUpFailed != null)
                        {
                            _onSignUpFailed.Invoke(errorTxt);
        
                        }
                    }
                );                
            }
            else
            {
                //string errorTxt = "SignUp Failed: Unknown error.";
                string errorTxt = Localization.Get("TITLE_SIGNUP_MESSAGE_FAILED");
                Debug.Log(errorTxt);
                if (_onSignUpFailed != null)
                {
                    _onSignUpFailed.Invoke(errorTxt);

                }
            }
        });
    }

    public Dictionary<string, int> ConvertCardListToDict(DeckData deck)
    {
        List<PlayerCardData> list = new List<PlayerCardData>();
        for (int index = 0; index < deck.Count; ++index)
        {
            list.Add(deck.GetCardAt(index).PlayerCardData);
        }

        return ConvertCardListToDict(list);
    }

    public Dictionary<string, int> ConvertCardListToDict(List<PlayerCardData> cardList)
    {
        Dictionary<string, int> result = new Dictionary<string, int>();

        foreach (PlayerCardData data in cardList)
        {
            if (!result.ContainsKey(data.PlayerCardID))
            {
                // new
                result.Add(data.PlayerCardID, 1);
            }
            else
            {
                // same
                result[data.PlayerCardID] = result[data.PlayerCardID] + 1;
            }
        }

        return result;
    }

    public void Login(string email, string password, OnLoginCompleted onLoginCompleted = null, OnLoginFailed onLoginFailed = null)
    {
        if (_isProcessLogin)
        {
            //string errorTxt = "Login Failed: Login is on process.";
            string errorTxt = Localization.Get("TITLE_LOGIN_MESSAGE_FAILED_CONNECTION");
            Debug.Log(errorTxt);
            ScreenDubugLog.Instance.Print(errorTxt);
            if (onLoginFailed != null)
            {
                onLoginFailed.Invoke(errorTxt);

            }
            return;
        }

        _isLogin = false;
        _isProcessLogin = true;

        _onLoginCompleted = onLoginCompleted;
        _onLoginFailed = onLoginFailed;

        KOSServer._auth.SignInWithEmailAndPasswordAsync(email, password).ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                _isProcessLogin = false;
                _isLogin = false;

                //string errorTxt = "Login Failed: Login has been canceled.";
                string errorTxt = Localization.Get("TITLE_LOGIN_MESSAGE_FAILED_CONNECTION");
                Debug.Log(errorTxt);
                ScreenDubugLog.Instance.Print(errorTxt);
                if (_onLoginFailed != null)
                {
                    _onLoginFailed.Invoke(errorTxt);
                }
                return;
            }

            if (task.IsFaulted)
            {
                _isProcessLogin = false;
                _isLogin = false;

                // string errorTxt = string.Format("Login Failed: Error meassage: {0}", task.Exception);
                string errorTxt = Localization.Get("TITLE_LOGIN_MESSAGE_FAILED_DESC");
                Debug.Log(errorTxt);
                ScreenDubugLog.Instance.Print(errorTxt);
                if (_onLoginFailed != null)
                {
                    _onLoginFailed.Invoke(errorTxt);
                }
                return;
            }

            if (task.IsCompleted)
            {
                ScreenDubugLog.Instance.Print("login complete!!");
                FirebaseUser cuser = task.Result;
                Firebase.Auth.FirebaseUser user = KOSServer._auth.CurrentUser;
                ScreenDubugLog.Instance.Print("user = " + user);

                if (user != null)
                {
                    // Successful. 
                    string log = string.Format("User login successfully: {0} ({1})", user.DisplayName, user.UserId);
                    ScreenDubugLog.Instance.Print(log);
                    Debug.Log(log);

                    _database = FirebaseDatabase.GetInstance(KOSServer._auth.App);
                    _firebaseRef = _database.RootReference;

                    _isProcessLogin = false;
                    _isLogin = true;

                    PlayerSave.SavePlayerUid(KOSServer._auth.CurrentUser.UserId);

                    //Cashe User Data
                    CacheData(delegate ()
                    {
                        //Cashe Stash
                        CacheDataStash(delegate ()
                        {
                            //Cashe Deck
                            CacheDataDeck(delegate ()
                            {
                                PlayerSave.SetAutoLogin(true);

                                if (_onLoginCompleted != null)
                                {
                                    _onLoginCompleted.Invoke();
                                }
                            }, delegate (string errorTxt)
                            {
                                if (_onLoginFailed != null)
                                {
                                    _onLoginFailed.Invoke(errorTxt);
                                }
                            });
                        }, delegate (string errorTxt)
                        {
                            if (_onLoginFailed != null)
                            {
                                _onLoginFailed.Invoke(errorTxt);
                            }
                        });
                    }, delegate (string errorTxt)
                    {
                        if (_onLoginFailed != null)
                        {
                            _onLoginFailed.Invoke(errorTxt);
                        }
                    });

                    /*
                    // For get only user data
                    CacheData(delegate(){
                        if (_onLoginCompleted != null)
                        {
                            _onLoginCompleted.Invoke();
                        }  
                    }, delegate(string errorTxt){
                        if(_onLoginFailed != null)
                        {
                            _onLoginFailed.Invoke(errorTxt);
                        }
                    });
                    */

                    return;
                }
                else
                {
                    _isProcessLogin = false;
                    _isLogin = false;

                    string errorTxt = string.Format("Login Failed: Unknown error.");
                    Debug.Log(errorTxt);
                    if (_onLoginFailed != null)
                    {
                        _onLoginFailed.Invoke(errorTxt);
                    }
                    return;
                }
            }
        });
    }

    public void Logout()
    {
        if (IsLogin)
        {
            _isLogin = false;
            KOSServer._auth.SignOut();
            
            PlayerSave.ClearAllSave();
            PlayerSave.SetAutoLogin(false);
        }
    }

    public void CacheData(OnCacheCompleted onCacheCompleted, OnCacheFailed onCacheFailed)
    {
        ScreenDubugLog.Instance.Print("Start data caching...");
        if (IsLogin)
        {
            if (_isProcessCache)
            {
                string errorTxt = "Rejected start data caching. Other caching in process.";
                ScreenDubugLog.Instance.Print(errorTxt);
                Debug.Log(errorTxt);
                if (onCacheFailed != null)
                {
                    onCacheFailed.Invoke(errorTxt);
                }
                return;
            }

            _isCacheData = false;
            _isProcessCache = true;

            _data = new UserDBData();
            _firebaseRef.Child("player").Child(KOSServer._auth.CurrentUser.UserId).GetValueAsync().ContinueWith(task =>
            {
                _isProcessCache = false;

                if (task.IsFaulted)
                {
                    _isCacheData = false;

                    string errorTxt = "Data caching failed. Task is failed. " + task.Exception;
                    ScreenDubugLog.Instance.Print(errorTxt);
                    Debug.Log(errorTxt);
                    if (onCacheFailed != null)
                    {
                        onCacheFailed.Invoke(errorTxt);
                    }
                }
                else if (task.IsCompleted)
                {
                    DataSnapshot snapshot = task.Result;

                    string json = snapshot.GetRawJsonValue();
                    ScreenDubugLog.Instance.Print("Json=" + json);
                    {
                        try
                        {
                            _data.uid = (string)snapshot.Child("uid").Value;
                            _data.UserName = (string)snapshot.Child("UserName").Value;
                            _data.Useremail = (string)snapshot.Child("Useremail").Value;
                            _data.Userexp = (int)(long)snapshot.Child("Userexp").Value;
                            _data.Userphoto = (string)snapshot.Child("Userphoto").Value;
                            _data.FirstFlow = (string)snapshot.Child("FirstFlow").Value;

                            _data.UsersilverCoin = (int)(long)snapshot.Child("UsersilverCoin").Value;
                            _data.UserDiamond = (int)(long)snapshot.Child("UserDiamond").Value;
                            _data.UsergoldCoin = (int)(long)snapshot.Child("UsergoldCoin").Value;

                            _data.UserSoloWinCount = (int)(long)snapshot.Child("UserSoloWinCount").Value;
                            _data.UserSoloDrawCount = (int)(long)snapshot.Child("UserSoloDrawCount").Value;
                            _data.UserSoloLoseCount = (int)(long)snapshot.Child("UserSoloLoseCount").Value;

                            _data.NPCWinCount1 = (int)(long)snapshot.Child("NPCWinCount1").Value;
                            _data.NPCWinCount2 = (int)(long)snapshot.Child("NPCWinCount2").Value;
                            _data.NPCWinCount3 = (int)(long)snapshot.Child("NPCWinCount3").Value;
                            _data.NPCWinCount4 = (int)(long)snapshot.Child("NPCWinCount4").Value;
                            _data.NPCWinCount5 = (int)(long)snapshot.Child("NPCWinCount5").Value;
                            _data.NPCWinCount6 = (int)(long)snapshot.Child("NPCWinCount6").Value;

                            // Update player save
                            PlayerSave.SavePlayerName(_data.UserName);
                            PlayerSave.SavePlayerUid(_data.uid);
                            PlayerSave.SavePlayerEmail(_data.Useremail);
                            PlayerSave.SavePlayerExp(_data.Userexp);
                            PlayerSave.SavePlayerAvatar(_data.Userphoto);
                            PlayerSave.SavePlayerFirstTime(_data.FirstFlow == "X" ? false : true);

                            PlayerSave.SavePlayerSilverCoin(_data.UsersilverCoin);
                            PlayerSave.SavePlayerDiamond(_data.UserDiamond);
                            PlayerSave.SavePlayerGoldCoin(_data.UsergoldCoin);

                            PlayerSave.SaveSoloWin(_data.UserSoloWinCount);
                            PlayerSave.SaveSoloDraw(_data.UserSoloDrawCount);
                            PlayerSave.SaveSoloLose(_data.UserSoloLoseCount);

                            PlayerSave.SetWinCount("NPC0001", _data.NPCWinCount1);
                            PlayerSave.SetWinCount("NPC0002", _data.NPCWinCount2);
                            PlayerSave.SetWinCount("NPC0003", _data.NPCWinCount3);
                            PlayerSave.SetWinCount("NPC0004", _data.NPCWinCount4);
                            PlayerSave.SetWinCount("NPC0005", _data.NPCWinCount5);
                            PlayerSave.SetWinCount("NPC0006", _data.NPCWinCount6);

                            PlayerSave.SavePlayerAccountType(AccountType.KOS);

                            _isCacheData = true;
                        }
                        catch (System.Exception e)
                        {
                            _isCacheData = false;

                            string errorTxt = string.Format(
                                  "Data caching failed. Error: {0}\nJson={1}"
                                , e.Message.ToString()
                                , json
                            );
                            ScreenDubugLog.Instance.Print(errorTxt);
                            Debug.Log(errorTxt);

                            if (onCacheFailed != null)
                            {
                                onCacheFailed.Invoke(errorTxt);
                            }

                            return;
                        }
                    }

                    if (_isCacheData)
                    {
                        string msg = "Data caching Successful.";
                        ScreenDubugLog.Instance.Print(msg);
                        Debug.Log(msg);

                        if (onCacheCompleted != null)
                        {
                            onCacheCompleted.Invoke();
                        }
                    }
                    else
                    {
                        string errorTxt = string.Format("Data caching failed. Json={0}", json);
                        Debug.Log(errorTxt);

                        if (onCacheFailed != null)
                        {
                            onCacheFailed.Invoke(errorTxt);
                        }
                    }
                }
                else
                {
                    _isCacheData = false;
                    string errorTxt = "Data caching failed. Unknown error.";
                    Debug.Log(errorTxt);

                    if (onCacheFailed != null)
                    {
                        onCacheFailed.Invoke(errorTxt);
                    }
                }
            });
        }
        else
        {
            _isCacheData = false;

            string errorTxt = "Cache data failed. Not login KOS.";
            ScreenDubugLog.Instance.Print(errorTxt);
            Debug.Log(errorTxt);

            if (onCacheFailed != null)
            {
                onCacheFailed.Invoke(errorTxt);
            }
        }
    }

    public void CacheDataStash(OnCacheCompleted onCacheCompleted, OnCacheFailed onCacheFailed = null)
    {
        ScreenDubugLog.Instance.Print("Start stash caching...");

        if (IsLogin)
        {
            string uid = KOSServer._auth.CurrentUser.UserId;

            if (_isProcessCache)
            {
                string errorTxt = "Rejected start stash caching. Other caching in process.";
                ScreenDubugLog.Instance.Print(errorTxt);
                Debug.Log(errorTxt);

                if (onCacheFailed != null)
                {
                    onCacheFailed.Invoke(errorTxt);
                }
                return;
            }

            _isCacheStash = false;
            _isProcessCache = true;

            Dictionary<string, int> card = new Dictionary<string, int>();
            _stashData = new StashFirebaseData(uid, card);

            _firebaseRef.Child("player_stash").Child(uid).GetValueAsync().ContinueWith(task =>
            {
                _isProcessCache = false;

                if (task.IsFaulted)
                {
                    _isCacheStash = false;

                    string errorTxt = "Stash caching failed. Task is failed." + task.Exception; ;
                    ScreenDubugLog.Instance.Print(errorTxt);
                    Debug.Log(errorTxt);

                    if (onCacheFailed != null)
                    {
                        onCacheFailed.Invoke(errorTxt);
                    }
                }
                else if (task.IsCompleted)
                {
                    DataSnapshot snapshot = task.Result;
                    IEnumerator<DataSnapshot> en = snapshot.Children.GetEnumerator();

                    string json = snapshot.GetRawJsonValue();
                    ScreenDubugLog.Instance.Print("Json=" + json);

                    PlayerSave.ClearSave(PlayerSave.SaveIndex.CardStash);
                    PlayerSave.ClearSave(PlayerSave.SaveIndex.CardCollection);

                    while (en.MoveNext())
                    {
                        try
                        {
                            string cardID = en.Current.Key;
                            int cardAmount = int.Parse(en.Current.Value.ToString());

                            _stashData.Card.Add(cardID, cardAmount);

                            // Save to stash
                            for (int index = 0; index < cardAmount; ++index)
                            {
                                PlayerSave.SavePlayerCardStash(cardID);
                            }

                            // Save to card collection.
                            PlayerSave.SavePlayerCardCollection(cardID);

                            _isCacheStash = true;
                        }
                        catch (System.Exception e)
                        {
                            _isCacheStash = false;

                            string errorTxt = string.Format(
                                  "Stash caching failed. Error: {0}\nJson={1}"
                                , e.Message.ToString()
                                , json
                            );
                            ScreenDubugLog.Instance.Print(errorTxt);
                            Debug.Log(errorTxt);

                            if (onCacheFailed != null)
                            {
                                onCacheFailed.Invoke(errorTxt);
                            }
                            return;
                        }
                    }

                    if (_isCacheStash)
                    {
                        string msg = "Stash caching successful.";
                        ScreenDubugLog.Instance.Print(msg);
                        Debug.Log(msg);

                        if (onCacheCompleted != null)
                        {
                            onCacheCompleted.Invoke();
                        }
                    }
                    else
                    {
                        string errorTxt = string.Format("Stash caching failed. Json={0}", json);
                        ScreenDubugLog.Instance.Print(errorTxt);
                        Debug.Log(errorTxt);

                        if (onCacheFailed != null)
                        {
                            onCacheFailed.Invoke(errorTxt);
                        }
                    }
                }
                else
                {
                    _isCacheStash = false;

                    string errorTxt = "Stash caching failed: Unknown error.";
                    ScreenDubugLog.Instance.Print(errorTxt);
                    Debug.Log(errorTxt);

                    if (onCacheFailed != null)
                    {
                        onCacheFailed.Invoke(errorTxt);
                    }
                }
            });
        }
        else
        {
            _isCacheStash = false;

            string errorTxt = "Stash caching failed. Not login KOS.";
            ScreenDubugLog.Instance.Print(errorTxt);
            Debug.Log(errorTxt);

            if (onCacheFailed != null)
            {
                onCacheFailed.Invoke(errorTxt);
            }
        }
    }

    public void CacheChangePassword(string newPwd,OnCacheCompleted onCacheCompleted, OnCacheFailed onCacheFailed = null)
    {
        ScreenDubugLog.Instance.Print("Start change password caching...");
        if (IsLogin)
        {
            if (_isProcessCache)             {                 string errorTxt = "Rejected start data caching. Other caching in process.";                 ScreenDubugLog.Instance.Print(errorTxt);                 Debug.Log(errorTxt);                 if (onCacheFailed != null)                 {                     onCacheFailed.Invoke(errorTxt);                 }                 return;             }              _isCacheData = false;             _isProcessCache = true;              //_data = new UserDBData();             KOSServer._auth.CurrentUser.UpdatePasswordAsync(newPwd).ContinueWith(task =>
            {
                _isProcessCache = false;                  if (task.IsFaulted)                 {                     _isCacheData = false;                      string errorTxt = "Change password caching failed. Task is failed. " + task.Exception;                     ScreenDubugLog.Instance.Print(errorTxt);                     Debug.Log(errorTxt);                     if (onCacheFailed != null)                     {                         onCacheFailed.Invoke(errorTxt);                     }                 }

                else if (task.IsCompleted)                 {
                    try
                    {
                        PlayerSave.SavePlayerPassword(newPwd);

                        _isCacheData = true;

                    }

                    catch (System.Exception e)
                    {
                        _isCacheData = false;

                        string errorTxt = string.Format(
                              "change password caching failed. Error: {0}"
                            , e.Message.ToString()
                            , ""
                        );
                        ScreenDubugLog.Instance.Print(errorTxt);
                        Debug.Log(errorTxt);

                        if (onCacheFailed != null)
                        {
                            onCacheFailed.Invoke(errorTxt);
                        }

                        return;
                    }

                    if (_isCacheData)
                    {
                        string msg = "Data caching Successful.";
                        ScreenDubugLog.Instance.Print(msg);
                        Debug.Log(msg);

                        if (onCacheCompleted != null)
                        {
                            onCacheCompleted.Invoke();
                        }
                    }
                    else
                    {
                        string errorTxt = string.Format("Data caching failed.", "");
                        Debug.Log(errorTxt);

                        if (onCacheFailed != null)
                        {
                            onCacheFailed.Invoke(errorTxt);
                        }
                    }


                } //task is complete

                else
                {
                    _isCacheData = false;
                    string errorTxt = "Data caching failed. Unknown error.";
                    Debug.Log(errorTxt);

                    if (onCacheFailed != null)
                    {
                        onCacheFailed.Invoke(errorTxt);
                    }
                }
            });
        }
        else         {
                _isCacheData = false;

                string errorTxt = "Cache data failed. Not login KOS.";
                ScreenDubugLog.Instance.Print(errorTxt);
                Debug.Log(errorTxt);

                if (onCacheFailed != null)
                {
                    onCacheFailed.Invoke(errorTxt);
                }
            } 
    }
          
    public void CacheDataDeck(OnCacheCompleted onCacheCompleted, OnCacheFailed onCacheFailed = null)
    {
        ScreenDubugLog.Instance.Print("Start deck caching...");

        if (IsLogin)
        {
            string uid = KOSServer._auth.CurrentUser.UserId;

            if (_isProcessCache)
            {
                string errorTxt = "Rejected start deck caching. Other caching in process.";
                ScreenDubugLog.Instance.Print(errorTxt);
                Debug.Log(errorTxt);

                if (onCacheFailed != null)
                {
                    onCacheFailed.Invoke(errorTxt);
                }
                return;
            }

            _isCacheDeck = false;
            _isProcessCache = true;

            Dictionary<string, int> card = new Dictionary<string, int>();
            _deckData = new DeckData();

            _firebaseRef.Child("player_deck").Child(uid).GetValueAsync().ContinueWith(task =>
            {
                _isProcessCache = false;

                if (task.IsFaulted)
                {
                    _isCacheDeck = false;

                    string errorTxt = "Deck caching failed: Task is failed. " + task.Exception;
                    ScreenDubugLog.Instance.Print(errorTxt);
                    Debug.Log(errorTxt);

                    if (onCacheFailed != null)
                    {
                        onCacheFailed.Invoke(errorTxt);
                    }
                }
                else if (task.IsCompleted)
                {
                    DataSnapshot snapshot = task.Result;
                    IEnumerator<DataSnapshot> en = snapshot.Children.GetEnumerator();

                    string json = snapshot.GetRawJsonValue();
                    ScreenDubugLog.Instance.Print("Json=" + json);

                    while (en.MoveNext())
                    {
                        try
                        {
                            string cardID = en.Current.Key;
                            int cardAmount = int.Parse(en.Current.Value.ToString());

                            for (int index = 0; index < cardAmount; ++index)
                            {
                                _deckData.Add(new PlayerCardData(cardID));
                            }

                            _isCacheDeck = true;
                        }
                        catch (System.Exception e)
                        {
                            _isCacheDeck = false;

                            string errorTxt = string.Format(
                                  "Deck caching failed. Error: {0}\nJson={1}"
                                , e.Message.ToString()
                                , json
                            );

                            Debug.Log(errorTxt);
                            if (onCacheFailed != null)
                            {
                                onCacheFailed.Invoke(errorTxt);
                            }
                            return;
                        }
                    }

                    if (_isCacheDeck)
                    {
                        PlayerSave.SavePlayerDeck(_deckData);

                        string msg = "Deck caching successful.";
                        Debug.Log(msg);
                        if (onCacheCompleted != null)
                        {
                            onCacheCompleted.Invoke();
                        }
                    }
                    else
                    {
                        string errorTxt = string.Format("Deck caching failed. Json={0}", json);
                        ScreenDubugLog.Instance.Print(errorTxt);
                        Debug.Log(errorTxt);

                        if (onCacheFailed != null)
                        {
                            onCacheFailed.Invoke(errorTxt);
                        }
                    }
                }
                else
                {
                    _isCacheDeck = false;

                    string errorTxt = "Deck caching failed. Unknown error.";
                    ScreenDubugLog.Instance.Print(errorTxt);
                    Debug.Log(errorTxt);

                    if (onCacheFailed != null)
                    {
                        onCacheFailed.Invoke(errorTxt);
                    }
                }
            });
        }
        else
        {
            _isCacheDeck = false;

            string errorTxt = "Deck caching failed. Not login KOS.";
            ScreenDubugLog.Instance.Print(errorTxt);
            Debug.Log(errorTxt);

            if (onCacheFailed != null)
            {
                onCacheFailed.Invoke(errorTxt);
            }
        }
    }

    public static void WriteUserData(UserDBData data, UnityAction onComplete = null, UnityAction onFailed = null)
    {
        ScreenDubugLog.Instance.Print("WriteUserData");
        KOSServer.WriteData<UserDBData>(KOSServer.DataTable.Player, data.uid, data, onComplete, onFailed);
    }

    public static void WriteNewStash(Dictionary<string, int> cardList, UnityAction onComplete, UnityAction onFailed)
    {
        ScreenDubugLog.Instance.Print("WriteNewStash");

        string json = MiniJSON.Json.Serialize(cardList);

        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl(_url);

        // Get the root reference location of the database.
        DatabaseReference mdatabaseRef = FirebaseDatabase.DefaultInstance.RootReference;
        FirebaseDatabase mdatabase = FirebaseDatabase.GetInstance(_url);

        ScreenDubugLog.Instance.Print("uid=" + PlayerSave.GetPlayerUid() + " Json=" + json);

        GameObject obj = new GameObject();
        WriteDataTask writeDataTask = obj.AddComponent<WriteDataTask>();

        System.Threading.Tasks.Task task = mdatabaseRef.Child(GetTableKey(KOSServer.DataTable.Stash)).Child(PlayerSave.GetPlayerUid()).SetRawJsonValueAsync(json);
        writeDataTask.SetupTask(task, onComplete, onFailed);
    }

    public static Dictionary<string, int> ToSaveServerDeck(DeckData deckData)
    {
        string cardID = "";
        Dictionary<string, int> deck;
        deck = new Dictionary<string, int>();
        deck.Clear();

        for (int index = 0; index < deckData.Count; ++index)
        {

            cardID = deckData.GetCardAt(index).PlayerCardData.CardID;

            if (deck.ContainsKey(cardID))
            {
                deck[cardID]++;
            }
            else
            {
                deck.Add(cardID, 1);
            }
        }

        return deck;
    }

    public static void WriteNewDeck(DeckData deckData, UnityAction onComplete, UnityAction onFailed)
    {
        ScreenDubugLog.Instance.Print("WriteNewDeck");

        Dictionary<string, int> deck = ToSaveServerDeck(deckData);

        string json = MiniJSON.Json.Serialize(deck);

        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl(_url);

        // Get the root reference location of the database.
        DatabaseReference mdatabaseRef = FirebaseDatabase.DefaultInstance.RootReference;
        FirebaseDatabase mdatabase = FirebaseDatabase.GetInstance(_url);

        ScreenDubugLog.Instance.Print("uid=" + PlayerSave.GetPlayerUid() + " Json=" + json);

        GameObject obj = new GameObject();
        WriteDataTask writeDataTask = obj.AddComponent<WriteDataTask>();

        System.Threading.Tasks.Task task = mdatabaseRef.Child(GetTableKey(KOSServer.DataTable.Deck)).Child(PlayerSave.GetPlayerUid()).SetRawJsonValueAsync(json);
        writeDataTask.SetupTask(task, onComplete, onFailed);
    }

    public static string GetNewPlayerNameUniqueID()
    {
        DateTime epochStart = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        int currentEpochTime = (int)(DateTime.UtcNow - epochStart).TotalSeconds;
        int z1 = UnityEngine.Random.Range(0, 1000000);
        int z2 = UnityEngine.Random.Range(0, 1000000);
        string uid = "U" + currentEpochTime + ":" + z1; //+ ":" + z2;
        return uid;
    }

    /*
    public static void AddSilverCoin(int amount, UnityAction onComplete = null, UnityAction onFailed = null)
    {
        if (_instance != null)
        {
            if (_instance.IsLogin && _instance.IsCache)
            {
                int currentSilver = _instance._data.UsersilverCoin;
            
                RequestAddSilver(
                      amount
                    , delegate(bool isSuccess, string result) 
                    {
                        if (isSuccess)
                        {
                            _instance._data.UsersilverCoin = currentSilver + amount;
                        
                            if (onComplete != null)
                            {
                                onComplete.Invoke();
                            }
                        }
                        else
                        {
                            if(onFailed != null)
                            {
                                onFailed.Invoke();
                            }
                        }

	                }
                );
                
                return;
            }
        }
        
        if(onFailed != null)
        {
            onFailed.Invoke();
        }
    }
    */

    /*
    public static void WriteNewNPCWinCount(string npcID, int newWinCount, UnityAction onComplete = null, UnityAction onFailed = null)
    {
        if (_instance != null)
        {
            if (_instance.IsLogin && _instance.IsCache)
            {
                switch (npcID)
                {
                    case "NPC0001":
                        _instance._data.NPCWinCount1 = newWinCount;
                        WriteUserData(_instance._data, onComplete, onFailed);
                        return;
                        
                    case "NPC0002":
                        _instance._data.NPCWinCount2 = newWinCount;
                        WriteUserData(_instance._data, onComplete, onFailed);
                        return;
                        
                    case "NPC0003":
                        _instance._data.NPCWinCount3 = newWinCount;
                        WriteUserData(_instance._data, onComplete, onFailed);
                        return;
                        
                    case "NPC0004":
                        _instance._data.NPCWinCount4 = newWinCount;
                        WriteUserData(_instance._data, onComplete, onFailed);
                        return;
                        
                    case "NPC0005":
                        _instance._data.NPCWinCount5 = newWinCount;
                        WriteUserData(_instance._data, onComplete, onFailed);
                        return;
                        
                    case "NPC0006":
                        _instance._data.NPCWinCount6 = newWinCount;
                        WriteUserData(_instance._data, onComplete, onFailed);
                        return;
                        
                    default:
                        break;
                }

            }
        }
        
        if(onFailed != null)
        {
            onFailed.Invoke();
        }
    }
    */

    /*
    public static void WritePlayerFirstTime()
    {
        if (_instance != null)
        {
            if (_instance.IsLogin && _instance.IsCache)
            {
                _instance._data.FirstFlow = "X";
                WriteUserData(_instance._data);
            }
        }
    }
    */

    private static void WriteData<T>(DataTable dataTable, string dataKey, T data, UnityAction onComplete, UnityAction onFailed)
    {
        string json = JsonUtility.ToJson(data);

        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl(_url);

        // Get the root reference location of the database.
        DatabaseReference mdatabaseRef = FirebaseDatabase.DefaultInstance.RootReference;
        FirebaseDatabase mdatabase = FirebaseDatabase.GetInstance(_url);

        Debug.Log("Json = " + json);

        GameObject obj = new GameObject();
        WriteDataTask writeDataTask = obj.AddComponent<WriteDataTask>();

        System.Threading.Tasks.Task task = mdatabaseRef.Child(GetTableKey(dataTable)).Child(dataKey).SetRawJsonValueAsync(json);
        writeDataTask.SetupTask(task, onComplete, onFailed);
    }

    private static void ReadData<T>(DataTable dataTable, string datakey, T data)
    {
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl(_url);
        DatabaseReference mdatabaseRef = FirebaseDatabase.DefaultInstance.RootReference;
        FirebaseDatabase mdatabase = FirebaseDatabase.GetInstance(_url);
    }

    public static void UpdateUserData(UserDBData data, string fieldToUpdate, object value)
    {
        KOSServer.UpdateDataByField<UserDBData>(KOSServer.DataTable.Player, data.uid, fieldToUpdate, value, data);
    }

    public static void UpdateStashData(StashFirebaseData data, string fieldToUpdate, object value)
    {
        try
        {
            KOSServer.UpdateDataByFieldNew<StashFirebaseData>(KOSServer.DataTable.Stash, data.UID, fieldToUpdate, value, data);
        }

        catch (System.Exception e)
        {
            Debug.Log("Update Error : " + e.Message);
        }
    }

    private static void UpdateDataByFieldNew<T>(DataTable dataTable, string dataKey, string fieldName, object value, T data)
    {
        string json = MiniJSON.Json.Serialize(data);

        Debug.Log("update data by field Json = " + json);
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl(_url);

        // Get the root reference location of the database.
        DatabaseReference mdatabaseRef = FirebaseDatabase.DefaultInstance.RootReference;
        FirebaseDatabase mdatabase = FirebaseDatabase.GetInstance(_url);

        mdatabaseRef.Child(GetTableKey(dataTable)).Child(dataKey).Child(fieldName).SetValueAsync(value);
    }

    private static void UpdateDataByField<T>(DataTable dataTable, string dataKey, string fieldName, object value, T data)
    {
        string json = JsonUtility.ToJson(data);

        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl(_url);

        // Get the root reference location of the database.
        DatabaseReference mdatabaseRef = FirebaseDatabase.DefaultInstance.RootReference;
        FirebaseDatabase mdatabase = FirebaseDatabase.GetInstance(_url);

        mdatabaseRef.Child(GetTableKey(dataTable)).Child(dataKey).Child(fieldName).SetValueAsync(value);
    }
    //shoppackid 1 = normal , 2 = premium pack
    public static void RequestBuyShopPack(int shopPackID, WebRequest.OnRequestCompleted onComplete)
    {
        RequestBuyShopPack(shopPackID, PlayerSave.GetPlayerUid(), onComplete);
    }

    public static void RequestBuyShopPack(int shopPackID, string playerHash, WebRequest.OnRequestCompleted onComplete)
    {
        string url = "https://us-central1-koscardgame.cloudfunctions.net/requestBuyShopPack";

#if UNITY_EDITOR
        url += "/" + shopPackID + "/" + "R5bQrA3lVPYHsbEPtSZf2BZraJ13"; //"3RENwwBoCgTLkogxXc8ME3L4uS03";
#else
        url += "/" + shopPackID + "/" + playerHash;
#endif

        ScreenDubugLog.Instance.Print("RequestBuyShopPack: " + url);

        WebRequestURL(url, onComplete);
    }

    public static void WebRequestURL(string url, WebRequest.OnRequestCompleted onComplete)
    {
        WebRequest.RequestUrl(url, onComplete);
    }

    public static void RequestKOSVersion(OnGetVersionCompleted onGetVersionCompleted)
    {
        string sVersion = "";
        string sCheckActive = "1";

        FirebaseApp app = FirebaseApp.DefaultInstance;
        app.SetEditorDatabaseUrl(_url);

        FirebaseDatabase.DefaultInstance
        .GetReference("Version")
        .GetValueAsync().ContinueWith(
            task =>
            {
                if (task.IsFaulted)
                {
                    // Handle the error...                    
                    if (onGetVersionCompleted != null)
                    {
                        onGetVersionCompleted.Invoke(false, "faulted", (sCheckActive != "0") ? true : false);
                    }
                }
                else if (task.IsCompleted)
                {
                    DataSnapshot snapshot = task.Result;
    
                    try
                    {
                        string json = snapshot.GetRawJsonValue();
    
                        //Debug.Log("json =  " + json);
    
                        #if UNITY_IOS
                        sVersion = (string)snapshot.Child("iOSCurrentVersion").Value;
                        sCheckActive = (string)snapshot.Child("iOSActiveCheck").Value;
                        #elif UNITY_ANDROID
                        sVersion = (string)snapshot.Child("AndroidCurrentVersion").Value;
                        sCheckActive = (string)snapshot.Child("AndroidActiveCheck").Value;
                        #endif
    
                        //Debug.Log("version =  " + sVersion);
    
                        if (onGetVersionCompleted != null)
                        {
                            onGetVersionCompleted(true, sVersion, (sCheckActive != "0") ? true : false);
                        }
                    }
                    catch (System.Exception e)
    				{                        
                        if(onGetVersionCompleted != null)
                        {
                            onGetVersionCompleted.Invoke(false, e.Message, (sCheckActive != "0") ? true : false);
                        }
    				}
                }
            }
        );
	}

    public static void RequestServerChangePassword(string currPwd, string newPwd, UnityAction onChangePasswordCompleted, UnityAction<string> onChangePasswordFailed)                                            
    {
        try
        {
            FirebaseAuth auth = FirebaseAuth.DefaultInstance;
            string email = PlayerSave.GetPlayerEmail();
            ScreenDubugLog.Instance.Print("email = " + email);

            if (_instance._isProcessPassword)
            {
                string errorTxt = "Change Password Failed: Change Password is on process.";
                Debug.Log(errorTxt);
                ScreenDubugLog.Instance.Print(errorTxt);
                if (onChangePasswordFailed != null)
                {
                    onChangePasswordFailed.Invoke(errorTxt);

                }
                return;
            }

            // To do request to server to change name.

            if (email != "")
            {
                _instance._isProcessPassword = true;

                KOSServer._auth.CurrentUser.UpdatePasswordAsync(newPwd).ContinueWith(task =>
                {

                //    auth.CurrentUser.UpdatePasswordAsync(newPwd).ContinueWith(task =>
                //{
                    if (task.IsCanceled)
                    {
                        _instance._isProcessPassword = false;
                       // string errorTxt = "Change Password Failed: Change Password has been canceld.";
                        string errorTxt = Localization.Get("TITLE_FORGET_PASSWORD_MESSAGE_CANCEL");
                        Debug.Log(errorTxt + task.Exception.Message);
                        ScreenDubugLog.Instance.Print(errorTxt);
                        if (onChangePasswordFailed != null)
                        {
                            onChangePasswordFailed.Invoke(errorTxt);
                        }
                        return;
                    }

                    if (task.IsFaulted)
                    {
                        _instance._isProcessPassword = false;
                        //string errorTxt = string.Format("Change Password Failed: Error meassage: {0}", task.Exception);
                        string errorTxt = Localization.Get("TITLE_FORGET_PASSWORD_MESSAGE_FAILED");
                        Debug.Log(errorTxt + task.Exception.Message);
                        ScreenDubugLog.Instance.Print(errorTxt);

                        if (onChangePasswordFailed != null)
                        {
                            onChangePasswordFailed.Invoke(errorTxt);
                        }
                        return;
                    }

                    if (task.IsCompleted)
                    {
                        PlayerSave.SavePlayerPassword(newPwd);

                        if (onChangePasswordCompleted != null)
                        {
                            onChangePasswordCompleted.Invoke();
                        }
                        return;
                    }
                });
            }
            else
            {
                if (onChangePasswordFailed != null)
                {
                    onChangePasswordFailed.Invoke("Email empty.");
                }
                return;
            }
        }

        catch (System.Exception e)
        {
            ScreenDubugLog.Instance.Print(e.Message);
            return;
        }
    }
    
    public static void RequestSubmitMatchLog(MatchLogData log)
    {
        string json = MiniJSON.Json.Serialize(log);        
        Debug.Log("Json = " + json);

        KOSServer.WriteData<MatchLogData>(KOSServer.DataTable.Matchlog, log.Key, log, null, null);
    }
    
    public static void RequestMatchLog(UnityAction<bool, List<MatchLogData>> onComplete)
    {
        FirebaseApp app = FirebaseApp.DefaultInstance;
        app.SetEditorDatabaseUrl(_url);

        FirebaseDatabase.DefaultInstance
        .GetReference("match_log")
        .GetValueAsync().ContinueWith(
            task => {
                if (task.IsFaulted)
                {
                    Debug.Log("Task Faulted");
                                       
                    if(onComplete != null)
                    {
                        onComplete.Invoke(false, null);
                    }
                }
                else if (task.IsCompleted)
                {
                    DataSnapshot snapshot = task.Result;
                    List<MatchLogData> logList = new List<MatchLogData>();

                    try
                    {
                        //string json = snapshot.GetRawJsonValue();
                        //Debug.Log("json =  " + json);
                        
                        foreach(DataSnapshot child in snapshot.Children)
                        {
                            string childJson = child.GetRawJsonValue();
                            //Debug.Log("childJson =  " + childJson);
                            
                            MatchLogData logData = JsonUtility.FromJson<MatchLogData>(childJson);
                            if(logData != null)
                            {
                                logData.Time = MatchLogData.ToDateTime(logData.Key);
                                logList.Add(logData);
                            }
                        }
                        
                        if(onComplete != null)
                        {
                            onComplete.Invoke(true, logList);
                        }
                    }
                    catch (System.Exception e)
                    {
                        Debug.Log("Error: " + e.Message);
                        
                        if(onComplete != null)
                        {
                            onComplete.Invoke(false, null);
                        }
                    }
                }
                else
                {
                    Debug.Log("Error: Unknown error.");
                
                    if(onComplete != null)
                    {
                        onComplete.Invoke(false, null);
                    }
                }
            }
        );
    }
    
    #region Receipt Methods
    public static void RequestSaveReceipt(PurchaseReceipt receipt, UnityAction onComplete, UnityAction onFailed)
    {
        KOSServer.WriteData<PurchaseReceipt>(KOSServer.DataTable.Receipts, receipt.TransactionPath, receipt, onComplete, onFailed);
    }
    
    public static void RequestRedeemReceipt(PurchaseReceipt receipt, string playerHash, WebRequest.OnRequestCompleted onComplete)
    {
        string url = "https://us-central1-koscardgame.cloudfunctions.net/requestRedeemGoldPack";

        url += "/" + playerHash + "/" + receipt.TransactionPath;
        
        ScreenDubugLog.Instance.Print("RequestRedeemReceipt: " + url);
        
        WebRequestURL(url, onComplete);
    }
    #endregion

    #region Update Player Data Methods
    public enum PlayerDataKey
    {
          FirstFlow
        , NPCWinCount1
        , NPCWinCount2
        , NPCWinCount3
        , NPCWinCount4
        , NPCWinCount5
        , NPCWinCount6
        , UserDiamond
        , UserName
        , UserSoloDrawCount
        , UserSoloLoseCount
        , UserSoloWinCount
        , UserEmail
        , UserEXP
        , UserGoldCoin
        , UserLevel
        , UserPhoto
        , UserSilverCoin
        , UID
    }

    public static string GetRealPlayerDataKey(PlayerDataKey playerDataKey)
    {
        switch (playerDataKey)
        {
            case PlayerDataKey.FirstFlow: return "FirstFlow";
            case PlayerDataKey.NPCWinCount1: return "NPCWinCount1";
            case PlayerDataKey.NPCWinCount2: return "NPCWinCount2";
            case PlayerDataKey.NPCWinCount3: return "NPCWinCount3";
            case PlayerDataKey.NPCWinCount4: return "NPCWinCount4";
            case PlayerDataKey.NPCWinCount5: return "NPCWinCount5";
            case PlayerDataKey.NPCWinCount6: return "NPCWinCount6";
            case PlayerDataKey.UserDiamond: return "UserDiamond";
            case PlayerDataKey.UserName: return "UserName";
            case PlayerDataKey.UserSoloDrawCount: return "UserSoloDrawCount";
            case PlayerDataKey.UserSoloLoseCount: return "UserSoloLoseCount";
            case PlayerDataKey.UserSoloWinCount: return "UserSoloWinCount";
            case PlayerDataKey.UserEmail: return "Useremail";
            case PlayerDataKey.UserEXP: return "Userexp";
            case PlayerDataKey.UserGoldCoin: return "UsergoldCoin";
            case PlayerDataKey.UserLevel: return "Userlevel";
            case PlayerDataKey.UserPhoto: return "Userphoto";
            case PlayerDataKey.UserSilverCoin: return "UsersilverCoin";
            case PlayerDataKey.UID: return "uid";
        
            default: return "";
        }
    }
       
    public static void RequestPlayerData(UnityAction<UserDBData> onComplete, UnityAction<string> onFail)
    {
        try
        {
            FirebaseDatabase database = FirebaseDatabase.GetInstance(KOSServer._auth.App);
            DatabaseReference firebaseRef = database.RootReference;

            firebaseRef.Child(GetTableKey(DataTable.Player))
            .Child(KOSServer._auth.CurrentUser.UserId)
            .GetValueAsync()
            .ContinueWith(
                task =>
                {
                    if (task.IsCompleted)
                    {
                        DataSnapshot snapshot = task.Result;
    
                        string json = snapshot.GetRawJsonValue();
                        UserDBData playerData = new UserDBData();
    
                        playerData.uid = (string)snapshot.Child(GetRealPlayerDataKey(PlayerDataKey.UID)).Value;
                        playerData.UserName = (string)snapshot.Child(GetRealPlayerDataKey(PlayerDataKey.UserName)).Value;
                        playerData.Useremail = (string)snapshot.Child(GetRealPlayerDataKey(PlayerDataKey.UserEmail)).Value;
                        playerData.Userexp = (int)(long)snapshot.Child(GetRealPlayerDataKey(PlayerDataKey.UserEXP)).Value;
                        playerData.Userphoto = (string)snapshot.Child(GetRealPlayerDataKey(PlayerDataKey.UserPhoto)).Value;
                        playerData.FirstFlow = (string)snapshot.Child(GetRealPlayerDataKey(PlayerDataKey.FirstFlow)).Value;
    
                        playerData.UsersilverCoin = (int)(long)snapshot.Child(GetRealPlayerDataKey(PlayerDataKey.UserSilverCoin)).Value;
                        playerData.UserDiamond = (int)(long)snapshot.Child(GetRealPlayerDataKey(PlayerDataKey.UserDiamond)).Value;
                        playerData.UsergoldCoin = (int)(long)snapshot.Child(GetRealPlayerDataKey(PlayerDataKey.UserGoldCoin)).Value;
    
                        playerData.UserSoloWinCount = (int)(long)snapshot.Child(GetRealPlayerDataKey(PlayerDataKey.UserSoloWinCount)).Value;
                        playerData.UserSoloDrawCount = (int)(long)snapshot.Child(GetRealPlayerDataKey(PlayerDataKey.UserSoloDrawCount)).Value;
                        playerData.UserSoloLoseCount = (int)(long)snapshot.Child(GetRealPlayerDataKey(PlayerDataKey.UserSoloLoseCount)).Value;
    
                        playerData.NPCWinCount1 = (int)(long)snapshot.Child(GetRealPlayerDataKey(PlayerDataKey.NPCWinCount1)).Value;
                        playerData.NPCWinCount2 = (int)(long)snapshot.Child(GetRealPlayerDataKey(PlayerDataKey.NPCWinCount2)).Value;
                        playerData.NPCWinCount3 = (int)(long)snapshot.Child(GetRealPlayerDataKey(PlayerDataKey.NPCWinCount3)).Value;
                        playerData.NPCWinCount4 = (int)(long)snapshot.Child(GetRealPlayerDataKey(PlayerDataKey.NPCWinCount4)).Value;
                        playerData.NPCWinCount5 = (int)(long)snapshot.Child(GetRealPlayerDataKey(PlayerDataKey.NPCWinCount5)).Value;
                        playerData.NPCWinCount6 = (int)(long)snapshot.Child(GetRealPlayerDataKey(PlayerDataKey.NPCWinCount6)).Value;
    
                        if (onComplete != null)
                        {
                            onComplete.Invoke(playerData);
                        }
                    }
                    else if (task.IsFaulted)
                    {
                        string errorTxt = string.Format("RequestPlayerData failed. {0}", task.Exception);
    
                        if (onFail != null)
                        {
                            onFail.Invoke(errorTxt);
                        }
                    }
                    else
                    {
                        string errorTxt = string.Format("RequestPlayerData failed. Unknown error.");
    
                        if (onFail != null)
                        {
                            onFail.Invoke(errorTxt);
                        }
                    }
                }
            );
        }
        catch (System.Exception e)
        {
            string errorTxt = string.Format("RequestPlayerData failed. {0}", e.Message.ToString());

            if (onFail != null)
            {
                onFail.Invoke(errorTxt);
            }
        }
    }

    public static void UpdatePlayerData(Dictionary<PlayerDataKey, object> datas)
    {
        if (datas != null)
        {
            foreach (KeyValuePair<PlayerDataKey, object> keyValue in datas)
            {
                UpdatePlayerData(keyValue.Key, keyValue.Value);
            }
        } 
    }

    public static void UpdatePlayerData(PlayerDataKey playerDataKey, object value)
    {
        UpdateDataByField(DataTable.Player, PlayerSave.GetPlayerUid(), GetRealPlayerDataKey(playerDataKey), value);
    }
       
    private static void UpdateDataByField(DataTable dataTable, string key, string fieldName, object value)
    {
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl(_url);
    
        DatabaseReference mdatabaseRef = FirebaseDatabase.DefaultInstance.RootReference;
        mdatabaseRef.Child(GetTableKey(dataTable)).Child(key).Child(fieldName).SetValueAsync(value);
    }
    
    public static void RequestCompleteFirstFlow(UnityAction onComplete, UnityAction<string> onFail)
    {
        UpdatePlayerData(PlayerDataKey.FirstFlow, "X");
    }
   
    public static void RequestAddSilver(int amount, UnityAction onComplete, UnityAction<string> onFail)
    {
        RequestPlayerData(
            delegate (UserDBData playerData)
            {
                int newValue = playerData.UsersilverCoin + amount;

                RequestUpdateSilver(newValue, onComplete, onFail);
            }
            , onFail
        );
    }

    public static void RequestUseSilver(int amount, UnityAction onComplete, UnityAction<string> onFail)
    {
        RequestPlayerData(
            delegate (UserDBData playerData)
            {
                int newValue = playerData.UsersilverCoin - amount;
                if (newValue >= 0)
                {
                    RequestUpdateSilver(newValue, onComplete, onFail);
                }
                else
                {
                    _instance._data.UsersilverCoin = playerData.UsersilverCoin;
                
                    if (onFail != null)
                    {
                        onFail.Invoke("RequestUseSilver failed. Not enough silver.");
                    }
                }
            }
            , onFail
        );
    }

    private static void RequestUpdateSilver(int newValue, UnityAction onComplete, UnityAction<string> onFail)
    {
        try
        {
            KOSServer.UpdatePlayerData(PlayerDataKey.UserSilverCoin, newValue);

            _instance._data.UsersilverCoin = newValue;

            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }
        catch (System.Exception e)
        {
            if (onFail != null)
            {
                onFail.Invoke(e.Message);
            }
        }  
    }

    public static void RequestAddWinCount(string npcID, int silverReward, int exp, UnityAction onComplete, UnityAction<string> onFail)
    {
        RequestPlayerData(
            delegate (UserDBData playerData)
            {
                Dictionary<PlayerDataKey, object> updateDatas = new Dictionary<PlayerDataKey, object>();

                PlayerDataKey npcWinKey = PlayerDataKey.NPCWinCount1;
                int newWinValue = -1; 
                
                switch (npcID)
                {        
                    case "NPC0001": npcWinKey = PlayerDataKey.NPCWinCount1; newWinValue = playerData.NPCWinCount1 + 1; break;
                    case "NPC0002": npcWinKey = PlayerDataKey.NPCWinCount2; newWinValue = playerData.NPCWinCount2 + 1; break;
                    case "NPC0003": npcWinKey = PlayerDataKey.NPCWinCount3; newWinValue = playerData.NPCWinCount3 + 1; break;
                    case "NPC0004": npcWinKey = PlayerDataKey.NPCWinCount4; newWinValue = playerData.NPCWinCount4 + 1; break;
                    case "NPC0005": npcWinKey = PlayerDataKey.NPCWinCount5; newWinValue = playerData.NPCWinCount5 + 1; break;
                    case "NPC0006": npcWinKey = PlayerDataKey.NPCWinCount6; newWinValue = playerData.NPCWinCount6 + 1; break;
                    
                    default:
                    { 
                        if (onFail != null)
                        {
                            onFail.Invoke("RequestAddWinCount failed. invalid npcID.");
                        }
                        return;
                    }
                }
                
                updateDatas.Add(npcWinKey, newWinValue);
                updateDatas.Add(PlayerDataKey.UserSilverCoin, playerData.UsersilverCoin + silverReward);
                updateDatas.Add(PlayerDataKey.UserEXP, playerData.Userexp + exp);

                UpdatePlayerData(updateDatas);

                if (onComplete != null)
                {
                    onComplete.Invoke(); 
                }
            }
            , onFail
        ); 
    }

    public static void RequestAddWinCount(string npcID, UnityAction onComplete, UnityAction<string> onFail)
    {
        RequestPlayerData(
            delegate (UserDBData playerData)
            {
                int newValue = -1; 
                
                switch (npcID)
                {
                    case "NPC0001": newValue = playerData.NPCWinCount1 + 1; break;
                    case "NPC0002": newValue = playerData.NPCWinCount2 + 1; break;
                    case "NPC0003": newValue = playerData.NPCWinCount3 + 1; break;
                    case "NPC0004": newValue = playerData.NPCWinCount4 + 1; break;
                    case "NPC0005": newValue = playerData.NPCWinCount5 + 1; break;
                    case "NPC0006": newValue = playerData.NPCWinCount6 + 1; break;
                    
                    default:
                    { 
                        if (onFail != null)
                        {
                            onFail.Invoke("RequestUpdateWinCount failed. invalid npcID.");
                        }
                        return;
                    }
                }

                RequestUpdateWinCount(npcID, newValue, onComplete, onFail);
            }
            , onFail
        ); 
    }
    
    private static void RequestUpdateWinCount(string npcID, int newValue, UnityAction onComplete, UnityAction<string> onFail)
    {
        try
        {
            PlayerDataKey key = PlayerDataKey.NPCWinCount1;

            switch (npcID)
            {
                case "NPC0001": key = PlayerDataKey.NPCWinCount1; break;
                case "NPC0002": key = PlayerDataKey.NPCWinCount2; break;
                case "NPC0003": key = PlayerDataKey.NPCWinCount3; break;
                case "NPC0004": key = PlayerDataKey.NPCWinCount4; break;
                case "NPC0005": key = PlayerDataKey.NPCWinCount5; break;
                case "NPC0006": key = PlayerDataKey.NPCWinCount6; break;
                
                default:
                { 
                    if (onFail != null)
                    {
                        onFail.Invoke("RequestUpdateWinCount failed. invalid npcID.");
                    }
                    return;
                }
            }
        
            KOSServer.UpdatePlayerData(key, newValue);
                        
            switch (npcID)
            {
                case "NPC0001": _instance._data.NPCWinCount1 = newValue; break;
                case "NPC0002": _instance._data.NPCWinCount2 = newValue; break;
                case "NPC0003": _instance._data.NPCWinCount3 = newValue; break;
                case "NPC0004": _instance._data.NPCWinCount4 = newValue; break;
                case "NPC0005": _instance._data.NPCWinCount5 = newValue; break;
                case "NPC0006": _instance._data.NPCWinCount6 = newValue; break;
            }

            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }
        catch (System.Exception e)
        {
            if (onFail != null)
            {
                onFail.Invoke(e.Message);
            }
        }  
    }

    public static void RequestAddGold(int amount, WebRequest.OnRequestCompleted onComplete)
    { 
        string url = "https://us-central1-koscardgame.cloudfunctions.net/requestAddGold";
        url += "/" + PlayerSave.GetPlayerUid() + "/" + amount;
        
        ScreenDubugLog.Instance.Print("RequestAddSilver: " + url);
        
        WebRequestURL(url, onComplete);  
    }

    public static void RequestUseGold(int amount, WebRequest.OnRequestCompleted onComplete)
    {
        string url = "https://us-central1-koscardgame.cloudfunctions.net/requestUseGold";
        url += "/" + PlayerSave.GetPlayerUid() + "/" + amount;
        
        ScreenDubugLog.Instance.Print("RequestUseSilver: " + url);
        
        WebRequestURL(url, onComplete);   
    }    
    #endregion
}
