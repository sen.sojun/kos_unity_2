﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using Firebase.Auth;

public class UserDBData {

	public string uid;
	public string UserName;
	public string Useremail;
	public string Userphoto;
    public string FirstFlow;
	public int  Userlevel;
	public int Userexp;
	public int UsersilverCoin;
	public int UsergoldCoin;
    public int UserDiamond;
    public int UserSoloWinCount;
    public int UserSoloDrawCount;
    public int UserSoloLoseCount;
    public int NPCWinCount1;
    public int NPCWinCount2;
    public int NPCWinCount3;
    public int NPCWinCount4;
    public int NPCWinCount5;
    public int NPCWinCount6;

	#region Constructors
	public UserDBData()
	{
		uid = "";
		UserName = "";
		Useremail = "";
		Userphoto = "";
        FirstFlow = "";
		Userlevel = 0;
		Userexp = 0;
		UsersilverCoin = 0;
		UsergoldCoin = 0;
        UserDiamond = 0;
        UserSoloWinCount = 0;
        UserSoloDrawCount = 0;
        UserSoloLoseCount = 0;
        NPCWinCount1 = 0;
        NPCWinCount2 = 0;
        NPCWinCount3 = 0;
        NPCWinCount4 = 0;
        NPCWinCount5 = 0;
        NPCWinCount6 = 0;
	}

    public UserDBData(string uId,
        string userName,
        string userMail,
        string userPhoto,
        string firstFlow,
        int userLevel,
        int userExp,
        int userSilverCoin,
        int userGoldCoin,
        int userDiamond,
        int userSoloWinCount,
        int userSoloDrawCount,
        int userSoloLoseCount,
        int NPCWin1,
        int NPCWin2,
        int NPCWin3,
        int NPCWin4,
        int NPCWin5,
        int NPCWin6
    )
	{
		uid = uId;
		UserName = userName;
		Useremail = userMail;
		Userphoto = userPhoto;
		Userlevel = userLevel;
        FirstFlow = firstFlow;
		Userexp = userExp;
		UsersilverCoin = userSilverCoin;
		UsergoldCoin = userGoldCoin;
        UserDiamond = userDiamond;
        UserSoloWinCount = userSoloWinCount;
        UserSoloDrawCount = userSoloDrawCount;
        UserSoloLoseCount = userSoloLoseCount;
        NPCWinCount1 = NPCWin1;
        NPCWinCount2 = NPCWin2;
        NPCWinCount3 = NPCWin3;
        NPCWinCount4 = NPCWin4;
        NPCWinCount5 = NPCWin5;
        NPCWinCount6 = NPCWin6;

	}

    public Dictionary<string, object> ToUserProfileDictionary()
    {
        Dictionary<string, object> result = new Dictionary<string, object>();
        result["uid"] = uid;
        result["UserName"] = UserName;
        result["Useremail"] = Useremail;
        result["Userphoto"] = Userphoto;
        result["Userlevel"] = Userlevel;
        result["FirstFlow"] = FirstFlow;
        result["Userexp"] = Userexp;
        result["UsersilverCoin"] = UsersilverCoin;
        result["UsergoldCoin"] = UsergoldCoin;
        result["UserDiamond"] = UserDiamond;
        result["UserSoloWinCount"] = UserSoloWinCount;
        result["UserSoloDrawCount"] = UserSoloDrawCount;
        result["UserSoloLoseCount"] = UserSoloLoseCount;
        result["NPC0001"] = NPCWinCount1;
        result["NPC0002"] = NPCWinCount2;
        result["NPC0003"] = NPCWinCount3;
        result["NPC0004"] = NPCWinCount4;
        result["NPC0005"] = NPCWinCount5;
        result["NPC0006"] = NPCWinCount6;

        return result;
    }

    public Dictionary<string, object> ToSilverCoinDictionary()
    {
        Dictionary<string, object> result = new Dictionary<string, object>();
        result["uid"] = uid;
        result["UsersilverCoin"] = UsersilverCoin;
        return result;
    }

	#endregion

	#region Methods
	public string GetDebugPrintText()
	{
		string resultText;
		resultText = string.Format(
            "Userid: {0}\nUser Name: {1}\nUser Email:D {2}\nUser Photo: {3}\nUser Level: {4}\nUSer Exp: {5}\n" +
            "User Silver: {6}\nUser Gold: {7}\nUser Diamond: {8}\nUser SoloWinCount {9}\nUser SoloDrawCount {10}\nUser SoloLoseCount {11}\n" +
            "NPC0001Win Count: {12}\nNPC0001Win Count: {13}\nNPC0001Win Count: {14}\nNPC0001Win Count: {15}\nNPC0001Win Count: {16}\nNPC0001Win Count: {17}\n" +
            "FirstFlow: {18}\n"
			, uid 
			, UserName 
			, Useremail 
			, Userphoto 
			, Userlevel 
			, Userexp 
			, UsersilverCoin 
			, UsergoldCoin 
            , UserDiamond
            , UserSoloWinCount
            , UserSoloDrawCount
            , UserSoloLoseCount
            , NPCWinCount1
            , NPCWinCount2
            , NPCWinCount3
            , NPCWinCount4
            , NPCWinCount5
            , NPCWinCount6
            , FirstFlow

		);
		return (resultText);
	}


	public void DebugPrintData()
	{
		Debug.Log(GetDebugPrintText());
	}


	#endregion
}

public class UserDB
{
	#region Enums
	private enum UserDBIndex //: int
	{
		uid = 0
		, UserName 
		, Useremail 
		, Userphoto 
        , FirstFlow
		, Userlevel 
		, Userexp 
		, UsersilverCoin 
		, UsergoldCoin 
        , UserDiamond
        , UserSoloWinCount
        , UserSoloDrawCount
        , UserSoloLoseCount
        , NPCWinCount1
        , NPCWinCount2
        , NPCWinCount3
        , NPCWinCount4
        , NPCWinCount5
        , NPCWinCount6

	}
    private FirebaseAuth auth;
	#endregion

	#region Private Properties
	private static Dictionary<string, UserDBData> _cardJobDBList = null;
	private static bool _isLoadDB = false;
	#endregion

	#region Constructors
	static UserDB()
	{
	}
	#endregion

	#region Methods
    /*
	public void writeNewUser(string userId, 
		string name, 
		string email,
		string photourl,
        string FirstFlow,
		int level,
		int exp,
		int silvercoin,
		int goldcoin,
        int diamond,
        int soloWinCount,
        int soloDrawCount,
        int soloLoseCount,
        int NPCWCount1,
        int NPCWCount2,
        int NPCWCount3,
        int NPCWCount4,
        int NPCWCount5,
        int NPCWCount6
    )
	{
		UserDBData user = new UserDBData(userId,name, email,photourl,FirstFlow, level,exp,silvercoin,goldcoin,
                                         diamond,soloWinCount,soloDrawCount,soloLoseCount,NPCWCount1,
                                         NPCWCount2,NPCWCount3,NPCWCount4,NPCWCount5,NPCWCount6);

		KOSServer.WriteUserData(user);
	}
    */

    public void authUser()
    {
        try{
            Debug.Log("0");
            FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://koscardgame.firebaseio.com/");
            Debug.Log("1");
        Firebase.Auth.FirebaseUser user = auth.CurrentUser;
            Debug.Log("2");

        user.TokenAsync(true).ContinueWith(task => {
            if (task.IsCanceled)
            {
                Debug.LogError("TokenAsync was canceled.");
                return;
            }

            if (task.IsFaulted)
            {
                Debug.LogError("TokenAsync encountered an error: " + task.Exception);
                return;
            }

                Debug.Log("before id");
            string idToken = task.Result;
            Debug.Log("id = "+ idToken);
            // Send token to your backend via HTTPS
            // ...
        });
        }
        catch (System.Exception e)
        {
            Debug.Log(e.ToString());
        }
    }

	public void readUser()
	{
        try
        {
        string clanNameThai = "";
		FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://koscardgame.firebaseio.com/");

		// Get the root reference location of the database.
		DatabaseReference reference = FirebaseDatabase.DefaultInstance.RootReference;
       
        InitializeFirebase();
		FirebaseDatabase.DefaultInstance.GetReference("koscardgame/player")
			.GetValueAsync().ContinueWith(task => 
            {
                Debug.Log("start...");
				if (task.IsFaulted)
                {
					Debug.Log("Cant read user player db");
				}
				else if (task.IsCompleted) 
               {
                Dictionary<string, object> results = (Dictionary<string, object>) task.Result.Value;
                clanNameThai = results["0"].ToString();
                    //DataSnapshot snapshot = task.Result;
                    //if (snapshot != null && snapshot.ChildrenCount > 0) {

                    //foreach (var childSnapshot in snapshot.Children) {

                    //	string name = snapshot.Child ("ClanNameThai").Value.ToString (); 

                    //	//text.text = name.ToString();
                    //	Debug.Log(name.ToString());
                    //	//text.text = childSnapshot.ToString();
                    Debug.Log("result = " + clanNameThai);
						//}

				}
				
                Debug.Log("stop....");
			});
        }
        catch (System.Exception e){
            Debug.Log(e.ToString());
        }
	}

    void InitializeFirebase()
    {
        
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        auth.StateChanged += AuthStateChanged;
        AuthStateChanged(this, null);
    }

    void AuthStateChanged(object sender, System.EventArgs eventArgs)
    {
              //FirebaseUser user = PlayerPrefs.GetString ("LoginUser");
        
              //if (auth.CurrentUser != user) {
                  //bool signedIn = user != auth.CurrentUser && auth.CurrentUser != null;
                  //if (!signedIn && user != null) {
                  //    Debug.Log("Signed out " + user.UserId);
                  //}
                  //user = auth.CurrentUser;
                  //if (signedIn) {
                  //    Debug.Log("Signed in " + user.UserId);
                  //    displayName = user.DisplayName ??   "";
                  //    emailAddress = user.Email ??   "";
                  //    photoUrl = user.PhotoUrl ??  "";
                  //    Debug.Log ("name = " + displayName + " email " + emailAddress + " photourl " + photoUrl);
                  //}
              //}
    }

	#endregion
}
