﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Auth;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class UserDataAdjust : MonoBehaviour
{

    //UI
    public InputField AddSilverAmt, MinusSilverAmt;
    public Button AddSilverButton;
    public Button MinusSilverButton;
    public Button AddGoldButton;
    public Button MinusGoldButton;
    public Text ErrorText;
    public Text ResultText;

    UserDBData data = new UserDBData();
    int silverCoin = 0;
    // Use this for initialization
    void Start()
    {
        RequestCacheData();

        AddSilverButton.onClick.AddListener(() => AddSilverCoin(1));
        MinusSilverButton.onClick.AddListener(() => MinusSilverCoin(1));
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void AddSilverCoin(int amtCoin)
    {
        ResultText.text = string.Format("coin = {0}", data.UsersilverCoin.ToString());
        silverCoin += amtCoin;
        AddSilverAmt.text = silverCoin.ToString();
        
        KOSServer.RequestAddSilver(
              amtCoin
            , delegate() 
            {
                Debug.LogFormat("AddSilverCoin Completed");
            }
            , delegate(string errorMsg)
            { 
                Debug.LogFormat("AddSilverCoin Failed");
            }
	    );
    }
    
    public void MinusSilverCoin(int amtCoin)
    {
        ResultText.text = string.Format("coin = {0}", data.UsersilverCoin.ToString());
        silverCoin -= amtCoin;
        AddSilverAmt.text = silverCoin.ToString();
        
        KOSServer.RequestUseSilver(
              amtCoin
            , delegate() 
            {
                Debug.LogFormat("MinusSilverCoin Completed");
            }
            , delegate(string errorMsg)
            { 
                Debug.LogFormat("MinusSilverCoin Failed");
            }
        );
    }

    public void RequestCacheData()
    {
        ResultText.text = "Start Cache data...";

        KOSServer.Instance.CacheData(this.OnCacheCompleted, this.OnCacheFailed);
    }

    public void OnCacheCompleted()
    {
        ErrorText.text = "Cache Successful.";
        string text = "";
        try
        {
            data = KOSServer.Instance.PlayerData;

            text = string.Format("Cache Successful.\nUID : {0}\nSilver Coin : {1}\njson : {2}",
                data.uid,
                data.UsersilverCoin,
                data.UserName
            );
            ResultText.text = text;
            AddSilverAmt.text = data.UsersilverCoin.ToString();
            silverCoin = data.UsersilverCoin;
        }
        catch (System.Exception e)
        {
            ResultText.text = e.Message.ToString();
        }
    }

    public void OnCacheFailed(string errorMessage)
    {
        ResultText.text = errorMessage;
    }
}
