﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using Firebase.Auth;

public class StashFirebaseData
{
    public string UID;
    public Dictionary<string, int> Card;

    #region Constructors
    public StashFirebaseData()
    {
        UID = "";
        Card = new Dictionary<string, int>();
    }

    public StashFirebaseData(string uid, Dictionary<string, int> card)
    {
        UID = uid;
        Card = new Dictionary<string, int>(card);
    }
    #endregion

    #region Methods
    public string GetDebugPrintText()
    {
        string resultText = string.Format("Userid: {0}\n", UID);

        foreach (KeyValuePair<string, int> v in Card)
        {
            resultText += string.Format(
                "ID: {0} : {1}, "
                , v.Key
                , v.Value
            );
        }
        return (resultText);
    }

    public void DebugPrintData()
    {
        Debug.Log(GetDebugPrintText());
    }
    #endregion
}
