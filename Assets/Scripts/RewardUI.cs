﻿using UnityEngine;
using System.Collections;

public class RewardUI : MonoBehaviour 
{
	#region Delagates
	public delegate void OnShowFinish();
	#endregion

	public FullCardUI FullCardUI;
	public UILabel CardLabel;

	public UIButton MainMenuButton;
	public UIButton RetryButton;

	public float ShowTime = 1.5f;

	private OnShowFinish _onShowFinish = null;

	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
	}

	public void ShowUI(PlayerCardData rewardCard)
	{
		string rareColorCode = "[FFFFFF]";

		if(rewardCard.SymbolData == CardSymbolData.CardSymbol.Mass)
		{
			rareColorCode = "[B78968]";
		}
		else if(rewardCard.SymbolData == CardSymbolData.CardSymbol.Common)
		{
			rareColorCode = "[949494]";
		}
		else if(rewardCard.SymbolData == CardSymbolData.CardSymbol.Uncommon)
		{
			rareColorCode = "[B5E8DF]";
		}
		else if(rewardCard.SymbolData == CardSymbolData.CardSymbol.Rare || rewardCard.SymbolData == CardSymbolData.CardSymbol.SuperRare)
		{
			rareColorCode = "[FAF0E6]";
		}
		else if(rewardCard.SymbolData == CardSymbolData.CardSymbol.Forbidden)
		{
			rareColorCode = "[EC1F02]";
		}

        string cardName = "";
        switch (LocalizationManager.CurrentLanguage)
        {
            case LocalizationManager.Language.Thai:
            {
                cardName = rewardCard.Name_TH;
                if (rewardCard.Alias_TH.Length > 0)
                {
                    cardName += ", " + rewardCard.Alias_TH;
                }
            }
            break;

            case LocalizationManager.Language.English:
            default:
            {
                cardName = rewardCard.Name_EN;
                if (rewardCard.Alias_EN.Length > 0)
                {
                    cardName += ", " + rewardCard.Alias_EN;
                }
            }
            break;
        }

        string cardSymbol = "";
        CardSymbolData symbolData;
        if (rewardCard.SymbolData == CardSymbolData.CardSymbol.SuperRare)
        {
            symbolData = new CardSymbolData(CardSymbolData.EnumCardSymbolToID(CardSymbolData.CardSymbol.Rare));
        }
        else
        {
            symbolData = rewardCard.SymbolData;
        }

        switch (LocalizationManager.CurrentLanguage)
        {
            case LocalizationManager.Language.Thai:
            {
                cardSymbol = symbolData.SymbolName_TH;
            }
            break;

            case LocalizationManager.Language.English:
            default:
            {
                cardSymbol = symbolData.SymbolName_EN;
            }
            break;
        }

        FullCardUI.SetCardData(rewardCard);
		CardLabel.text = string.Format("{0}\n\"{1}\"\n({2})"
			, Localization.Get("BATTLE_REWARD_GET_A_CARD")
			, cardName
			, rareColorCode + cardSymbol + "[-]"
		); 

		ShowButton(false);
		gameObject.SetActive(true);
		BattleUIManager.RequestBringForward(gameObject);

		StartCoroutine(WaitShowFinish(ShowTime));
	}

	private IEnumerator WaitShowFinish(float waitTime)
	{
		yield return new WaitForSeconds(waitTime);

		ShowButton(true);
	}

	public void ShowButton(bool isShow)
	{
		MainMenuButton.gameObject.SetActive(isShow);
		RetryButton.gameObject.SetActive(isShow);
	}
}
