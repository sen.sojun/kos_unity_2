﻿using UnityEngine;
using System.Collections;

public class ResultUI : MonoBehaviour 
{
	#region Delagates
	public delegate void OnShowFinish();
	#endregion

	public UILabel ResultLabel;
	public UILabel WinLabel;

	public UIButton MainMenuButton;
	public UIButton RetryButton;

	public GameObject WinFrame;

	public float ShowTime = 1.5f;

	private OnShowFinish _onShowFinish = null;

	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
	}

	public void ShowUI(GameResultUI.GameResultType gameResult, int rewardSilverCoin, int winCount, OnShowFinish onShowFinish)
	{
		_onShowFinish = onShowFinish;

		switch(gameResult)
		{
			case GameResultUI.GameResultType.Lose: 
			{ 
				ResultLabel.text = Localization.Get("BATTLE_RESULT_LOSE"); 

				WinFrame.SetActive(true);

				{
					string text = string.Format (Localization.Get ("BATTLE_REWARD_GET_SILVER")
						, rewardSilverCoin.ToString ()
					);

					WinLabel.text = text;
				}
			}
			break;

			case GameResultUI.GameResultType.Draw: 
			{ 
				ResultLabel.text = Localization.Get("BATTLE_RESULT_DRAW"); 

				WinFrame.SetActive(false);
				//WinLabel.text = "";
			}
			break;

			case GameResultUI.GameResultType.Win: 
			{ 
				ResultLabel.text = Localization.Get("BATTLE_RESULT_WIN"); 

				WinFrame.SetActive(true);

				WinLabel.text = string.Format(Localization.Get("BATTLE_RESULT_WIN_COUNT"), winCount.ToString());
				if(winCount % 5 > 0)
				{
					int remainWin = 5 - (winCount % 5);
					{
						string text = string.Format (Localization.Get ("BATTLE_RESULT_WIN_DETAIL")
							, remainWin.ToString ()
							, ((remainWin > 1) ? "s" : "")
						);

						WinLabel.text += "\n" + text;
					}
					{
						string text = string.Format (Localization.Get ("BATTLE_REWARD_GET_SILVER")
							, rewardSilverCoin.ToString ()
						);

						WinLabel.text += "\n" + text;
					}
				}
			}
			break;
		}

		ShowButton(false);
		NGUITools.SetActive(gameObject, true);
		BattleUIManager.RequestBringForward(gameObject);

		StartCoroutine(WaitShowFinish(ShowTime));
	}

    public void ShowPVPUI(GameResultUI.GameResultType gameResult, int rewardSilverCoin, OnShowFinish onShowFinish)
    {
        _onShowFinish = onShowFinish;
                
        if(WinFrame != null) 
        {
            WinFrame.SetActive(true);
            {
                WinLabel.text = string.Format(
                      Localization.Get ("BATTLE_REWARD_GET_SILVER")
                    , rewardSilverCoin.ToString ()
                );
            }
        }
        
        switch(gameResult)
        {
            case GameResultUI.GameResultType.Lose: 
            { 
                ResultLabel.text = Localization.Get("BATTLE_RESULT_LOSE"); 
            }
            break;

            case GameResultUI.GameResultType.Draw: 
            { 
                ResultLabel.text = Localization.Get("BATTLE_RESULT_DRAW"); 
            }
            break;

            case GameResultUI.GameResultType.Win: 
            { 
                ResultLabel.text = Localization.Get("BATTLE_RESULT_WIN"); 
            }
            break;
        }

        ShowButton(true);
        NGUITools.SetActive(gameObject, true);
        BattleUIManager.RequestBringForward(gameObject);
        
        //StartCoroutine(WaitShowFinish(ShowTime));
    }

	private IEnumerator WaitShowFinish(float waitTime)
	{
		yield return new WaitForSeconds(waitTime);

		ShowFinish();
	}

	private void ShowFinish()
	{
		if(_onShowFinish != null)
		{
			_onShowFinish.Invoke();
		}	
	}

	public void HideUI()
	{
		NGUITools.SetActive(gameObject, false);
	}

	public void ShowButton(bool isShow)
	{
		MainMenuButton.gameObject.SetActive(isShow);
        
        if(RetryButton != null)
        {
		    RetryButton.gameObject.SetActive(isShow);
        }
	}
}