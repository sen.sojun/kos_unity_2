﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SoloLeagueManager : MonoBehaviour {

	public SoloLeagueDetailUI SoloLeageDetailUI;
	public SoloLeagueLoadingUI SoloLeagueLoadingUI;
	//public SoloLeageResultUI ResultUI;
	public SoloLeagueRewardUI SoloLeagueRewardUI;
	public SoloLeagueRewardInfoUI SoloLeagueRewardInfoUI;
	public SoloLeagueRewardConfirmUI SoloLeagueRewardConfirmUI;
	public SoloLeaguePopUpUI SoloLeaguePopUpUI;
	public SoloLeagueRewardPackResultUI SoloLeagueRewardPackResultUI;
	public SoloLeagueRewardPreviewCardUI SoloLeagueRewardPreviewCardUI;
    public PopupBoxUI popupBoxUI;

	#region Private Properties
	private Dictionary<string, ShopPackData> _shopList;
	private List<SoloLeagueRewardDBData> _dataList = null;
	//private List<PlayerLevelDBData> _pleyerLevelDBList = null;
	private int _currentIndex = 159;

	private int _playerLevel = 0;
	private int _enemyLevel = 0;
	private int _playerExp = 0;
	private int _playerDiamond = 0;
	private int _showRewardIndex = 0;
	private bool _isEnoughToGetReward = false; //

	private const int iBase = 1;
	private const int iUnique = 1;
    private const int iResource = 12; //10;
	private const int iUnit = 20;
	private const int iEventStructure = 8;

	private int _iFromLevel = 0;
	private int _iFromDiamond = 0;
	private int _iCostLevel = 0;
	private int _iCostDiamond = 0;
	//private int _iCurrExp = 0;
	private int _iNewExp = 0;

    private string _sAmtToShow = "";

    private string Tutorial1 = "T0040";   //Battle arena msg
    //private string Tutorial2 = "T0040_1"; //Level and exp msg
    //private string Tutorial3 = "T0040_1"; //reward msg 
    //for tutorial database
    private List<TutorialData> _tutorialDataList = null;
    public bool ForcePlayFirstTime = false;
	#endregion


	// Use this for initialization
	void Start () 
	{
       
	}
	

	// Update is called once per frame
	void Update () {
		
	}

    public void ShowBattleArenaTutorial()
    {
        if (ForcePlayFirstTime || PlayerSave.IsPlayerFirstTime())
        {
            // Load first time tutorial
            _tutorialDataList = new List<TutorialData>();

            // Load Tutorial1
            _tutorialDataList.AddRange(LoadFirstFlowTutorial(Tutorial1, true));

            // Load Tutorial2
            //_tutorialDataList.AddRange(LoadFirstFlowTutorial(Tutorial2, true));

            // Load Tutorial3
            //_tutorialDataList.AddRange(LoadFirstFlowTutorial(Tutorial3, true));
                
            ShowPopupUI(0);
        }
    }

    public List<TutorialData> LoadFirstFlowTutorial(string tutorialCode, bool isFirstFlow = true)
    {
        // Load Tutorial1
        List<TutorialData> tutorialList = new List<TutorialData>();

        List<TutorialDBData> dataList;
        bool _isSuccess = TutorialDB.GetDataByCode(tutorialCode, out dataList);
        if (_isSuccess)
        {
            List<TutorialDBData> showList = new List<TutorialDBData>();

            // Filter not show in first flow.
            foreach (TutorialDBData data in dataList)
            {
                if (isFirstFlow)
                {
                    if (data.ShowInFirstFlow > 0)
                    {
                        showList.Add(data);
                    }
                }
                else
                {
                    showList.Add(data);
                }
            }

            int page = 0;
            foreach (TutorialDBData data in showList)
            {
                TutorialData tutorialData = new TutorialData(data, (++page).ToString() + "/" + showList.Count.ToString());
                tutorialList.Add(tutorialData);
            }
        }

        return tutorialList;
    }


    #region Popup UI Methods
    public void ShowPopupUI(int index)
    {
        TutorialData data = _tutorialDataList[index++];

        string title = "";
        string message = "";
        string buttonText = "";
        if (LocalizationManager.IsCurrentLanguage(LocalizationManager.Language.Thai))
        {
            title = data.TitleTh;
            message = data.TextTh;
        }
        else
        {
            title = data.TitleEng;
            message = data.TextEng;
        }

        buttonText = Localization.Get(data.ButtonKey);

        if (string.Compare(data.DisplayMode, "DIALOG") == 0)
        {
            popupBoxUI.ShowMessageUI(
                  message
                , buttonText
                , () =>
                {
                    if (index < _tutorialDataList.Count)
                    {
                        ShowPopupUI(index);
                    }
                    else
                    {
                        this.ClosePopUpUI();
                    }
                }
                , false
            );
        }
        else
        {
            popupBoxUI.ShowImageUI(
                  data.ImagePath
                , title
                , data.PageText
                , message
                , buttonText
                , () =>
                {
                    if (index < _tutorialDataList.Count)
                    {
                        ShowPopupUI(index);
                    }
                    else
                    {
                        this.ClosePopUpUI();
                    }
                }
                , false
            );
        }

        popupBoxUI.gameObject.SetActive(true);
        BattleUIManager.RequestBringForward(popupBoxUI.gameObject);
    }

    public void ClosePopUpUI()
    {
        popupBoxUI.gameObject.SetActive(false);
    }
    #endregion

	public void LoadData()
	{
        
		SoloLeagueRewardDB.GetAllData (out _dataList);
	}

	private string GetNPCID()
	{
		string npcID = "";

		_currentIndex = 1 + Random.Range (0, 100);
		npcID = "N" + _currentIndex.ToString ("0000"); 

		return npcID;
	}

	public int getNpcLevel(int playerLevel)
	{
		int npcLevel = 0;
		if (playerLevel <= 3) 
		{
			//+- 3
			npcLevel = playerLevel + Random.Range (0, 3);
		}
		else if (playerLevel < 10) 
		{
			//+- 3
			npcLevel = playerLevel + Random.Range (-3, 4) ;
		}
		else
		{
		    //+-5
			npcLevel = playerLevel + Random.Range (-5, 6) ;
		}

		return npcLevel;

	}

	public List<string> RandomDeck()
	{
		List<string> resultList = new List<string> ();

		List<CardDBData> cardList;
		CardDB.GatAllData (out cardList);

		// Base Random\
		List<CardDBData> baseList = new List<CardDBData>();
		foreach (CardDBData data in cardList)
		{
			if (string.Compare (CardTypeData.EnumCardTypeToID (CardTypeData.CardType.Base), data.TypeID) == 0)
			{
				if ((string.Compare ("C0045", data.CardID) != 0)
					&& (string.Compare("C0074",data.CardID) != 0)
                    && (string.Compare(("C0099"),data.CardID) != 0)
				    && (string.Compare ("C0050", data.CardID) != 0))
				{
					baseList.Add (data);
				}
			}
		}	
		for (int i = 0; i < iBase; ++i)
		{	
			resultList.Add (baseList [Random.Range (0, baseList.Count)].CardID);
		}

		// Unique Random
		List<CardDBData> uniqueList = new List<CardDBData>();
		foreach (CardDBData data in cardList)
		{
			foreach(string subTypeID in data.SubTypeList)
			{
                if (string.Compare(CardSubTypeData.EnumCardSubTypeToID(CardSubTypeData.CardSubType.Unique), subTypeID) == 0)
                {
                    if ((string.Compare("C0058", data.CardID) != 0)
                    && (string.Compare("C0083", data.CardID) != 0)
                    && (string.Compare("C0084", data.CardID) != 0)
                    && (string.Compare("C0085", data.CardID) != 0)
                    && (string.Compare("C0088", data.CardID) != 0)
                    && (string.Compare("C0095", data.CardID) != 0)
                    && (string.Compare("C0096", data.CardID) != 0)
                    && (string.Compare("C0097", data.CardID) != 0)
                    && (string.Compare("C0098", data.CardID) != 0))
                    {
                        uniqueList.Add(data);
                        break;
                    }
                }

			}
		}	
		for (int i = 0; i < iUnique; ++i) 
		{	
			resultList.Add (uniqueList [Random.Range (0, uniqueList.Count)].CardID);
		}

		// Resource
		List<CardDBData> resourceList = new List<CardDBData>();
		foreach (CardDBData data in cardList)
		{
			foreach(string subTypeID in data.SubTypeList)
			{
				if (string.Compare (CardSubTypeData.EnumCardSubTypeToID (CardSubTypeData.CardSubType.Resource ), subTypeID) == 0)
				{
					resourceList.Add (data);
					break;
				}
			}
		}	

		for (int i = 0; i < iResource; ++i) 
		{
			resultList.Add (resourceList [Random.Range (0, resourceList.Count)].CardID);
		}

        //start of insertion vit 07112017
        //fill the rest
        List<CardDBData> unitstruceventList = new List<CardDBData>();

        List<CardDBData> unitList          = new List<CardDBData>();
        List<CardDBData> structureList     = new List<CardDBData>();
        List<CardDBData> eventList          = new List<CardDBData>();


        foreach (CardDBData data in cardList)
        {
            PlayerCardData card = new PlayerCardData(data.CardID);

            if (!card.IsCardSubType(CardSubTypeData.CardSubType.Resource))
            {
                if (card.IsCardSymbol(CardSymbolData.CardSymbol.Mass))
                {
                    
                    for (int i = 1; i <= 10; ++i)
                    {
                        
                        if (card.IsCardType(CardTypeData.CardType.Unit))
                        {
                            unitList.Add(data);
                        }
                        else if (card.IsCardType(CardTypeData.CardType.Structure))
                        {
                            structureList.Add(data);
                        }
                        else if (card.IsCardType(CardTypeData.CardType.Event))
                        {
                            eventList.Add(data);
                        }
                        /////
                        /// else technic card
                        //// 

                        
              
                    }

                }
                else
                {
                    for (int i = 1; i <= 3; ++i)
                    {
                        
                        if (card.IsCardType(CardTypeData.CardType.Unit))
                        {
                            if ( !card.IsCardID("C0058")
                                && !card.IsCardID("C0083")
                                && !card.IsCardID("C0084")
                                && !card.IsCardID("C0085")
                                && !card.IsCardID("C0088")
                                && !card.IsCardID("C0095")
                                && !card.IsCardID("C0096")
                                && !card.IsCardID("C0097")
                                    && !card.IsCardID("C0098"))
                            {
                                unitList.Add(data);   
                            }

                        }
                        else if (card.IsCardType(CardTypeData.CardType.Structure))
                        {
                            structureList.Add(data);
                        }
                        else if (card.IsCardType(CardTypeData.CardType.Event))
                        {
                            eventList.Add(data);
                        }
                        /////
                        /// else technic card
                        ////

                    }
                }

            }
           
        }

        //random 
        //ratio 90% = unit
        //91-95     = structure
        //96-100    = event
        int iMax = 40 - resultList.Count;
        for (int i = 1; i <= iMax; ++i)
        {
            float rnd = Random.Range(1.0f, 100.0f);
            if (rnd <= 90)
            {
                // add unit
                //Debug.Log("unit count = " + unitList.Count);
                int index = Random.Range(0, unitList.Count);
                resultList.Add(unitList[index].CardID);
                unitList.RemoveAt(index);


            }
            else if ((rnd > 90) && (rnd <= 95))
            {
                // structure
                // add unit
                //Debug.Log("struct count = " + structureList.Count);
                int index = Random.Range(0, structureList.Count);
                resultList.Add(structureList[index].CardID);
                structureList.RemoveAt(index);
            }
            else 
            {
                // event
                // add unit
                int index = Random.Range(0, unitList.Count);
                resultList.Add(unitList[index].CardID);
                unitList.RemoveAt(index);
            }


        }



        //end of insertion vit 07112017


        //// start of deletion vit 07112017
        //// Unit
        //List<CardDBData> unitList = new List<CardDBData>();
        //foreach (CardDBData data in cardList)
        //{
        //	if (string.Compare (CardTypeData.EnumCardTypeToID (CardTypeData.CardType.Unit), data.TypeID) == 0)
        //	{
        //			unitList.Add (data);
        //	}
        //}	
        //for (int i = 0; i < iUnit; ++i)
        //{	
        //	resultList.Add (unitList [Random.Range (0, unitList.Count)].CardID);
        //}

        //// Event & Structure
        //List<CardDBData> eventStructureList = new List<CardDBData>();
        //foreach (CardDBData data in cardList)
        //{
        //	if (
        //		   (string.Compare (CardTypeData.EnumCardTypeToID (CardTypeData.CardType.Event), data.TypeID) == 0)
        //		|| (string.Compare (CardTypeData.EnumCardTypeToID (CardTypeData.CardType.Structure), data.TypeID) == 0)
        //	)

        //	{
        //		eventStructureList.Add (data);
        //	}
        //}	
        //for (int i = 0; i < iUnit; ++i)
        //{	
        //	resultList.Add (eventStructureList [Random.Range (0, eventStructureList.Count)].CardID);

        //}
        /////end of deletion vit 07112017



		return resultList;
	}

	public void OnCLickBattle()
	{
		MatchMakingManager matchMaking = GameObject.FindObjectOfType<MatchMakingManager> ();

		bool isSuccess = matchMaking.CreateSoloLeagueData (
			  RandomDeck()
			, GetNPCID()
			, _enemyLevel
		);

		if (isSuccess) 
		{
			PlayerSave.SavePlayFromSoloLeague (true); //indicate that from sololeage
		}
		MatchingData matchingData = GameObject.FindObjectOfType<MatchingData> ();
        
        KOSNetwork.NetworkSetting.IsNetworkEnabled = false;
		LoadScene ("NetworkBattleScene", matchingData);
	}
		
	public void ShowDetail()
	{
		SoloLeagueLoadingUI.HideUI ();
		SoloLeagueRewardUI.HideUI ();

        ForcePlayFirstTime = true;
        ShowBattleArenaTutorial();
        ForcePlayFirstTime = false;

		///Load PlayerSave Exp and detail
		_playerExp = PlayerSave.GetPlayerExp();
		// _playerExp = 100138;
		_playerDiamond = PlayerSave.GetPlayerDiamond ();
		_playerLevel = PlayerLevelDB.GetLevel (_playerExp);
		_enemyLevel = getNpcLevel (_playerLevel);

		Debug.Log (string.Format("Player LV: {0}\nEnemy LV: {1}", _playerLevel, _enemyLevel));

		SoloLeageDetailUI.ShowUI (_playerLevel, _playerExp, _playerDiamond);
	}
		
	public void HideSoloLeague()
	{
		SoloLeageDetailUI.HideUI ();
		SoloLeagueLoadingUI.HideUI ();
		SoloLeagueRewardUI.HideUI ();
	}
		
	public void ShowLoading(MatchingData matchingData)
	{
		SoloLeagueLoadingUI.ShowUI(matchingData);
		SoloLeageDetailUI.HideUI ();
		SoloLeagueRewardUI.HideUI ();
	}

	public void LoadScene(string sceneName, MatchingData matchingData)
	{
		StartCoroutine(LoadingScene(sceneName, matchingData));
	}

	IEnumerator LoadingScene(string sceneName, MatchingData matchingData)
	{
		ShowLoading(matchingData);
		yield return new WaitForSeconds(3.0f);
		SceneManager.LoadSceneAsync(sceneName);
	}

	public void ShowReward()
	{
		LoadData ();

		SoloLeageDetailUI.HideUI ();
		SoloLeagueLoadingUI.HideUI ();

		SoloLeagueRewardPackResultUI.HideUI ();

		SoloLeagueRewardUI.ShowUI (
			  PlayerSave.GetPlayerLevel()
			, PlayerSave.GetPlayerDiamond()
			, _dataList
		);

	}

	public void OnClickReward(int i)
	{
		i -= 1;
		_showRewardIndex = i;
		SoloLeageDetailUI.HideUI ();
		SoloLeagueLoadingUI.HideUI ();
		//SoloLeagueRewardUI.HideUI ();

		SoloLeagueRewardInfoUI.ShowUI(_dataList[i]);
	}


	public void OnClickConfirmReward()
	{
        bool isOK = false;

		SoloLeageDetailUI.HideUI ();
		SoloLeagueLoadingUI.HideUI ();
		//SoloLeagueRewardUI.HideUI ();

//		SoloLeagueRewardConfirmUI.ShowUI (_dataList[_showRewardIndex]);

		_iFromLevel = PlayerSave.GetPlayerLevel();
		_iFromDiamond = PlayerSave.GetPlayerDiamond();
		_iCostLevel = _iFromLevel - _dataList [_showRewardIndex].LevelUse;
		_iCostDiamond = _iFromDiamond - _dataList [_showRewardIndex].DiamondUse;

		Debug.Log (_iFromLevel + " + " + _iCostLevel + " + " + _iFromDiamond + " + " + _iCostDiamond);
        isOK = calculate_reward_cost(_iCostLevel, _iCostDiamond);

        if (isOK)
        {
            SoloLeagueRewardConfirmUI.ShowUI2(
            _iFromLevel.ToString(),
            _iCostLevel.ToString(),
            _iFromDiamond.ToString(),
            _iCostDiamond.ToString()
           );  
        }
        else
        {
            SoloLeaguePopUpUI.ShowUI (
                "SOLOLEAGUE_REWARD_FAILED_TITLE",
                "SOLOLEAGUE_REWARD_FAILED_MSG",
                "SOLOLEAGUE_REWARD_FAILED_BUTTON",
                this.ShowReward
            );   
        }
		

	}

//	bool calculate_reward_cost(int costLevel,int costDiamond)
//	{
//		SoloLeagueRewardDBData data;
//		data = _dataList [_showRewardIndex];
//		//show from diamond
//		_iFromDiamond = PlayerSave.GetPlayerDiamond ();
//
//		//show from level
//		_iFromLevel = PlayerSave.GetPlayerLevel ();
//
//		if ( (_iFromLevel > data.LevelUse ) && 
//			(_iFromDiamond > data.DiamondUse ) )
//		{
//			//get destination diamond
//			_iCostDiamond = _iFromDiamond - data.DiamondUse;
//
//
//			//get destination level
//			_iCostLevel = _iFromLevel - data.LevelUse;
//			_isEnoughToGetReward = true;
//
//			Debug.Log ("_icostLeve = " + _iCostLevel);
//			Debug.Log ("_icostDiamon =  " + _iCostDiamond);
//			//PlayerSave.SavePlayerExp (_iCostLevel);
//			//PlayerSave.SavePlayerDiamond (_iCostDiamond);
//
//			return true;
//		}
//		else
//		{
//			_isEnoughToGetReward = false;
//			return false;
//		}
//
//	}


	bool calculate_reward_cost(int costLevel,int costDiamond)
	{
		int iFromLevel = 0;
		int iFromDiamond = 0;
		int iCostLevel = 0;
		int iCostDiamond = 0;
		int iCurrExp = 0;

		int iDestinationLevelExp = 0;

		int iNextLevel = 0;   //next level from now
		int iDestLevel = 0;   //new level after swap
		int iNextFromDestLevel = 0;

		SoloLeagueRewardDBData data;
		data = _dataList [_showRewardIndex];

		//show from diamond
		_iFromDiamond = PlayerSave.GetPlayerDiamond ();

		//show from level
		_iFromLevel = PlayerSave.GetPlayerLevel ();

		if ( (_iFromLevel > data.LevelUse ) && 
			(_iFromDiamond > data.DiamondUse ) )
		{
			//get destination diamond
			_iCostDiamond = _iFromDiamond - data.DiamondUse;


			//get destination level
			_iCostLevel = _iFromLevel - data.LevelUse;
			//get currexp
			iCurrExp = PlayerSave.GetPlayerExp ();
			Debug.Log ("_now player's exp = " + iCurrExp.ToString ());

			//show from level
			iFromLevel = PlayerSave.GetPlayerLevel ();

			iDestinationLevelExp = PlayerSave.GetPlayerExp ();

			//get destination level
			iCostLevel = iFromLevel - data.LevelUse;
			iNextLevel = iFromLevel + 1;
			iNextFromDestLevel = iCostLevel + 1;

			iDestinationLevelExp = getNewExp (iCurrExp,iFromLevel, iNextLevel, iCostLevel, iNextFromDestLevel);

//
//			LevelFromLabel.text = iFromLevel.ToString();
//			LevelUseLabel.text = "> " + iCostLevel.ToString();
//			DiamondFromLabel.text = iFromDiamond.ToString();
//			DiamondUseLabel.text = "> " + iCostDiamond.ToString();

			_isEnoughToGetReward = true;

			Debug.Log ("_icostLeve = " + _iCostLevel);
			Debug.Log ("_icostDiamon =  " + _iCostDiamond);
			Debug.Log ("_new exp = " + iDestinationLevelExp);
			//PlayerSave.SavePlayerExp (_iCostLevel);
			//PlayerSave.SavePlayerDiamond (_iCostDiamond);




			return true;
		}
		else
		{
			_isEnoughToGetReward = false;
			return false;
		}

	}

	public int getNewExp(int iCurrExp ,int iFromLevel,int iNextLevel,
		int iCostLevel,int iNextDestLevel)
	{
		int iCurrLevelExp = 0;
		int iNextLevelExp = 0;
		int iCostLevelExp = 0;
		int iNextDestLevelExp = 0;

		int iDeltaD = 0;
		int iSpaceFromNextToCurr = 0;

		float fPercent = 0;
		float fToAddNewExp = 0;


		iCurrLevelExp = PlayerSave.GetPlayerExpByLevel (iFromLevel);
		iNextLevelExp = PlayerSave.GetPlayerExpByLevel (iNextLevel);
		iCostLevelExp = PlayerSave.GetPlayerExpByLevel (iCostLevel);
		iNextDestLevelExp = PlayerSave.GetPlayerExpByLevel (iNextDestLevel);

		Debug.Log ("_curr level's exp = " + iCurrLevelExp);
		Debug.Log ("_next level's exp = " + iNextLevelExp);
		Debug.Log ("_cost level's exp = " + iCostLevelExp);
		Debug.Log ("_cost level's next exp = " + iNextDestLevelExp);

		iDeltaD = iCurrExp - iCurrLevelExp;
		iSpaceFromNextToCurr = iNextLevelExp - iCurrLevelExp;
		fPercent = (float)iDeltaD / (float)iSpaceFromNextToCurr;

		iSpaceFromNextToCurr = iNextDestLevelExp - iCostLevelExp;

		fToAddNewExp = iSpaceFromNextToCurr * fPercent;
		_iNewExp = Mathf.RoundToInt(fToAddNewExp) + iCostLevelExp;

//		Debug.Log ("_new exp after purchase = " + iNewExp);
//		Debug.Log ("_new diamond after purchase = " + _iCostDiamond);
		return _iNewExp;

	}



	//to open popup box that show user wheater can get reward or not.
	public void OnCheckConfirmReward()
	{
		SoloLeagueRewardConfirmUI.HideUI ();

		bool isOK = false;

		// check valid
		//send _dataList[_showRewardIndex]; to deck

		Debug.Log( "data = " + _dataList[_showRewardIndex].RewardKey);

		isOK = calculate_reward_cost (_iCostLevel, _iCostDiamond);

		Debug.Log ("_new exp after purchase = " + _iNewExp);
		Debug.Log ("_new diamond after purchase = " + _iCostDiamond);
		Debug.Log ("reward key = " + _dataList [_showRewardIndex].RewardType);

		if (isOK) 
		{
			if (_dataList [_showRewardIndex].RewardType == SoloLeagueRewardDBData.RewardTypeEnum.Card)
			{
                
                ////start of insertion vit 01112017
                /// 
                //update new exp,diamond
                PlayerSave.SavePlayerExp(_iNewExp);
                PlayerSave.SavePlayerDiamond(_iCostDiamond);
                _sAmtToShow = "1";

                // add reward to player save
                SetShowCardResultUI(true);
                PlayerCardData card = new PlayerCardData(new CardData(_dataList[_showRewardIndex].RewardKey));

                // add new card to stash
                bool isSuccess = AddCardToStash(card.PlayerCardID);
                if (isSuccess)
                {
                    bool isNew = false;
                    if (PlayerSave.SavePlayerCardCollection(card.PlayerCardID))
                    {
                        // New card
                        isNew = true;
                    }

                    bool isGlow = false;
                    if ((card.SymbolData == CardSymbolData.CardSymbol.Rare) ||
                        (card.SymbolData == CardSymbolData.CardSymbol.SuperRare) ||
                        (card.SymbolData == CardSymbolData.CardSymbol.Forbidden))
                    {
                        isGlow = true;
                    }


                    SoloLeagueRewardPackResultUI.ShowCard(2, new PlayerCardData(card), isGlow, isNew, this.OnClickedPreview);
                } 


                //AddCardToStash(_dataList[_showRewardIndex].RewardKey);


                ////end of insertion vit 01112017




                ////start of deletion vit 01112017

				//// add reward to player save
				//AddCardToStash (_dataList [_showRewardIndex].RewardKey);

				////update new exp,diamond
				//PlayerSave.SavePlayerExp(_iNewExp);
				//PlayerSave.SavePlayerDiamond (_iCostDiamond);


				//// show card in pack (in case buy pack)
				//SoloLeagueRewardInfoUI.HideUI ();

				//SoloLeaguePopUpUI.ShowUI 
				//("SOLOLEAGUE_REWARD_SUCCESS_TITLE",
				//	"SOLOLEAGUE_REWARD_SUCCESS_MSG",
				//	"SOLOLEAGUE_REWARD_SUCCESS_BUTTON",
				//	this.ShowReward
				//);

                /////end of deletion vit 01112017
			} 
			else if (_dataList [_showRewardIndex].RewardType == SoloLeagueRewardDBData.RewardTypeEnum.PackNormal)
			{
				_shopList = new Dictionary<string, ShopPackData> ();
				_shopList.Add (_dataList [_showRewardIndex].ShopID
					, new ShopPackData(_dataList [_showRewardIndex].ShopID));

				//update new exp,diamond
				PlayerSave.SavePlayerExp(_iNewExp);
				PlayerSave.SavePlayerDiamond (_iCostDiamond);

				List<string> resultCardList;
                _sAmtToShow = "5";
				RandomNormal("SP0001", out resultCardList);

			}
			else if (_dataList [_showRewardIndex].RewardType == SoloLeagueRewardDBData.RewardTypeEnum.PackPremium)
			{
				_shopList = new Dictionary<string, ShopPackData> ();
				_shopList.Add (_dataList [_showRewardIndex].ShopID
					, new ShopPackData(_dataList [_showRewardIndex].ShopID));

				//update new exp,diamond
				PlayerSave.SavePlayerExp(_iNewExp);
				PlayerSave.SavePlayerDiamond (_iCostDiamond);


				List<string> resultCardList; 
                _sAmtToShow = "5";
				RandomPremium("SP0001", out resultCardList);

			}

		} 
		else 
		{
			SoloLeaguePopUpUI.ShowUI (
				"SOLOLEAGUE_REWARD_FAILED_TITLE",
				"SOLOLEAGUE_REWARD_FAILED_MSG",
				"SOLOLEAGUE_REWARD_FAILED_BUTTON",
				this.ShowReward
			);	
		}

	}

	public void onClickDoneAfterShowPackResultReward()
	{
		// show card in pack (in case buy pack)
		SoloLeagueRewardInfoUI.HideUI ();
	}


	private void RandomNormal(string shopPackID, out List<string> resultList)
	{
		ShopPackData shop = null;
		bool isFound = _shopList.TryGetValue (shopPackID, out shop);
		Debug.Log ("found shoplist or not ? " + isFound);
		if (isFound) 
		{
			//ConfirmPurchaseUI.HideUI();
			SetShowCardResultUI (true);

			shop.RandomNormal(out resultList);
			int index = 0;
			foreach (string id in resultList) 
			{
				PlayerCardData card = new PlayerCardData (new CardData(id));

				// add new card to stash
				bool isSuccess = AddCardToStash(card.PlayerCardID);
				if(isSuccess)
				{
					bool isNew = false;
					if (PlayerSave.SavePlayerCardCollection(card.PlayerCardID)) 
					{
						// New card
						isNew = true;
					} 

					bool isGlow = false;
					if ( (card.SymbolData == CardSymbolData.CardSymbol.Rare) ||
						(card.SymbolData == CardSymbolData.CardSymbol.SuperRare) ||
						(card.SymbolData == CardSymbolData.CardSymbol.Forbidden)) 
					{
						isGlow = true;
					} 

					SoloLeagueRewardPackResultUI.ShowCard (index, new PlayerCardData (card), isGlow, isNew, this.OnClickedPreview);
				}
				++index;
			}

			return;
		}

		resultList = null;
	}

	private void RandomPremium(string shopPackID, out List<string> resultList)
	{
		ShopPackData shop = null;
		bool isFound = _shopList.TryGetValue (shopPackID, out shop);
		if (isFound) 
		{
			//ConfirmPurchaseUI.HideUI();
			SetShowCardResultUI (true);

			shop.RandomPremium(out resultList);
			int index = 0;
			foreach (string id in resultList) 
			{
				PlayerCardData card = new PlayerCardData (new CardData(id));

				// add new card to stash
				bool isSuccess = AddCardToStash(card.PlayerCardID);
				if(isSuccess)
				{
					bool isNew = false;
					if (PlayerSave.SavePlayerCardCollection(card.PlayerCardID)) 
					{
						// New card
						isNew = true;
					} 

					bool isGlow = false;
					if ( (card.SymbolData == CardSymbolData.CardSymbol.Rare) ||
						(card.SymbolData == CardSymbolData.CardSymbol.SuperRare) ||
						(card.SymbolData == CardSymbolData.CardSymbol.Forbidden)) 
					{
						isGlow = true;
					} 

					SoloLeagueRewardPackResultUI.ShowCard (index, new PlayerCardData (card), isGlow, isNew, this.OnClickedPreview);
				}
				++index;
			}

			return;
		}

		resultList = null;
	}


	private bool AddCardToStash(string playerCardID)
	{
		List<PlayerCardData> stash;
		bool isSuccess = PlayerSave.GetPlayerCardStash(out stash);
		if(isSuccess)
		{
			stash.Add(new PlayerCardData(playerCardID));
			PlayerSave.SavePlayerCardStash (stash);

			return true;
		}
		else
		{
			stash = new List<PlayerCardData>();
			stash.Add(new PlayerCardData(playerCardID));
			PlayerSave.SavePlayerCardStash (stash);

			return true;
		}

		return false;
	}

	private void SetShowCardResultUI(bool isShow)
	{
		if (isShow)
		{
            SoloLeagueRewardPackResultUI.ShowUI (_sAmtToShow);
		} 
		else
		{
			SoloLeagueRewardPackResultUI.HideUI ();
		}
	}

	void OnClickedPreview(FullCardUI fullCardUI)
	{
		Debug.Log ("Name:" + fullCardUI.PlayerCardData.Name_EN);

		SoloLeagueRewardPreviewCardUI.ShowUI (fullCardUI.PlayerCardData);
	}


	public void OnCancelConfirmReward()
	{
		SoloLeagueRewardConfirmUI.HideUI();
	}

	public void HideSoloRewardConfirmUI()
	{
		SoloLeagueRewardInfoUI.HideUI ();
	}

	public void HideSoloLeagueReardFinalConfirmUI()
	{
		SoloLeagueRewardConfirmUI.HideUI ();
	}

}
