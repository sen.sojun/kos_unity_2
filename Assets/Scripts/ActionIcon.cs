﻿using UnityEngine;
using System.Collections;

public class ActionIcon : MonoBehaviour 
{
	public enum ActionState : int
	{
		  None		= 0
		, Attack
		, Move
		, Acted
	}

	public GameObject AttackIconUI;
	public GameObject MoveIconUI;
	public GameObject ActedIconUI;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ShowActionIcon(ActionState actionType)
	{
		switch(actionType)
		{
			case ActionState.Attack:
			{
				AttackIconUI.SetActive(true);
				MoveIconUI.SetActive(false);
				ActedIconUI.SetActive(false);

				BattleUIManager.RequestBringForward(AttackIconUI.gameObject);
			}
			break;

			case ActionState.Move:
			{
				AttackIconUI.SetActive(false);
				MoveIconUI.SetActive(true);
				ActedIconUI.SetActive(false);

				BattleUIManager.RequestBringForward(MoveIconUI.gameObject);
			}
			break;

			case ActionState.Acted:
			{
				AttackIconUI.SetActive(false);
				MoveIconUI.SetActive(false);
				ActedIconUI.SetActive(true);

				BattleUIManager.RequestBringForward(ActedIconUI.gameObject);
			}
			break;

			default:
			{
				AttackIconUI.SetActive(false);
				MoveIconUI.SetActive(false);
				ActedIconUI.SetActive(false);
			}
			break;
		}
	}
}
