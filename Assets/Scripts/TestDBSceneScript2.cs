﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestDBSceneScript2 : MonoBehaviour 
{
    private List<CardDBData> _dataList;
    private int _index = 0;
    
    public FullCardUI2 FullCardUI_L;
    public FullCardUI2 FullCardUI_M;
    public FullCardUI2 FullCardUI_S;
    public Text IDText;

	// Use this for initialization
	void Start () 
    {
        CardDB.GatAllData(out _dataList);
         
		ShowCurrentCard();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    
    public void Next()
    {
        _index++;
        
        if(_index >= _dataList.Count)
        {
            _index = 0;
        }
        
        ShowCurrentCard();
    }
    
    public void Previous()
    {
        _index--;
        
        if(_index < 0)
        {
            _index = _dataList.Count - 1;
        }
        
        ShowCurrentCard();
    }
    
    public void OnToggleLanguage()
    {
        switch (LocalizationManager.CurrentLanguage)
        {
            case LocalizationManager.Language.Thai:
            {
                LocalizationManager.SetLanguage (LocalizationManager.Language.English);
            }
            break;

            case LocalizationManager.Language.English:
            default:
            {
                LocalizationManager.SetLanguage (LocalizationManager.Language.Thai);
            }
            break;
        }
        
        ShowCurrentCard();
    }
    
    private void ShowCurrentCard()
    {
        PlayerCardData data = new PlayerCardData(_dataList[_index].CardID);
        FullCardUI_L.SetData(data);
        FullCardUI_M.SetData(data);
        FullCardUI_S.SetData(data);
        IDText.text = data.CardID;
    }
}
