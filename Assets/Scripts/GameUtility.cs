﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.Networking.Types;
using UnityEngine.Networking.Match;
using GamePlayerUI;

using System.Security.Cryptography;

public static class GameUtility 
{
	#region Compare Methods
	public static RPSResult Compare(RPSType rps1, RPSType rps2)
	{
		switch (rps1)
		{
			case RPSType.Rock:
			{
				switch(rps2)
				{
					case RPSType.Rock:
					{
						return RPSResult.Draw;
					}
					break;

					case RPSType.Paper:
					{
						return RPSResult.Lose;
					}
					break;

					case RPSType.Scissors:
					{
						return RPSResult.Win;
					}
					break;
				}
			}
			break;

			case RPSType.Paper:
			{
				switch(rps2)
				{
					case RPSType.Rock:
					{
						return RPSResult.Win;
					}
					break;

					case RPSType.Paper:
					{
						return RPSResult.Draw;
					}
					break;

					case RPSType.Scissors:
					{
						return RPSResult.Lose;
					}
					break;
				}
			}
			break;

			case RPSType.Scissors:
			{
				switch(rps2)
				{
					case RPSType.Rock:
					{
						return RPSResult.Lose;
					}
					break;

					case RPSType.Paper:
					{
						return RPSResult.Win;
					}
					break;

					case RPSType.Scissors:
					{
						return RPSResult.Draw;
					}
					break;
				}
			}
			break;
		}

		return RPSResult.Draw;
	}
	#endregion

	#region Convert Methods
	public static DisplayBattlePhase BattlePhaseToDisplayBattlePhase(BattlePhase battlePhase)
		{
			if (battlePhase >= BattlePhase.BeginGameSetup && battlePhase <= BattlePhase.EndGameSetup)
			{
				return DisplayBattlePhase.GameSetup;
			}
			else if (battlePhase >= BattlePhase.BeginBegin && battlePhase <= BattlePhase.EndBegin)
			{
				return DisplayBattlePhase.Begin;
			}
			/*
	        else if (battlePhase >= BattlePhase.BeginInactive && battlePhase <= BattlePhase.EndInactive)
	        {
	            return DisplayBattlePhase.Inactive;
	        }
	        else if (battlePhase >= BattlePhase.BeginReiterate && battlePhase <= BattlePhase.EndReiterate)
	        {
	            return DisplayBattlePhase.Reiterate;
	        }
			*/
			else if (battlePhase >= BattlePhase.BeginDraw && battlePhase <= BattlePhase.EndDraw)
			{
				return DisplayBattlePhase.Draw;
			}
			else if (battlePhase >= BattlePhase.BeginPreBattle && battlePhase <= BattlePhase.EndPreBattle)
			{
				return DisplayBattlePhase.PreBattle;
			}
			else if (battlePhase >= BattlePhase.BeginBattle && battlePhase <= BattlePhase.EndBattle)
			{
				return DisplayBattlePhase.Battle;
			}
			else if (battlePhase >= BattlePhase.BeginPostBattle && battlePhase <= BattlePhase.EndPostBattle)
			{
				return DisplayBattlePhase.PostBattle;
			}
			else if (battlePhase >= BattlePhase.BeginEnd && battlePhase <= BattlePhase.EndEnd)
			{
				return DisplayBattlePhase.End;
			}

			Debug.LogError("BattleManager/GetDisplayBattlePhase: CurrentBattlePhase not in any phase.");
			return DisplayBattlePhase.End;
		}
	#endregion

	#region UI Methods
	public static void RequestBringForward(GameObject go, bool isIncludeParentPanel = true)
	{
		if(go != null)
		{
			//Debug.Log(go.name);

			UIPanel panel = go.GetComponent<UIPanel>();
			if(panel != null)
			{
				// Panel
				//Debug.Log("Panel");

				NGUITools.BringForward(go);
			}
			else
			{
				// Widget
				panel = NGUITools.FindInParents<UIPanel>(go);

				UIWidget[] widgets = go.GetComponentsInChildren<UIWidget>(true);

				for (int i = 0, imax = widgets.Length; i < imax; ++i)
				{
					UIWidget w = widgets[i];

					// init widget panel.
					if (w.panel == null || w.panel != panel)
					{
						w.panel = panel;

						//if(!w.gameObject.activeSelf) continue;

						NGUITools.MarkParentAsChanged(w.gameObject);
					}
				}

				/*
				{
					UIWidget widget = go.GetComponent<UIWidget>();

					if(widget.panel == null) 
					{
						//Debug.Log("Null Panel");

						//panel = NGUITools.FindInParents<UIPanel>(go);

						UIWidget[] parentsWidget = go.GetComponentsInParent<UIWidget>(true);

						if(parentsWidget.Length > 0)
						{
							bool isFound = false;
							foreach (UIWidget parentWidget in parentsWidget) 
							{
								if(parentWidget.panel != null)
								{
									widget.panel = parentWidget.panel;
									isFound = true;
									break;
								}
							}

							if(!isFound)
							{
								UIPanel[] parentsPanel = widget.GetComponentsInParent<UIPanel>(true);
								if(parentsPanel.Length > 0)
								{
									widget.panel = parentsPanel[0];
								}
							}
						}
					}
				}
				*/

				if(isIncludeParentPanel) 
				{
					//Debug.Log("Parent Panel: " + widget.panel.name);

					UIWidget widget = go.GetComponent<UIWidget>();
					if(widget != null)
					{
						if(widget.panel != null)
						{
							NGUITools.BringForward(widget.panel.gameObject);
						}
						else
						{
							//Debug.LogError("Not found panel.");
						}
					}
					/*
					if(widget.panel != null && NGUITools.GetActive(widget.panel.gameObject))
					{
						//NGUITools.ImmediatelyCreateDrawCalls(widget.panel.gameObject);
					}
					*/
				}

				//Debug.Log("GO: " + go.name);

				NGUITools.BringForward(go);

				/*
				if(go != null)
				{
					//NGUITools.ImmediatelyCreateDrawCalls(go);
				}
				*/
			}
		}
	} 
	#endregion

    public static RPSType RandomRPS()
    {    
        //int r = UnityEngine.Random.Range(0, 3); // [0-3)        
        int r = RandomNumber.Between(0, 2); // [0-2]
        //int r = RandomByDeltaTime(0, 3);
                
        r = Mathf.Clamp(r, 0, 2);
        
        RPSType optionType = (RPSType)r;

        return optionType;
    }

    public static List<PlayerCardData> SortCard(List<PlayerCardData> cardList)
    {
        List<PlayerCardData> result = cardList;

        if (result != null && result.Count > 0)
        {
            result.Sort(GameUtility.SortCardByTypeClanSymbolCostName);
        }

        return result;
    }

    public static List<UniqueCardData> SortCard(List<UniqueCardData> cardList)
    {
        List<UniqueCardData> result = cardList;

        if (result != null && result.Count > 0)
        {
            result.Sort(GameUtility.SortCardByTypeClanSymbolCostName);
        }

        return result;
    }

    private static int SortCardByTypeClanSymbolCostName(PlayerCardData a, PlayerCardData b)
    {
        int result = 0;
         
        if(result == 0)
        {
            // Sort by type.   
            int va = (int)CardTypeData.EnumIDToCardType(a.TypeData.CardTypeID);
            int vb = (int)CardTypeData.EnumIDToCardType(b.TypeData.CardTypeID);

            if (va > vb)        result = 1;
            else if (va < vb)   result = -1;
            else                result = 0;
        }

        if(result == 0)
        {
            // Sort by clan.   
            result = string.Compare(a.ClanData.CardClanID, b.ClanData.CardClanID);
        }
            
        if(result == 0)
        {
            // Sort by symbol.   
            result = string.Compare(a.SymbolData.CardSymbolID, b.SymbolData.CardSymbolID);
        }

        if(result == 0)
        {
            // Sort by cost.   
            if (a.Cost > b.Cost)        result = 1;
            else if (a.Cost > b.Cost)   result = -1;
            else                        result = 0;
        }

        if(result == 0)
        {
            // Sort by name eng.   
            result = string.Compare(a.Name_EN, b.Name_EN);
        }

        return result;
    }

    private static int SortCardByTypeClanSymbolCostName(UniqueCardData a, UniqueCardData b)
    {
        PlayerCardData aa = a.PlayerCardData;
        PlayerCardData bb = b.PlayerCardData;

        return SortCardByTypeClanSymbolCostName(aa, bb);
    }

    #region Battle Methods

    #endregion

    public static string InfixToPostfix(string infixText)
    {
        Dictionary<string, int> prec = new Dictionary<string, int>();
        prec.Add("!", 3); // NOT
        prec.Add("|", 2); // OR
        prec.Add("&", 2); // AND
        prec.Add("(", 1); 

        Stack<string> opStack = new Stack<string>();
        string postFixText = "";

        for (int index = 0; index < infixText.Length; ++index)
        {
            char token = infixText[index];

            if (!prec.ContainsKey(token.ToString()) && token != ')')
            {
                // Not operator
                postFixText = postFixText + token.ToString();
            }
            else if (token == '(')
            {
                opStack.Push(token.ToString());
            }
            else if (token == ')')
            {
                string topToken = opStack.Pop();
                while (string.Compare(topToken, "(") != 0)
                {
                    postFixText = postFixText + topToken;
                    topToken = opStack.Pop();
                }
            }
            else
            {
                while(opStack.Count > 0 && prec[opStack.Peek()] >= prec[token.ToString()])
                {
                    postFixText = postFixText + opStack.Pop();
                }
                opStack.Push(token.ToString());
            }

            //Debug.Log(postFixText);
        }

        while (opStack.Count > 0)
        {
            postFixText = postFixText + opStack.Pop();
        }

        return postFixText;
    }
    
    public static class RandomNumber
    {
        private static readonly RNGCryptoServiceProvider _generator = new RNGCryptoServiceProvider();

        public static int Between(int minimumValue, int maximumValue)
        {
            byte[] randomNumber = new byte[1];

            _generator.GetBytes(randomNumber);

            double asciiValueOfRandomCharacter = Convert.ToDouble(randomNumber[0]);

            // We are using Math.Max, and substracting 0.00000000001, 
            // to ensure "multiplier" will always be between 0.0 and .99999999999
            // Otherwise, it's possible for it to be "1", which causes problems in our rounding.
            double multiplier = Math.Max(0, (asciiValueOfRandomCharacter / 255d) - 0.00000000001d);

            // We need to add one to the range, to allow for the rounding done with Math.Floor
            int range = maximumValue - minimumValue + 1;

            double randomValueInRange = Math.Floor(multiplier * range);

            return (int)(minimumValue + randomValueInRange);
        }
    }
    
    public static int RandomByDeltaTime(int min, int max)
    {
        float t = Time.deltaTime;
        int range = max - min;
        if(range >= 0)
        {
            int n =  min + ((Mathf.FloorToInt(t * 1000.0f)) % range);
        }
        
        return min;
    }
}

[System.Serializable]
public class UnityStringEvent : UnityEvent<string>
{
}

[System.Serializable]
public class UnityString2Event : UnityEvent<string, string>
{
}

[System.Serializable]
public class UnityString3Event : UnityEvent<string, string, string>
{
}

[System.Serializable]
public class UnityIntEvent : UnityEvent<int>
{
}

[System.Serializable]
public class UnityFloatEvent : UnityEvent<float>
{
}

[System.Serializable]
public class UnityBoolEvent : UnityEvent<bool>
{
}

[System.Serializable]
public class UnityNetworkIDEvent : UnityEvent<NetworkID>
{
}


