﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CardEditorListUI : MonoBehaviour 
{
	#region Delegates
    public delegate void OnClickCallback(PlayerCardData data);
    public delegate void OnDropCardCallback(int listIndex, int cardDataIndex);
	#endregion

	#region Private Properties
    private int _listIndex;
    private int _cardPerPage = 5;
    private int __pageIndex = 0;
    private int _pageIndex 
    { 
        get { return __pageIndex; } 
        set 
        {
            __pageIndex = value;

            if (PrevButton != null)
            {
                if (__pageIndex <= 0)
                {
                    PrevButton.gameObject.SetActive(false);
                }
                else
                {
                    PrevButton.gameObject.SetActive(true);
                }
            }

            if (NextButton != null)
            {
                if (__pageIndex >= MaxPageIndex)
                {
                    NextButton.gameObject.SetActive(false);
                }
                else
                {
                    NextButton.gameObject.SetActive(true);
                }
            }
        }
    }
    private List<PlayerCardData> _cardDataList;
    private List<CardEditorUI> _cardUIList;
    private bool _isInit = false;

	private OnClickCallback _onClickCallback = null;
    private OnDropCardCallback _onDropCardCallback = null;
	#endregion

	#region Public Properties
    public int CardPerPage { get { return _cardPerPage; } }
    public int PageIndex { get { return _pageIndex; } }
    public int MaxPageIndex 
    { 
        get 
        { 
            if(_cardDataList != null && _cardDataList.Count > 0) 
            {
                return (Mathf.CeilToInt((float)_cardDataList.Count / (float)_cardPerPage)) - 1;
            }
            else
            {
                return 0;
            }
        }
    }
    public int CardListIndex { get { return _listIndex; } }
	#endregion

	#region Public Inspector Properties
    public UILabel PageLabel;
	public UILabel CardQtyLabel;

    public UITable Table;
    public UIButton PrevButton;
    public UIButton NextButton;

    public GameObject CardEditorUIPrefab;

    public int MaxDeck = 0;
	#endregion

    #region Awake
    void Awake()
    {
        _cardUIList = new List<CardEditorUI>();
    }
    #endregion

	#region Start
	// Use this for initialization
	void Start () 
	{
        Init();
	}
	#endregion

	#region Update
	// Update is called once per frame
	void Update () 
	{
        //Init();
	}
	#endregion

	#region Methods
    public void Init()
    {
        UIWidget widget = Table.GetComponent<UIWidget>();
        Vector2 size = new Vector2(widget.width, widget.height);

        //Debug.Log(size);

        Vector2 cardSize = new Vector2(246, 340); // card size.

        int showNum = Mathf.FloorToInt(size.x/cardSize.x);

        float freeSpace = (size.x - (cardSize.x * showNum));
        float space = freeSpace / (float)showNum / 2.0f; 

        //Debug.Log(freeSpace);

        _cardPerPage = showNum;
        Table.columns = showNum;
        Table.padding = new Vector2(space, 0.0f);

        _isInit = true;
    }

	public void ClearUI()
	{
        Table.transform.DestroyChildren();
        _cardUIList.Clear(); 
	}

    public void SetInitDataList(int listIndex, List<PlayerCardData> cardDataList, OnClickCallback onClickCallback, OnDropCardCallback onDropCardCallback)
	{
		// Clear old UI.
		ClearUI();

        /*
        if (!_isInit)
            Init();
        */

        _listIndex = listIndex;
		_onClickCallback = onClickCallback;
        _onDropCardCallback = onDropCardCallback;

        _cardDataList = cardDataList;
        if (_cardDataList == null)
        {
            _cardDataList = new List<PlayerCardData>();
        }

        if (_pageIndex <= 0)
        {
            _pageIndex = 0;
        }
        else if (_pageIndex >= MaxPageIndex)
        {
            _pageIndex = MaxPageIndex;
        }
        else
        {
            _pageIndex = _pageIndex;
        }

        ShowPage(_pageIndex);
	}
        
    private void ShowPage(int pageIndex)
    {
        if (_cardDataList != null)
        {
            int startShowIndex = pageIndex * CardPerPage;
            int endShowIndex = (startShowIndex + CardPerPage);

            if (endShowIndex > _cardDataList.Count)
            {
                endShowIndex = _cardDataList.Count;
            }

            int showCount = endShowIndex - startShowIndex;

            _cardUIList.Clear();
            foreach (Transform child in Table.transform)
            {
                CardEditorUI ui = child.GetComponent<CardEditorUI>();
                _cardUIList.Add(ui);
            }

            if (_cardUIList.Count < showCount)
            {
                while (_cardUIList.Count < showCount)
                {
                    // Create new card obj.
                    GameObject obj = Instantiate(CardEditorUIPrefab) as GameObject;
                    obj.transform.SetParent(Table.transform);
                    obj.transform.localScale = Vector3.one;
                    NGUITools.SetLayer(obj, Table.gameObject.layer);

                    _cardUIList.Add(obj.GetComponent<CardEditorUI>());
                }
            }

            if (_cardUIList.Count > showCount)
            {
                while (_cardUIList.Count > showCount)
                {
                    GameObject obj = _cardUIList[0].gameObject;
                    _cardUIList.RemoveAt(0);

                    NGUITools.Destroy(obj);
                }
            }
                
            int cardDataIndex = startShowIndex;
            for (int cardUIIndex = 0; cardUIIndex < _cardUIList.Count; ++cardUIIndex)
            {
                if (cardDataIndex < endShowIndex)
                {
                    _cardUIList[cardUIIndex].SetData(
                          CardListIndex
                        , cardDataIndex
                        , _cardDataList[cardDataIndex]
                        , this.OnClickUI
                    );

                    _cardUIList[cardUIIndex].gameObject.SetActive(true);
                    ++cardDataIndex;
                }
                else
                {
                    _cardUIList[cardUIIndex].gameObject.SetActive(false);
                }
            }
        }

        Table.Reposition();

            
        UpdateQtyLabel();
        UpdatePageLabel();

        //NGUITools.BringForward(gameObject);
    }

    public void UpdateUI()
    {
        if (_pageIndex <= 0)
        {
            _pageIndex = 0;
        }
        else if (_pageIndex >= MaxPageIndex)
        {
            _pageIndex = MaxPageIndex;
        }

        ShowPage(_pageIndex);

        Init();
    }

    private void UpdateQtyLabel()
    {
        int count = 0;

        if(_cardDataList != null)
        {
            count = _cardDataList.Count;
        }

        if(MaxDeck > 0)
        {
            CardQtyLabel.text = count.ToString() + "/" + MaxDeck.ToString();
        }
        else
        {
            CardQtyLabel.text = count.ToString();
        }
    }

    private void UpdatePageLabel()
    {
        PageLabel.text = string.Format(
            "{0}/{1}"
            , PageIndex + 1
            , MaxPageIndex + 1
        );
    }

    public void ReIndexUI()
    {
        _cardUIList.Clear();

        foreach (Transform child in Table.transform)
        {
            _cardUIList.Add(child.GetComponent<CardEditorUI>());
        }
    }
    #endregion 

    #region Event Methods
    public void OnClickPrev()
    {
        --_pageIndex;
        ShowPage(_pageIndex);
    }

    public void OnClickNext()
    {
        ++_pageIndex;
        ShowPage(_pageIndex);
    }

    private void OnClickUI(PlayerCardData data)
	{
		if(_onClickCallback != null)
		{
            _onClickCallback.Invoke(data);
		}
	}

    public void OnDropCard(int cardDataIndex)
    {
        if(_onDropCardCallback != null)
        {
            _onDropCardCallback.Invoke(_listIndex, cardDataIndex);
        }
    }
	#endregion
}
