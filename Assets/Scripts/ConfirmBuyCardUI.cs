﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConfirmBuyCardUI : MonoBehaviour {

    public delegate void OnPurchase();
    public delegate void OnBack();

    public Text HeadLabel;
    public Text DescriptionLabel;

    public Button ConfirmButton;
    public Button CancelButton;
    public Button CenterBackButton;

    private OnPurchase _onPurchase;
    private OnBack _onBack;

	// Use this for initialization
	void Start () 
    {	
	}
	
    /*
	// Update is called once per frame
	void Update () 
    {	
	}
    */

    public void ShowUI(
          string headLabelText
        , string descriptionText
        , OnPurchase onPurchase
        , OnBack onBack
        , bool confirmShowButton
        , bool cancelShowButton
        , bool centerBackButtonShow
    )
    {
        HeadLabel.text = headLabelText;
        DescriptionLabel.text = descriptionText;
        
        ConfirmButton.gameObject.SetActive(confirmShowButton);
        CancelButton.gameObject.SetActive(cancelShowButton);
        CenterBackButton.gameObject.SetActive(centerBackButtonShow);
        
        ConfirmButton.interactable = confirmShowButton;
        CancelButton.interactable = cancelShowButton;
        CenterBackButton.interactable = centerBackButtonShow;

        _onPurchase = onPurchase;
        _onBack = onBack;

        gameObject.SetActive(true);      
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }

    public void OnPurchaseClick()
    {
        ConfirmButton.interactable = false;
        CancelButton.interactable = false;
        CenterBackButton.interactable = false;
    
        if (_onPurchase != null)
        {
            _onPurchase.Invoke();
        }
    }

    public void OnBackClick()
    {
        ConfirmButton.interactable = false;
        CancelButton.interactable = false;
        CenterBackButton.interactable = false;
    
        if (_onBack != null)
        {
            _onBack.Invoke();
        }
    }
}
