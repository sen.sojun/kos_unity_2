﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

[RequireComponent(typeof(UILabel))]
public class BlinkLabel : MonoBehaviour 
{
	public Color StartColor = Color.white;
	public Color EndColor = Color.white;
	public float BlinkTime = 1.0f;
	public Ease EaseType = Ease.Linear;

	private Tweener _t = null;

	// Use this for initialization
	void Start () {
		//StartBlink ();
	}

	// Update is called once per frame
	void Update () 
	{

	}

	void OnEnable()
	{
		StartBlink ();
	}

	void OnDisable()
	{
		StopBlink ();
	}

	private void StartBlink()
	{   
		if (_t == null) {
			UILabel label = GetComponent<UILabel> ();
			label.color = StartColor;

			_t = DOTween.To (
				() => label.color
				, x => label.color = x
				, EndColor
				, BlinkTime);

			_t.SetLoops (-1, LoopType.Yoyo);
			_t.SetEase (EaseType);
			_t.SetAutoKill (false);
			_t.Play ();
		} else {
			_t.Play ();
		}
	}

	private void StopBlink()
	{  
		_t.Pause ();
	}
}
