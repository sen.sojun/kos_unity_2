﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameAnalyticsSDK;

public class TestDBSceneScriptPlayerDeck : MonoBehaviour 
{
	public FullCardUI FullCard_S;
	public FullCardUI FullCard_M;
	public FullCardUI FullCard_L;

	public BattleCardUI BattleCard;

    public UILabel IndexLabel;

	private List<PlayerCardData> _deck; //<LIST> -> array like ,with memory allocation
    private int cardIndex = 0;

	private GAProgressionStatus progressionStart = GAProgressionStatus.Start;
	private GAProgressionStatus progressionComplete = GAProgressionStatus.Complete;



	// Use this for initialization
	void Start () 
    {
		
		GameAnalytics.NewProgressionEvent(progressionStart,"Start Deck",30);
		_deck = new List<PlayerCardData>(); //create empty list

		DeckPackData pack = new DeckPackData ("D0001");

		// Save to player local.
		// PlayerSave.SavePlayerDeck (pack.DeckData);

		Debug.Log ("Deck: " + pack.DeckData.Count);

		for(int index = 0; index < pack.DeckData.Count; ++index)
        {
			_deck.Add(pack.DeckData.GetCardAt(index).PlayerCardData);
        }

        ShowCardData(cardIndex);
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public void ShowNextCard()
    {
		//Debug.Log("Show Next Card");
        ++cardIndex;

        if (cardIndex >= _deck.Count)
        {
            cardIndex = 0;
        }

        IndexLabel.text = (cardIndex + 1).ToString();

        ShowCardData(cardIndex);
   }

    public void ShowPrevCard()
    {
		//Debug.Log("Show Prev Card");
        --cardIndex;

        if (cardIndex < 0)
        {
            cardIndex = _deck.Count - 1;
        }

        IndexLabel.text = (cardIndex + 1).ToString();

        ShowCardData(cardIndex);
    }

    public void ShowCardData(int index)
    {
        if(index >= 0 && index < _deck.Count)
        {
			PlayerCardData playerCardData = new PlayerCardData(_deck[index]);

			FullCard_S.SetCardData(playerCardData);
			FullCard_M.SetCardData(playerCardData);
            FullCard_L.SetCardData(playerCardData);

			BattleCard.SetInitData(
				  new BattleCardData(playerCardData, PlayerIndex.One, BattleZoneIndex.Battlefield) // Dummy
				, FieldUI.FieldSide.Player
				, null
			);
        }
    }
}
