﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TestDBSceneScript : MonoBehaviour 
{
	public FullCardUI FullCard_S;
	public FullCardUI FullCard_M;
	public FullCardUI FullCard_L;
	public HandCardUI HandCardUI;

	public BattleCardUI BattleCard;

    public UILabel IndexLabel;

    private List<CardData> _deck; //<LIST> -> array like ,with memory allocation
    private int cardIndex = 0;

	// Use this for initialization
	void Start () 
    {
        _deck = new List<CardData>(); //create empty list

        List<CardDBData> rawDataList;
        bool isSuccess = CardDB.GatAllData(out rawDataList);
        if (isSuccess)
        {
            foreach (CardDBData rawData in rawDataList)
            {
                CardData cardData = new CardData(rawData.CardID);
                _deck.Add(cardData);
            }
        }
        else
        {
            Debug.LogError("TestDBSceneScript/Start: CardDB.GatAllData Failed!!");
        }

		/*
		IDParams idParams;
		bool isSuccessParse = IDParams.StrToIDParams("A0001(2)", out idParams);
		if(isSuccessParse)
		{
			AbilityData.AbilityParams ap = new AbilityData.AbilityParams(idParams.ID, idParams.Parameters);

			AbilityData a = AbilityData.AbilityParams.ToAbilityData(null, ap);
			Debug.Log(a.GetDebugPrintText());
		}
		*/

        ShowCardData(cardIndex);
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public void ShowNextCard()
    {
		//Debug.Log("Show Next Card");
        ++cardIndex;

        if (cardIndex >= _deck.Count)
        {
            cardIndex = 0;
        }

        IndexLabel.text = (cardIndex + 1).ToString();

        ShowCardData(cardIndex);
    }

    public void ShowPrevCard()
    {
		//Debug.Log("Show Prev Card");
        --cardIndex;

        if (cardIndex < 0)
        {
            cardIndex = _deck.Count - 1;
        }

        IndexLabel.text = (cardIndex + 1).ToString();

        ShowCardData(cardIndex);
    }

    public void ShowCardData(int index)
    {
        if(index >= 0 && index < _deck.Count)
        {
			PlayerCardData playerCardData = new PlayerCardData(_deck[index]);

			FullCard_S.SetCardData(playerCardData);
			FullCard_M.SetCardData(playerCardData);
            FullCard_L.SetCardData(playerCardData);
			HandCardUI.SetCardData(playerCardData);

			BattleCard.SetInitData(
				  new BattleCardData(playerCardData, PlayerIndex.One, BattleZoneIndex.Battlefield) // Dummy
				, FieldUI.FieldSide.Player
				, null
			);

			int actionIndex = Random.Range(0, 4);
			BattleCard.ActionIcon.ShowActionIcon((ActionIcon.ActionState)actionIndex);

			BattleCard.ShowArrow(true);
			HandCardUI.ShowArrow(true);
        }
    }

	public void AddCardToStash()
	{
		int index = cardIndex;
		PlayerCardData playerCardData = new PlayerCardData(_deck[index]);

		PlayerSave.SavePlayerCardStash (playerCardData.CardID);
		PlayerSave.SavePlayerCardCollection (playerCardData.CardID);
	}
}
