﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(UIButton))]
public class NPCButtonNew : MonoBehaviour {

	public string NPCID = "";
    
    public Image NPCImage;
    public Text NameLabel;
    public GameObject GlowEffect;

    private bool _isInit = false;
    private NPCDBData _npcDBData;

	string imagename = "";

    // Use this for initialization
    void Start()
    {
        UpdateUI();
    }

    void OnEnable()
    {
        UpdateUI();
    }

    // Update is called once per frame
    void Update()
    {
        if (!_isInit || string.Compare(NPCID, _npcDBData.NPCID) != 0)
        {
            UpdateUI();
        }
    }

    public void UpdateUI()
    {
        bool isSuccess = NPCDB.GetData(NPCID, out _npcDBData);
        if (isSuccess)
        {
			try
			{
				imagename = "Images/Avatars/Campaign/" + _npcDBData.ImageName;
 
				Debug.Log("image name = " + imagename);
                
				Texture imageTexture = Resources.Load(imagename, typeof(Texture)) as Texture;
				if (imageTexture != null)
				{
					Texture2D texture2D = imageTexture as Texture2D;
					Sprite mySprite = Sprite.Create(texture2D, new Rect(0.0f, 0.0f, texture2D.width, texture2D.height), new Vector2(0.5f, 0.5f), 100.0f);

					NPCImage.sprite = mySprite; //_npcDBData.ImageName;
				}
                else
				{
					Debug.LogError(
                           string.Format(
                                 "NPCButtonNew Image not found! ({0}"
							, imagename
                           )
                       );
				}
			}

            catch (System.Exception e)
			{
				Debug.Log(e);
			}


            string name = "";
            switch (LocalizationManager.CurrentLanguage)
            {
                case LocalizationManager.Language.Thai:
                    {
                        name = _npcDBData.Name_TH;
                    }
                    break;

                case LocalizationManager.Language.English:
                default:
                    {
                        name = _npcDBData.Name_EN;
                    }
                    break;
            }

            NameLabel.text = name;
            _isInit = true;
        }
    }

    public void SetEnableButton(bool isEnable)
    {
        GetComponent<UIButton>().isEnabled = isEnable;
    }

    public void SetShowGlowEffect(bool isShow)
    {
        GlowEffect.SetActive(isShow);
    }
}
