﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CardEditorListUINew : MonoBehaviour, IDropHandler
{
    #region Delegates
    public int MaxCardPerPage = 7;
    public delegate void OnClickCallback(PlayerCardData data);
    public delegate void OnClickEvent(FullCardUI2 fullcardUI, string sDeckOrStash);
    public delegate void OnDropCardCallback(int listIndex, int cardDataIndex);
    //public List<FullCardUI2> FullCardList;
    public List<DeckCardUI> FullCardList;
    public FullCardUI2 FullCard_L;
    public GameObject ShowCardUIView;
    public Text TextMoveDeckOrStash;
    public Text TextCardIDToSwap;

    #endregion

   
    #region Private Properties
    private int _listIndex;
    private int _pageList = 0;
    private int _cardPerPage = 5;
    private int __pageIndex = 0;
    private int _pageIndex
    {
        get { return __pageIndex; }
        set
        {
            __pageIndex = value;

            if (PrevButton != null)
            {
                if (__pageIndex <= 0)
                {
                    PrevButton.gameObject.SetActive(false);
                }
                else
                {
                    PrevButton.gameObject.SetActive(true);
                }
            }

            if (NextButton != null)
            {
                if (__pageIndex >= MaxPageIndex)
                {
                    NextButton.gameObject.SetActive(false);
                }
                else
                {
                    NextButton.gameObject.SetActive(true);
                }

            }
        }
    }
    private List<PlayerCardData> _cardDataList;
    private List<PlayerCardData> _pageDataList;
    private List<CardEditorUINew> _cardUIList;

   
    private int _maxPageIndex = 0;
    private bool _isInit = false;
    private List<CardData> myCollection = new List<CardData>();
    private Dictionary<string, CardData> _myCollection;

    private Dictionary<string, structCollection> _myCollection2;

    private OnClickCallback _onClickCallback = null;
    //private OnClickEvent _onClickEvent = null;
    private OnDropCardCallback _onDropCardCallback = null;

    //private List<PlayerCardData> _cardStashList;
    //private List<PlayerCardData> _cardDeckList;

    private bool _bFromStash = false;
    private bool _bFromDeck = true;
    private int _DeckOrStorage = 0;
    #endregion

    #region collection
    public struct structCollection
    {
        public int index;
        public CardData cardData;
        public int Qty;
    };

    private List<structCollection> myStorageCollection = new List<structCollection>();
    private List<structCollection> myDeckCollection = new List<structCollection>();
    #endregion



    #region Public Properties
    public int PageList { get { return _pageList;  } }
    public int CardPerPage { get { return _cardPerPage; } }
    public int PageIndex { get { return _pageIndex; } }
    public int MaxPageIndex
    {
        //get
        //{
        //    if (_cardDataList != null && _cardDataList.Count > 0)
        //    {
        //        return (Mathf.CeilToInt((float)_cardDataList.Count / (float)_cardPerPage)) - 1;
        //    }
        //    else
        //    {
        //        return 0;
        //    }
        //}
        get
        {
            if (_pageList > 0)
            {
                return (Mathf.CeilToInt((float)_pageList / (float)_cardPerPage)) - 1;
            }
            else
            {
                return 0;
            }
        }
    }
    public int CardListIndex { get { return _listIndex; } }
    #endregion

    #region Public Inspector Properties
    public Text PageLabel;
    public Text CardQtyLabel;

    public HorizontalLayoutGroup Table;
    public Button PrevButton;
    public Button NextButton;

    public GameObject DragDropRoot;
    public GameObject CardEditorUIPrefab;

    public int MaxDeck = 0;
    #endregion

    #region Awake
    void Awake()
    {
        _cardUIList = new List<CardEditorUINew>();
    }
    #endregion

    #region Start
    // Use this for initialization
    void Start()
    {
        Init();
    }
    #endregion

    #region Update
    // Update is called once per frame
    void Update()
    {
        //Init();
    }
    #endregion

    #region Methods
    public void Init()
    {

        RectTransform widget = Table.GetComponent<RectTransform>();
        //UIWidget widget = Table.GetComponent<UIWidget>();
        Vector2 size = new Vector2(158,220);

        ////Debug.Log(size);

        Vector2 cardSize = new Vector2(158, 220); // card size.

        int showNum = MaxCardPerPage;
            //Mathf.FloorToInt(size.x / cardSize.x);

        float freeSpace = (size.x - (cardSize.x * showNum));
        float space = freeSpace / (float)showNum / 2.0f;

        //Debug.Log(freeSpace);

        _cardPerPage = showNum;

        Table.spacing = showNum;
        Table.padding.left = 10; //new Vector2(space, 0.0f);
        Table.padding.right = 10;
      

        _isInit = true;
    }

    public void ClearUI()
    {
       // Table.transform.DestroyChildren();
        //Table.transform.DestroyChildren();
        _cardUIList.Clear();
    }

    public void setStorageQty(int deckorstorage, List<PlayerCardData> cardDataList)
    {
        //    struct structCollection
        //{
        //    public int index;
        //    public CardData cardData;
        //    public int Qty;
        //};

        //private List<structCollection> myStorageCollection = new List<structCollection>();

        myStorageCollection.Clear();
        myDeckCollection.Clear();

        _cardDataList = cardDataList;
        if (_cardDataList == null)
        {
            _cardDataList = new List<PlayerCardData>();
        }

        _myCollection = new Dictionary<string, CardData>();
        _myCollection2 = new Dictionary<string, structCollection>();
        bool foundDub = false;
        int index = 0;

        foreach (PlayerCardData data in _cardDataList)
        {
            if (deckorstorage == 0)
            {
                for (int i = 0; i < myStorageCollection.Count; i++)
                {
                    foundDub = false;
                    //data.CardID

                    if (_cardDataList[index].CardID == myStorageCollection[i].cardData.CardID)
                    {
                        foundDub = true;
                        var v = myStorageCollection[i];
                        v.Qty = myStorageCollection[i].Qty + 1;
                        myStorageCollection[i] = v;
                        break;
                    }
                }
            }
            else
            {
                for (int i = 0; i < myDeckCollection.Count; i++)
                {
                    foundDub = false;

                    if (_cardDataList[index].CardID == myDeckCollection[i].cardData.CardID)
                    {
                        foundDub = true;
                        var v = myDeckCollection[i];
                        v.Qty = myDeckCollection[i].Qty + 1;
                        myDeckCollection[i] = v;
                        break;
                    }
                }
            }
            

            if (!foundDub)
            {
                structCollection d = new structCollection();
                d.cardData = data.CardData;
                d.Qty = 1;
                if (deckorstorage == 0)
                {
                    myStorageCollection.Add(d);
                }
                else
                {
                    myDeckCollection.Add(d);
                }

            }

            //foreach (structCollection structvar in myStorageCollection)
            //{
            //    if (structvar.cardData.CardID == data.CardID)
            //    {
            //        int index_ = myStorageCollection.FindIndex()  item => item.Number == textBox6.Text); 
            //       myStorageCollection[]
            //    }
            //    else
            //    {
                    
            //    }
            //}
            //_myCollection.Add(_cardDataList[index].CardID, data);
            ++index;
        }

        if (deckorstorage == 0)
        {
            
        }
        for (int i = 0; i < myStorageCollection.Count; i++)
        {
            Debug.Log("StroageCardid = " + myStorageCollection[i].cardData.CardID + " qty = " + myStorageCollection[i].Qty);
        }

        for (int i = 0; i < myDeckCollection.Count; i++)
        {
            Debug.Log("DeckCardid = " + myDeckCollection[i].cardData.CardID + " qty = " + myDeckCollection[i].Qty);
        }

 
    }

    public void SetInitDataList(int listIndex, List<PlayerCardData> cardDataList, OnClickCallback onClickCallback) //, OnDropCardCallback onDropCardCallback)
    {
        
 
        // Clear old UI.
        ClearUI();

        setStorageQty(listIndex,cardDataList);

        _DeckOrStorage = listIndex;

        if (listIndex == 0)
        {
            _pageList = myStorageCollection.Count;
        }
        else
        {
            _pageList = myDeckCollection.Count;
        }

        /*
        if (!_isInit)
            Init();
        */

        _listIndex = listIndex;
        _onClickCallback = onClickCallback;
        //_onClickEvent = OnClickEvent;
       // _onDropCardCallback = onDropCardCallback;

        _cardDataList = cardDataList;
        if (_cardDataList == null)
        {
            _cardDataList = new List<PlayerCardData>();
        }

        if (_pageIndex <= 0)
        {
            _pageIndex = 0;
        }
        else if (_pageIndex >= MaxPageIndex)
        {
            _pageIndex = MaxPageIndex;
        }
        else
        {
            _pageIndex = _pageIndex;
        }

        _myCollection = new Dictionary<string, CardData>();

        int index = 0;

        foreach (CardData data in myCollection)
        {
            
            _myCollection.Add(_cardDataList[index].CardID, data);
            ++index;
            //GameAnalytics.NewProgressionEvent(progressionStart,"Card Collection",30);
        }

        foreach (FullCardUI2 fullCardUI in FullCardList)
        {
            if (listIndex == 0)
            {
                fullCardUI.BindOnClickCallback(this.OnClickedPreviewStash); 
            }
            else
            {
                fullCardUI.BindOnClickCallback(this.OnClickedPreviewDeck);
            }

            //fullCardUI.BindOnClickCallback(this.OnClickEvent());
            //fullCardUI.BindOnClickCallback(this.OnClickUI);
        }

                                                           
       // Debug.Log("page index = " + _pageIndex + " deck count = " + _cardDataList.Count);
        ShowPage(_pageIndex);

    }

    private void ShowPage(int pageIndex)
    {

        if (_cardDataList != null)
        {

            int startIndex = pageIndex * CardPerPage;
            int stopIndex = (startIndex + (CardPerPage - 1));

            // Debug.Log("start = " + startIndex + " stopindex " + stopIndex);

            for (int index = startIndex; index <= stopIndex; ++index)
            {
                // CardData cardData;

                // Debug.Log("index = " + index + " " + _cardDataList.Count);
                if (_DeckOrStorage == 0) //storage
                {
                    if (index < myStorageCollection.Count)
                    {
                   
                        FullCardList[index - startIndex].SetData(new PlayerCardData(myStorageCollection[index].cardData));// cardData));

                        if (myStorageCollection[index].Qty > 1)
                        {
                            FullCardList[index - startIndex].QtyText.text = "x" + myStorageCollection[index].Qty.ToString();
                            FullCardList[index - startIndex].QtyText.gameObject.SetActive(true);
                        }
                        else
                        {
                            FullCardList[index - startIndex].QtyText.gameObject.SetActive(false);
                        }

                        FullCardList[index - startIndex].gameObject.SetActive(true);
                    }
                    else
                    {
                        // last page
                        FullCardList[index - startIndex].gameObject.SetActive(false);

                    }
                }

                else //deck
                {
                    if (index < myDeckCollection.Count)
                    {

                        FullCardList[index - startIndex].SetData(new PlayerCardData(myDeckCollection[index].cardData));// cardData));

                        if (myDeckCollection[index].Qty > 1)
                        {
                            FullCardList[index - startIndex].QtyText.text = "x" + myDeckCollection[index].Qty.ToString();
                            FullCardList[index - startIndex].QtyText.gameObject.SetActive(true);
                        }
                        else
                        {
                            FullCardList[index - startIndex].QtyText.gameObject.SetActive(false);
                        }

                        FullCardList[index - startIndex].gameObject.SetActive(true);
                    }
                    else
                    {
                        // last page
                        FullCardList[index - startIndex].gameObject.SetActive(false);

                    }
                }
               

            }

            // PageLabel.text = (_pageIndex + 1).ToString() + "/" + (_maxPageIndex + 1).ToString();

        }

        else //not found stash or deck ,so hide default cardui
        {
            for (int i = 0; i < 7; ++i)
            {
                FullCardList[i].gameObject.SetActive(false);
            }
        }

        PageLabel.text = (_pageIndex + 1).ToString() + "/" + (_maxPageIndex + 1).ToString();


        UpdateQtyLabel();
        UpdatePageLabel();

        //NGUITools.BringForward(gameObject);
    }


    private void ShowPageOld2(int pageIndex)
    {
       
        if (_cardDataList != null)
        {

            int startIndex = pageIndex * CardPerPage;
            int stopIndex = (startIndex + ( CardPerPage - 1 ));

           // Debug.Log("start = " + startIndex + " stopindex " + stopIndex);

            for (int index = startIndex; index <= stopIndex; ++index)
            {
               // CardData cardData;

               // Debug.Log("index = " + index + " " + _cardDataList.Count);

                if (index < _cardDataList.Count)
                {
                   // Debug.Log("index = " + index + " card count  " + _cardDataList.Count);
                   // Debug.Log("check card id = " + _cardDataList[index].CardID);

                    //if (_myCollection.TryGetValue(_cardDataList[index].CardID, out cardData))
                    //{
                        // Found data
                        //Debug.Log("found card id = " + _cardDataList[index].CardID);
                    FullCardList[index - startIndex].SetData(new PlayerCardData(_cardDataList[index].CardData));// cardData));
                    FullCardList[index - startIndex].gameObject.SetActive(true);
                    //}

                    //else
                    //{
                    ////    // not found
                    //    Debug.Log("not found card id = " + _cardDataList[index].CardID);
                    ////    FullCardList[index - startIndex].gameObject.SetActive(false);
                    //}
                }
                else
                {
                    // last page
                    FullCardList[index - startIndex].gameObject.SetActive(false);

                }

            }

           // PageLabel.text = (_pageIndex + 1).ToString() + "/" + (_maxPageIndex + 1).ToString();

        }

        else //not found stash or deck ,so hide default cardui
        {
            for (int i = 0; i < 7;++i)
            {
                FullCardList[i].gameObject.SetActive(false);
            }
        }

        PageLabel.text = (_pageIndex + 1).ToString() + "/" + (_maxPageIndex + 1).ToString();


        UpdateQtyLabel();
        UpdatePageLabel();

        //NGUITools.BringForward(gameObject);
    }



    private void ShowPageOld(int pageIndex)
    {
        if (_cardDataList != null)
        {
            
            int startShowIndex = pageIndex * CardPerPage;
            int endShowIndex = (startShowIndex + CardPerPage);

            if (endShowIndex > _cardDataList.Count)
            {
                endShowIndex = _cardDataList.Count;
            }

            int showCount = endShowIndex - startShowIndex;

            _cardUIList.Clear();
            foreach (Transform child in Table.transform)
            {
                CardEditorUINew ui = child.GetComponent<CardEditorUINew>();
                _cardUIList.Add(ui);
            }

            if (_cardUIList.Count < showCount)
            {
                while (_cardUIList.Count < showCount)
                {
                    // Create new card obj.
                    GameObject obj = Instantiate(CardEditorUIPrefab) as GameObject;
                    obj.transform.SetParent(Table.transform);
                    obj.transform.localScale = Vector3.one;
                   // NGUITools.SetLayer(obj, Table.gameObject.layer);

                    _cardUIList.Add(obj.GetComponent<CardEditorUINew>());
                }
            }

            Debug.Log("_cardUIList.Count " + _cardUIList.Count );
            if (_cardUIList.Count > showCount)
            {
                try
                {
                    
                    while (_cardUIList.Count > showCount)
                    {
                        Debug.Log("_cardUIList.Count > showCount here!");
                        GameObject obj = _cardUIList[0].gameObject;
                        _cardUIList.RemoveAt(0);

                        // NGUITools.Destroy(obj);
                    }
                }
                catch (System.Exception e)
                {
                    Debug.Log(e.ToString());
                }
              
            }

            try
            {
                int cardDataIndex = startShowIndex;
                for (int cardUIIndex = 0; cardUIIndex < _cardUIList.Count; ++cardUIIndex)
                {
                    if (cardDataIndex < endShowIndex)
                    {
                        _cardUIList[cardUIIndex].SetData(
                              CardListIndex
                            , cardDataIndex
                            , _cardDataList[cardDataIndex]
                            , this.OnClickUI
                        );

                        _cardUIList[cardUIIndex].gameObject.SetActive(true);
                        ++cardDataIndex;
                    }
                    else
                    {
                        _cardUIList[cardUIIndex].gameObject.SetActive(false);
                    }
                }
            }

            catch(System.Exception e)
            {
                Debug.Log(e.Message.ToString());
            }


        }

        Table.transform.SetAsFirstSibling();


        UpdateQtyLabel();
        UpdatePageLabel();

        //NGUITools.BringForward(gameObject);
    }

    void ShowCurrentPage()
    {
        int startIndex = _pageIndex * MaxCardPerPage;
        int stopIndex = startIndex + (MaxCardPerPage - 1);

        for (int index = startIndex; index <= stopIndex; ++index)
        {
            CardData cardData;

            if (index < _cardDataList.Count)
            {
                if (_myCollection.TryGetValue(_cardDataList[index].CardID, out cardData))
                {
                    // Found data
                    FullCardList[index - startIndex].SetData(new PlayerCardData(cardData));

                    FullCardList[index - startIndex].gameObject.SetActive(true);
                                   }
                else
                {
                    // not found
                    FullCardList[index - startIndex].gameObject.SetActive(false);
                  
                }
            }
            else
            {
                // last page
                FullCardList[index - startIndex].gameObject.SetActive(false);
      
            }
        }

        PageLabel.text = (_pageIndex + 1).ToString() + "/" + (_maxPageIndex + 1).ToString();

        //PercentLabel.text = Localization.Get("COLLECTION_PROGESS") + " " + (((float)_myCollection.Count / (float)_database.Count) * 100.0f).ToString("0.0") + " %";
    }



    public void UpdateUI()
    {
        if (_pageIndex <= 0)
        {
            _pageIndex = 0;
        }
        else if (_pageIndex >= MaxPageIndex)
        {
            _pageIndex = MaxPageIndex;
        }

        ShowPage(_pageIndex);

        Init();
    }

    private void UpdateQtyLabel()
    {
        int count = 0;

        if (_cardDataList != null)
        {
            count = _cardDataList.Count;
        }

        if (MaxDeck > 0)
        {
            CardQtyLabel.text = count.ToString() + "/" + MaxDeck.ToString();
        }
        else
        {
            CardQtyLabel.text = count.ToString();
        }
    }

    private void UpdatePageLabel()
    {
        PageLabel.text = string.Format(
            "{0}/{1}"
            , PageIndex + 1
            , MaxPageIndex + 1
        );
    }

    public void ReIndexUI()
    {
        _cardUIList.Clear();

        /*
        foreach (Transform child in Table.transform)
        {
            _cardUIList.Add(child.GetComponent<CardEditorUINew>());
        }
        */
    }
    #endregion 

    #region Event Methods
    public void OnClickPrev()
    {
        --_pageIndex;
        ShowPage(_pageIndex);
    }

    public void OnClickNext()
    {
        ++_pageIndex;
        ShowPage(_pageIndex);
    }

    private void OnClickUI(PlayerCardData data)
    {
        if (_onClickCallback != null)
        {
            _onClickCallback.Invoke(data);
            Debug.Log("OnclickUI: card =  " + data.CardID);
        }
    }


    void OnClickedPreviewDeck(FullCardUI2 fullCardUI)
    {
        FullCard_L.SetData(fullCardUI.Data);
        _bFromDeck = true;
        _bFromStash = false;
        this.TextCardIDToSwap.text = string.Concat("S", fullCardUI.Data.CardID);
        // this.TextMoveDeckOrStash.text = "Move to Stash";
        this.TextMoveDeckOrStash.text = Localization.Get("DECK_EDITOR_MOVE_TO_STASH_BUTTON");
        ShowCardUIView.SetActive(true);
    }

	void OnClickedPreviewStash(FullCardUI2 fullCardUI)
    {
        FullCard_L.SetData(fullCardUI.Data);
        _bFromDeck = false;
        _bFromStash = true;
        this.TextCardIDToSwap.text = string.Concat("D", fullCardUI.Data.CardID);
        // this.TextMoveDeckOrStash.text = "Move to Deck"; 
        this.TextMoveDeckOrStash.text = Localization.Get("DECK_EDITOR_MOVE_TO_DECK_BUTTON");
        ShowCardUIView.SetActive(true);

        //GameAnalytics.NewProgressionEvent(progressionStart,"Start Card Collection",30);
        //GameAnalytics.print ("From Analytics!");
        //GameAnalytics.NewDesignEvent ("PreviewCard");
        //GameAnalytics.SetGender (GAGender.Male);

//         FullCard_L.SetData(fullCardUI.Data);
        //ShowCardUIView.SetActive(true);

        //try remove card after click
        //Debug.Log("Count = " + _myCollection.Count + " Remove card At " + fullCardUI.Data.Name_EN + fullCardUI.Data.CardID);
        //_myCollection.Remove(fullCardUI.Data.CardID);
        //Debug.Log("after delete " + _myCollection.Count);
        //ShowCurrentPage();
        //ReloadCollectionData();

        // BattleUIManager.RequestBringForward(ShowCardUIView.gameObject);
    }

    public void onClosePreview()
    {
        ShowCardUIView.SetActive(false);
    }

    public void onClickFromStash()
    {
        Debug.Log("from stash!");
    }
	

	public void OnDropCard(int cardDataIndex)
    {
        if (_onDropCardCallback != null)
        {
            _onDropCardCallback.Invoke(_listIndex, cardDataIndex);
            Debug.Log("OnDropCard : Dropped object was: " + cardDataIndex);
        }
        Debug.Log("on drop card here");
    }

    public void OnDrop(PointerEventData eventData)
    {
        if (eventData.pointerDrag != null)
        {
            Debug.Log("OnDrop : Dropped object was: " + eventData.pointerDrag);

            CardEditorUI cardUI = eventData.pointerDrag.GetComponent<CardEditorUI>();
            OnDropCard(cardUI.CardDataIndex);
        }
    }
    #endregion
}
