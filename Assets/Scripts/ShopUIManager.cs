﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Firebase.Auth;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using UnityEngine.UI;
using UnityEngine.Purchasing;

public class ShopUIManager : MonoBehaviour {

    public int NormalPrice = 150; // 150 silver
    public int GoldPrice = 15; //15 gold
    int amountOfGold = 0;
    int completeGoldAdd = 0;
    string logText = "";

    #region Public UI
    public PlayerGoldUI goldUI;
    public ShopPackUI shopPackUI;

    //public Button buttonNormalPack;
    //public Button buttonPremiumPack;

    #endregion 

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ShowUI()
    {
        TopBarManager.Instance.SetUpBar(
          true
            , Localization.Get("MAINMENU_SHOP_TITLE") //"SHOP"
          , MenuManagerV2.Instance.ShowMainMenu
      );
        gameObject.SetActive(true);
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }

}
