﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System;
using Firebase.Auth;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using UnityEngine.Events;
using UnityEngine.UI;

public class MenuManagerV2 : MonoBehaviour
{
    private static MenuManagerV2 _instance = null;
    public static MenuManagerV2 Instance { get { return _instance; } }

    #region private_section
    private enum eAudioClips : int
    {
        mainmenu = 0
    , campaign
    , jinglebell
    }

    // Use this for initialization
    static Dictionary<string, int> card = new Dictionary<string, int>();
    StashFirebaseData stashdb;
    private string _accType = "";
    private string userID;
    UserDBData data = new UserDBData();


    private string _email;
    private string _password;
    private bool _isAutoLogin = false;

    private string _titleScene = "TitleScene2";
    private string _mainMenuScene = "MainMenuScene2";

    #endregion

    #region public_section
    public MainMenuUI MainMenuUI;
    public BattleUI BattleUI;
    public SinglePlayerUI SinglePlayerUI;
    public NPCDetailUINew NPCDetailUINew;
    public PlayerDeckCollectionUI PlayerDeckCollectionUI;
    public PlayerCardCollectionManager PlayerCardCollectionUI;
    public DeckEditorManagerNew DeckEditorManagerNew;
    public ShopUIManager ShopUI;
    public ShopPackUI ShopPackUI;
    public GameSettingUI SettingUI;
    public PlayerGoldUI playerGoldUI;
    public CreditUI CreditUI;
    public TutorialMenuUINew TutorialMenuUINew;
    public AnnouncementUINew AnnouncementUINew;
    public PopupBoxUINew PopUpBoxUINew;

    //for instance register from guest
    public MessageBoxUI MessageBoxUI;
    public RegisterUI RegisterUI;
    public LoginUI LoginUI;
    public GameObject LoadingUI;
    public ForgotPasswordUI ForgotPasswordUI;

    //confirm box
    // public ConfirmBox ConfirmBoxUI;

    //public GameObject ShopUI;
    public bool ForcePlayFirstTime = false;
    public List<string> PopUpMessageList;
    public List<string> PopUpButtonList;
    public Text Version;
    #endregion

    #region Tutorial
    //private bool testBool = false; 
    private string Tutorial1 = "T0001"; //First 3 Message before campaign
    private string Tutorial2 = "T0002"; //get first deck message
    private string Tutorial3 = "T0003"; //tell to open Campaign  
    private string Tutorial4 = "T0004"; //in Campaign Message
    private string Tutorial5 = "T0005"; //tell to select davox

    public List<Image> FirstFlowButtonToHide;
    //for tutorial database
    private List<TutorialData> _tutorialDataList = null;
    #endregion

    #region Popup UI Methods
    public void ShowErrorVersionPopUp(string header, string message, string buttonText)
    {
        PopUpBoxUINew.ShowMessageUI(
             message
           , buttonText
           , () =>
           {
               {
                   this.ClosePopUpUI();
               }
           }
           , false
       );
    }

    public void ShowErrorVersionPopUp(string header, string message, string buttonText, UnityAction onClick)
    {
        PopUpBoxUINew.ShowMessageUI(
             message
           , buttonText
           , delegate () { if (onClick != null) onClick.Invoke(); }
           , false
       );
    }

    public void ShowPopupUI(int index)
    {
        TutorialData data = _tutorialDataList[index++];

        string title = "";
        string message = "";
        string buttonText = "";
        if (LocalizationManager.IsCurrentLanguage(LocalizationManager.Language.Thai))
        {
            title = data.TitleTh;
            message = data.TextTh;
        }
        else
        {
            title = data.TitleEng;
            message = data.TextEng;
        }

        buttonText = Localization.Get(data.ButtonKey);

        if (string.Compare(data.DisplayMode, "DIALOG") == 0)
        {
            PopUpBoxUINew.ShowMessageUI(
                  message
                , buttonText
                , () =>
                {
                    if (index < _tutorialDataList.Count)
                    {
                        ShowPopupUI(index);
                    }
                    else
                    {
                        this.ClosePopUpUI();
                    }
                }
                , false
            );
        }
        else
        {
            PopUpBoxUINew.ShowImageUI(
                  data.ImagePath
                , title
                , data.PageText
                , message
                , buttonText
                , () =>
                {
                    if (index < _tutorialDataList.Count)
                    {
                        ShowPopupUI(index);
                    }
                    else
                    {
                        this.ClosePopUpUI();
                    }
                }
                , false
            );
        }

        PopUpBoxUINew.gameObject.SetActive(true);
    }


    public void ClosePopUpUI()
    {
        PopUpBoxUINew.gameObject.SetActive(false);
    }
    #endregion

    #region RegisterUI Methods
    private void ShowRegisterUI()
    {
        RegisterUI.ShowUI(OnRegisterUIClickCreate, OnRegisterUIClickCancel);
    }

    private void OnRegisterUIClickCreate(string email, string password)
    {
        if (string.IsNullOrEmpty(email) && string.IsNullOrEmpty(password))
        {
            ShowMessageBox(
                  "Error"
                , "Please Enter Email and Password"
                , "Close"
                , ShowRegisterUI
            );
        }
        else if (string.IsNullOrEmpty(email))
        {
            ShowMessageBox(
                  "Error"
                , "Please Enter Email"
                , "Close"
                , ShowRegisterUI
            );
        }
        else if (string.IsNullOrEmpty(password))
        {
            ShowMessageBox(
                  "Error"
                , "Please Enter Password"
                , "Close"
                , ShowRegisterUI
            );
        }
        else
        {
            Debug.Log(string.Format("Start signup Email : {0}", email));
            SignUp(email, password);
        }
    }

    private void OnRegisterUIClickCancel()
    {
        //ShowLoginUI();
        RegisterUI.HideUI();
    }

    private void OnRegisterUIClickCancelFromGuest()
    {

        RegisterUI.HideUI();
    }
    #endregion

    #region MessageBoxUI Methods
    private void ShowMessageBox(string header, string message, string buttonText, UnityAction onClickButton)
    {
        MessageBoxUI.ShowUI(header, message, buttonText, onClickButton);
    }
    #endregion

    #region SignUp Methods
    public void SignUp(string email, string password)
    {
        // ResultText.text = PasswordIn.value + " " + EmailInput.text.ToString() + " " + UserNameInput.text.ToString();
        try
        {
            if (!KOSServer.Instance.IsProcessSignUp)
            {
                _email = email;
                _password = password;
                KOSServer.Instance.SignUp(email, password, email, this.OnSignUpCompleted, this.OnSignUpFailed);
            }
        }
        catch (FirebaseException e)
        {
            ShowMessageBox(
                  "Error Signup"
                , e.Message
                , "Close"
                , ShowRegisterUI
            );
        }
    }

    private void OnSignUpCompleted(string email)
    {
        ShowMessageBox(
              "Success"
            , "Your KOS account has been created"
            , "START GAME"
            , () =>
            {
                Login(email, _password);
            }
        );
    }

    private void OnSignUpFailed(string errorMessage)
    {
        ShowMessageBox(
             "Signup Failed"
           , errorMessage
           , "Close"
           , ShowRegisterUI
       );
    }
    #endregion

    #region Login Methods
    private void Login(string email, string password)
    {
        try
        {
            if (!KOSServer.Instance.IsProcessLogin)
            {
                KOSServer.Instance.Login(email, password, this.OnLoginCompleted, this.OnLoginFailed);
            }
        }
        catch (FirebaseException e)
        {
            ShowMessageBox(
                  "Error Login"
                , "Please correct your email or password"
                , "Close"
                , ShowLoginUI
            );
        }

    }

    private void OnLoginCompleted()
    {

        ShowMessageBox(
              "Success"
            , "You successfully logged in to the game"
            , "START GAME"
            , LoadMainMenu
        );

    }

    private void OnLoginFailed(string errorMessage)
    {
        ShowMessageBox(
              "Login Failed"
            , errorMessage
            , "Close"
            , ShowLoginUI
        );
    }
    #endregion

    #region LoginUI Methods
    private void ShowLoginUI()
    {
        LoginUI.ShowUI(OnLoginUIClickLogin, OnLoginUIClickCancel, OnLoginUIClickCreate, OnLoginUIClickForgot);
    }

    private void OnLoginUIClickLogin(string email, string password)
    {
        if (string.IsNullOrEmpty(email) && string.IsNullOrEmpty(password))
        {
            ShowMessageBox(
                  "Error"
                , "Please Enter Email and Password"
                , "Close"
                , ShowLoginUI
            );
        }
        else if (string.IsNullOrEmpty(email))
        {
            ShowMessageBox(
                  "Error"
                , "Please Enter Email"
                , "Close"
                , ShowLoginUI
            );
        }
        else if (string.IsNullOrEmpty(password))
        {
            ShowMessageBox(
                  "Error"
                , "Please Enter Password"
                , "Close"
                , ShowLoginUI
            );
        }
        else
        {
            Debug.Log(string.Format("Start login Email : {0}", email));
            _isAutoLogin = false;
            Login(email, password);
        }
    }

    private void OnLoginUIClickCancel()
    {
        LoginUI.HideUI();
    }

    private void OnLoginUIClickCreate()
    {
        ShowRegisterUI();
    }

    private void OnLoginUIClickForgot()
    {
        ShowForgotPasswordUI();
    }
    #endregion

    #region ForgotPasswordUI Methods
    private void ShowForgotPasswordUI()
    {
        ForgotPasswordUI.ShowUI(OnForgotPasswordUIClickSend, OnForgotPasswordUIClickCancel);
    }

    private void OnForgotPasswordUIClickSend(string email)
    {
        ForgetPassword(email, ShowLoginUI, ShowForgotPasswordUI);
    }

    private void OnForgotPasswordUIClickCancel()
    {
        ShowLoginUI();
    }
    #endregion

    #region ForgetPassword Method
    private void ForgetPassword(string email, UnityAction onSuccess, UnityAction onFail)
    {
        //Debug.Log("ForgetPassword Email = " + email);

        FirebaseAuth auth = FirebaseAuth.DefaultInstance;

        if (!string.IsNullOrEmpty(email))
        {
            auth.SendPasswordResetEmailAsync(email).ContinueWith(task =>
            {
                if (task.IsCanceled)
                {
                    ShowMessageBox(
                          "Forget Password"
                        , "Password reset has been canceled."
                        , "Close"
                        , onFail
                    );
                    //Debug.LogError("SendPasswordResetEmailAsync was canceled.");
                    return;
                }
                if (task.IsFaulted)
                {
                    ShowMessageBox(
                          "Forget Password"
                        , "Password reset has failed. " + task.Exception
                        , "Close"
                        , onFail
                    );
                    //Debug.LogError("SendPasswordResetEmailAsync encountered an error: " + task.Exception);
                    return;
                }
                if (CheckEmail.IsEmail(email))
                {
                    ShowMessageBox(
                          "Forget Password"
                        , "Password reset email sent successfully."
                        , "Close"
                        , onSuccess
                    );
                    //Debug.Log("Password reset email sent successfully.");
                }
                else
                {
                    ShowMessageBox(
                          "Forget Password"
                        , "Password reset has unknown error."
                        , "Close"
                        , onFail
                    );
                    //Debug.LogError("SendPasswordResetEmailAsync encountered an error.");
                }
            });
        }
        else
        {
            ShowMessageBox(
                  "Forget Password"
                , "Please enter an email address"
                , "Close"
                , onFail
            );
            //Debug.Log("no email");        
        }
    }
    #endregion


    #region LoadingUI Methods
    public void ShowLoading()
    {
        LoadingUI.gameObject.SetActive(true);
    }

    public void HideLoading()
    {
        LoadingUI.gameObject.SetActive(false);
    }
    #endregion

    #region Logout
    public void Logout()
    {

        if (KOSServer.Instance.IsLogin)
        {
            PlayerSave.ClearAllSave();
            KOSServer.Instance.Logout();
            LoadSceneTitle();
        }
    }
    #endregion

    #region LoadSceneTitle

    private void LoadSceneTitle()
    {
        ShowLoading();
        SceneManager.LoadSceneAsync(_titleScene);
    }

    #endregion



    #region LoadMainMenu

    private void LoadMainMenu()
    {
        ShowLoading();

        if (!_isRequestCacheData)
        {
            /*
            #if UNITY_EDITOR
            OnCacheDataCompleted();
            #else
            */
            if (KOSServer.Instance.IsLogin)
            {
                StartCoroutine(_StartCacheData(0.1f));
            }
            else
            {
                OnCacheDataCompleted();
            }
            //#endif
        }
    }

    private IEnumerator _StartCacheData(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        RequestCacheData(OnCacheDataCompleted, OnCacheDataFailed);
    }

    private void OnCacheDataCompleted()
    {
        StartCoroutine(_LoadMainMenu());
    }

    private void OnCacheDataFailed(string error)
    {
        // retry.
        ShowMessageBox(
              "Error"
            , error
            , "Retry"
            , () => { StartCoroutine(_StartCacheData(3.0f)); }
        );
    }

    private IEnumerator _LoadMainMenu()
    {
        yield return new WaitForSeconds(0.1f);

        DBManager.Instance.InitDBAsync(delegate ()
        {
            //_endTime = Time.time;
            //float playTime = _endTime - _startTime;
            //playTime = playTime / 60.0f;
            // GameAnalytics.NewDesignEvent("Achievement:EndTitle(Min)", playTime);

            SceneManager.LoadSceneAsync("MainMenuScene2");
        });
    }

    #endregion

    #region CacheData Methods
    private bool _isRequestCacheData = false;
    private UnityAction _onCacheSuccess = null;
    private UnityAction<string> _onCacheFail = null;

    private void RequestCacheData(UnityAction onSuccess, UnityAction<string> onFail)
    {
        if (!_isRequestCacheData)
        {
            _isRequestCacheData = true;
            _onCacheSuccess = onSuccess;
            _onCacheFail = onFail;

            Debug.Log("Start Cache data...");
            KOSServer.Instance.CacheData(this.OnCacheCompleted, this.OnCacheFailed);
        }
        else
        {
            if (onFail != null)
            {
                onFail.Invoke("Already request cache data.");
            }
        }
    }

    //private void OnCacheCompleted()
    //{
    //    Debug.Log("Cache Successful.");
    //    string text = "";
    //    try
    //    {
    //        UserDBData data = KOSServer.Instance.PlayerData;
    //
    //        text = string.Format("Cache Successful.\n{0}", data.GetDebugPrintText());
    //        Debug.Log(text);
    //    }
    //    catch (System.Exception e)
    //    {
    //        Debug.Log(e.Message);
    //    }
    //
    //    _isRequestCacheData = false;
    //    if (_onCacheSuccess != null)
    //    {
    //        _onCacheSuccess.Invoke();
    //    }
    //}
    //
    //private void OnCacheFailed(string errorMessage)
    //{
    //    Debug.Log(errorMessage);
    //
    //    _isRequestCacheData = false;
    //    if (_onCacheFail != null)
    //    {
    //        _onCacheFail.Invoke(errorMessage);
    //    }
    //}
    #endregion

    private void Awake()
    {
        _instance = this;
    }

    // Use this for initialization
    void Start()
    {
        //PlayerSave.SavePlayerSilverCoin(1000);
        //Debug.Log("hererere");
        //PlayerSave.SavePlayerAccountType(AccountType.KOS);

        TopBarManager.Instance.isFromMenu = true;

        if (KOSServer.Instance.IsKOSAccount())
        {
            RequestCacheData();
        }

        //ScreenDubugLog.Instance.Print("1");

        #if !UNITY_EDITOR
            #if UNITY_ANDROID || UNITY_IOS
            StartCheckVersion();
            #endif
        #endif
        
        this.ShowMainMenu();

        //this.ShowErrorVersionPopUp("Incorect Version", "Please update version", "Close");

        /*
        KOSServer.RequestBuyShopPack(
              2
            , "3RENwwBoCgTLkogxXc8ME3L4uS03"
            , delegate(bool isSuccess, string result){
                Debug.Log(result);
            }
        );
        */

        /*
        {
            //PlayerSave.ClearAllSave();
            PlayerSave.ClearSave(PlayerSave.SaveIndex.CardCollection);
            //test first time
            // Load default deck.
            DeckPackData pack = new DeckPackData("D0001");

            // Save to player local save.
            PlayerSave.SavePlayerDeck(pack.DeckData);

            //  Update new card to card collection to default.
            for (int index = 0; index < pack.DeckData.Count; ++index)
            {
                // Save new card to card collection.
                PlayerSave.SavePlayerCardCollection(pack.DeckData.GetCardAt(index).PlayerCardData.CardData.CardID);
            }
        }
        */
    }

    private void StartCheckVersion()
    {
        Version.text = "";
        KOSServer.RequestKOSVersion(OnGetVersionComplete);
    }
    
    private void OnGetVersionComplete(bool isSuccess, string version, bool isCheckVersion)
    { 
        if(isSuccess)
        {   
            // Success
            Version.text = version;
            
            Debug.Log(string.Format(
                  "Current[{0}] - Server[{1}]"
                , PlayerSave.GetBuildVersion()
                , version
            ));

            if(version != PlayerSave.GetBuildVersion() && isCheckVersion)
            {
                // invaid version
                
                this.ShowErrorVersionPopUp(
                      LocalizationManager.GetText("MENU_VERSION_OUTDATE")
                    , LocalizationManager.GetText("MENU_VERSION_OUTDATE_DESCRIPTION")
                    , LocalizationManager.GetText("MENU_VERSION_OUTDATE_BUTTON")
                    , delegate()
                    {
#if UNITY_ANDROID
                        Application.OpenURL("https://play.google.com/store/apps/details?id=com.lunarstudio.koscard");
#elif UNITY_IOS
                        Application.OpenURL("https://appsto.re/th/Ft6Gfb.i");
#else
                        Application.OpenURL("https://www.facebook.com/koscardgameth/");
#endif

                    }
                );
            }
            else
            {
                this.ShowMainMenu();
            } 
        }
        else
        {
            // Failed                        
            this.ShowErrorVersionPopUp(
                  "Error"
                , "Cannot connect server."
                , LocalizationManager.GetText("MENU_VERSION_OUTDATE_BUTTON")
                , delegate()
                {
#if UNITY_ANDROID
                    Application.OpenURL("https://play.google.com/store/apps/details?id=com.lunarstudio.koscard");
#elif UNITY_IOS
                    Application.OpenURL("https://appsto.re/th/Ft6Gfb.i");
#else
                    Application.OpenURL("https://www.facebook.com/koscardgameth/");
#endif

                }
            );
        }
    }

    public void CloseAnnouncement()
    {
        AnnouncementUINew.HideUI();
    }

    public void UpdateTopBarText()
    {
        TopBarManager.Instance.UpdateSilverAmount(PlayerSave.GetPlayerSilverCoin());
        TopBarManager.Instance.UpdateGoldAmount(PlayerSave.GetPlayerGoldCoin());
    }

    public void RequestCacheData()
    {
        //ResultText.text = "Start Cache data...";

        KOSServer.Instance.CacheData(this.OnCacheCompleted, this.OnCacheFailed);
    }

    public void OnCacheCompleted()
    {
        //ErrorText.text = "Cache Successful.";
        string text = "";
        try
        {
            data = KOSServer.Instance.PlayerData;

            text = string.Format("Cache Successful.\nUID : {0}\nSilver Coin : {1}\njson : {2}",
                data.uid,
                data.UsersilverCoin,
                data.UserName
            );

            TopBarManager.Instance.ShowPlayerNameLabel(data.UserName);
            Debug.Log("texter = " + text);
            //ResultText.text = text;
            //AddSilverAmt.text = data.UsersilverCoin.ToString();
            //silverCoin = data.UsersilverCoin;
        }
        catch (System.Exception e)
        {
            string resultMsg = "";
            resultMsg = "E01 Connection error : User Profile " + e.Message.ToString(); ;
            Debug.Log("login cached = " + resultMsg);
            //ShowResult(resultMsg);
            //ResultText.text = e.Message.ToString();
        }
    }

    public void OnCacheFailed(string errorMessage)
    {
        Debug.Log("menu cache faile = " + errorMessage);
        //ShowResult(errorMessage);
        //ResultText.text = errorMessage;
    }

	// Update is called once per frame
	void Update () 
    {	
	}

    private void OnDestroy()
    {
        _instance = null;
    }

    public void ShowMainMenu()
    {
        
        BattleUI.HideUI();
        SinglePlayerUI.HideUI();
        NPCDetailUINew.HideUI();
        PlayerDeckCollectionUI.HideUI();
        SettingUI.HideUI();
        ShopUI.HideUI();
        MainMenuUI.ShowUI();
        TopBarManager.Instance.isFromMenu = true;

		if (ForcePlayFirstTime || PlayerSave.IsPlayerFirstTime())
		{
			for (int index = 0; index < FirstFlowButtonToHide.Count;++index)
			{
				FirstFlowButtonToHide[index].gameObject.SetActive(true);
			}

			// Load first time tutorial
            _tutorialDataList = new List<TutorialData>();

            // Load Tutorial1
            _tutorialDataList.AddRange(LoadFirstFlowTutorial(Tutorial1, true));

            // Load Tutorial2
            _tutorialDataList.AddRange(LoadFirstFlowTutorial(Tutorial2, true));

            // Load Tutorial3
            _tutorialDataList.AddRange(LoadFirstFlowTutorial(Tutorial3, true));

            // Load Tutorial4
            //_tutorialDataList.AddRange(LoadFirstFlowTutorial(Tutorial4, true));

   //         // Load default deck.
   //         DeckPackData pack = new DeckPackData("D0001");

			//// Save to player local save.
            //PlayerSave.SavePlayerDeck (pack.DeckData);


            ////  Update new card to card collection to default.
            //for (int index = 0; index < pack.DeckData.Count; ++index) 
            //{
            //    // Save new card to card collection.
            //    PlayerSave.SavePlayerCardCollection (pack.DeckData.GetCardAt (index).PlayerCardData.CardData.CardID);
            //}
                
            ShowPopupUI (0);    
		}

		else
		{
			AnnouncementUI.IsShowAnnouncement = false;
            if (AnnouncementUI.IsShowAnnouncement)
            {
                Debug.Log("toshow announcement");
                AnnouncementUINew.ShowAnnouncementUI();
            }

            Debug.Log("current silver = " + PlayerSave.GetPlayerSilverCoin());
            UpdateTopBarText();
		}
    }

	public List<TutorialData> LoadFirstFlowTutorial(string tutorialCode, bool isFirstFlow = true)
    {
        // Load Tutorial1
        List<TutorialData> tutorialList = new List<TutorialData>();

        List<TutorialDBData> dataList;
        bool _isSuccess = TutorialDB.GetDataByCode(tutorialCode, out dataList);
        if (_isSuccess)
        {
            List<TutorialDBData> showList = new List<TutorialDBData>();

            // Filter not show in first flow.
            foreach (TutorialDBData data in dataList)
            {
                if (isFirstFlow)
                {
					Debug.Log("data = " + data.TextEng);
                    if (data.ShowInFirstFlow > 0)
                    {
                        showList.Add(data);
                    }
                }
                else
                {
                    showList.Add(data);
                }
            }

            int page = 0;
            foreach (TutorialDBData data in showList)
            {
                TutorialData tutorialData = new TutorialData(data, (++page).ToString() + "/" + showList.Count.ToString());
                tutorialList.Add(tutorialData);
            }
        }

        return tutorialList;
    }

    public void ShowAnnouncement()
    {
        
    }

    public void ShowBattleUI()
    {
        MainMenuUI.HideUI();
        SinglePlayerUI.HideUI();
        NPCDetailUINew.HideUI();
        
        BattleUI.ShowUI(ShowSinglePlayerUI, ShowLobbyScene, null);
        TopBarManager.Instance.isFromMenu = false;
    }

    public void ShowSinglePlayerUI()
    {
        BattleUI.HideUI();
        NPCDetailUINew.HideUI();
        SinglePlayerUI.ShowUI();
        TopBarManager.Instance.isFromMenu = false;
    }

    public void ShowNPCDetailUINew()
    {
        BattleUI.HideUI();
        SinglePlayerUI.HideUI();
    
        TopBarManager.Instance.isFromMenu = false;

    }

    public void ShowPlayerDeckCollectionUI()
    {
        PlayerDeckCollectionUI.ShowUI();
        PlayerCardCollectionUI.HideUI();
        DeckEditorManagerNew.HideUI();
        MainMenuUI.HideUI();
        TopBarManager.Instance.isFromMenu = false;
    }

    public void ShowDeckEditorManagerUI()
    {
        DeckEditorManagerNew.ShowUI();
        PlayerDeckCollectionUI.HideUI();
        MainMenuUI.HideUI();
        TopBarManager.Instance.isFromMenu = false;
    }

    public void ShowSettingUI()
    {
        SettingUI.ShowUI();
        CreditUI.HideUI();
        TutorialMenuUINew.HideUI();
        MainMenuUI.HideUI();
        TopBarManager.Instance.isFromMenu = false;
    }

    public void ShowShopUI()
    {
        
        ShopUI.ShowUI();
        MainMenuUI.HideUI();
        ShopPackUI.HideUI();
        TopBarManager.Instance.HideGoldUIFromShop();
        TopBarManager.Instance.isFromMenu = false;
    }


    public void ShowShopPackUI()
    {
        ShopPackUI.ShowUI();
        ShopUI.HideUI();
        TopBarManager.Instance.isFromMenu = false;
    }

    //public void ShowGoldShopUI()
    //{
    //    TopBarManager.Instance.isFromMenu = true;
    //    TopBarManager.Instance.OpenGoldFromShop();
    //    ShopUI.HideUI();
    //    TopBarManager.Instance.isFromMenu = false;
    //}

    public void ShowDeckEditor()
    {
        LoadScene("DeckEditorScene");
    }

    public void ShowCardCollection()
    {
        PlayerCardCollectionUI.ShowUI();
        PlayerDeckCollectionUI.HideUI();
        TopBarManager.Instance.isFromMenu = false;
        
        //TopBarManager.Instance.SetUpBar(
        //      true
        //    , "Deck & Collection"
        //    , MenuManagerV2.Instance.ShowMainMenu
        //);

        //gameObject.SetActive(true);
        //LoadScene("CardCollectionScene");
    }

    public void ShowCreditUI()
    {
        SettingUI.HideUI();
        CreditUI.ShowUI();
        TopBarManager.Instance.isFromMenu = false;
    }

    public void ShowTutorialMenu()
    {
        SettingUI.HideUI();
        TutorialMenuUINew.ShowUI();
        TopBarManager.Instance.isFromMenu = false;
    }

    public void ShowBattleScene()
    {
        LoadScene("BattleScene");
    }
    
    public void ShowLobbyScene()
    {
        LoadScene("LobbyScene");
    }
    
    public void ToggleLanguage()
    {
        LocalizationManager.ToggleLanguage();
        TopBarManager.Instance.UpdateTitle(Localization.Get("MAINMENU_SETTING_TITLE"));
    }

    public void LoadScene(string sceneName)
    {
        StartCoroutine(LoadingScene(sceneName));
    }

    public void SendFeedBack()
    {
        string email = "hello@koscardgame.com";
        string subject = MyEscapeURL("Feedback for KOS");
        string body = MyEscapeURL("");
        Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);
    }

    string MyEscapeURL(string url)
    {
        return WWW.EscapeURL(url).Replace("+", "%20");
    }

    IEnumerator LoadingScene(string sceneName)
    {
        //ShowLoading();
        yield return new WaitForSeconds(0.01f);
        SceneManager.LoadSceneAsync(sceneName);
    }
}
