﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpenPackUI : MonoBehaviour {

    #region Private Properties
    public List<FullCardUI2> FullCardList;
    public List<Image> GlowList;
    public List<Text> NewLabelList;
    public Text lblHeaderText;
    public GameObject ShowCardUIView;//added 09042018 vit
    public FullCardUI2 FullCard_L;    //added 09042018 vit
    #endregion

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ShowUI(string sAmtCard)
    {
        string headText = "";
        string phural = "s";

        if (sAmtCard == "1")
        {
            phural = "";
        }

        gameObject.SetActive(true);
        headText = string.Format(Localization.Get("SHOP_NEW_CARD_HEADER"), sAmtCard, phural);
        lblHeaderText.text = headText;

        foreach (FullCardUI2 card in FullCardList)
        {
            card.gameObject.SetActive(false);
        }

        //foreach (GameObject glow in GlowList)
        //{
        //    glow.SetActive(false);
        //}

        foreach (Text newLabel in NewLabelList)
        {
            newLabel.gameObject.SetActive(false);
        }

    }

    public void ShowCard(int index, PlayerCardData data, bool isGlow, bool isNew) //, FullCardUI2.OnClickEvent onClickedPreview)
    {
        //FullCardList[index].SetCardData(data);
        FullCardList[index].SetData(data);
        FullCardList[index].gameObject.SetActive(true);
        GlowList[index].gameObject.SetActive(isGlow);
        NewLabelList[index].gameObject.SetActive(isNew);
        FullCardList[index].BindOnClickCallback(this.OnClickedPreview);

        //FullCardList[index].RemoveAllOnClickCallback();
        //FullCardList[index].BindOnClickCallback(onClickedPreview);
    }

    void OnClickedPreview(FullCardUI2 fullCardUI)
    {
        //Debug.Log ("Name:" + fullCardUI.PlayerCardData.Name_EN);
        //GameAnalytics.NewProgressionEvent(progressionStart,"Start Card Collection",30);
        //GameAnalytics.print ("From Analytics!");
        //GameAnalytics.NewDesignEvent ("PreviewCard");
        //GameAnalytics.SetGender (GAGender.Male);

        FullCard_L.SetData(fullCardUI.Data);
        ShowCardUIView.SetActive(true);

        // BattleUIManager.RequestBringForward(ShowCardUIView.gameObject);
    }


    public void HideUI()
    {
        gameObject.SetActive(false);
    }

}
