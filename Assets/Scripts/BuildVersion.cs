﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

[assembly: System.Reflection.AssemblyVersion("1.0.*")]
public class BuildVersion 
{
    private static System.Version Version() 
    {
        return Assembly.GetExecutingAssembly().GetName().Version;
    }
    
    private static System.DateTime Date() 
    {
        System.Version version = Version();
        System.DateTime startDate = new System.DateTime( 2000, 1, 1, 0, 0, 0 );
        System.TimeSpan span = new System.TimeSpan( version.Build, 0, 0, version.Revision * 2 );
        System.DateTime buildDate = startDate.Add( span );
        return buildDate;
    }
    
    public static string GetBuildTime() 
    {
        System.DateTime date = Date();
        
        return string.Format("{0}{1}{2}" //-{3}{4}{5}"
            , date.Year.ToString("0000")
            , date.Month.ToString("00")
            , date.Day.ToString("00")
            /*
            , date.Hour.ToString("00")
            , date.Minute.ToString("00")
            , date.Second.ToString("00")
            */
        );
    }
}
