﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(UITexture))]
public class BattleBGUI : MonoBehaviour 
{
	public List<Texture> BGTexture;

	/*
	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
	}
	*/

	public void SetBGTexture(int bgIndex)
	{
		UITexture bg = GetComponent<UITexture>();
		bg.mainTexture = BGTexture[bgIndex];

		NGUITools.ImmediatelyCreateDrawCalls(bg.gameObject);
	}
}
