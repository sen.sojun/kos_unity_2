﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class LocalizeText : MonoBehaviour, ILocalize
{
    #region Private Properties
    #endregion

    #region Public Properties
    public string LocalizeKey = "";
	#endregion

	#region Start
	private void Start()
	{
		OnUpdateLanguage();
	}
	#endregion
    
    #region Update
    /*
    private void Update()
    {
    }
    */
    #endregion
    
    void OnEnable()
    {
        OnUpdateLanguage();
    }
    
    public void OnUpdateLanguage()
    {
        this.GetComponent<Text>().text = LocalizationManager.GetText(LocalizeKey);
    }
}
