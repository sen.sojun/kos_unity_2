﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

public class WriteDataTask : MonoBehaviour 
{
    private bool _isUpdate = false;
    private Task _task = null;
    private UnityAction _onTaskComplete = null;
    private UnityAction _onTaskFailed = null;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
    {	
        if(_isUpdate)
        {
            if(_task != null)
            {
                if(_task.IsFaulted || _task.IsCanceled)
                {
                    OnTaskFailed();
                }
            
                if(_task.IsCompleted)
                {
                    OnTaskComplete();
                }
            }
        }
	}
    
    public void SetupTask(Task task, UnityAction onTaskComplete, UnityAction onTaskFailed)
    {
        _task = task;

        _onTaskComplete = onTaskComplete;
        _onTaskFailed = onTaskFailed;
        
        _isUpdate = true;
        DontDestroyOnLoad(gameObject);
    }
    
    private void OnTaskComplete()
    {
        _isUpdate = false;

        if (_onTaskComplete != null)
        {
            _onTaskComplete.Invoke();
        }
        
       
        
        Destroy(gameObject);
    }
    
    private void OnTaskFailed()
    {
        _isUpdate = false;

        if (_onTaskFailed != null)
        {
            _onTaskFailed.Invoke();
        }
        
       
        
        Destroy(gameObject);
    }
}
