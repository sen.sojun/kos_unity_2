﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

public class SelectGridUI : MonoBehaviour 
{
	public delegate void OnSelectCardEvent(PlayerCardData playerCardData);

	private OnSelectCardEvent _onSelectCardEvent = null;

	public UILabel HeaderLabel;
	public UIGrid Grid;
	public UIScrollView ScrollView;

	public GameObject FullCardPrefab;

	// Use this for initialization
	void Start () 
	{
        NGUITools.SetActive(gameObject, false);
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void OnEnable()
	{
		ResetScrollView();
	}

	void OnDisable()
	{
		RemoveAllCard();
	}

	public void ShowUI(string headerText, List<PlayerCardData> cardDataList, OnSelectCardEvent onSelectCardCallback = null)
	{
		HeaderLabel.text = headerText;
		_onSelectCardEvent = onSelectCardCallback;

		foreach(PlayerCardData playerCardData in cardDataList)
		{
			AddCard(playerCardData);	
		}
			
		NGUITools.SetActive(gameObject, true);
	}

	public void HideUI()
	{
		NGUITools.SetActive(gameObject, false);
	}

	public void AddCard(PlayerCardData playerCardData)
	{
		GameObject cardObj = NGUITools.AddChild(Grid.gameObject, FullCardPrefab);

		// set tranform
		cardObj.transform.parent = Grid.transform;
		cardObj.transform.localScale = Vector3.one;

		// set init data
		FullCardUI fullCard = cardObj.GetComponent<FullCardUI>();
		fullCard.SetCardData(playerCardData);
		fullCard.BindOnClickCallback(OnSelectCard);

		ResetScrollView();
		BattleUIManager.RequestBringForward(cardObj);
	}

	public void ResetScrollView()
	{
		Grid.Reposition();
		ScrollView.ResetPosition();
	}

	public void RemoveAllCard()
	{
		Grid.transform.DestroyChildren();
	}

	public void OnSelectCard(FullCardUI fullCard)
	{
		Debug.Log(fullCard.gameObject.name +  " : " + fullCard.name);

		if(_onSelectCardEvent != null)
		{
			_onSelectCardEvent.Invoke(fullCard.PlayerCardData);
		}
	}
}
