﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSettingUI : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ShowUI()
    {
        TopBarManager.Instance.SetUpBar(
               true
            , Localization.Get("MAINMENU_SETTING_TITLE") // "SETTINGS"
             , MenuManagerV2.Instance.ShowMainMenu
         );

        gameObject.SetActive(true);
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }
}
