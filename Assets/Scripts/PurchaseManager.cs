﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;

public class PurchaseManager : IAPListener 
{
    private bool _isSubscribe = false;
    private static PurchaseManager _instance = null;
    public static PurchaseManager Instance { get {return _instance; } }

	// Use this for initialization
	void Start () 
    {
        if(_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
	}
	
	// Update is called once per frame
	void Update () 
    {	
	}

	void OnEnable()
    {
        if(!_isSubscribe)
        {
            CodelessIAPStoreListener.Instance.AddListener(this);
            
            onPurchaseComplete.AddListener(OnPurchaseComplete);
            onPurchaseFailed.AddListener(OnPurchaseFail);
            
            _isSubscribe = true;
        }
        
        // Clear
        #if !UNITY_EDITOR
        UnityPurchasing.ClearTransactionLog();
        #endif
    }

    void OnDisable()
    {
        if(_isSubscribe)
        {
            CodelessIAPStoreListener.Instance.RemoveListener(this);
        
            onPurchaseComplete.RemoveListener(OnPurchaseComplete);
            onPurchaseFailed.RemoveListener(OnPurchaseFail);
        
            _isSubscribe = false;
        }
    }
    
    public void OnPurchaseDummyComplete()
    {
        Product product = null;
        
        OnPurchaseComplete(product);
    } 

	public void OnPurchaseComplete(Product product)
    {    
        if(product != null)
        {
            Debug.Log("PurchaseManager/OnPurchaseComplete: " + product.transactionID);    
        }
        PurchaseReceipt receipt = new PurchaseReceipt(
              product
            , PlayerSave.GetPlayerUid()
            , PlayerSave.GetPlayerEmail()
            , IsValidReceipt(product)
        );
        
        KOSServer.RequestSaveReceipt(
              receipt
            , delegate(){        
                KOSServer.RequestRedeemReceipt(
                      receipt
                    , PlayerSave.GetPlayerUid()
                    , delegate(bool isSuccess, string result) { 
                        OnRedeemReceiptComplete(isSuccess, result, product, receipt); 
                    }
                );        
            }
            , delegate(){        
                OnRedeemReceiptComplete(false, "", product, receipt);
            }
        );
    }
    
    private bool IsValidReceipt(Product product)
    {
        #if UNITY_ANDROID
        {
            bool isValid = product.transactionID.StartsWith("GPA");
            return isValid;
        }
        #elif UNITY_IOS
        {
            return true;    
        }
        #elif UNITY_EDITOR
        {
            return true;
        }
        #else
        {
            return false;
        }
        #endif        
    }
    
    private void OnRedeemReceiptComplete(bool isSuccess, string result, Product product, PurchaseReceipt receipt)
    {
        Debug.Log(string.Format(
            "OnRedeemReceiptComplete: isSuccess[{0}] result[{1}]"
            , isSuccess
            , result
        ));   
    
        if(isSuccess)
        {
            // request refresh player data.
            string[] texts = result.Split(' ');
            
            string complete = texts[0];
            if(complete == "1")
            {      
                int gold = int.Parse(texts[1]);
                
                Debug.Log("New gold : " + gold);                
                PlayerSave.SavePlayerGoldCoin(gold);                
                
                if(TopBarManager.Instance != null)
                {
                    TopBarManager.Instance.UpdateGoldAmount(PlayerSave.GetPlayerGoldCoin());
                }
                                
                CodelessIAPStoreListener.Instance.StoreController.ConfirmPendingPurchase(product);
            }
        }
    }
    
    public void OnPurchaseFail(Product product, PurchaseFailureReason reason)
    {
        Debug.Log("PurchaseManager/OnPurchaseFail: " + product.transactionID + " : " + reason.ToString());
    
        if(reason == PurchaseFailureReason.DuplicateTransaction)
        {
            OnPurchaseComplete(product);
        }
    }
}
