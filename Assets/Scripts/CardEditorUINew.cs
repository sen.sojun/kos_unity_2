﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardEditorUINew : UIDragDropItem  
{
    #region Delegates
    public delegate void OnClickCallback(PlayerCardData clickedCardData);
    #endregion

    #region Private Properties 
    private int _cardlistIndex = -1;
    private int _cardDataIndex = -1;
    private PlayerCardData _data = null;
    private OnClickCallback _onClickCallback = null;
    #endregion

    #region Private Properties 
    public PlayerCardData Data { get { return _data; } }
    #endregion

    #region Public Inspector Properties
   // public FullCardUI FullCardUI;
    public FullCardUI2 FullCardUI;
    #endregion

    #region Start
    // Use this for initialization
    void Start()
    {
    }
    #endregion

    #region Update
    // Update is called once per frame
    void Update()
    {
    }
    #endregion

    #region Methods
    public void SetData(PlayerCardData data, OnClickCallback onClickCallback)
    {
        _cardlistIndex = -1;
        _cardDataIndex = -1;
        _data = data;
        _onClickCallback = onClickCallback;
        //FullCardUI.BindOnClickCallback
        FullCardUI.SetData(data);
        //FullCardUI.RequestBringForward(false);
    }

    public void SetData(int cardListIndex, int cardDataIndex, PlayerCardData data, OnClickCallback onClickCallback)
    {
        _cardlistIndex = cardListIndex;
        _cardDataIndex = cardDataIndex;
        _data = data;
        _onClickCallback = onClickCallback;

        FullCardUI.SetData(data);
       // FullCardUI.RequestBringForward(false);
    }

    public void OnClick()
    {
        if (_onClickCallback != null)
        {
            _onClickCallback.Invoke(_data);
        }
    }

    protected override void OnDragDropRelease(GameObject surface)
    {
        //Debug.Log("OnDragDropRelease " + surface.name);

        if (surface != null)
        {
            CardEditorListUINew cardListUI = null;
            cardListUI = surface.GetComponent<CardEditorListUINew>();

            if (cardListUI == null)
            {
                cardListUI = surface.GetComponentInParent<CardEditorListUINew>();
            }

            if (cardListUI != null)
            {
                //Debug.Log("name: " + cardListUI.name);

                base.OnDragDropRelease(surface);
                if (this._cardlistIndex != cardListUI.CardListIndex)
                {
                    //Debug.Log(this._cardlistIndex + "," + cardListUI.CardListIndex);
                    cardListUI.OnDropCard(this._cardDataIndex);
                }

                // Destroy this icon as it's no longer needed
                //NGUITools.Destroy(gameObject);
                return;
            }
        }

        base.OnDragDropRelease(surface);
    }
    #endregion
}
