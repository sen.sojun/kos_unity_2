﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TutorialData 

{
	#region Private Properties
	private string _tutorialID;
	private string _tutorialCode;
	private string _displayMode;
	private string _imagePath;
	private int _showInFirstFlow;
	private string _buttonKey;

	private string _pageText;

	private string _titleEng;
	private string _titleTh;

	private string _textEng;
	private string _textTh;
	#endregion

	#region Public Properties
	public string TutorialID { get { return _tutorialID; } }
	public string TutorialCode { get { return _tutorialCode; } }
	public string DisplayMode { get { return _displayMode; } }
	public string ImagePath { get { return _imagePath; } }
	public int ShowInFirstFlow { get { return _showInFirstFlow; } }
	public string ButtonKey { get { return _buttonKey; } }

	public string PageText { get { return _pageText; } }

	public string TitleEng { get { return _titleEng; } }
	public string TitleTh { get { return _titleTh; } }

	public string TextEng { get { return _textEng; } }
	public string TextTh { get { return _textTh; } }
	#endregion

	#region Constructors
	public TutorialData ()
	{
		_tutorialID = "";
		_tutorialCode = "";
		_displayMode = "";
		_imagePath = "";
		_showInFirstFlow = 0;
		_buttonKey = "";

		_pageText = "";

		_titleEng = "";
		_titleTh = "";

		_textEng = "";
		_textTh = "";
	}

	public TutorialData(string tutorialID, string tutorialCode, string displayMode, string imagePath, int showInFirstFlow, string buttonKey, string pageText, string titleEng, string titleTh, string textEng, string textTh)
	{
		_tutorialID = tutorialID;
		_tutorialCode = tutorialCode;
		_displayMode = displayMode;
		_imagePath = imagePath;
		_showInFirstFlow = showInFirstFlow;
		_buttonKey = buttonKey;

		_pageText = pageText;

		_titleEng = titleEng;
		_titleTh = titleTh;

		_textEng = textEng;
		_textTh = textTh;
	}

	public TutorialData(TutorialDBData data, string pageText)
	{
		_tutorialID = data.TutorialID;
		_tutorialCode =  data.TutorialCode;
		_displayMode =  data.DisplayMode;
		_imagePath =  data.ImagePath;
		_showInFirstFlow =  data.ShowInFirstFlow;
		_buttonKey = data.ButtonKey;

		_pageText = pageText;

		_titleEng =  data.TitleEng;
		_titleTh =  data.TitleTh;

		_textEng =  data.TextEng;
		_textTh =  data.TextTh;
	}
	#endregion

	#region Relational Operator Overloading
	public static bool operator ==(TutorialData c1, TutorialData c2)
	{
		if((System.Object)c1 == null && (System.Object)c2 == null) return true;
		else if((System.Object)c1 == null || (System.Object)c2 == null) return false;
		else return (string.Compare(c1.TutorialID, c2.TutorialID) == 0);
	}

	public static bool operator !=(TutorialData c1, TutorialData c2)
	{
		return !(c1 == c2);
	}

	public override bool Equals(System.Object obj)
	{
		// Check for null values and compare run-time types.
		if (obj == null || GetType() != obj.GetType())
			return false;

		TutorialData c = (obj as TutorialData);
		return (string.Compare(TutorialID, c.TutorialID) == 0);
	}

	public override int GetHashCode()
	{
		return 0;
	}
	#endregion
}
