﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PurchaseResultUI : MonoBehaviour {

//	#region Delegates
//	public delegate void OnClickCallback(PlayerCardData clickData);
//	#endregion
//
//	#region Private Properties
//	private OnClickCallback _onClickCallback = null;
//	private int _iIndex = 0;
//	private Dictionary<string, ShopPackData> _shopList;
//	public int NormalPrice =  150; //500;
//	#endregion
//
//	#region Public Inspector Properties
//	public GameObject CardEditorUIPrefab;
//	public List<FullCardUI> FullCardList;
//	public GameObject ShowCardUIView;
//	public GameObject CardContainer; 
//	public FullCardUI FullCard_L;   
//	#endregion
//
//	#region Private
//	private int __currentSilverCoin = 0;
//
//
//	private int _currentSilverCoin
//	{
//		get { return __currentSilverCoin; }
//		set 
//		{
//			__currentSilverCoin = value;
//
//			UpdateSilverCoinLabel ();
//		}
//	}
//
//	private int __currentGoldCoin = 0;
//	private int _currentGoldCoin
//	{
//		get { return __currentGoldCoin; }
//		set 
//		{
//			__currentGoldCoin = value;
//
//			UpdateSilverCoinLabel ();
//		}
//	}
//	#endregion
//
//
//	public int MaxDeck = 0;
//
//	// Use this for initialization
//	void Start () {
//		_shopList = new Dictionary<string, ShopPackData> ();
//		_shopList.Add ("SP0001", new ShopPackData("SP0001"));
//	}
//	
//	// Update is called once per frame
//	void Update () {
//	
//	}
//
//	public void BuyStarterSet_5()
//	{
//		BuyStarterSet(NormalPrice);
//	}
//
//
//
//	private void RandomNormal(string shopPackID, out List<string> resultList)
//	{
//
//		ShopPackData shop = null;
//		bool isFound = _shopList.TryGetValue (shopPackID, out shop);
//		if (isFound) 
//		{
//			CardContainer.SetActive (true); //insert vit
//
//			shop.RandomNormal(out resultList);
//			_iIndex = 0;
//			foreach (string id in resultList) 
//			{
//				PlayerCardData card = new PlayerCardData (new CardData(id));
//
//				// add new card to stash
//				bool isSuccess = AddCardToStash (card.PlayerCardID);
//				if(isSuccess)
//				{
//					// save new card to collection
//					PlayerSave.SavePlayerCardCollection (card.PlayerCardID);
//
//					//insertion vit
//
//					FullCardList [_iIndex].SetCardData (new PlayerCardData (card));
//					FullCardList [_iIndex].gameObject.SetActive (true);
//
//					GameObject obj = Instantiate(CardEditorUIPrefab) as GameObject;
//					CardEditorUI cardUI = obj.GetComponent<CardEditorUI>();
//					cardUI.SetData(card, this.OnClickUI);
//
//					obj.name = string.Format("{0}{1}{2}"
//						, cardUI.Data.TypeData.CardTypeID
//						, cardUI.Data.SymbolData.CardSymbolID
//						, cardUI.Data.Name_EN
//					);
//					//ResetPosition(false);
//					//end insertion vit
//
//				}
//				++_iIndex;
//			}
//
//			return;
//		}
//
//		resultList = null;
//	}
//
//
//
//	private bool AddCardToStash(string playerCardID)
//	{
//		List<PlayerCardData> stash;
//		bool isSuccess = PlayerSave.GetPlayerCardStash(out stash);
//		if(isSuccess)
//		{
//			stash.Add(new PlayerCardData(playerCardID));
//			PlayerSave.SavePlayerCardStash (stash);
//
//			return true;
//		}
//		else
//		{
//			stash = new List<PlayerCardData>();
//			stash.Add(new PlayerCardData(playerCardID));
//			PlayerSave.SavePlayerCardStash (stash);
//
//			return true;
//		}
//
//		return false;
//	}
//
//	private void DebugPrintText(List<string> cardIDList)
//	{
//		string resultText = "";
//		foreach(string id in cardIDList)
//		{
//			CardData data = new CardData (id);
//			resultText += data.SymbolData.SymbolName_EN + " " + id + ":" + data.Name_EN + "\n";
//		}
//
//		Debug.Log (resultText);
//	}



}
