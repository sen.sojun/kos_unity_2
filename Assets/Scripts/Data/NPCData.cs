﻿using UnityEngine;
using System.Collections;

public class NPCData  
{
	#region Enums
	public enum NPCDifficulty : int
	{
		  Easy		= 0
		, Normal
		, Hard
		, Hell
	}
	#endregion

	#region Private Properties
	private string _npcID;
	private string _name_EN;
    private string _name_TH;
    private string _imageName;
	private string _battleImageName;
	private DeckData _deckData;
	private string _rewardCardID;
	private int _bgIndex;
	private NPCDifficulty _difficulty;
    private string _deckStyle_EN;
	private string _deckStyle_TH;
	private string _description_EN;
    private string _description_TH;
	#endregion

	#region Public Properties
	public string NPCID
	{
		get { return _npcID; }
	}
	public string Name_EN
	{
		get { return _name_EN; }
	}
    public string Name_TH
    {
        get { return _name_TH; }
    }
    public string ImageName
	{
		get { return _imageName; }
	}
	public string BattleImageName
	{
		get { return _battleImageName; }
	}
	public DeckData DeckData
	{
		get { return _deckData; }
	}
	public string RewardCardID
	{
		get { return _rewardCardID; }
	}
	public int BGIndex
	{
		get { return _bgIndex; }
	}
	public NPCDifficulty Difficulty
	{
		get { return _difficulty; }
	}
    public string DeckStyle_EN
    {
        get { return _deckStyle_EN; }
    }
	public string DeckStyle_TH
	{
		get { return _deckStyle_TH; }
	}
    public string Description_EN
    {
        get { return _description_EN; }
    }
	public string Description_TH
	{
        get { return _description_TH; }
	}
	#endregion

	#region Constructors
	public NPCData()
	{
		this._npcID = "";
		this._name_EN = "";
        this._name_TH = "";
        this._imageName = "";
		this._battleImageName = "";
		this._deckData = null;
		this._rewardCardID = "";
		this._bgIndex = 0;
		this._difficulty = NPCDifficulty.Easy;
        this._deckStyle_EN = "";
		this._deckStyle_TH = "";
        this._description_EN = "";
        this._description_TH = "";
	}

	public NPCData(string npcID)
	{
		NPCDBData npcDBData;
		bool isSuccess = NPCDB.GetData(npcID, out npcDBData);

		if (isSuccess)
		{
			DeckPackData deckPack = new DeckPackData(npcDBData.DeckID);
			if(deckPack.DeckID.Length > 0)
			{
				this._npcID = npcDBData.NPCID;
				this._name_EN = npcDBData.Name_EN;
                this._name_TH = npcDBData.Name_TH;
                this._imageName = npcDBData.ImageName;
				this._battleImageName = npcDBData.BattleImageName;
				this._deckData = deckPack.DeckData;
				this._rewardCardID = npcDBData.RewardCardID;
				this._bgIndex = npcDBData.BattleBGIndex;
				this._difficulty = (NPCDifficulty)npcDBData.Difficulty;
				this._deckStyle_EN = npcDBData.DeckStyle_EN;
				this._deckStyle_TH = npcDBData.DeckStyle_TH;
                this._description_EN = npcDBData.Description_EN;
                this._description_TH = npcDBData.Description_TH;

				return;
			}
		}

		string errorLog = string.Format("NPCDBData: npcID({0}) not found.", npcID);
		Debug.LogError(errorLog);

		this._npcID = "";
		this._name_EN = "";
        this._name_TH = "";
        this._imageName = "";
		this._battleImageName = "";
		this._deckData = null;
		this._rewardCardID = "";
		this._bgIndex = 0;
		this._difficulty = NPCDifficulty.Easy;
        this._deckStyle_EN = "";
		this._deckStyle_TH = "";
        this._description_EN = "";
        this._description_TH = "";
	}
	#endregion

	#region Relational Operator Overloading
	public static bool operator ==(NPCData c1, NPCData c2)
	{
		if((System.Object)c1 == null && (System.Object)c2 == null) return true;
		else if((System.Object)c1 == null || (System.Object)c2 == null) return false;
		else return (string.Compare(c1.NPCID, c2.NPCID) == 0);
	}
	public static bool operator !=(NPCData c1, NPCData c2)
	{
		return !(c1 == c2);
	}
	public override bool Equals(System.Object obj)
	{
		// Check for null values and compare run-time types.
		if (obj == null || GetType() != obj.GetType())
			return false;

		NPCData c = (obj as NPCData);
		return (string.Compare(NPCID, c.NPCID) == 0);
	}
	public override int GetHashCode()
	{
		return 0;
	}
	#endregion
}
