﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DeckPackData
{
	#region Private Properties
	private string _deckID;
	private string _deckName;

	private DeckData _deckData;
	#endregion

	#region Public Properties
	public string DeckID
	{
		get { return _deckID; }
	}

	public string DeckName
	{
		get { return _deckName; }
	}

	public DeckData DeckData
	{
		get { return _deckData; }
	}
	#endregion

	#region Constructors
	public DeckPackData()
	{
		this._deckID = "";
		this._deckName = "";
		this._deckData = new DeckData();
	}

	/*
	public void ABC()
	{
		DeckPackData d = new DeckPackData("D0001");
		for(int index = 0; index < d.DeckData.Count; ++index)
		{
			UniqueCardData dd = d.DeckData.GetCardAt(index);
			dd.PlayerCardData;
		}
	}
	*/

	public DeckPackData(string deckID)
	{
		List<DeckDBData> rawDeckData;
		DeckNameDBData deckNameData;

		bool isFoundDeck = DeckDB.GetData(deckID, out rawDeckData);
		bool isFoundDeckName = DeckNameDB.GetData(deckID, out deckNameData);
		if(isFoundDeck && isFoundDeckName)
		{
			_deckID = deckNameData.DeckID;
			_deckName = deckNameData.Name;

			_deckData = new DeckData();
			foreach(DeckDBData deckCardData in rawDeckData)
			{
				PlayerCardData playerCardData = new PlayerCardData(deckCardData.PlayerCardID);
				for(int index = 0; index < deckCardData.Qty; ++index)
				{
					// Qty
					_deckData.Add(playerCardData);
				}
			}
		}
		else
		{
			this._deckID = "";
			this._deckName = "";
			this._deckData = new DeckData();
		}
	}

	// Copy Constructor
	public DeckPackData(DeckPackData data) 
		: this(data.DeckID)
	{ 
	}
	#endregion

	#region Relational Operator Overloading
	public static bool operator ==(DeckPackData c1, DeckPackData c2)
	{
		if((System.Object)c1 == null && (System.Object)c2 == null) return true;
		else if((System.Object)c1 == null || (System.Object)c2 == null) return false;
		else return (string.Compare(c1.DeckID, c2.DeckID) == 0);
	}

	public static bool operator !=(DeckPackData c1, DeckPackData c2)
	{
		return !(c1 == c2);
	}

	public override bool Equals(System.Object obj)
	{
		// Check for null values and compare run-time types.
		if (obj == null || GetType() != obj.GetType())
			return false;

		DeckPackData c = (obj as DeckPackData);
		return (string.Compare(DeckID, c.DeckID) == 0);
	}

	public override int GetHashCode()
	{
		return 0;
	}
	#endregion
}
