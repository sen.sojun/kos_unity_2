﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DeckData
{
	public enum AddCardMethod
	{
		  Top
		, Bottom
	}

    #region Private Properties
	private List<UniqueCardData> _deckCardList = null;
	private PlayerIndex _playerIndex = PlayerIndex.One;
    private int _deckID = -1;

    //private int _deckCardIndex = 0; // 1, 2, 3, 4, ...
   
    private static int _deckIndex = 0; // 1, 2, 3, 4, ...
    private static string[] _spliters = new string[] {"[deck]"};
    #endregion

    #region Public Properties
    public int DeckID
    {
        get { return _deckID;  }
    }

    public int Count
    {
        get
        {
            if (_deckCardList != null)
                return _deckCardList.Count;
            else
                return 0;
        }
    }

    public bool IsEmpty
    {
        get { return (this.Count <= 0); }
    }

	public PlayerIndex PlayerIndex
	{
		get { return _playerIndex; }
	}
    #endregion

    #region Constructors
    public DeckData()
    {
		_deckCardList = new List<UniqueCardData>();
        _deckID = DeckData.RequestDeckID();
    }

    public DeckData(List<PlayerCardData> cardList)
    {
		_deckCardList = new List<UniqueCardData>();
        _deckID = DeckData.RequestDeckID();

        for (int index = 0; index < cardList.Count; ++index)
        {
			_deckCardList.Add(new UniqueCardData(
				  cardList[index]
				, this.PlayerIndex
				, CardZoneIndex.Deck
			));
        }
    }
    
    public DeckData(int deckID, PlayerIndex playerIndex, List<UniqueCardData> cardList)
    {
        _deckCardList = cardList;
        _playerIndex = playerIndex;
        _deckID = deckID;

        for (int index = 0; index < _deckCardList.Count; ++index)
        {
            _deckCardList[index].SetPlayerIndex(_playerIndex);
        }
    }

	/*
	public DeckData(string dataString)
	{
		string[] textList = dataString.Split ('+');

		_deckCardList = new List<UniqueCardData>();
		_deckID = DeckData.RequestDeckID();

		for (int index = 0; index < textList.Length; ++index)
		{
			_deckCardList.Add(new UniqueCardData(
				  new PlayerCardData(textList[index])
				, this.PlayerIndex
				, CardZoneIndex.Deck
			));
		}
	}
	*/

    // Copy Constructor
    public DeckData(DeckData deck)
    {
		_deckCardList = new List<UniqueCardData>();
        _deckID = deck._deckID;

        for (int index = 0; index < deck._deckCardList.Count; ++index)
        {
			_deckCardList.Add(new UniqueCardData(
				  deck._deckCardList[index].PlayerCardData
				, this.PlayerIndex
				, CardZoneIndex.Deck
			));
        }
    }
    #endregion

    #region Methods
	public void SetPlayerIndex(PlayerIndex playerIndex)
	{
		_playerIndex = playerIndex;

		foreach(UniqueCardData card in _deckCardList)
		{
			card.SetPlayerIndex(_playerIndex);
		}
	}
		  
	public void Add(PlayerCardData playerCardData, AddCardMethod method = AddCardMethod.Bottom)
    {
		UniqueCardData newDeckCard = new UniqueCardData(playerCardData, this.PlayerIndex, CardZoneIndex.Deck);

		this.Add(newDeckCard, method);
    }

	public void Add(UniqueCardData uniqueCardData, AddCardMethod method = AddCardMethod.Bottom)
	{
		UniqueCardData newDeckCard = new UniqueCardData(uniqueCardData);
		newDeckCard.SetCurrentZone(CardZoneIndex.Deck);
	
		switch(method)
		{
			case AddCardMethod.Top:
			{
				AddTop(newDeckCard);
			}
			break;

			case AddCardMethod.Bottom:
			default:
			{
				AddBottom(newDeckCard);
			}
			break;
		}
	}

	private void AddTop(UniqueCardData deckCardData)
    {
		//Debug.Log ("Add Top");
        _deckCardList.Insert(0, deckCardData);
    }

	private void AddBottom(UniqueCardData deckCardData)
    {
		//Debug.Log ("Add Bottom");
        _deckCardList.Add(deckCardData);
    }

    public void Clear()
    {
        _deckCardList.Clear();
    }

	public bool Remove(UniqueCardData deckCardData)
    {
        return _deckCardList.Remove(deckCardData);
    }

    public bool RemoveAt(int index)
    {
        if (index >= 0 && index < _deckCardList.Count)
        {
            _deckCardList.RemoveAt(index);
            return true;
        }
        return false;
    }

	private bool Draw(out UniqueCardData drawPlayerCardData)
    {

        if (_deckCardList.Count > 0)
        {
			drawPlayerCardData = new UniqueCardData(_deckCardList[0]);
            this.RemoveAt(0);

            return true;
        }

        drawPlayerCardData = null;
        return false;
    }

	private bool DrawAt(int index, out UniqueCardData drawPlayerCardData)
    {
        if (_deckCardList.Count > index)
        {
			drawPlayerCardData = new UniqueCardData(_deckCardList[index]);
            this.RemoveAt(index);

			return true;
        }

        drawPlayerCardData = null;
        return false;
    }

	public bool DrawByID(int uniqueCardID, out UniqueCardData drawPlayerCardData)
    {
        if (_deckCardList.Count > 0)
        {
			UniqueCardData resultCard;
			int index = this.FindByID(uniqueCardID, out resultCard);
            if (index >= 0)
            {
				drawPlayerCardData = new UniqueCardData(resultCard);
                this.RemoveAt(index);

                return true;
            }
        }

        drawPlayerCardData = null;
        return false;
    }

	public UniqueCardData GetCardAt(int orderIndex)
	{
        if (_deckCardList.Count > orderIndex)
		{
			UniqueCardData drawPlayerCardData = _deckCardList[orderIndex];

            return drawPlayerCardData;
		}

		return null;
	}

	public int FindFirst(string cardID, out UniqueCardData resultCardData)
    {
        if (_deckCardList.Count > 0)
        {
            for (int index = 0; index < _deckCardList.Count; ++index)
            {
                if(string.Compare(_deckCardList[index].PlayerCardData.CardID, cardID) == 0)
                {
					resultCardData = new UniqueCardData(_deckCardList[index]);
					return index;
                }
            }
        }

		resultCardData = null;
        return -1;
    }

    /*
    public int FindFirst(string playerCardID, out DeckCardData resultCardData)
    {
        if (_deckCardList.Count > 0)
        {
            for (int index = 0; index < _deckCardList.Count; ++index)
            {
                if (string.Compare(_deckCardList[index].PlayerCardData.PlayerCardID, playerCardID) == 0)
                {
                    resultCardData = new DeckCardData(_deckCardList[index]);
                    return index;
                }
            }
        }

        resultCardData = null;
        return -1;
    }
    */

	public int FindFirst(CardSymbolData symbolData, out UniqueCardData resultCardData)
    {
        if (_deckCardList.Count > 0)
        {
            for (int index = 0; index < _deckCardList.Count; ++index)
            {
                if (_deckCardList[index].PlayerCardData.SymbolData == symbolData)
                {
					resultCardData = new UniqueCardData(_deckCardList[index]);
					return index;
                }
            }
        }

		resultCardData = null;
        return -1;
    }

	public int FindFirst(CardClanData clanData, out UniqueCardData resultCardData)
    {
        if (_deckCardList.Count > 0)
        {
            for (int index = 0; index < _deckCardList.Count; ++index)
            {
                if (_deckCardList[index].PlayerCardData.ClanData == clanData)
                {
					resultCardData = new UniqueCardData(_deckCardList[index]);
					return index;
                }
            }
        }

		resultCardData = null;
		return -1;
    }

	public int FindFirst(CardTypeData typeData, out UniqueCardData resultCardData)
    {
        if (_deckCardList.Count > 0)
        {
            for (int index = 0; index < _deckCardList.Count; ++index)
            {
                if (_deckCardList[index].PlayerCardData.TypeData == typeData)
                {
					resultCardData = new UniqueCardData(_deckCardList[index]);
					return index;
                }
            }
        }

		resultCardData = null;
		return -1;
    }

	public int FindFirst(CardSubTypeData subTypeData, out UniqueCardData resultCardData)
    {
        if (_deckCardList.Count > 0)
        {
            for (int index = 0; index < _deckCardList.Count; ++index)
            {
                foreach (CardSubTypeData subType in _deckCardList[index].PlayerCardData.SubTypeDataList)
                {
                    if (subType == subTypeData)
                    {
						resultCardData = new UniqueCardData(_deckCardList[index]);
                        return index;
                    }
                }
            }
        }

		resultCardData = null;
		return -1;
    }

	public int FindFirst(CardJobData jobData, out UniqueCardData resultCardData)
    {
        if (_deckCardList.Count > 0)
        {
            for (int index = 0; index < _deckCardList.Count; ++index)
            {
                foreach (CardJobData job in _deckCardList[index].PlayerCardData.JobDataList)
                {
                    if (job == jobData)
                    {
						resultCardData = new UniqueCardData(_deckCardList[index]);
                        return index;
                    }
                }
            }
        }

		resultCardData = null;
		return -1;
    }

	public int FindFirst(PlayerCardData playerCardData, out UniqueCardData resultCardData)
	{
        if (_deckCardList.Count > 0)
		{
            for (int index = 0; index < _deckCardList.Count; ++index)
			{
                if (playerCardData == _deckCardList[index].PlayerCardData)
				{
					resultCardData = new UniqueCardData(_deckCardList[index]);
					return index;
				}
			}
		}

		resultCardData = null;
		return -1;
	}

	public int FindByID(int uniqueCardID, out UniqueCardData resultCardData)
    {
        if (_deckCardList.Count > 0)
        {
            for (int index = 0; index < _deckCardList.Count; ++index)
            {
				if (uniqueCardID == _deckCardList[index].UniqueCardID)
                {
					resultCardData = new UniqueCardData(_deckCardList[index]);
                    return index;
                }
            }
        }

        resultCardData = null;
        return -1;
    }

	public bool FindAll(string cardID, out List<UniqueCardData> resultList)
	{
		resultList = new List<UniqueCardData>();

        if (_deckCardList.Count > 0)
		{
            for (int index = 0; index < _deckCardList.Count; ++index)
			{
                if (string.Compare(_deckCardList[index].PlayerCardData.CardID, cardID) == 0)
				{
					UniqueCardData deckCardData = new UniqueCardData(_deckCardList[index]);
					resultList.Add(deckCardData);
				}
			}
			if(resultList.Count > 0)
			{ 
				return true;
			}

		}

		return false;
	}

    /*
    public bool FindAll(string playerCardID, out List<DeckCardData> resultList)
    {
        resultList = new List<DeckCardData>();

        if (_deckCardList.Count > 0)
        {
            for (int index = 0; index < _deckCardList.Count; ++index)
            {
                if (string.Compare(_deckCardList[index].PlayerCardData.PlayerCardID, playerCardID) == 0)
                {
                    DeckCardData deckCardData = new DeckCardData(_deckCardList[index]);
                    resultList.Add(deckCardData);
                }
            }
            if (resultList.Count > 0)
            {
                return true;
            }

        }

        return false;
    }
    */

	public bool FindAll(CardSymbolData symbolData, out List<UniqueCardData> resultList)
	{
		resultList = new List<UniqueCardData>();

        if (_deckCardList.Count > 0)
		{
            for (int index = 0; index < _deckCardList.Count; ++index)
			{
                if (_deckCardList[index].PlayerCardData.SymbolData == symbolData)
				{
					UniqueCardData deckCardData = new UniqueCardData(_deckCardList[index]);
					resultList.Add(deckCardData);
				}
			}
			if(resultList.Count > 0)
			{ 
				return true;
			}

		}

		return false;
	}

	public bool FindAll(CardClanData clanData, out List<UniqueCardData> resultList)
	{
		resultList = new List<UniqueCardData>();

        if (_deckCardList.Count > 0)
		{
            for (int index = 0; index < _deckCardList.Count; ++index)
			{
                if (_deckCardList[index].PlayerCardData.ClanData == clanData)
				{
					UniqueCardData deckCardData = new UniqueCardData(_deckCardList[index]);
					resultList.Add(deckCardData);
				}
			}
			if(resultList.Count > 0)
			{ 
				return true;
			}

		}

		return false;
	}

	public bool FindAll(CardTypeData typeData, out List<UniqueCardData> resultList)
	{
		resultList = new List<UniqueCardData>();

        if (_deckCardList.Count > 0)
		{
            for (int index = 0; index < _deckCardList.Count; ++index)
			{
                if (_deckCardList[index].PlayerCardData.TypeData == typeData)
				{
					UniqueCardData deckCardData = new UniqueCardData(_deckCardList[index]);
					resultList.Add(deckCardData);
				}
			}
			if(resultList.Count > 0)
			{ 
				return true;
			}
		}

		return false;
	}

	public bool FindAll(CardSubTypeData subTypeData, out List<UniqueCardData> resultList)
	{
		resultList = new List<UniqueCardData>();

        if (_deckCardList.Count > 0)
		{
            for (int index = 0; index < _deckCardList.Count; ++index)
			{
                foreach (CardSubTypeData subType in _deckCardList[index].PlayerCardData.SubTypeDataList)
                {
                    if (subType == subTypeData)
                    {
						UniqueCardData deckCardData = new UniqueCardData(_deckCardList[index]);
                        resultList.Add(deckCardData);
                        break;
                    }
                }
			}
			if(resultList.Count > 0)
			{ 
				return true;
			}

		}

		return false;
	}

	public bool FindAll(CardJobData jobData, out List<UniqueCardData> resultList)
	{
		resultList = new List<UniqueCardData>();

        if (_deckCardList.Count > 0)
		{
            for (int index = 0; index < _deckCardList.Count; ++index)
			{
                foreach (CardJobData job in _deckCardList[index].PlayerCardData.JobDataList)
                {
                    if (job == jobData)
                    {
						UniqueCardData deckCardData = new UniqueCardData(_deckCardList[index]);
                        resultList.Add(deckCardData);
                        break;
                    }
                }
			}
			if(resultList.Count > 0)
			{ 
				return true;
			}

		}

		return false;
	}

    public void Shuffle()
    {
        for (int index = 0; index < _deckCardList.Count; ++index)
        {
			UniqueCardData temp = _deckCardList[index];
            int randomIndex = Random.Range(index, _deckCardList.Count);
            _deckCardList[index] = _deckCardList[randomIndex];
            _deckCardList[randomIndex] = temp;
        }
    }

    public void Sort()
    {
        _deckCardList = GameUtility.SortCard(_deckCardList);
    }
    
    public void ReIndexCard(int firstIndex)
    {
        for(int index = 0; index < _deckCardList.Count; ++index)
        {
           _deckCardList[index].SetUniqueCardID(firstIndex + index);
        }
    }

	public override string ToString()
	{
		string text = "";

		for (int index = 0; index < _deckCardList.Count; ++index)
		{
			UniqueCardData temp = _deckCardList[index];
			if (text.Length > 0)
			{
				text += "+" + temp.PlayerCardData.CardID;
			} 
			else
			{
				text += temp.PlayerCardData.CardID;
			}
		}

		return text;
	}
    
    public string ToDeckText()
    {
        string result = "";
        string cardListText = "";
        foreach(UniqueCardData card in _deckCardList)
        {
            cardListText += _spliters[0] + card.UniqueCardID;
            cardListText += _spliters[0] + card.PlayerCardData.PlayerCardID;
        }

        result += this.DeckID;
        result += _spliters[0] + this.PlayerIndex;    
        result += _spliters[0] + cardListText;

        return result;
    }

	public static List<string> StringToIDList(string text)
	{
		List<string> idList = new List<string> ();

		string[] dataList = text.Split ('+');

		if(dataList != null)
		{
			idList = new List<string> (dataList);
		}

		return idList;
	}
    
    public static DeckData StrToDeckData(string deckText)
    {
        DeckData deck;
        
        string[] texts = deckText.Split(_spliters, System.StringSplitOptions.RemoveEmptyEntries);
        
        int deckID = int.Parse(texts[0]);
        PlayerIndex playerIndex = PlayerIndex.One;
        switch(texts[1])
        {
            case "One":
            case "ONE":
            case "one":
            {
                playerIndex = PlayerIndex.One;
            }
            break;
            
            case "Two":
            case "TWO":
            case "two":
            {
                playerIndex = PlayerIndex.Two;
            }
            break;
        }
        
        List<UniqueCardData> cardList = new List<UniqueCardData>();
        for(int index = 2; index < texts.Length; index += 2)
        {
            int uniqueCardID    = int.Parse(texts[index]);
            string playerCardID = texts[index + 1];
            UniqueCardData card = new UniqueCardData(uniqueCardID, playerCardID, playerIndex, CardZoneIndex.Deck);
            
            cardList.Add(card);
        }
        
        deck = new DeckData(deckID, playerIndex, cardList);
        
        return deck;
    }

    public static int RequestDeckID()
    {
        return ++_deckIndex;
    }
    
    public static void ResetDeckIndex()
    {
        _deckIndex = 0;
    }
    
    private static readonly List<string> _invalidCardIDList = new List<string>() {
          "C0045" // Di alno Stronghold
        , "C0050" // Mutant Lair
        , "C0074" // Forbidden Cave
        , "C0078" // Teleport
        , "C0083" // Lidia Berondo
        , "C0084" // Sonata Di alno
        , "C0085" // Ashura
        , "C0088" // Lunar Rosette
        , "C0095" // Angel of Fortune
        , "C0096" // Obsidian Dragon
        , "C0097" // Lost Number
        , "C0098" // Weirding Insect
        , "C0099" // Ancient Library
        , "C0103" // Sonata's Safehouse
        , "C0104" // Sonata's Bodyguard
        , "C0105" // Titanic Hive
        , "C0106" // Colossal Drone
        , "C0107" // Manufacturing Center
        , "C0108" // Manufacturing Manager
        , "C0109" // Bunny Princess
    };
    
    public static List<string> CreateRandomDeck(
          int baseNum = 1
        , int uniqueNum = 1
        , int resourceNum = 12
        , int unitNum = 18
        , int eventNum = 4
        , int structureNum = 4
    )
    {            
        if(baseNum + uniqueNum + resourceNum + unitNum + eventNum + structureNum != 40)
        {
            Debug.LogWarning("CreateRandomDeck: Invalid deck card number.");
            return null;
        }
    
        List<string> resultList = new List<string> ();

        List<PlayerCardData> cardList = new List<PlayerCardData>();
        {
            List<CardDBData> rawCardList;
            CardDB.GatAllData (out rawCardList);
            foreach(CardDBData data in rawCardList)
            {
                if(!_invalidCardIDList.Contains(data.CardID)) // Filter invalid card out.
                {
                    cardList.Add(new PlayerCardData(data.CardID));
                }
            }
        }

        // Base Random
        List<PlayerCardData> baseList = new List<PlayerCardData>();
        {
            // gather all base card.
            foreach (PlayerCardData data in cardList)
            {
                if (data.IsCardType(CardTypeData.CardType.Base))
                {
                    baseList.Add (data);              
                }
            }   
        }
        for (int i = 0; i < baseNum; ++i)
        {   
            resultList.Add (baseList [Random.Range (0, baseList.Count)].CardID);
        }

        // Unique Random
        List<PlayerCardData> uniqueList = new List<PlayerCardData>();
        {
            foreach (PlayerCardData data in cardList)
            {
                if(data.IsCardSubType(CardSubTypeData.CardSubType.Unique))
                {
                    if (!_invalidCardIDList.Contains(data.CardID))
                    {
                        uniqueList.Add(data);
                    }
                }
            }   
        }
        for (int i = 0; i < uniqueNum; ++i) 
        {   
            resultList.Add (uniqueList [Random.Range (0, uniqueList.Count)].CardID);
        }

        // Resource Random
        List<PlayerCardData> resourceList = new List<PlayerCardData>();
        {
            foreach (PlayerCardData data in cardList)
            {
                if(data.IsCardSubType(CardSubTypeData.CardSubType.Resource))
                {
                    resourceList.Add (data);
                }
            }   
        }
        for (int i = 0; i < resourceNum; ++i) 
        {            
            if(resourceList.Count > 0)
            {
                int index = Random.Range (0, resourceList.Count);
                
                if(resourceList[index].IsCardSymbol(CardSymbolData.CardSymbol.Mass))
                {
                    resultList.Add (resourceList[index].CardID);
                }
                else
                {
                    List<string> idList = resultList.FindAll(
                        delegate(string id) {
                            return(resourceList[index].CardID == id);
                        }
                    );                   
                    if(idList.Count < 3)
                    {
                        resultList.Add (resourceList[index].CardID);
                    }
                    else
                    {
                        // retry
                        --i;
                        resourceList.RemoveAt(index);
                    }
                }
            }
        }

        // Unit Random
        List<PlayerCardData> unitList = new List<PlayerCardData>();
        {
            foreach (PlayerCardData data in cardList)
            {
                if(data.IsCardType(CardTypeData.CardType.Unit))
                {
                    unitList.Add(data);   
                }
            }
        }   
        for (int i = 0; i < unitNum; ++i) 
        {
            if(unitList.Count > 0)
            {
                int index = Random.Range (0, unitList.Count);
                
                if(unitList[index].IsCardSymbol(CardSymbolData.CardSymbol.Mass))
                {
                    resultList.Add (unitList[index].CardID);
                }
                else
                {
                    List<string> idList = resultList.FindAll(
                        delegate(string id) {
                            return(unitList[index].CardID == id);
                        }
                    );                   
                    if(idList.Count < 3)
                    {
                        resultList.Add (unitList[index].CardID);
                    }
                    else
                    {
                        // retry
                        --i;
                    }
                }
            }
        }  
        
        // Event Random
        List<PlayerCardData> eventList = new List<PlayerCardData>();
        {
            foreach (PlayerCardData data in cardList)
            {
                if(data.IsCardType(CardTypeData.CardType.Event))
                {
                    eventList.Add(data);   
                }
            }
        }   
        for (int i = 0; i < eventNum; ++i) 
        {
            if(eventList.Count > 0)
            {
                int index = Random.Range (0, eventList.Count);
                
                if(eventList[index].IsCardSymbol(CardSymbolData.CardSymbol.Mass))
                {
                    resultList.Add(eventList[index].CardID);
                }
                else
                {
                    List<string> idList = resultList.FindAll(
                        delegate(string id) {
                            return(eventList[index].CardID == id);
                        }
                    );                   
                    if(idList.Count < 3)
                    {
                        resultList.Add (eventList[index].CardID);
                    }
                    else
                    {
                        // retry
                        --i;
                    }
                }
            }
        }  
        
        // Structure Random
        List<PlayerCardData> structureList = new List<PlayerCardData>();
        {
            foreach (PlayerCardData data in cardList)
            {
                if(data.IsCardType(CardTypeData.CardType.Structure))
                {
                    structureList.Add(data);   
                }
            }
        }   
        for (int i = 0; i < structureNum; ++i) 
        {
            if(structureList.Count > 0)
            {
                int index = Random.Range (0, structureList.Count);
                
                if(structureList[index].IsCardSymbol(CardSymbolData.CardSymbol.Mass))
                {
                    resultList.Add (structureList[index].CardID);
                }
                else
                {
                    List<string> idList = resultList.FindAll(
                        delegate(string id) {
                            return(structureList[index].CardID == id);
                        }
                    );                   
                    if(idList.Count < 3)
                    {
                        resultList.Add (structureList[index].CardID);
                    }
                    else
                    {
                        // retry
                        --i;
                        structureList.RemoveAt(index);
                    }
                }
            }
        }  
        
        return resultList;
    }
    #endregion
}
