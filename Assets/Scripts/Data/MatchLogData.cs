﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class MatchLogData
{   
    public static readonly string TimeFormat = "yyyy:MM:dd:HH:mm:ss:ffff";

    #region Public Properties
    public string Key;
    public DateTime Time;
    public string PlayerName_L;
    public string PlayerName_R;
    public string ImagePath_L;
    public string ImagePath_R;
    #endregion
    
    #region Constructors
    public MatchLogData(DateTime time, string playerName_L, string playerName_R, string imagePath_L, string imagePath_R)
    {
        Time = time;
        PlayerName_L = playerName_L;
        PlayerName_R = playerName_R;
        ImagePath_L = imagePath_L;
        ImagePath_R = imagePath_R;
                
        Key = Time.ToString(TimeFormat);
    }
    
    public MatchLogData(MatchLogData data)
    {
        Time = data.Time;
        PlayerName_L = data.PlayerName_L;
        PlayerName_R = data.PlayerName_R;
        ImagePath_L = data.ImagePath_L;
        ImagePath_R = data.ImagePath_R;
        
        Key = Time.ToString(TimeFormat);
    }
    #endregion
    
    #region Methods
    public static DateTime ToDateTime(string text)
    {
        DateTime dateTime = DateTime.ParseExact(text, MatchLogData.TimeFormat, CultureInfo.InvariantCulture);
        return dateTime;
    }
    #endregion
}
