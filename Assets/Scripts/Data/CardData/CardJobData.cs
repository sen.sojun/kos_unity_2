﻿using UnityEngine;
using System.Collections;

public class CardJobData
{
	public enum JobType
	{
		  None
		, Port
		, Angel
		, PowerPlant
		, Worker
		, Portal
		, Soldier
		, Mine
		, TreasureHunter
		, Factory
		, StarFencer
		, Silo
		, Resident
		, Engineer
		, Cave
		, Foundry
		, Tower
		, Lab
		, Library
		, MartialArtist
		, Silencer
		, Dictator
		, Lord
	}

    #region Private Properties
    private string _cardJobID;
    private string _jobName_EN;
    private string _jobName_TH;
    #endregion

    #region Public Properties
    public string CardJobID
    {
        get { return _cardJobID; }
    }
    public string JobName_EN
    {
        get { return _jobName_EN; }
    }
    public string JobName_TH
    {
        get { return _jobName_TH; }
    }
    #endregion

    #region Constructors
    public CardJobData()
    {
        this._cardJobID = "";
        this._jobName_EN = "";
        this._jobName_TH = "";
    }

	public CardJobData(JobType jobType)
		: this(CardJobData.EnumJobTypeToID(jobType))
	{
	}

    public CardJobData(string cardJobID)
    {
        CardJobDBData rawCardJobData;
        bool isSuccess = CardJobDB.GetData(cardJobID, out rawCardJobData);

        if (isSuccess)
        {
            this._cardJobID = cardJobID;
            this._jobName_EN = rawCardJobData.JobName_EN;
            this._jobName_TH = rawCardJobData.JobName_TH;
        }
        else
        {
            string errorLog = string.Format("CardJobData: cardJobID({0}) not found.", cardJobID);
            Debug.LogError(errorLog);

            this._cardJobID = "";
            this._jobName_EN = "";
            this._jobName_TH = "";
        }
    }
    #endregion

	#region Methods
	public static string EnumJobTypeToID(JobType jobType)
	{
		switch (jobType)
		{ 
			case JobType.Port:
				return "J0002";

			case JobType.Angel:
				return "J0003";

			case JobType.PowerPlant:
				return "J0004";

			case JobType.Worker:
				return "J0005";

			case JobType.Portal:
				return "J0006";

			case JobType.Soldier:
				return "J0007";

			case JobType.Mine:
				return "J0008";

			case JobType.TreasureHunter:
				return "J0009";

			case JobType.Factory:
				return "J0010";

			case JobType.StarFencer:
				return "J0011";

			case JobType.Silo:
				return "J0012";

			case JobType.Resident:
				return "J0013";

			case JobType.Engineer:
				return "J0014";

			case JobType.Cave:
				return "J0015";

			case JobType.Foundry:
				return "J0016";

			case JobType.Tower:
				return "J0017";

			case JobType.Lab:
				return "J0018";

			case JobType.Library:
				return "J0019";

			case JobType.MartialArtist:
				return "J0020";

			case JobType.Silencer:
				return "J0021";

			case JobType.Dictator:
				return "J0022";

			case JobType.Lord:
				return "J0023";

			default:
				return "J0000";
		}
	}

    public static JobType StringToCardJob(string text)
    {
        foreach (JobType jobType in System.Enum.GetValues(typeof(JobType)))
        {
            if(string.Compare(text.ToUpper(), jobType.ToString().ToUpper()) == 0)
            {
                return jobType;
            }
        }

        return JobType.None;
    }
	#endregion

    #region Relational Operator Overloading
	public static bool operator ==(CardJobData c1, CardJobData.JobType cj2)
	{
		if((System.Object)c1 == null) return false;
		else return (string.Compare(c1.CardJobID, CardJobData.EnumJobTypeToID(cj2)) == 0);
	}

	public static bool operator !=(CardJobData c1, CardJobData.JobType cj2)
	{
		return !(c1 == cj2);
	}

    public static bool operator ==(CardJobData c1, CardJobData c2)
    {
		if((System.Object)c1 == null && (System.Object)c2 == null) return true;
		else if((System.Object)c1 == null || (System.Object)c2 == null) return false;
        else return (string.Compare(c1.CardJobID, c2.CardJobID) == 0);
    }

    public static bool operator !=(CardJobData c1, CardJobData c2)
    {
        return !(c1 == c2);
    }

    public override bool Equals(System.Object obj)
    {
        // Check for null values and compare run-time types.
        if (obj == null || GetType() != obj.GetType())
            return false;

        CardJobData c = (obj as CardJobData);
        return (string.Compare(CardJobID, c.CardJobID) == 0);
    }

    public override int GetHashCode()
    {
        return 0;
    }
    #endregion
}
