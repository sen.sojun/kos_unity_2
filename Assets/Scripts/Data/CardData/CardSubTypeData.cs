﻿using UnityEngine;
using System.Collections;

public class CardSubTypeData
{
    public enum CardSubType
    {
		  None
        , Unique
		, Resource
		, Mission
		, Sudden
		, Trap
		, Skill
		, Spell
		, Machine
		, Human
		, Mutant
		, Dragonian
		, Tron
		, Weapon
		, Aircraft
		, Robot
		, Cyborg
		, Rabbit
		, Armor
		, Glove
		, Vehicle
		, Bike
		, Turret
		, Plant
		, Skira
		, Foundry
		, Insect
    }

    #region Private Properties
    private string _cardSubTypeID;
    private string _subTypeName_EN;
    private string _subTypeName_TH;
    #endregion

    #region Public Properties
    public string CardSubTypeID
    {
        get { return _cardSubTypeID; }
    }
    public string SubTypeName_EN 
    {
        get { return _subTypeName_EN; }
    }
    public string SubTypeName_TH
    {
        get { return _subTypeName_TH; }
    }
    #endregion

    #region Constructors
    public CardSubTypeData()
    {
        this._cardSubTypeID = "";
        this._subTypeName_EN = "";
        this._subTypeName_TH = "";
    }

	public CardSubTypeData(CardSubType cardSubType)
		: this(EnumCardSubTypeToID(cardSubType))
	{
	}

    public CardSubTypeData(string cardSubTypeID)
    {
        CardSubTypeDBData rawCardSubTypeData;
        bool isSuccess = CardSubTypeDB.GetData(cardSubTypeID, out rawCardSubTypeData);

        if (isSuccess)
        {
            this._cardSubTypeID = cardSubTypeID;
            this._subTypeName_EN = rawCardSubTypeData.SubTypeName_EN;
            this._subTypeName_TH = rawCardSubTypeData.SubTypeName_TH;
        }
        else
        {
            string errorLog = string.Format("CardSubTypeData: cardSubTypeID({0}) not found.", cardSubTypeID);
            Debug.LogError(errorLog);

            this._cardSubTypeID = "";
            this._subTypeName_EN = "";
            this._subTypeName_TH = "";
        }
    }
    #endregion

    #region Methods
    public static string EnumCardSubTypeToID(CardSubType cardSubType)
    {
		/*
		ST0000	None
		ST0001	Unique
		ST0002	Resource
		ST0003	Mission
		ST0004	Sudden
		ST0005	Trap
		ST0006	Skill
		ST0007	Spell
		ST0008	Machine
		ST0009	Human
		ST0010	Mutant
		ST0011	Dragonian
		ST0012	Tron
		ST0013	Weapon
		ST0014	Aircraft
		ST0015	Robot
		ST0016	Cyborg
		ST0017	Rabbit
		ST0018	Armor
		ST0019	Glove
		ST0020	Vehicle
		ST0021	Bike
		ST0022	Turret
		ST0023	Plant
		ST0024	Skira
		ST0025	Foundry
		ST0026	Insect
		*/
        switch (cardSubType)
        {
            case CardSubType.Unique: 	return "ST0001";
			case CardSubType.Resource: 	return "ST0002";
			case CardSubType.Mission: 	return "ST0003";
			case CardSubType.Sudden: 	return "ST0004";
			case CardSubType.Trap: 		return "ST0005";
			case CardSubType.Skill: 	return "ST0006";
			case CardSubType.Spell: 	return "ST0007";
			case CardSubType.Machine: 	return "ST0008";
			case CardSubType.Human: 	return "ST0009";
			case CardSubType.Mutant: 	return "ST0010";
			case CardSubType.Dragonian: return "ST0011";
			case CardSubType.Tron: 		return "ST0012";
			case CardSubType.Weapon: 	return "ST0013";
			case CardSubType.Aircraft: 	return "ST0014";
			case CardSubType.Robot: 	return "ST0015";
			case CardSubType.Cyborg: 	return "ST0016";
			case CardSubType.Rabbit: 	return "ST0017";
			case CardSubType.Armor: 	return "ST0018";
			case CardSubType.Glove: 	return "ST0019";
			case CardSubType.Vehicle: 	return "ST0020";
			case CardSubType.Bike: 		return "ST0021";
			case CardSubType.Turret: 	return "ST0022";
			case CardSubType.Plant: 	return "ST0023";
			case CardSubType.Skira: 	return "ST0024";
			case CardSubType.Foundry: 	return "ST0025";
			case CardSubType.Insect: 	return "ST0026";

			case CardSubType.None:
            default:
                return "ST0000";
        }
    }

    public static CardSubType StringToCardSubType(string text)
    {
        foreach (CardSubType subType in System.Enum.GetValues(typeof(CardSubType)))
        {
            if(string.Compare(text.ToUpper(), subType.ToString().ToUpper()) == 0)
            {
                return subType;
            }
        }
            
        return CardSubType.None;
    }
    #endregion

    #region Relational Operator Overloading
	public static bool operator ==(CardSubTypeData c1, CardSubTypeData.CardSubType cst2)
	{
		if((System.Object)c1 == null) return false;
		else return (string.Compare(c1.CardSubTypeID, CardSubTypeData.EnumCardSubTypeToID(cst2)) == 0);
	}
	public static bool operator !=(CardSubTypeData c1, CardSubTypeData.CardSubType cst2)
	{
		return !(c1 == cst2);
	}

    public static bool operator ==(CardSubTypeData c1, CardSubTypeData c2)
    {
		if((System.Object)c1 == null && (System.Object)c2 == null) return true;
		else if((System.Object)c1 == null || (System.Object)c2 == null) return false;
        else return (string.Compare(c1.CardSubTypeID, c2.CardSubTypeID) == 0);
    }
    public static bool operator !=(CardSubTypeData c1, CardSubTypeData c2)
    {
        return !(c1 == c2);
    }
    public override bool Equals(System.Object obj)
    {
        // Check for null values and compare run-time types.
        if (obj == null || GetType() != obj.GetType())
            return false;

        CardSubTypeData c = (obj as CardSubTypeData);
        return (string.Compare(CardSubTypeID, c.CardSubTypeID) == 0);
    }
    public override int GetHashCode()
    {
        return 0;
    }
    #endregion
}
