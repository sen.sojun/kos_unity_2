﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UniqueCardData
{
    #region Private Properties
    protected int _uniqueCardID;
    protected CardZoneIndex _cardZoneIndex;
    protected PlayerIndex _ownerPlayerIndex;
    protected PlayerCardData _playerCardData;

    protected List<AbilityData> _abilityList = null;
    private bool _isInitAbility = false;

    private static int _uniqueCardIndex = 0; // 1, 2, 3, 4, ...
    #endregion

    #region Public Properties

    static public int LastUniqueCardIndex
    {
        get { return _uniqueCardIndex; }
    }

    public int UniqueCardID
    {
        get { return _uniqueCardID; }
    }

    public CardZoneIndex CurrentZone
    {
        get { return _cardZoneIndex; }
    }

    public PlayerIndex OwnerPlayerIndex
    {
        get { return _ownerPlayerIndex; }
    }

    public PlayerCardData PlayerCardData
    {
        get { return _playerCardData; }
    }
    public List<AbilityData> AbilityList
    {
        get { return _abilityList; }
    }
    public bool IsInitAbility
    {
        get
        {
            return _isInitAbility;
        }
    }

    public bool IsSubType(CardSubTypeData.CardSubType subType)
    {
        foreach (CardSubTypeData subTypeData in PlayerCardData.SubTypeDataList)
        {
            if (subTypeData == subType)
            {
                return true;
            }
        }

        return false;
    }

    public bool IsJob(CardJobData.JobType job)
    {
        foreach (CardJobData jobData in PlayerCardData.JobDataList)
        {
            if (jobData == job)
            {
                return true;
            }
        }

        return false;
    }
    #endregion

    #region Constructors
    public UniqueCardData(PlayerCardData playerCardData, PlayerIndex playerIndex, CardZoneIndex cardZone)
	{
		_uniqueCardID = UniqueCardData.RequestUniqueCardID();

		_playerCardData = new PlayerCardData(playerCardData);
		_ownerPlayerIndex = playerIndex;

		_cardZoneIndex = cardZone;
	}

	public UniqueCardData(int uniqueID, string playerCardID, PlayerIndex playerIndex, CardZoneIndex cardZone)
	{
		_uniqueCardID = uniqueID;

		_playerCardData = new PlayerCardData(playerCardID);
		_ownerPlayerIndex = playerIndex;

		_cardZoneIndex = cardZone;
	}

	// Copy Constructor
	public UniqueCardData(UniqueCardData uniqueCardData)
	{
		_uniqueCardID = uniqueCardData._uniqueCardID;

		_playerCardData = new PlayerCardData(uniqueCardData._playerCardData);
		_ownerPlayerIndex = uniqueCardData.OwnerPlayerIndex;

		_cardZoneIndex = uniqueCardData.CurrentZone;
	}
    #endregion

    #region Methods
    public static int RequestUniqueCardID()
	{
		return ++_uniqueCardIndex;
	}
    
    public static void ResetUniqueCardIndex()
    {
        _uniqueCardIndex = 0;
    }
    
    public void SetUniqueCardID(int uniqueCardIndex)
    {
        _uniqueCardID = uniqueCardIndex;
    }

	public void SetPlayerIndex(PlayerIndex playerIndex)
	{
		_ownerPlayerIndex = playerIndex;
	}

	public void SetCurrentZone(CardZoneIndex cardZone)
	{
		_cardZoneIndex = cardZone;
	}

	public void InitAbility()
	{
		_abilityList = new List<AbilityData>();

		foreach(AbilityData.AbilityParams abilityParams in this.PlayerCardData.AbilityParamsList)
		{
			int index = _abilityList.Count;
			_abilityList.Add(new AbilityData(this, index, abilityParams));
		}

		_isInitAbility = true;
	}

	public void DeinitAbility()
	{
		if(_abilityList != null && _abilityList.Count > 0)
		{
			//Debug.Log("DeinitCard 1");

			foreach(AbilityData ability in _abilityList)
			{
				ability.Deinit();
			}

			_abilityList.Clear();
		}

		_abilityList = null;
		_isInitAbility = false;
	}

	public bool IsCanUse()
	{
		if(this.IsInitAbility)
		{
			if(    (PlayerCardData.TypeData == CardTypeData.CardType.Event)
				|| (PlayerCardData.TypeData == CardTypeData.CardType.Technic)
			)
			{
				foreach(AbilityData ability in _abilityList)
				{
					bool isCanUse = ability.IsCanUse();
					if(!isCanUse) return false;
				}

				return true;
			}
		}

		return false;
	}
	#endregion

    #region UniqueCard Compare

    // Status

    public bool IsCardOwner(PlayerIndex ownerIndex)
    {
        return (this.OwnerPlayerIndex == ownerIndex);
    }

    public bool IsCardZone(CardZoneIndex cardZone)
    {
        return (this.CurrentZone == cardZone);
    }
    #endregion

	#region Relational Operator Overloading
	public static bool operator ==(UniqueCardData c1, UniqueCardData c2)
	{
		if ((System.Object)c1 == null && (System.Object)c2 == null) return true;
		else if ((System.Object)c1 == null || (System.Object)c2 == null) return false;
		else return ((c1.PlayerCardData == c2.PlayerCardData) && (c1.UniqueCardID == c2.UniqueCardID));
	}
	public static bool operator !=(UniqueCardData c1, UniqueCardData c2)
	{
		return !(c1 == c2);
	}
	public override bool Equals(System.Object obj)
	{
		// Check for null values and compare run-time types.
		if (obj == null || GetType() != obj.GetType())
			return false;

		UniqueCardData c = (obj as UniqueCardData);
		return ((c.PlayerCardData == this.PlayerCardData) && (c.UniqueCardID == this.UniqueCardID));
	}
	public override int GetHashCode()
	{
		return 0;
	}
	#endregion

}
