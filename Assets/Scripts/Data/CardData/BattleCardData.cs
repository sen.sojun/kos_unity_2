﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BattleCardData
{
    #region Private Properties
    private int _battleCardID;
    
    private PlayerCardData _playerCardData;
	private PlayerIndex _ownerPlayer;
	private BattleZoneIndex _currentZone;

	private int _currentShield;
    private int _currentHP;
	private int _currentAtk;
	private int _currentSpeed;
	private int _currentMove;
	private int _currentAttackRange;
    private PassiveIndex _currentPassiveIndex;

	private int _bonusShield;
	private int _bonusHP;
	private int _bonusAtk;
	private int _bonusSpeed;
	private int _bonusMove;
	private int _bonusAttackRange;
	private PassiveIndex _bonusPassiveIndex;

    private bool _isActive;
	private bool _isFaceDown;
    private bool _isLeader;
	private bool _isForceDestroy = false;
	private bool _isDestroy = false;

    private List<CardBuff> _buffList;
	private List<AbilityData> _abilityList;

    private static int _battleCardIndex = 0; // 1, 2, 3, 4, ...
    #endregion

    #region Public Properties
    public int BattleCardID
    {
        get { return _battleCardID; }
    }

	public PlayerIndex OwnerPlayerIndex
	{
		get { return _ownerPlayer; } 
	}

	public BattleZoneIndex CurrentZone
	{
		get { return _currentZone; }
	}
		
	public PlayerCardData PlayerCardData
	{
		get { return _playerCardData; }
	}

    public string CardID
    {
        get { return _playerCardData.CardID; }
    }

    public CardImageData ImageData
    {
        get { return _playerCardData.ImageData; }
    }

    public CardSymbolData SymbolData
    {
        get { return _playerCardData.SymbolData; }
    }

    public CardClanData ClanData
    {
        get { return _playerCardData.ClanData; }
    }

    public CardTypeData TypeData
    {
        get { return _playerCardData.TypeData; }
    }

    public List<CardSubTypeData> SubTypeDataList
    {
        get 
		{ 
			List<CardSubTypeData> subTypeDataList = new List<CardSubTypeData>(_playerCardData.SubTypeDataList);
			foreach(CardBuff buff in _buffList)
			{
				subTypeDataList.AddRange(buff.SubTypeList);
			}

			return subTypeDataList; 
		}
    }

    public List<CardJobData> JobDataList
    {
		get 
		{ 
			List<CardJobData> jobDataList = new List<CardJobData>(_playerCardData.JobDataList);
			foreach(CardBuff buff in _buffList)
			{
				jobDataList.AddRange(buff.JobTypeList);
			}

			return jobDataList; 
		}
    }

    public string Name_EN
    {
        get { return _playerCardData.Name_EN; }
    }

    public string Name_TH
    {
        get { return _playerCardData.Name_TH; }
    }

    public string Alias_EN
    {
        get { return _playerCardData.Alias_EN; }
    }

    public string Alias_TH
    {
        get { return _playerCardData.Alias_TH; }
    }

    public int LV
    {
        get { return _playerCardData.LV; }
    }

    public int Cost
    {
        get { return _playerCardData.Cost; }
    }
		
	// Base

    public int BaseShield
    {
        get { return _playerCardData.Shield; }
    }

	public int BaseHP
    {
        get { return _playerCardData.HitPoint; }
    }

	public int BaseAttack
    {
        get { return _playerCardData.Attack; }
    }

	public int BaseSpeed
    {
        get { return _playerCardData.Speed; }
    }

	public int BaseMove
	{
		get 
		{
			return 1;
		}
	}

	public int BaseAttackRange
	{
		get 
		{
			return 0;
		}
	}

    public PassiveIndex BasePassiveIndex
    {
        get { return _playerCardData.PassiveIndex; }
    }

	// Bonus

	public int BonusShield
	{
		get 
		{ 
			int sum = 0;

			if(_buffList != null && _buffList.Count > 0)
			{
				foreach(CardBuff buff in _buffList)
				{
					if(buff.IsActive)
					{
						sum += buff.Shield;
					}
				}
			}

			return sum; 
		}
	}

	public int BonusHP
	{
		get 
		{ 
			int sum = 0;

			if(_buffList != null && _buffList.Count > 0)
			{
				foreach(CardBuff buff in _buffList)
				{
					if(buff.IsActive)
					{
						sum += buff.HP;
					}
				}
			}

			return sum; 
		}
	}

	public int BonusAttack
	{
		get 
		{ 
			int sum = 0;

			if(_buffList != null && _buffList.Count > 0)
			{
				foreach(CardBuff buff in _buffList)
				{
					if(buff.IsActive)
					{
						sum += buff.Attack;
					}
				}
			}

			return sum; 
		}
	}

	public int BonusSpeed
	{
		get 
		{ 
			int sum = 0;

			if(_buffList != null && _buffList.Count > 0)
			{
				foreach(CardBuff buff in _buffList)
				{
					if(buff.IsActive)
					{
						sum += buff.Speed;
					}
				}
			}

			return sum; 
		}
	}

	public int BonusMove
	{
		get 
		{ 
			int sum = 0;

			if(_buffList != null && _buffList.Count > 0)
			{
				foreach(CardBuff buff in _buffList)
				{
					if(buff.IsActive)
					{
						sum += buff.Move;
					}
				}
			}

			return sum; 
		}
	}

	public int BonusAttackRange
	{
		get 
		{ 
			int sum = 0;

			if(_buffList != null && _buffList.Count > 0)
			{
				foreach(CardBuff buff in _buffList)
				{
					if(buff.IsActive)
					{
						sum += buff.AttackRange;
					}
				}
			}

			return sum; 
		}
	}

	public PassiveIndex BonusPassiveIndex
	{
		get 
		{ 
			PassiveIndex sum = new PassiveIndex();

			if(_buffList != null && _buffList.Count > 0)
			{
				foreach(CardBuff buff in _buffList)
				{
					if(buff.IsActive)
					{
						sum += buff.PassiveIndex;
					}
				}
			}

			return sum; 
		}
	}

	// Current

	public int CurrentShield
	{
		get { return _currentShield; }
	}

	public int CurrentHP
	{
		get { return _currentHP; }
	}

	public int CurrentAttack
	{
		get { return _currentAtk; }
	}

	public int CurrentSpeed
	{
		get { return _currentSpeed; }
	}

	public int CurrentMove
	{
		get { return _currentMove; }
	}

	public int CurrentAttackRange
	{
		get { return _currentAttackRange; }
	}

    public PassiveIndex CurrentPassiveIndex
    {
        get { return _currentPassiveIndex; }
    }

    public string DescriptionText_EN
    {
        get { return _playerCardData.DescriptionText_EN; }
    }

    public string DescriptionText_TH
    {
        get { return _playerCardData.DescriptionText_TH; }
    }

    public List<AbilityData> AbilityList
    {
		get { return _abilityList; }
    }

    public bool IsCanMove
    {
		get 
		{
			if(!this.IsFaceDown && !this.IsDead && !this.IsDestroy && (this.CurrentSpeed > 0) && (this.CurrentMove > 0))
			{
				return true;
			}

			return false;
		}
    }

	public bool IsCanAttack
	{
		get
		{
			if(!this.IsFaceDown && !this.IsDead && !this.IsDestroy && (this.CurrentSpeed > 0))
			{
				if(this.CurrentAttackRange >= 0)
				{
					if(this.IsActive)
					{
						return true;
					}
				}
			}

			return false;
		}
	}

	public bool IsCanCounter
	{
		get 
		{
			if(!this.IsFaceDown && !this.IsDead && !this.IsDestroy && (this.CurrentSpeed > 0))
			{
				if(this.CurrentAttackRange >= 0)
				{
					//if(this.IsActive)
					{
						return true;
					}
				}
			}

			return false;
		}
	}

    public bool IsActive 
    {
        get { return _isActive; }
        //set { _isActive = value; }
    }

    public bool IsDead
    {
		get { return (this.CurrentHP <= 0) || IsDestroy; }
    }

	public bool IsFaceDown
	{
		get { return _isFaceDown; }
	}

    public bool IsLeader
    {
        get { return _isLeader;  }
    }

	public bool IsDestroy 
	{ 
		get { return _isDestroy; } 
		set { _isDestroy = value; }
	}
    #endregion

	#region Constructors
	public BattleCardData(PlayerCardData data, PlayerIndex ownerPlayer, BattleZoneIndex spawnZone, bool isLeader = false, bool isFaceDown = false)
	{
        _playerCardData = new PlayerCardData(data);
		_ownerPlayer = ownerPlayer;
		_currentZone = spawnZone;

		_isFaceDown = isFaceDown;
        _isLeader = isLeader;
		_isActive = false;

		_abilityList = new List<AbilityData>();
        _buffList = new List<CardBuff>();

		_battleCardID = BattleCardData.RequestBattleCardID();

		if(!IsFaceDown)
		{
			_isActive = true;
        	InitCard();
		}

		/*
		if(this.CurrentPassiveIndex == PassiveIndex.PassiveType.AirUnit)
		{
			Debug.Log(this.Name + " is air unit.");
		}
		if(this.CurrentPassiveIndex == PassiveIndex.PassiveType.Piercing)
		{
			Debug.Log(this.Name + " is Piercing.");
		}
		if(this.CurrentPassiveIndex == PassiveIndex.PassiveType.Knockback)
		{
			Debug.Log(this.Name + " is Knockback.");
		}
		*/
	}

    // Copy Constructor.
    public BattleCardData(BattleCardData battleCardData)
    {
        _playerCardData = new PlayerCardData(battleCardData._playerCardData);
        _battleCardID = battleCardData._battleCardID;
		_ownerPlayer = battleCardData._ownerPlayer;
		_currentZone = battleCardData._currentZone;

		_currentShield = battleCardData._currentShield;
		_currentHP = battleCardData._currentHP;
		_currentAtk = battleCardData._currentAtk;
		_currentSpeed = battleCardData._currentSpeed;
        _currentPassiveIndex = battleCardData._currentPassiveIndex;
		_currentMove = battleCardData._currentMove;

		_abilityList = battleCardData._abilityList;
        _buffList = battleCardData._buffList;

		_isActive = battleCardData._isActive;
		_isFaceDown = battleCardData._isFaceDown;
		_isLeader = battleCardData._isLeader;  
    }

	/*
	~BattleCardData()
	{
		//DeinitCard();
	}
	*/
	#endregion

    #region Relational Operator Overloading
    public static bool operator ==(BattleCardData c1, BattleCardData c2)
    {
		if((System.Object)c1 == null && (System.Object)c2 == null) return true;
		else if((System.Object)c1 == null || (System.Object)c2 == null) return false;
        else return (c1.BattleCardID == c2.BattleCardID);
    }

    public static bool operator !=(BattleCardData c1, BattleCardData c2)
    {
        return !(c1 == c2);
    }

    public override bool Equals(System.Object obj)
    {
        // Check for null values and compare run-time types.
        if (obj == null || GetType() != obj.GetType())
            return false;

        BattleCardData c = (obj as BattleCardData);
        return (BattleCardID == c.BattleCardID);
    }

    public override int GetHashCode()
    {
        return 0;
    }
    #endregion

    #region Methods
	public void InitCard()
	{
		//Debug.Log("Init BattleCard:" + this.Name + ":" + this.BattleCardID);

		_InitAbility();
		_InitCard();
	}

    private void _InitCard()
    {
		ReactiveCard();
		ResetCardValue();
    }

	private void _InitAbility()
	{
		foreach(AbilityData.AbilityParams abilityParams in this.PlayerCardData.AbilityParamsList)
		{
			int index = _abilityList.Count;
			AbilityData abilityData = new AbilityData(this, index, abilityParams);
			_abilityList.Add(abilityData);
		}
	}

	public void DeinitCard()
	{
		//Debug.Log("DeinitCard");

		// Clear all ability.
		if(_abilityList != null && _abilityList.Count > 0)
		{
			//Debug.Log("DeinitCard 1");

			foreach(AbilityData ability in _abilityList)
			{
				ability.Deinit();
			}
				
			_abilityList.Clear();
		}

		// Clear all buff.
		if(_buffList != null && _buffList.Count > 0)
		{
			foreach(CardBuff buff in _buffList)
			{
				buff.TargetDestroyed();
			}
				
			_buffList.Clear();
		}
	}

	public void AttackTo(BattleCardData target, out List<BattleCompareUI.PerformAtkStep> stepList)
	{
    
		stepList = new List<BattleCompareUI.PerformAtkStep>();

		if(IsCanAttackTo(target))
		{
			bool isAttackSuccess = false;
			if(this.CurrentSpeed > target.CurrentSpeed)
			{
				// this attack first.
				{
					{
						// Create new step.
						BattleCompareUI.PerformAtkStep atkStep;

						// Create new actionList.
						List<BattleCompareUI.PerformAtkAction> actionList = new List<BattleCompareUI.PerformAtkAction>();

						BattleCompareUI.PerformAtkAction slashAction = BattleCompareUI.CreateSlashEffectAction(
							  BattleCompareUI.BattleSide.Target
							, true
						);

						// add action to list.
						actionList.Add(slashAction);

						// define action list to step.
						atkStep.ActionList = actionList;

						// add step to list.
						stepList.Add(atkStep);
					}

					{
						// Create new step.
						BattleCompareUI.PerformAtkStep atkStep;

						// Create new actionList.
						List<BattleCompareUI.PerformAtkAction> actionList = new List<BattleCompareUI.PerformAtkAction>();

						BattleCompareUI.PerformAtkAction atkAction = BattleCompareUI.CreateDamageAction(
							  BattleCompareUI.BattleSide.Target
							, this.CurrentAttack
							, this.CurrentPassiveIndex
						);

						// add action to list.
						actionList.Add(atkAction);

						// define action list to step.
						atkStep.ActionList = actionList;

						// add step to list.
						stepList.Add(atkStep);
					}
				}

				isAttackSuccess = target.GetBattleDamaged(this.CurrentAttack, this);

				if(target.IsDead)
				{
					{
						// Create new step.
						BattleCompareUI.PerformAtkStep atkStep;

						// Create new actionList.
						List<BattleCompareUI.PerformAtkAction> actionList = new List<BattleCompareUI.PerformAtkAction>();

						BattleCompareUI.PerformAtkAction atkAction = BattleCompareUI.CreateDestroyAction(
							BattleCompareUI.BattleSide.Target
							, true
						);

						// add action to list.
						actionList.Add(atkAction);

						// define action list to step.
						atkStep.ActionList = actionList;

						// add step to list.
						stepList.Add(atkStep);
					}
				}

				if(!target.IsDead && target.IsCanCounterTo(this))
				{
					{
						// Create new step.
						BattleCompareUI.PerformAtkStep atkStep;

						// Create new actionList.
						List<BattleCompareUI.PerformAtkAction> actionList = new List<BattleCompareUI.PerformAtkAction>();

						BattleCompareUI.PerformAtkAction slashAction = BattleCompareUI.CreateSlashEffectAction(
							BattleCompareUI.BattleSide.Attacker
							, true
						);

						// add action to list.
						actionList.Add(slashAction);

						// define action list to step.
						atkStep.ActionList = actionList;

						// add step to list.
						stepList.Add(atkStep);
					}

					{
						// Create new step.
						BattleCompareUI.PerformAtkStep atkStep;

						// Create new actionList.
						List<BattleCompareUI.PerformAtkAction> actionList = new List<BattleCompareUI.PerformAtkAction>();

						BattleCompareUI.PerformAtkAction atkAction = BattleCompareUI.CreateDamageAction(
							BattleCompareUI.BattleSide.Attacker
							, target.CurrentAttack
							, target.CurrentPassiveIndex
						);

						// add action to list.
						actionList.Add(atkAction);

						// define action list to step.
						atkStep.ActionList = actionList;

						// add step to list.
						stepList.Add(atkStep);
					}

					this.GetBattleDamaged(target.CurrentAttack, target);

					if(this.IsDead)
					{
						{
							// Create new step.
							BattleCompareUI.PerformAtkStep atkStep;

							// Create new actionList.
							List<BattleCompareUI.PerformAtkAction> actionList = new List<BattleCompareUI.PerformAtkAction>();

							BattleCompareUI.PerformAtkAction atkAction = BattleCompareUI.CreateDestroyAction(
								BattleCompareUI.BattleSide.Attacker
								, true
							);

							// add action to list.
							actionList.Add(atkAction);

							// define action list to step.
							atkStep.ActionList = actionList;

							// add step to list.
							stepList.Add(atkStep);
						}
					}
				}
			}
			else if(this.CurrentSpeed < target.CurrentSpeed)
			{
				// this attack last.
				{
					{
						// Create new step.
						BattleCompareUI.PerformAtkStep atkStep;

						// Create new actionList.
						List<BattleCompareUI.PerformAtkAction> actionList = new List<BattleCompareUI.PerformAtkAction>();

						BattleCompareUI.PerformAtkAction slashAction = BattleCompareUI.CreateSlashEffectAction(
							BattleCompareUI.BattleSide.Attacker
							, true
						);

						// add action to list.
						actionList.Add(slashAction);

						// define action list to step.
						atkStep.ActionList = actionList;

						// add step to list.
						stepList.Add(atkStep);
					}

					{
						// Create new step.
						BattleCompareUI.PerformAtkStep atkStep;

						// Create new actionList.
						List<BattleCompareUI.PerformAtkAction> actionList = new List<BattleCompareUI.PerformAtkAction>();

						BattleCompareUI.PerformAtkAction atkAction = BattleCompareUI.CreateDamageAction(
							BattleCompareUI.BattleSide.Attacker
							, target.CurrentAttack
							, target.CurrentPassiveIndex
						);

						// add action to list.
						actionList.Add(atkAction);

						// define action list to step.
						atkStep.ActionList = actionList;

						// add step to list.
						stepList.Add(atkStep);
					}
				}
					
				this.GetBattleDamaged(target.CurrentAttack, target);

				if(this.IsDead)
				{
					// Create new step.
					BattleCompareUI.PerformAtkStep atkStep;

					// Create new actionList.
					List<BattleCompareUI.PerformAtkAction> actionList = new List<BattleCompareUI.PerformAtkAction>();

					BattleCompareUI.PerformAtkAction atkAction = BattleCompareUI.CreateDestroyAction(
						BattleCompareUI.BattleSide.Attacker
						, true
					);

					// add action to list.
					actionList.Add(atkAction);

					// define action list to step.
					atkStep.ActionList = actionList;

					// add step to list.
					stepList.Add(atkStep);
				}

				if(!this.IsDead && this.IsCanCounterTo(target))
				{
					{
						// Create new step.
						BattleCompareUI.PerformAtkStep atkStep;

						// Create new actionList.
						List<BattleCompareUI.PerformAtkAction> actionList = new List<BattleCompareUI.PerformAtkAction>();

						BattleCompareUI.PerformAtkAction slashAction = BattleCompareUI.CreateSlashEffectAction(
							BattleCompareUI.BattleSide.Target
							, true
						);

						// add action to list.
						actionList.Add(slashAction);

						// define action list to step.
						atkStep.ActionList = actionList;

						// add step to list.
						stepList.Add(atkStep);
					}

					{
						// Create new step.
						BattleCompareUI.PerformAtkStep atkStep;

						// Create new actionList.
						List<BattleCompareUI.PerformAtkAction> actionList = new List<BattleCompareUI.PerformAtkAction>();

						BattleCompareUI.PerformAtkAction atkAction = BattleCompareUI.CreateDamageAction(
							BattleCompareUI.BattleSide.Target
							, this.CurrentAttack
							, this.CurrentPassiveIndex
						);

						// add action to list.
						actionList.Add(atkAction);

						// define action list to step.
						atkStep.ActionList = actionList;

						// add step to list.
						stepList.Add(atkStep);
					}

					isAttackSuccess = target.GetBattleDamaged(this.CurrentAttack, this);

					if(target.IsDead)
					{
						// Create new step.
						BattleCompareUI.PerformAtkStep atkStep;

						// Create new actionList.
						List<BattleCompareUI.PerformAtkAction> actionList = new List<BattleCompareUI.PerformAtkAction>();

						BattleCompareUI.PerformAtkAction atkAction = BattleCompareUI.CreateDestroyAction(
							BattleCompareUI.BattleSide.Target
							, true
						);

						// add action to list.
						actionList.Add(atkAction);

						// define action list to step.
						atkStep.ActionList = actionList;

						// add step to list.
						stepList.Add(atkStep);
					}
				}
			}
			else
			{
				{
					// Create new step.
					BattleCompareUI.PerformAtkStep atkStep;

					// Create new actionList.
					List<BattleCompareUI.PerformAtkAction> actionList = new List<BattleCompareUI.PerformAtkAction>();

					BattleCompareUI.PerformAtkAction slashAction1 = BattleCompareUI.CreateSlashEffectAction(
						BattleCompareUI.BattleSide.Target
						, true
					);

					BattleCompareUI.PerformAtkAction slashAction2 = BattleCompareUI.CreateSlashEffectAction(
						BattleCompareUI.BattleSide.Attacker
						, true
					);

					// add action to list.
					actionList.Add(slashAction1);
					actionList.Add(slashAction2);

					// define action list to step.
					atkStep.ActionList = actionList;

					// add step to list.
					stepList.Add(atkStep);
				}

				// Equal
				{
					// Create new step.
					BattleCompareUI.PerformAtkStep atkStep;

					// Create new actionList.
					List<BattleCompareUI.PerformAtkAction> actionList = new List<BattleCompareUI.PerformAtkAction>();

					BattleCompareUI.PerformAtkAction atkAction1 = BattleCompareUI.CreateDamageAction(
						BattleCompareUI.BattleSide.Target
						, this.CurrentAttack
						, this.CurrentPassiveIndex
					);

					BattleCompareUI.PerformAtkAction atkAction2 = BattleCompareUI.CreateDamageAction(
						BattleCompareUI.BattleSide.Attacker
						, target.CurrentAttack
						, target.CurrentPassiveIndex
					);

					// add action to list.
					actionList.Add(atkAction1);
					actionList.Add(atkAction2);

					// define action list to step.
					atkStep.ActionList = actionList;

					// add step to list.
					stepList.Add(atkStep);
				}

				this.GetBattleDamaged(target.CurrentAttack, target);
				isAttackSuccess = target.GetBattleDamaged(this.CurrentAttack, this);

				if(this.IsDead || target.IsDead)
				{
					// Create new step.
					BattleCompareUI.PerformAtkStep atkStep;

					// Create new actionList.
					List<BattleCompareUI.PerformAtkAction> actionList = new List<BattleCompareUI.PerformAtkAction>();

					if(this.IsDead)
					{
						BattleCompareUI.PerformAtkAction atkAction = BattleCompareUI.CreateDestroyAction(
							BattleCompareUI.BattleSide.Attacker
							, true
						);

						// add action to list.
						actionList.Add(atkAction);
					}

					if(target.IsDead)
					{
						BattleCompareUI.PerformAtkAction atkAction = BattleCompareUI.CreateDestroyAction(
							BattleCompareUI.BattleSide.Target
							, true
						);

						// add action to list.
						actionList.Add(atkAction);
					}

					// define action list to step.
					atkStep.ActionList = actionList;

					// add step to list.
					stepList.Add(atkStep);
				}
			}

			this._isActive = false;

			if(isAttackSuccess)
			{
				TriggerOnAttackSuccessEvent();

				if(target.TypeData == CardTypeData.CardType.Base)
				{
					TriggerOnAttackBaseSuccessEvent();
				}
			}
		}
	}

	public bool IsCanAttackTo(BattleCardData target)
	{
		if(!IsCanAttack) return false;

		if(	   (target.TypeData != CardTypeData.CardType.Base) 
			&& (target.TypeData != CardTypeData.CardType.Structure) 
			&& (target.TypeData != CardTypeData.CardType.Unit) 
		)
		{
			return false;
		}

		if(target.IsFaceDown || target.IsDead || target.IsDestroy) 
		{
			return false;
		}

		if(this.CurrentPassiveIndex != PassiveIndex.PassiveType.AirUnit)
		{
			if(target.CurrentPassiveIndex == PassiveIndex.PassiveType.AirUnit)
			{
				return false;
			}
		}

		//Debug.Log(this.CurrentAttackRange.ToString() + " " + target.Name);
			
		if(this.CurrentAttackRange >= 2)
		{
			return true;
		}
		else if(this.CurrentAttackRange >= 1)
		{
			switch(this.CurrentZone)
			{
				case BattleZoneIndex.Battlefield: return true;
				
				case BattleZoneIndex.BaseP1:
				{
					if(target.CurrentZone == BattleZoneIndex.BaseP1 || target.CurrentZone == BattleZoneIndex.Battlefield)
					{
						return true;
					}
					else
					{
						return false;
					}
				}

				case BattleZoneIndex.BaseP2:
				{
					if(target.CurrentZone == BattleZoneIndex.BaseP2 || target.CurrentZone == BattleZoneIndex.Battlefield)
					{
						return true;
					}
					else
					{
						return false;
					}
				}

				default: return false;
			}
		}
		else if(this.CurrentAttackRange >= 0)
		{
			if(this.CurrentZone == target.CurrentZone)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	public bool IsCanCounterTo(BattleCardData target)
	{
		if(!IsCanCounter) return false;

		if(	   (target.TypeData != CardTypeData.CardType.Base) 
			&& (target.TypeData != CardTypeData.CardType.Structure) 
			&& (target.TypeData != CardTypeData.CardType.Unit) 
		)
		{
			return false;
		}

		if(target.IsFaceDown) 
		{
			return false;
		}

		//Debug.Log(this.CurrentAttackRange.ToString() + " " + target.Name);

		if(this.CurrentAttackRange >= 2)
		{
			return true;
		}
		else if(this.CurrentAttackRange >= 1)
		{
			switch(this.CurrentZone)
			{
			case BattleZoneIndex.Battlefield: return true;

			case BattleZoneIndex.BaseP1:
				{
					if(target.CurrentZone == BattleZoneIndex.BaseP1 || target.CurrentZone == BattleZoneIndex.Battlefield)
					{
						return true;
					}
					else
					{
						return false;
					}
				}

			case BattleZoneIndex.BaseP2:
				{
					if(target.CurrentZone == BattleZoneIndex.BaseP2 || target.CurrentZone == BattleZoneIndex.Battlefield)
					{
						return true;
					}
					else
					{
						return false;
					}
				}

			default: return false;
			}
		}
		else if(this.CurrentAttackRange >= 0)
		{
			if(this.CurrentZone == target.CurrentZone)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	private bool GetBattleDamaged(int damage, BattleCardData attacker)
	{
		if(damage <= 0) return false;

		//Debug.Log(this.Name + " get damaged " + damage.ToString());

		if(attacker != null && attacker.CurrentPassiveIndex == PassiveIndex.PassiveType.Piercing)
		{
			// Ignore Sheild.
			//Debug.Log("Damage is piercing effect.");

			int realDamage = damage;
			_currentHP -= realDamage;
		}
		else
		{
			if(damage <= _currentShield)
			{
				_currentShield -= damage;
			}
			else
			{
				int realDamage = damage - _currentShield;
				_currentShield = 0;
				_currentHP -= realDamage;
			}
		}

		if(attacker != null && attacker.CurrentPassiveIndex == PassiveIndex.PassiveType.Knockback)
		{
			if(!this.IsDead)
			{
				// Have push back.
				BattleZoneIndex backZone;
				bool isCan = IsCanForceMoveBackward(out backZone);
				if(isCan)
				{
					BattleManager.Instance.RequestMoveBattleCard(
						  this.BattleCardID
						, backZone
						, false
                        , true
					);
				}
			}
		}

		TriggerOnDamageEvent();
		return true;
	}

	public bool GetDamaged(int damage, bool isPiercing = false)
	{
		if(damage <= 0) return false;

		//Debug.Log(this.Name + " get damaged " + damage.ToString());

		if(isPiercing)
		{
			// Ignore Sheild.
			//Debug.Log("Damage is piercing effect.");

			int realDamage = damage;
			_currentHP -= realDamage;
		}
		else
		{
			if(damage <= _currentShield)
			{
				_currentShield -= damage;
			}
			else
			{
				int realDamage = damage - _currentShield;
				_currentShield = 0;
				_currentHP -= realDamage;
			}
		}

		TriggerOnDamageEvent();
		return true;
	}

	private void TriggerOnDamageEvent()
	{
		if(BattleManager.Instance != null)
		{
			//Debug.Log("TriggerOnDamageEvent");

			BattleManager.Instance.OnBattleEventTrigger(
				this.OwnerPlayerIndex
				, BattleEventTrigger.BattleTriggerType.CARD_BE_DAMAGED
				, this.BattleCardID
			);

			BattleManager.Instance.OnBattleEventTrigger(
				this.OwnerPlayerIndex
				, BattleEventTrigger.BattleTriggerType.CARD_BE_DAMAGED
				, BattleEventTrigger.BattleSide.Friendly
			);


			PlayerIndex enemyIndex;
			if(this.OwnerPlayerIndex == PlayerIndex.One)
			{
				enemyIndex = PlayerIndex.Two;
			}
			else
			{
				enemyIndex = PlayerIndex.One;
			}

			BattleManager.Instance.OnBattleEventTrigger(
				enemyIndex
				, BattleEventTrigger.BattleTriggerType.CARD_BE_DAMAGED
				, BattleEventTrigger.BattleSide.Enemy
			);
		}

	}

	private void TriggerOnAttackSuccessEvent()
	{
		if(BattleManager.Instance != null)
		{
			BattleManager.Instance.OnBattleEventTrigger(
				this.OwnerPlayerIndex
				, BattleEventTrigger.BattleTriggerType.CARD_ATTACK_SUCCESSED
				, this.BattleCardID
			);

			BattleManager.Instance.OnBattleEventTrigger(
				this.OwnerPlayerIndex
				, BattleEventTrigger.BattleTriggerType.CARD_ATTACK_SUCCESSED
				, BattleEventTrigger.BattleSide.Friendly
			);


			PlayerIndex enemyIndex;
			if(this.OwnerPlayerIndex == PlayerIndex.One)
			{
				enemyIndex = PlayerIndex.Two;
			}
			else
			{
				enemyIndex = PlayerIndex.One;
			}

			BattleManager.Instance.OnBattleEventTrigger(
				enemyIndex
				, BattleEventTrigger.BattleTriggerType.CARD_ATTACK_SUCCESSED
				, BattleEventTrigger.BattleSide.Enemy
			);
		}
	}

	private void TriggerOnAttackBaseSuccessEvent()
	{
		if(BattleManager.Instance != null)
		{
			BattleManager.Instance.OnBattleEventTrigger(
				this.OwnerPlayerIndex
				, BattleEventTrigger.BattleTriggerType.CARD_ATTACK_BASE_SUCCESSED
				, this.BattleCardID
			);

			BattleManager.Instance.OnBattleEventTrigger(
				this.OwnerPlayerIndex
				, BattleEventTrigger.BattleTriggerType.CARD_ATTACK_BASE_SUCCESSED
				, BattleEventTrigger.BattleSide.Friendly
			);


			PlayerIndex enemyIndex;
			if(this.OwnerPlayerIndex == PlayerIndex.One)
			{
				enemyIndex = PlayerIndex.Two;
			}
			else
			{
				enemyIndex = PlayerIndex.One;
			}

			BattleManager.Instance.OnBattleEventTrigger(
				enemyIndex
				, BattleEventTrigger.BattleTriggerType.CARD_ATTACK_BASE_SUCCESSED
				, BattleEventTrigger.BattleSide.Enemy
			);
		}
	}

	public void TriggerOnLeaveEvent()
	{
		if(BattleManager.Instance != null)
		{
			//Debug.Log("TriggerOnLeaveEvent");

			BattleManager.Instance.OnBattleEventTrigger(
				this.OwnerPlayerIndex
				, BattleEventTrigger.BattleTriggerType.CARD_LEAVED
				, this.BattleCardID
			);

			BattleManager.Instance.OnBattleEventTrigger(
				this.OwnerPlayerIndex
				, BattleEventTrigger.BattleTriggerType.CARD_LEAVED
				, BattleEventTrigger.BattleSide.Friendly
			);


			PlayerIndex enemyIndex;
			if(this.OwnerPlayerIndex == PlayerIndex.One)
			{
				enemyIndex = PlayerIndex.Two;
			}
			else
			{
				enemyIndex = PlayerIndex.One;
			}

			BattleManager.Instance.OnBattleEventTrigger(
				enemyIndex
				, BattleEventTrigger.BattleTriggerType.CARD_LEAVED
				, BattleEventTrigger.BattleSide.Enemy
			);
		}
	}

	public void TriggerOnDestroyEvent()
	{
		//Debug.Log("TriggerOnDestroyEvent");
		if(BattleManager.Instance != null)
		{
			BattleManager.Instance.OnBattleEventTrigger(
				this.OwnerPlayerIndex
				, BattleEventTrigger.BattleTriggerType.CARD_BE_DESTROYED
				, this.BattleCardID
			);

			BattleManager.Instance.OnBattleEventTrigger(
				this.OwnerPlayerIndex
				, BattleEventTrigger.BattleTriggerType.CARD_BE_DESTROYED
				, BattleEventTrigger.BattleSide.Friendly
			);


			PlayerIndex enemyIndex;
			if(this.OwnerPlayerIndex == PlayerIndex.One)
			{
				enemyIndex = PlayerIndex.Two;
			}
			else
			{
				enemyIndex = PlayerIndex.One;
			}

			BattleManager.Instance.OnBattleEventTrigger(
				enemyIndex
				, BattleEventTrigger.BattleTriggerType.CARD_BE_DESTROYED
				, BattleEventTrigger.BattleSide.Enemy
			);
		}
	}

	public bool IsCanAttackSuccess(BattleCardData target)
	{
		if(IsCanAttackTo(target))
		{
			if(target.TypeData == CardTypeData.CardType.Base)
			{
				return true;
			}

			if(target.CurrentSpeed > this.CurrentSpeed)
			{
				// me atk last
				if(IsICanKillIt(target) && !IsItCanKillMe(target))
				{
					// Can killed and survive from damaged.
					if(!target.IsICanKnockbackIt(this))
					{
						// target can't knockback.
						return true;
					}
					else
					{
						/*
						// Target can knockback
						if(this.IsICanCounterAfterGetKnockbackBy(target))
						{
							// I can counter it.
							return true;
						}
						else
						{
							// I can't counter.
						}
						*/
					}
				}
			}
			else if(target.CurrentSpeed < this.CurrentSpeed)
			{
				// me atk first.
				if(IsICanKillIt(target))
				{
					// Can killed
					return true;
				}
				else if(IsICanKnockbackIt(target))
				{
					// Can knockback first
					//if(!target.IsICanCounterAfterGetKnockbackBy(this))
					{
						//  target can't counter after get knockback.
						return true;
					}
				}
			}
			else if(target.CurrentSpeed == this.CurrentSpeed)
			{
				// Same speed.
				if(IsICanKillIt(target) || !IsItCanKillMe(target))
				{
					// Can killed or survive from damaged.
					return true;
				}
			}
		}

		return false;
	}

	public bool IsItCanKillMe(BattleCardData target)
	{
		if(target.CurrentPassiveIndex == PassiveIndex.PassiveType.Piercing)
		{
			// Piercing
			if(target.CurrentAttack >= this.CurrentHP)
			{
				return true;
			}
		}
		else
		{
			if(target.CurrentAttack >= (this.CurrentHP + this.CurrentShield))
			{
				return true;
			}
		}

		return false;
	}

	public bool IsICanKill()
	{
		PlayerIndex enemyIndex;
		if(OwnerPlayerIndex == PlayerIndex.One) enemyIndex = PlayerIndex.Two;
		else 									enemyIndex = PlayerIndex.One;

		List<BattleCardData> enemyList;
		BattleManager.Instance.GetTargetList(
			  enemyIndex
			, out enemyList
			, (BattleCardData card) => this.IsICanKillIt(card) && this.IsCanAttackSuccess(card)
		);

		if(enemyList != null && enemyList.Count > 0)
		{
			return true;
		}
			
		return false;
	}

	public bool IsICanAttackKillit(BattleCardData target)
	{
		return (this.IsCanAttackSuccess(target) && IsICanKillIt(target));
	}

	public bool IsICanKillIt(BattleCardData target)
	{
		if(this.CurrentPassiveIndex == PassiveIndex.PassiveType.Piercing)
		{
			// Piercing
			if(this.CurrentAttack >= target.CurrentHP)
			{
				return true;
			}
		}
		else
		{
			if(this.CurrentAttack >= (target.CurrentHP + target.CurrentShield))
			{
				return true;
			}
		}

		return false;
	}

	public bool IsICanKnockbackIt(BattleCardData target)
	{
		if(IsCanAttackTo(target) && (this.CurrentPassiveIndex == PassiveIndex.PassiveType.Knockback))
		{
			switch(target.CurrentZone)
			{
				case BattleZoneIndex.BaseP1:
				{
					if(target.OwnerPlayerIndex != PlayerIndex.One)
					{
						return true;
					}
				}
				break;

				case BattleZoneIndex.BaseP2:
				{
					if(target.OwnerPlayerIndex != PlayerIndex.Two)
					{
						return true;
					}
				}
				break;

				case BattleZoneIndex.Battlefield:
				return true;
			}
		}

		return false;
	}

	/*
	public bool IsICanCounterAfterGetKnockbackBy(BattleCardData target)
	{
		if(target.IsICanKnockbackIt(this))
		{
			// Can knock back me.
			if(this._currentAttackRange >= 2)
			{
				return true;
			}
			else if(this._currentAttackRange >= 1)
			{
				if(this.CurrentZone == target.CurrentZone)
				{
					return true;
				}
				else if(target.CurrentZone == BattleZoneIndex.Battlefield)
				{
					return true;
				}
			}

			return false;
		}
		else
		{
			// Can't knockback me.
			if(this._currentAttackRange >= 2)
			{
				return true;
			}
			else if(this._currentAttackRange >= 1)
			{
				if(this.CurrentZone == target.CurrentZone)
				{
					return true;
				}
				else if(target.CurrentZone == BattleZoneIndex.Battlefield)
				{
					return true;
				}
				else if(this.CurrentZone == BattleZoneIndex.Battlefield)
				{
					return true;
				}
			}
			else if(this._currentAttackRange >= 0)
			{
				if(this.CurrentZone == target.CurrentZone)
				{
					return true;
				}
			}

			return false;
		}
	}
	*/

	public bool IsCanMoveForward()
	{
		BattleZoneIndex forwardZoneIndex;
		return IsCanMoveForward(out forwardZoneIndex);
	}

	public bool IsCanMoveForward(out BattleZoneIndex forwardZoneIndex)
	{
		if(IsCanMove)
		{
			return IsCanForceMoveForward(out forwardZoneIndex);
		}

		forwardZoneIndex = BattleZoneIndex.Battlefield;
		return false;
	}

	public bool IsCanForceMoveForward(out BattleZoneIndex forwardZoneIndex)
	{
		switch(this.OwnerPlayerIndex)
		{
			case PlayerIndex.One:
			{
				switch(this.CurrentZone)
				{
					case BattleZoneIndex.BaseP1:		forwardZoneIndex = BattleZoneIndex.Battlefield; return true;
					case BattleZoneIndex.Battlefield: 	forwardZoneIndex = BattleZoneIndex.BaseP2; return true;
					case BattleZoneIndex.BaseP2:		forwardZoneIndex = BattleZoneIndex.BaseP2; return false;
				}
			}
			break;

			case PlayerIndex.Two:
			{
				switch(this.CurrentZone)
				{
					case BattleZoneIndex.BaseP1:		forwardZoneIndex = BattleZoneIndex.BaseP1; return false;
					case BattleZoneIndex.Battlefield: 	forwardZoneIndex = BattleZoneIndex.BaseP1; return true;
					case BattleZoneIndex.BaseP2:		forwardZoneIndex = BattleZoneIndex.Battlefield; return true;
				}
			}
			break;
		}

		forwardZoneIndex = BattleZoneIndex.BaseP1;
		return false;
	}

	public bool IsCanMoveBackward()
	{
		BattleZoneIndex backwardZoneIndex;
		return IsCanMoveBackward(out backwardZoneIndex);
	}

	public bool IsCanMoveBackward(out BattleZoneIndex backwardZoneIndex)
	{
		if(IsCanMove)
		{
			return IsCanForceMoveBackward(out backwardZoneIndex);
		}

		backwardZoneIndex = BattleZoneIndex.Battlefield;
		return false;
	}

	public bool IsCanForceMoveBackward(out BattleZoneIndex backwardZoneIndex)
	{
		switch(this.CurrentZone)
		{
			case BattleZoneIndex.BaseP1:
			{
				if(this.OwnerPlayerIndex == PlayerIndex.One)
				{
					// Can't forward.
				}
				else
				{
					backwardZoneIndex = BattleZoneIndex.Battlefield;
					return true;
				}	
			}
			break;

			case BattleZoneIndex.BaseP2:
			{
				if(this.OwnerPlayerIndex == PlayerIndex.One)
				{
					backwardZoneIndex = BattleZoneIndex.Battlefield;
					return true;
				}
				else
				{
					// Can't backward.
				}
			}
			break;

			case BattleZoneIndex.Battlefield:
			{
				if(this.OwnerPlayerIndex == PlayerIndex.One)
				{
					backwardZoneIndex = BattleZoneIndex.BaseP1;
				}
				else
				{
					backwardZoneIndex = BattleZoneIndex.BaseP2;
				}

				return true;
			}
			break;
		}

		backwardZoneIndex = BattleZoneIndex.Battlefield;
		return false;
	}

    /*
	public void MoveForward()
	{
		switch(this.CurrentZone)
		{
			case BattleZoneIndex.BaseP1:
			case BattleZoneIndex.BaseP2:
			{
				MoveTo(BattleZoneIndex.Battlefield);
			}
			break;

			case BattleZoneIndex.Battlefield:
			{
				if(this.OwnerPlayerIndex == PlayerIndex.One)
				{
					MoveTo(BattleZoneIndex.BaseP2);
				}
				else
				{
					MoveTo(BattleZoneIndex.BaseP1);
				}
			}
			break;
		}
	}

	public void MoveBackward()
	{
		switch(this.CurrentZone)
		{
			case BattleZoneIndex.BaseP1:
			{
				if(this.OwnerPlayerIndex == PlayerIndex.One)
				{
					//Can't go back.
				}
				else
				{
					MoveTo(BattleZoneIndex.Battlefield);
				}
			}
			break;

			case BattleZoneIndex.BaseP2:
			{
				if(this.OwnerPlayerIndex == PlayerIndex.One)
				{
					MoveTo(BattleZoneIndex.Battlefield);
				}
				else
				{
					//Can't go back.
				}
			}
			break;

			case BattleZoneIndex.Battlefield:
			{
				if(this.OwnerPlayerIndex == PlayerIndex.One)
				{
					MoveTo(BattleZoneIndex.BaseP1);
				}
				else
				{
					MoveTo(BattleZoneIndex.BaseP2);
				}
			}
			break;
		}
	}
	*/

    public void ForceMoveTo(BattleZoneIndex targetZone)
	{
		//Debug.Log("ForceMove");
		_currentZone = targetZone;
	}

	public void MoveTo(BattleZoneIndex targetZone)
	{
		if(IsCanMove)
		{
			_currentZone = targetZone;

			--_currentMove;

			if(BattleManager.Instance != null)
			{
				BattleManager.Instance.OnBattleEventTrigger(
					  this.OwnerPlayerIndex
					, BattleEventTrigger.BattleTriggerType.CARD_BE_MOVED
					, this.BattleCardID
				);

				BattleManager.Instance.OnBattleEventTrigger(
					  this.OwnerPlayerIndex
					, BattleEventTrigger.BattleTriggerType.CARD_BE_MOVED
					, BattleEventTrigger.BattleSide.Friendly
				);

				PlayerIndex enemyIndex;
				if(this.OwnerPlayerIndex == PlayerIndex.One)
				{
					enemyIndex = PlayerIndex.Two;
				}
				else
				{
					enemyIndex = PlayerIndex.One;
				}

				BattleManager.Instance.OnBattleEventTrigger(
					  enemyIndex
					, BattleEventTrigger.BattleTriggerType.CARD_BE_MOVED
					, BattleEventTrigger.BattleSide.Enemy
				);
			}
		}
	}
		
	public void FaceUp()
	{
		if(IsFaceDown)
		{
			_isFaceDown = false;
			InitCard();

			if(BattleManager.Instance != null)
			{
				BattleManager.Instance.OnBattleEventTrigger(
					  this.OwnerPlayerIndex
					, BattleEventTrigger.BattleTriggerType.CARD_BE_SUMMONED
					, this.BattleCardID
				);

				BattleManager.Instance.OnBattleEventTrigger(
					  this.OwnerPlayerIndex
					, BattleEventTrigger.BattleTriggerType.CARD_BE_SUMMONED
					, BattleEventTrigger.BattleSide.Friendly
				);
					
				PlayerIndex enemyIndex;
				if(this.OwnerPlayerIndex == PlayerIndex.One)
				{
					enemyIndex = PlayerIndex.Two;
				}
				else
				{
					enemyIndex = PlayerIndex.One;
				}

				BattleManager.Instance.OnBattleEventTrigger(
					  enemyIndex
					, BattleEventTrigger.BattleTriggerType.CARD_BE_SUMMONED
					, BattleEventTrigger.BattleSide.Enemy
				);
			}
		}
	}

	public void ReactiveCard()
	{
		_isActive = true;

		// reset move
		_currentMove = this.BaseMove + this.BonusMove;

		RequestUpdateBattleCardUI();
	}

	public void SetCardActive(bool isActive)
	{
		_isActive = isActive;

		RequestUpdateBattleCardUI();
	}

    public void ResetCard()
	{
		ResetCardValue();

		RequestUpdateBattleCardUI();

        //_isActive = true;
    }

	private void ResetCardValue()
	{
		_currentShield = this.BaseShield + this.BonusShield;
		_currentHP = this.BaseHP + this.BonusHP;
		_currentAtk = this.BaseAttack + this.BonusAttack;
		_currentSpeed = this.BaseSpeed + this.BonusSpeed;
		//_currentMove = this.BaseMove + this.BonusMove;
		_currentAttackRange = this.BaseAttackRange + this.BonusAttackRange;
		_currentPassiveIndex = this.BasePassiveIndex + this.BonusPassiveIndex;
	}

    public static int RequestBattleCardID()
    {
        return ++_battleCardIndex;
    }
    
    public static void ResetBattleCardIndex()
    {
       _battleCardIndex = 0;
    }

	public void RequestUpdateBattleCardUI()
	{
		if(BattleManager.Instance != null)
		{
			BattleManager.Instance.RequestUpdateBattleCardUI(this.BattleCardID);
		}
	}
    #endregion

    #region BattleCard Compare

    // Properties

    public bool IsCardID(string cardID)
    {
        return (string.Compare(this.CardID, cardID) == 0);
    }

    public bool IsCardType(CardTypeData typeData)
    {
        return (this.TypeData == typeData);
    }

    public bool IsCardType(CardTypeData.CardType cardType)
    {
        return (this.TypeData == cardType);
    }

    public bool IsCardClan(CardClanData clanData)
    {
        return (this.ClanData == clanData);
    }

    public bool IsCardClan(CardClanData.CardClanType clan)
    {
        return (this.ClanData == clan);
    }

    public bool IsCardSubType(CardSubTypeData subTypeData)
    {
        foreach (CardSubTypeData subType in this.SubTypeDataList)
        {
            if (subType == subTypeData)
            {
                return true;
            }
        }

        return false;
    }

    public bool IsCardSubType(CardSubTypeData.CardSubType cardSubType)
    {
        foreach (CardSubTypeData subType in this.SubTypeDataList)
        {
            if (subType == cardSubType)
            {
                return true;
            }
        }
        return false;
    }

    public bool IsCardJobType(CardJobData jobData)
    {
        foreach (CardJobData cardJob in this.JobDataList)
        {
            if (cardJob == jobData)
            {
                return true;
            }
        }

        return false;
    }

    public bool IsCardJobType(CardJobData.JobType job)
    {
        foreach (CardJobData cardJob in this.JobDataList)
        {
            if (cardJob == job)
            {
                return true;
            }
        }
        return false;
    }

    public bool IsCardSymbol(CardSymbolData symbolData)
    {
        return (this.SymbolData == symbolData);
    }

    public bool IsCardSymbol(CardSymbolData.CardSymbol symbol)
    {
        return (this.SymbolData == symbol);
    }

    // Costs

    public bool IsCardCostEqual(int cost)
    {
        return (this.Cost == cost);
    }

    public bool IsCardCostMoreEqual(int cost)
    {
        return (this.Cost >= cost);
    }

    public bool IsCardCostMoreThan(int cost)
    {
        return (this.Cost > cost);
    }

    public bool IsCardCostLessEqual(int cost)
    {
        return (this.Cost <= cost);
    }

    public bool IsCardCostLessThan(int cost)
    {
        return (this.Cost < cost);
    }

    // Shield

    public bool IsCardShieldEqual(int shield)
    {
        return (this.CurrentShield == shield);
    }

    public bool IsCardShieldMoreEqual(int shield)
    {
        return (this.CurrentShield >= shield);
    }

    public bool IsCardShieldMoreThan(int shield)
    {
        return (this.CurrentShield > shield);
    }

    public bool IsCardShieldLessEqual(int shield)
    {
        return (this.CurrentShield <= shield);
    }

    public bool IsCardShieldLessThan(int shield)
    {
        return (this.CurrentShield < shield);
    }

    // HP

    public bool IsCardHPEqual(int hp)
    {
        return (this.CurrentHP == hp);
    }

    public bool IsCardHPMoreEqual(int hp)
    {
        return (this.CurrentHP >= hp);
    }

    public bool IsCardHPMoreThan(int hp)
    {
        return (this.CurrentHP > hp);
    }

    public bool IsCardHPLessEqual(int hp)
    {
        return (this.CurrentHP <= hp);
    }

    public bool IsCardHPLessThan(int hp)
    {
        return (this.CurrentHP < hp);
    }

    // Attack

    public bool IsCardAtkEqual(int atk)
    {
        return (this.CurrentAttack == atk);
    }

    public bool IsCardAtkMoreEqual(int atk)
    {
        return (this.CurrentAttack >= atk);
    }

    public bool IsCardAtkMoreThan(int atk)
    {
        return (this.CurrentAttack > atk);
    }

    public bool IsCardAtkLessEqual(int atk)
    {
        return (this.CurrentAttack <= atk);
    }

    public bool IsCardAtkLessThan(int atk)
    {
        return (this.CurrentAttack < atk);
    }

    // Speed

    public bool IsCardSpeedEqual(int speed)
    {
        return (this.CurrentSpeed == speed);
    }

    public bool IsCardSpeedMoreEqual(int speed)
    {
        return (this.CurrentSpeed >= speed);
    }

    public bool IsCardSpeedMoreThan(int speed)
    {
        return (this.CurrentSpeed > speed);
    }

    public bool IsCardSpeedLessEqual(int speed)
    {
        return (this.CurrentSpeed <= speed);
    }

    public bool IsCardSpeedLessThan(int speed)
    {
        return (this.CurrentSpeed < speed);
    }

    // Status
    public bool IsAirUnit()
    {
        return (this.CurrentPassiveIndex == PassiveIndex.PassiveType.AirUnit);
    }

    public bool IsKnockback()
    {
        return (this.CurrentPassiveIndex == PassiveIndex.PassiveType.Knockback);
    }

    public bool IsPiercing()
    {
        return (this.CurrentPassiveIndex == PassiveIndex.PassiveType.Piercing);
    }

    public bool IsCardOwner(PlayerIndex ownerIndex)
    {
        return (this.OwnerPlayerIndex == ownerIndex);
    }

    public bool IsCardLeader()
    {
        return this.IsLeader;
    }

    public bool IsCardFacedown()
    {
        return this.IsFaceDown;
    }

    public bool IsCardCanMove()
    {
        return this.IsCanMove;
    }

    public bool IsCardCanAtk()
    {
        return this.IsCanAttack;
    }

    public bool IsCardCanAct()
    {
        return !this.IsActive;
    }

    public bool IsCardBattleZone(BattleZoneIndex battleZone)
    {
        return (this.CurrentZone == battleZone);
    }
    #endregion

	#region Ability Methods
	public bool GetCanUseAbilityList(out List<AbilityData> abilityList)
	{
		abilityList = new List<AbilityData>();

		if(IsCanUseAbility())
		{
			foreach(AbilityData ability in _abilityList)
			{
				if(ability.IsCanUse() && ability.TriggerIndex == TriggerIndex.TriggerType.Act)
				{
					abilityList.Add(ability);
				}
			}

			return true;
		}

		abilityList = null;
		return false;
	}

	public bool IsCanUseAbility()
	{
		if(AbilityList != null && AbilityList.Count > 0)
		{
			foreach(AbilityData ability in AbilityList)
			{
				if(ability.IsCanUse() && ability.TriggerIndex == TriggerIndex.TriggerType.Act)
				{
					return true;
				}
			}
		}

		return false;
	}

	public bool IsCanUseAbility(int index)
	{
		if(_abilityList != null && _abilityList.Count > index)
		{
			AbilityData ability;
			GetAbility(index, out ability);

			if(ability.IsCanUse() && ability.TriggerIndex == TriggerIndex.TriggerType.Act)
			{
				return true;
			}
		}

		return false;
	}
		
	public bool GetAbility(int index, out AbilityData abilityData)
	{
		if(_abilityList != null && _abilityList.Count > index)
		{
			abilityData = _abilityList[index];
			return true;
		}

		abilityData = null;
		return false;
	}

	/*
	public void UseAbility(int index, AbilityData.OnFinishedAction onUseAbilityFinish)
	{
		if(IsCanUseAbility(index))
		{
			AbilityData ability;
			GetAbility(index, out ability);

			ability.ActiveAbility(onUseAbilityFinish);
		}
	}
	*/
	#endregion

	#region Buff Methods
	private void ActiveBuff(CardBuff buff)
	{
		_currentShield += buff.Shield;
		_currentHP += buff.HP;
		_currentAtk += buff.Attack;
		_currentSpeed += buff.Speed;
		_currentMove += buff.Move;
		_currentAttackRange += buff.AttackRange;

		_currentPassiveIndex = this.BasePassiveIndex;
		foreach(CardBuff currentBuff in _buffList)
		{
			_currentPassiveIndex += currentBuff.PassiveIndex;
		}
		_currentPassiveIndex += buff.PassiveIndex;
	}

	private void DeactiveBuff(CardBuff buff)
	{
		_currentShield -= buff.Shield;
		_currentHP -= buff.HP;
		_currentAtk -= buff.Attack;
		_currentSpeed -= buff.Speed;
		_currentMove -= buff.Move;
		_currentAttackRange -= buff.AttackRange;

		_currentPassiveIndex = this.BasePassiveIndex;
		foreach(CardBuff currentBuff in _buffList)
		{
			_currentPassiveIndex += currentBuff.PassiveIndex;
		}
	}

	public void AddBuff(CardBuff buff)
	{
		//Debug.Log("AddBuff: " + buff.EffectorID.ToString() + ", " + buff.AbilityEffectID + ", " + buff.BuffID.ToString());

		if(_buffList == null)
		{
			_buffList = new List<CardBuff>();
		}
			
		ActiveBuff(buff);

		_buffList.Add(buff);

		BattleManager.Instance.RequestUpdateBattleCardUI(this.BattleCardID);
		BattleManager.Instance.CheckIsDead(this.BattleCardID);
	}

	public void UpdateBuff()
	{
		if(_buffList != null && _buffList.Count > 0)
		{
			for(int index = 0; index < _buffList.Count; ++index)
			{
				CardBuff buff = _buffList[index];
				buff.UpdateTurn();

				if(!buff.IsActive)
				{
					RemoveBuff(buff.BuffID);
					index--;
				}
			}
		}
	}

	public bool RemoveBuff(int buffID)
	{
		if(_buffList != null && _buffList.Count > 0)
		{
			for(int index = 0; index < _buffList.Count; ++index)
			{
				if(_buffList[index].BuffID == buffID)
				{
					//Debug.Log(this.Name + " remove buff " + _buffList[index].BuffID);
					DeactiveBuff(_buffList[index]);

					_buffList.RemoveAt(index);

					BattleManager.Instance.RequestUpdateBattleCardUI(this.BattleCardID);
					BattleManager.Instance.CheckIsDead(this.BattleCardID);

					return true;
				}
			}
		}

		return false;
	}

	public bool RemoveBuff(int effectorID, string abilityEffectID)
	{
		//Debug.Log("RemoveBuff: " + effectorID.ToString() + ", " + abilityEffectID);

		bool isRemove = false;
		if(_buffList != null && _buffList.Count > 0)
		{
			for(int index = 0; index < _buffList.Count; ++index)
			{
				if( (_buffList[index].EffectorID == effectorID)
					&& string.Compare(_buffList[index].AbilityEffectID, abilityEffectID) == 0)
				{
					//Debug.Log(this.Name + " remove buff " + _buffList[index].BuffID);
					DeactiveBuff(_buffList[index]);

					_buffList.RemoveAt(index);
					isRemove = true;

					BattleManager.Instance.RequestUpdateBattleCardUI(this.BattleCardID);
					BattleManager.Instance.CheckIsDead(this.BattleCardID);

					--index;
				}
			}
		}

		if(isRemove)
		{
			return true;	
		}
		else
		{
			return false;
		}
	}

	public bool GetBuff(int buffID, out CardBuff buff)
	{
		if(_buffList != null && _buffList.Count > 0)
		{
			for(int index = 0; index < _buffList.Count; ++index)
			{
				if(_buffList[index].BuffID == buffID)
				{
					buff = _buffList[index];
					return true;
				}
			}
		}

		buff = null;
		return false;
	}

	public bool GetBuff(int effectorID, out List<CardBuff> buffList)
	{
		if(_buffList != null && _buffList.Count > 0)
		{
			buffList = new List<CardBuff>();
			for(int index = 0; index < _buffList.Count; ++index)
			{
				if(_buffList[index].EffectorID == effectorID)
				{
					buffList.Add(_buffList[index]);
				}
			}

			if(buffList.Count > 0)
			{
				return true;
			}
		}

		buffList = null;
		return false;
	}
	#endregion
}
