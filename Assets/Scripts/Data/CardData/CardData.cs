﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PassiveIndex
{
    #region PassiveType Enums
    public enum PassiveType : int
    {
          AirUnit = (1 << 0)      // 001
        , Knockback = (1 << 1)    // 010       
        , Piercing = (1 << 2)     // 100
    }
    #endregion

    #region PassiveIndex Properties
    private int _passiveIndex;
    #endregion

    #region PassiveIndex Constructors
    public PassiveIndex()
    {
        _passiveIndex = 0;
    }

    public PassiveIndex(int passiveIndex)
    {
        _passiveIndex = passiveIndex;
    }

    public PassiveIndex(PassiveType passiveType)
    {
        _passiveIndex = (int)passiveType;
    }

    // Copy Constructor
    public PassiveIndex(PassiveIndex passiveIndex)
        :this(passiveIndex._passiveIndex)
    {
    }
    #endregion

    #region Methods
    public static PassiveIndex StringToPassiveIndex(string text)
    {
        PassiveIndex result = new PassiveIndex();
        
        foreach (PassiveType passiveType in System.Enum.GetValues(typeof(PassiveType)))
        {
            if(string.Compare(text.ToUpper(), passiveType.ToString().ToUpper()) == 0)
            {
                result = new PassiveIndex(passiveType);
                break;
            }
        }

        return result;
    }
    #endregion

	#region Operator Overloading
	public static PassiveIndex operator +(PassiveIndex p1, PassiveIndex p2)
	{
		return new PassiveIndex((p1._passiveIndex | p2._passiveIndex));
	}
	public static PassiveIndex operator +(PassiveIndex p1, PassiveType p2)
	{
		return new PassiveIndex((p1._passiveIndex | (int)p2));
	}
	#endregion

    #region Relational Operator Overloading
    public static bool operator ==(PassiveType c1, PassiveIndex c2)
    {
		if((System.Object)c1 == null && (System.Object)c2 == null) return true;
		else if((System.Object)c1 == null || (System.Object)c2 == null) return false;
        else 
		{
			int n = (c2._passiveIndex & ((int)c1));
			//Debug.Log("PassiveIndex == : " + n.ToString() + "(" + c2._passiveIndex.ToString() + "|" + ((int)c1).ToString() + ")");
			if(n > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
    }

    public static bool operator !=(PassiveType c1, PassiveIndex c2)
    {
        return !(c1 == c2);
    }

    public static bool operator ==(PassiveIndex c1, PassiveType c2)
    {
        return (c2 == c1);
    }

    public static bool operator !=(PassiveIndex c1, PassiveType c2)
    {
        return !(c2 == c1);
    }

    public static bool operator ==(PassiveIndex c1, PassiveIndex c2)
    {
        if ((System.Object)c1 == null && (System.Object)c2 == null) return true;
        else if ((System.Object)c1 == null || (System.Object)c2 == null) return false;
        else return (c1._passiveIndex == c2._passiveIndex);
    }

    public static bool operator !=(PassiveIndex c1, PassiveIndex c2)
    {
        return !(c1 == c2);
    }

    public override bool Equals(System.Object obj)
    {
        // Check for null values and compare run-time types.
        if (obj == null || GetType() != obj.GetType())
            return false;

        PassiveIndex c = (obj as PassiveIndex);
        return (_passiveIndex == c._passiveIndex);
    }

    public override int GetHashCode()
    {
        return 0;
    }
    #endregion
}

#region CardData
public class CardData
{
    #region Private Properties
    private string _cardID;

    private CardImageData   _imageData;
    private CardSymbolData  _symbolData;
    private CardClanData _clanData;
    private CardTypeData    _typeData;
    private List<CardSubTypeData> _subTypeDataList;
    private List<CardJobData> _jobDataList;

    private string _name_EN;
    private string _name_TH;
    private string _alias_EN;
    private string _alias_TH;

    private int _lv;
    private int _cost;
    private int _shield;
    private int _hitPoint;
    private int _attack;
    private int _speed;

    private PassiveIndex _passiveIndex;
    private string _descriptionText_EN;
    private string _descriptionText_TH;

    private List<AbilityData.AbilityParams> _abilityParamsList;
    #endregion

    #region Public Properties
    public string CardID
    {
        get { return _cardID;  }
    }

    public CardImageData ImageData
    {
        get { return _imageData;  }
    }

    public CardSymbolData SymbolData
    {
        get { return _symbolData; }
    }

    public CardClanData ClanData
    {
        get { return _clanData; }
    }

    public CardTypeData TypeData
    {
        get { return _typeData; }
    }

    public List<CardSubTypeData> SubTypeDataList
    {
        get { return _subTypeDataList; }
    }

    public List<CardJobData> JobDataList
    {
        get { return _jobDataList; }
    }

    public string Name_EN
    {
        get { return _name_EN; }
    }

    public string Name_TH
    {
        get { return _name_TH; }
    }

    public string Alias_EN
    {
        get { return _alias_EN; }
    }

    public string Alias_TH
    {
        get { return _alias_TH; }
    }

    public int LV
    {
        get { return _lv; }
    }

    public int Cost
    {
        get { return _cost; }
    }

    public int Shield
    {
        get { return _shield; }
    }

    public int HitPoint
    {
        get { return _hitPoint; }
    }

    public int Attack
    {
        get { return _attack; }
    }

    public int Speed
    {
        get { return _speed; }
    }

    public PassiveIndex PassiveIndex
    {
        get { return _passiveIndex; }
    }

    public string DescriptionText_EN
    {
        get { return _descriptionText_EN; }
    }

    public string DescriptionText_TH
    {
        get { return _descriptionText_TH; }
    }

    public List<AbilityData.AbilityParams> AbilityParamsList
    {
		get { return _abilityParamsList; }
    }
    #endregion

    #region Constructors
    public CardData()
    {
        this._cardID = "";

        this._imageData = null;
        this._symbolData = null;
        this._clanData = null;
        this._typeData = null;
        this._subTypeDataList = new List<CardSubTypeData>();
        this._jobDataList = new List<CardJobData>();

        this._name_EN = "";
        this._name_TH = "";
        this._alias_EN = "";
        this._alias_TH = "";

        this._lv = 0;
        this._cost = 0;
        this._shield = 0;
        this._hitPoint = 0;
        this._attack = 0;
        this._speed = 0;
        this._passiveIndex = new PassiveIndex();

        this._descriptionText_EN = "";
        this._descriptionText_TH = "";

        this._abilityParamsList = new List<AbilityData.AbilityParams>();
    }

    public CardData(string cardID)
    {
        CardDBData rawCardData;
        bool isSuccess = CardDB.GetData(cardID, out rawCardData);

        if (isSuccess)
        {
            this._cardID = cardID;

            this._imageData = new CardImageData(rawCardData.ImageID);
            this._symbolData = new CardSymbolData(rawCardData.SymbolID);
            this._clanData = new CardClanData(rawCardData.ClanID);
            this._typeData = new CardTypeData(rawCardData.TypeID);

            this._subTypeDataList = new List<CardSubTypeData>();
            foreach(string subTypeID in rawCardData.SubTypeList)
            {
                this._subTypeDataList.Add(new CardSubTypeData(subTypeID));
            }

            this._jobDataList = new List<CardJobData>();
            foreach (string jobID in rawCardData.JobList)
            {
                this._jobDataList.Add(new CardJobData(jobID));
            }

            this._name_EN = rawCardData.Name_EN;
            this._name_TH = rawCardData.Name_TH;
            this._alias_EN = rawCardData.Alias_EN;
            this._alias_TH = rawCardData.Alias_TH;

            this._lv = rawCardData.LV;
            this._cost = rawCardData.Cost;
            this._shield = rawCardData.Shield;
            this._hitPoint = rawCardData.HitPoint;
            this._attack = rawCardData.Attack;
            this._speed = rawCardData.Speed;
            this._passiveIndex = new PassiveIndex(rawCardData.PassiveIndex);

            this._descriptionText_EN = rawCardData.DescriptionText_EN;
            this._descriptionText_TH = rawCardData.DescriptionText_TH;

            this._abilityParamsList = new List<AbilityData.AbilityParams>();
			foreach(string abilityID in rawCardData.AbilityIDList)
			{
				IDParams idParams;
				bool isParseSuccess = IDParams.StrToIDParams(abilityID, out idParams);
				if(isParseSuccess)
				{
					_abilityParamsList.Add(new AbilityData.AbilityParams(
						  idParams.ID
						, idParams.Parameters
					));
				}
			}
        }
        else
        {
            string errorLog = string.Format("CardData: cardID({0}) not found.", cardID);
            Debug.LogError(errorLog);

            this._cardID = "";

            this._imageData = null;
            this._symbolData = null;
            this._clanData = null;
            this._typeData = null;
            this._subTypeDataList = new List<CardSubTypeData>();
            this._jobDataList = new List<CardJobData>();

            this._name_EN = "";
            this._name_TH = "";
            this._alias_EN = "";
            this._alias_TH = "";

            this._lv = 0;
            this._cost = 0;
            this._shield = 0;
            this._hitPoint = 0;
            this._attack = 0;
            this._speed = 0;
            this._passiveIndex = new PassiveIndex();

            this._descriptionText_EN = "";
            this._descriptionText_TH = "";

            this._abilityParamsList = new List<AbilityData.AbilityParams>();
        }
    }

    // Copy Constructor
    public CardData(CardData data) 
        : this(data.CardID)
    { 
    }
    #endregion

    #region Relational Operator Overloading
    public static bool operator ==(CardData c1, CardData c2)
    {
		if((System.Object)c1 == null && (System.Object)c2 == null) return true;
		else if((System.Object)c1 == null || (System.Object)c2 == null) return false;
        else return (string.Compare(c1.CardID, c2.CardID) == 0);
    }

    public static bool operator !=(CardData c1, CardData c2)
    {
        return !(c1 == c2);
    }

    public override bool Equals(System.Object obj)
    {
        // Check for null values and compare run-time types.
        if (obj == null || GetType() != obj.GetType())
            return false;

        CardData c = (obj as CardData);
        return (string.Compare(CardID, c.CardID) == 0);
    }

    public override int GetHashCode()
    {
        return 0;
    }
    #endregion
}
#endregion
