﻿using UnityEngine;
using System.Collections;

public class CardClanData 
{
	public enum CardClanType : int
	{
		  None		= 0
		, Berondo	= 1
		, DiAlno	= 2
		, Mutant	= 3
	}

    #region Private Properties
    private string _cardClanID;
    private string _clanImageFile;

    private string _clanName_EN;
    private string _clanName_TH;
    #endregion

    #region Public Properties
    public string CardClanID 
    {
        get { return _cardClanID; }
    }
    public string ClanImageFile
    {
        get { return _clanImageFile; }
    }
    public string ClanName_EN
    {
        get { return _clanName_EN; }
    }
    public string ClanName_TH
    {
        get { return _clanName_TH; }
    }
    #endregion

    #region Constructors
    public CardClanData()
    {
        this._cardClanID = "";
        this._clanImageFile = "";

        this._clanName_EN = "";
        this._clanName_TH = "";
    }

	public CardClanData(CardClanType cardClanType)
		: this(EnumCardClanTypeToID(cardClanType))
	{
	}

    public CardClanData(string cardClanID)
    { 
        CardClanDBData rawCardClanData;
        bool isSuccess = CardClanDB.GetData(cardClanID, out rawCardClanData);

        if (isSuccess)
        {
            this._cardClanID = cardClanID;
            this._clanImageFile = rawCardClanData.ImageFile;

            this._clanName_EN = rawCardClanData.ClanName_EN;
            this._clanName_TH = rawCardClanData.ClanName_TH;
        }
        else
        {
            string errorLog = string.Format("CardClanData: cardClanID({0}) not found.", cardClanID);
            Debug.LogError(errorLog);

            this._cardClanID = "";
            this._clanImageFile = "";

            this._clanName_EN = "";
            this._clanName_TH = "";
        }
    }
    #endregion

	#region Methods
	public static string EnumCardClanTypeToID(CardClanType cardClanType)
	{
		/*
		CL0000	No Clan
		CL0001	Berondo
		CL0002	Di alno
		CL0003	Mutant
		*/
		switch (cardClanType)
		{
		case CardClanType.Berondo: 	return "CL0001";
		case CardClanType.DiAlno: 	return "CL0002";
		case CardClanType.Mutant: 	return "CL0003";
			
		case CardClanType.None:
		default:
			return "CL0000";
		}
	}

	public static string EnumCardClanTypeToText(CardClanType cardClanType)
	{
		switch (cardClanType)
		{
		case CardClanType.Berondo: 	return Localization.Get("CLAN_BERONDO");
		case CardClanType.DiAlno: 	return Localization.Get("CLAN_DIALNO");
		case CardClanType.Mutant: 	return Localization.Get("CLAN_MUTANT");

		case CardClanType.None:
		default:
			return Localization.Get("CLAN_NONE");
		}
		return "";
	}

    public static CardClanType StringToCardClan(string text)
    {
        foreach (CardClanType clan in System.Enum.GetValues(typeof(CardClanType)))
        {
            if(string.Compare(text.ToUpper(), clan.ToString().ToUpper()) == 0)
            {
                return clan;
            }
        }

        return CardClanType.None;
    }
	#endregion

    #region Relational Operator Overloading
	public static bool operator ==(CardClanData c1, CardClanData.CardClanType cc2)
	{
		if((System.Object)c1 == null) return false;
		else return (string.Compare(c1.CardClanID, CardClanData.EnumCardClanTypeToID(cc2)) == 0);
	}
	public static bool operator !=(CardClanData c1, CardClanData.CardClanType cc2)
	{
		return !(c1 == cc2);
	}

    public static bool operator ==(CardClanData c1, CardClanData c2)
    {
		if((System.Object)c1 == null && (System.Object)c2 == null) return true;
		else if((System.Object)c1 == null || (System.Object)c2 == null) return false;
        else return (string.Compare(c1.CardClanID, c2.CardClanID) == 0);
    }
    public static bool operator !=(CardClanData c1, CardClanData c2)
    {
        return !(c1 == c2);
    }
    public override bool Equals(System.Object obj)
    {
        // Check for null values and compare run-time types.
        if (obj == null || GetType() != obj.GetType())
            return false;

        CardClanData c = (obj as CardClanData);
        return (string.Compare(CardClanID, c.CardClanID) == 0);
    }
    public override int GetHashCode()
    {
        return 0;
    }
    #endregion
}