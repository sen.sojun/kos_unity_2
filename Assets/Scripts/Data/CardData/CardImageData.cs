﻿using UnityEngine;
using System.Collections;

public class CardImageData
{
    #region Private Properties
    private string _cardImageID;
    private string _cardImageFile;
    private string _artistName_EN;
    private string _artistName_TH;
    #endregion

    #region Public Properties
    public string CardImageID
    {
        get { return _cardImageID; }
       // set { _cardImageID = value; }
    }
    public string CardImageFile
    {
        get { return _cardImageFile; }
       // set { _cardImageFile = value; }
    }
    public string ArtistName_EN
    {
        get { return _artistName_EN; }
       // set { _artistName = value; }
    }
    public string ArtistName_TH
    {
        get { return _artistName_TH; }
        // set { _artistName = value; }
    }
    #endregion

    #region Constructors
    public CardImageData()
    {
        this._cardImageID = "";
        this._cardImageFile = "";
        this._artistName_EN = "";
        this._artistName_TH = "";
    }

    public CardImageData(string cardImageID)
    {
        CardImageDBData rawCardImageData;
        bool isSuccess = CardImageDB.GetData(cardImageID, out rawCardImageData);

        if (isSuccess)
        {
            this._cardImageID = cardImageID;
            this._cardImageFile = rawCardImageData.ImageFile;
            this._artistName_EN = rawCardImageData.ArtistName_EN;
            this._artistName_TH = rawCardImageData.ArtistName_TH;
        }
        else
        {
            string errorLog = string.Format("CardImageData: cardImageID({0}) not found.", cardImageID);
            Debug.LogError(errorLog);

            this._cardImageID = "";
            this._cardImageFile = "";
            this._artistName_EN = "";
            this._artistName_TH = "";
        }
    }
    #endregion

    #region Relational Operator Overloading
    public static bool operator ==(CardImageData c1, CardImageData c2)
    {
		if((System.Object)c1 == null && (System.Object)c2 == null) return true;
		else if((System.Object)c1 == null || (System.Object)c2 == null) return false;
        else return (string.Compare(c1.CardImageID, c2.CardImageID) == 0);
    }

    public static bool operator !=(CardImageData c1, CardImageData c2)
    {
        return !(c1 == c2);
    }

    public override bool Equals(System.Object obj)
    {
        // Check for null values and compare run-time types.
        if (obj == null || GetType() != obj.GetType())
            return false;

        CardImageData ci = (obj as CardImageData);
        return (string.Compare(CardImageID, ci.CardImageID) == 0);
    }

    public override int GetHashCode()
    {
        return 0;
    }
    #endregion
}