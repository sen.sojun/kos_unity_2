﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerCardData
{
    #region Private Properties
    private CardData _cardData;
    #endregion

    #region Public Properties
    public CardData CardData
    {
        get { return _cardData; }
    }

    public string CardID
    {
        get { return _cardData.CardID; }
    }

	public string PlayerCardID
	{
		// TODO: will contain list of soul crystal ID.
		get { return _cardData.CardID; }
	}

    public CardImageData ImageData
    {
        get { return _cardData.ImageData; }
    }

    public CardSymbolData SymbolData
    {
        get { return _cardData.SymbolData; }
    }

    public CardClanData ClanData
    {
        get { return _cardData.ClanData; }
    }

    public CardTypeData TypeData
    {
        get { return _cardData.TypeData; }
    }

    public List<CardSubTypeData> SubTypeDataList
    {
        get { return _cardData.SubTypeDataList; }
    }

    public List<CardJobData> JobDataList
    {
        get { return _cardData.JobDataList; }
    }

    public string Name_EN
    {
        get { return _cardData.Name_EN; }
    }

    public string Name_TH
    {
        get { return _cardData.Name_TH; }
    }

    public string Alias_EN
    {
        get { return _cardData.Alias_EN; }
    }

    public string Alias_TH
    {
        get { return _cardData.Alias_TH; }
    }

    public int LV
    {
        get { return _cardData.LV; }
    }

    public int Cost
    {
        get { return _cardData.Cost; }
    }

    public int Shield
    {
        get { return _cardData.Shield; }
    }

    public int HitPoint
    {
        get { return _cardData.HitPoint; }
    }

    public int Attack
    {
        get { return _cardData.Attack; }
    }

    public int Speed
    {
        get { return _cardData.Speed; }
    }

    public PassiveIndex PassiveIndex
    {
        get { return _cardData.PassiveIndex; }
    }
    
    public string DescriptionText_EN
    {
        get { return _cardData.DescriptionText_EN; }
    }

    public string DescriptionText_TH
    {
        get { return _cardData.DescriptionText_TH; }
    }

    public List<AbilityData.AbilityParams> AbilityParamsList
    {
		get { return _cardData.AbilityParamsList; }
    }

    #endregion

    #region Constructors
    public PlayerCardData(CardData data)
    {
        _cardData = new CardData(data);
    }

	public PlayerCardData(string playerCardID)
	{
		_cardData = new CardData(playerCardID);
	}

    // Copy Constructor
    public PlayerCardData(PlayerCardData data)
        : this(data.CardData)
    { 
    }
    #endregion

	#region Relational Operator Overloading
    public static bool operator >(PlayerCardData c1, PlayerCardData c2)
    {
        if ((System.Object)c1 == null && (System.Object)c2 == null) return false;
        else if ((System.Object)c1 == null || (System.Object)c2 == null) return false;
        else return (string.Compare(c1.PlayerCardID, c2.PlayerCardID) > 0);
    }
    public static bool operator >=(PlayerCardData c1, PlayerCardData c2)
    {
        if ((System.Object)c1 == null && (System.Object)c2 == null) return true;
        else if ((System.Object)c1 == null || (System.Object)c2 == null) return false;
        else return (string.Compare(c1.PlayerCardID, c2.PlayerCardID) >= 0);
    }
    public static bool operator <(PlayerCardData c1, PlayerCardData c2)
    {
        return !(c1 >= c2);
    }
    public static bool operator <=(PlayerCardData c1, PlayerCardData c2)
    {
        return !(c1 > c2);
    }

	public static bool operator ==(PlayerCardData c1, PlayerCardData c2)
	{
		if((System.Object)c1 == null && (System.Object)c2 == null) return true;
		else if((System.Object)c1 == null || (System.Object)c2 == null) return false;
		else return (string.Compare(c1.PlayerCardID, c2.PlayerCardID) == 0);
	}
	public static bool operator !=(PlayerCardData c1, PlayerCardData c2)
	{
		return !(c1 == c2);
	}
	public override bool Equals(System.Object obj)
	{
		// Check for null values and compare run-time types.
		if (obj == null || GetType() != obj.GetType())
			return false;

		PlayerCardData c = (obj as PlayerCardData);
		return (string.Compare(PlayerCardID, c.PlayerCardID) == 0);
	}
	public override int GetHashCode()
	{
		return 0;
	}
	#endregion

    #region Methods
	/*
    public static bool IsSameCard(PlayerCardData card1, PlayerCardData card2)
    {
		if ((System.Object)card1 != null && (System.Object)card2 != null)
        {
            return (string.Compare(card1.CardID, card2.CardID) == 0);
        }

        return false;
    }
    */

    public bool IsCardID(string cardID)
    {
        return (string.Compare(cardID, this._cardData.CardID) == 0);
    }

    public bool IsCardType(CardTypeData typeData)
    {
        return (this.TypeData == typeData);
    }

    public bool IsCardType(CardTypeData.CardType cardType)
    {
        return (this.TypeData == cardType);
    }

    public bool IsCardClan(CardClanData clanData)
    {
        return (this.ClanData == clanData);
    }

    public bool IsCardClan(CardClanData.CardClanType clan)
    {
        return (this.ClanData == clan);
    }

    public bool IsCardSubType(CardSubTypeData subTypeData)
    {
        foreach (CardSubTypeData subType in this.SubTypeDataList)
        {
            if (subType == subTypeData)
            {
                return true;
            }
        }

        return false;
    }

    public bool IsCardSubType(CardSubTypeData.CardSubType cardSubType)
    {
        foreach (CardSubTypeData subType in this.SubTypeDataList)
        {
            if (subType == cardSubType)
            {
                return true;
            }
        }
        return false;
    }

    public bool IsCardJobType(CardJobData jobData)
    {
        foreach (CardJobData cardJob in this.JobDataList)
        {
            if (cardJob == jobData)
            {
                return true;
            }
        }

        return false;
    }

    public bool IsCardJobType(CardJobData.JobType job)
    {
        foreach (CardJobData cardJob in this.JobDataList)
        {
            if (cardJob == job)
            {
                return true;
            }
        }
        return false;
    }
       
    public bool IsCardSymbol(CardSymbolData symbolData)
    {
        return (this.SymbolData == symbolData);
    }

    public bool IsCardSymbol(CardSymbolData.CardSymbol symbol)
    {
        return (this.SymbolData == symbol);
    }

    public bool IsAirUnit()
    {
        return (this.PassiveIndex == PassiveIndex.PassiveType.AirUnit);
    }

    public bool IsKnockback()
    {
        return (this.PassiveIndex == PassiveIndex.PassiveType.Knockback);
    }

    public bool IsPiercing()
    {
        return (this.PassiveIndex == PassiveIndex.PassiveType.Piercing);
    }
    #endregion
}
