﻿using UnityEngine;
using System.Collections;

public class CardTypeData
{
    public enum CardType : int
    {
          None          = 0
        , Unit          = 2
        , Structure     = 1
        , Base          = 6
        , Item          = 3
        , Event         = 4
        , Technic       = 5
    }

    #region Private Properties
    private string _cardTypeID;
    private string _typeName_EN;
    private string _typeName_TH;
    #endregion

    #region Public Properties
    public string CardTypeID
    {
        get { return _cardTypeID; }
    }

    public string TypeName_EN
    {
        get { return _typeName_EN; }
    }

    public string TypeName_TH
    {
        get { return _typeName_TH; }
    }
    #endregion

    #region Constructors
    public CardTypeData()
    {
        this._cardTypeID = "";
        this._typeName_EN = "";
        this._typeName_TH = "";
    }

	public CardTypeData(CardType cardType)
		: this(CardTypeData.EnumCardTypeToID(cardType))
    {
    }

	public CardTypeData(string cardTypeID)
	{
		CardTypeDBData rawCardTypeData;
		bool isSuccess = CardTypeDB.GetData(cardTypeID, out rawCardTypeData);

		if (isSuccess)
		{
			this._cardTypeID = cardTypeID;
			this._typeName_EN = rawCardTypeData.TypeName_EN;
            this._typeName_TH = rawCardTypeData.TypeName_TH;
        }
		else
		{
			string errorLog = string.Format("CardTypeData: cardTypeID({0}) not found.", cardTypeID);
			Debug.LogError(errorLog);

			this._cardTypeID = "";
			this._typeName_EN = "";
            this._typeName_TH = "";
        }
	}
    #endregion

    #region Methods
    public static string EnumCardTypeToID(CardType cardType)
    {
        switch (cardType)
        { 
            case CardType.Unit:
                return "T0001";

            case CardType.Structure:
                return "T0002";

            case CardType.Base:
                return "T0003";

            case CardType.Item:
                return "T0004";

            case CardType.Event:
                return "T0005";

            case CardType.Technic:
                return "T0006";

            case CardType.None:
            default:
                return "T0000";
        }
    }

    public static CardType EnumIDToCardType(string id)
    {
        switch (id)
        { 
            case "T0001":
                return CardType.Unit;

            case "T0002":
                return CardType.Structure;

            case "T0003":
                return CardType.Base;

            case "T0004":
                return CardType.Item;

            case "T0005":
                return CardType.Event;

            case "T0006":
                return CardType.Technic;

            case "T0000":
            default:
                return CardType.None;
        }
    }

    public static CardType StringToCardType(string text)
    {
        foreach (CardType cardType in System.Enum.GetValues(typeof(CardType)))
        {
            if(string.Compare(text.ToUpper(), cardType.ToString().ToUpper()) == 0)
            {
                return cardType;
            }
        }

        return CardType.None;
    }
    #endregion


    #region Relational Operator Overloading
	public static bool operator ==(CardTypeData c1, CardTypeData.CardType ct2)
    {
		if((System.Object)c1 == null) return false;
		else return (string.Compare(c1.CardTypeID, CardTypeData.EnumCardTypeToID(ct2)) == 0);
    }

	public static bool operator !=(CardTypeData c1, CardTypeData.CardType ct2)
    {
		return !(c1 == ct2);
    }

	public static bool operator ==(CardTypeData c1, CardTypeData c2)
	{
		if((System.Object)c1 == null && (System.Object)c2 == null) return true;
		else if((System.Object)c1 == null || (System.Object)c2 == null) return false;
		else return (string.Compare(c1.CardTypeID, c2.CardTypeID) == 0);
	}

	public static bool operator !=(CardTypeData c1, CardTypeData c2)
	{
		if((System.Object)c1 == null && (System.Object)c2 == null) return true;
		else if((System.Object)c1 == null || (System.Object)c2 == null) return false;
		else return (string.Compare(c1.CardTypeID, c2.CardTypeID) == 0);
	}

    public override bool Equals(System.Object obj)
    {
        // Check for null values and compare run-time types.
        if (obj == null || GetType() != obj.GetType())
            return false;

        CardTypeData c = (obj as CardTypeData);
        return (string.Compare(CardTypeID, c.CardTypeID) == 0);
    }

    public override int GetHashCode()
    {
        return 0;
    }
    #endregion
}