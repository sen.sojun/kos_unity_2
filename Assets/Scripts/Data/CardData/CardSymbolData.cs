﻿using UnityEngine;
using System.Collections;

public class CardSymbolData
{
	public enum CardSymbol
	{
		  None
		, Mass
		, Common
		, Uncommon
		, Rare
		, SuperRare
		, Forbidden
	}

    #region Private Properties
    private string _cardSymbolID;
    private string _symbolImageFile;
    private string _symbolName_EN;
    private string _symbolName_TH;
    #endregion

    #region Public Properties
    public string CardSymbolID
    {
        get { return _cardSymbolID; }
    }
    
    public string SymbolImageFile
    {
        get { return _symbolImageFile; }
    }

    public string SymbolName_EN
    {
        get { return _symbolName_EN; }
    }

    public string SymbolName_TH
    {
        get { return _symbolName_TH; }
    }
    #endregion

    #region Constructors
    public CardSymbolData()
    {
        this._cardSymbolID = "";    
        this._symbolImageFile = "";

        this._symbolName_EN = "";
        this._symbolName_TH = "";
    }

    public CardSymbolData(string cardSymbolID)
    {
        CardSymbolDBData rawCardSymbolData;
        bool isSuccess = CardSymbolDB.GetData(cardSymbolID, out rawCardSymbolData);

        if (isSuccess)
        {
            this._cardSymbolID = cardSymbolID;
            this._symbolImageFile = rawCardSymbolData.ImageFile;

            this._symbolName_EN = rawCardSymbolData.SymbolName_EN;
            this._symbolName_TH = rawCardSymbolData.SymbolName_TH;
        }
        else
        {
            string errorLog = string.Format("CardSymbolData: cardSymbolID({0}) not found.", cardSymbolID);
            Debug.LogError(errorLog);

            this._cardSymbolID = "";
            this._symbolImageFile = "";

            this._symbolName_EN = "";
            this._symbolName_TH = "";
        }
    }
    #endregion

	#region Methods
	public static string EnumCardSymbolToID(CardSymbol cardSymbol)
	{
		switch (cardSymbol)
		{ 
		case CardSymbol.Mass:
			return "S0001";

		case CardSymbol.Common:
			return "S0002";

		case CardSymbol.Uncommon:
			return "S0003";

		case CardSymbol.Rare:
			return "S0004";

		case CardSymbol.SuperRare:
			return "S0005";

		case CardSymbol.Forbidden:
			return "S0006";

		case CardSymbol.None:
		default:
			return "S0000";
		}
	}

    public static CardSymbol StringToCardSymbol(string text)
    {
        foreach (CardSymbol symbol in System.Enum.GetValues(typeof(CardSymbol)))
        {
            if(string.Compare(text.ToUpper(), symbol.ToString().ToUpper()) == 0)
            {
                return symbol;
            }
        }

        return CardSymbol.None;
    }
	#endregion

    #region Relational Operator Overloading
	public static bool operator ==(CardSymbolData c1, CardSymbolData.CardSymbol cs2)
	{
		if((System.Object)c1 == null) return false;
		else return (string.Compare(c1.CardSymbolID, CardSymbolData.EnumCardSymbolToID(cs2)) == 0);
	}

	public static bool operator !=(CardSymbolData c1, CardSymbolData.CardSymbol cs2)
	{
		return !(c1 == cs2);
	}

	public static bool operator ==(CardSymbolData.CardSymbol cs1, CardSymbolData c2)
	{
		return (c2 == cs1);
	}

	public static bool operator !=(CardSymbolData.CardSymbol cs1, CardSymbolData c2)
	{
		return (c2 != cs1);
	}

    public static bool operator ==(CardSymbolData c1, CardSymbolData c2)
    {
		if((System.Object)c1 == null && (System.Object)c2 == null) return true;
		else if((System.Object)c1 == null || (System.Object)c2 == null) return false;
        else return (string.Compare(c1.CardSymbolID, c2.CardSymbolID) == 0);
    }
    public static bool operator !=(CardSymbolData c1, CardSymbolData c2)
    {
        return !(c1 == c2);
    }
    public override bool Equals(System.Object obj)
    {
        // Check for null values and compare run-time types.
        if (obj == null || GetType() != obj.GetType())
            return false;

        CardSymbolData c = (obj as CardSymbolData);
        return (string.Compare(CardSymbolID, c.CardSymbolID) == 0);
    }
    public override int GetHashCode()
    {
        return 0;
    }
    #endregion
}
