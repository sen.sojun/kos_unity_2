﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MatchingData : MonoBehaviour 
{
    private IBattleControlController[] _players; 
	private PlayerData[] _playerData;
	private int _bgIndex = 0;
	private bool _isVSBot;
	private bool _isTutorial= false;

	#region Public Properties
	public GameObject playerControllerPrefab;
	public GameObject botControllerPrefab;

    public IBattleControlController[] Players { get { return _players; } }
	public PlayerData[] PlayerData { get { return _playerData; } }
	public int BGIndex { get { return _bgIndex; } }
	public bool IsTutorial { get { return _isTutorial; } }
	#endregion

	void Awake()
	{
	}

	/*
	// Use this for initialization
	void Start () 
	{
		// TODO: Have to send this GameObject from another scene.
		CreateDummyData();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
	*/
		
	private void CreateDummyData()
	{
		// Test Data
		List<PlayerCardData> cardList = new List<PlayerCardData>();

		PlayerSave.GenerateDummySave();
		bool isSuccess = PlayerSave.GetPlayerCardStash(out cardList);
		if(isSuccess)
		{
			DeckData deck1 = new DeckData(cardList);

			PlayerData hostPlayerData = new PlayerData("PL0001", "ME", "None", "None", 1, deck1);
			PlayerData clientPlayerData = new PlayerData("PL0002", "ENEMY", "None", "None", 1, deck1);

			_playerData = new PlayerData[2];
			_playerData[0] = hostPlayerData;
			_playerData[1] = clientPlayerData;
			_bgIndex = 0;
		}
	}
		
	public void SetPlayersData(PlayerData player1, PlayerData player2, int bgIndex, bool isVSBot = true, bool isTutorial = false)
	{
		_playerData = new PlayerData[2];
		_playerData[0] = player1;
		_playerData[1] = player2;
		_bgIndex = bgIndex;

		_isVSBot = isVSBot;
		_isTutorial = isTutorial;
        
        UniqueCardData.ResetUniqueCardIndex();
        int count = _playerData[0].Deck.Count;
        _playerData[0].Deck.ReIndexCard(1);
        _playerData[1].Deck.ReIndexCard(count + 1);
        
        //Debug.Log("SetPlayersData 0: " + _playerData[0].ToPlayerDataText());
        //Debug.Log("SetPlayersData 1: " + _playerData[1].ToPlayerDataText());
	}

	public void CreateBattleControl()
	{
		_players = new IBattleControlController[2];

		// Create player 1
		GameObject obj1 = Instantiate(playerControllerPrefab) as GameObject;
		_players[0] = obj1.GetComponent<PlayerController>();

		// Create player 2
		if(_isVSBot)
		{
			// VS bot.
			GameObject obj2 = Instantiate(botControllerPrefab) as GameObject;
			_players[1] = obj2.GetComponent<BotController>();
		}
		else
		{
			// VS player.
			GameObject obj2 = Instantiate(playerControllerPrefab) as GameObject;
			_players[1] = obj2.GetComponent<PlayerController>();
		}
	}

    public void SetPlayers(IBattleControlController[] players)
	{
		_players = players;
	}
}
