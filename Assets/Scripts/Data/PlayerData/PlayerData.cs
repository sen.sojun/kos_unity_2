﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerData
{
    #region Private Properties
    protected string _playerID;
    protected string _playerName;
	protected string _soloImageName;
	protected string _imageName;
	protected int _lv;

    protected DeckData _deck;
    private static string[] _spliters = new string[] {"[playerdata]"};
    #endregion


    #region Public Properties
    public string ID
    {
        get { return _playerID; }
    }
    public string Name
    {
        get { return _playerName; }
    }

	public string SoloImageName
	{
		get { return _soloImageName; }
	}

	public string ImageName
	{
		get { return _imageName; }
	}

	public int Lv
	{
		get { return _lv; }
	}

    public DeckData Deck
    {
        get { return _deck; }
    }
    #endregion

    #region Constructors
    public PlayerData()
    {
        _deck = new DeckData();

        _playerID = "";
		_soloImageName = "";
        _playerName = "";
		_imageName = "";
		_lv = 0;
    }

	public PlayerData(string playerID, string playerName, string soloImageName, string imageName, int lv, DeckData deck)
    {
        _playerID = playerID;
        _playerName = playerName;
		_soloImageName = soloImageName;
		_imageName = imageName;
		_lv = lv;

        _deck = new DeckData(deck);
    }

    public PlayerData(PlayerData playerData)
    {
        _playerID = playerData.ID;
        _playerName = playerData.Name;
		_soloImageName = playerData.SoloImageName;
		_imageName = playerData.ImageName;
		_lv = playerData.Lv;

        _deck = new DeckData(playerData.Deck);
    }
    #endregion

    #region Methods
    public string ToPlayerDataText()
    {
        string result = "";
        
        result += this.ID;
        result += _spliters[0] + this.Name;
        result += _spliters[0] + this.SoloImageName;
        result += _spliters[0] + this.ImageName;
        result += _spliters[0] + this.Lv;
        result += _spliters[0] + this.Deck.ToDeckText();
        
        return result;
    }
    
    public static PlayerData StrToPlayerData(string playerDataText)
    {
        string[] texts = playerDataText.Split(_spliters, System.StringSplitOptions.RemoveEmptyEntries);
        
        string playerID         = texts[0];
        string playerName       = texts[1];
        string soloImageName    = texts[2];
        string imageName        = texts[3];
        int lv                  = int.Parse(texts[4]);
        DeckData deck           = DeckData.StrToDeckData(texts[5]);
        
        PlayerData data = new PlayerData(playerID, playerName, soloImageName, imageName, lv, deck);
        return data;   
    }

    #endregion

}
