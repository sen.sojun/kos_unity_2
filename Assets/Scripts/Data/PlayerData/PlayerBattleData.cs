﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerBattleData : PlayerData
{
    #region Private Properties
    private GamePlayerUI.RPSType _selectedRPSType;

    private int _gold;
    private int _hitPoint;
	private PlayerIndex _playerIndex;

	private PlayerCardData _baseCard;
	private PlayerCardData _leaderCard;

    private BattleCardData _battleLeaderCard;
	private bool _isSummon;

    private List<BattleCardData> _base1;
    private List<BattleCardData> _battlefield;
    private List<BattleCardData> _base2;

	private List<UniqueCardData> _hand;
	private List<UniqueCardData> _graveyard;
	private List<UniqueCardData> _removeZone;
    #endregion

    #region Public Properties
    public GamePlayerUI.RPSType SelectedRPSType
	{
		get { return _selectedRPSType; }
		set { _selectedRPSType = value; }
	}
		
    public int Gold
    {
        get { return _gold; }
    }

    public int HitPoint
    {
        get 
        { 
            List<BattleCardData> baseCardList;
            bool isFound = FindAllSummonedBaseCard(out baseCardList);

            //Debug.Log("HitPoint : " + isFound);
            if (isFound)
            {
                int hitPoint = 0;

                foreach (BattleCardData baseCard in baseCardList)
                {
					hitPoint += baseCard.CurrentHP;
                    //Debug.Log("HitPoint : " + baseCard.Name + " : " + baseCard.CurrentHitPoint.ToString());
                }

                return hitPoint; 
            }

            return 0;
        }
    }

	public PlayerCardData BaseCard
	{
		get { return _baseCard; }
	}

	public PlayerCardData LeaderCard
	{
		get { return _leaderCard; }
	}

    public BattleCardData BattleLeaderCard
    {
        get { return _battleLeaderCard;  }
    }
   
	public int HandCount
	{
		get 
		{
			if(this._hand != null)
			{
				return this._hand.Count;
			}

			return 0;
		}
	}

	public PlayerIndex PlayerIndex
	{
		get { return _playerIndex; }
	}

	/*
    public List<BattleCardData> Base1
    {
        get { return _base1; }
    }
    public List<BattleCardData> Battlefield
    {
        get { return _battlefield; }
    }
    public List<BattleCardData> Base2
    {
        get { return _base2; }
    }
	*/

    /*
	public List<UniqueCardData> Hand
	{
		get { return _hand; }
	}
	public List<UniqueCardData> Graveyard
    {
        get { return _graveyard; }
    }
	public List<UniqueCardData> RemoveZone
    {
        get { return _removeZone; }
    }
    */
    #endregion

    #region Constructors
	public PlayerBattleData(string playerID, string playerName, string soloImageName, string imageName, int lv, DeckData deck, PlayerIndex playerIndex) 
		: base(playerID, playerName, soloImageName, imageName, lv, deck)
    {
        _gold = 0;
        _hitPoint = 0;

		_baseCard = null;
		_leaderCard = null;
		_isSummon = false;
		_playerIndex = playerIndex;

        _base1 = new List<BattleCardData>();
        _battlefield = new List<BattleCardData>();
        _base2 = new List<BattleCardData>();

		_hand = new List<UniqueCardData>();
		_graveyard = new List<UniqueCardData>();
		_removeZone = new List<UniqueCardData>();

		_deck.SetPlayerIndex(this.PlayerIndex);
    }

	public PlayerBattleData(PlayerData playerData, PlayerIndex playerIndex)
        : base(playerData)
    {
        _gold = 0;
        _hitPoint = 0;

		_baseCard = null;
		_leaderCard = null;
		_isSummon = false;
		_playerIndex = playerIndex;

        _base1 = new List<BattleCardData>();
        _battlefield = new List<BattleCardData>();
        _base2 = new List<BattleCardData>();

		_hand = new List<UniqueCardData>();
		_graveyard = new List<UniqueCardData>();
		_removeZone = new List<UniqueCardData>();

		_deck.SetPlayerIndex(this.PlayerIndex);
    }
    #endregion

    #region Methods
    public void AddGold(int deltaGold)
    {
        _gold += deltaGold;
    }

	public void ClearGold()
	{
		_gold = 0;
	}

	public void SetBaseCard(PlayerCardData baseCard)
	{
		_baseCard = baseCard;
	}

	public void SetLeaderCard(PlayerCardData leaderCard)
	{
		_leaderCard = leaderCard;
	}

    public void SetBattleLeaderCard(BattleCardData battleLeaderCard)
    {
        _battleLeaderCard = battleLeaderCard;
    }

	public void SetIsSummon(bool isSummon)
	{
		_isSummon = isSummon;
	}

    public List<BattleCardData> GetFieldDataList(BattleZoneIndex targetZone)
	{
		switch (targetZone)
		{
			case BattleZoneIndex.BaseP1:        return _base1;
			case BattleZoneIndex.Battlefield:   return _battlefield;
			case BattleZoneIndex.BaseP2:        return _base2;
		}
		return null;
	}

    public List<UniqueCardData> GetZoneDataList(CardZoneIndex targetZone)
	{
		switch (targetZone)
		{
			case CardZoneIndex.Hand:        return _hand;
			case CardZoneIndex.Graveyard:   return _graveyard;
			case CardZoneIndex.RemoveZone:  return _removeZone;
			case CardZoneIndex.Deck:
            {
                Debug.LogError("PlayerBattleData/GetZoneDataList: Error can't get dataList in deck. It protected.");
                return null;
            }
		}
		return null;
	}
    #endregion

    #region CardData Methods

    // Unique Card

    public bool MoveUniqueCard(int uniqueCardID, CardZoneIndex targetZone)
	{
		UniqueCardData uniqueCard;
		bool isFound = FindUniqueCardByID(uniqueCardID, out uniqueCard);
		if(isFound)
		{
			return _MoveUniqueCard(uniqueCard, targetZone);
		}
        else
        {
            Debug.LogError("PlayerBattleData/MoveUniqueCard: not found unique card.");
		    return false;
        }
	}

    public bool MoveUniqueCard(int uniqueCardID, BattleZoneIndex battleZoneIndex, bool isLeader, bool isFacedown, out BattleCardData resultBattleCard)
    {
        //Debug.Log("MoveUniqueCard to " + battleZoneIndex);
        UniqueCardData uniqueCard;
        bool isFound = FindUniqueCardByID(uniqueCardID, out uniqueCard);
        if (isFound)
        {
            return _MoveUniqueCard(uniqueCard, battleZoneIndex, isLeader, isFacedown, out resultBattleCard);
        }
        else
        {
            Debug.LogError("PlayerBattleData/MoveUniqueCard: not found unique card.");
            resultBattleCard = null;
            return false;
        }
    }

    public bool MoveAllHandCardTo(CardZoneIndex targetZone)
    {
        if (targetZone != CardZoneIndex.Hand)
        {
            while (this._hand.Count > 0)
            {
                UniqueCardData handCard = this._hand[0];
                bool isSuccess = _MoveUniqueCard(handCard, targetZone);
                if (!isSuccess)
                {
                    Debug.LogError("MoveAllHandCardTo: Failed to move hand card to " + targetZone.ToString());
                    return false;
                }
            }

            return true;
        }
        else
        {
            Debug.LogError("PlayerBattleData/MoveAllHandCardTo: Move to same location.");
            return false;
        }
    }

    private bool _MoveUniqueCard(UniqueCardData uniqueCard, CardZoneIndex targetZone)
	{
        if (uniqueCard.CurrentZone == targetZone)
		{
            Debug.LogWarning("PlayerBattleData/_MoveUniqueCard: Move to same location.");
			return false;
		}

        if (uniqueCard != null)
        {
            if (uniqueCard.IsInitAbility)
            {
                uniqueCard.DeinitAbility();
            }

            bool isSuccess;
            isSuccess = _RemoveUniqueCardData(uniqueCard);
            if (!isSuccess)
            {
                Debug.LogWarning("PlayerBattleData/_MoveUniqueCard: Failed to remove unique card. " + uniqueCard.UniqueCardID);
                return false;
            }
            isSuccess = _AddUniqueCardData(uniqueCard, targetZone);
            if (!isSuccess)
            {
                Debug.LogWarning("PlayerBattleData/_MoveUniqueCard: Failed to add unique card." + uniqueCard.UniqueCardID);
                return false;
            }

            return true;
        }
        else
        {
            Debug.LogWarning("PlayerBattleData/MoveUniqueCard: uniqueCard is null.");
            return false;
        }
	}

    private bool _MoveUniqueCard(UniqueCardData uniqueCard, BattleZoneIndex battleZoneIndex, bool isLeader, bool isFacedown, out BattleCardData resultBattleCard)
    {
        bool isSuccess = _RemoveUniqueCardData(uniqueCard);
        if (!isSuccess)
        {
            Debug.LogError("PlayerBattleData/MoveUniqueCard: Failed to remove unique card. " + uniqueCard.UniqueCardID);
            resultBattleCard = null;
            return false;
        }

        return _AddBattleCard(uniqueCard.PlayerCardData, uniqueCard.OwnerPlayerIndex, battleZoneIndex, isLeader, isFacedown, out resultBattleCard);
    }

    private bool _AddUniqueCardData(UniqueCardData uniqueCard, CardZoneIndex cardZoneIndex)
    {
        if (cardZoneIndex == CardZoneIndex.Deck)
        {
            uniqueCard.SetPlayerIndex(this.PlayerIndex);
            uniqueCard.SetCurrentZone(CardZoneIndex.Deck);
            Deck.Add(uniqueCard);
            return true;
        }
        else
        {
            List<UniqueCardData> dataList = GetZoneDataList(cardZoneIndex);
            if (dataList == null)
            {
                Debug.LogError("PlayerBattleData/_AddUniqueCardData: not found card zone data list. " + cardZoneIndex.ToString());
                return false;
            }

            uniqueCard.SetPlayerIndex(this.PlayerIndex);
            uniqueCard.SetCurrentZone(cardZoneIndex);
            dataList.Add(uniqueCard);
            return true;
        }
    }

    private bool _AddUniqueCardData(PlayerCardData playerCard, CardZoneIndex cardZoneIndex, out UniqueCardData resultUniqueCard)
    {
        resultUniqueCard = new UniqueCardData(playerCard, this.PlayerIndex, cardZoneIndex);
        bool isSuccess = _AddUniqueCardData(resultUniqueCard, cardZoneIndex);
        if (!isSuccess)
        {
            Debug.LogError("PlayerBattleData/_AddUniqueCardData: Fail to add unique card to " + cardZoneIndex.ToString());
            resultUniqueCard = null;
            return false;
        }

        return true;
    }

    private bool _RemoveUniqueCardData(UniqueCardData uniqueCardData)
    {
        if(uniqueCardData.CurrentZone == CardZoneIndex.Deck)
        {
            UniqueCardData drawCard;
            return Deck.DrawByID(uniqueCardData.UniqueCardID, out drawCard);
        }
        else
        {
            List<UniqueCardData> dataList = GetZoneDataList(uniqueCardData.CurrentZone);
            return dataList.Remove(uniqueCardData);
        }
    }

    // Battle Card

    public bool MoveBattleCard(int battleCardID, BattleZoneIndex targetZone, bool isForceMove = false)
	{
		BattleCardData battleCard;
		bool isFound = FindBattleCardByID(battleCardID, out battleCard);
		if(isFound)
		{
			return _MoveBattleCard(battleCard, targetZone, isForceMove);
		}
		else
		{
            Debug.LogError("PlayerBattleData/MoveBattleCard: not found move battle card.");
			return false;
		}
	}

    public bool MoveBattleCard(int battleCardID, CardZoneIndex targetZone, out UniqueCardData resultUniqueCard)
    {
        BattleCardData battleCard;
        bool isFound = FindBattleCardByID(battleCardID, out battleCard);
        if (isFound)
        {
            return _MoveBattleCard(battleCard, targetZone, out resultUniqueCard);
        }
        else
        {
            Debug.LogError("PlayerBattleData/MoveBattleCard: not found move battle card.");
            resultUniqueCard = null;
            return false;
        }
    }

    private bool _MoveBattleCard(BattleCardData battleCard, BattleZoneIndex targetZone, bool isForceMove = false)
	{
		if(battleCard.CurrentZone == targetZone)
		{
            Debug.LogError("PlayerBattleData/MoveBattleCard: Move to same battle zone.");
			return false;
		}

        bool isSuccess;
        isSuccess = _RemoveBattleCard(battleCard);
		if(!isSuccess)
		{
            Debug.LogError("PlayerBattleData/MoveBattleCard: Can't remove battle card in " + battleCard.CurrentZone.ToString() + ".");
            return false;
        }

		if(isForceMove)
		{
			battleCard.ForceMoveTo(targetZone);
		}
		else
		{
			battleCard.MoveTo(targetZone);
		}

        isSuccess = _AddBattleCard(battleCard, targetZone);
        if (!isSuccess)
        {
            Debug.LogError("PlayerBattleData/MoveBattleCard: Can't add battle card in " + targetZone.ToString() + ".");
            return false;
        }

	    return true;
	}

    private bool _MoveBattleCard(BattleCardData battleCard, CardZoneIndex targetZone, out UniqueCardData resultUniqueCard)
    {
        bool isSuccess;
        isSuccess = _RemoveBattleCard(battleCard);
        if (!isSuccess)
        {
            Debug.LogError("PlayerBattleData/_MoveBattleCard: Can't remove battle card in " + battleCard.CurrentZone.ToString() + ".");
            resultUniqueCard = null;
            return false;
        }

        isSuccess = _AddUniqueCardData(battleCard.PlayerCardData, targetZone, out resultUniqueCard);
        if (!isSuccess)
        {
            Debug.LogError("PlayerBattleData/_MoveBattleCard: Can't add battle card in " + targetZone.ToString() + ".");
             resultUniqueCard = null;
            return false;
        }

        return true;
    }

    private bool _AddBattleCard(BattleCardData battleCard, BattleZoneIndex targetZone)
    {
        List<BattleCardData> targetFieldList = GetFieldDataList(targetZone);
        if (targetFieldList == null)
        {
            Debug.LogError("PlayerBattleData/_AddBattleCard: Can't add battle card in " + targetZone.ToString() + ".");
            return false;
        }

        targetFieldList.Add(battleCard);
        return true;
    }

    private bool _AddBattleCard(PlayerCardData playerCard, PlayerIndex ownerIndex, BattleZoneIndex targetZone, bool isLeader, bool isFacedown, out BattleCardData resultBattleCard)
    {
        resultBattleCard = new BattleCardData(playerCard, ownerIndex, targetZone, isLeader, isFacedown);

        bool isSuccess = _AddBattleCard(resultBattleCard, targetZone);
        if (!isSuccess)
        {
            Debug.LogError("PlayerBattleData/_AddBattleCard: Can't add battle card in " + targetZone.ToString() + ".");
            resultBattleCard = null;
            return false;
        }

        return true;
    }

    private bool _RemoveBattleCard(BattleCardData battleCard)
    {
        List<BattleCardData> targetFieldList = GetFieldDataList(battleCard.CurrentZone);
        if (targetFieldList == null)
        {
            Debug.LogError("PlayerBattleData/_RemoveBattleCard: Can't remove battle card in " + battleCard.CurrentZone.ToString() + ".");
            return false;
        }

        return targetFieldList.Remove(battleCard);
    }
    #endregion

    
    /*
	public bool AddPlayerCardData(CardZoneIndex cardZoneIndex, PlayerCardData playerCardData)
	{
		switch(cardZoneIndex)
		{
			case CardZoneIndex.Deck:
			{
				Deck.Add(playerCardData);
				return true;
			}

			case CardZoneIndex.Hand:
			{
				UniqueCardData data = new UniqueCardData(playerCardData, this.PlayerIndex, CardZoneIndex.Hand);
				_hand.Add(data);

				if(BattleManager.Instance != null)
				{
					IBattleControlController localPlayer;
					bool isFoundLocal = BattleManager.Instance.GetLocalPlayer(out localPlayer);

					// If local player.
					if (isFoundLocal && this.PlayerIndex == localPlayer.GetPlayerIndex())
					{
						BattleManager.Instance.CreateHandCard(data);
					}
				}

				return true;
			}

			case CardZoneIndex.Graveyard:
			{
				UniqueCardData data = new UniqueCardData(playerCardData, this.PlayerIndex, CardZoneIndex.Graveyard);
				_graveyard.Add(data);
				return true;
			}

			case CardZoneIndex.RemoveZone:
			{
				UniqueCardData data = new UniqueCardData(playerCardData, this.PlayerIndex, CardZoneIndex.RemoveZone);
				_removeZone.Add(data);
				return true;
			}
		}
		return false;
	}
    */
	
    /*
	public bool MoveBattleCard(BattleCardData battleCard, CardZoneIndex targetZone)
	{
		List<BattleCardData> targetField = null;
		targetField = _GetFieldDataList(battleCard.CurrentZone);

		if (targetField != null && targetField.Count > 0)
		{
			for (int index = 0; index < targetField.Count; ++index)
			{
				if (targetField[index].BattleCardID == battleCard.BattleCardID)
				{
					battleCard = targetField[index];

					this.AddPlayerCardData(targetZone, battleCard.PlayerCardData);

					battleCard.DeinitCard();
					targetField.Remove(battleCard);

					return true;
				}
			}
		}
			
		return false;
	}
    */
		
    /*
	public bool RemoveUniqueCard(int uniqueCardID)
	{
		UniqueCardData uniqueCard;
		bool isFound = FindUniqueCardByID(uniqueCardID, out uniqueCard);
		if(isFound)
		{
			return RemoveUniqueCard(uniqueCard);
		}
		else
		{
			return false;
		}

		return false;
	}
    */

    /*
	public bool RemoveUniqueCard(UniqueCardData uniqueCardData)
	{
		if(uniqueCardData.CurrentZone == CardZoneIndex.Deck)
		{
			return _deck.Remove(uniqueCardData);
		}
		else
		{
			List<UniqueCardData> currentZoneList = _GetZoneDataList(uniqueCardData.CurrentZone);

			if(uniqueCardData.IsInitAbility)
			{
				uniqueCardData.DeinitAbility();
			}

			return currentZoneList.Remove(uniqueCardData);
		}
		return false;
	}
    */
	
    /*
    public bool GetZoneDataList(CardZoneIndex targetZone, out List<UniqueCardData> data)
    {
        data = _GetZoneDataList(targetZone);
        return (data != null);
    }

	public bool ReactiveBattleCard(int battleCardID)
	{
		BattleCardData data;
		bool isFound = FindBattleCardByID(battleCardID, out data);
		if(isFound)
		{
			if (data.TypeData == CardTypeData.CardType.Unit)
			{
				data.ResetCard();
				return true;
			}
			else
			{
				Debug.LogWarning("PlayerBattleCardData/ReactiveBattleCard: Can't reactive [" + data.Name + " : " + data.TypeData.TypeName +"].");
				return false;
			}
		}

		Debug.LogWarning("PlayerBattleCardData/ReactiveBattleCard: not found battle card.");
		return false;
	}
    */

	public void ReactiveAllBattleCard()
	{
		foreach (BattleZoneIndex battleZoneIndex in System.Enum.GetValues(typeof(BattleZoneIndex)))
		{
			List<BattleCardData> targetField = GetFieldDataList(battleZoneIndex);
			if (targetField != null && targetField.Count > 0)
			{
				for (int index = 0; index < targetField.Count; ++index)
				{
					//if (targetField[index].TypeData == CardTypeData.CardType.Unit)
					{
						targetField[index].ReactiveCard();
					}
				}
			}
		}
	}

	public void ResetAllBattleCard()
	{
		foreach (BattleZoneIndex battleZoneIndex in System.Enum.GetValues(typeof(BattleZoneIndex)))
		{
			List<BattleCardData> targetField = GetFieldDataList(battleZoneIndex);
			if (targetField != null && targetField.Count > 0)
			{
				for (int index = 0; index < targetField.Count; ++index)
				{
					if (targetField[index].TypeData != CardTypeData.CardType.Base)
					{
						targetField[index].ResetCard();
					}
				}
			}
		}
	}

	public void UpdateAllBuffBattleCard()
	{
		foreach (BattleZoneIndex battleZoneIndex in System.Enum.GetValues(typeof(BattleZoneIndex)))
		{
			List<BattleCardData> targetField = GetFieldDataList(battleZoneIndex);
			if (targetField != null && targetField.Count > 0)
			{
				for (int index = 0; index < targetField.Count; ++index)
				{
					//if (targetField[index].TypeData != CardTypeData.CardType.Base)
					{
						targetField[index].UpdateBuff();
					}
				}
			}
		}
	}

	public void UpdateAllBattleCardUI()
	{
		foreach (BattleZoneIndex battleZoneIndex in System.Enum.GetValues(typeof(BattleZoneIndex)))
		{
			List<BattleCardData> targetField = GetFieldDataList(battleZoneIndex);
			if (targetField != null && targetField.Count > 0)
			{
				for (int index = 0; index < targetField.Count; ++index)
				{
					BattleManager.Instance.RequestUpdateBattleCardUI(targetField[index].BattleCardID);
				}
			}
		}
	}

    /*
    public bool Draw(out PlayerCardData drawCard)
    {
        if (!Deck.IsEmpty)
        {
            drawCard = Deck.Draw();
            _hand.Add(drawCard);
            return true;
        }
        else
        {
            drawCard = null;
            return false;
        }
    }
    */

    #region Find Data Methods
    public bool FindIn(string cardID, BattleZoneIndex targetZone, out List<BattleCardData> resultList)
    {
        resultList = new List<BattleCardData>();

        List<BattleCardData> targetField = GetFieldDataList(targetZone);

        if (targetField != null && targetField.Count > 0)
        {
            for (int index = 0; index < targetField.Count; ++index)
            {
                if (string.Compare(targetField[index].CardID, cardID) == 0)
                {
                    resultList.Add(targetField[index]);
                }
            }

            if (resultList.Count > 0)
            {
                return true;
            }
        }

        return false;
    }

    public bool FindIn(CardSymbolData symbolData, BattleZoneIndex targetZone, out List<BattleCardData> resultList)
    {
        resultList = new List<BattleCardData>();

        List<BattleCardData> targetField = GetFieldDataList(targetZone);

        if (targetField != null && targetField.Count > 0)
        {
            for (int index = 0; index < targetField.Count; ++index)
            {
                if (targetField[index].SymbolData == symbolData)
                {
                    resultList.Add(targetField[index]);
                }
            }

            if (resultList.Count > 0)
            {
                return true;
            }
        }

        return false;
    }

    public bool FindIn(CardClanData clanData, BattleZoneIndex targetZone, out List<BattleCardData> resultList)
    {
        resultList = new List<BattleCardData>();

        List<BattleCardData> targetField = GetFieldDataList(targetZone);

        if (targetField != null && targetField.Count > 0)
        {
            for (int index = 0; index < targetField.Count; ++index)
            {
                if (targetField[index].ClanData == clanData)
                {
                    resultList.Add(targetField[index]);
                }
            }

            if (resultList.Count > 0)
            {
                return true;
            }
        }

        return false;
    }

    public bool FindIn(CardTypeData typeData, BattleZoneIndex targetZone, out List<BattleCardData> resultList)
    {
        resultList = new List<BattleCardData>();

        List<BattleCardData> targetField = GetFieldDataList(targetZone);

        if (targetField != null && targetField.Count > 0)
        {
            for (int index = 0; index < targetField.Count; ++index)
            {
                if (targetField[index].TypeData == typeData)
                {
                    resultList.Add(targetField[index]);
                }
            }

            if (resultList.Count > 0)
            {
                return true;
            }
        }

        return false;
    }

    public bool FindIn(CardSubTypeData subTypeData, BattleZoneIndex targetZone, out List<BattleCardData> resultList)
    {
        resultList = new List<BattleCardData>();

        List<BattleCardData> targetField = GetFieldDataList(targetZone);

        if (targetField != null && targetField.Count > 0)
        {
            for (int index = 0; index < targetField.Count; ++index)
            {
                foreach (CardSubTypeData subType in targetField[index].SubTypeDataList)
                {
                    if (subType == subTypeData)
                    {
                        resultList.Add(targetField[index]);
                        break;
                    }
                }
            }

            if (resultList.Count > 0)
            {
                return true;
            }
        }

        return false;
    }

    public bool FindIn(CardJobData jobData, BattleZoneIndex targetZone, out List<BattleCardData> resultList)
    {
        resultList = new List<BattleCardData>();

        List<BattleCardData> targetField = GetFieldDataList(targetZone);

        if (targetField != null && targetField.Count > 0)
        {
            for (int index = 0; index < targetField.Count; ++index)
            {
                foreach (CardJobData job in targetField[index].JobDataList)
                {
                    if (job == jobData)
                    {
                        resultList.Add(targetField[index]);
                        break;
                    }
                }
            }

            if (resultList.Count > 0)
            {
                return true;
            }
        }

        return false;
    }

    public bool FindAll(string cardID, out List<BattleCardData> resultList)
    {
        resultList = new List<BattleCardData>();
        bool isTotalFound = false;

        foreach (BattleZoneIndex battleZoneIndex in System.Enum.GetValues(typeof(BattleZoneIndex)))
        {
            List<BattleCardData> temp;
            bool isFound = FindIn(cardID, battleZoneIndex, out temp);
            if (isFound)
            {
                resultList.AddRange(temp);
                isTotalFound = true;
            }
        }

        return isTotalFound;
    }

    public bool FindAll(CardSymbolData symbolData, out List<BattleCardData> resultList)
    {
        resultList = new List<BattleCardData>();
        bool isTotalFound = false;

        foreach (BattleZoneIndex battleZoneIndex in System.Enum.GetValues(typeof(BattleZoneIndex)))
        {
            List<BattleCardData> temp;
            bool isFound = FindIn(symbolData, battleZoneIndex, out temp);
            if (isFound)
            {
                resultList.AddRange(temp);
                isTotalFound = true;
            }
        }

        return isTotalFound;
    }

    public bool FindAll(CardClanData clanData, out List<BattleCardData> resultList)
    {
        resultList = new List<BattleCardData>();
        bool isTotalFound = false;

        foreach (BattleZoneIndex battleZoneIndex in System.Enum.GetValues(typeof(BattleZoneIndex)))
        {
            List<BattleCardData> temp;
            bool isFound = FindIn(clanData, battleZoneIndex, out temp);
            if (isFound)
            {
                resultList.AddRange(temp);
                isTotalFound = true;
            }
        }

        return isTotalFound;
    }

    public bool FindAll(CardTypeData typeData, out List<BattleCardData> resultList)
    {
        resultList = new List<BattleCardData>();
        bool isTotalFound = false;

        foreach (BattleZoneIndex battleZoneIndex in System.Enum.GetValues(typeof(BattleZoneIndex)))
        {
            List<BattleCardData> temp;
            bool isFound = FindIn(typeData, battleZoneIndex, out temp);
            if (isFound)
            {
                resultList.AddRange(temp);
                isTotalFound = true;
            }
        }

        return isTotalFound;
    }

    public bool FindAll(CardSubTypeData subTypeData, out List<BattleCardData> resultList)
    {
        resultList = new List<BattleCardData>();
        bool isTotalFound = false;

        foreach (BattleZoneIndex battleZoneIndex in System.Enum.GetValues(typeof(BattleZoneIndex)))
        {
            List<BattleCardData> temp;
            bool isFound = FindIn(subTypeData, battleZoneIndex, out temp);
            if (isFound)
            {
                resultList.AddRange(temp);
                isTotalFound = true;
            }
        }

        return isTotalFound;
    }

    public bool FindAll(CardJobData jobData, out List<BattleCardData> resultList)
    {
        resultList = new List<BattleCardData>();
        bool isTotalFound = false;

        foreach (BattleZoneIndex battleZoneIndex in System.Enum.GetValues(typeof(BattleZoneIndex)))
        {
            List<BattleCardData> temp;
            bool isFound = FindIn(jobData, battleZoneIndex, out temp);
            if (isFound)
            {
                resultList.AddRange(temp);
                isTotalFound = true;
            }
        }

        return isTotalFound;
    }

    public bool FindBattleCardByID(int battleCardID, out BattleCardData resultCard)
    {
        foreach (BattleZoneIndex battleZoneIndex in System.Enum.GetValues(typeof(BattleZoneIndex)))
        {
            List<BattleCardData> targetField = GetFieldDataList(battleZoneIndex);
            if (targetField != null && targetField.Count > 0)
            {
                for (int index = 0; index < targetField.Count; ++index)
                {
                    if (targetField[index].BattleCardID == battleCardID)
                    {
                        resultCard = targetField[index];
                        return true;
                    }
                }
            }
        }

        resultCard = null;
        return false;
    }

	public bool FindUniqueCardByID(int uniqueCardID, out UniqueCardData resultCard)
	{
		foreach (CardZoneIndex cardZoneIndex in System.Enum.GetValues(typeof(CardZoneIndex)))
		{
            bool isFound = FindUniqueCardByID(uniqueCardID, cardZoneIndex, out resultCard);
			if(isFound)
			{
				return true;
			}
		}

		resultCard = null;
		return false;
	}

	public bool FindUniqueCardByID(int uniqueCardID, CardZoneIndex zoneIndex, out UniqueCardData resultCard)
	{
        //Debug.Log(uniqueCardID + " " + zoneIndex);
		if(zoneIndex == CardZoneIndex.Deck)
		{
			int foundIndex = _deck.FindByID(uniqueCardID, out resultCard);
            if (foundIndex >= 0)
            {
                //Debug.Log("found  " + foundIndex);
                return true;
            }
            else
            {
                //Debug.Log("not found  " + foundIndex);
                return false;
            }
		}
		else
		{
			List<UniqueCardData> targetZoneDataList = GetZoneDataList(zoneIndex);
			if (targetZoneDataList != null && targetZoneDataList.Count > 0)
			{
				for (int index = 0; index < targetZoneDataList.Count; ++index)
				{
					if (targetZoneDataList[index].UniqueCardID == uniqueCardID)
					{
						resultCard = targetZoneDataList[index];
						return true;
					}
				}
			}
		}
			
		resultCard = null;
		return false;
	}

	public bool FindUniqueCardIn(string cardID, CardZoneIndex zoneIndex, out List<UniqueCardData> resultList)
	{
		resultList = new List<UniqueCardData>();

		if(zoneIndex == CardZoneIndex.Deck)
		{
			return _deck.FindAll(cardID, out resultList);
		}
		else
		{
			List<UniqueCardData> targetField = GetZoneDataList(zoneIndex);

			if (targetField != null && targetField.Count > 0)
			{
				for (int index = 0; index < targetField.Count; ++index)
				{
					if (string.Compare(targetField[index].PlayerCardData.CardID, cardID) == 0)
					{
						resultList.Add(targetField[index]);
					}
				}

				if (resultList.Count > 0)
				{
					return true;
				}
			}
		}

		return false;
	}
		
	public bool FindUniqueCardIn(CardSymbolData symbolData, CardZoneIndex zoneIndex, out List<UniqueCardData> resultList)
	{
		resultList = new List<UniqueCardData>();

		if(zoneIndex == CardZoneIndex.Deck)
		{
			return _deck.FindAll(symbolData, out resultList);
		}
		else
		{
			List<UniqueCardData> targetField = GetZoneDataList(zoneIndex);

			if (targetField != null && targetField.Count > 0)
			{
				for (int index = 0; index < targetField.Count; ++index)
				{
					if (targetField[index].PlayerCardData.SymbolData == symbolData)
					{
						resultList.Add(targetField[index]);
					}
				}

				if (resultList.Count > 0)
				{
					return true;
				}
			}
		}

		return false;
	}
		
	public bool FindUniqueCardIn(CardClanData clanData, CardZoneIndex zoneIndex, out List<UniqueCardData> resultList)
	{
		resultList = new List<UniqueCardData>();

		if(zoneIndex == CardZoneIndex.Deck)
		{
			return _deck.FindAll(clanData, out resultList);
		}
		else
		{
			List<UniqueCardData> targetField = GetZoneDataList(zoneIndex);

			if (targetField != null && targetField.Count > 0)
			{
				for (int index = 0; index < targetField.Count; ++index)
				{
					if (targetField[index].PlayerCardData.ClanData == clanData)
					{
						resultList.Add(targetField[index]);
					}
				}

				if (resultList.Count > 0)
				{
					return true;
				}
			}
		}

		return false;
	}

	public bool FindUniqueCardIn(CardTypeData typeData, CardZoneIndex zoneIndex, out List<UniqueCardData> resultList)
	{
		resultList = new List<UniqueCardData>();

		if(zoneIndex == CardZoneIndex.Deck)
		{
			return _deck.FindAll(typeData, out resultList);
		}
		else
		{
			List<UniqueCardData> targetField = GetZoneDataList(zoneIndex);

			if (targetField != null && targetField.Count > 0)
			{
				for (int index = 0; index < targetField.Count; ++index)
				{
					if (targetField[index].PlayerCardData.TypeData == typeData)
					{
						resultList.Add(targetField[index]);
					}
				}

				if (resultList.Count > 0)
				{
					return true;
				}
			}
		}

		return false;
	}

	public bool FindUniqueCardIn(CardTypeData.CardType cardType, CardZoneIndex zoneIndex, out List<UniqueCardData> resultList)
	{
		return FindUniqueCardIn(new CardTypeData(cardType), zoneIndex, out resultList);
	}

	public bool FindUniqueCardIn(CardSubTypeData subTypeData, CardZoneIndex zoneIndex, out List<UniqueCardData> resultList)
	{
		resultList = new List<UniqueCardData>();

		if(zoneIndex == CardZoneIndex.Deck)
		{
			return _deck.FindAll(subTypeData, out resultList);
		}
		else
		{
			List<UniqueCardData> targetField = GetZoneDataList(zoneIndex);

			if (targetField != null && targetField.Count > 0)
			{
				for (int index = 0; index < targetField.Count; ++index)
				{
					foreach (CardSubTypeData subType in targetField[index].PlayerCardData.SubTypeDataList)
					{
						if (subType == subTypeData)
						{
							resultList.Add(targetField[index]);
							break;
						}
					}
				}

				if (resultList.Count > 0)
				{
					return true;
				}
			}
		}

		return false;
	}

	public bool FindUniqueCardIn(CardSubTypeData.CardSubType subType, CardZoneIndex zoneIndex, out List<UniqueCardData> resultList)
	{
		return FindUniqueCardIn(new CardSubTypeData(subType), zoneIndex, out resultList);
	}

	public bool FindUniqueCardIn(CardJobData jobData, CardZoneIndex zoneIndex, out List<UniqueCardData> resultList)
	{
		resultList = new List<UniqueCardData>();

		if(zoneIndex == CardZoneIndex.Deck)
		{
			return _deck.FindAll(jobData, out resultList);
		}
		else
		{
			List<UniqueCardData> targetField = GetZoneDataList(zoneIndex);

			if (targetField != null && targetField.Count > 0)
			{
				for (int index = 0; index < targetField.Count; ++index)
				{
					foreach (CardJobData job in targetField[index].PlayerCardData.JobDataList)
					{
						if (job == jobData)
						{
							resultList.Add(targetField[index]);
							break;
						}
					}
				}

				if (resultList.Count > 0)
				{
					return true;
				}
			}
		}

		return false;
	}

	public bool FindAllSummonedBaseCard(out List<BattleCardData> resultList)
    {
        return FindAll(new CardTypeData(CardTypeData.EnumCardTypeToID(CardTypeData.CardType.Base)), out resultList);
    }

	public void GetAllSummonedCard(out List<BattleCardData> resultList)
	{
		resultList = new List<BattleCardData>();

		foreach (BattleZoneIndex battleZoneIndex in System.Enum.GetValues(typeof(BattleZoneIndex)))
		{
			List<BattleCardData> targetField = GetFieldDataList(battleZoneIndex);
			if (targetField != null && targetField.Count > 0)
			{
				resultList.AddRange(targetField);
			}
		}
	}
    #endregion
}
