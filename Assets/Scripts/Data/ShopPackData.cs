﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShopPackData
{
	#region Delegates
	public delegate bool IsCardCanRandom(CardData card);
	#endregion

	#region Private Properties
	private string _shopPackID;
	private List<ShopPackDBData> _shopPackDBList;
	private int _overAllChance = 0;
	#endregion

	#region Public Properties
	public string ShopPackID
	{
		get { return _shopPackID; }
	}
	#endregion

	#region Constructors
	public ShopPackData()
	{
		this._shopPackID = "";
		this._shopPackDBList = new List<ShopPackDBData>();
		_overAllChance = 0;
	}

	public ShopPackData(string shopPackID)
	{
		List<ShopPackDBData> shopPackDataList;

		bool isFound = ShopPackDB.GetData(shopPackID , out shopPackDataList);
		if (isFound) 
		{
			this._shopPackID = shopPackID;
			this._shopPackDBList = new List<ShopPackDBData>(shopPackDataList);
			_overAllChance = 0;

			foreach(ShopPackDBData data in this._shopPackDBList)
			{
				_overAllChance += data.iChance;
			}
		}
		else
		{
			this._shopPackID = "";
			this._shopPackDBList = new List<ShopPackDBData>();
			_overAllChance = 0;
		}
	}
		
	// Copy Constructor
	public ShopPackData(ShopPackData data) 
		: this(data.ShopPackID)
	{ 
	}
	#endregion

	#region Methods
	/*
	public string RandomCardID()
	{
		if (this._overAllChance > 0) 
		{
			int r = Random.Range (0, this._overAllChance);

			foreach(ShopPackDBData data in this._shopPackDBList)
			{
				if (r < data.iChance) {
					return data.CardID;
				} else {
					r -= data.iChance;	
				}
			}
		}

		return "";
	}
	*/

	public void RandomNormal(out List<string> cardIDList)
	{
		cardIDList = new List<string>();

		//add common type and mass type
		List<CardSymbolData.CardSymbol> symbolList = new List<CardSymbolData.CardSymbol>();
		symbolList.Add (CardSymbolData.CardSymbol.Mass);
		symbolList.Add (CardSymbolData.CardSymbol.Common);

		//Create common and mass list. 
		List<ShopPackDBData> commonMassList = new List<ShopPackDBData> ();
		int commonMassChance = 0;

		foreach (ShopPackDBData data in _shopPackDBList) 
		{
			foreach(CardSymbolData.CardSymbol symbol in symbolList)
			{
				CardData cardData = new CardData (data.CardID);
				if (cardData.SymbolData == symbol)
				{
					commonMassList.Add (data);
					commonMassChance += data.iChance;
					break;
				}
			}
		}
			
		// Start Random 4 common or mass.
		int cardNum = 4; // 4 common or mass.
		for (int i = 0; i < cardNum; ++i) 
		{
			cardIDList.Add(Random_1(commonMassList, commonMassChance).CardID);
			/*
			int r = Random.Range (0, commonMassChance);
			foreach (ShopPackDBData data in commonMassList) 
			{
				if (r < data.iChance) 
				{
					cardIDList.Add (data.CardID);
					break;
				} else 
				{
					r -= data.iChance;
				}
			}
			*/
		}

		// Random 1 any card. 
		{
			cardIDList.Add(Random_1(this._shopPackDBList, this._overAllChance).CardID);

			/*
			int r = Random.Range (0, this._overAllChance);
			foreach (ShopPackDBData data in this._shopPackDBList) 
			{
				if (r < data.iChance) 
				{
					cardIDList.Add (data.CardID);
					break;
				} else 
				{
					r -= data.iChance;
				}
			}
			*/
		}
	}

	public void RandomPremium(out List<string> cardIDList)
	{
		cardIDList = new List<string>();

		//Create common and mass list. 
		List<ShopPackDBData> commonMassList = new List<ShopPackDBData> ();
		int commonMassChance = 0;

		{
			//add common type and mass type
			List<CardSymbolData.CardSymbol> symbolList = new List<CardSymbolData.CardSymbol> ();
			symbolList.Add (CardSymbolData.CardSymbol.Mass);
			symbolList.Add (CardSymbolData.CardSymbol.Common);

			foreach (ShopPackDBData data in _shopPackDBList) {
				foreach (CardSymbolData.CardSymbol symbol in symbolList) {
					CardData cardData = new CardData (data.CardID);
					if (cardData.SymbolData == symbol) {
						commonMassList.Add (data);
						commonMassChance += data.iChance;
						break;
					}
				}
			}
		}

		//Create uncommon list. 
		List<ShopPackDBData> UncommonList = new List<ShopPackDBData> ();
		int uncommonChance = 0;

		{
			//add common type and mass type
			List<CardSymbolData.CardSymbol> symbolList = new List<CardSymbolData.CardSymbol> ();
			symbolList.Add (CardSymbolData.CardSymbol.Uncommon);

			foreach (ShopPackDBData data in _shopPackDBList) {
				foreach (CardSymbolData.CardSymbol symbol in symbolList) {
					CardData cardData = new CardData (data.CardID);
					if (cardData.SymbolData == symbol) {
						UncommonList.Add (data);
						uncommonChance += data.iChance;
						break;
					}
				}
			}
		}

		//Create rare list. 
		List<ShopPackDBData> rareList = new List<ShopPackDBData> ();
		int rareChance = 0;

		{
			//add rare type
			List<CardSymbolData.CardSymbol> symbolList = new List<CardSymbolData.CardSymbol> ();
			symbolList.Add (CardSymbolData.CardSymbol.Rare);
			symbolList.Add (CardSymbolData.CardSymbol.SuperRare);
			symbolList.Add (CardSymbolData.CardSymbol.Forbidden);

			foreach (ShopPackDBData data in _shopPackDBList) {
				foreach (CardSymbolData.CardSymbol symbol in symbolList) {
					CardData cardData = new CardData (data.CardID);
					if (cardData.SymbolData == symbol) {
						rareList.Add (data);
						rareChance += data.iChance;
						break;
					}
				}
			}
		}

		// Start Random 3 common or mass.
		int cardNum = 3; // 3 common or mass.
		for (int i = 0; i < cardNum; ++i) 
		{
			cardIDList.Add(Random_1(commonMassList, commonMassChance).CardID);

			/*
			int r = Random.Range (0, commonMassChance);
			foreach (ShopPackDBData data in commonMassList) 
			{
				if (r < data.iChance) 
				{
					cardIDList.Add (data.CardID);
					break;
				} else 
				{
					r -= data.iChance;
				}
			}
			*/
		}

		// Random 1 uncommon.
		{
			cardIDList.Add(Random_1(UncommonList, uncommonChance).CardID);

			/*
			int r = Random.Range (0, uncommonChance);
			foreach (ShopPackDBData data in UncommonList) 
			{
				if (r < data.iChance) 
				{
					cardIDList.Add (data.CardID);
					break;
				} else 
				{
					r -= data.iChance;
				}
			}
			*/
		}

		// Random 1 rare.
		{
			cardIDList.Add(Random_1(rareList, rareChance).CardID);

			/*
			int r = Random.Range (0, rareChance);
			foreach (ShopPackDBData data in rareList) 
			{
				if (r < data.iChance) 
				{
					cardIDList.Add (data.CardID);
					break;
				} else 
				{
					r -= data.iChance;
				}
			}
			*/
		}
	}

	public void RandomByCondition(out List<string> cardIDList, int randomCardNum, IsCardCanRandom isCanRandom) 
	{
		cardIDList = new List<string>();

		//Create card list. 
		List<ShopPackDBData> cardList = new List<ShopPackDBData> ();
		int sumChance = 0;

		foreach (ShopPackDBData data in _shopPackDBList) 
		{
			CardData cardData = new CardData (data.CardID);
			if (isCanRandom != null) 
			{
				if(isCanRandom.Invoke(cardData))
				{
					cardList.Add(data);
					sumChance += data.iChance;
				}
			}
			else
			{
				cardList.Add(data);
				sumChance += data.iChance;
			}
		}

		for(int index = 0; index < randomCardNum; ++index)
		{
			cardIDList.Add(Random_1(cardList, sumChance).CardID);
		}
	}

	private ShopPackDBData Random_1(List<ShopPackDBData> dataList, int sumChance)
	{
		int r = Random.Range (0, sumChance);
		foreach (ShopPackDBData data in dataList) 
		{
			if (r < data.iChance) 
			{
				return data;
			} 
			else 
			{
				r -= data.iChance;
			}
		}
		return null;
	}
	#endregion

	#region Relational Operator Overloading
	public static bool operator ==(ShopPackData c1, ShopPackData c2)
	{
		if((System.Object)c1 == null && (System.Object)c2 == null) return true;
		else if((System.Object)c1 == null || (System.Object)c2 == null) return false;
		else return (string.Compare(c1.ShopPackID, c2.ShopPackID) == 0);
	}

	public static bool operator !=(ShopPackData c1, ShopPackData c2)
	{
		return !(c1 == c2);
	}

	public override bool Equals(System.Object obj)
	{
		// Check for null values and compare run-time types.
		if (obj == null || GetType() != obj.GetType())
			return false;

		ShopPackData c = (obj as ShopPackData);
		return (string.Compare(ShopPackID, c.ShopPackID) == 0);
	}

	public override int GetHashCode()
	{
		return 0;
	}
	#endregion
}
