﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#region AbilityCondition Class
public abstract class AbilityCondition
{
	#region AbilityConditionParams Class
	public class AbilityConditionParams : IDParams
    {
        #region AbilityConditionParams Constructors
        public AbilityConditionParams(string id, List<string> parameters)
			: base(id, parameters)
		{
		}
		#endregion

		#region AbilityConditionParams Methods
		public static AbilityCondition ToAbilityCondition(AbilityConditionParams abilityConditionParams)
		{
			switch(abilityConditionParams.ID)
			{
				case "AC0001": return (new AtBeginPhaseAC(abilityConditionParams));
                case "AC0002": return (new AtEndPhaseAC(abilityConditionParams));
                case "AC0003": return (new AtDiscardAC(abilityConditionParams));
				case "AC0004": return (new AtMeEnterAC(abilityConditionParams));
				case "AC0005": return (new AtMeRuinAC(abilityConditionParams));
				case "AC0006": return (new AtMeAttackEnemyBaseSuccessAC(abilityConditionParams));

				case "AC0010": return (new PayAC(abilityConditionParams));
				case "AC0011": return (new ActAC(abilityConditionParams));
				
				case "AC0013": return (new DiscardAC(abilityConditionParams));
				
				case "AC0020": return (new DestroyUnitYouControlAC(abilityConditionParams));

				case "AC0036": return (new RemoveCardAC(abilityConditionParams));
				case "AC0037": return (new RemoveMutantUnitAC(abilityConditionParams));

				case "AC0043": return (new RemoveUnitAC(abilityConditionParams));
			}

			Debug.LogError("AbilityConditionParams/ToAbilityCondition: Not found " + abilityConditionParams.ID + ".");
			return null;
		}
		#endregion
	}
	#endregion

	#region AbilityCondition Properties
	protected string _abilityConditionID;
	protected AbilityData _abilityData = null;
	protected string _description;

    public string AbilityConditionID
	{
        get { return _abilityConditionID; }
	}
	public string Description
	{
		get { return _description; }
	}
	#endregion

	#region AbilityCondition Methods
	public virtual void Init(AbilityData abilityData)
	{
		_abilityData = abilityData;
	}

	public virtual void Deinit()
	{
		_abilityData = null;
	}

    public abstract bool IsCanTrigger();

    public string GetDebugText()
    {
        string resultText = "";
        resultText = string.Format(
              "AbilityConditionID: {0}\nDescription: {1}"
            , this.AbilityConditionID
            , this.Description
        );

        return resultText; 
    }
	#endregion
}
#endregion

#region AtBeginPhaseAC Class
public class AtBeginPhaseAC : AbilityCondition
{
    #region AtBeginPhaseAC Properties
    private DisplayBattlePhase _displayBattlePhase;

    public DisplayBattlePhase DisplayBattlePhase
    {
        get { return _displayBattlePhase; }
    }
    #endregion

    #region AtBeginPhaseAC Constructors
    public AtBeginPhaseAC(AbilityConditionParams abilityConditionParams)
	{
		this._abilityConditionID = abilityConditionParams.ID;
        this._displayBattlePhase = (DisplayBattlePhase)(int.Parse(abilityConditionParams.Parameters[0]));

        this._description = "At beginning of " + this.DisplayBattlePhase.ToString() + " phase.";
	}
	#endregion

    #region AtBeginPhaseAC Methods
	public override void Init(AbilityData abilityData)
    {
		_abilityData = abilityData;

		if(BattleManager.Instance != null && _abilityData != null)
		{
			if(_abilityData.IsSummon)
			{
				BattleManager.Instance.BindBattleEventTrigger(
					  _abilityData.OnBattleEventTrigger
					, _abilityData.OwnerPlayerIndex
					, BattleEventTrigger.BattleTriggerType.BEGIN_OF_PHASE
					, BattleEventTrigger.BattleSide.Friendly
				);
			}
		}
    }

	public override void Deinit()
	{
		if(BattleManager.Instance != null && _abilityData != null)
		{
			if(_abilityData.IsSummon)
			{
				BattleManager.Instance.UnbindBattleEventTrigger(
					  _abilityData.OnBattleEventTrigger
					, _abilityData.OwnerPlayerIndex
					, BattleEventTrigger.BattleTriggerType.BEGIN_OF_PHASE
					, BattleEventTrigger.BattleSide.Friendly
				);
			}
		}

		_abilityData = null;
	}
		
    public override bool IsCanTrigger()
	{
		//Debug.Log("Ability:BeginPhase IsCanTrigger.");

		if(BattleManager.Instance == null) return false;
		if(_abilityData != null && !_abilityData.IsSummon) return false;

		if(_abilityData.OwnerPlayerIndex == BattleManager.Instance.CurrentActivePlayerIndex)
		{
			// My turn.
            switch (this.DisplayBattlePhase)
            {
                case DisplayBattlePhase.GameSetup:
                {
                    if (BattleManager.Instance.CurrentBattlePhase == BattlePhase.BeginGameSetup)
                    {
                        return true;
                    }
                }
                break;

	            case DisplayBattlePhase.Begin:
                {
                    if (BattleManager.Instance.CurrentBattlePhase == BattlePhase.BeginBegin)
                    {
                        return true;
                    }
                }
                break;

				case DisplayBattlePhase.Reactive:
				{
					if (BattleManager.Instance.CurrentBattlePhase == BattlePhase.BeginInactive)
					{
						return true;
					}
				}
				break;

				case DisplayBattlePhase.Reiterate:
				{
					if (BattleManager.Instance.CurrentBattlePhase == BattlePhase.BeginReiterate)
					{
						return true;
					}
				}
				break;

	            case DisplayBattlePhase.Draw:
                {
                    if (BattleManager.Instance.CurrentBattlePhase == BattlePhase.BeginDraw)
                    {
                        return true;
                    }
                }
                break;

	            case DisplayBattlePhase.PreBattle:
                {
                    if (BattleManager.Instance.CurrentBattlePhase == BattlePhase.BeginPreBattle)
                    {
                        return true;
                    }
                }
                break;

	            case DisplayBattlePhase.Battle:
                {
                    if (BattleManager.Instance.CurrentBattlePhase == BattlePhase.BeginBattle)
                    {
                        return true;
                    }
                }
                break;

	            case DisplayBattlePhase.PostBattle:
                {
                    if (BattleManager.Instance.CurrentBattlePhase == BattlePhase.BeginPostBattle)
                    {
                        return true;
                    }
                }
                break;

	            case DisplayBattlePhase.End:
                {
                    if (BattleManager.Instance.CurrentBattlePhase == BattlePhase.BeginEnd)
                    {
                        return true;
                    }
                }
                break;
            }
		}
        
        return false;
	}
	#endregion
}
#endregion

#region AtEndPhaseAC Class
public class AtEndPhaseAC : AbilityCondition
{
    #region AtEndPhaseAC Properties
    private DisplayBattlePhase _displayBattlePhase;

    public DisplayBattlePhase DisplayBattlePhase
    {
        get { return _displayBattlePhase; }
    }
    #endregion

    #region AtEndPhaseAC Constructors
    public AtEndPhaseAC(AbilityConditionParams abilityConditionParams)
    {
        this._abilityConditionID = abilityConditionParams.ID;
        this._displayBattlePhase = (DisplayBattlePhase)(int.Parse(abilityConditionParams.Parameters[0]));

        this._description = "At ending of " + this.DisplayBattlePhase.ToString() + " phase.";
    }
    #endregion

    #region AtEndPhaseAC Methods
	public override void Init(AbilityData abilityData)
	{
		_abilityData = abilityData;

		if(BattleManager.Instance != null && _abilityData != null)
		{
			if(_abilityData.IsSummon)
			{
				BattleManager.Instance.BindBattleEventTrigger(
					  _abilityData.OnBattleEventTrigger
					, _abilityData.OwnerPlayerIndex
					, BattleEventTrigger.BattleTriggerType.END_OF_PHASE
					, BattleEventTrigger.BattleSide.Friendly
				);
			}
		}
	}

	public override void Deinit()
	{
		if(BattleManager.Instance != null && _abilityData != null)
		{
			if(_abilityData.IsSummon)
			{
				BattleManager.Instance.UnbindBattleEventTrigger(
					  _abilityData.OnBattleEventTrigger
					, _abilityData.OwnerPlayerIndex
					, BattleEventTrigger.BattleTriggerType.END_OF_PHASE
					, BattleEventTrigger.BattleSide.Friendly
				);
			}
		}

		_abilityData = null;
	}

    public override bool IsCanTrigger()
    {
		if(BattleManager.Instance == null) return false;
		if(_abilityData != null && !_abilityData.IsSummon) return false;

		if(_abilityData.OwnerPlayerIndex == BattleManager.Instance.CurrentActivePlayerIndex)
		{
			// My turn.
       		switch (this.DisplayBattlePhase)
       		{
	            case DisplayBattlePhase.GameSetup:
	            {
	                if (BattleManager.Instance.CurrentBattlePhase == BattlePhase.EndGameSetup)
	                {
	                    return true;
	                }
	            }
	            break;

	            case DisplayBattlePhase.Begin:
	            {
	                if (BattleManager.Instance.CurrentBattlePhase == BattlePhase.EndBegin)
	                {
	                    return true;
	                }
	            }
	            break;

				case DisplayBattlePhase.Reactive:
				{
					if (BattleManager.Instance.CurrentBattlePhase == BattlePhase.EndInactive)
					{
						return true;
					}
				}
				break;

				case DisplayBattlePhase.Reiterate:
				{
					if (BattleManager.Instance.CurrentBattlePhase == BattlePhase.EndReiterate)
					{
						return true;
					}
				}
				break;

	            case DisplayBattlePhase.Draw:
	            {
	                if (BattleManager.Instance.CurrentBattlePhase == BattlePhase.EndDraw)
	                {
	                    return true;
	                }
	            }
	            break;

	            case DisplayBattlePhase.PreBattle:
	            {
	                if (BattleManager.Instance.CurrentBattlePhase == BattlePhase.EndPreBattle)
	                {
	                    return true;
	                }
	            }
	            break;

	            case DisplayBattlePhase.Battle:
	            {
	                if (BattleManager.Instance.CurrentBattlePhase == BattlePhase.EndBattle)
	                {
	                    return true;
	                }
	            }
	            break;

	            case DisplayBattlePhase.PostBattle:
	            {
	                if (BattleManager.Instance.CurrentBattlePhase == BattlePhase.EndPostBattle)
	                {
	                    return true;
	                }
	            }
	            break;

	            case DisplayBattlePhase.End:
	            {
	                if (BattleManager.Instance.CurrentBattlePhase == BattlePhase.EndEnd)
	                {
	                    return true;
	                }
	            }
	            break;
	        }
		}
 
        return false;
    }
    #endregion
}
#endregion

#region AtDiscardAC Class
public class AtDiscardAC : AbilityCondition
{
	#region AtDiscardAC Properties
    #endregion

	#region AtDiscardAC Constructors
	public AtDiscardAC(AbilityConditionParams abilityConditionParams)
    {
        this._abilityConditionID = abilityConditionParams.ID;

        this._description = "At owner discard.";
    }
    #endregion

	#region AtDiscardAC Methods
	public override void Init(AbilityData abilityData)
	{
		_abilityData = abilityData;

		if(BattleManager.Instance != null && _abilityData != null)
		{
			if(_abilityData.IsSummon)
			{
				BattleManager.Instance.BindBattleEventTrigger(
					  _abilityData.OnBattleEventTrigger
					, _abilityData.OwnerPlayerIndex
					, BattleEventTrigger.BattleTriggerType.PLAYER_DISCARDED
					, BattleEventTrigger.BattleSide.Friendly
				);
			}
		}
	}

	public override void Deinit()
	{
		if(BattleManager.Instance != null && _abilityData != null)
		{
			if(_abilityData.IsSummon)
			{
				BattleManager.Instance.UnbindBattleEventTrigger(
					  _abilityData.OnBattleEventTrigger
					, _abilityData.OwnerPlayerIndex
					, BattleEventTrigger.BattleTriggerType.PLAYER_DISCARDED
					, BattleEventTrigger.BattleSide.Friendly
				);
			}
		}

		_abilityData = null;
	}

    public override bool IsCanTrigger()
    {
		if(BattleManager.Instance != null && _abilityData != null)
		{
			if(_abilityData.IsSummon)
			{
            	return true;
			}
        }

        return false;
    }
    #endregion
}
#endregion

#region AtMeEnterAC Class
public class AtMeEnterAC : AbilityCondition
{
	#region AtMeEnterAC Properties
	#endregion

	#region AtMeEnterAC Constructors
	public AtMeEnterAC(AbilityConditionParams abilityConditionParams)
	{
		this._abilityConditionID = abilityConditionParams.ID;

		this._description = "At me enter.";
	}
	#endregion

	#region AtMeEnterAC Methods
	public override void Init(AbilityData abilityData)
	{
		_abilityData = abilityData;

		if(BattleManager.Instance != null && _abilityData != null)
		{
			if(_abilityData.IsSummon)
			{
				BattleManager.Instance.BindBattleEventTrigger(
					  _abilityData.OnBattleEventTrigger
					, _abilityData.OwnerPlayerIndex
					, BattleEventTrigger.BattleTriggerType.CARD_BE_SUMMONED
					, _abilityData.BattleCard.BattleCardID
				);
			}
		}
	}

	public override void Deinit()
	{
		if(BattleManager.Instance != null && _abilityData != null)
		{
			if(_abilityData.IsSummon)
			{
				BattleManager.Instance.UnbindBattleEventTrigger(
					  _abilityData.OnBattleEventTrigger
					, _abilityData.OwnerPlayerIndex
					, BattleEventTrigger.BattleTriggerType.CARD_BE_SUMMONED
					, _abilityData.BattleCard.BattleCardID
				);
			}
		}

		_abilityData = null;
	}

	public override bool IsCanTrigger()
	{
		if(BattleManager.Instance != null && _abilityData != null)
		{
			if(_abilityData.IsSummon)
			{
				return true;
			}
		}

		return false;
	}
	#endregion
}
#endregion

#region AtMeRuinAC Class
public class AtMeRuinAC : AbilityCondition
{
	#region AtMeRuinAC Properties
	#endregion

	#region AtMeRuinAC Constructors
	public AtMeRuinAC(AbilityConditionParams abilityConditionParams)
	{
		this._abilityConditionID = abilityConditionParams.ID;

		this._description = "At me ruin.";
	}
	#endregion

	#region AtMeRuinAC Methods
	public override void Init(AbilityData abilityData)
	{
		_abilityData = abilityData;

		if(BattleManager.Instance != null && _abilityData != null)
		{
			if(_abilityData.IsSummon)
			{
				BattleManager.Instance.BindBattleEventTrigger(
					  _abilityData.OnBattleEventTrigger
					, _abilityData.OwnerPlayerIndex
					, BattleEventTrigger.BattleTriggerType.CARD_BE_DESTROYED
					, _abilityData.BattleCard.BattleCardID
				);
			}
		}
	}

	public override void Deinit()
	{
		if(BattleManager.Instance != null && _abilityData != null)
		{
			if(_abilityData.IsSummon)
			{
				BattleManager.Instance.UnbindBattleEventTrigger(
					_abilityData.OnBattleEventTrigger
					, _abilityData.OwnerPlayerIndex
					, BattleEventTrigger.BattleTriggerType.CARD_BE_DESTROYED
					, _abilityData.BattleCard.BattleCardID
				);
			}
		}

		_abilityData = null;
	}

	public override bool IsCanTrigger()
	{
		if(BattleManager.Instance != null && _abilityData != null)
		{
			if(_abilityData.IsSummon)
			{
				return true;
			}
		}

		return false;
	}
	#endregion
}
#endregion

#region AtMeAttackEnemyBaseSuccessAC Class
public class AtMeAttackEnemyBaseSuccessAC : AbilityCondition
{
	#region AtMeAttackEnemyBaseSuccessAC Properties
	#endregion

	#region AtMeAttackEnemyBaseSuccessAC Constructors
	public AtMeAttackEnemyBaseSuccessAC(AbilityConditionParams abilityConditionParams)
	{
		this._abilityConditionID = abilityConditionParams.ID;

		this._description = "At me attack enemy base successed.";
	}
	#endregion

	#region AtMeAttackEnemyBaseSuccessAC Methods
	public override void Init(AbilityData abilityData)
	{
		_abilityData = abilityData;

		if(BattleManager.Instance != null && _abilityData != null)
		{
			if(_abilityData.IsSummon)
			{
				BattleManager.Instance.BindBattleEventTrigger(
					_abilityData.OnBattleEventTrigger
					, _abilityData.OwnerPlayerIndex
					, BattleEventTrigger.BattleTriggerType.CARD_ATTACK_BASE_SUCCESSED
					, _abilityData.BattleCard.BattleCardID
				);
			}
		}
	}

	public override void Deinit()
	{
		if(BattleManager.Instance != null && _abilityData != null)
		{
			if(_abilityData.IsSummon)
			{
				BattleManager.Instance.UnbindBattleEventTrigger(
					_abilityData.OnBattleEventTrigger
					, _abilityData.OwnerPlayerIndex
					, BattleEventTrigger.BattleTriggerType.CARD_ATTACK_BASE_SUCCESSED
					, _abilityData.BattleCard.BattleCardID
				);
			}
		}

		_abilityData = null;
	}

	public override bool IsCanTrigger()
	{
		if(BattleManager.Instance != null && _abilityData != null)
		{
			if(_abilityData.IsSummon)
			{
				return true;
			}
		}

		return false;
	}
	#endregion
}
#endregion

#region PayAC Class
public class PayAC : AbilityCondition
{
	#region PayAC Properties
	private int _cost;

	public int Cost { get { return _cost; } }
	#endregion

	#region PayAC Constructors
	public PayAC(AbilityConditionParams abilityConditionParams)
	{
		this._abilityConditionID = abilityConditionParams.ID;

		this._cost = int.Parse(abilityConditionParams.Parameters[0]);

		this._description = "Pay " + this.Cost.ToString() + "G.";
	}
	#endregion

	#region PayAC Methods
	public override void Init(AbilityData abilityData)
	{
		_abilityData = abilityData;
	}

	public override void Deinit()
	{
		_abilityData = null;
	}

	public override bool IsCanTrigger()
	{
		if(BattleManager.Instance != null && _abilityData != null)
		{
			if(BattleManager.Instance.IsCanUseGold(_abilityData.OwnerPlayerIndex, this.Cost))
			{
				return true;
			}
		}

		return false;
	}
	#endregion
}
#endregion

#region ActAC Class
public class ActAC : AbilityCondition
{
	#region ActAC Properties
	#endregion

	#region ActAC Constructors
	public ActAC(AbilityConditionParams abilityConditionParams)
	{
		this._abilityConditionID = abilityConditionParams.ID;

		this._description = "Act.";
	}
	#endregion

	#region ActAC Methods
	public override void Init(AbilityData abilityData)
	{
		_abilityData = abilityData;
	}

	public override void Deinit()
	{
		_abilityData = null;
	}

	public override bool IsCanTrigger()
	{
		if(BattleManager.Instance != null && _abilityData != null)
		{
			if(_abilityData.BattleCard.IsActive)
			{
				return true;
			}
		}

		return false;
	}
	#endregion
}
#endregion

#region DiscardAC Class
public class DiscardAC : AbilityCondition
{
	#region DiscardAC Properties
	private int _num;

	public int Num { get { return _num; } }
	#endregion

	#region DiscardAC Constructors
	public DiscardAC(AbilityConditionParams abilityConditionParams)
	{
		this._abilityConditionID = abilityConditionParams.ID;

		this._num = int.Parse(abilityConditionParams.Parameters[0]);

		this._description = "Discard " + this.Num.ToString() + " card.";
	}
	#endregion

	#region PayAC Methods
	public override void Init(AbilityData abilityData)
	{
		_abilityData = abilityData;
	}

	public override void Deinit()
	{
		_abilityData = null;
	}

	public override bool IsCanTrigger()
	{
		if(BattleManager.Instance != null && _abilityData != null)
		{
			if(this.Num <= BattleManager.Instance.GetHandCount(_abilityData.BattleCard.OwnerPlayerIndex))
			{
				return true;
			}
		}

		return false;
	}
	#endregion
}
#endregion

#region DestroyUnitYouControlAC Class
public class DestroyUnitYouControlAC : AbilityCondition
{
	// AC0020
	#region DestroyUnitYouControlAC Properties
	private int _num;

	public int Num { get { return _num; } }
	#endregion

	#region DestroyUnitYouControlAC Constructors
	public DestroyUnitYouControlAC(AbilityConditionParams abilityConditionParams)
	{
		this._abilityConditionID = abilityConditionParams.ID;

		this._num = int.Parse(abilityConditionParams.Parameters[0]);

		this._description = "Destroy " + this.Num.ToString() + " unit you control.";
	}
	#endregion

	#region DestroyUnitYouControlAC Methods
	public override void Init(AbilityData abilityData)
	{
		_abilityData = abilityData;
	}

	public override void Deinit()
	{
		_abilityData = null;
	}

	public override bool IsCanTrigger()
	{
		if(BattleManager.Instance != null && _abilityData != null)
		{
			List<BattleCardData> target;
			bool isSuccess = GetTargetList(out target);
			if(isSuccess)
			{
				if(this.Num <= target.Count)
				{
					return true;
				}
			}
		}

		return false;
	}

	private bool GetTargetList(out List<BattleCardData> targetList)
	{
		targetList = new List<BattleCardData>();

		List<BattleCardData> target;
		BattleManager.Instance.GetTargetList(
			_abilityData.OwnerPlayerIndex
			, out target
			, this.IsCanBeTarget
		);

		if(target != null && target.Count > 0)
		{
			targetList.AddRange(target);
			return true;
		}

		targetList = null;
		return false;
	}

	private bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown && !battleCard.IsDead)
			{
				if(battleCard.TypeData == CardTypeData.CardType.Unit)
				{
					return true;
				}
			}
		}

		return false;
	}
	#endregion
}
#endregion

#region RemoveCardAC Class
public class RemoveCardAC : AbilityCondition
{
	// AC0036
	#region RemoveCardAC Properties
	private int _num;

	public int Num { get { return _num; } }
	#endregion

	#region RemoveCardAC Constructors
	public RemoveCardAC(AbilityConditionParams abilityConditionParams)
	{
		this._abilityConditionID = abilityConditionParams.ID;

		this._num = int.Parse(abilityConditionParams.Parameters[0]);

		this._description = "Remove " + this.Num.ToString() + " card from graveyard.";
	}
	#endregion

	#region RemoveCardAC Methods
	public override void Init(AbilityData abilityData)
	{
		_abilityData = abilityData;
	}

	public override void Deinit()
	{
		_abilityData = null;
	}

	public override bool IsCanTrigger()
	{
		if(BattleManager.Instance != null && _abilityData != null)
		{
			List<UniqueCardData> target;
			bool isSuccess = GetTargetList(out target);
			if(isSuccess)
			{
				if(this.Num <= target.Count)
				{
					return true;
				}
			}
		}

		return false;
	}

	private bool GetTargetList(out List<UniqueCardData> targetList)
	{
		targetList = new List<UniqueCardData>();

		List<UniqueCardData> target;
		BattleManager.Instance.GetTargetList(
			  _abilityData.OwnerPlayerIndex
			, CardZoneIndex.Graveyard
			, out target
			, this.IsCanBeTarget
		);

		if(target != null && target.Count > 0)
		{
			targetList.AddRange(target);
			return true;
		}

		targetList = null;
		return false;
	}

	protected virtual bool IsCanBeTarget(UniqueCardData uniqueCard)
	{
		if(uniqueCard != null)
		{
			return true;
		}

		return false;
	}
	#endregion
}
#endregion

#region RemoveMutantUnitAC Class
public class RemoveMutantUnitAC : RemoveCardAC
{
	#region RemoveMutantUnitAC Properties
	#endregion

	#region RemoveMutantUnitAC Constructors
	public RemoveMutantUnitAC(AbilityConditionParams abilityConditionParams)
		: base(abilityConditionParams)
	{
		this._description = "Remove " + this.Num.ToString() + " mutant unit from graveyard.";
	}
	#endregion

	#region RemoveMutantUnitAC Methods
	protected override bool IsCanBeTarget(UniqueCardData uniqueCard)
	{
		if(uniqueCard != null)
		{
			if(uniqueCard.PlayerCardData.TypeData == CardTypeData.CardType.Unit)
			{
				if(uniqueCard.PlayerCardData.ClanData == CardClanData.CardClanType.Mutant)
				{
					return true;
				}
			}
		}

		return false;
	}
	#endregion
}
#endregion

#region RemoveUnitAC Class
public class RemoveUnitAC : RemoveCardAC
{
	#region RemoveUnitAC Properties
	#endregion

	#region RemoveUnitAC Constructors
	public RemoveUnitAC(AbilityConditionParams abilityConditionParams)
		: base(abilityConditionParams)
	{
		this._description = "Remove " + this.Num.ToString() + " unit from graveyard.";
	}
	#endregion

	#region RemoveUnitAC Methods
	protected virtual bool IsCanBeTarget(UniqueCardData uniqueCard)
	{
		if(uniqueCard != null)
		{
			if(uniqueCard.PlayerCardData.TypeData == CardTypeData.CardType.Unit)
			{
				return true;
			}
		}

		return false;
	}
	#endregion
}
#endregion