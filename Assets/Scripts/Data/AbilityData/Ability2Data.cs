﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class Ability2Data
{
    #region Enums
    public enum AbilityActiveType
    {
          Always
        , Use
        , Case
    }

    public enum AbilityTriggerType
    {
          Enter
        , Use
        , Remain
        , BeginPhase
        , Ruin
    }

    public enum AbilityTargetType
    {
          None
        , BattleCard
        , UniqueCard
        , Player
    }

    public enum AbilityBenefitType
    {
          Good
        , Neutral
        , Bad
    }

   
    #endregion

    #region Public Properties
    public string AbilityID;

    public string Description_EN;
    public string Description_TH;

    public CardAbility.AbilityActiveZone ActiveZone;
    public CardAbility.AbilityActivePhase ActivePhase;

    public AbilityActiveType ActiveType;
    public List<AbilityTriggerType> TriggerTypeList;
    public AbilityTargetType TargetType;
    public string Target;
    public string Cost;
    public string Condition;
    public string Effect;
    public AbilityBenefitType BenefitType;
    #endregion

    #region Constructors
    public Ability2Data()
    {
        AbilityID = "";

        ActiveZone = new CardAbility.AbilityActiveZone(0);
        ActivePhase = new CardAbility.AbilityActivePhase(0);

        TriggerTypeList = new List<AbilityTriggerType>();
        TargetType = AbilityTargetType.None;
        Target = "";
        Cost = "";
        Condition = "";
        Effect = "";
        BenefitType = AbilityBenefitType.Neutral;

        Description_EN = "";
        Description_TH = "";
    }

    public Ability2Data(
        string abilityID
        , CardAbility.AbilityActiveZone actionZone
        , CardAbility.AbilityActivePhase activePhase
        , AbilityActiveType active
        , List<AbilityTriggerType> triggerTypeList
        , AbilityTargetType target
        , string targetText
        , string cost
        , string condition
        , string effect
        , AbilityBenefitType benefit
        , string description_EN
        , string description_TH
    )
    {
        AbilityID = abilityID;

        ActiveZone = actionZone;
        ActivePhase = activePhase;

        ActiveType = active;
        TriggerTypeList = triggerTypeList;
        TargetType = target;
        Target = targetText;
        Cost = cost;
        Condition = condition;
        Effect = effect;
        BenefitType = benefit;

        Description_EN = description_EN;
        Description_TH = description_TH;
    }

    public Ability2Data(Ability2DBData ability2DBData)
    {
        AbilityID = ability2DBData.AbilityID;

        ActiveZone = new CardAbility.AbilityActiveZone(ability2DBData.ActionZone);
        ActivePhase = new CardAbility.AbilityActivePhase(ability2DBData.ActivePhase);

        switch(ability2DBData.Type)
        {
            case "Always":  ActiveType = AbilityActiveType.Always;    break;
            case "Use":     ActiveType = AbilityActiveType.Use;       break;
            case "Case":    ActiveType = AbilityActiveType.Case;      break;
        }

        TriggerTypeList = new List<AbilityTriggerType>();
        string[] triggerTypeList = ability2DBData.Trigger.Split(',');
        foreach (string triggerTypeText in triggerTypeList)
        {
            switch(triggerTypeText.Trim().ToUpper())
            {
                case "ENTER":       TriggerTypeList.Add(AbilityTriggerType.Enter);      break;
                case "USE":         TriggerTypeList.Add(AbilityTriggerType.Use);        break;
                case "REMAIN":      TriggerTypeList.Add(AbilityTriggerType.Remain);     break;
                case "BEGINPHASE":  TriggerTypeList.Add(AbilityTriggerType.BeginPhase); break;
                case "RUIN":        TriggerTypeList.Add(AbilityTriggerType.Ruin);       break;
            }
        }

        switch(ability2DBData.Type)
        {
            case "BattleCard":  TargetType = AbilityTargetType.BattleCard; break;
            case "UniqueCard":  TargetType = AbilityTargetType.UniqueCard; break;
            case "Player":      TargetType = AbilityTargetType.Player;     break;
            default:            TargetType = AbilityTargetType.None;       break;
        }

        Target = ability2DBData.Target;
        Cost = ability2DBData.Cost;
        Condition = ability2DBData.Condition;
        Effect = ability2DBData.Effect;

        switch(ability2DBData.Benefit)
        {
            case "Good":    BenefitType = AbilityBenefitType.Good;     break;
            case "Neutral": BenefitType = AbilityBenefitType.Neutral;  break;
            case "Bad":     BenefitType = AbilityBenefitType.Bad;      break;
        }

        Description_EN = ability2DBData.Description_EN;
        Description_TH = ability2DBData.Description_TH;
    }
    #endregion

    #region Methods
    public string GetDebugPrintText()
    {
        string triggerText = "";
        if (TriggerTypeList != null && TriggerTypeList.Count > 0)
        {
            foreach (AbilityTriggerType triggerType in TriggerTypeList)
            {
                if (triggerText.Length > 0)
                {
                    triggerText += ", " + triggerType.ToString();
                }
                else
                {
                    triggerText += triggerType.ToString();
                }
            }
        }
        else
        {
            triggerText = "[empty]";
        }

        string resultText;
        resultText = string.Format(
              "AbilityID: {0}\n"
            + "ActionZone: {1}\n"
            + "ActivePhase: {2}\n"
            + "ActiveType: {3}\n"
            + "TriggerTypeList: {4}\n"  
            + "TargetType: {5}\n"
            + "Target: {6}\n"
            + "Cost: {7}\n"
            + "Condition: {8}\n"
            + "Effect: {9}\n"
            + "BenefitType: {10}\n"
            + "Description EN:\n{11}\nDescription TH:\n{12}"
            , AbilityID
            , ActiveZone
            , ActivePhase
            , ActiveType.ToString()
            , triggerText
            , TargetType.ToString()
            , Target
            , Cost
            , Condition
            , Effect
            , BenefitType.ToString()
            , Description_EN
            , Description_TH
        );

        return resultText;
    }

    public void DebugPrintData()
    {
        Debug.Log(GetDebugPrintText());
    }
    #endregion

    #region Static Methods
    public static bool CreateData(string abilityID, out Ability2Data data)
    {
        Ability2DBData dbData;
        bool isSuccess = Ability2DB.GetData(abilityID, out dbData);
        if(isSuccess)
        {
            data = new Ability2Data(dbData);
            return true;
        }

        data = null;
        return false;
    }
    #endregion
}
