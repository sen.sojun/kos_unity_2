﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#region AbilityEffect Class
public abstract class AbilityEffect
{
	#region AbilityEffectParams Class
	public class AbilityEffectParams : IDParams
	{
		#region AbilityEffectParams Constructors
		public AbilityEffectParams(string id, List<string> parameters)
			: base(id, parameters)
		{
		}
		#endregion

		#region AbilityEffectParams Methods
		public static AbilityEffect ToAbilityEffect(AbilityEffectParams abilityEffectParams)
		{
			switch(abilityEffectParams.ID)
			{
				case "AE0001": return (new OwnerGetGoldAE(abilityEffectParams));
				case "AE0002": return (new OwnerDrawCardAE(abilityEffectParams));
				case "AE0003": return (new DealDamageTargetAE(abilityEffectParams));
				case "AE0004": return (new BackToHandAE(abilityEffectParams));
				case "AE0005": return (new ForceBackToHandAE(abilityEffectParams));
				case "AE0006": return (new AirUnitGetBonusMoveAE(abilityEffectParams));
				case "AE0007": return (new UnitGetBonusShieldAE(abilityEffectParams));
				case "AE0008": return (new PlantGetBonus1AT1SPAE(abilityEffectParams));
				case "AE0009": return (new ClanGetBonus1HP1ATAE(abilityEffectParams));
				case "AE0010": return (new OwnerPayGoldAE(abilityEffectParams)); 
				case "AE0011": return (new ActAE(abilityEffectParams)); 
				case "AE0012": return (new TargetGetBonusMoveAE(abilityEffectParams)); 
				case "AE0013": return (new DiscardAE(abilityEffectParams));
				case "AE0014": return (new SelfGetBonusAttackAE(abilityEffectParams));
				case "AE0015": return (new DealDamageAllRangeAE(abilityEffectParams));
				case "AE0016": return (new MeBackToHandAE(abilityEffectParams));
				case "AE0017": return (new SummonAtCostStructureAE(abilityEffectParams));
				case "AE0018": return (new GoToDeckShuffleAE(abilityEffectParams));
				case "AE0019": return (new GetBonusMoveAE(abilityEffectParams));
				case "AE0020": return (new DestroyTargetMyUnitAE(abilityEffectParams));
				case "AE0021": return (new DealDamageTargetRangeAE(abilityEffectParams));
				case "AE0022": return (new DealDamageTargetRangeAirUnitAE(abilityEffectParams));
				case "AE0023": return (new DealDamageTargetRangeGroundUnitAE(abilityEffectParams));
				case "AE0024": return (new DealDamageAllRangeGroundUnitAE(abilityEffectParams));
				case "AE0025": return (new DealDamageTargetEnemyAE(abilityEffectParams));
				case "AE0026": return (new DealDamageByMyNumWeaponTargetUnitAE(abilityEffectParams));
				case "AE0027": return (new SearchZoneToZoneShuffleAE(abilityEffectParams));
				case "AE0028": return (new SearchZoneWeaponTypeToZoneShuffleAE(abilityEffectParams));
                case "AE0029": return (new SearchZoneStructureToZoneShuffleAE(abilityEffectParams));
                case "AE0030": return (new SearchZoneHarvesterToZoneShuffleAE(abilityEffectParams));
                case "AE0031": return (new SearchZonePlant80ToFieldShuffleAE(abilityEffectParams));
                case "AE0032": return (new SearchZoneUnitToZoneShuffleAE(abilityEffectParams));
                case "AE0033": return (new SearchZoneBerondoToZoneShuffleAE(abilityEffectParams));
                case "AE0034": return (new SearchZoneToFieldShuffleAE(abilityEffectParams));
				case "AE0035": return (new SearchZoneUnitToFieldAE(abilityEffectParams));
				case "AE0036": return (new RemoveCardAE(abilityEffectParams));
				case "AE0037": return (new RemoveMutantUnitAE(abilityEffectParams));
				case "AE0038": return (new Me3HP3ATwith3DialnoUnitAE(abilityEffectParams));
				case "AE0039": return (new MeAT1TurnAE(abilityEffectParams));
				case "AE0040": return (new MyMutantUnit1HP1AT1TurnAE(abilityEffectParams));
				case "AE0041": return (new Me1HP1ATAE(abilityEffectParams));
				case "AE0042": return (new OtherSoldierUnit1HP1ATPiercingAE(abilityEffectParams));
				case "AE0043": return (new RemoveUnitAE(abilityEffectParams));
				case "AE0044": return (new GetGoldIfControlUniqueMutantGetGoldAE(abilityEffectParams));
				case "AE0045": return (new DeckToGaveyardAE(abilityEffectParams));
				case "AE0046": return (new DestroyAllUnitAtFieldAE(abilityEffectParams));
				case "AE0047": return (new DealDamageTargetMyBaseAE(abilityEffectParams));
				case "AE0048": return (new AllUnitAt0AE(abilityEffectParams));
				case "AE0049": return (new DestroyTargetNonLeaderMachineUnitAE(abilityEffectParams));
				case "AE0050": return (new OwnerMayDrawCardAE(abilityEffectParams));
				case "AE0051": return (new TargetNonBaseStructureMinus2HP1AT1SPMachineTurnAE(abilityEffectParams));
                case "AE0052": return (new IfHandGetGoldAE(abilityEffectParams));
                case "AE0053": return (new DestroyTargetAE(abilityEffectParams));
                case "AE0054": return (new DestroyAllAtFieldAE(abilityEffectParams));
                case "AE0055": return (new DestroyTargetNonLeaderUnitLowestHPAE(abilityEffectParams));
				case "AE0056": return (new SearchZoneDiAlnoUnit30ToFieldShuffleAE(abilityEffectParams));
				case "AE0057": return (new OpponentDiscardAE(abilityEffectParams));
				case "AE0058": return (new Me1HP1ATKnockbackWithNoNonMutantUnitAE(abilityEffectParams));
				case "AE0059": return (new Me5HP5ATwith3DialnoUnitAE(abilityEffectParams));
				case "AE0060": return (new DestroyMeAE(abilityEffectParams));
				case "AE0061": return (new TargetUnit3HP3ATTurnAE(abilityEffectParams));
				case "AE0062": return (new DealDamageTargetXSameFieldAE(abilityEffectParams));
				case "AE0063": return (new DealDamageAllUnitAE(abilityEffectParams));
				case "AE0064": return (new TargetBaseGetHPPerUnitAE(abilityEffectParams));
				case "AE0065": return (new TargetSkiraUnitGetHPTurnAE(abilityEffectParams));
				case "AE0066": return (new TargetMutantUnitGet2AT1SPTurnAE(abilityEffectParams));
				case "AE0067": return (new PutWeakCounterTargetUnitRangeAE(abilityEffectParams));
				case "AE0068": return (new GetGoldByMyWorkerAE(abilityEffectParams));
				case "AE0069": return (new DestroyTargetNonLeaderCostAE(abilityEffectParams));
				case "AE0070": return (new MeDiscardRandomAE(abilityEffectParams));
				case "AE0071": return (new ReturnAllNonLeaderSameFieldToOwnerHandAE(abilityEffectParams));
				case "AE0072": return (new DealDamageTargetUnitAE(abilityEffectParams));
				case "AE0073": return (new AllPlayerDiscardAllAE(abilityEffectParams));
				case "AE0074": return (new SearchDeckMutantDroneToFieldShuffleAE(abilityEffectParams));
				case "AE0075": return (new MeGet1HP1ATEachResourceInGaveyardAE(abilityEffectParams));
                case "AE0076": return (new DealDamageAllNonPlantUnitAE(abilityEffectParams));
				case "AE0077": return (new SearchZoneMutantUnitToZoneShuffleAE(abilityEffectParams));
				case "AE0078": return (new YouMayAE(abilityEffectParams));
				case "AE0079": return (new AllOpponentDiscardRandomAE(abilityEffectParams));
            }

			Debug.LogError("AbilityEffectParams/ToAbilityEffect: Not found " + abilityEffectParams.ID + ".");
			return null;
		}
		#endregion
	}
	#endregion

	#region AbilityEffect Delegates
	public delegate void OnFinishPrepare();
	public delegate void OnCancelPrepare();
	public delegate void OnFinishAction();
	#endregion

	#region AbilityEffect Properties
	protected string _abilityEffectID;
	protected AbilityData _abilityData = null;
	protected string _description_EN;
	protected string _description_TH;

    protected OnFinishPrepare _onFinishPrepare = null;
    protected OnCancelPrepare _onCancelPrepare = null;

	public string AbilityEffectID
	{
		get { return _abilityEffectID; }
	}
	public string Description_EN
	{
		get { return _description_EN; }
	}
	public string Description_TH
	{
		get { return _description_TH; }
	}
    public bool IsOwnerTurn
    {   
        get
        {
            if(_abilityData != null)
            {
                return (_abilityData.OwnerPlayerIndex == BattleManager.Instance.CurrentActivePlayerIndex);
            }
            return false;
        }
    }
    public bool IsSummon
    {   
        get
        {
            if(_abilityData != null)
            {
                return (_abilityData.IsSummon);
            }
            return false;
        }
    }
	#endregion

	#region AbilityEffect Constructors
	private AbilityEffect()
	{
		_abilityEffectID = "";
		_description_EN = "";
		_description_TH = "";
	}

	protected AbilityEffect(string abilityEffectID)
	{
		_abilityEffectID = abilityEffectID;

		AbilityEffectDBData effectDBData;
		bool isFoundEffect = AbilityEffectDB.GetData(abilityEffectID, out effectDBData);
		if (isFoundEffect)
		{
			_description_EN = effectDBData.Description_EN;
			_description_TH = effectDBData.Description_TH;
		} 
		else
		{
			_description_EN = "";
			_description_TH = "";
		}
	}
	#endregion

	#region AbilityEffect Methods
	public virtual void Init(AbilityData abilityData)
	{
		//Debug.Log("AbilityEffect: Init");

		_abilityData = abilityData;
	}

	public virtual void Deinit()
	{
		_abilityData = null;
	}

	public virtual bool IsHasTarget()
	{
		return false;	
	}
		
	public virtual bool GetTargetList(out List<BattleCardData> targetList)
	{
		targetList = null;
		return false;	
	}

	public virtual bool GetTargetList(out List<UniqueCardData> targetList)
	{
		targetList = null;
		return false;	
	}

	public abstract void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare);

	public abstract void Action(OnFinishAction onFinishAction);

    public string GetDebugText()
    {
        string resultText = "";
        resultText = string.Format(
			"AbilityEffectID: {0}\nDescription_EN: {1}\nDescription_TH: {2}"
            , this.AbilityEffectID
            , this.Description_EN
			, this.Description_TH
        );

        return resultText;
    }

    public static string GetPiercingText(bool isPiercing)
    {
        if (isPiercing)
        {
            return Localization.Get("ABILITY_PIERCING_TEXT");
        }
        else
        {
            return "";
        }
    }
    public static string RangeIndexToText(int rangeIndex)
    {
        switch (rangeIndex)
        {
            case 1: return "[0]";   // 001
            case 2: return "[1]";   // 010
            case 3: return "[0-1]"; // 011
            case 4: return "[2]";   // 100
            case 5: return "[0,2]"; // 101
            case 6: return "[1-2]"; // 110
            case 7: return "[0-2]"; // 111

            default: return "";
        }
    }
    public static bool IsInRange(BattleCardData me, BattleCardData target, int rangeIndex)
    {
        if (me == null || target == null)
        {
            return false;
        }
        else
        {
            if (rangeIndex == 7) return true; // 111

            if ((rangeIndex & 1) > 0) // 001
            {
                // At range 0
                if (me.CurrentZone == target.CurrentZone)
                {
                    return true;
                }
            }

            if ((rangeIndex & 2) > 0) // 010
            {
                // At range 1
                switch (me.CurrentZone)
                {
                    case BattleZoneIndex.BaseP1:
                    case BattleZoneIndex.BaseP2:
                    {
                        if (target.CurrentZone == BattleZoneIndex.Battlefield) return true;
                    }
                    break;

                    case BattleZoneIndex.Battlefield:
                    {
                        if (target.CurrentZone == BattleZoneIndex.BaseP1 || target.CurrentZone == BattleZoneIndex.BaseP2) return true;
                    }
                    break;
                }
            }

            if ((rangeIndex & 4) > 0) // 100
            {
                // At range 2
                switch (me.CurrentZone)
                {
                    case BattleZoneIndex.BaseP1:
                    {
                        if (target.CurrentZone == BattleZoneIndex.BaseP2) return true;
                    }
                    break;

                    case BattleZoneIndex.BaseP2:
                    {
                        if (target.CurrentZone == BattleZoneIndex.BaseP1) return true;
                    }
                    break;
                }
            }
        }

        return false;
    }
    
    public void AddParamInt(int number)
    {
        if(this._abilityData.AbilityParamList != null)
        {
            this._abilityData.AbilityParamList.Add(number.ToString());
        }
    }
    public bool GetParamInt(out int number)
    {
        if(this._abilityData.AbilityParamList.Count > 0)
        {
            try
            {
                string paramText = this._abilityData.AbilityParamList[0];
                bool isSuccess = int.TryParse(paramText, out number);

                if(isSuccess)
                {
                    this._abilityData.PopAbilityParamList();
                    return true;
                }
            }
            catch(System.Exception e)
            {
                Debug.Log(e.Message.ToString());
            }
        }
        
        number = 0;
        return false;
    }
    public void AddParamBool(bool b)
    {
        if(this._abilityData.AbilityParamList != null)
        {
            this._abilityData.AbilityParamList.Add(b.ToString());
        }
    }
    public bool GetParamBool(out bool b)
    {
        if(this._abilityData.AbilityParamList.Count > 0)
        {
            try
            {
                string paramText = this._abilityData.AbilityParamList[0];
                bool isSuccess = bool.TryParse(paramText, out b);

                if(isSuccess)
                {
                    this._abilityData.PopAbilityParamList();
                    return true;
                }
            }
            catch(System.Exception e)
            {
                Debug.Log(e.Message.ToString());
            }
        }
        
        b = false;
        return false;
    }
    public void AddParamIntList(List<int> intList)
    {
        if(this._abilityData.AbilityParamList != null)
        {
            string result = "";
            bool isSuccess = ConvertIntListToString(intList, out result);
            if(isSuccess)
            {
                this._abilityData.AbilityParamList.Add(result);
            }
        }
    }
    public bool GetParamIntList(out List<int> intList)
    {
        if(this._abilityData.AbilityParamList.Count > 0)
        {
            string paramText = this._abilityData.AbilityParamList[0];
            bool isSuccess = ConvertStringToIntList(paramText, out intList);
            if(isSuccess)
            {
                this._abilityData.PopAbilityParamList();
                return true;
            }
        }
        
        intList = new List<int>();
        return false;
    }
    
    protected static bool ConvertIntListToString(List<int> intList, out string result)
    {
        result = "";
        
        try
        {
            foreach(int number in intList)
            {
                if(result.Length > 0)
                {
                    result += "," + number.ToString();
                }
                else
                {
                    result += number.ToString();
                }
            }
        }
        catch(System.Exception e)
        {
            Debug.Log("ConvertIntListToString: Error " + e.Message.ToString());
            result = "";
            return false;
        }
        
        return true;
    }   
    protected static bool ConvertStringToIntList(string text, out List<int> resultList)
    {
        resultList = new List<int>();
        
        try
        {
            string[] texts = text.Split(',');
            foreach(string numberText in texts)
            {
                int number = int.Parse(numberText);
                resultList.Add(number);
            }
        }
        catch(System.Exception e)
        {
            Debug.Log(string.Format(
                  "ConvertStringToIntList: {0} Error {1}" 
                , text
                , e.Message.ToString()
            ));
            resultList = new List<int>();
            return false;
        }
        
        return true;
    }
    #endregion
}
#endregion

#region OwnerGetGoldAE Class
public class OwnerGetGoldAE : AbilityEffect
{
	// AE0001
	#region OwnerGetGoldAE Properties
	private int _gold;

	public int Gold
	{
		get { return _gold; }
	}
	#endregion

	#region OwnerGetGoldAE Constructors
	public OwnerGetGoldAE(string id, int gold)
		: base(id)
	{
		this._gold = gold;

		_description_EN = string.Format(_description_EN, this.Gold);
		_description_TH = string.Format(_description_TH, this.Gold);
	}

	public OwnerGetGoldAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID, int.Parse(abilityEffectParams.Parameters[0]))
	{
	}
	#endregion

	#region OwnerGetGoldAE Methods
	public override bool IsHasTarget()
	{
		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				return true;
			}
		}

		return false;
	}

	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
		if(onFinishPrepare != null)
		{
			onFinishPrepare.Invoke();
		}
	}

	public override void Action(OnFinishAction onFinishAction)
	{
		if(IsHasTarget())
		{
			//Debug.Log("Player " + _abilityData.OwnerPlayerIndex.ToString() + " get gold " + this.Gold + "G.");

			BattleManager.Instance.RequestGetGold(
				  _abilityData.OwnerPlayerIndex
				, this.Gold
			);
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region OwnerDrawCardAE Class
public class OwnerDrawCardAE : AbilityEffect
{
	// AE0002
	#region OwnerDrawCardAE Properties
	private int _drawNum;

	public int DrawNum
	{
		get { return _drawNum; }
	}
	#endregion

	#region OwnerDrawCardAE Constructors
	public OwnerDrawCardAE(string id, int drawNum)
		: base(id)
	{
		this._drawNum = drawNum;

		_description_EN = string.Format(_description_EN, this.DrawNum);
		_description_TH = string.Format(_description_TH, this.DrawNum);
	}

	public OwnerDrawCardAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID, int.Parse(abilityEffectParams.Parameters[0]))
	{
	}
	#endregion

	#region OwnerDrawCardAE Methods
	public override bool IsHasTarget()
	{
		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				return true;
			}
		}

		return false;
	}

	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
		if(onFinishPrepare != null)
		{
			onFinishPrepare.Invoke();
		}
	}

	public override void Action(OnFinishAction onFinishAction)
	{
		if(IsHasTarget())
		{
			//Debug.Log("Player " + _abilityData.OwnerPlayerIndex.ToString() + " draw " + this.DrawNum + " card" + ((this.DrawNum > 1) ? "s" : "") + ".");
			BattleManager.Instance.RequestDrawCard(
				  _abilityData.OwnerPlayerIndex
				, this.DrawNum
			);
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region DealDamageTargetAE Class
public class DealDamageTargetAE : AbilityEffect
{
	// AE0003
	#region DealDamageTargetAE Properties
	protected int _damage;
	protected int _num;
	protected bool _isPiercing;

	protected List<int> _selectedTargetIDList = null;

	public int Damage { get { return _damage; } }
	public int Num { get { return _num; } }
	public bool IsPiercing { get { return _isPiercing; } }
	#endregion

	#region DealDamageTargetAE Constructors
	public DealDamageTargetAE(string id, int damage, int num, int isPiercing)
		: base(id)
	{
		this._num = num;
		this._damage = damage;
		this._isPiercing = ((isPiercing > 0) ? true : false);

		_description_EN = string.Format(_description_EN, this.Damage, this.Num, GetPiercingText(IsPiercing));
		_description_TH = string.Format(_description_TH, this.Damage, this.Num, GetPiercingText(IsPiercing));
	}

	public DealDamageTargetAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID, int.Parse(abilityEffectParams.Parameters[0]), int.Parse(abilityEffectParams.Parameters[1]), int.Parse(abilityEffectParams.Parameters[2]))
	{
	}
	#endregion

	#region DealDamageTargetAE Methods
	public override bool IsHasTarget()
	{
		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
				{
					bool isHasTarget = BattleManager.Instance.CheckHasTargetList(
						  playerIndex
						, this.IsCanBeTarget
					);
						
					if(isHasTarget) return true;
				}
			}
		}

		return false;
	}
		
	public override bool GetTargetList(out List<BattleCardData> targetList)
	{
		targetList = new List<BattleCardData>();
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			List<BattleCardData> target;
			BattleManager.Instance.GetTargetList(
				  playerIndex
				, out target
				, this.IsCanBeTarget
			);

			if(target != null && target.Count > 0)
			{
				targetList.AddRange(target);
			}
		}

		if(targetList.Count > 0)
		{
			return true;
		}

		targetList = null;
		return false;
	}

	protected virtual bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown && !battleCard.IsDead)
			{
				return true;
			}
		}

		return false;
	}

	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
        /*
        if(_abilityData.IsSummon)
        {
            Debug.Log("DealDamageAE: Prepare " + _abilityData.BattleCard.Name);
        }
        else
        {
            Debug.Log("DealDamageAE: Prepare " + _abilityData.UniqueCard.PlayerCardData.Name);
        }
        */
        _onFinishPrepare = onFinishPrepare;
        _onCancelPrepare = onCancelPrepare;
        
		if(IsHasTarget())
		{
			List<BattleCardData> targetList;
			GetTargetList(out targetList);

			BattleManager.Instance.RequestDealDamage(
				  _abilityData.OwnerPlayerIndex
				, targetList
				, this.Damage
				, this.Num
				, new BattleManager.OnFinishDealDamage(this.OnSelectedTarget)
				, new BattleManager.OnCancelDealDamage(this.OnCancelTarget)
                , (BattleManager.Instance.IsHaveTimeout && !this.IsOwnerTurn)
                , !this.IsSummon
			);
            return;
		}

		OnCancelTarget();
	}

	protected virtual void OnSelectedTarget(List<int> selectedTargetIDList)
    {
        //Debug.Log("DealDamageAE: OnSelectedTarget " + selectedTargetIDList.Count.ToString());
        
        _selectedTargetIDList = selectedTargetIDList;
                
        AddParamIntList(_selectedTargetIDList);
        
		if(_onFinishPrepare != null)
		{
			_onFinishPrepare.Invoke();
		}
	}
    
    protected virtual void OnCancelTarget()
    {
        if(_onCancelPrepare != null)
        {
            _onCancelPrepare.Invoke();
        }
    }

	public override void Action(OnFinishAction onFinishAction)
	{
        /*
        if (_abilityData.IsSummon)
        {
            Debug.Log("DealDamageAE: Action " + _abilityData.BattleCard.Name);
        }
        else
        {
            Debug.Log("DealDamageAE: Action " + _abilityData.UniqueCard.PlayerCardData.Name);
        }
        */
        
        GetParamIntList(out _selectedTargetIDList);

		if(_selectedTargetIDList != null && _selectedTargetIDList.Count > 0)
		{
			foreach(int battleCardID in _selectedTargetIDList)
			{
				BattleManager.Instance.RequestDealDamageBattleCard(
					  battleCardID
					, this.Damage
					, this.IsPiercing
				);
			}
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region DestroyTargetAE Class
public class DestroyTargetAE : AbilityEffect
{
	// AE0053
    #region DestroyTargetAE Properties
    protected int _num;
    protected CardZoneIndex _cardZone;

	protected bool _isCanCancel = false;
    protected List<int> _selectedTargetIDList = null;

    public int Num
    {
        get { return _num; }
    }
	public CardZoneIndex CardZone
	{
		get { return _cardZone; }
	}
    #endregion

    #region DestroyTargetAE Constructors
    public DestroyTargetAE(string id, int num, int cardZoneIndex)
		: base(id)
    {
        this._num = num;
        this._cardZone = (CardZoneIndex)cardZoneIndex;

		_description_EN = string.Format(_description_EN, this.Num, LocalizationManager.CardZoneToText(this.CardZone));
		_description_TH = string.Format(_description_TH, this.Num, LocalizationManager.CardZoneToText(this.CardZone));
    }

    public DestroyTargetAE(AbilityEffectParams abilityEffectParams)
        : this(abilityEffectParams.ID
        , int.Parse(abilityEffectParams.Parameters[0])
        , int.Parse(abilityEffectParams.Parameters[1])
        )
    {
    }
    #endregion

    #region DestroyTargetAE Methods
    public override bool IsHasTarget()
    {
        if (BattleManager.Instance != null)
        {
            if (this._abilityData != null)
            {
                if (this._abilityData.IsSummon)
                {
                    // Unit or Structure Ability
                    return true;
                }
                else
                {
                    // Event Ability
                    List<BattleCardData> targetList = new List<BattleCardData>();
                    bool isFound = GetTargetList(out targetList);
                    if (isFound)
                    {
                        if(targetList != null && targetList.Count > 0)
                        {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    public override bool GetTargetList(out List<BattleCardData> targetList)
    {
        targetList = new List<BattleCardData>();
        foreach (PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
        {
            List<BattleCardData> target;
            BattleManager.Instance.GetTargetList(
                  playerIndex
                , out target
                , this.IsCanBeTarget
            );

            if (target != null && target.Count > 0)
            {
                targetList.AddRange(target);
            }
        }

        if (targetList.Count > 0)
        {
            return true;
        }

        targetList = null;
        return false;
    }

    protected virtual bool IsCanBeTarget(BattleCardData battleCard)
    {
        if (battleCard != null)
        {
            if (!battleCard.IsFaceDown && !battleCard.IsDead)
            {
                return true;
            }
        }

        return false;
    }

    public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
    {
        _onFinishPrepare = onFinishPrepare;
        _onCancelPrepare = onCancelPrepare;
            
        if (IsHasTarget())
        {
            List<BattleCardData> targetList;
            GetTargetList(out targetList);

            if(targetList != null && targetList.Count > 0)
            {
    			string headerText = "";
    			switch (LocalizationManager.CurrentLanguage)
    			{
    				case LocalizationManager.Language.Thai:
    				{
    					headerText = this._description_TH;
    				}
    				break;
    
    				case LocalizationManager.Language.English:
    				default:
    				{
    					headerText = this._description_EN;
    				}
    				break;
    			}
    				
                BattleManager.Instance.RequestSelectTarget(
    				headerText
                    , _abilityData.OwnerPlayerIndex
                    , targetList
                    , this.Num
                    , new BattleManager.OnFinishSelectTargetCallback(this.OnSelectedTarget)
                    , new BattleManager.OnCancelSelectTargetCallback(this.OnCancelTarget)
                    , (BattleManager.Instance.IsHaveTimeout && !this.IsOwnerTurn)
    				, _isCanCancel
                );
                return;
            }
        }
        
        OnCancelTarget();
    }

    protected virtual void OnSelectedTarget(List<int> selectedTargetIDList)
    {
        _selectedTargetIDList = selectedTargetIDList;
        
        AddParamIntList(_selectedTargetIDList);

        if (_onFinishPrepare != null)
        {
            _onFinishPrepare.Invoke();
        }
    }
    
    protected virtual void OnCancelTarget()
    {
        if(_onCancelPrepare != null)
        {
            _onCancelPrepare.Invoke();
        }
    }

    public override void Action(OnFinishAction onFinishAction)
    {
        GetParamIntList(out _selectedTargetIDList);
    
        if (_selectedTargetIDList != null && _selectedTargetIDList.Count > 0)
        {
            foreach (int battleCardID in _selectedTargetIDList)
            {
                BattleManager.Instance.RequestDestroyBattleCard(
                      battleCardID
                    , this._cardZone
                );
            }
        }

        if (onFinishAction != null)
        {
            onFinishAction.Invoke();
        }
    }
    #endregion
}
#endregion

#region DestroyAllAtFieldAE Class
public class DestroyAllAtFieldAE : AbilityEffect
{
	// AE0054
    #region DestroyAllAtFieldAE Properties
    protected LocalBattleZoneIndex _localTargetBattleZone;
    protected CardZoneIndex _targetCardZone;

    protected BattleZoneIndex TargetBattleZone
    {
        get
        {
            if (_abilityData != null)
            {
                if (_abilityData.OwnerPlayerIndex == PlayerIndex.One)
                {
                    switch (_localTargetBattleZone)
                    {
                        case LocalBattleZoneIndex.PlayerBase: return BattleZoneIndex.BaseP1;
                        case LocalBattleZoneIndex.Battlefield: return BattleZoneIndex.Battlefield;
                        case LocalBattleZoneIndex.EnemyBase: return BattleZoneIndex.BaseP2;
                    }
                }
                else
                {
                    switch (_localTargetBattleZone)
                    {
                        case LocalBattleZoneIndex.PlayerBase: return BattleZoneIndex.BaseP2;
                        case LocalBattleZoneIndex.Battlefield: return BattleZoneIndex.Battlefield;
                        case LocalBattleZoneIndex.EnemyBase: return BattleZoneIndex.BaseP1;
                    }
                }
            }

            return BattleZoneIndex.Battlefield;
        }
    }
    #endregion

    #region DestroyAllAtFieldAE Constructors
    public DestroyAllAtFieldAE(string id, int localBattleZone, int cardZoneIndex)
		:base (id)
    {
        this._localTargetBattleZone = (LocalBattleZoneIndex)localBattleZone;
        this._targetCardZone = (CardZoneIndex)cardZoneIndex;

		_description_EN = string.Format(_description_EN, LocalizationManager.LocalBattleZoneToText(this._localTargetBattleZone), LocalizationManager.CardZoneToText(this._targetCardZone));
		_description_TH = string.Format(_description_TH, LocalizationManager.LocalBattleZoneToText(this._localTargetBattleZone), LocalizationManager.CardZoneToText(this._targetCardZone));
    }

    public DestroyAllAtFieldAE(AbilityEffectParams abilityEffectParams)
        : this(abilityEffectParams.ID
        , int.Parse(abilityEffectParams.Parameters[0])
        , int.Parse(abilityEffectParams.Parameters[1])
        )
    {
    }
    #endregion

    #region DestroyAllAtFieldAE Methods
    public override bool IsHasTarget()
    {
        if (BattleManager.Instance != null)
        {
            if (this._abilityData != null)
            {
                if (this._abilityData.IsSummon)
                {
                    // Unit or Structure Ability
                    return true;
                }
                else
                {
                    // Event Ability
					/*
                    List<BattleCardData> targetList = new List<BattleCardData>();
                    bool isFound = GetTargetList(out targetList);
                    if (isFound) return true;
					*/
					return true;
                }
            }
        }

        return false;
    }

    public override bool GetTargetList(out List<BattleCardData> targetList)
    {
        targetList = new List<BattleCardData>();
        foreach (PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
        {
            List<BattleCardData> target;
            BattleManager.Instance.GetTargetList(
                  playerIndex
                , out target
                , this.IsCanBeTarget
            );

            if (target != null && target.Count > 0)
            {
                targetList.AddRange(target);
            }
        }

        if (targetList.Count > 0)
        {
            return true;
        }

        targetList = null;
        return false;
    }

    protected virtual bool IsCanBeTarget(BattleCardData battleCard)
    {
        if (battleCard != null)
        {
            if (!battleCard.IsFaceDown && !battleCard.IsDead)
            {
                if (battleCard.CurrentZone == this.TargetBattleZone)
                {
                    return true;
                }
            }
        }

        return false;
    }

    public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
    {
        if (IsHasTarget())
        {
            if (onFinishPrepare != null)
            {
                onFinishPrepare.Invoke();
            }
        }
        else
        {
            if (onCancelPrepare != null)
            {
                onCancelPrepare.Invoke();
            }
        }
    }

    public override void Action(OnFinishAction onFinishAction)
    {
        if (IsHasTarget())
        {
            List<BattleCardData> targetList;
            GetTargetList(out targetList);

            if (targetList != null && targetList.Count > 0)
            {
                foreach (BattleCardData battleCard in targetList)
                {
                    BattleManager.Instance.RequestDestroyBattleCard(
                          battleCard.BattleCardID
                        , this._targetCardZone
                    );
                }
            }
        }

        if (onFinishAction != null)
        {
            onFinishAction.Invoke();
        }
    }
    #endregion
}
#endregion

#region BackToHandAE Class
public class BackToHandAE : AbilityEffect
{
	// AE0004
	#region BackToHandAE Properties
	private int _num;

	private List<int> _selectedTargetIDList = null;

	public int Num
	{
		get { return _num; }
	}
	#endregion

	#region BackToHandAE Constructors
	public BackToHandAE(string id, int num)
		: base (id)
	{
		this._num = num;

		_description_EN = string.Format(_description_EN, this.Num);
		_description_TH = string.Format(_description_TH, this.Num);
	}

	public BackToHandAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID, int.Parse(abilityEffectParams.Parameters[0]))
	{
	}
	#endregion

	#region BackToHandAE Methods
	public override bool IsHasTarget()
	{
		//Debug.Log("BackToHandAE IsHasTarget");

		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
				{
					//if(playerIndex != _abilityData.OwnerPlayerIndex)
					{
						bool isHasTarget = BattleManager.Instance.CheckHasTargetList(
							  playerIndex
							, this.IsCanBeTarget
						);

						if(isHasTarget) return true;
					}
				}
			}
		}

		return false;
	}

	public override bool GetTargetList(out List<BattleCardData> targetList)
	{
		targetList = new List<BattleCardData>();
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			//if(playerIndex != _abilityData.OwnerPlayerIndex)
			{
				List<BattleCardData> target;
				BattleManager.Instance.GetTargetList(
					  playerIndex
					, out target
					, this.IsCanBeTarget
				);

				if(target != null && target.Count > 0)
				{
					targetList.AddRange(target);
				}
			}
		}

		if(targetList.Count > 0)
		{
			return true;
		}

		targetList = null;
		return false;
	}

	private bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown)
			{
				if(	   (battleCard.TypeData == CardTypeData.CardType.Unit)
					&& (!battleCard.IsLeader)
				)
				{
					return true;
				}
			}
		}

		return false;
	}

	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
        _onFinishPrepare = onFinishPrepare;
        _onCancelPrepare = onCancelPrepare;
            
		if(IsHasTarget())
		{
			List<BattleCardData> targetList;
			GetTargetList(out targetList);
            
			BattleManager.Instance.RequestBackToOwnerHand(
				  _abilityData.OwnerPlayerIndex
				, targetList
				, this.Num
				, new BattleManager.OnFinishBackToOwnerHand(this.OnSelectedTarget)
				, new BattleManager.OnCancelBackToOwnerHand(this.OnCancelTarget)
                , !IsOwnerTurn
                , !this.IsSummon
			);
            return;
		}
        
		OnCancelTarget();
	}

	private void OnSelectedTarget(List<int> selectedTargetIDList)
	{
		_selectedTargetIDList = selectedTargetIDList;
        
        AddParamIntList(_selectedTargetIDList);

		if(_onFinishPrepare != null)
		{
			_onFinishPrepare.Invoke();
		}
	}
    
    private void OnCancelTarget()
    {
        if(_onCancelPrepare != null)
        {
            _onCancelPrepare.Invoke();
        }
    }

	public override void Action(OnFinishAction onFinishAction)
	{
		//Debug.Log("BackToHandAE Action");
        
        GetParamIntList(out _selectedTargetIDList);

		if(_selectedTargetIDList != null && _selectedTargetIDList.Count > 0)
		{
			foreach(int battleCardID in _selectedTargetIDList)
			{
				BattleManager.Instance.RequestDestroyBattleCard(
					  battleCardID
					, CardZoneIndex.Hand
				);
			}
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region ForceBackToHandAE
public class ForceBackToHandAE : AbilityEffect
{
	// AE0005
	#region ForceBackToHandAE Properties
	private int _num;

	private List<int> _selectedTargetIDList = null;

	public int Num
	{
		get { return _num; }
	}
	#endregion

	#region ForceBackToHandAE Constructors
	public ForceBackToHandAE(string id, int num)
		: base(id)
	{
		this._num = num;

		_description_EN = string.Format(_description_EN, this.Num);
		_description_TH = string.Format(_description_TH, this.Num);
	}

	public ForceBackToHandAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID, int.Parse(abilityEffectParams.Parameters[0]))
	{
	}
	#endregion

	#region ForceBackToHandAE Methods
	public override bool IsHasTarget()
	{
		//Debug.Log("BackToHandAE IsHasTarget");

		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				/*
				foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
				{
					//if(playerIndex != _abilityData.OwnerPlayerIndex)
					{
						bool isHasTarget = BattleManager.Instance.CheckHasTargetList(
							  playerIndex
							, this.IsCanBeTarget
						);

						if(isHasTarget) return true;
					}
				}
				*/

				return true;
			}
		}

		return false;
	}

	public override bool GetTargetList(out List<BattleCardData> targetList)
	{
		targetList = new List<BattleCardData>();
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			//if(playerIndex != _abilityData.OwnerPlayerIndex)
			{
				List<BattleCardData> target;
				BattleManager.Instance.GetTargetList(
					playerIndex
					, out target
					, this.IsCanBeTarget
				);

				if(target != null && target.Count > 0)
				{
					targetList.AddRange(target);
				}
			}
		}

		if(targetList.Count > 0)
		{
			return true;
		}

		targetList = null;
		return false;
	}

	private bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown)
			{
				if(	   (battleCard.TypeData == CardTypeData.CardType.Unit)
					&& (!battleCard.IsLeader)
				)
				{
					return true;
				}
			}
		}

		return false;
	}

	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
		//Debug.Log("Prepare: ForceBackToHandAE");
        _onFinishPrepare = onFinishPrepare;
        _onCancelPrepare = onCancelPrepare;

		if(IsHasTarget())
		{
			List<BattleCardData> targetList;
			GetTargetList(out targetList); 

			//Debug.Log("Prepare: targetList:" + targetList.Count.ToString());

			BattleManager.Instance.RequestBackToOwnerHand(
				_abilityData.OwnerPlayerIndex
				, targetList
				, this.Num
				, new BattleManager.OnFinishBackToOwnerHand(this.OnSelectedTarget)
                , new BattleManager.OnCancelBackToOwnerHand(this.OnCancelTarget)
                , !IsOwnerTurn
                , false 
			);
            
            return;
		}
		
        OnCancelTarget();
	}

	private void OnSelectedTarget(List<int> selectedTargetIDList)
	{
		//Debug.Log("Selected: ForceBackToHandAE");

		_selectedTargetIDList = selectedTargetIDList;
        
         AddParamIntList(_selectedTargetIDList);

		if(_onFinishPrepare != null)
		{
			_onFinishPrepare.Invoke();
		}
	}
    
    private void OnCancelTarget()
    {
        if(_onCancelPrepare != null)
        {
            _onCancelPrepare.Invoke();
        }
    }
		
	public override void Action(OnFinishAction onFinishAction)
	{
		//Debug.Log("Action: ForceBackToHandAE");
         GetParamIntList(out _selectedTargetIDList);

		if(_selectedTargetIDList != null && _selectedTargetIDList.Count > 0)
		{
			foreach(int battleCardID in _selectedTargetIDList)
			{
				BattleManager.Instance.RequestDestroyBattleCard(
					  battleCardID
					, CardZoneIndex.Hand
				);
			}
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region BuffAE
public abstract class BuffAE : AbilityEffect
{
	#region BuffAE Properties
	protected bool _isOwnerDestroyed = false;
	protected List<BattleCardData> _targetList = null;
	protected List<CardBuff> _buffList = null;

	public bool IsOwnerDestroyed { get { return _isOwnerDestroyed; } }
	#endregion

	#region BuffAE Constructors
	public BuffAE(string id)
		: base(id)
	{
	}

	public BuffAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID)
	{
	}
	#endregion

	#region BuffAE Methods
	public override void Init(AbilityData abilityData)
	{
		//Debug.Log("Init: BuffAE");

		_abilityData = abilityData;

		if(BattleManager.Instance != null && _abilityData != null)
		{
			if(_abilityData.IsSummon)
			{
				BattleManager.Instance.BindBattleEventTrigger(
					  this.OnOwnerDestroyed
					, _abilityData.OwnerPlayerIndex
					, BattleEventTrigger.BattleTriggerType.CARD_BE_DESTROYED
					, _abilityData.BattleCard.BattleCardID
				);

				BattleManager.Instance.BindBattleEventTrigger(
					this.OnOwnerDestroyed
					, _abilityData.OwnerPlayerIndex
					, BattleEventTrigger.BattleTriggerType.CARD_LEAVED
					, _abilityData.BattleCard.BattleCardID
				);

				BattleManager.Instance.BindBattleEventTrigger(
					  this.OnUnitUpdate
					, _abilityData.OwnerPlayerIndex
					, BattleEventTrigger.BattleTriggerType.CARD_BE_SUMMONED
					, BattleEventTrigger.BattleSide.Friendly
				);

				BattleManager.Instance.BindBattleEventTrigger(
					  this.OnUnitUpdate
					, _abilityData.OwnerPlayerIndex
					, BattleEventTrigger.BattleTriggerType.CARD_BE_DESTROYED
					, BattleEventTrigger.BattleSide.Friendly
				);

				BattleManager.Instance.BindBattleEventTrigger(
					this.OnUnitUpdate
					, _abilityData.OwnerPlayerIndex
					, BattleEventTrigger.BattleTriggerType.CARD_LEAVED
					, BattleEventTrigger.BattleSide.Friendly
				);
			}
		}
	}

	public override void Deinit ()
	{
		//Debug.Log("Deinit: BuffAE");

		if(BattleManager.Instance != null && _abilityData != null)
		{
			if(_abilityData.IsSummon)
			{
				BattleManager.Instance.UnbindBattleEventTrigger(
					  this.OnOwnerDestroyed
					, _abilityData.OwnerPlayerIndex
					, BattleEventTrigger.BattleTriggerType.CARD_BE_DESTROYED
					, _abilityData.BattleCard.BattleCardID
				);

				BattleManager.Instance.UnbindBattleEventTrigger(
					  this.OnOwnerDestroyed
					, _abilityData.OwnerPlayerIndex
					, BattleEventTrigger.BattleTriggerType.CARD_LEAVED
					, _abilityData.BattleCard.BattleCardID
				);

				BattleManager.Instance.UnbindBattleEventTrigger(
					  this.OnUnitUpdate
					, _abilityData.OwnerPlayerIndex
					, BattleEventTrigger.BattleTriggerType.CARD_BE_SUMMONED
					, BattleEventTrigger.BattleSide.Friendly
				);

				BattleManager.Instance.UnbindBattleEventTrigger(
					  this.OnUnitUpdate
					, _abilityData.OwnerPlayerIndex
					, BattleEventTrigger.BattleTriggerType.CARD_BE_DESTROYED
					, BattleEventTrigger.BattleSide.Friendly
				);

				BattleManager.Instance.UnbindBattleEventTrigger(
					this.OnUnitUpdate
					, _abilityData.OwnerPlayerIndex
					, BattleEventTrigger.BattleTriggerType.CARD_LEAVED
					, BattleEventTrigger.BattleSide.Friendly
				);
			}
		}

		OnOwnerDestroyed();

		_abilityData = null;
	}

	protected virtual void OnUnitUpdate()
	{
		//Debug.Log("BuffAE: OnUnitUpdate " + this._abilityData.BattleCard.BattleCardID);

		if(IsOwnerDestroyed) return;

		if(IsActive())
		{
			// Active
			List<BattleCardData> prevTargetList = _targetList;

			List<BattleCardData> targetList = null;
			GetTargetList(out targetList);

			List<BattleCardData> removeTargetList = new List<BattleCardData>();
			List<BattleCardData> oldTargetList = new List<BattleCardData>();
			List<BattleCardData> newTargetList = new List<BattleCardData>();

			if(targetList != null && targetList.Count > 0)
			{
				//Debug.Log("Terget: " + targetList.Count.ToString());

				if(prevTargetList != null && prevTargetList.Count > 0)
				{
					//Debug.Log("Prev: " + prevTargetList.Count.ToString());

					// has old target.
					for(int i = 0; i < targetList.Count; ++i)
					{
						for(int j = 0; j < prevTargetList.Count; ++j)
						{
							if(targetList[i] == prevTargetList[j])
							{
								// Is remain target.
								oldTargetList.Add(targetList[i]);

								targetList.RemoveAt(i);
								prevTargetList.RemoveAt(j);

								--i;
								break;
							}
						}
					}

					if(prevTargetList.Count > 0)
					{
						removeTargetList.AddRange(prevTargetList);
					}

					if(targetList.Count > 0)
					{
						newTargetList.AddRange(targetList);
					}
				}
				else
				{
					//Debug.Log("Prev: No target.");

					// no old target.
					newTargetList.AddRange(targetList);
				}
			}
			else
			{
				//Debug.Log("Terget: No target.");

				// No target.
				// Remove all current target.
				if(prevTargetList != null && prevTargetList.Count > 0)
				{
					removeTargetList.AddRange(prevTargetList);
				}
			}

			/*
			Debug.Log("Remove: " + removeTargetList.Count);
			Debug.Log("New: " + newTargetList.Count);
			Debug.Log("Old: " + oldTargetList.Count);
			*/

			List<BattleCardData> currentTargetList = new List<BattleCardData>();

			for(int index = 0; index < removeTargetList.Count; ++index)
			{
				//Debug.Log("Remove Terget: " + removeTargetList.Count.ToString());
				RemoveBuff(removeTargetList[index]);
			}
			for(int index = 0; index < newTargetList.Count; ++index)
			{
				//Debug.Log("New Terget: " + newTargetList.Count.ToString());
				AddBuff(newTargetList[index]);
			}

			currentTargetList.AddRange(oldTargetList);
			currentTargetList.AddRange(newTargetList);

			_targetList = currentTargetList;
		}
		else
		{
			// Deactive
			if(_targetList != null && _targetList.Count > 0)
			{
				for(int index = 0; index < _targetList.Count; ++index)
				{
					RemoveBuff(_targetList[index]);
				}

				_targetList.Clear();
			}
		}
	}

	protected void OnOwnerDestroyed()
	{
		//Debug.Log("BuffAE: OnOwnerDestroyed");
		if(!_isOwnerDestroyed)
		{
			if(_targetList != null && _targetList.Count > 0)
			{
				while(_targetList.Count > 0)
				{
					BattleCardData battleCard = _targetList[0];
					_targetList.RemoveAt(0);

					RemoveBuff(battleCard);
				}

				_targetList.Clear();
			}
		}

		_isOwnerDestroyed = true;
	}

	protected virtual bool IsActive()
	{
		return true;
	}

	protected virtual void AddBuff(BattleCardData battleCard)
	{
		// do nothing.
	}

	protected virtual void RemoveBuff(BattleCardData battleCard)
	{
		battleCard.RemoveBuff(
			  this._abilityData.BattleCard.BattleCardID
			, this.AbilityEffectID
		);
	}

	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
		if(onFinishPrepare != null)
		{
			onFinishPrepare.Invoke();
		}
	}

	public override void Action(OnFinishAction onFinishAction)
	{
		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region AirUnitGetBonusMoveAE
public class AirUnitGetBonusMoveAE : BuffAE
{
	// AE0006
	#region AirUnitGetBonusMoveAE Properties
	private int _numMove;

	public int NumMove
	{
		get { return _numMove; }
	}
	#endregion

	#region AirUnitGetBonusMoveAE Constructors
	public AirUnitGetBonusMoveAE(string id, int numMove)
		:base(id)
	{
		//Debug.Log("AirUnitGetBonusMoveAE: Constructor");

		this._numMove = numMove;

		_description_EN = string.Format(_description_EN, this.NumMove);
		_description_TH = string.Format(_description_TH, this.NumMove);
	}

	public AirUnitGetBonusMoveAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID, int.Parse(abilityEffectParams.Parameters[0]))
	{
	}
	#endregion

	#region AirUnitGetBonusMoveAE Methods
	public override bool IsHasTarget()
	{
		return true;
	}

	public override bool GetTargetList(out List<BattleCardData> targetList)
	{
		targetList = new List<BattleCardData>();
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			if(playerIndex == _abilityData.OwnerPlayerIndex)
			{
				List<BattleCardData> target;
				BattleManager.Instance.GetTargetList(
					  playerIndex
					, out target
					, this.IsCanBeTarget
				);

				if(target != null && target.Count > 0)
				{
					targetList.AddRange(target);
				}
			}
		}

		if(targetList.Count > 0)
		{
			return true;
		}

		targetList = null;
		return false;
	}

	private bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown)
			{
				if(	   (battleCard.TypeData == CardTypeData.CardType.Unit)
					&& (battleCard.CurrentPassiveIndex == PassiveIndex.PassiveType.AirUnit)
				)
				{
					return true;
				}
			}
		}

		return false;
	}

	protected override void AddBuff(BattleCardData battleCard)
	{
		CardBuff buff = new CardBuff(
			  this.AbilityEffectID
			, this._abilityData.BattleCard.BattleCardID
			, battleCard.BattleCardID
			, 0 // shield
			, 0 // hp
			, 0 // attack
			, 0 // speed
			, 1 // move
			, 0 // attack range
			, new PassiveIndex()
			, new List<CardSubTypeData>()
			, new List<CardJobData>()
			, this.OnUnitUpdate
			, -1 // active turn
		);

		BattleManager.Instance.RequestAddBuffBattleCard(
			  battleCard.BattleCardID
			, buff
		);
	}
	#endregion
}
#endregion

#region UnitGetBonusShieldAE
public class UnitGetBonusShieldAE : BuffAE
{
	// AE0007
	#region UnitGetBonusShieldAE Properties
	private int _shield;

	public int Shield
	{
		get { return _shield; }
	}
	#endregion

	#region UnitGetBonusShieldAE Constructors
	public UnitGetBonusShieldAE(string id, int shield)
		:base(id)
	{
		this._shield = shield;

		_description_EN = string.Format(_description_EN, this.Shield);
		_description_TH = string.Format(_description_TH, this.Shield);
	}

	public UnitGetBonusShieldAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID, int.Parse(abilityEffectParams.Parameters[0]))
	{
	}
	#endregion

	#region UnitGetBonusShieldAE Methods
	public override bool IsHasTarget()
	{
		return true;
	}

	public override bool GetTargetList(out List<BattleCardData> targetList)
	{
		targetList = new List<BattleCardData>();
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			if(playerIndex == _abilityData.OwnerPlayerIndex)
			{
				List<BattleCardData> target;
				BattleManager.Instance.GetTargetList(
					playerIndex
					, out target
					, this.IsCanBeTarget
				);

				if(target != null && target.Count > 0)
				{
					targetList.AddRange(target);
				}
			}
		}

		if(targetList.Count > 0)
		{
			return true;
		}

		targetList = null;
		return false;
	}

	private bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown && !battleCard.IsDead)
			{
				if(battleCard.TypeData == CardTypeData.CardType.Unit)
				{
					return true;
				}
			}
		}

		return false;
	}

	protected override void AddBuff(BattleCardData battleCard)
	{
		CardBuff buff = new CardBuff(
			  this.AbilityEffectID
			, this._abilityData.BattleCard.BattleCardID
			, battleCard.BattleCardID
			, 1 // shield
			, 0 // hp
			, 0 // attack
			, 0 // speed
			, 0 // move
			, 0 // attack range
			, new PassiveIndex()
			, new List<CardSubTypeData>()
			, new List<CardJobData>()
			, this.OnUnitUpdate
			, -1 // active turn
		);

		BattleManager.Instance.RequestAddBuffBattleCard(battleCard.BattleCardID, buff);
	}
	#endregion
}
#endregion

#region PlantGetBonus1AT1SPAE Class
public class PlantGetBonus1AT1SPAE : BuffAE
{
	// AE0008
	#region PlantGetBonus1AT1SPAE Properties
	#endregion

	#region PlantGetBonus1AT1SPAE Constructors
	public PlantGetBonus1AT1SPAE(string id)
		:base(id)
	{
		/*
		_description_EN = string.Format(_description_EN);
		_description_TH = string.Format(_description_TH);
		*/
	}

	public PlantGetBonus1AT1SPAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID)
	{
	}
	#endregion

	#region PlantGetBonus1AT1SPAE Methods
	public override bool IsHasTarget()
	{
		return true;
	}

	public override bool GetTargetList(out List<BattleCardData> targetList)
	{
		targetList = new List<BattleCardData>();
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			if(playerIndex == _abilityData.OwnerPlayerIndex)
			{
				List<BattleCardData> target;
				BattleManager.Instance.GetTargetList(
					playerIndex
					, out target
					, this.IsCanBeTarget
				);

				if(target != null && target.Count > 0)
				{
					targetList.AddRange(target);
				}
			}
		}

		if(targetList.Count > 0)
		{
			return true;
		}

		targetList = null;
		return false;
	}

	private bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown)
			{
                if (battleCard.IsCardSubType(CardSubTypeData.CardSubType.Plant))
                {
                    return true;
                }
			}
		}

		return false;
	}

	protected override void AddBuff(BattleCardData battleCard)
	{
		CardBuff buff = new CardBuff(
			this.AbilityEffectID
			, this._abilityData.BattleCard.BattleCardID
			, battleCard.BattleCardID
			, 0 // shield
			, 0 // hp
			, 1 // attack
			, 1 // speed
			, 0 // move
			, 0 // attack range
			, new PassiveIndex()
			, new List<CardSubTypeData>()
			, new List<CardJobData>()
			, this.OnUnitUpdate
			, -1 // active turn
		);

		BattleManager.Instance.RequestAddBuffBattleCard(battleCard.BattleCardID, buff);
	}
	#endregion
}
#endregion

#region ClanGetBonus1HP1ATAE
public class ClanGetBonus1HP1ATAE : BuffAE
{
	// AE0009
	#region ClanGetBonus1HP1ATAE Properties
	private CardClanData.CardClanType _clanType;
	#endregion

	#region ClanGetBonus1HP1ATAE Constructors
	public ClanGetBonus1HP1ATAE(string id, int clanIndex)
		:base(id)
	{
		_clanType = (CardClanData.CardClanType)clanIndex;

		_description_EN = string.Format(_description_EN, CardClanData.EnumCardClanTypeToText(_clanType));
		_description_TH = string.Format(_description_TH, CardClanData.EnumCardClanTypeToText(_clanType));
	}

	public ClanGetBonus1HP1ATAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID, int.Parse(abilityEffectParams.Parameters[0]))
	{
	}
	#endregion

	#region PlantGetBonus1AT1SPAE Methods
	public override bool IsHasTarget()
	{
		return true;
	}

	public override bool GetTargetList(out List<BattleCardData> targetList)
	{
		targetList = new List<BattleCardData>();
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			if(playerIndex == _abilityData.OwnerPlayerIndex)
			{
				List<BattleCardData> target;
				BattleManager.Instance.GetTargetList(
					playerIndex
					, out target
					, this.IsCanBeTarget
				);

				if(target != null && target.Count > 0)
				{
					targetList.AddRange(target);
				}
			}
		}

		if(targetList.Count > 0)
		{
			return true;
		}

		targetList = null;
		return false;
	}

	private bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown)
			{
				if(battleCard != _abilityData.BattleCard)
				{
					if(battleCard.ClanData == _clanType)
					{
						return true;
					}
				}
			}
		}

		return false;
	}

	protected override void AddBuff(BattleCardData battleCard)
	{
		CardBuff buff = new CardBuff(
			this.AbilityEffectID
			, this._abilityData.BattleCard.BattleCardID
			, battleCard.BattleCardID
			, 0 // shield
			, 1 // hp
			, 1 // attack
			, 0 // speed
			, 0 // move
			, 0 // attack range
			, new PassiveIndex()
			, new List<CardSubTypeData>()
			, new List<CardJobData>()
			, this.OnUnitUpdate
			, -1 // active turn
		);

		BattleManager.Instance.RequestAddBuffBattleCard(battleCard.BattleCardID, buff);
	}
	#endregion
}
#endregion

#region OwnerPayGoldAE
public class OwnerPayGoldAE : AbilityEffect
{
	// AE0010
	#region OwnerPayGoldAE Properties
	private int _gold;

	public int Gold {get { return _gold; } }
	#endregion

	#region OwnerPayGoldAE Constructors
	public OwnerPayGoldAE(string id, int gold)
		: base(id)
	{
		this._gold = gold;

		_description_EN = string.Format(_description_EN, this.Gold);
		_description_TH = string.Format(_description_TH, this.Gold);
	}

	public OwnerPayGoldAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID, int.Parse(abilityEffectParams.Parameters[0]))
	{
	}
	#endregion

	#region OwnerPayGoldAE Methods
	public override bool IsHasTarget()
	{
		//Debug.Log("BackToHandAE IsHasTarget");

		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				if(BattleManager.Instance.IsCanUseGold(this._abilityData.OwnerPlayerIndex, this.Gold))
				{
					return true;
				}
			}
		}

		return false;
	}

	public override bool GetTargetList(out List<BattleCardData> targetList)
	{
		targetList = null;
		return false;
	}

	private bool IsCanBeTarget(BattleCardData battleCard)
	{
		return false;
	}

	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
		//Debug.Log("Prepare: ForceBackToHandAE");

		if(IsHasTarget())
		{
			if(onFinishPrepare != null)
			{
				onFinishPrepare.Invoke();
			}
		}
		else
		{
			if(onCancelPrepare != null)
			{
				onCancelPrepare.Invoke();
			}
		}
	}
		
	public override void Action(OnFinishAction onFinishAction)
	{
		//Debug.Log("Action: ForceBackToHandAE");

		if(IsHasTarget())
		{
			BattleManager.Instance.RequestGetGold(_abilityData.OwnerPlayerIndex, -(this.Gold));
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region ActAE
public class ActAE : AbilityEffect
{
	// AE0011
	#region ActAE Properties
	#endregion

	#region ActAE Constructors
	public ActAE(string id)
		: base(id)
	{
		this._abilityEffectID = id;

		/*
		_description_EN = string.Format(_description_EN);
		_description_TH = string.Format(_description_TH);
		*/
	}

	public ActAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID)
	{
	}
	#endregion

	#region ActAE Methods
	public override bool IsHasTarget()
	{
		//Debug.Log("BackToHandAE IsHasTarget");

		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				if(_abilityData.BattleCard != null && _abilityData.BattleCard.IsActive)
				{
					return true;
				}
			}
		}

		return false;
	}

	public override bool GetTargetList(out List<BattleCardData> targetList)
	{
		targetList = null;
		return false;
	}

	private bool IsCanBeTarget(BattleCardData battleCard)
	{
		return false;
	}

	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
		if(IsHasTarget())
		{
			onFinishPrepare.Invoke();
		}
	}

	public override void Action(OnFinishAction onFinishAction)
	{
		if(IsHasTarget())
		{
			BattleManager.Instance.RequestActBattleCard(_abilityData.BattleCard.BattleCardID);
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region TargetGetBonusMoveAE
public class TargetGetBonusMoveAE : AbilityEffect
{
	// AE0012
	#region TargetGetBonusMoveAE Properties
	private int _move;
	private int _turn;

	public int Move { get { return _move; } }
	public int Turn { get { return _turn; } }

	private List<int> _selectedTargetIDList = null;
	#endregion

	#region TargetGetBonusMoveAE Constructors
	public TargetGetBonusMoveAE(string id, int move, int turn)
		: base(id)
	{
		this._move = move;
		this._turn = turn;

		_description_EN = string.Format(_description_EN, this.Move, this.Turn);
		_description_TH = string.Format(_description_TH, this.Move, this.Turn);
	}

	public TargetGetBonusMoveAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID, int.Parse(abilityEffectParams.Parameters[0]), int.Parse(abilityEffectParams.Parameters[1]))
	{
	}
	#endregion

	#region TargetGetBonusMoveAE Methods
	public override bool IsHasTarget()
	{
		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				//if(_abilityData.BattleCard != null && _abilityData.BattleCard.IsActive)
				{
					return true;
				}
			}
		}

		return false;
	}

	public override bool GetTargetList(out List<BattleCardData> targetList)
	{
		targetList = new List<BattleCardData>();
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			//if(playerIndex == _abilityData.OwnerPlayerIndex)
			{
				List<BattleCardData> target;
				BattleManager.Instance.GetTargetList(
					  playerIndex
					, out target
					, this.IsCanBeTarget
				);

				if(target != null && target.Count > 0)
				{
					targetList.AddRange(target);
				}
			}
		}

		if(targetList.Count > 0)
		{
			return true;
		}

		targetList = null;
		return false;
	}

	private bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown)
			{
				if(battleCard.TypeData == CardTypeData.CardType.Unit)
				{
					return true;
				}
			}
		}

		return false;
	}

	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
		//Debug.Log("Prepare: TargetGetBonusMoveAE");
        _onFinishPrepare = onFinishPrepare;
        _onCancelPrepare = onCancelPrepare;
        
		if(IsHasTarget())
		{
			//Debug.Log("Prepare: TargetGetBonusMoveAE Has Target");

			List<BattleCardData> targetList;
			GetTargetList(out targetList);

			string headerText = "";
			switch (LocalizationManager.CurrentLanguage)
			{
				case LocalizationManager.Language.Thai:
				{
					headerText = this._description_TH;
				}
				break;

				case LocalizationManager.Language.English:
				default:
				{
					headerText = this._description_EN;
				}
				break;
			}

			//Debug.Log("Prepare: targetList:" + targetList.Count.ToString());

			BattleManager.Instance.RequestSelectTarget(
				headerText
				, _abilityData.OwnerPlayerIndex
				, targetList
				, 1
				, new BattleManager.OnFinishSelectTargetCallback(this.OnSelectedTarget)
				, new BattleManager.OnCancelSelectTargetCallback(this.OnCancelTarget)
                , (BattleManager.Instance.IsHaveTimeout && !this.IsOwnerTurn)
				, false
			);
            return;
		}
		
        OnCancelTarget();
	}

	private void OnSelectedTarget(List<int> selectedTargetIDList)
	{
		//Debug.Log("Prepare: OTargetGetBonusMoveAE nSelectedTarget");

		_selectedTargetIDList = selectedTargetIDList;
        AddParamIntList(_selectedTargetIDList);

		if(_onFinishPrepare != null)
		{
			_onFinishPrepare.Invoke();
		}
	}
    
    private void OnCancelTarget()
    {
        if(_onCancelPrepare != null)
        {
            _onCancelPrepare.Invoke();
        }
    }

	public override void Action(OnFinishAction onFinishAction)
	{
        GetParamIntList(out _selectedTargetIDList);
        
		if(_selectedTargetIDList != null && _selectedTargetIDList.Count > 0)
		{
			foreach(int battleCardID in _selectedTargetIDList)
			{
				BattleCardData battleCard;
				bool isFound = BattleManager.Instance.FindBattleCard(
					_abilityData.BattleCard.BattleCardID
					, out battleCard
				);

				if(isFound)
				{
					CardBuff buff = new CardBuff(
						  this.AbilityEffectID
						, -1
						, battleCardID
						, 0 // shield
						, 0 // hp
						, 0 // attack
						, 0 // speed
						, this.Move // move
						, 0 // attack range
						, new PassiveIndex()
						, new List<CardSubTypeData>()
						, new List<CardJobData>()
						, null
						, this.Turn // active turn
					);

					BattleManager.Instance.RequestAddBuffBattleCard(battleCardID, buff);
				}
			}
		}
			
		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region DiscardAE
public class DiscardAE : AbilityEffect
{
	// AE0013
	#region DiscardAE Properties
	private int _num;

	protected bool _isCanCancel = true;
	private List<int> _selectHandCardIDList = null;

	public int Num { get { return _num; } }
	#endregion

	#region DiscardAE Constructors
	public DiscardAE(string id, int num)
		: base(id)
	{
		this._num = num;

		_description_EN = string.Format(_description_EN, this.Num);
		_description_TH = string.Format(_description_TH, this.Num);
	}

	public DiscardAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID, int.Parse(abilityEffectParams.Parameters[0]))
	{
	}
	#endregion

	#region DiscardAE Methods
	public override bool IsHasTarget()
	{
		//Debug.Log("DiscardAE IsHasTarget");

		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				if(_abilityData.BattleCard != null)
				{
					if(this._num <= BattleManager.Instance.GetHandCount(_abilityData.BattleCard.OwnerPlayerIndex))
					{
						return true;
					}
				}
			}
		}

		return false;
	}
		
	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
        _onFinishPrepare = onFinishPrepare;
        _onCancelPrepare = onCancelPrepare;
    
		if(IsHasTarget())
		{
			//Debug.Log("DiscardAE: Has Target");
			string headerText;
			switch (LocalizationManager.CurrentLanguage)
			{
				case LocalizationManager.Language.Thai:
				{
					headerText = this._description_TH;
				}
				break;

				case LocalizationManager.Language.English:
				default:
				{
					headerText = this._description_EN;
				}
				break;
			}

			BattleManager.Instance.StartSelectHandCard(
				  headerText
				, _abilityData.BattleCard.OwnerPlayerIndex
				, this._num
				, new BattleManager.OnFinishSelectHandCardCallback(this.OnSelectHandCard)
				, new BattleManager.OnCancelSelectHandCardCallback(this.OnCancelHandCard)
				, null
                , (BattleManager.Instance.IsHaveTimeout && !this.IsOwnerTurn)
				, _isCanCancel

			);
            return;
		}
        
		OnCancelHandCard();
	}

	private void OnSelectHandCard(List<int> selectHandCardIDList)
	{
		_selectHandCardIDList = selectHandCardIDList;
        AddParamIntList(_selectHandCardIDList);

		if(_onFinishPrepare != null)
		{
			_onFinishPrepare.Invoke();
		}
	}
    
    private void OnCancelHandCard()
    {
        if(_onCancelPrepare != null)
        {
            _onCancelPrepare.Invoke();
        }
    }

	public override void Action(OnFinishAction onFinishAction)
	{
        GetParamIntList(out _selectHandCardIDList);
        
		if(_selectHandCardIDList != null && _selectHandCardIDList.Count > 0)
		{
            foreach(int discardID in _selectHandCardIDList)
            {
			    BattleManager.Instance.ActionMoveUniqueCard(discardID, CardZoneIndex.Graveyard);
            }
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region MeDiscardRandomAE
public class MeDiscardRandomAE : AbilityEffect
{
	// AE0070
	#region DiscardAE Properties
	private int _num;

	protected bool _isCanCancel = true;
	private List<int> _selectHandCardIDList = null;

	public int Num { get { return _num; } }
	#endregion

	#region MeDiscardRandomAE Constructors
	public MeDiscardRandomAE(string id, int num)
		: base(id)
	{
		this._num = num;

		_description_EN = string.Format(_description_EN, this.Num);
		_description_TH = string.Format(_description_TH, this.Num);
	}

	public MeDiscardRandomAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID, int.Parse(abilityEffectParams.Parameters[0]))
	{
	}
	#endregion

	#region MeDiscardRandomAE Methods
	public override bool IsHasTarget()
	{
		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				return true;
			}
		}

		return false;
	}

	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
        _onFinishPrepare = onFinishPrepare;
        _onCancelPrepare = onCancelPrepare;
    
		if(IsHasTarget())
		{
			BattleManager.Instance.StartSelectHandCardRandom(
                  _abilityData.BattleCard.OwnerPlayerIndex
				, _abilityData.BattleCard.OwnerPlayerIndex
				, this._num
				, new BattleManager.OnFinishSelectHandCardCallback(this.OnSelectHandCard)
                , new BattleManager.OnCancelSelectHandCardCallback(this.OnCancelHandCard)
				, null
			);
            
            return;
		}

		OnCancelHandCard();
	}

	private void OnSelectHandCard(List<int> selectHandCardIDList)
	{
		_selectHandCardIDList = selectHandCardIDList;
        
         AddParamIntList(_selectHandCardIDList);

		if(_onFinishPrepare != null)
		{
			_onFinishPrepare.Invoke();
		}
	}
    
    private void OnCancelHandCard()
    {
        if(_onCancelPrepare != null)
        {
            _onCancelPrepare.Invoke();
        }
    }

	public override void Action(OnFinishAction onFinishAction)
	{
        GetParamIntList(out _selectHandCardIDList);
    
		if(_selectHandCardIDList != null && _selectHandCardIDList.Count > 0)
        {
			foreach(int discardID in _selectHandCardIDList)
			{
		        BattleManager.Instance.ActionMoveUniqueCard(discardID, CardZoneIndex.Graveyard);
			}
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region AllPlayerDiscardAllAE
public class AllPlayerDiscardAllAE : AbilityEffect
{
	// AE0073
	#region AllPlayerDiscardAllAE Properties
	#endregion

	#region AllPlayerDiscardAllAE Constructors
	public AllPlayerDiscardAllAE(string id)
		: base(id)
	{
		/*
		_description_EN = string.Format(_description_EN);
		_description_TH = string.Format(_description_TH);
		*/
	}

	public AllPlayerDiscardAllAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID)
	{
	}
	#endregion

	#region AllPlayerDiscardAllAE Methods
	public override bool IsHasTarget()
	{
		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				return true;
			}
		}

		return false;
	}

	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
		if(IsHasTarget())
		{
			if(onFinishPrepare != null)
			{
				onFinishPrepare.Invoke();
			}
		}
		else
		{
			if(onCancelPrepare != null)
			{
				onCancelPrepare.Invoke();
			}
		}
	}
		
	public override void Action(OnFinishAction onFinishAction)
	{
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			BattleManager.Instance.MoveAllHandCardTo (playerIndex, CardZoneIndex.Graveyard);
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region SelfGetBonusAttackAE
public class SelfGetBonusAttackAE : AbilityEffect
{
	// AE0014
	#region SelfGetBonusAttackAE Properties
	private int _attack;

	public int Attack { get { return _attack; } }
	#endregion

	#region SelfGetBonusAttackAE Constructors
	public SelfGetBonusAttackAE(string id, int attack)
		: base(id)
	{
		this._attack = attack;

		_description_EN = string.Format(_description_EN, this.Attack);
		_description_TH = string.Format(_description_TH, this.Attack);
	}

	public SelfGetBonusAttackAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID, int.Parse(abilityEffectParams.Parameters[0]))
	{
	}
	#endregion

	#region SelfGetBonusAttackAE Methods
	public override bool IsHasTarget()
	{
		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				if(_abilityData.BattleCard != null)
				{
					return true;
				}
			}
		}

		return false;
	}
		
	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
		//Debug.Log("Prepare: SelfGetBonusAttackAE");

		if(IsHasTarget())
		{
			if(onFinishPrepare != null)
			{
				onFinishPrepare.Invoke();
			}
		}
		else
		{
			if(onCancelPrepare != null)
			{
				onCancelPrepare.Invoke();
			}
		}
	}

	public override void Action(OnFinishAction onFinishAction)
	{
		if(IsHasTarget())
		{
			BattleCardData battleCard;
			bool isFound = BattleManager.Instance.FindBattleCard(
				  _abilityData.BattleCard.BattleCardID
				, out battleCard
			);

			if(isFound)
			{
				CardBuff buff = new CardBuff(
					this.AbilityEffectID
					, -1
					, battleCard.BattleCardID
					, 0 // shield
					, 0 // hp
					, this.Attack // attack
					, 0 // speed
					, 0 // move
					, 0 // attack range
					, new PassiveIndex()
					, new List<CardSubTypeData>()
					, new List<CardJobData>()
					, null
					, 1 // active turn
				);

				BattleManager.Instance.RequestAddBuffBattleCard(battleCard.BattleCardID, buff);
			}
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region DealDamageAllRangeAE
public class DealDamageAllRangeAE : AbilityEffect
{
	// AE0015
	#region DealDamageAllRangeAE Properties
	private int _damage;
	private int _rangeIndex;
	private bool _isPiercing;

	public int Damage { get { return _damage; } }
	public int RangeIndex { get { return _rangeIndex; } }
	public 	bool IsPiercing { get { return _isPiercing; } }
	#endregion

	#region DealDamageAllRangeAE Constructors
	public DealDamageAllRangeAE(string id, int damage, int rangeIndex, int isPiercing)
		: base(id)
	{
		//"ABILITY_PIERCING_TEXT"

		this._damage = damage;
		this._rangeIndex = rangeIndex;
		this._isPiercing = ((isPiercing > 0) ? true : false);

		_description_EN = string.Format(_description_EN, this.Damage, AbilityEffect.RangeIndexToText(this.RangeIndex), GetPiercingText(IsPiercing));
		_description_TH = string.Format(_description_TH, this.Damage, AbilityEffect.RangeIndexToText(this.RangeIndex), GetPiercingText(IsPiercing));
	}

	public DealDamageAllRangeAE(AbilityEffectParams abilityEffectParams)
		: this(
			abilityEffectParams.ID
			, int.Parse(abilityEffectParams.Parameters[0])
			, int.Parse(abilityEffectParams.Parameters[1])
			, int.Parse(abilityEffectParams.Parameters[2])
		)
	{
	}
	#endregion

	#region DealDamageAllRangeAE Methods
	public override bool IsHasTarget()
	{
		if(BattleManager.Instance != null)
		{
			if(_abilityData != null)
			{
				if(_abilityData.IsSummon)
				{
					return true;
				}
			}
		}

		return false;
	}

	public override bool GetTargetList(out List<BattleCardData> targetList)
	{
		targetList = new List<BattleCardData>();
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			List<BattleCardData> target;
			BattleManager.Instance.GetTargetList(
				  playerIndex
				, out target
				, this.IsCanBeTarget
			);

			if(target != null && target.Count > 0)
			{
				targetList.AddRange(target);
			}
		}

		if(targetList.Count > 0)
		{
			return true;
		}

		targetList = null;
		return false;
	}

	protected virtual bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null && _abilityData.IsSummon)
		{
			if(!battleCard.IsFaceDown && !battleCard.IsDead)
			{
                if (AbilityEffect.IsInRange(_abilityData.BattleCard, battleCard, this.RangeIndex))
                {
                    return true;
                }
			}
		}

		return false;
	}
		
	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
		//Debug.Log("Prepare: DealDamageAllRangeAE");

		if(onFinishPrepare != null)
		{
			onFinishPrepare.Invoke();
		}
	}
		
	public override void Action(OnFinishAction onFinishAction)
	{
		//Debug.Log("Action: DealDamageAllRangeAE");

		if(IsHasTarget())
		{
			List<BattleCardData> targetList;
			GetTargetList(out targetList);

			if(targetList != null && targetList.Count > 0)
			{
				foreach(BattleCardData targetCard in targetList)
				{
					BattleManager.Instance.RequestDealDamageBattleCard(
						  targetCard.BattleCardID
						, this.Damage
						, this.IsPiercing
					);
				}
			}
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region MeBackToHandAE Class
public class MeBackToHandAE : AbilityEffect
{
	// AE0016
	#region MeBackToHandAE Properties
	#endregion

	#region MeBackToHandAE Constructors
	public MeBackToHandAE(string id)
		:base(id)
	{
		/*
		_description_EN = string.Format(_description_EN);
		_description_TH = string.Format(_description_TH);
		*/
	}

	public MeBackToHandAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID)
	{
	}
	#endregion

	#region MeBackToHandAE Methods
	public override bool IsHasTarget()
	{
		//Debug.Log("BackToHandAE IsHasTarget");

		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				if(this._abilityData.IsSummon)
				{
					return true;
				}
			}
		}

		return false;
	}
		
	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
		if(IsHasTarget())
		{
			if(onFinishPrepare != null)
			{
				onFinishPrepare.Invoke();
			}
		}
		else
		{
			if(onCancelPrepare != null)
			{
				onCancelPrepare.Invoke();
			}
		}
	}

	public override void Action(OnFinishAction onFinishAction)
	{
		//Debug.Log("MeBackToHandAE Action");

		if(IsHasTarget())
		{
			BattleManager.Instance.RequestDestroyBattleCard(
				  _abilityData.BattleCard.BattleCardID
				, CardZoneIndex.Hand
			);
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region SummonAtCostStructureAE Class
public class SummonAtCostStructureAE : AbilityEffect
{
	// AE0017
	#region SummonAtCostStructureAE Properties
	private int _cost;

	private int _handCardID = -1;
	private bool _isHasTarget = false;

	public int Cost
	{
		get { return _cost; }
	}
	#endregion

	#region SummonAtCostStructureAE Constructors
	public SummonAtCostStructureAE(string id, int cost)
		: base(id)
	{
		this._cost = cost;

		_description_EN = string.Format(_description_EN, this.Cost);
		_description_TH = string.Format(_description_TH, this.Cost);
	}

	public SummonAtCostStructureAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID, int.Parse(abilityEffectParams.Parameters[0]))
	{
	}
	#endregion

	#region SummonAtCostStructureAE Methods
	public override bool IsHasTarget()
	{
		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				if(this._abilityData.IsSummon)
				{
					return true;
				}
			}
		}

		return false;
	}

	public bool IsCanBeTarget(PlayerCardData card)
	{
		if(card.TypeData == CardTypeData.CardType.Structure || card.TypeData == CardTypeData.CardType.Base)
		{
			if(card.Cost <= this.Cost)
			{
				return true;
			}
		}

		return false;
	}

	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
        _onFinishPrepare = onFinishPrepare;
        _onCancelPrepare = onCancelPrepare;
    
		if(IsHasTarget())
		{
			_isHasTarget = false;

			BattleManager.Instance.StartSelectHandCard(
				  "Choose a structure card."
				, this._abilityData.OwnerPlayerIndex
				, 1
				, new BattleManager.OnFinishSelectHandCardCallback(this.OnSelectedHandCard)
				, new BattleManager.OnCancelSelectHandCardCallback(this.OnCancelHandCard)
				, new BattleManager.IsHandCardCanSelect(this.IsCanBeTarget)
                , (BattleManager.Instance.IsHaveTimeout && !this.IsOwnerTurn)
				, false
			);
            return;
		}
		
        OnCancelHandCard();
	}

	private void OnSelectedHandCard(List<int> selectList)
	{
		if(selectList.Count > 0)
		{
			_handCardID = selectList[0];
			_isHasTarget = true;
            
             AddParamIntList(selectList);
		}
		else
		{
			_isHasTarget = false;
            
             AddParamIntList(new List<int>());
		}

		if(_onFinishPrepare != null)
		{
			_onFinishPrepare.Invoke();
		}
	}
    
    private void OnCancelHandCard()
    {
        if(_onCancelPrepare != null)
        {
            _onCancelPrepare.Invoke();
        }
    }

	public override void Action(OnFinishAction onFinishAction)
	{
        List<int> selectList;
        GetParamIntList(out selectList);
        _isHasTarget = (selectList.Count > 0);
        
		if(_isHasTarget)
		{
            _handCardID = selectList[0];
        
			LocalBattleZoneIndex localZone = LocalBattleZoneIndex.PlayerBase;
			switch(_abilityData.BattleCard.CurrentZone)
			{
				case BattleZoneIndex.BaseP1: 
				{
					// Find local target field.
					switch(_abilityData.OwnerPlayerIndex)
					{
						case PlayerIndex.One: localZone = LocalBattleZoneIndex.PlayerBase; break;
						case PlayerIndex.Two: localZone = LocalBattleZoneIndex.EnemyBase; break;
						default: 
						{
							Debug.LogError("SummonAtCostStructureAE/Action: targetfield is not basefield.");
							return; 
						}
					}
				}
				break;

				case BattleZoneIndex.Battlefield:
				{
					localZone = LocalBattleZoneIndex.Battlefield; 
				}
				break;

				case BattleZoneIndex.BaseP2: 
				{
					// Find local target field.
					switch(_abilityData.OwnerPlayerIndex)
					{
						case PlayerIndex.One: localZone = LocalBattleZoneIndex.EnemyBase; break;
						case PlayerIndex.Two: localZone = LocalBattleZoneIndex.PlayerBase; break;
						default: 
						{
							Debug.LogError("SummonAtCostStructureAE/Action: targetfield is not basefield.");
							return; 
						}
					}
				}
				break;
			}
				
			BattleManager.Instance.RequestSummonUniqueCard(
				  _handCardID
                , false
				, localZone
				, true
			);
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region GoToDeckShuffleAE Class
public class GoToDeckShuffleAE : AbilityEffect
{
	// AE0018
	#region GoToDeckShuffleAE Properties
    protected int _seed;
	#endregion

	#region GoToDeckShuffleAE Constructors
	public GoToDeckShuffleAE(string id)
		: base(id)
	{
		/*
		_description_EN = string.Format(_description_EN);
		_description_TH = string.Format(_description_TH);
		*/
	}

	public GoToDeckShuffleAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID)
	{
	}
	#endregion

	#region GoToDeckShuffleAE Methods
	public override bool IsHasTarget()
	{
		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				if(this._abilityData.IsSummon)
				{
					return true;
				}
			}
		}

		return false;
	}
		
	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
		if(IsHasTarget())
		{
            _seed = System.DateTime.Now.Millisecond;
            AddParamInt(_seed);
        
			if(onFinishPrepare != null)
			{
				onFinishPrepare.Invoke();
			}
		}
		else
		{
			if(onCancelPrepare != null)
			{
				onCancelPrepare.Invoke();
			}
		}
	}

	public override void Action(OnFinishAction onFinishAction)
	{
        GetParamInt(out _seed);
    
		if(IsHasTarget())
		{
			BattleManager.Instance.RequestShuffleDeckBySeed(
				  _abilityData.OwnerPlayerIndex
                , _seed
			);

			BattleManager.Instance.RequestDestroyBattleCard(
				  _abilityData.BattleCard.BattleCardID
				, CardZoneIndex.Deck
			);
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region GetBonusMoveAE Class
public class GetBonusMoveAE : BuffAE
{
	// AE0019
	#region GetBonusMoveAE Properties
	private int _move;
	#endregion

	#region GetBonusMoveAE Constructors
	public GetBonusMoveAE(string id, int move)
		:base(id)
	{
		_move = move;
		this._description_EN = "get " + this._move.ToString() + " move.";
	}

	public GetBonusMoveAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID, int.Parse(abilityEffectParams.Parameters[0]))
	{
	}
	#endregion

	#region GetBonusMoveAE Methods
	public override bool IsHasTarget()
	{
		return true;
	}
		
	public override bool GetTargetList(out List<BattleCardData> targetList)
	{
		targetList = new List<BattleCardData>();
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			if(playerIndex == _abilityData.OwnerPlayerIndex)
			{
				List<BattleCardData> target;
				BattleManager.Instance.GetTargetList(
					  playerIndex
					, out target
					, this.IsCanBeTarget
				);

				if(target != null && target.Count > 0)
				{
					targetList.AddRange(target);
				}
			}
		}

		if(targetList.Count > 0)
		{
			return true;
		}

		targetList = null;
		return false;
	}

	private bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown)
			{
				if(battleCard.BattleCardID == _abilityData.BattleCard.BattleCardID)
				{
					return true;
				}
			}
		}

		return false;
	}

	protected override void AddBuff(BattleCardData battleCard)
	{
		CardBuff buff = new CardBuff(
			this.AbilityEffectID
			, this._abilityData.BattleCard.BattleCardID // effector
			, battleCard.BattleCardID //effectee
			, 0 // shield
			, 0 // hp
			, 0 // attack
			, 0 // speed
			, 1 // move
			, 0 // attack range
			, new PassiveIndex()
			, new List<CardSubTypeData>()
			, new List<CardJobData>()
			, this.OnUnitUpdate
			, -1 // active turn
		);

		BattleManager.Instance.RequestAddBuffBattleCard(battleCard.BattleCardID, buff);
	}
	#endregion
}
#endregion

#region DestroyTargetMyUnitAE Class
public class DestroyTargetMyUnitAE : DestroyTargetAE
{
	// AE0020
	#region DestroyTargetMyUnitAE Properties
	#endregion

	#region DestroyTargetMyUnitAE Constructors
	public DestroyTargetMyUnitAE(string id, int num)
        : base(id, num, (int)CardZoneIndex.Graveyard)
	{
		AbilityEffectDBData abilityEffectDBData;
		bool isFound = AbilityEffectDB.GetData (this.AbilityEffectID, out abilityEffectDBData);
		if (isFound)
		{
			_description_EN = string.Format (abilityEffectDBData.Description_EN, this.Num);
			_description_TH = string.Format (abilityEffectDBData.Description_TH, this.Num);
		} 
		else
		{
			_description_EN = "";
			_description_TH = "";
		}

		_isCanCancel = true;
	}

	public DestroyTargetMyUnitAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID, int.Parse(abilityEffectParams.Parameters[0]))
	{
	}
	#endregion

	#region DestroyTargetMyUnitAE Methods
	protected override bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown && !battleCard.IsDead)
			{
                if(battleCard.OwnerPlayerIndex == _abilityData.OwnerPlayerIndex)
                {
				    if(battleCard.TypeData == CardTypeData.CardType.Unit)
				    {
					    return true;
				    }
                }
			}
		}

		return false;
	}
	#endregion
}
#endregion

#region DealDamageTargetRangeAE Class
public class DealDamageTargetRangeAE : AbilityEffect
{
	// AE0021
	#region DealDamageTargetRangeAE Properties
	private int _damage;
	private int _num;
	private int _rangeIndex;
	private bool _isPiercing;

	private List<int> _selectedTargetIDList = null;

	public int Damage
	{
		get { return _damage; }
	}
	public int Num
	{
		get { return _num; }
	}
	public int RangeIndex
	{
		get {return _rangeIndex; }
	}
	public bool IsPiercing
	{
		get { return _isPiercing; }
	}
	#endregion

	#region DealDamageTargetRangeAE Constructors
	public DealDamageTargetRangeAE(string id, int damage, int num, int rangeIndex, int isPiercing)
		: base(id)
	{
		this._num = num;
		this._damage = damage;
		this._rangeIndex = rangeIndex;
		this._isPiercing = ((isPiercing > 0) ? true : false);

		_description_EN = string.Format(_description_EN, this.Damage, this.Num, AbilityEffect.RangeIndexToText(this.RangeIndex), GetPiercingText(IsPiercing));
		_description_TH = string.Format(_description_TH, this.Damage, this.Num, AbilityEffect.RangeIndexToText(this.RangeIndex), GetPiercingText(IsPiercing));
	}

	public DealDamageTargetRangeAE(AbilityEffectParams abilityEffectParams)
		: this(
			  abilityEffectParams.ID
			, int.Parse(abilityEffectParams.Parameters[0])
			, int.Parse(abilityEffectParams.Parameters[1])
			, int.Parse(abilityEffectParams.Parameters[2])
			, int.Parse(abilityEffectParams.Parameters[3])
		)
	{
	}
	#endregion

	#region DealDamageTargetRangeAE Methods
	public override bool IsHasTarget()
	{
		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				if(this._abilityData.IsSummon)
				{
					foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
					{
						bool isHasTarget = BattleManager.Instance.CheckHasTargetList(
							  playerIndex
							, this.IsCanBeTarget
						);

						if(isHasTarget) return true;
					}
				}
			}
		}

		return false;
	}

	public override bool GetTargetList(out List<BattleCardData> targetList)
	{
		targetList = new List<BattleCardData>();
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			List<BattleCardData> target;
			BattleManager.Instance.GetTargetList(
				playerIndex
				, out target
				, this.IsCanBeTarget
			);

			if(target != null && target.Count > 0)
			{
				targetList.AddRange(target);
			}
		}

		if(targetList.Count > 0)
		{
			return true;
		}

		targetList = null;
		return false;
	}

	protected virtual bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null && _abilityData.IsSummon)
		{
			if(!battleCard.IsFaceDown && !battleCard.IsDead)
			{
                if (AbilityEffect.IsInRange(_abilityData.BattleCard, battleCard, this.RangeIndex))
                {
                    return true;
                }
			}
		}

		return false;
	}

	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
		/*
        if(_abilityData.IsSummon)
        {
            Debug.Log("DealDamageRangeAE: Prepare " + _abilityData.BattleCard.Name);
        }
        else
        {
            Debug.Log("DealDamageRangeAE: Prepare " + _abilityData.UniqueCard.PlayerCardData.Name);
        }
        */

        _onFinishPrepare = onFinishPrepare;
        _onCancelPrepare = onCancelPrepare;

		if(IsHasTarget())
		{
			List<BattleCardData> targetList;
			GetTargetList(out targetList);

			BattleManager.Instance.RequestDealDamage(
				_abilityData.OwnerPlayerIndex
				, targetList
				, this.Damage
				, this.Num
				, new BattleManager.OnFinishDealDamage(this.OnSelectedTarget)
				, new BattleManager.OnCancelDealDamage(this.OnCancelTarget)
                , (BattleManager.Instance.IsHaveTimeout && !this.IsOwnerTurn)
                , !this.IsSummon
			);
            
            return;
		}
		
        OnCancelTarget();
	}

	private void OnSelectedTarget(List<int> selectedTargetIDList)
	{
		//Debug.Log("DealDamageRangeAE: OnSelectedTarget " + selectedTargetIDList.Count.ToString());

		_selectedTargetIDList = selectedTargetIDList;
        
        AddParamIntList(_selectedTargetIDList);

		if(_onFinishPrepare != null)
		{
			_onFinishPrepare.Invoke();
		}
	}
    
    private void OnCancelTarget()
    {
        if(_onCancelPrepare != null)
        {
            _onCancelPrepare.Invoke();
        }
    }

	public override void Action(OnFinishAction onFinishAction)
	{
        #if UNITY_EDITOR
        if (_abilityData.IsSummon)
		{
			Debug.Log("DealDamageRangeAE: Action " + _abilityData.BattleCard.Name_EN);
		}
		else
		{
			Debug.Log("DealDamageRangeAE: Action " + _abilityData.UniqueCard.PlayerCardData.Name_EN);
		}
        #endif
        
        GetParamIntList(out _selectedTargetIDList);

        if(_selectedTargetIDList != null && _selectedTargetIDList.Count > 0)
		{
			foreach(int battleCardID in _selectedTargetIDList)
			{
				BattleManager.Instance.RequestDealDamageBattleCard(battleCardID, this.Damage, this.IsPiercing);
			}
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region DealDamageTargetRangeAirUnitAE Class
public class DealDamageTargetRangeAirUnitAE : DealDamageTargetRangeAE
{
	// AE0022
	#region DealDamageTargetRangeAirUnitAE Properties
	#endregion

	#region DealDamageTargetRangeAirUnitAE Constructors
	public DealDamageTargetRangeAirUnitAE(string id, int damage, int num, int range, int isPiercing)
		:base(id, damage, num, range, isPiercing)
	{
		AbilityEffectDBData effectDBData;
		bool isFoundEffect = AbilityEffectDB.GetData(this.AbilityEffectID, out effectDBData);
		if (isFoundEffect)
		{
			_description_EN = string.Format(effectDBData.Description_EN, this.Damage, this.Num, this.RangeIndex, GetPiercingText(IsPiercing));
			_description_TH = string.Format(effectDBData.Description_TH, this.Damage, this.Num, this.RangeIndex, GetPiercingText(IsPiercing));
		} 
		else
		{
			_description_EN = "";
			_description_TH = "";
		}
	}

	public DealDamageTargetRangeAirUnitAE(AbilityEffectParams abilityEffectParams)
		: this(
			abilityEffectParams.ID
			, int.Parse(abilityEffectParams.Parameters[0])
			, int.Parse(abilityEffectParams.Parameters[1])
			, int.Parse(abilityEffectParams.Parameters[2])
			, int.Parse(abilityEffectParams.Parameters[3])
		)
	{
	}
	#endregion

	#region DealDamageTargetRangeAirUnitAE Methods
	protected override bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(base.IsCanBeTarget(battleCard))
		{
			if(battleCard.TypeData == CardTypeData.CardType.Unit)
			{
				if(battleCard.CurrentPassiveIndex == PassiveIndex.PassiveType.AirUnit)
				{
					return true;
				}
			}
		}
			
		return false;
	}
	#endregion
}
#endregion

#region DealDamageTargetRangeGroundUnitAE Class
public class DealDamageTargetRangeGroundUnitAE : DealDamageTargetRangeAE
{
	// AE0023
	#region DealDamageTargetRangeGroundUnitAE Properties
	#endregion

	#region DealDamageTargetRangeGroundUnitAE Constructors
	public DealDamageTargetRangeGroundUnitAE(string id, int damage, int num, int range, int isPiercing)
		:base(id, damage, num, range, isPiercing)
	{
		AbilityEffectDBData effectDBData;
		bool isFoundEffect = AbilityEffectDB.GetData(this.AbilityEffectID, out effectDBData);
		if (isFoundEffect)
		{
			_description_EN = string.Format(effectDBData.Description_EN, this.Damage, this.Num, AbilityEffect.RangeIndexToText(this.RangeIndex), GetPiercingText(IsPiercing));
			_description_TH = string.Format(effectDBData.Description_TH, this.Damage, this.Num, AbilityEffect.RangeIndexToText(this.RangeIndex), GetPiercingText(IsPiercing));
		} 
		else
		{
			_description_EN = "";
			_description_TH = "";
		}
	}

	public DealDamageTargetRangeGroundUnitAE(AbilityEffectParams abilityEffectParams)
		: this(
			abilityEffectParams.ID
			, int.Parse(abilityEffectParams.Parameters[0])
			, int.Parse(abilityEffectParams.Parameters[1])
			, int.Parse(abilityEffectParams.Parameters[2])
			, int.Parse(abilityEffectParams.Parameters[3])
		)
	{
	}
	#endregion

	#region DealDamageTargetRangeGroundUnitAE Methods
	protected override bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(base.IsCanBeTarget(battleCard))
		{
			if(battleCard.TypeData == CardTypeData.CardType.Unit)
			{
				if(battleCard.CurrentPassiveIndex != PassiveIndex.PassiveType.AirUnit)
				{
					return true;
				}
			}
		}

		return false;
	}
	#endregion
}
#endregion

#region DealDamageAllRangeGroundUnitAE Class
public class DealDamageAllRangeGroundUnitAE : DealDamageAllRangeAE
{
	// AE0024
	#region DealDamageAllRangeGroundUnitAE Properties
	#endregion

	#region DealDamageAllRangeGroundUnitAE Constructors
	public DealDamageAllRangeGroundUnitAE(string id, int damage, int range, int isPiercing)
		:base(id, damage, range, isPiercing)
	{
		AbilityEffectDBData effectDBData;
		bool isFoundEffect = AbilityEffectDB.GetData(this.AbilityEffectID, out effectDBData);
		if (isFoundEffect)
		{
			_description_EN = string.Format(effectDBData.Description_EN, this.Damage, AbilityEffect.RangeIndexToText(this.RangeIndex), GetPiercingText(IsPiercing));
			_description_TH = string.Format(effectDBData.Description_TH, this.Damage, AbilityEffect.RangeIndexToText(this.RangeIndex), GetPiercingText(IsPiercing));
		} 
		else
		{
			_description_EN = "";
			_description_TH = "";
		}
	}

	public DealDamageAllRangeGroundUnitAE(AbilityEffectParams abilityEffectParams)
		: this(
			abilityEffectParams.ID
			, int.Parse(abilityEffectParams.Parameters[0])
			, int.Parse(abilityEffectParams.Parameters[1])
			, int.Parse(abilityEffectParams.Parameters[2])
		)
	{
	}
	#endregion

	#region DealDamageRangeGroundUnitAE Methods
	protected override bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(base.IsCanBeTarget(battleCard))
		{
			if(battleCard.TypeData == CardTypeData.CardType.Unit)
			{
				if(battleCard.CurrentPassiveIndex != PassiveIndex.PassiveType.AirUnit)
				{
					return true;
				}
			}
		}

		return false;
	}
	#endregion
}
#endregion

#region DealDamageTargetEnemyAE Class
public class DealDamageTargetEnemyAE : DealDamageTargetAE
{
	// AE0025
	#region DealDamageTargetEnemyAE Properties
	#endregion

	#region DealDamageTargetEnemyAE Constructors
	public DealDamageTargetEnemyAE(string id, int damage, int num, int isPiercing)
		: base(id, damage, num, isPiercing)
	{		AbilityEffectDBData effectDBData;
		bool isFoundEffect = AbilityEffectDB.GetData(this.AbilityEffectID, out effectDBData);
		if (isFoundEffect)
		{
			_description_EN = string.Format(effectDBData.Description_EN, this.Damage, this.Num, GetPiercingText(IsPiercing));
			_description_TH = string.Format(effectDBData.Description_TH, this.Damage, this.Num, GetPiercingText(IsPiercing));
		} 
		else
		{
			_description_EN = "";
			_description_TH = "";
		}
	}

	public DealDamageTargetEnemyAE(AbilityEffectParams abilityEffectParams)
		: this(
			abilityEffectParams.ID
			, int.Parse(abilityEffectParams.Parameters[0])
			, int.Parse(abilityEffectParams.Parameters[1])
			, int.Parse(abilityEffectParams.Parameters[2])
		)
	{
	}
	#endregion

	#region DealDamageTargetEnemyAE Methods
	protected override bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown && !battleCard.IsDead)
			{
				if(battleCard.OwnerPlayerIndex != this._abilityData.OwnerPlayerIndex)
				{
					return true;
				}
			}
		}

		return false;
	}
	#endregion
}
#endregion

#region DealDamageByMyNumWeaponTargetUnitAE Class
public class DealDamageByMyNumWeaponTargetUnitAE : DealDamageTargetAE
{
	// AE0026
	#region DealDamageByMyNumWeaponTargetUnitAE Properties
	public int BaseDamage { get { return this.Damage; } }
	#endregion

	#region DealDamageByMyNumWeaponTargetUnitAE Constructors
	public DealDamageByMyNumWeaponTargetUnitAE(string id, int baseDamage, int num, int isPiercing)
		: base(id, baseDamage, num, isPiercing)
	{
		AbilityEffectDBData effectDBData;
		bool isFoundEffect = AbilityEffectDB.GetData(this.AbilityEffectID, out effectDBData);
		if (isFoundEffect)
		{
			_description_EN = string.Format(effectDBData.Description_EN, this.BaseDamage, this.Num, GetPiercingText(IsPiercing));
			_description_TH = string.Format(effectDBData.Description_TH, this.BaseDamage, this.Num, GetPiercingText(IsPiercing));
		} 
		else
		{
			_description_EN = "";
			_description_TH = "";
		}
	}

	public DealDamageByMyNumWeaponTargetUnitAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID
			, int.Parse(abilityEffectParams.Parameters[0])
			, int.Parse(abilityEffectParams.Parameters[1])
			, int.Parse(abilityEffectParams.Parameters[2])
		)
	{
	}
	#endregion

	#region DealDamageByMyNumWeaponTargetUnitAE Methods
	protected override bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown && !battleCard.IsDead)
			{
				if(battleCard.TypeData == CardTypeData.CardType.Unit)
				{
					return true;
				}
			}
		}

		return false;
	}

	public override void Action(OnFinishAction onFinishAction)
	{
		/*
		if (_abilityData.IsSummon)
		{
			Debug.Log("DealDamageByNumWeaponTargetUnitAE: Action " + _abilityData.BattleCard.Name);
		}
		else
		{
			Debug.Log("DealDamageByNumWeaponTargetUnitAE: Action " + _abilityData.UniqueCard.PlayerCardData.Name);
		}
		*/
        
        GetParamIntList(out _selectedTargetIDList);

		if(_selectedTargetIDList != null && _selectedTargetIDList.Count > 0)
		{
			int bonusDamage = 0;
			List<BattleCardData> weaponTypeList;
			BattleManager.Instance.GetTargetList(
				  _abilityData.OwnerPlayerIndex
				, out weaponTypeList
				, this.IsWeaponType
			);

			if(weaponTypeList != null && weaponTypeList.Count > 0)
			{
				bonusDamage = weaponTypeList.Count;
			}

			foreach(int battleCardID in _selectedTargetIDList)
			{
				BattleManager.Instance.RequestDealDamageBattleCard(
					  battleCardID
					, this.Damage + bonusDamage
					, this.IsPiercing
				);
			}
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}

	private bool IsWeaponType(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown && !battleCard.IsDead)
			{
				if(battleCard.OwnerPlayerIndex == _abilityData.OwnerPlayerIndex)
				{
					if(battleCard.TypeData == CardTypeData.CardType.Unit)
					{
                        if (battleCard.IsCardSubType(CardSubTypeData.CardSubType.Weapon))
                        {
                            return true;
                        }
					}
				}
			}
		}

		return false;
	}
	#endregion
}
#endregion

#region DealDamageTargetUnitAE Class
public class DealDamageTargetUnitAE : DealDamageTargetAE
{
	// AE0072
	#region DealDamageTargetUnitAE Properties
	#endregion

	#region DealDamageTargetUnitAE Constructors
	public DealDamageTargetUnitAE(string id, int damage, int num, int isPiercing)
		: base(id, damage, num, isPiercing)
	{
		AbilityEffectDBData effectDBData;
		bool isFoundEffect = AbilityEffectDB.GetData(this.AbilityEffectID, out effectDBData);
		if (isFoundEffect)
		{
			_description_EN = string.Format(effectDBData.Description_EN, this.Damage, this.Num, GetPiercingText(IsPiercing));
			_description_TH = string.Format(effectDBData.Description_TH, this.Damage, this.Num, GetPiercingText(IsPiercing));
		} 
		else
		{
			_description_EN = "";
			_description_TH = "";
		}
	}

	public DealDamageTargetUnitAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID
			, int.Parse(abilityEffectParams.Parameters[0])
			, int.Parse(abilityEffectParams.Parameters[1])
			, int.Parse(abilityEffectParams.Parameters[2])
		)
	{
	}
	#endregion

	#region DealDamageTargetUnitAE Methods
	protected override bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown && !battleCard.IsDead)
			{
				if(battleCard.TypeData == CardTypeData.CardType.Unit)
				{
					return true;
				}
			}
		}

		return false;
	}

	public override void Action(OnFinishAction onFinishAction)
	{
        GetParamIntList(out _selectedTargetIDList);
    
		if(_selectedTargetIDList != null && _selectedTargetIDList.Count > 0)
		{
			foreach(int battleCardID in _selectedTargetIDList)
			{
				BattleManager.Instance.RequestDealDamageBattleCard(
					  battleCardID
					, this.Damage
					, this.IsPiercing
				);
			}
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region SearchZoneToZoneShuffleAE Class
public class SearchZoneToZoneShuffleAE : AbilityEffect
{
	// AE0027
    #region SearchZoneToZoneShuffleAE Properties
    protected CardZoneIndex _sourceZone;
	protected int _num;
	protected CardZoneIndex _targetZone;
	protected bool _isRequireNum;

	protected bool _isCanCancel = false;
	protected List<int> _selectedTargetIDList = null;

	public int Num { get { return _num; } }
	public CardZoneIndex SourceZone { get { return _sourceZone; } }
	public CardZoneIndex TargetZone { get { return _targetZone; } }
	public bool IsRequireNum { get { return _isRequireNum; } }
	#endregion

    #region SearchZoneToZoneShuffleAE Constructors
	public SearchZoneToZoneShuffleAE(string id, int sourceZoneIndex, int num, int targetZoneIndex)
		: base(id)
	{
		this._sourceZone = (CardZoneIndex)sourceZoneIndex;
		this._num = num;
		this._targetZone = (CardZoneIndex)targetZoneIndex;
		this._isRequireNum = false;

		_description_EN = string.Format(_description_EN, LocalizationManager.CardZoneToText(SourceZone), this.Num, LocalizationManager.CardZoneToText(TargetZone));
		_description_TH = string.Format(_description_TH, LocalizationManager.CardZoneToText(SourceZone), this.Num, LocalizationManager.CardZoneToText(TargetZone));
	}

	public SearchZoneToZoneShuffleAE(AbilityEffectParams abilityEffectParams)
		: this(
			  abilityEffectParams.ID
			, int.Parse(abilityEffectParams.Parameters[0])
			, int.Parse(abilityEffectParams.Parameters[1])
			, int.Parse(abilityEffectParams.Parameters[2])
		)
	{
	}
	#endregion

    #region SearchZoneToZoneShuffleAE Methods
    public override bool IsHasTarget()
	{
		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
                List<UniqueCardData> targetList;
                GetTargetList (out targetList);
				if (IsRequireNum)
				{
					if (targetList != null && targetList.Count >= this.Num)
					{
						return true;
					}
				} 
				else
				{
                    if (targetList != null && targetList.Count > 0) 
                    {
					    return true;
                    }
				}
			}
		}

		return false;
	}

	public override bool GetTargetList(out List<UniqueCardData> targetList)
	{
		targetList = new List<UniqueCardData>();

		List<UniqueCardData> target;
		bool isFound = BattleManager.Instance.GetTargetList(
			  _abilityData.OwnerPlayerIndex
			, this.SourceZone
			, out target
			, this.IsCanBeTarget
		);

		if(isFound)
		{
			targetList.AddRange(target);
			return true;
		}

		targetList = new List<UniqueCardData>();
		return false;
	}

	protected virtual bool IsCanBeTarget(UniqueCardData uniqueCard)
	{
		if(uniqueCard != null)
		{
			return true;
		}

		return false;
	}

	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
        _onFinishPrepare = onFinishPrepare;
        _onCancelPrepare = onCancelPrepare;
    
		if(IsHasTarget())
		{
			List<UniqueCardData> targetList;
			GetTargetList(out targetList);

			string headerText = "";
			switch (LocalizationManager.CurrentLanguage)
			{
				case LocalizationManager.Language.Thai:
				{
					headerText = this._description_TH;
				}
				break;

				case LocalizationManager.Language.English:
				default:
				{
					headerText = this._description_EN;
				}
				break;
			}
            
            if(targetList != null && targetList.Count > 0)
            {
    			BattleManager.Instance.StartSelectUniqueCard(
    				  headerText
    				,  _abilityData.OwnerPlayerIndex
    				, targetList
    				, this.Num
    				, new BattleManager.OnFinishSelectUniqueCardCallback(this.OnSelectedTarget)
    				, new BattleManager.OnCancelSelectUniqueCardCallback(this.OnCancelTarget)
    				, null
                    , (BattleManager.Instance.IsHaveTimeout && !this.IsOwnerTurn)
    				, _isCanCancel
    			);
                
                return;
            }
		}
        
		OnCancelTarget();
	}

	protected void OnSelectedTarget(List<int> selectedTargetIDList)
	{
		_selectedTargetIDList = selectedTargetIDList;
        
        AddParamIntList(_selectedTargetIDList);

		if(_onFinishPrepare != null)
		{
			_onFinishPrepare.Invoke();
		}
	}
    
    protected void OnCancelTarget()
    {
        if(_onCancelPrepare != null)
        {
            _onCancelPrepare.Invoke();
        }
    }

	public override void Action(OnFinishAction onFinishAction)
	{
		/*
		if (_abilityData.IsSummon)
		{
			Debug.Log("SearchDeckPushToHandSuffleAE: Action " + _abilityData.BattleCard.Name);
		}
		else
		{
			Debug.Log("SearchDeckPushToHandSuffleAE: Action " + _abilityData.UniqueCard.PlayerCardData.Name);
		}
		*/
        
        GetParamIntList(out _selectedTargetIDList);

		if(_selectedTargetIDList != null && _selectedTargetIDList.Count > 0)
		{
			foreach(int uniqueID in _selectedTargetIDList)
			{
				BattleManager.Instance.RequestMoveUniqueCard(uniqueID, TargetZone);
			}

			if(this.SourceZone == CardZoneIndex.Deck)
			{
				BattleManager.Instance.ActionShuffleDeck(_abilityData.OwnerPlayerIndex);
			}
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region SearchZoneWeaponTypeToZoneShuffleAE Class
public class SearchZoneWeaponTypeToZoneShuffleAE : SearchZoneToZoneShuffleAE
{
	// AE0028
    #region SearchZoneWeaponTypeToZoneShuffleAE Properties
    #endregion

    #region SearchZoneWeaponTypeToZoneShuffleAE Constructors
    public SearchZoneWeaponTypeToZoneShuffleAE(string id, int sourceZoneIndex, int num, int targetZoneIndex)
		: base(id, sourceZoneIndex, num, targetZoneIndex)
	{
		AbilityEffectDBData effectDBData;
		bool isFoundEffect = AbilityEffectDB.GetData(this.AbilityEffectID, out effectDBData);
		if (isFoundEffect)
		{
			_description_EN = string.Format(effectDBData.Description_EN, LocalizationManager.CardZoneToText(this.SourceZone), this.Num, LocalizationManager.CardZoneToText(this.TargetZone));
			_description_TH = string.Format(effectDBData.Description_TH, LocalizationManager.CardZoneToText(this.SourceZone), this.Num, LocalizationManager.CardZoneToText(this.TargetZone));
		} 
		else
		{
			_description_EN = "";
			_description_TH = "";
		}
	}

	public SearchZoneWeaponTypeToZoneShuffleAE(AbilityEffectParams abilityEffectParams)
		: this(
			abilityEffectParams.ID
			, int.Parse(abilityEffectParams.Parameters[0])
			, int.Parse(abilityEffectParams.Parameters[1])
			, int.Parse(abilityEffectParams.Parameters[2])
		)
	{
	}
	#endregion

    #region SearchZoneWeaponTypeToZoneShuffleAE Methods
    protected override bool IsCanBeTarget(UniqueCardData uniqueCard)
	{
		if(uniqueCard != null)
		{
			if(uniqueCard.PlayerCardData.TypeData == CardTypeData.CardType.Unit)
			{
                if (uniqueCard.IsSubType(CardSubTypeData.CardSubType.Weapon))
                {
                    return true;
                }
			}
		}

		return false;
	}
	#endregion
}
#endregion

#region SearchZoneStructureToZoneShuffleAE Class
public class SearchZoneStructureToZoneShuffleAE : SearchZoneToZoneShuffleAE
{
	// AE0029
    #region SearchZoneStructureToZoneShuffleAE Properties
    #endregion

    #region SearchZoneStructureToZoneShuffleAE Constructors
    public SearchZoneStructureToZoneShuffleAE(string id, int sourceZoneIndex, int num, int targetZoneIndex)
        : base(id, sourceZoneIndex, num, targetZoneIndex)
    {
		AbilityEffectDBData effectDBData;
		bool isFoundEffect = AbilityEffectDB.GetData(this.AbilityEffectID, out effectDBData);
		if (isFoundEffect)
		{
			_description_EN = string.Format(effectDBData.Description_EN, LocalizationManager.CardZoneToText(this.SourceZone), this.Num, LocalizationManager.CardZoneToText(this.TargetZone));
			_description_TH = string.Format(effectDBData.Description_TH, LocalizationManager.CardZoneToText(this.SourceZone), this.Num, LocalizationManager.CardZoneToText(this.TargetZone));
		} 
		else
		{
			_description_EN = "";
			_description_TH = "";
		}
    }

    public SearchZoneStructureToZoneShuffleAE(AbilityEffectParams abilityEffectParams)
        : this(
            abilityEffectParams.ID
            , int.Parse(abilityEffectParams.Parameters[0])
            , int.Parse(abilityEffectParams.Parameters[1])
            , int.Parse(abilityEffectParams.Parameters[2])
        )
    {
    }
    #endregion

    #region SearchZoneWeaponTypeToZoneShuffleAE Methods
    protected override bool IsCanBeTarget(UniqueCardData uniqueCard)
    {
        if (uniqueCard != null)
        {
            if (uniqueCard.PlayerCardData.TypeData == CardTypeData.CardType.Structure || uniqueCard.PlayerCardData.TypeData == CardTypeData.CardType.Base)
            {
                return true;
            }
        }

        return false;
    }
    #endregion
}
#endregion

#region SearchZoneHarvesterToZoneShuffleAE Class
public class SearchZoneHarvesterToZoneShuffleAE : SearchZoneToZoneShuffleAE
{
	// AE0030
    #region SearchZoneHarvesterToZoneShuffleAE Properties
    #endregion

    #region SearchZoneHarvesterToZoneShuffleAE Constructors
    public SearchZoneHarvesterToZoneShuffleAE(string id, int sourceZoneIndex, int num, int targetZoneIndex)
        : base(id, sourceZoneIndex, num, targetZoneIndex)
    {
		AbilityEffectDBData effectDBData;
		bool isFoundEffect = AbilityEffectDB.GetData(this.AbilityEffectID, out effectDBData);
		if (isFoundEffect)
		{
			_description_EN = string.Format(effectDBData.Description_EN, LocalizationManager.CardZoneToText(this.SourceZone), this.Num, LocalizationManager.CardZoneToText(this.TargetZone));
			_description_TH = string.Format(effectDBData.Description_TH, LocalizationManager.CardZoneToText(this.SourceZone), this.Num, LocalizationManager.CardZoneToText(this.TargetZone));
		} 
		else
		{
			_description_EN = "";
			_description_TH = "";
		}
    }

    public SearchZoneHarvesterToZoneShuffleAE(AbilityEffectParams abilityEffectParams)
        : this(
            abilityEffectParams.ID
            , int.Parse(abilityEffectParams.Parameters[0])
            , int.Parse(abilityEffectParams.Parameters[1])
            , int.Parse(abilityEffectParams.Parameters[2])
        )
    {
    }
    #endregion

    #region SearchZoneWeaponTypeToZoneShuffleAE Methods
    protected override bool IsCanBeTarget(UniqueCardData uniqueCard)
    {
        if (uniqueCard != null)
        {
            if ((uniqueCard.PlayerCardData.Name_EN).ToLower().Contains("harvester"))
            {
                return true;
            }
        }

        return false;
    }
    #endregion
}
#endregion

#region SearchZoneToFieldShuffleAE Class
public class SearchZoneToFieldShuffleAE : AbilityEffect
{
	// AE0034
    #region SearchZoneToFieldShuffleAE Properties
    protected CardZoneIndex _sourceZone;
	protected int _num;
	protected LocalBattleZoneIndex _targetField;

	protected List<int> _selectedTargetIDList = null;

	public int Num { get { return _num; } }
	public CardZoneIndex SourceZone { get { return _sourceZone; } }
	public LocalBattleZoneIndex TargetField { get { return _targetField; } }
	#endregion

    #region SearchZoneToFieldShuffleAE Constructors
    public SearchZoneToFieldShuffleAE(string id, int sourceZoneIndex, int num, int targetField)
		: base(id)
	{
		this._sourceZone = (CardZoneIndex)sourceZoneIndex;
		this._num = num;
		this._targetField = (LocalBattleZoneIndex)targetField;

		_description_EN = string.Format(_description_EN, LocalizationManager.CardZoneToText(this.SourceZone), this.Num, LocalizationManager.LocalBattleZoneToText(this.TargetField));
		_description_TH = string.Format(_description_TH, LocalizationManager.CardZoneToText(this.SourceZone), this.Num, LocalizationManager.LocalBattleZoneToText(this.TargetField));
	}

    public SearchZoneToFieldShuffleAE(AbilityEffectParams abilityEffectParams)
		: this(
			  abilityEffectParams.ID
			, int.Parse(abilityEffectParams.Parameters[0])
			, int.Parse(abilityEffectParams.Parameters[1])
			, int.Parse(abilityEffectParams.Parameters[2])
		)
	{
	}
	#endregion

    #region SearchZoneToFieldShuffleAE Methods
    public override bool IsHasTarget()
	{
		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
            {
                List<UniqueCardData> targetList;
                GetTargetList (out targetList);

                if (targetList != null && targetList.Count > 0) 
                {
                    return true;
                }
            }
		}

		return false;
	}

	public override bool GetTargetList(out List<UniqueCardData> targetList)
	{
		targetList = new List<UniqueCardData>();

		List<UniqueCardData> target;
		bool isFound = BattleManager.Instance.GetTargetList(
			  _abilityData.OwnerPlayerIndex
			, this.SourceZone
			, out target
			, this.IsCanBeTarget
		);

		if(isFound)
		{
			targetList.AddRange(target);
			return true;
		}

		targetList = new List<UniqueCardData>();
		return false;
	}

	protected virtual bool IsCanBeTarget(UniqueCardData uniqueCard)
	{
		if(uniqueCard != null)
		{
			return true;
		}

		return false;
	}

	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
        _onFinishPrepare = onFinishPrepare;
        _onCancelPrepare = onCancelPrepare;
    
		if(IsHasTarget())
		{
			List<UniqueCardData> targetList;
			GetTargetList(out targetList);
            
			string headerText = "";
			switch (LocalizationManager.CurrentLanguage)
			{
				case LocalizationManager.Language.Thai:
				{
					headerText = this._description_TH;
				}
				break;

				case LocalizationManager.Language.English:
				default:
				{
					headerText = this._description_EN;
				}
				break;
			}

			BattleManager.Instance.StartSelectUniqueCard(
				  headerText
				,  _abilityData.OwnerPlayerIndex
				, targetList
				, this.Num
				, new BattleManager.OnFinishSelectUniqueCardCallback(this.OnSelectedTarget)
				, new BattleManager.OnCancelSelectUniqueCardCallback(onCancelPrepare)
                , null
                , (BattleManager.Instance.IsHaveTimeout && !this.IsOwnerTurn)
                , false 
			);
            return;
		}
        
		OnCancelTarget();
	}

	protected void OnSelectedTarget(List<int> selectedTargetIDList)
	{
		_selectedTargetIDList = selectedTargetIDList;
        
        AddParamIntList(_selectedTargetIDList);

		if(_onFinishPrepare != null)
		{
			_onFinishPrepare.Invoke();
		}
	}
    
    protected void OnCancelTarget()
    {
        if(_onCancelPrepare != null)
        {
            _onCancelPrepare.Invoke();
        }
    }

	public override void Action(OnFinishAction onFinishAction)
	{
		/*
		if (_abilityData.IsSummon)
		{
			Debug.Log("SearchZoneToFieldShuffleAE: Action " + _abilityData.BattleCard.Name);
		}
		else
		{
			Debug.Log("SearchZoneToFieldShuffleAE: Action " + _abilityData.UniqueCard.PlayerCardData.Name);
		}
		*/
        
        GetParamIntList(out _selectedTargetIDList);

		if(_selectedTargetIDList != null && _selectedTargetIDList.Count > 0)
		{
			foreach(int uniqueID in _selectedTargetIDList)
			{
				BattleManager.Instance.RequestSummonUniqueCard(uniqueID, false, TargetField, true, false);
			}

			if(this.SourceZone == CardZoneIndex.Deck)
			{
				BattleManager.Instance.ActionShuffleDeck(_abilityData.OwnerPlayerIndex);
			}
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region SearchZonePlant80ToFieldShuffleAE  Class
public class SearchZonePlant80ToFieldShuffleAE : SearchZoneToFieldShuffleAE
{
	// AE0031
    #region SearchZonePlant80ToFieldShuffleAE Properties
    #endregion

    #region SearchZonePlant80ToFieldShuffleAE Constructors
    public SearchZonePlant80ToFieldShuffleAE(string id, int sourceZoneIndex, int num, int targetZoneIndex)
        : base(id, sourceZoneIndex, num, targetZoneIndex)
    {
		AbilityEffectDBData effectDBData;
		bool isFoundEffect = AbilityEffectDB.GetData(this.AbilityEffectID, out effectDBData);
		if (isFoundEffect)
		{
			_description_EN = string.Format(effectDBData.Description_EN, LocalizationManager.CardZoneToText(this.SourceZone), this.Num, LocalizationManager.LocalBattleZoneToText(this.TargetField));
			_description_TH = string.Format(effectDBData.Description_TH, LocalizationManager.CardZoneToText(this.SourceZone), this.Num, LocalizationManager.LocalBattleZoneToText(this.TargetField));
		} 
		else
		{
			_description_EN = "";
			_description_TH = "";
		}
    }

    public SearchZonePlant80ToFieldShuffleAE(AbilityEffectParams abilityEffectParams)
        : this(
            abilityEffectParams.ID
            , int.Parse(abilityEffectParams.Parameters[0])
            , int.Parse(abilityEffectParams.Parameters[1])
            , int.Parse(abilityEffectParams.Parameters[2])
        )
    {
    }
    #endregion

    #region SearchZonePlant80ToFieldShuffleAE Methods
    protected override bool IsCanBeTarget(UniqueCardData uniqueCard)
    {
        if (uniqueCard != null)
        {
            if (uniqueCard.PlayerCardData.TypeData == CardTypeData.CardType.Unit)
            {
                if (uniqueCard.PlayerCardData.Cost <= 80)
                {
                    if (uniqueCard.IsSubType(CardSubTypeData.CardSubType.Plant))
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }
    #endregion
}
#endregion

#region SearchZoneUnitToZoneShuffleAE Class
public class SearchZoneUnitToZoneShuffleAE : SearchZoneToZoneShuffleAE
{
	// AE0032
    #region SearchZoneUnitToZoneShuffleAE Properties
    #endregion

    #region SearchZoneUnitToZoneShuffleAE Constructors
    public SearchZoneUnitToZoneShuffleAE(string id, int sourceZoneIndex, int num, int targetZoneIndex)
        : base(id, sourceZoneIndex, num, targetZoneIndex)
    {
		AbilityEffectDBData effectDBData;
		bool isFoundEffect = AbilityEffectDB.GetData(this.AbilityEffectID, out effectDBData);
		if (isFoundEffect)
		{
			_description_EN = string.Format(effectDBData.Description_EN, LocalizationManager.CardZoneToText(this.SourceZone), this.Num, LocalizationManager.CardZoneToText(this.TargetZone));
			_description_TH = string.Format(effectDBData.Description_TH, LocalizationManager.CardZoneToText(this.SourceZone), this.Num, LocalizationManager.CardZoneToText(this.TargetZone));
		} 
		else
		{
			_description_EN = "";
			_description_TH = "";
		}
    }

    public SearchZoneUnitToZoneShuffleAE(AbilityEffectParams abilityEffectParams)
        : this(
            abilityEffectParams.ID
            , int.Parse(abilityEffectParams.Parameters[0])
            , int.Parse(abilityEffectParams.Parameters[1])
            , int.Parse(abilityEffectParams.Parameters[2])
        )
    {
    }
    #endregion

    #region SearchZoneUnitToZoneShuffleAE Methods
    protected override bool IsCanBeTarget(UniqueCardData uniqueCard)
    {
        if (uniqueCard != null)
        {
            if (uniqueCard.PlayerCardData.TypeData == CardTypeData.CardType.Unit)
            {
                return true;
            }
        }

        return false;
    }
    #endregion
}
#endregion

#region SearchZoneBerondoToZoneShuffleAE  Class
public class SearchZoneBerondoToZoneShuffleAE  : SearchZoneToZoneShuffleAE
{
	// AE0033
    #region SearchZoneBerondoToZoneShuffleAE Properties
    #endregion

    #region SearchZoneBerondoToZoneShuffleAE Constructors
    public SearchZoneBerondoToZoneShuffleAE(string id, int sourceZoneIndex, int num, int targetZoneIndex)
        : base(id, sourceZoneIndex, num, targetZoneIndex)
    {
		AbilityEffectDBData effectDBData;
		bool isFoundEffect = AbilityEffectDB.GetData(this.AbilityEffectID, out effectDBData);
		if (isFoundEffect)
		{
			_description_EN = string.Format(effectDBData.Description_EN, LocalizationManager.CardZoneToText(this.SourceZone), this.Num, LocalizationManager.CardZoneToText(this.TargetZone));
			_description_TH = string.Format(effectDBData.Description_TH, LocalizationManager.CardZoneToText(this.SourceZone), this.Num, LocalizationManager.CardZoneToText(this.TargetZone));
		} 
		else
		{
			_description_EN = "";
			_description_TH = "";
		}
    }

    public SearchZoneBerondoToZoneShuffleAE(AbilityEffectParams abilityEffectParams)
        : this(
            abilityEffectParams.ID
            , int.Parse(abilityEffectParams.Parameters[0])
            , int.Parse(abilityEffectParams.Parameters[1])
            , int.Parse(abilityEffectParams.Parameters[2])
        )
    {
    }
    #endregion

    #region SearchZoneBerondoToZoneShuffleAE Methods
    protected override bool IsCanBeTarget(UniqueCardData uniqueCard)
    {
        if (uniqueCard != null)
        {
            if (uniqueCard.PlayerCardData.ClanData == CardClanData.CardClanType.Berondo)
            {
                return true;
            }
        }

        return false;
    }
    #endregion
}
#endregion

#region SearchZoneUnitToFieldAE  Class
public class SearchZoneUnitToFieldAE : SearchZoneToFieldShuffleAE
{
	// AE0035
	#region SearchZoneUnitToFieldAE Properties
	#endregion

	#region SearchZoneUnitToFieldAE Constructors
	public SearchZoneUnitToFieldAE(string id, int sourceZoneIndex, int num, int targetZoneIndex)
		: base(id, sourceZoneIndex, num, targetZoneIndex)
	{
		AbilityEffectDBData effectDBData;
		bool isFoundEffect = AbilityEffectDB.GetData(this.AbilityEffectID, out effectDBData);
		if (isFoundEffect)
		{
			_description_EN = string.Format(effectDBData.Description_EN, LocalizationManager.CardZoneToText(this.SourceZone), this.Num, LocalizationManager.LocalBattleZoneToText(this.TargetField));
			_description_TH = string.Format(effectDBData.Description_TH, LocalizationManager.CardZoneToText(this.SourceZone), this.Num, LocalizationManager.LocalBattleZoneToText(this.TargetField));
		} 
		else
		{
			_description_EN = "";
			_description_TH = "";
		}
	}

	public SearchZoneUnitToFieldAE(AbilityEffectParams abilityEffectParams)
		: this(
			abilityEffectParams.ID
			, int.Parse(abilityEffectParams.Parameters[0])
			, int.Parse(abilityEffectParams.Parameters[1])
			, int.Parse(abilityEffectParams.Parameters[2])
		)
	{
	}
	#endregion

	#region SearchZoneUnitToFieldAE Methods
	protected override bool IsCanBeTarget(UniqueCardData uniqueCard)
	{
		if (uniqueCard != null)
		{
			if (uniqueCard.PlayerCardData.TypeData == CardTypeData.CardType.Unit)
			{
				return true;
			}
		}

		return false;
	}
	#endregion
}
#endregion

#region RemoveCardAE Class
public class RemoveCardAE : SearchZoneToZoneShuffleAE
{
	// AE0036
	#region RemoveCardAE Properties
	#endregion

	#region RemoveCardAE Constructors
	public RemoveCardAE(string id, int num)
		: base(id, 2, num, 3)
	{
		AbilityEffectDBData effectDBData;
		bool isFoundEffect = AbilityEffectDB.GetData(this.AbilityEffectID, out effectDBData);
		if (isFoundEffect)
		{
			_description_EN = string.Format(effectDBData.Description_EN, this.Num);
			_description_TH = string.Format(effectDBData.Description_TH, this.Num);
		} 
		else
		{
			_description_EN = "";
			_description_TH = "";
		}

		this._isCanCancel = true;
	}

	public RemoveCardAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID, int.Parse(abilityEffectParams.Parameters[0]))
	{
	}
	#endregion

	#region SearchZoneUnitToZoneShuffleAE Methods
	protected override bool IsCanBeTarget(UniqueCardData uniqueCard)
	{
		if (uniqueCard != null)
		{
			return true;
		}

		return false;
	}
	#endregion
}
#endregion

#region SearchZoneMutantUnitToZoneShuffleAE Class
public class SearchZoneMutantUnitToZoneShuffleAE : SearchZoneToZoneShuffleAE
{
	// AE0077
	#region SearchZoneMutantUnitToZoneShuffleAE Properties
	#endregion

	#region SearchZoneMutantUnitToZoneShuffleAE Constructors
	public SearchZoneMutantUnitToZoneShuffleAE(string id, int sourceZoneIndex, int num, int targetZoneIndex, int isRequireNum)
		: base(id, sourceZoneIndex, num, targetZoneIndex)
	{
		this._isRequireNum = ((isRequireNum == 1) ? true : false);

		AbilityEffectDBData effectDBData;
		bool isFoundEffect = AbilityEffectDB.GetData(this.AbilityEffectID, out effectDBData);
		if (isFoundEffect)
		{
			_description_EN = string.Format(effectDBData.Description_EN, LocalizationManager.CardZoneToText(this.SourceZone), this.Num, LocalizationManager.CardZoneToText(this.TargetZone));
			_description_TH = string.Format(effectDBData.Description_TH, LocalizationManager.CardZoneToText(this.SourceZone), this.Num, LocalizationManager.CardZoneToText(this.TargetZone));
		} 
		else
		{
			_description_EN = "";
			_description_TH = "";
		}
	}

	public SearchZoneMutantUnitToZoneShuffleAE(AbilityEffectParams abilityEffectParams)
		: this(
			abilityEffectParams.ID
			, int.Parse(abilityEffectParams.Parameters[0])
			, int.Parse(abilityEffectParams.Parameters[1])
			, int.Parse(abilityEffectParams.Parameters[2])
			, int.Parse(abilityEffectParams.Parameters[3])
		)
	{
	}
	#endregion

	#region SearchZoneMutantUnitToZoneShuffleAE Methods
	protected override bool IsCanBeTarget(UniqueCardData uniqueCard)
	{
		if(uniqueCard != null)
		{
			if(uniqueCard.PlayerCardData.TypeData == CardTypeData.CardType.Unit)
			{
				if (uniqueCard.PlayerCardData.ClanData == CardClanData.CardClanType.Mutant)
				{
					return true;
				}
			}
		}

		return false;
	}
	#endregion
}
#endregion

#region RemoveMutantUnitAE Class
public class RemoveMutantUnitAE : RemoveCardAE
{
	// AE0037
	#region RemoveMutantUnitAE Properties
	#endregion

	#region RemoveMutantUnitAE Constructors
	public RemoveMutantUnitAE(string id, int num)
		: base(id, num)
	{
		AbilityEffectDBData effectDBData;
		bool isFoundEffect = AbilityEffectDB.GetData(this.AbilityEffectID, out effectDBData);
		if (isFoundEffect)
		{
			_description_EN = string.Format(effectDBData.Description_EN, this.Num);
			_description_TH = string.Format(effectDBData.Description_TH, this.Num);
		} 
		else
		{
			_description_EN = "";
			_description_TH = "";
		}

		_isCanCancel = true;
	}

	public RemoveMutantUnitAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID, int.Parse(abilityEffectParams.Parameters[0]))
	{
	}
	#endregion

	#region RemoveMutantUnitAE Methods
	protected override bool IsCanBeTarget(UniqueCardData uniqueCard)
	{
		if (uniqueCard != null)
		{
			if(uniqueCard.PlayerCardData.TypeData == CardTypeData.CardType.Unit)
			{
				if(uniqueCard.PlayerCardData.ClanData == CardClanData.CardClanType.Mutant)
				{
					return true;
				}
			}
		}

		return false;
	}
	#endregion
}
#endregion

#region Me3HP3ATwith3DialnoUnitAE Class
public class Me3HP3ATwith3DialnoUnitAE : BuffAE
{
	// AE0038
	#region Me3HP3ATwith3DialnoUnitAE Properties
	#endregion

	#region Me3HP3ATwith3DialnoUnitAE Constructors
	public Me3HP3ATwith3DialnoUnitAE(string id)
		:base(id)
	{
		/*
		_description_EN = string.Format(_description_EN);
		_description_TH = string.Format(_description_TH);
		*/
	}

	public Me3HP3ATwith3DialnoUnitAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID)
	{
	}
	#endregion

	#region Me3HP3ATwith3DialnoUnitAE Methods
	public override bool IsHasTarget()
	{
		return true;
	}

	public override bool GetTargetList(out List<BattleCardData> targetList)
	{
		targetList = new List<BattleCardData>();
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			if(playerIndex == _abilityData.OwnerPlayerIndex)
			{
				List<BattleCardData> target;
				BattleManager.Instance.GetTargetList(
					playerIndex
					, out target
					, this.IsCanBeTarget
				);

				if(target != null && target.Count > 0)
				{
					targetList.AddRange(target);
				}
			}
		}

		if(targetList.Count > 0)
		{
			return true;
		}

		targetList = null;
		return false;
	}

	private bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown && !battleCard.IsDead)
			{
				if(battleCard.BattleCardID == _abilityData.BattleCard.BattleCardID)
				{
					return true;
				}
			}
		}

		return false;
	}

	protected override bool IsActive ()
	{
		List<BattleCardData> target;
		BattleManager.Instance.GetTargetList(
			  _abilityData.OwnerPlayerIndex
			, out target
			, this.IsMatchCondition
		);

		if(target != null && target.Count >= 3)
		{
			return true;
		}

		return false;
	}

	private bool IsMatchCondition(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown && !battleCard.IsDead)
			{
				if(battleCard.TypeData == CardTypeData.CardType.Unit)
				{
					if(battleCard.ClanData == CardClanData.CardClanType.DiAlno)
					{
						return true;
					}
				}
			}
		}

		return false;
	}

	protected override void AddBuff(BattleCardData battleCard)
	{
		CardBuff buff = new CardBuff(
			this.AbilityEffectID
			, this._abilityData.BattleCard.BattleCardID
			, battleCard.BattleCardID
			, 0 // shield
			, 3 // hp
			, 3 // attack
			, 0 // speed
			, 0 // move
			, 0 // attack range
			, new PassiveIndex()
			, new List<CardSubTypeData>()
			, new List<CardJobData>()
			, this.OnUnitUpdate
			, -1 // active turn
		);

		BattleManager.Instance.RequestAddBuffBattleCard(battleCard.BattleCardID, buff);
	}
	#endregion
}
#endregion

#region Me5HP5ATwith3DialnoUnitAE Class
public class Me5HP5ATwith3DialnoUnitAE : BuffAE
{
	// AE0059
	#region Me5HP5ATwith3DialnoUnitAE Properties
	#endregion

	#region Me5HP5ATwith3DialnoUnitAE Constructors
	public Me5HP5ATwith3DialnoUnitAE(string id)
		:base(id)
	{
		/*
		_description_EN = string.Format(_description_EN);
		_description_TH = string.Format(_description_TH);
		*/
	}

	public Me5HP5ATwith3DialnoUnitAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID)
	{
	}
	#endregion

	#region Me5HP5ATwith3DialnoUnitAE Methods
	public override bool IsHasTarget()
	{
		return true;
	}

	public override bool GetTargetList(out List<BattleCardData> targetList)
	{
		targetList = new List<BattleCardData>();
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			if(playerIndex == _abilityData.OwnerPlayerIndex)
			{
				List<BattleCardData> target;
				BattleManager.Instance.GetTargetList(
					playerIndex
					, out target
					, this.IsCanBeTarget
				);

				if(target != null && target.Count > 0)
				{
					targetList.AddRange(target);
				}
			}
		}

		if(targetList.Count > 0)
		{
			return true;
		}

		targetList = null;
		return false;
	}

	private bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown && !battleCard.IsDead)
			{
				if(battleCard.BattleCardID == _abilityData.BattleCard.BattleCardID)
				{
					return true;
				}
			}
		}

		return false;
	}

	protected override bool IsActive ()
	{
		List<BattleCardData> target;
		BattleManager.Instance.GetTargetList(
			_abilityData.OwnerPlayerIndex
			, out target
			, this.IsMatchCondition
		);

		if(target != null && target.Count >= 3)
		{
			return true;
		}

		return false;
	}

	private bool IsMatchCondition(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown && !battleCard.IsDead)
			{
				if(battleCard.TypeData == CardTypeData.CardType.Unit)
				{
					if(battleCard.ClanData == CardClanData.CardClanType.DiAlno)
					{
						return true;
					}
				}
			}
		}

		return false;
	}

	protected override void AddBuff(BattleCardData battleCard)
	{
		CardBuff buff = new CardBuff(
			this.AbilityEffectID
			, this._abilityData.BattleCard.BattleCardID
			, battleCard.BattleCardID
			, 0 // shield
			, 5 // hp
			, 5 // attack
			, 0 // speed
			, 0 // move
			, 0 // attack range
			, new PassiveIndex()
			, new List<CardSubTypeData>()
			, new List<CardJobData>()
			, this.OnUnitUpdate
			, -1 // active turn
		);

		BattleManager.Instance.RequestAddBuffBattleCard(battleCard.BattleCardID, buff);
	}
	#endregion
}
#endregion

#region MeAT1TurnAE Class
public class MeAT1TurnAE : AbilityEffect
{
	// AE0039
	#region MeAT1TurnAE Properties
	private int _attack;

	public int Attack { get { return _attack; } }
	#endregion

	#region MeAT1TurnAE Constructors
	public MeAT1TurnAE(string id, int attack)
		: base(id)
	{
		this._attack = attack;

		_description_EN = string.Format(_description_EN, this.Attack);
		_description_TH = string.Format(_description_TH, this.Attack);
	}

	public MeAT1TurnAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID, int.Parse(abilityEffectParams.Parameters[0]))
	{
	}
	#endregion

	#region MeAT1TurnAE Methods
	public override bool IsHasTarget()
	{
		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				if(this._abilityData.IsSummon)
				{
					return true;
				}
			}
		}

		return false;
	}
		
	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
		if(IsHasTarget())
		{
			if(onFinishPrepare != null)
			{
				onFinishPrepare.Invoke();
			}
		}
		else
		{
			if(onCancelPrepare != null)
			{
				onCancelPrepare.Invoke();
			}
		}
	}

	public override void Action(OnFinishAction onFinishAction)
	{
		if(IsHasTarget())
		{
			BattleCardData card;
			bool isFound = BattleManager.Instance.FindBattleCard(
				  _abilityData.BattleCard.BattleCardID
				, out card
			);

			if(isFound)
			{
				CardBuff buff = new CardBuff(
					this.AbilityEffectID
					, -1
					, card.BattleCardID
					, 0 // shield
					, 0 // hp
					, this.Attack // attack
					, 0 // speed
					, 0 // move
					, 0 // attack range
					, new PassiveIndex()
					, new List<CardSubTypeData>()
					, new List<CardJobData>()
					, null
					, 1 // active turn
				);

				BattleManager.Instance.RequestAddBuffBattleCard(card.BattleCardID, buff);
			}
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region MyMutantUnit1HP1AT1TurnAE Class
public class MyMutantUnit1HP1AT1TurnAE : AbilityEffect
{
	// AE0040
	#region MyMutantUnit1HP1AT1TurnAE Properties
	private int _summonTurn = -1;

	protected List<BattleCardData> _targetList = null;
	protected List<CardBuff> _buffList = null;
	#endregion

	#region MyMutantUnit1HP1AT1TurnAE Constructors
	public MyMutantUnit1HP1AT1TurnAE(string id)
		: base(id)
	{
		/*
		_description_EN = string.Format(_description_EN);
		_description_TH = string.Format(_description_TH);
		*/
	}

	public MyMutantUnit1HP1AT1TurnAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID)
	{
	}
	#endregion

	#region MyMutantUnit1HP1AT1TurnAE Methods
	public override void Init(AbilityData abilityData)
	{
		//Debug.Log("Init: BuffAE");

		_abilityData = abilityData;

		if(BattleManager.Instance != null && _abilityData != null)
		{
			if(_abilityData.IsSummon)
			{
				_summonTurn = BattleManager.Instance.Turn;

				BattleManager.Instance.BindBattleEventTrigger(
					  this.OnUnitUpdate
					, _abilityData.OwnerPlayerIndex
					, BattleEventTrigger.BattleTriggerType.CARD_BE_SUMMONED
					, BattleEventTrigger.BattleSide.Friendly
				);
			}
		}
	}

	public override void Deinit()
	{
		//Debug.Log("Deinit: BuffAE");

		if(BattleManager.Instance != null && _abilityData != null)
		{
			if(_abilityData.IsSummon)
			{
				BattleManager.Instance.UnbindBattleEventTrigger(
					this.OnUnitUpdate
					, _abilityData.OwnerPlayerIndex
					, BattleEventTrigger.BattleTriggerType.CARD_BE_SUMMONED
					, BattleEventTrigger.BattleSide.Friendly
				);
			}
		}

		_abilityData = null;
	}

	protected void OnUnitUpdate()
	{
		//Debug.Log("BuffAE: OnUnitUpdate " + this._abilityData.BattleCard.BattleCardID);

		if(_summonTurn != BattleManager.Instance.Turn) return;

		if(IsActive())
		{
			// Active
			List<BattleCardData> prevTargetList = _targetList;

			List<BattleCardData> targetList = null;
			GetTargetList(out targetList);

			List<BattleCardData> removeTargetList = new List<BattleCardData>();
			List<BattleCardData> oldTargetList = new List<BattleCardData>();
			List<BattleCardData> newTargetList = new List<BattleCardData>();

			if(targetList != null && targetList.Count > 0)
			{
				//Debug.Log("Terget: " + targetList.Count.ToString());

				if(prevTargetList != null && prevTargetList.Count > 0)
				{
					//Debug.Log("Prev: " + prevTargetList.Count.ToString());

					// has old target.
					for(int i = 0; i < targetList.Count; ++i)
					{
						for(int j = 0; j < prevTargetList.Count; ++j)
						{
							if(targetList[i] == prevTargetList[j])
							{
								// Is remain target.
								oldTargetList.Add(targetList[i]);

								targetList.RemoveAt(i);
								prevTargetList.RemoveAt(j);

								--i;
								break;
							}
						}
					}

					if(prevTargetList.Count > 0)
					{
						removeTargetList.AddRange(prevTargetList);
					}

					if(targetList.Count > 0)
					{
						newTargetList.AddRange(targetList);
					}
				}
				else
				{
					//Debug.Log("Prev: No target.");

					// no old target.
					newTargetList.AddRange(targetList);
				}
			}
			else
			{
				//Debug.Log("Terget: No target.");

				// No target.
				// Remove all current target.
				if(prevTargetList != null && prevTargetList.Count > 0)
				{
					removeTargetList.AddRange(prevTargetList);
				}
			}

			/*
			Debug.Log("Remove: " + removeTargetList.Count);
			Debug.Log("New: " + newTargetList.Count);
			Debug.Log("Old: " + oldTargetList.Count);
			*/

			List<BattleCardData> currentTargetList = new List<BattleCardData>();

			for(int index = 0; index < removeTargetList.Count; ++index)
			{
				//Debug.Log("Remove Terget: " + removeTargetList.Count.ToString());
				RemoveBuff(removeTargetList[index]);
			}
			for(int index = 0; index < newTargetList.Count; ++index)
			{
				//Debug.Log("New Terget: " + newTargetList.Count.ToString());
				AddBuff(newTargetList[index]);
			}

			currentTargetList.AddRange(oldTargetList);
			currentTargetList.AddRange(newTargetList);

			_targetList = currentTargetList;
		}
		else
		{
			// Deactive
			if(_targetList != null && _targetList.Count > 0)
			{
				for(int index = 0; index < _targetList.Count; ++index)
				{
					RemoveBuff(_targetList[index]);
				}

				_targetList.Clear();
			}
		}
	}

	protected virtual bool IsActive()
	{
		return true;
	}

	protected virtual void AddBuff(BattleCardData battleCard)
	{
		CardBuff buff = new CardBuff(
			this.AbilityEffectID
			, -1
			, battleCard.BattleCardID
			, 0 // shield
			, 1 // hp
			, 1 // attack
			, 0 // speed
			, 0 // move
			, 0 // attack range
			, new PassiveIndex()
			, new List<CardSubTypeData>()
			, new List<CardJobData>()
			, null
			, 1 // active turn
		);

		BattleManager.Instance.RequestAddBuffBattleCard(battleCard.BattleCardID, buff);
	}

	protected virtual void RemoveBuff(BattleCardData battleCard)
	{
		battleCard.RemoveBuff(
			  this._abilityData.BattleCard.BattleCardID
			, this.AbilityEffectID
		);
	}

	public override bool IsHasTarget()
	{
		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				return true;
			}
		}

		return false;
	}

	public override bool GetTargetList(out List<BattleCardData> targetList)
	{
		targetList = new List<BattleCardData>();
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			if(playerIndex == _abilityData.OwnerPlayerIndex)
			{
				List<BattleCardData> target;
				BattleManager.Instance.GetTargetList(
					playerIndex
					, out target
					, this.IsCanBeTarget
				);

				if(target != null && target.Count > 0)
				{
					targetList.AddRange(target);
				}
			}
		}

		if(targetList.Count > 0)
		{
			return true;
		}

		targetList = null;
		return false;
	}

	private bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown && !battleCard.IsDead)
			{
				if(battleCard.TypeData == CardTypeData.CardType.Unit)
				{
					if(battleCard.ClanData == CardClanData.CardClanType.Mutant)
					{
						return true;
					}
				}
			}
		}

		return false;
	}

	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
		if(onFinishPrepare != null)
		{
			onFinishPrepare.Invoke();
		}
	}

	public override void Action(OnFinishAction onFinishAction)
	{
		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region Me1HP1ATAE Class
public class Me1HP1ATAE : AbilityEffect
{
	// AE0041
	#region Me1HP1ATAE Properties
	#endregion

	#region Me1HP1ATAE Constructors
	public Me1HP1ATAE(string id)
		: base(id)
	{
		this._abilityEffectID = id;

		/*
		_description_EN = string.Format(_description_EN);
		_description_TH = string.Format(_description_TH);
		*/
	}

	public Me1HP1ATAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID)
	{
	}
	#endregion

	#region Me1HP1ATAE Methods
	public override bool IsHasTarget()
	{
		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				if(_abilityData.IsSummon)
				{
					return true;
				}
			}
		}

		return false;
	}

	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
		if(IsHasTarget())
		{
			if(onFinishPrepare != null)
			{
				onFinishPrepare.Invoke();
			}
		}
		else
		{
			if(onCancelPrepare != null)
			{
				onCancelPrepare.Invoke();
			}
		}
	}

	public override void Action(OnFinishAction onFinishAction)
	{
		if(IsHasTarget())
		{
			BattleCardData battleCard;
			bool isFound = BattleManager.Instance.FindBattleCard(
				  _abilityData.BattleCard.BattleCardID
				, out battleCard
			);

			if(isFound)
			{
				CardBuff buff = new CardBuff(
					this.AbilityEffectID
					, -1
					, battleCard.BattleCardID
					, 0 // shield
					, 1 // hp
					, 1 // attack
					, 0 // speed
					, 0 // move
					, 0 // attack range
					, new PassiveIndex()
					, new List<CardSubTypeData>()
					, new List<CardJobData>()
					, null
					, -1 // active turn
				);

				BattleManager.Instance.RequestAddBuffBattleCard(battleCard.BattleCardID, buff);
			}
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region OtherSoldierUnit1HP1ATPiercingAE Class
public class OtherSoldierUnit1HP1ATPiercingAE : BuffAE
{
	// AE0042
	#region OtherSoldierUnit1HP1ATPiercingAE Properties
	#endregion

	#region OtherSoldierUnit1HP1ATPiercingAE Constructors
	public OtherSoldierUnit1HP1ATPiercingAE(string id)
		:base(id)
	{
		/*
		_description_EN = string.Format(_description_EN);
		_description_TH = string.Format(_description_TH);
		*/
	}

	public OtherSoldierUnit1HP1ATPiercingAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID)
	{
	}
	#endregion

	#region OtherSoldierUnit1HP1ATPiercingAE Methods
	public override bool IsHasTarget()
	{
		return true;
	}

	public override bool GetTargetList(out List<BattleCardData> targetList)
	{
		targetList = new List<BattleCardData>();
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			if(playerIndex == _abilityData.OwnerPlayerIndex)
			{
				List<BattleCardData> target;
				BattleManager.Instance.GetTargetList(
					playerIndex
					, out target
					, this.IsCanBeTarget
				);

				if(target != null && target.Count > 0)
				{
					targetList.AddRange(target);
				}
			}
		}

		if(targetList.Count > 0)
		{
			return true;
		}

		targetList = null;
		return false;
	}

	private bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
            // Not facedown and Not dead
			if(!battleCard.IsFaceDown && !battleCard.IsDead)
			{
                // Not Me
                if(battleCard.BattleCardID != this._abilityData.BattleCard.BattleCardID)
                {
                    // Unit
    				if(battleCard.TypeData == CardTypeData.CardType.Unit)
    				{
                        // Job == Soldier
                        if (battleCard.IsCardJobType(CardJobData.JobType.Soldier))
                        {
                            return true;
                        }
    				}
                }
			}
		}

		return false;
	}

	protected override void AddBuff(BattleCardData battleCard)
	{
		CardBuff buff = new CardBuff(
			this.AbilityEffectID
			, this._abilityData.BattleCard.BattleCardID
			, battleCard.BattleCardID
			, 0 // shield
			, 1 // hp
			, 1 // attack
			, 0 // speed
			, 0 // move
			, 0 // attack range
			, new PassiveIndex((int)PassiveIndex.PassiveType.Piercing)
			, new List<CardSubTypeData>()
			, new List<CardJobData>()
			, this.OnUnitUpdate
			, -1 // active turn
		);

		BattleManager.Instance.RequestAddBuffBattleCard(battleCard.BattleCardID, buff);
	}
	#endregion
}
#endregion

#region RemoveUnitAE Class
public class RemoveUnitAE : RemoveCardAE
{
	// AE0043
	#region RemoveUnitAE Properties
	#endregion

	#region RemoveUnitAE Constructors
	public RemoveUnitAE(string id, int num)
		: base(id, num)
	{
		AbilityEffectDBData effectDBData;
		bool isFoundEffect = AbilityEffectDB.GetData(this.AbilityEffectID, out effectDBData);
		if (isFoundEffect)
		{
			_description_EN = string.Format(effectDBData.Description_EN, this.Num);
			_description_TH = string.Format(effectDBData.Description_TH, this.Num);
		} 
		else
		{
			_description_EN = "";
			_description_TH = "";
		}

		_isCanCancel = true;
	}

	public RemoveUnitAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID, int.Parse(abilityEffectParams.Parameters[0]))
	{
	}
	#endregion

	#region RemoveUnitAE Methods
	protected override bool IsCanBeTarget(UniqueCardData uniqueCard)
	{
		if (uniqueCard != null)
		{
			if(uniqueCard.PlayerCardData.TypeData == CardTypeData.CardType.Unit)
			{
				return true;
			}
		}

		return false;
	}
	#endregion
}
#endregion

#region GetGoldIfControlUniqueMutantGetGoldAE Class
public class GetGoldIfControlUniqueMutantGetGoldAE : AbilityEffect
{
	// AE0044
	#region GetGoldIfControlUniqueMutantGetGoldAE Properties
	protected int _gold1;
	protected int _gold2;

	public int Gold1 { get { return _gold1; } }
	public int Gold2 { get { return _gold2; } }
	#endregion

	#region GetGoldIfControlUniqueMutantGetGoldAE Constructors
	public GetGoldIfControlUniqueMutantGetGoldAE(string id, int gold1, int gold2)
		: base(id)
	{
		this._gold1 = gold1;
		this._gold2 = gold2;

		_description_EN = string.Format(_description_EN, this.Gold1, this.Gold2);
		_description_TH = string.Format(_description_TH, this.Gold1, this.Gold2);
	}

	public GetGoldIfControlUniqueMutantGetGoldAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID, int.Parse(abilityEffectParams.Parameters[0]), int.Parse(abilityEffectParams.Parameters[1]))
	{
	}
	#endregion

	#region GetGoldIfControlUniqueMutantGetGoldAE Methods
	public override bool IsHasTarget()
	{
		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				return true;
			}
		}

		return false;
	}
		
	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
		if(IsHasTarget())
		{
			if(onFinishPrepare != null)
			{
				onFinishPrepare.Invoke();
			}
		}
		else
		{
			if(onCancelPrepare != null)
			{
				onCancelPrepare.Invoke();
			}
		}
	}

	public override void Action(OnFinishAction onFinishAction)
	{
		if(IsHasTarget())
		{
			List<BattleCardData> unitList;
			BattleManager.Instance.GetTargetList(
				  _abilityData.OwnerPlayerIndex
				, out unitList
				, IsUniqueMutantUnit
			);

			if(unitList != null && unitList.Count > 0)
			{
				BattleManager.Instance.RequestGetGold(_abilityData.OwnerPlayerIndex, this._gold2);
			}
			else
			{
				BattleManager.Instance.RequestGetGold(_abilityData.OwnerPlayerIndex, this._gold1);
			}
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}

	private bool IsUniqueMutantUnit(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown && !battleCard.IsDead)
			{
				if(battleCard.TypeData == CardTypeData.CardType.Unit)
				{						
					if(battleCard.ClanData == CardClanData.CardClanType.Mutant)
					{
                        if (battleCard.IsCardSubType(CardSubTypeData.CardSubType.Unique))
                        {
                            return true;
                        }
					}
				}
			}
		}

		return false;
	}
	#endregion
}
#endregion

#region DeckToGaveyardAE Class
public class DeckToGaveyardAE : AbilityEffect
{
	// AE0045
	#region DeckToGaveyardAE Properties
	private int _num;

	public int Num { get { return _num; } }
	#endregion

	#region DeckToGaveyardAE Constructors
	public DeckToGaveyardAE(string id, int num)
		: base(id)
	{
		this._num = num;

		_description_EN = string.Format(_description_EN, this.Num);
		_description_TH = string.Format(_description_TH, this.Num);
	}

	public DeckToGaveyardAE(AbilityEffectParams abilityEffectParams)
		: this(
			  abilityEffectParams.ID
			, int.Parse(abilityEffectParams.Parameters[0])
	)
	{
	}
	#endregion

	#region DeckToGaveyardAE Methods
	public override bool IsHasTarget()
	{
		if(BattleManager.Instance != null && this._abilityData != null)
		{
			return true;
		}

		return false;
	}

	public override bool GetTargetList(out List<UniqueCardData> targetList)
	{
		targetList = new List<UniqueCardData>();

		List<UniqueCardData> target;
		BattleManager.Instance.GetTargetList(
			  _abilityData.OwnerPlayerIndex
			, CardZoneIndex.Deck
			, out target
			, null
		);

		if(target != null && target.Count > 0)
		{
			for(int index = 0; index < target.Count && index < this._num; ++index)
			{
				targetList.Add(target[index]);
			}
		}

		if(targetList != null && targetList.Count > 0)
		{
			return true;
		}

		targetList = null;
		return false;
	}

	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
		if(IsHasTarget())
		{
			if(onFinishPrepare != null)
			{
				onFinishPrepare.Invoke();
			}
		}
		else
		{
			if(onCancelPrepare != null)
			{
				onCancelPrepare.Invoke();
			}
		}
	}

	public override void Action(OnFinishAction onFinishAction)
	{
		if(IsHasTarget())
		{
			List<UniqueCardData> targetList;
			GetTargetList(out targetList);

			if(targetList != null && targetList.Count > 0)
			{
				for(int index = 0; index < targetList.Count; ++index)
				{
					BattleManager.Instance.ActionMoveUniqueCard(
						  targetList[index].UniqueCardID
						, CardZoneIndex.Graveyard
					);
				}
			}
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region DestroyAllUnitAtFieldAE Class
public class DestroyAllUnitAtFieldAE : DestroyAllAtFieldAE
{
	// AE0046
	#region DestroyAllUnitAtFieldAE Properties
	#endregion

	#region DestroyAllUnitAtFieldAE Constructors
	public DestroyAllUnitAtFieldAE(string id, int localTargetBattleZone)
        : base(id, localTargetBattleZone, (int)CardZoneIndex.Graveyard)
	{
		_description_EN = string.Format(_description_EN, LocalizationManager.LocalBattleZoneToText(this.TargetBattleZone));
		_description_TH = string.Format(_description_TH, LocalizationManager.LocalBattleZoneToText(this.TargetBattleZone));
	}

	public DestroyAllUnitAtFieldAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID, int.Parse(abilityEffectParams.Parameters[0]))
	{
	}
	#endregion

	#region DestroyAllUnitAtFieldAE Methods
	protected override bool IsCanBeTarget(BattleCardData battleCard)
	{
        if (base.IsCanBeTarget(battleCard))
		{
			if(battleCard.TypeData == CardTypeData.CardType.Unit)
			{
				return true;
			}
		}

		return false;
	}
	#endregion
}
#endregion

#region DealDamageTargetMyBaseAE Class
public class DealDamageTargetMyBaseAE : AbilityEffect
{
	// AE0047
	#region DealDamageTargetMyBaseAE Properties
	protected int _damage;
	protected int _num;

	protected List<int> _selectedTargetIDList = null;

	public int Damage
	{
		get { return _damage; }
	}
	public int Num
	{
		get { return _num; }
	}
	#endregion

	#region DealDamageTargetMyBaseAE Constructors
	public DealDamageTargetMyBaseAE(string id, int damage, int num)
		: base(id)
	{
		this._abilityEffectID = id;
		this._num = num;
		this._damage = damage;

		_description_EN = string.Format(_description_EN, this.Damage, this.Num);
		_description_TH = string.Format(_description_TH, this.Damage, this.Num);
	}

	public DealDamageTargetMyBaseAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID
			, int.Parse(abilityEffectParams.Parameters[0])
			, int.Parse(abilityEffectParams.Parameters[1])
	)
	{
	}
	#endregion

	#region DealDamageTargetMyBaseAE Methods
	public override bool IsHasTarget()
	{
		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				return true;
			}
		}

		return false;
	}

	public override bool GetTargetList(out List<BattleCardData> targetList)
	{
		targetList = new List<BattleCardData>();

		List<BattleCardData> target;
		BattleManager.Instance.GetTargetList(
			  _abilityData.OwnerPlayerIndex
			, out target
			, this.IsCanBeTarget
		);

		if(target != null && target.Count > 0)
		{
			targetList.AddRange(target);
		}

		if(targetList.Count > 0)
		{
			return true;
		}

		targetList = null;
		return false;
	}

	protected virtual bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown && !battleCard.IsDead)
			{
				if(battleCard.TypeData == CardTypeData.CardType.Base)
				{
					return true;
				}
			}
		}

		return false;
	}

	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
		/*
        if(_abilityData.IsSummon)
        {
            Debug.Log("DealDamageAE: Prepare " + _abilityData.BattleCard.Name);
        }
        else
        {
            Debug.Log("DealDamageAE: Prepare " + _abilityData.UniqueCard.PlayerCardData.Name);
        }
        */
        
        _onFinishPrepare = onFinishPrepare;
        _onCancelPrepare = onCancelPrepare;

		if(IsHasTarget())
		{
			List<BattleCardData> targetList;
			GetTargetList(out targetList);
            
			BattleManager.Instance.RequestDealDamage(
				_abilityData.OwnerPlayerIndex
				, targetList
				, this.Damage
				, this.Num
				, new BattleManager.OnFinishDealDamage(this.OnSelectedTarget)
				, new BattleManager.OnCancelDealDamage(this.OnCancelTarget)
                , (BattleManager.Instance.IsHaveTimeout && !this.IsOwnerTurn)
                , !this.IsSummon
			);
            
            return;
		}
		
        OnCancelTarget();
	}

	protected virtual void OnSelectedTarget(List<int> selectedTargetIDList)
	{
		_selectedTargetIDList = selectedTargetIDList;
        
        AddParamIntList(_selectedTargetIDList);

		if(_onFinishPrepare != null)
		{
			_onFinishPrepare.Invoke();
		}
	}
    
    private void OnCancelTarget()
    {
        if(_onCancelPrepare != null)
        {
            _onCancelPrepare.Invoke();
        }
    }

	public override void Action(OnFinishAction onFinishAction)
	{
        GetParamIntList(out _selectedTargetIDList);
    
		if(_selectedTargetIDList != null && _selectedTargetIDList.Count > 0)
		{
			foreach(int battleCardID in _selectedTargetIDList)
			{
				BattleManager.Instance.RequestDealDamageBattleCard(
					  battleCardID
					, this.Damage
					, false
				);
			}
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region AllUnitAt0AE Class
public class AllUnitAt0AE : AbilityEffect
{
	// AE0048
	#region AllUnitAt0AE Properties
	#endregion

	#region AllUnitAt0AE Constructors
	public AllUnitAt0AE(string id)
		: base(id)
	{
		/*
		_description_EN = string.Format(_description_EN);
		_description_TH = string.Format(_description_TH);
		*/ 
	}

	public AllUnitAt0AE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID)
	{
	}
	#endregion

	#region AllUnitAt0AE Methods
	public override bool IsHasTarget()
	{
		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				return true;
			}
		}

		return false;
	}

	public override bool GetTargetList (out List<BattleCardData> targetList)
	{
		targetList = new List<BattleCardData>();
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			//if(playerIndex == _abilityData.OwnerPlayerIndex)
			{
				List<BattleCardData> target;
				BattleManager.Instance.GetTargetList(
					playerIndex
					, out target
					, this.IsCanBeTarget
				);

				if(target != null && target.Count > 0)
				{
					targetList.AddRange(target);
				}
			}
		}

		if(targetList.Count > 0)
		{
			return true;
		}

		targetList = null;
		return false;
	}

	private bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown && !battleCard.IsDead)
			{
				if(battleCard.TypeData == CardTypeData.CardType.Unit)
				{
					return true;
				}
			}
		}

		return false;
	}

	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
		if(IsHasTarget())
		{
			if(onFinishPrepare != null)
			{
				onFinishPrepare.Invoke();
			}
		}
		else
		{
			if(onCancelPrepare != null)
			{
				onCancelPrepare.Invoke();
			}
		}
	}

	public override void Action(OnFinishAction onFinishAction)
	{
		if(IsHasTarget())
		{
			List<BattleCardData> targetList;
			GetTargetList(out targetList);

			if(targetList != null && targetList.Count > 0)
			{
				foreach(BattleCardData battleCard in targetList)
				{
					BattleCardData card;
					bool isFound = BattleManager.Instance.FindBattleCard(
						  battleCard.BattleCardID
						, out card
					);

					if(isFound)
					{
						CardBuff buff = new CardBuff(
							this.AbilityEffectID
							, -1
							, card.BattleCardID
							, 0 // shield
							, 0 // hp
							, -card.CurrentAttack // attack
							, 0 // speed
							, 0 // move
							, 0 // attack range
							, new PassiveIndex()
							, new List<CardSubTypeData>()
							, new List<CardJobData>()
							, null
							, 1 // active turn
						);

						BattleManager.Instance.RequestAddBuffBattleCard(card.BattleCardID, buff);
					}
				}
			}
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region DestroyTargetNonLeaderMachineUnitAE Class
public class DestroyTargetNonLeaderMachineUnitAE : DestroyTargetAE
{
	// AE0049
	#region DestroyTargetNonLeaderMachineUnitAE Properties
	#endregion

	#region DestroyTargetNonLeaderMachineUnitAE Constructors
	public DestroyTargetNonLeaderMachineUnitAE(string id, int num)
        : base(id, num, (int)CardZoneIndex.Graveyard)
	{
		_description_EN = string.Format(_description_EN, this.Num);
		_description_TH = string.Format(_description_TH, this.Num);
	}

	public DestroyTargetNonLeaderMachineUnitAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID
			, int.Parse(abilityEffectParams.Parameters[0])
		)
	{
	}
	#endregion

	#region DestroyTargetNonLeaderMachineUnitAE Methods
	protected override bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown && !battleCard.IsDead)
			{
				if(battleCard.TypeData == CardTypeData.CardType.Unit)
				{
					if(!battleCard.IsLeader)
					{
                        if (battleCard.IsCardSubType(CardSubTypeData.CardSubType.Machine))
                        {
                            return true;
                        }
					}
				}
			}
		}

		return false;
	}
	#endregion
}
#endregion

#region OwnerMayDrawCardAE Class
public class OwnerMayDrawCardAE : AbilityEffect
{
	// AE0050
	#region OwnerMayDrawCardAE Properties
	private int _drawNum;
	private bool _bIsWantDraw = false;

	public int DrawNum
	{
		get { return _drawNum; }
	}
	#endregion

	#region OwnerMayDrawCardAE Constructors
	public OwnerMayDrawCardAE(string id, int drawNum)
		: base(id)
	{
		this._drawNum = drawNum;

		_description_EN = string.Format(_description_EN, this.DrawNum);
		_description_TH = string.Format(_description_TH, this.DrawNum);
	}

	public OwnerMayDrawCardAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID, int.Parse(abilityEffectParams.Parameters[0]))
	{
	}
	#endregion

	#region OwnerMayDrawCardAE Methods
	public override bool IsHasTarget()
	{
		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				return true;
			}
		}

		return false;
	}

	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
        _onFinishPrepare = onFinishPrepare;
        _onCancelPrepare = onCancelPrepare;
    
		if(IsHasTarget())
		{
			string numText = "";
			string numPostfixText = "";
			switch (LocalizationManager.CurrentLanguage)
			{
				case LocalizationManager.Language.Thai:
				{
					numText = this.DrawNum.ToString();
					numPostfixText = "";
				}
				break;

				case LocalizationManager.Language.English:
				default:
				{
					if (this.DrawNum == 1)
					{
						numText = "a";
						numPostfixText = "";
					} 
					else
					{
						numText = this.DrawNum.ToString();
						numPostfixText = "s";
					}
				}
				break;
			}

			string question = string.Format(
				Localization.Get("BATTLE_ASK_DRAW_CARD")
				, numText
				, numPostfixText
			);

			BattleManager.Instance.AskYesNo(
                  this._abilityData.OwnerPlayerIndex
                , question
                , this.OnYesDraw
                , this.OnNoDraw
                , this.OnCancel
                , (BattleManager.Instance.IsHaveTimeout && !this.IsOwnerTurn)
            );
            
            return;
		}
        
		OnCancel();
	}

	private void OnYesDraw()
	{
		_bIsWantDraw = true;
        AddParamBool(_bIsWantDraw);

		if(_onFinishPrepare != null)
		{
			_onFinishPrepare.Invoke();
		}
	}

	private void OnNoDraw()
	{
		_bIsWantDraw = false;
        AddParamBool(_bIsWantDraw);

		if(_onFinishPrepare != null)
		{
			_onFinishPrepare.Invoke();
		}
	}
    
    private void OnCancel()
    {
        if(_onCancelPrepare != null)
        {
            _onCancelPrepare.Invoke();
        }
    }

	public override void Action(OnFinishAction onFinishAction)
	{
        GetParamBool(out _bIsWantDraw);
    
		if(_bIsWantDraw)
		{
			BattleManager.Instance.RequestDrawCard(
				  _abilityData.OwnerPlayerIndex
				, this.DrawNum
			);
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region TargetNonBaseStructureMinus2HP1AT1SPMachineTurnAE Class
public class TargetNonBaseStructureMinus2HP1AT1SPMachineTurnAE : AbilityEffect
{
	// AE0051
	#region TargetNonBaseStructureMinus2HP1AT1SPMachineTurnAE Properties
	private int _num;

	public int Num { get { return _num; } }

	private List<int> _selectedTargetIDList = null;
	#endregion

	#region TargetNonBaseStructureMinus2HP1AT1SPMachineTurnAE Constructors
	public TargetNonBaseStructureMinus2HP1AT1SPMachineTurnAE(string id, int num)
		: base(id)
	{
		this._num = num;

		_description_EN = string.Format(_description_EN, this.Num);
		_description_TH = string.Format(_description_TH, this.Num);
	}

	public TargetNonBaseStructureMinus2HP1AT1SPMachineTurnAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID, int.Parse(abilityEffectParams.Parameters[0]))
	{
	}
	#endregion

	#region TargetNonBaseStructureMinus2HP1AT1SPMachineTurnAE Methods
	public override bool IsHasTarget()
	{
		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				//if(_abilityData.BattleCard != null && _abilityData.BattleCard.IsActive)
				{
					return true;
				}
			}
		}

		return false;
	}

	public override bool GetTargetList(out List<BattleCardData> targetList)
	{
		targetList = new List<BattleCardData>();

		List<BattleCardData> target;
		BattleManager.Instance.GetTargetList(
			  _abilityData.OwnerPlayerIndex
			, out target
			, this.IsCanBeTarget
		);

		if(target != null && target.Count > 0)
		{
			targetList.AddRange(target);
		}

		if(targetList.Count > 0)
		{
			return true;
		}

		targetList = null;
		return false;
	}

	private bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown)
			{
				if(battleCard.TypeData == CardTypeData.CardType.Structure)
				{
					return true;
				}
			}
		}

		return false;
	}

	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
        _onFinishPrepare = onFinishPrepare;
        _onCancelPrepare = onCancelPrepare;
    
		if(IsHasTarget())
		{
			List<BattleCardData> targetList;
			GetTargetList(out targetList);

			string headerText = "";
			switch (LocalizationManager.CurrentLanguage)
			{
				case LocalizationManager.Language.Thai:
				{
					headerText = this._description_TH;
				}
				break;

				case LocalizationManager.Language.English:
				default:
				{
					headerText = this._description_EN;
				}
				break;
			}

			BattleManager.Instance.RequestSelectTarget(
				headerText
				, _abilityData.OwnerPlayerIndex
				, targetList
				, this.Num
				, new BattleManager.OnFinishSelectTargetCallback(this.OnSelectedTarget)
				, new BattleManager.OnCancelSelectTargetCallback(this.OnCancelTarget)
                , (BattleManager.Instance.IsHaveTimeout && !this.IsOwnerTurn)
				, false
			);
            
            return;
		}
        
		OnCancelTarget();
	}

	private void OnSelectedTarget(List<int> selectedTargetIDList)
	{
		_selectedTargetIDList = selectedTargetIDList;
        
        AddParamIntList(_selectedTargetIDList);

		if(_onFinishPrepare != null)
		{
			_onFinishPrepare.Invoke();
		}
	}
    
    private void OnCancelTarget()
    {
        if(_onCancelPrepare != null)
        {
            _onCancelPrepare.Invoke();
        }
    }

	public override void Action(OnFinishAction onFinishAction)
	{
        GetParamIntList(out _selectedTargetIDList);
        
		if(_selectedTargetIDList != null && _selectedTargetIDList.Count > 0)
		{
			foreach(int battleCardID in _selectedTargetIDList)
			{
				BattleCardData battleCard;
				bool isFound = BattleManager.Instance.FindBattleCard(battleCardID, out battleCard);
				if(isFound)
				{
					List<CardSubTypeData> subTypeList = new List<CardSubTypeData>();
					subTypeList.Add(new CardSubTypeData(CardSubTypeData.CardSubType.Machine));
					subTypeList.Add(new CardSubTypeData(CardSubTypeData.CardSubType.Robot));

					CardBuff buff = new CardBuff(
						this.AbilityEffectID
						, -1
						, battleCard.BattleCardID
						, 0 // shield
						, -2 // hp
						, 1 // attack
						, 1 // speed
						, 0 // move
						, 0 // attack range
						, new PassiveIndex()
						, subTypeList
						, new List<CardJobData>()
						, null
						, 1 // active turn
					);

					BattleManager.Instance.RequestAddBuffBattleCard(battleCard.BattleCardID, buff);
				}
			}
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region IfHandGetGoldAE Class
public class IfHandGetGoldAE : AbilityEffect
{
	// AE0052
    #region IfHandGetGoldAE Properties
    private int _handNum;
    private int _gold;

	public int HandNum { get { return _handNum; } }
	public int Gold { get { return _gold; } }
    #endregion

    #region IfHandGetGoldAE Constructors
    public IfHandGetGoldAE(string id, int handNum, int gold)
		: base(id)
    {
        this._handNum = handNum;
        this._gold = gold;

		_description_EN = string.Format(_description_EN, this.HandNum, this.Gold);
		_description_TH = string.Format(_description_TH, this.HandNum, this.Gold);
    }

    public IfHandGetGoldAE(AbilityEffectParams abilityEffectParams)
        : this(abilityEffectParams.ID
        , int.Parse(abilityEffectParams.Parameters[0])
        , int.Parse(abilityEffectParams.Parameters[1])
    )
    {
    }
    #endregion

    #region IfHandGetGoldAE Methods
    public override bool IsHasTarget()
    {
        if (BattleManager.Instance != null)
        {
            if (this._abilityData != null)
            {
                return true;
            }
        }

        return false;
    }

    public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
    {
        if (IsHasTarget())
        {
            if (onFinishPrepare != null)
            {
                onFinishPrepare.Invoke();
            }
        }
        else
        {
            if (onCancelPrepare != null)
            {
                onCancelPrepare.Invoke();
            }
        }
    }

    public override void Action(OnFinishAction onFinishAction)
    {
        if (IsHasTarget())
        {
            if (BattleManager.Instance.GetHandCount(_abilityData.OwnerPlayerIndex) <= this._handNum)
            {
                BattleManager.Instance.RequestGetGold(
                    _abilityData.OwnerPlayerIndex
                    , this._gold
                );
            }
        }

        if (onFinishAction != null)
        {
            onFinishAction.Invoke();
        }
    }
    #endregion
}
#endregion

#region DestroyTargetNonLeaderUnitLowestHPAE Class
public class DestroyTargetNonLeaderUnitLowestHPAE : AbilityEffect
{
	// AE0055
	#region DestroyTargetNonLeaderUnitLowestHPAE Properties
	protected int _num;

    protected List<int> _selectedTargetIDList = null;

	public int Num { get { return _num; } }
	#endregion

	#region DestroyTargetNonLeaderUnitLowestHPAE Constructors
	public DestroyTargetNonLeaderUnitLowestHPAE(string id, int num)
		: base(id)
	{
        this._num = num;

		_description_EN = string.Format(_description_EN, this.Num);
		_description_TH = string.Format(_description_TH, this.Num);
	}

    public DestroyTargetNonLeaderUnitLowestHPAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID
			, int.Parse(abilityEffectParams.Parameters[0])
		)
	{
	}
	#endregion

    #region DestroyTargetNonLeaderUnitLowestHPAE Methods
    public override bool IsHasTarget()
    {
        if (BattleManager.Instance != null)
        {
            if (this._abilityData != null)
            {
                if (this._abilityData.IsSummon)
                {
                    return true;
                }
                else
                {
                    List<BattleCardData> targetList;
                    GetTargetList(out targetList);
                    if (targetList != null && targetList.Count > 0)
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public override bool GetTargetList(out List<BattleCardData> targetList)
    {
        targetList = new List<BattleCardData>();
        foreach (PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
        {
            List<BattleCardData> target;
            BattleManager.Instance.GetTargetList(
                  playerIndex
                , out target
                , this.IsCanBeTarget
            );

            if (target != null && target.Count > 0)
            {
                targetList.AddRange(target);
            }
        }

        if (targetList.Count > 0)
        {
            int lowestHP = int.MaxValue;
            while (targetList.Count > this._num)
            {
                bool isRemove = false;
                for (int index = 0; index < targetList.Count; ++index)
                {
                    if (lowestHP >= targetList[index].CurrentHP)
                    {
                        lowestHP = targetList[index].CurrentHP;
                    }
                    else
                    {
                        targetList.RemoveAt(index);
                        isRemove = true;
                        index--;
                    }
                }

                if (!isRemove)
                {
                    break;
                }
            }

            Debug.Log(targetList.Count);

            return true;
        }

        targetList = null;
        return false;
    }

    protected virtual bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown && !battleCard.IsDead)
			{
				if(battleCard.TypeData == CardTypeData.CardType.Unit)
				{
					if(!battleCard.IsLeader)
					{
						return true;
					}
				}
			}
		}

		return false;
	}

    public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
        _onFinishPrepare = onFinishPrepare;
        _onCancelPrepare = onCancelPrepare;
    
		if(IsHasTarget())
		{
			List<BattleCardData> targetList;
			GetTargetList(out targetList);

			string headerText = "";
			switch (LocalizationManager.CurrentLanguage)
			{
    			case LocalizationManager.Language.Thai:
				{
					headerText = this._description_TH;
				}
				break;
    
    			case LocalizationManager.Language.English:
    			default:
				{
					headerText = this._description_EN;
				}
				break;
			}

		    BattleManager.Instance.RequestSelectTarget(
				  headerText
			    , _abilityData.OwnerPlayerIndex
			    , targetList
			    , this._num
			    , new BattleManager.OnFinishSelectTargetCallback(this.OnSelectedTarget)
			    , new BattleManager.OnCancelSelectTargetCallback(this.OnCancelTarget)
                , (BattleManager.Instance.IsHaveTimeout && !this.IsOwnerTurn)
				, false
		    );
            
            return;
		}
        
		OnCancelTarget();
	}

	private void OnSelectedTarget(List<int> selectedTargetIDList)
	{
		_selectedTargetIDList = selectedTargetIDList;
        
        AddParamIntList(_selectedTargetIDList);

		if(_onFinishPrepare != null)
		{
			_onFinishPrepare.Invoke();
		}
	}
    
    private void OnCancelTarget()
    {
        if(_onCancelPrepare != null)
        {
            _onCancelPrepare.Invoke();
        }
    }

	public override void Action(OnFinishAction onFinishAction)
	{
        GetParamIntList(out _selectedTargetIDList);
		if(_selectedTargetIDList != null && _selectedTargetIDList.Count > 0)
		{
            foreach (int battleCardID in _selectedTargetIDList)
            {
                BattleManager.Instance.RequestDestroyBattleCard(
                      battleCardID
                    , CardZoneIndex.Graveyard
                );
            }
        }

        if (onFinishAction != null)
        {
            onFinishAction.Invoke();
        }
    }
	#endregion
}
#endregion

#region SearchZoneDiAlnoUnit30ToFieldShuffleAE Class
public class SearchZoneDiAlnoUnit30ToFieldShuffleAE : SearchZoneToFieldShuffleAE
{
	// AE0056
	#region SearchZoneDiAlnoUnit30ToFieldShuffleAE Properties
	#endregion

	#region SearchZoneDiAlnoUnit30ToFieldShuffleAE Constructors
	public SearchZoneDiAlnoUnit30ToFieldShuffleAE(string id, int sourceZoneIndex, int num, int targetZoneIndex)
		: base(id, sourceZoneIndex, num, targetZoneIndex)
	{
		AbilityEffectDBData effectDBData;
		bool isFoundEffect = AbilityEffectDB.GetData(this.AbilityEffectID, out effectDBData);
		if (isFoundEffect)
		{
			_description_EN = string.Format(effectDBData.Description_EN, LocalizationManager.CardZoneToText(this.SourceZone), this.Num, LocalizationManager.LocalBattleZoneToText(this.TargetField));
			_description_TH = string.Format(effectDBData.Description_TH, LocalizationManager.CardZoneToText(this.SourceZone), this.Num, LocalizationManager.LocalBattleZoneToText(this.TargetField));
		} 
		else
		{
			_description_EN = "";
			_description_TH = "";
		}
	}

	public SearchZoneDiAlnoUnit30ToFieldShuffleAE(AbilityEffectParams abilityEffectParams)
		: this(
			abilityEffectParams.ID
			, int.Parse(abilityEffectParams.Parameters[0])
			, int.Parse(abilityEffectParams.Parameters[1])
			, int.Parse(abilityEffectParams.Parameters[2])
		)
	{
	}
	#endregion

	#region SearchZoneDiAlnoUnit30ToFieldShuffleAE Methods
	protected override bool IsCanBeTarget(UniqueCardData uniqueCard)
	{
		if (uniqueCard != null)
		{
			if (uniqueCard.PlayerCardData.TypeData == CardTypeData.CardType.Unit)
			{
				if (uniqueCard.PlayerCardData.ClanData == CardClanData.CardClanType.DiAlno)
				{
					if (uniqueCard.PlayerCardData.Cost <= 30)
					{
						return true;
					}
				}
			}
		}

		return false;
	}
	#endregion
}
#endregion

#region SearchDeckMutantDroneToFieldShuffleAE Class
public class SearchDeckMutantDroneToFieldShuffleAE : SearchZoneToFieldShuffleAE
{
	// AE0074
	#region SearchDeckMutantDroneToFieldShuffleAE Properties
	#endregion

	#region SearchDeckMutantDroneToFieldShuffleAE Constructors
	public SearchDeckMutantDroneToFieldShuffleAE(string id, int num, int targetZoneIndex)
		: base(id, 0, num, targetZoneIndex)
	{
		AbilityEffectDBData effectDBData;
		bool isFoundEffect = AbilityEffectDB.GetData(this.AbilityEffectID, out effectDBData);
		if (isFoundEffect)
		{
			_description_EN = string.Format(effectDBData.Description_EN, this.Num, LocalizationManager.LocalBattleZoneToText(this.TargetField));
			_description_TH = string.Format(effectDBData.Description_TH, this.Num, LocalizationManager.LocalBattleZoneToText(this.TargetField));
		} 
		else
		{
			_description_EN = "";
			_description_TH = "";
		}
	}

	public SearchDeckMutantDroneToFieldShuffleAE(AbilityEffectParams abilityEffectParams)
		: this(
			abilityEffectParams.ID
			, int.Parse(abilityEffectParams.Parameters[0])
			, int.Parse(abilityEffectParams.Parameters[1])
		)
	{
	}
	#endregion

	#region SearchDeckMutantDroneToFieldShuffleAE Methods
	protected override bool IsCanBeTarget(UniqueCardData uniqueCard)
	{
		if (uniqueCard != null)
		{
			if ((uniqueCard.PlayerCardData.Name_EN).ToLower ().Contains ("mutant drone"))
			{
				return true;
			}
		}

		return false;
	}
	#endregion
}
#endregion

#region OpponentDiscardAE Class
public class OpponentDiscardAE : AbilityEffect
{
	// AE0016
	#region OpponentDiscardAE Properties
	protected int _discardNum = 0;
	private List<int> _selectHandCardIDList = null;

	public int DiscardNum { get { return _discardNum; } }
	#endregion

	#region OpponentDiscardAE Constructors
	public OpponentDiscardAE(string id, int discardNum)
		:base(id)
	{
		_discardNum = discardNum;

		_description_EN = string.Format(_description_EN, this.DiscardNum);
		_description_TH = string.Format(_description_TH, this.DiscardNum);
	}

	public OpponentDiscardAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID, int.Parse(abilityEffectParams.Parameters[0]))
	{
	}
	#endregion

	#region OpponentDiscardAE Methods
	public override bool IsHasTarget()
	{
		//Debug.Log("BackToHandAE IsHasTarget");

		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				return true;
				/*
				if(_abilityData.BattleCard != null)
				{
					if(this._num <= BattleManager.Instance.GetHandCount(_abilityData.BattleCard.OwnerPlayerIndex))
					{
						return true;
					}
				}
				*/
			}
		}

		return false;
	}
		
	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
        _onFinishPrepare = onFinishPrepare;
        _onCancelPrepare = onCancelPrepare;
    
		if(IsHasTarget())
		{
			//Debug.Log("DiscardAE: Has Target");

			PlayerIndex enemyIndex = PlayerIndex.One;
			if (_abilityData.OwnerPlayerIndex == PlayerIndex.One)
			{
				enemyIndex = PlayerIndex.Two;
			} 
			else
			{
				enemyIndex = PlayerIndex.One;
			}

			string headerText;
			switch (LocalizationManager.CurrentLanguage)
			{
				case LocalizationManager.Language.Thai:
				{
					headerText = this._description_TH;
				}
				break;

				case LocalizationManager.Language.English:
				default:
				{
					headerText = this._description_EN;
				}
				break;
			}

			BattleManager.Instance.StartSelectHandCard(
				  headerText
				, enemyIndex
				, this.DiscardNum
				, new BattleManager.OnFinishSelectHandCardCallback(this.OnSelectHandCard)
				, new BattleManager.OnCancelSelectHandCardCallback(this.OnCancelHandCard)
				, null
                , (BattleManager.Instance.IsHaveTimeout)
				, false
			);
            
            return;
		}
		
        OnCancelHandCard();
	}

	private void OnSelectHandCard(List<int> selectHandCardIDList)
	{
		_selectHandCardIDList = selectHandCardIDList;
        
        AddParamIntList(_selectHandCardIDList);

		if(_onFinishPrepare != null)
		{
			_onFinishPrepare.Invoke();
		}
	}
    
    private void OnCancelHandCard()
    {
        if(_onCancelPrepare != null)
        {
            _onCancelPrepare.Invoke();
        }
    }

	public override void Action(OnFinishAction onFinishAction)
	{
        GetParamIntList(out _selectHandCardIDList);
        
		if(_selectHandCardIDList != null && _selectHandCardIDList.Count > 0)
		{
			foreach(int discardID in _selectHandCardIDList)
			{
				BattleManager.Instance.ActionMoveUniqueCard(discardID, CardZoneIndex.Graveyard);
			}
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region AllOpponentDiscardRandomAE
public class AllOpponentDiscardRandomAE : AbilityEffect
{
	// AE0079
	#region AllOpponentDiscardRandomAE Properties
	private int _num;

	protected bool _isCanCancel = true;
	private List<int> _selectHandCardIDList = null;

	public int Num { get { return _num; } }
	#endregion

	#region AllOpponentDiscardRandomAE Constructors
	public AllOpponentDiscardRandomAE(string id, int num)
		: base(id)
	{
		this._num = num;

		_description_EN = string.Format(_description_EN, this.Num);
		_description_TH = string.Format(_description_TH, this.Num);
	}

	public AllOpponentDiscardRandomAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID, int.Parse(abilityEffectParams.Parameters[0]))
	{
	}
	#endregion

	#region AllOpponentDiscardRandomAE Methods
	public override bool IsHasTarget()
	{
		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				return true;
			}
		}

		return false;
	}

	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
        _onFinishPrepare = onFinishPrepare;
        _onCancelPrepare = onCancelPrepare;
    
		if(IsHasTarget())
		{
			_selectHandCardIDList = new List<int>();

			foreach (PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
			{
				if (playerIndex != _abilityData.OwnerPlayerIndex)
				{
					BattleManager.Instance.StartSelectHandCardRandom (
                          _abilityData.OwnerPlayerIndex
					    , playerIndex
						, this._num
						, new BattleManager.OnFinishSelectHandCardCallback (this.OnSelectHandCard)
                        , new BattleManager.OnCancelSelectHandCardCallback (this.OnCancelHandCard)
						, null
					);
				}
			}
            
            AddParamIntList(_selectHandCardIDList);
			if(_onFinishPrepare != null)
			{
				_onFinishPrepare.Invoke();
			}
            
            return;
		}
        
		OnCancelHandCard();
	}

	private void OnSelectHandCard(List<int> selectHandCardIDList)
	{
		_selectHandCardIDList.AddRange(selectHandCardIDList);
	}
    
    private void OnCancelHandCard()
    {
        if(_onCancelPrepare != null)
        {
            _onCancelPrepare.Invoke();
        }
    }

	public override void Action(OnFinishAction onFinishAction)
	{
        GetParamIntList(out _selectHandCardIDList);
    
		if(_selectHandCardIDList != null && _selectHandCardIDList.Count > 0)
		{
			foreach(int discardID in _selectHandCardIDList)
			{
				BattleManager.Instance.ActionMoveUniqueCard(discardID, CardZoneIndex.Graveyard);
			}
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region Me1HP1ATKnockbackWithNoNonMutantUnitAE Class
public class Me1HP1ATKnockbackWithNoNonMutantUnitAE : BuffAE
{
	// AE0058
	#region Me1HP1ATKnockbackWithNoNonMutantUnitAE Properties
	#endregion

	#region Me1HP1ATKnockbackWithNoNonMutantUnitAE Constructors
	public Me1HP1ATKnockbackWithNoNonMutantUnitAE(string id)
		:base(id)
	{
		/*
		_description_EN = string.Format(_description_EN);
		_description_TH = string.Format(_description_TH);
		*/
	}

	public Me1HP1ATKnockbackWithNoNonMutantUnitAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID)
	{
	}
	#endregion

	#region Me1HP1ATKnockbackWithNoNonMutantUnitAE Methods
	public override bool IsHasTarget()
	{
		return true;
	}

	public override bool GetTargetList(out List<BattleCardData> targetList)
	{
		targetList = new List<BattleCardData>();
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			if(playerIndex == _abilityData.OwnerPlayerIndex)
			{
				List<BattleCardData> target;
				BattleManager.Instance.GetTargetList(
					playerIndex
					, out target
					, this.IsCanBeTarget
				);

				if(target != null && target.Count > 0)
				{
					targetList.AddRange(target);
				}
			}
		}

		if(targetList.Count > 0)
		{
			return true;
		}

		targetList = null;
		return false;
	}

	private bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown && !battleCard.IsDead)
			{
				if(battleCard.BattleCardID == _abilityData.BattleCard.BattleCardID)
				{
					return true;
				}
			}
		}

		return false;
	}

	protected override bool IsActive ()
	{
		List<BattleCardData> target;
		BattleManager.Instance.GetTargetList(
			_abilityData.OwnerPlayerIndex
			, out target
			, this.IsMatchCondition
		);

		if (target != null && target.Count > 0)
		{
			return false;
		} 
		else
		{
			return true;
		}

		//return false;
	}

	private bool IsMatchCondition(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown && !battleCard.IsDead)
			{
				if(battleCard.TypeData == CardTypeData.CardType.Unit)
				{
					if(battleCard.ClanData != CardClanData.CardClanType.Mutant)
					{
						return true;
					}
				}
			}
		}

		return false;
	}

	protected override void AddBuff(BattleCardData battleCard)
	{
		PassiveIndex passiveIndex = new PassiveIndex();
		passiveIndex += PassiveIndex.PassiveType.Knockback;

		CardBuff buff = new CardBuff(
			this.AbilityEffectID
			, this._abilityData.BattleCard.BattleCardID
			, battleCard.BattleCardID
			, 0 // shield
			, 1 // hp
			, 1 // attack
			, 0 // speed
			, 0 // move
			, 0 // attack range
			, passiveIndex
			, new List<CardSubTypeData>()
			, new List<CardJobData>()
			, this.OnUnitUpdate
			, -1 // active turn
		);

		BattleManager.Instance.RequestAddBuffBattleCard(battleCard.BattleCardID, buff);
	}
	#endregion
}
#endregion

#region DestroyMeAE Class
public class DestroyMeAE : AbilityEffect
{
	// AE0060
	#region DestroyMeAE Properties
	#endregion

	#region DestroyMeAE Constructors
	public DestroyMeAE(string id)
		: base(id)
	{
		/*
		_description_EN = string.Format(_description_EN);
		_description_TH = string.Format(_description_TH);
		*/
	}

	public DestroyMeAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID)
	{
	}
	#endregion

	#region DestroyMeAE Methods
	public override bool IsHasTarget()
	{
		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				if (this._abilityData.IsSummon)
				{
					return true;
				}
			}
		}

		return false;
	}

	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
		if(onFinishPrepare != null)
		{
			onFinishPrepare.Invoke();
		}
	}

	public override void Action(OnFinishAction onFinishAction)
	{
		if(IsHasTarget())
		{
			BattleManager.Instance.RequestDestroyBattleCard(
				  this._abilityData.BattleCard.BattleCardID
				, CardZoneIndex.Graveyard
			);
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region TargetUnit3HP3ATTurnAE
public class TargetUnit3HP3ATTurnAE : AbilityEffect
{
	// AE0061
	#region TargetUnit3HP3ATTurnAE Properties
	private List<int> _selectedTargetIDList = null;
	#endregion

	#region TargetUnit3HP3ATTurnAE Constructors
	public TargetUnit3HP3ATTurnAE(string id)
		: base(id)
	{
		/*
		_description_EN = string.Format(_description_EN);
		_description_TH = string.Format(_description_TH);
		*/
	}

	public TargetUnit3HP3ATTurnAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID)
	{
	}
	#endregion

	#region TargetUnit3HP3ATTurnAE Methods
	public override bool IsHasTarget()
	{
		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				//if(_abilityData.BattleCard != null && _abilityData.BattleCard.IsActive)
				{
					return true;
				}
			}
		}

		return false;
	}

	public override bool GetTargetList(out List<BattleCardData> targetList)
	{
		targetList = new List<BattleCardData>();
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			//if(playerIndex == _abilityData.OwnerPlayerIndex)
			{
				List<BattleCardData> target;
				BattleManager.Instance.GetTargetList(
					playerIndex
					, out target
					, this.IsCanBeTarget
				);

				if(target != null && target.Count > 0)
				{
					targetList.AddRange(target);
				}
			}
		}

		if(targetList.Count > 0)
		{
			return true;
		}

		targetList = null;
		return false;
	}

	private bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown)
			{
				if(battleCard.TypeData == CardTypeData.CardType.Unit)
				{
					return true;
				}
			}
		}

		return false;
	}

	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
		//Debug.Log("Prepare: TargetUnit3HP3ATTurnAE");
        _onFinishPrepare = onFinishPrepare;
        _onCancelPrepare = onCancelPrepare;

		if(IsHasTarget())
		{
			//Debug.Log("Prepare: TargetUnit3HP3ATTurnAE Has Target");

			List<BattleCardData> targetList;
			GetTargetList(out targetList);

			_onFinishPrepare = onFinishPrepare;

			string headerText = "";
			switch (LocalizationManager.CurrentLanguage)
			{
			case LocalizationManager.Language.Thai:
				{
					headerText = this._description_TH;
				}
				break;

			case LocalizationManager.Language.English:
			default:
				{
					headerText = this._description_EN;
				}
				break;
			}

			//Debug.Log("Prepare: targetList:" + targetList.Count.ToString());

			BattleManager.Instance.RequestSelectTarget(
				headerText
				, _abilityData.OwnerPlayerIndex
				, targetList
				, 1
				, new BattleManager.OnFinishSelectTargetCallback(this.OnSelectedTarget)
				, new BattleManager.OnCancelSelectTargetCallback(this.OnCancelTarget)
                , (BattleManager.Instance.IsHaveTimeout && !this.IsOwnerTurn)
				, true
			);
            return;
		}
        
		OnCancelTarget();
	}

	private void OnSelectedTarget(List<int> selectedTargetIDList)
	{
		//Debug.Log("Prepare: OTargetGetBonusMoveAE nSelectedTarget");

		_selectedTargetIDList = selectedTargetIDList;
        AddParamIntList(_selectedTargetIDList);

		if(_onFinishPrepare != null)
		{
			_onFinishPrepare.Invoke();
		}
	}
    
    private void OnCancelTarget()
    {
        if(_onCancelPrepare != null)
        {
            _onCancelPrepare.Invoke();
        }
    }

	public override void Action(OnFinishAction onFinishAction)
	{
        GetParamIntList(out _selectedTargetIDList);
    
		if(_selectedTargetIDList != null && _selectedTargetIDList.Count > 0)
		{
			foreach(int battleCardID in _selectedTargetIDList)
			{
				BattleCardData battleCard;
				bool isFound = BattleManager.Instance.FindBattleCard(
					  battleCardID
					, out battleCard
				);

				if(isFound)
				{
					CardBuff buff = new CardBuff(
						this.AbilityEffectID
						, -1
						, battleCardID
						, 0 // shield
						, 3 // hp
						, 3 // attack
						, 0 // speed
						, 0 // move
						, 0 // attack range
						, new PassiveIndex()
						, new List<CardSubTypeData>()
						, new List<CardJobData>()
						, null
						, 1 // active turn
					);

					BattleManager.Instance.RequestAddBuffBattleCard(battleCardID, buff);
				}
			}
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region DealDamageTargetXSameFieldAE Class
public class DealDamageTargetXSameFieldAE : AbilityEffect
{
	// AE0003
	#region DealDamageTargetXSameFieldAE Properties
	protected bool _isPiercing;

	protected List<int> _selectedTargetIDList = null;
	protected OnFinishPrepare _onFinishPrepare = null;

	public bool IsPiercing { get { return _isPiercing; } }
	#endregion

	#region DealDamageTargetXSameFieldAE Constructors
	public DealDamageTargetXSameFieldAE(string id, int isPiercing)
		: base(id)
	{
		this._isPiercing = ((isPiercing > 0) ? true : false);

		_description_EN = string.Format(_description_EN, GetPiercingText(IsPiercing));
		_description_TH = string.Format(_description_TH, GetPiercingText(IsPiercing));
	}

	public DealDamageTargetXSameFieldAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID, int.Parse(abilityEffectParams.Parameters[0]))
	{
	}
	#endregion

	#region DealDamageTargetXSameFieldAE Methods
	public override bool IsHasTarget()
	{
		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
				{
					bool isHasTarget = BattleManager.Instance.CheckHasTargetList(
						playerIndex
						, this.IsCanBeTarget
					);

					if(isHasTarget) return true;
				}
			}
		}

		return false;
	}

	public override bool GetTargetList(out List<BattleCardData> targetList)
	{
		targetList = new List<BattleCardData>();
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			List<BattleCardData> target;
			BattleManager.Instance.GetTargetList(
				playerIndex
				, out target
				, this.IsCanBeTarget
			);

			if(target != null && target.Count > 0)
			{
				targetList.AddRange(target);
			}
		}

		if(targetList.Count > 0)
		{
			return true;
		}

		targetList = null;
		return false;
	}

	protected virtual bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown && !battleCard.IsDead)
			{
				if (battleCard.TypeData == CardTypeData.CardType.Unit)
				{
					return true;
				}
			}
		}

		return false;
	}

	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
		/*
        if(_abilityData.IsSummon)
        {
            Debug.Log("DealDamageAE: Prepare " + _abilityData.BattleCard.Name);
        }
        else
        {
            Debug.Log("DealDamageAE: Prepare " + _abilityData.UniqueCard.PlayerCardData.Name);
        }
        */
        
        _onFinishPrepare = onFinishPrepare;
        _onCancelPrepare = onCancelPrepare;

		if(IsHasTarget())
		{
			List<BattleCardData> targetList;
			GetTargetList(out targetList);

			string headerText = "";
			switch (LocalizationManager.CurrentLanguage)
			{
			case LocalizationManager.Language.Thai:
				{
					headerText = this._description_TH;
				}
				break;

			case LocalizationManager.Language.English:
			default:
				{
					headerText = this._description_EN;
				}
				break;
			}

			BattleManager.Instance.RequestSelectTarget(
				  headerText
				, _abilityData.OwnerPlayerIndex
				, targetList
				, 1
				, new BattleManager.OnFinishSelectTargetCallback(this.OnSelectedTarget)
				, new BattleManager.OnCancelSelectTargetCallback(this.OnCancelTarget)
                , (BattleManager.Instance.IsHaveTimeout && !this.IsOwnerTurn)
				, false
			);
            
            return;
		}
        
		OnCancelTarget();
	}

	protected virtual void OnSelectedTarget(List<int> selectedTargetIDList)
	{
		_selectedTargetIDList = selectedTargetIDList;
        AddParamIntList(_selectedTargetIDList);

		if(_onFinishPrepare != null)
		{
			_onFinishPrepare.Invoke();
		}
	}
    
    protected virtual void OnCancelTarget()
    {
        if(_onCancelPrepare != null)
        {
            _onCancelPrepare.Invoke();
        }
    }

	private bool IsSameField(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown && !battleCard.IsDead)
			{
				if (battleCard.TypeData == CardTypeData.CardType.Unit)
				{
					if (_selectedTargetIDList != null && _selectedTargetIDList.Count > 0)
					{
						foreach (int battleCardID in _selectedTargetIDList)
						{
							BattleCardData selectedCard = null;
							BattleManager.Instance.FindBattleCard (battleCardID, out selectedCard);

							if (battleCard.CurrentZone == selectedCard.CurrentZone)
							{
								return true;
							}
						}
					}
				}
			}
		}

		return false;
	}

	public override void Action(OnFinishAction onFinishAction)
	{
		int damage = 0;

		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			List<BattleCardData> target;
			BattleManager.Instance.GetTargetList(
				playerIndex
				, out target
				, this.IsSameField
			);

			if(target != null && target.Count > 0)
			{
				damage += target.Count;
			}
		}

        GetParamIntList(out _selectedTargetIDList);
		if(_selectedTargetIDList != null && _selectedTargetIDList.Count > 0)
		{
			foreach(int battleCardID in _selectedTargetIDList)
			{
				BattleManager.Instance.RequestDealDamageBattleCard(
					  battleCardID
					, damage
					, this.IsPiercing
				);
			}
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region DealDamageAllUnitAE Class
public class DealDamageAllUnitAE : AbilityEffect
{
	// AE0063
	#region DealDamageAllUnitAE Properties
	protected int _damage;
	protected bool _isPiercing;

	public int Damage { get { return _damage; } }
	public bool IsPiercing { get { return _isPiercing; } }
	#endregion

	#region DealDamageAllUnitAE Constructors
	public DealDamageAllUnitAE(string id, int damage, int isPiercing)
		:base (id)
	{
		this._damage = damage;
		this._isPiercing = ((isPiercing > 0) ? true : false);

		_description_EN = string.Format(_description_EN, this.Damage, GetPiercingText(IsPiercing));
		_description_TH = string.Format(_description_TH, this.Damage, GetPiercingText(IsPiercing));
	}

	public DealDamageAllUnitAE(AbilityEffectParams abilityEffectParams)
		: this(
			  abilityEffectParams.ID
			, int.Parse(abilityEffectParams.Parameters[0])
			, int.Parse(abilityEffectParams.Parameters[1])
		)
	{
	}
	#endregion

	#region DealDamageAllUnitAE Methods
	public override bool IsHasTarget()
	{
		if (BattleManager.Instance != null)
		{
			if (this._abilityData != null)
			{

				if (this._abilityData.IsSummon)
				{
					// Unit or Structure Ability
					return true;
				}
				else
				{
					// Event Ability
					/*
                    List<BattleCardData> targetList = new List<BattleCardData>();
                    bool isFound = GetTargetList(out targetList);
                    if (isFound) return true;
					*/

					return true;
				}
			}
		}

		return false;
	}

	public override bool GetTargetList(out List<BattleCardData> targetList)
	{
		targetList = new List<BattleCardData>();
		foreach (PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			List<BattleCardData> target;
			BattleManager.Instance.GetTargetList(
				playerIndex
				, out target
				, this.IsCanBeTarget
			);

			if (target != null && target.Count > 0)
			{
				targetList.AddRange(target);
			}
		}

		if (targetList.Count > 0)
		{
			return true;
		}

		targetList = null;
		return false;
	}

	protected virtual bool IsCanBeTarget(BattleCardData battleCard)
	{
		if (battleCard != null)
		{
			if (!battleCard.IsFaceDown && !battleCard.IsDead)
			{
				if (battleCard.TypeData == CardTypeData.CardType.Unit)
				{
					if (battleCard.CurrentZone == BattleZoneIndex.Battlefield)
					{
						return true;
					}
				}
			}
		}

		return false;
	}

	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
		if (IsHasTarget())
		{
			if (onFinishPrepare != null)
			{
				onFinishPrepare.Invoke();
			}
		}
		else
		{
			if (onCancelPrepare != null)
			{
				onCancelPrepare.Invoke();
			}
		}
	}

	public override void Action(OnFinishAction onFinishAction)
	{
		if (IsHasTarget())
		{
			List<BattleCardData> targetList;
			GetTargetList(out targetList);

			if (targetList != null && targetList.Count > 0)
			{
				foreach (BattleCardData battleCard in targetList)
				{
					BattleManager.Instance.RequestDealDamageBattleCard(
						battleCard.BattleCardID
						, this.Damage
						, this.IsPiercing
					);
				}
			}
		}

		if (onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region TargetBaseGetHPPerUnitAE
public class TargetBaseGetHPPerUnitAE : AbilityEffect
{
	// AE0061
	#region TargetBaseGetHPPerUnitAE Properties
	protected int _hpPerUnit = 0;

	private List<int> _selectedTargetIDList = null;

	public int HPPerUnit { get { return _hpPerUnit; } }
	#endregion

	#region TargetBaseGetHPPerUnitAE Constructors
	public TargetBaseGetHPPerUnitAE(string id, int hpPerUnit)
		: base(id)
	{
		this._hpPerUnit = hpPerUnit;

		_description_EN = string.Format(_description_EN, this.HPPerUnit);
		_description_TH = string.Format(_description_TH, this.HPPerUnit);
	}

	public TargetBaseGetHPPerUnitAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID, int.Parse(abilityEffectParams.Parameters[0]))
	{
	}
	#endregion

	#region TargetBaseGetHPPerUnitAE Methods
	public override bool IsHasTarget()
	{
		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				//if(_abilityData.BattleCard != null && _abilityData.BattleCard.IsActive)
				{
					return true;
				}
			}
		}

		return false;
	}

	public override bool GetTargetList(out List<BattleCardData> targetList)
	{
		targetList = new List<BattleCardData>();
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			//if(playerIndex == _abilityData.OwnerPlayerIndex)
			{
				List<BattleCardData> target;
				BattleManager.Instance.GetTargetList(
					playerIndex
					, out target
					, this.IsCanBeTarget
				);

				if(target != null && target.Count > 0)
				{
					targetList.AddRange(target);
				}
			}
		}

		if(targetList.Count > 0)
		{
			return true;
		}

		targetList = null;
		return false;
	}

	private bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown)
			{
				if(battleCard.TypeData == CardTypeData.CardType.Base)
				{
					return true;
				}
			}
		}

		return false;
	}

	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
        _onFinishPrepare = onFinishPrepare;
        _onCancelPrepare = onCancelPrepare;
    
		if(IsHasTarget())
		{
			List<BattleCardData> targetList;
			GetTargetList(out targetList);

			string headerText = "";
			switch (LocalizationManager.CurrentLanguage)
			{
			    case LocalizationManager.Language.Thai:
				{
					headerText = this._description_TH;
				}
				break;

    			case LocalizationManager.Language.English:
    			default:
				{
					headerText = this._description_EN;
				}
				break;
			}

			//Debug.Log("Prepare: targetList:" + targetList.Count.ToString());

			BattleManager.Instance.RequestSelectTarget(
				headerText
				, _abilityData.OwnerPlayerIndex
				, targetList
				, 1
				, new BattleManager.OnFinishSelectTargetCallback(this.OnSelectedTarget)
				, new BattleManager.OnCancelSelectTargetCallback(this.OnCancelTarget)
                , (BattleManager.Instance.IsHaveTimeout && !this.IsOwnerTurn)
				, true
			);
            
            return;
		}
        
		OnCancelTarget();
	}

	private void OnSelectedTarget(List<int> selectedTargetIDList)
	{
		//Debug.Log("Prepare: OTargetGetBonusMoveAE nSelectedTarget");

		_selectedTargetIDList = selectedTargetIDList;
        AddParamIntList(_selectedTargetIDList);

		if(_onFinishPrepare != null)
		{
			_onFinishPrepare.Invoke();
		}
	}
    
    private void OnCancelTarget()
    {
        if(_onCancelPrepare != null)
        {
            _onCancelPrepare.Invoke();
        }
    }


	private bool IsMyUnit(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown && !battleCard.IsDead)
			{
				if(battleCard.TypeData == CardTypeData.CardType.Unit)
				{
					if (battleCard.OwnerPlayerIndex == this._abilityData.OwnerPlayerIndex)
					{
						return true;
					}
				}
			}
		}

		return false;
	}

	public override void Action(OnFinishAction onFinishAction)
	{
		// Count owned unit.
		int count = 0;

		List<BattleCardData> target;
		BattleManager.Instance.GetTargetList(
			  _abilityData.OwnerPlayerIndex
			, out target
			, this.IsMyUnit
		);

		if(target != null && target.Count > 0)
		{
			count += target.Count;
		}
			
        GetParamIntList(out _selectedTargetIDList);
		if(_selectedTargetIDList != null && _selectedTargetIDList.Count > 0)
		{
			foreach(int battleCardID in _selectedTargetIDList)
			{
				BattleCardData battleCard;
				bool isFound = BattleManager.Instance.FindBattleCard(
					  battleCardID
					, out battleCard
				);

				if(isFound)
				{
					CardBuff buff = new CardBuff(
						this.AbilityEffectID
						, -1
						, battleCardID
						, 0 // shield
						, (count * this.HPPerUnit) // hp
						, 0 // attack
						, 0 // speed
						, 0 // move
						, 0 // attack range
						, new PassiveIndex()
						, new List<CardSubTypeData>()
						, new List<CardJobData>()
						, null
						, -1 // active turn
					);

					BattleManager.Instance.RequestAddBuffBattleCard(battleCardID, buff);
				}
			}
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region TargetSkiraUnitGetHPTurnAE
public class TargetSkiraUnitGetHPTurnAE : AbilityEffect
{
	// AE0065
	#region TargetSkiraUnitGetHPTurnAE Properties
	protected int _hp;

	private List<int> _selectedTargetIDList = null;

	public int HP { get { return _hp; } }
	#endregion

	#region TargetSkiraUnitGetHPTurnAE Constructors
	public TargetSkiraUnitGetHPTurnAE(string id, int hp)
		: base(id)
	{
		_hp = hp;

		_description_EN = string.Format(_description_EN, this.HP);
		_description_TH = string.Format(_description_TH, this.HP);
	}

	public TargetSkiraUnitGetHPTurnAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID, int.Parse(abilityEffectParams.Parameters[0]))
	{
	}
	#endregion

	#region TargetSkiraUnitGetHPTurnAE Methods
	public override bool IsHasTarget()
	{
		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				List<BattleCardData> targetList;
				GetTargetList (out targetList);

				if(targetList != null && targetList.Count > 0)
				{
					return true;
				}
			}
		}

		return false;
	}

	public override bool GetTargetList(out List<BattleCardData> targetList)
	{
		targetList = new List<BattleCardData>();
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			//if(playerIndex == _abilityData.OwnerPlayerIndex)
			{
				List<BattleCardData> target;
				BattleManager.Instance.GetTargetList(
					playerIndex
					, out target
					, this.IsCanBeTarget
				);

				if(target != null && target.Count > 0)
				{
					targetList.AddRange(target);
				}
			}
		}

		if(targetList.Count > 0)
		{
			return true;
		}

		targetList = null;
		return false;
	}

	private bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown && !battleCard.IsDead)
			{
				if(battleCard.TypeData == CardTypeData.CardType.Unit)
				{
                    if (battleCard.IsCardSubType(CardSubTypeData.CardSubType.Skira))
                    {
                        return true;
                    }
				}
			}
		}

		return false;
	}

	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
        _onFinishPrepare = onFinishPrepare;
        _onCancelPrepare = onCancelPrepare;
    
		if(IsHasTarget())
		{
			List<BattleCardData> targetList;
			GetTargetList(out targetList);

			string headerText = "";
			switch (LocalizationManager.CurrentLanguage)
			{
				case LocalizationManager.Language.Thai:
				{
					headerText = this._description_TH;
				}
				break;

				case LocalizationManager.Language.English:
				default:
				{
					headerText = this._description_EN;
				}
				break;
			}

			//Debug.Log("Prepare: targetList:" + targetList.Count.ToString());

			BattleManager.Instance.RequestSelectTarget(
				headerText
				, _abilityData.OwnerPlayerIndex
				, targetList
				, 1
				, new BattleManager.OnFinishSelectTargetCallback(this.OnSelectedTarget)
				, new BattleManager.OnCancelSelectTargetCallback(this.OnCancelTarget)
                , (BattleManager.Instance.IsHaveTimeout && !this.IsOwnerTurn)
				, true
			);
            
            return;
		}
        
		OnCancelTarget();
	}

	private void OnSelectedTarget(List<int> selectedTargetIDList)
	{
		//Debug.Log("Prepare: OTargetGetBonusMoveAE nSelectedTarget");

		_selectedTargetIDList = selectedTargetIDList;
        AddParamIntList(_selectedTargetIDList);

		if(_onFinishPrepare != null)
		{
			_onFinishPrepare.Invoke();
		}
	}
    
    private void OnCancelTarget()
    {
        if(_onCancelPrepare != null)
        {
            _onCancelPrepare.Invoke();
        }
    }

	public override void Action(OnFinishAction onFinishAction)
	{
        GetParamIntList(out _selectedTargetIDList);
		if(_selectedTargetIDList != null && _selectedTargetIDList.Count > 0)
		{
			foreach(int battleCardID in _selectedTargetIDList)
			{
				BattleCardData battleCard;
				bool isFound = BattleManager.Instance.FindBattleCard(
					  battleCardID
					, out battleCard
				);

				if(isFound)
				{
					CardBuff buff = new CardBuff(
						this.AbilityEffectID
						, -1
						, battleCardID
						, 0 // shield
						, this.HP // hp
						, 0 // attack
						, 0 // speed
						, 0 // move
						, 0 // attack range
						, new PassiveIndex()
						, new List<CardSubTypeData>()
						, new List<CardJobData>()
						, null
						, 1 // active turn
					);

					BattleManager.Instance.RequestAddBuffBattleCard(battleCardID, buff);
				}
			}
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region TargetMutantUnitGet2AT1SPTurnAE
public class TargetMutantUnitGet2AT1SPTurnAE : AbilityEffect
{
	// AE0066
	#region TargetMutantUnitGet2AT1SPTurnAE Properties
	private List<int> _selectedTargetIDList = null;
	#endregion

	#region TargetMutantUnitGet2AT1SPTurnAE Constructors
	public TargetMutantUnitGet2AT1SPTurnAE(string id)
		: base(id)
	{
		/*
		_description_EN = string.Format(_description_EN);
		_description_TH = string.Format(_description_TH);
		*/
	}

	public TargetMutantUnitGet2AT1SPTurnAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID)
	{
	}
	#endregion

	#region TargetMutantUnitGet2AT1SPTurnAE Methods
	public override bool IsHasTarget()
	{
		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				List<BattleCardData> targetList;
				GetTargetList (out targetList);

				if(targetList != null && targetList.Count > 0)
				{
					return true;
				}
			}
		}

		return false;
	}

	public override bool GetTargetList(out List<BattleCardData> targetList)
	{
		targetList = new List<BattleCardData>();
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			//if(playerIndex == _abilityData.OwnerPlayerIndex)
			{
				List<BattleCardData> target;
				BattleManager.Instance.GetTargetList(
					playerIndex
					, out target
					, this.IsCanBeTarget
				);

				if(target != null && target.Count > 0)
				{
					targetList.AddRange(target);
				}
			}
		}

		if(targetList.Count > 0)
		{
			return true;
		}

		targetList = null;
		return false;
	}

	private bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown && !battleCard.IsDead)
			{
				if (battleCard.ClanData == CardClanData.CardClanType.Mutant)
				{
					if (battleCard.TypeData == CardTypeData.CardType.Unit)
					{
						return true;
					}
				}
			}
		}

		return false;
	}

	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
        _onFinishPrepare = onFinishPrepare;
        _onCancelPrepare = onCancelPrepare;
    
		if(IsHasTarget())
		{
			List<BattleCardData> targetList;
			GetTargetList(out targetList);

			string headerText = "";
			switch (LocalizationManager.CurrentLanguage)
			{
				case LocalizationManager.Language.Thai:
				{
					headerText = this._description_TH;
				}
				break;

				case LocalizationManager.Language.English:
				default:
				{
					headerText = this._description_EN;
				}
				break;
			}

			//Debug.Log("Prepare: targetList:" + targetList.Count.ToString());

			BattleManager.Instance.RequestSelectTarget(
				headerText
				, _abilityData.OwnerPlayerIndex
				, targetList
				, 1
				, new BattleManager.OnFinishSelectTargetCallback(this.OnSelectedTarget)
				, new BattleManager.OnCancelSelectTargetCallback(this.OnCancelTarget)
                , (BattleManager.Instance.IsHaveTimeout && !this.IsOwnerTurn)
				, true
			);
            return;
		}
        
		OnCancelTarget();
	}

	private void OnSelectedTarget(List<int> selectedTargetIDList)
	{
		_selectedTargetIDList = selectedTargetIDList;
        AddParamIntList(_selectedTargetIDList);

		if(_onFinishPrepare != null)
		{
			_onFinishPrepare.Invoke();
		}
	}
    
    private void OnCancelTarget()
    {
        if(_onCancelPrepare != null)
        {
            _onCancelPrepare.Invoke();
        }
    }

	public override void Action(OnFinishAction onFinishAction)
	{
        GetParamIntList(out _selectedTargetIDList);
		if(_selectedTargetIDList != null && _selectedTargetIDList.Count > 0)
		{
			foreach(int battleCardID in _selectedTargetIDList)
			{
				BattleCardData battleCard;
				bool isFound = BattleManager.Instance.FindBattleCard(
					battleCardID
					, out battleCard
				);

				if(isFound)
				{
					CardBuff buff = new CardBuff(
						this.AbilityEffectID
						, -1
						, battleCardID
						, 0 // shield
						, 0 // hp
						, 2 // attack
						, 1 // speed
						, 0 // move
						, 0 // attack range
						, new PassiveIndex()
						, new List<CardSubTypeData>()
						, new List<CardJobData>()
						, null
						, 1 // active turn
					);

					BattleManager.Instance.RequestAddBuffBattleCard(battleCardID, buff);
				}
			}
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region PutWeakCounterTargetUnitRangeAE
public class PutWeakCounterTargetUnitRangeAE : AbilityEffect
{
	// AE0067
	#region PutWeakCounterTargetUnitRangeAE Properties
	protected int _rangeIndex;

	private List<int> _selectedTargetIDList = null;

	public int RangeIndex { get { return _rangeIndex; } }
	#endregion

	#region PutWeakCounterTargetUnitRangeAE Constructors
	public PutWeakCounterTargetUnitRangeAE(string id, int rangeIndex)
		: base(id)
	{
		this._rangeIndex = rangeIndex;

		_description_EN = string.Format(_description_EN, AbilityEffect.RangeIndexToText(this.RangeIndex));
		_description_TH = string.Format(_description_TH, AbilityEffect.RangeIndexToText(this.RangeIndex));
	}

	public PutWeakCounterTargetUnitRangeAE(AbilityEffectParams abilityEffectParams)
		: this(
			abilityEffectParams.ID
			, int.Parse(abilityEffectParams.Parameters[0])
		)
	{
	}
	#endregion

	#region PutWeakCounterTargetUnitRangeAE Methods
	public override bool IsHasTarget()
	{
		if(BattleManager.Instance != null)
		{
			if(_abilityData != null)
			{
				List<BattleCardData> targetList;
				GetTargetList (out targetList);

				if(targetList != null && targetList.Count > 0)
				{
					return true;
				}
			}
		}

		return false;
	}

	public override bool GetTargetList(out List<BattleCardData> targetList)
	{
		targetList = new List<BattleCardData>();
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			List<BattleCardData> target;
			BattleManager.Instance.GetTargetList(
				  playerIndex
				, out target
				, this.IsCanBeTarget
			);

			if(target != null && target.Count > 0)
			{
				targetList.AddRange(target);
			}
		}

		if(targetList.Count > 0)
		{
			return true;
		}

		targetList = null;
		return false;
	}

	protected virtual bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null && _abilityData.IsSummon)
		{
			if(!battleCard.IsFaceDown && !battleCard.IsDead)
			{
				if (battleCard.TypeData == CardTypeData.CardType.Unit)
				{
                    if (AbilityEffect.IsInRange(_abilityData.BattleCard, battleCard, this.RangeIndex))
                    {
                        return true;
                    }
				}
			}
		}

		return false;
	}

	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
        _onFinishPrepare = onFinishPrepare;
        _onCancelPrepare = onCancelPrepare;
    
		if(IsHasTarget())
		{
			List<BattleCardData> targetList;
			GetTargetList(out targetList);

			if(targetList != null && targetList.Count > 0)
			{
				string headerText = "";
				switch (LocalizationManager.CurrentLanguage)
				{
					case LocalizationManager.Language.Thai:
					{
						headerText = this._description_TH;
					}
					break;

					case LocalizationManager.Language.English:
					default:
					{
						headerText = this._description_EN;
					}
					break;
				}

				//Debug.Log("Prepare: targetList:" + targetList.Count.ToString());

				BattleManager.Instance.RequestSelectTarget(
					  headerText
					, _abilityData.OwnerPlayerIndex
					, targetList
					, 1
					, new BattleManager.OnFinishSelectTargetCallback(this.OnSelectedTarget)
					, new BattleManager.OnCancelSelectTargetCallback(this.OnCancelTarget)
                    , (BattleManager.Instance.IsHaveTimeout && !this.IsOwnerTurn)
					, true
				);
			}
            return;
		}
        
		OnCancelTarget();
	}

	private void OnSelectedTarget(List<int> selectedTargetIDList)
	{
		_selectedTargetIDList = selectedTargetIDList;
        AddParamIntList(_selectedTargetIDList);

		if(_onFinishPrepare != null)
		{
			_onFinishPrepare.Invoke();
		}
	}
    
    private void OnCancelTarget()
    {
        if(_onCancelPrepare != null)
        {
            _onCancelPrepare.Invoke();
        }
    }

	public override void Action(OnFinishAction onFinishAction)
	{
        GetParamIntList(out _selectedTargetIDList);
		if(_selectedTargetIDList != null && _selectedTargetIDList.Count > 0)
		{
			foreach(int battleCardID in _selectedTargetIDList)
			{
				BattleCardData battleCard;
				bool isFound = BattleManager.Instance.FindBattleCard(
					battleCardID
					, out battleCard
				);

				if(isFound)
				{
					CardBuff buff = new CardBuff(
						this.AbilityEffectID
						, -1
						, battleCardID
						, 0 // shield
						, -1 // hp
						, -1 // attack
						, 0 // speed
						, 0 // move
						, 0 // attack range
						, new PassiveIndex()
						, new List<CardSubTypeData>()
						, new List<CardJobData>()
						, null
						, -1 // active turn
					);

					BattleManager.Instance.RequestAddBuffBattleCard(battleCardID, buff);
				}
			}
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region GetGoldByMyWorkerAE Class
public class GetGoldByMyWorkerAE : AbilityEffect
{
	// AE0068
	#region GetGoldByMyWorkerAE Properties
	private int _goldPerUnit = 0;

	public int GoldPerUnit
	{
		get { return _goldPerUnit; }
	}
	#endregion

	#region GetGoldByMyWorkerAE Constructors
	public GetGoldByMyWorkerAE(string id, int goldPerUnit)
		: base(id)
	{
		this._goldPerUnit = goldPerUnit;

		_description_EN = string.Format(_description_EN, this.GoldPerUnit);
		_description_TH = string.Format(_description_TH, this.GoldPerUnit);
	}

	public GetGoldByMyWorkerAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID, int.Parse(abilityEffectParams.Parameters[0]))
	{
	}
	#endregion

	#region GetGoldByMyWorkerAE Methods
	public override bool IsHasTarget()
	{
		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				return true;
			}
		}

		return false;
	}

	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
		if(onFinishPrepare != null)
		{
			onFinishPrepare.Invoke();
		}
	}

	public override void Action(OnFinishAction onFinishAction)
	{
		if(IsHasTarget())
		{
			int count = 0;

			List<BattleCardData> targetList = null;
			GetTargetList (out targetList);
			if (targetList != null && targetList.Count > 0)
			{
				count += targetList.Count;
			}
				
			BattleManager.Instance.RequestGetGold(
				_abilityData.OwnerPlayerIndex
				, count * this.GoldPerUnit
			);
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}

	public override bool GetTargetList(out List<BattleCardData> targetList)
	{
		targetList = new List<BattleCardData>();

		List<BattleCardData> target;
		BattleManager.Instance.GetTargetList(
			  _abilityData.OwnerPlayerIndex
			, out target
			, this.IsMyWorker
		);

		if(target != null && target.Count > 0)
		{
			targetList.AddRange(target);
		}
			
		if(targetList.Count > 0)
		{
			return true;
		}

		targetList = null;
		return false;
	}

	private bool IsMyWorker(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown && !battleCard.IsDead)
			{
				if (battleCard.TypeData == CardTypeData.CardType.Unit)
				{
                    if (battleCard.IsCardJobType(CardJobData.JobType.Worker))
                    {
                        return true;
                    }
				}
			}
		}

		return false;
	}
	#endregion
}
#endregion

#region DestroyTargetNonLeaderCostAE Class
public class DestroyTargetNonLeaderCostAE : DestroyTargetAE
{
	// AE0020
	#region DestroyTargetNonLeaderCostAE Properties
	protected int _cost;

	public int Cost { get { return _cost; } }
	#endregion

	#region DestroyTargetNonLeaderCostAE Constructors
	public DestroyTargetNonLeaderCostAE(string id, int cost)
		: base(id, 1, (int)CardZoneIndex.Graveyard)
	{
		this._cost = cost;

		AbilityEffectDBData abilityEffectDBData;
		bool isFound = AbilityEffectDB.GetData (this.AbilityEffectID, out abilityEffectDBData);
		if (isFound)
		{
			_description_EN = string.Format (abilityEffectDBData.Description_EN, this.Cost);
			_description_TH = string.Format (abilityEffectDBData.Description_TH, this.Cost);
		} 
		else
		{
			_description_EN = "";
			_description_TH = "";
		}

		_isCanCancel = true;
	}

	public DestroyTargetNonLeaderCostAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID, int.Parse(abilityEffectParams.Parameters[0]))
	{
	}
	#endregion

	#region DestroyTargetNonLeaderCostAE Methods
	protected override bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown && !battleCard.IsDead)
			{
				//if (battleCard.OwnerPlayerIndex == _abilityData.OwnerPlayerIndex)
				{
					if (battleCard.TypeData == CardTypeData.CardType.Unit)
					{
						if (battleCard.Cost <= this.Cost)
						{
							if (!battleCard.IsLeader)
							{
								return true;
							}
						}
					}
				}
			}
		}

		return false;
	}
	#endregion
}
#endregion

#region ReturnAllNonLeaderSameFieldToOwnerHandAE
public class ReturnAllNonLeaderSameFieldToOwnerHandAE : AbilityEffect
{
	// AE0071
	#region ReturnAllNonLeaderSameFieldToOwnerHandAE Properties
	#endregion

	#region ReturnAllNonLeaderSameFieldToOwnerHandAE Constructors
	public ReturnAllNonLeaderSameFieldToOwnerHandAE(string id)
		: base(id)
	{
		/*
		_description_EN = string.Format(_description_EN, this.Num);
		_description_TH = string.Format(_description_TH, this.Num);
		*/
	}

	public ReturnAllNonLeaderSameFieldToOwnerHandAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID)
	{
	}
	#endregion

	#region ReturnAllNonLeaderSameFieldToOwnerHandAE Methods
	public override bool IsHasTarget()
	{
		//Debug.Log("BackToHandAE IsHasTarget");

		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				if (this._abilityData.IsSummon)
				{
					return true;
				} 
			}
		}

		return false;
	}

	public override bool GetTargetList(out List<BattleCardData> targetList)
	{
		targetList = new List<BattleCardData>();
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			//if(playerIndex != _abilityData.OwnerPlayerIndex)
			{
				List<BattleCardData> target;
				BattleManager.Instance.GetTargetList(
					playerIndex
					, out target
					, this.IsCanBeTarget
				);

				if(target != null && target.Count > 0)
				{
					targetList.AddRange(target);
				}
			}
		}

		if(targetList.Count > 0)
		{
			return true;
		}

		targetList = null;
		return false;
	}

	private bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown && !battleCard.IsDead)
			{
				if(battleCard.TypeData == CardTypeData.CardType.Unit)
				{
					if(!battleCard.IsLeader)
					{
						if(battleCard.CurrentZone == _abilityData.BattleCard.CurrentZone)
						{
							if (battleCard.BattleCardID != _abilityData.BattleCard.BattleCardID)
							{
								return true;
							}
						}
					}
				}
			}
		}

		return false;
	}

	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
		if(IsHasTarget())
		{
			if(onFinishPrepare != null)
			{
				onFinishPrepare.Invoke();
			}
		}
		else
		{
			if(onCancelPrepare != null)
			{
				onCancelPrepare.Invoke();
			}
		}
	}
		
	public override void Action(OnFinishAction onFinishAction)
	{
		List<BattleCardData> targetList;
		GetTargetList(out targetList);

		if(targetList != null && targetList.Count > 0)
		{
			foreach(BattleCardData battleCard in targetList)
			{
				BattleManager.Instance.RequestDestroyBattleCard(
					  battleCard.BattleCardID
					, CardZoneIndex.Hand
				);
			}
		}

		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion

#region MeGet1HP1ATEachResourceInGaveyardAE
public class MeGet1HP1ATEachResourceInGaveyardAE : BuffAE
{
	// AE0075
	#region MeGet1HP1ATEachResourceInGaveyardAE Properties
	protected int m_cardCount = 0;
	#endregion

	#region MeGet1HP1ATEachResourceInGaveyardAE Constructors
	public MeGet1HP1ATEachResourceInGaveyardAE(string id)
		:base(id)
	{
		/*
		_description_EN = string.Format(_description_EN);
		_description_TH = string.Format(_description_TH);
		*/
	}

	public MeGet1HP1ATEachResourceInGaveyardAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID)
	{
	}
	#endregion

	#region MeGet1HP1ATEachResourceInGaveyardAE Methods
	public override void Init(AbilityData abilityData)
	{
		base.Init(abilityData);

		if(BattleManager.Instance != null && _abilityData != null)
		{
			if(_abilityData.IsSummon)
			{
				BattleManager.Instance.BindBattleEventTrigger(
					  this.OnUnitUpdate
					, _abilityData.OwnerPlayerIndex
					, BattleEventTrigger.BattleTriggerType.PLAYER_CARD_TO_GRAVEYARD
					, BattleEventTrigger.BattleSide.Friendly
				);
			}
		}
	}

	public override bool IsHasTarget()
	{
		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				if (this._abilityData.IsSummon)
				{
					return true;
				} 
			}
		}

		return false;
	}

	public override bool GetTargetList(out List<BattleCardData> targetList)
	{
		targetList = new List<BattleCardData>();
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			if(playerIndex == _abilityData.OwnerPlayerIndex)
			{
				List<BattleCardData> target;
				BattleManager.Instance.GetTargetList(
					playerIndex
					, out target
					, this.IsCanBeTarget
				);

				if(target != null && target.Count > 0)
				{
					targetList.AddRange(target);
				}
			}
		}

		if(targetList.Count > 0)
		{
			return true;
		}

		targetList = null;
		return false;
	}

	private bool IsCanBeTarget(BattleCardData battleCard)
	{
		if(battleCard != null)
		{
			if(!battleCard.IsFaceDown && !battleCard.IsDead)
			{
				if(battleCard.BattleCardID == _abilityData.BattleCard.BattleCardID)
				{
					return true;
				}
			}
		}

		return false;
	}

	protected override bool IsActive ()
	{
        List<UniqueCardData> target;
		BattleManager.Instance.GetTargetList(
			  _abilityData.OwnerPlayerIndex
			, CardZoneIndex.Graveyard
			, out target
			, this.IsMatchCondition
		);

		if (target != null && target.Count > 0)
		{
			m_cardCount = target.Count;
			return true;
		} 
		else
		{
			m_cardCount = 0;
		}

		return false;
	}

	private bool IsMatchCondition(UniqueCardData uniqueCard)
	{
		if(uniqueCard != null)
		{
            if (uniqueCard.IsSubType(CardSubTypeData.CardSubType.Resource))
            {
                return true;
            }
		}

		return false;
	}

	protected override void OnUnitUpdate()
	{
        //Debug.Log ("MeGet1HP1ATEachResourceInGaveyardAE: OnUnitUpdate");

        if (IsOwnerDestroyed) return;

        // Checking...
        bool isActive = IsActive();

		List<BattleCardData> targetList = null;
		GetTargetList(out targetList);

		//Debug.Log ("Count: " + this.m_cardCount);

		if (targetList != null && targetList.Count > 0)
		{
			// Remove Buff
			for (int index = 0; index < targetList.Count; ++index)
			{
				RemoveBuff (targetList[index]);
			}

			if (this.m_cardCount > 0)
			{
				// Add Buff
				for (int index = 0; index < targetList.Count; ++index)
				{
					AddBuff (targetList [index]);
				}
			}
		}
	}

	protected override void AddBuff(BattleCardData battleCard)
	{
		CardBuff buff = new CardBuff(
			this.AbilityEffectID
			, this._abilityData.BattleCard.BattleCardID
			, battleCard.BattleCardID
			, 0 // shield
			, this.m_cardCount // hp
			, this.m_cardCount // attack
			, 0 // speed
			, 0 // move
			, 0 // attack range
			, new PassiveIndex()
			, new List<CardSubTypeData>()
			, new List<CardJobData>()
			, this.OnUnitUpdate
			, -1 // active turn
		);

		BattleManager.Instance.RequestAddBuffBattleCard(battleCard.BattleCardID, buff);
	}
	#endregion
}
#endregion

#region DealDamageAllNonPlantUnitAE 
public class DealDamageAllNonPlantUnitAE : AbilityEffect
{
    // AE0076
    #region DealDamageAllNonPlantUnitAE  Properties
    protected int _damage;
    protected int _rangeIndex;
    protected bool _isPiercing;

    public int Damage { get { return _damage; } }
    public int RangeIndex { get { return _rangeIndex; } }
    public bool IsPiercing { get { return _isPiercing; } }
    #endregion

    #region DealDamageAllNonPlantUnitAE Constructors
    public DealDamageAllNonPlantUnitAE(string id, int damage, int rangeIndex, int isPiercing)
        : base(id)
    {
        this._damage = damage;
        this._rangeIndex = rangeIndex;
        this._isPiercing = ((isPiercing > 0) ? true : false);

        _description_EN = string.Format(_description_EN, this.Damage, AbilityEffect.RangeIndexToText(this.RangeIndex), ((IsPiercing) ? Localization.Get("ABILITY_PIERCING_TEXT") : ""));
        _description_TH = string.Format(_description_TH, this.Damage, AbilityEffect.RangeIndexToText(this.RangeIndex), ((IsPiercing) ? Localization.Get("ABILITY_PIERCING_TEXT") : ""));
    }

    public DealDamageAllNonPlantUnitAE(AbilityEffectParams abilityEffectParams)
        : this(
              abilityEffectParams.ID
            , int.Parse(abilityEffectParams.Parameters[0])
            , int.Parse(abilityEffectParams.Parameters[1])
            , int.Parse(abilityEffectParams.Parameters[2])
        )
    {
    }
    #endregion

    #region DealDamageAllNonPlantUnitAE Methods
    public override bool IsHasTarget()
    {
        if (BattleManager.Instance != null)
        {
            if (_abilityData != null)
            {
                if (_abilityData.IsSummon)
                {
                    return true;
                }
            }
        }

        return false;
    }

    public override bool GetTargetList(out List<BattleCardData> targetList)
    {
        targetList = new List<BattleCardData>();
        foreach (PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
        {
            List<BattleCardData> target;
            BattleManager.Instance.GetTargetList(
                  playerIndex
                , out target
                , this.IsCanBeTarget
            );

            if (target != null && target.Count > 0)
            {
                targetList.AddRange(target);
            }
        }

        if (targetList.Count > 0)
        {
            return true;
        }

        targetList = null;
        return false;
    }

    protected virtual bool IsCanBeTarget(BattleCardData battleCard)
    {
        if (battleCard != null && _abilityData.IsSummon)
        {
            if (!battleCard.IsFaceDown && !battleCard.IsDead)
            {
                if (battleCard.TypeData == CardTypeData.CardType.Unit)
                {
                    if (AbilityEffect.IsInRange(_abilityData.BattleCard, battleCard, this.RangeIndex))
                    {
                        if(!battleCard.IsCardSubType(CardSubTypeData.CardSubType.Plant))
                        { 
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
    {
        if (IsHasTarget())
        {
            if (onFinishPrepare != null)
            {
                onFinishPrepare.Invoke();
            }
        }
        else
        {
            if (onCancelPrepare != null)
            {
                onCancelPrepare.Invoke();
            }
        }
    }

    public override void Action(OnFinishAction onFinishAction)
    {
        List<BattleCardData> targetList;
        GetTargetList(out targetList);

        if (targetList != null && targetList.Count > 0)
        {
            foreach (BattleCardData battleCard in targetList)
            {
                BattleManager.Instance.RequestDealDamageBattleCard(
                      battleCard.BattleCardID
                    , this.Damage
                    , this.IsPiercing
                );
            }
        }

        if (onFinishAction != null)
        {
            onFinishAction.Invoke();
        }
    }
    #endregion
}
#endregion

#region YouMayAE
public class YouMayAE : AbilityEffect
{
	// AE0078
	#region YouMayAE Properties
	#endregion

	#region YouMayAE Constructors
	public YouMayAE(string id)
		: base(id)
	{
		this._abilityEffectID = id;

		/*
		_description_EN = string.Format(_description_EN, _abilityData.Description_EN);
		_description_TH = string.Format(_description_TH, _abilityData.Description_TH);
		*/
	}

	public YouMayAE(AbilityEffectParams abilityEffectParams)
		: this(abilityEffectParams.ID)
	{
	}
	#endregion

	#region YouMayAE Methods
	public override bool IsHasTarget()
	{
		if(BattleManager.Instance != null)
		{
			if(this._abilityData != null)
			{
				return true;
			}
		}

		return false;
	}

	/*
	public override bool GetTargetList(out List<BattleCardData> targetList)
	{
		targetList = null;
		return false;
	}
	*/

	public override void Prepare(OnFinishPrepare onFinishPrepare, OnCancelPrepare onCancelPrepare)
	{
        _onFinishPrepare = onFinishPrepare;
        _onCancelPrepare = onCancelPrepare;
    
		if(IsHasTarget())
		{
			string questionText = "";

			switch (LocalizationManager.CurrentLanguage)
			{
				case LocalizationManager.Language.Thai:
				{
					questionText = string.Format(_description_TH, _abilityData.Description_TH);
				}
				break;

				case LocalizationManager.Language.English:
				default:
				{
					questionText = string.Format (_description_EN, _abilityData.Description_EN);
				}
				break;
			}

			BattleManager.Instance.AskYesNo (
				  _abilityData.OwnerPlayerIndex
				, questionText
				, new YesNoPopupUI.OnSelectYes(onFinishPrepare)
				, new YesNoPopupUI.OnSelectNo(onCancelPrepare)
                , new YesNoPopupUI.OnCancel(onCancelPrepare)
                , (BattleManager.Instance.IsHaveTimeout && !this.IsOwnerTurn)
			);
		} 
		else
		{
			if (onCancelPrepare != null)
			{
				onCancelPrepare.Invoke ();
			}
		}
	}

	public override void Action(OnFinishAction onFinishAction)
	{
		if(onFinishAction != null)
		{
			onFinishAction.Invoke();
		}
	}
	#endregion
}
#endregion