﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#region IDParams Class
public class IDParams
{
	#region IDParams Properties
	private string _id;
	private List<string> _parameters;

	public string ID
	{
		get { return _id; }	
	}
	public List<string> Parameters
	{
		get { return _parameters; }
	}
	#endregion

	#region IDParams Constructors
	public IDParams(string id, List<string> parameters)
	{
		_id = id;
		_parameters = parameters;
	}

	// Copy Constructor
	public IDParams(IDParams idParams)
	{
		_id = idParams.ID;
		_parameters = idParams.Parameters;
	}
	#endregion

	#region IDParams Methods
	public static bool StrToIDParams(string text, out IDParams abilityParams)
	{
		if(text.Length > 0)
		{
			string[] texts = text.Split('(', ')');
			if(texts.Length > 0)
			{
				string id = texts[0];

				List<string> parameters = new List<string>();

				if(texts.Length > 1) 
				{
					// Has parameters.
					string[] parameterTexts = texts[1].Split(',');
					foreach(string param in parameterTexts)
					{
						parameters.Add(param.Trim());
					}
				}

				abilityParams = new IDParams(id, parameters);
				return true;
			}
		}

		abilityParams = null;
		return false;
	}
	#endregion
}
#endregion

#region TriggerIndex Class
public class TriggerIndex
{
	public enum TriggerType : int
	{
		  Auto = 0
		, Act = 1
		, Ask = 2
	}

	#region Properties
	private int _triggerIndex;
	#endregion

	#region Constructors
	public TriggerIndex()
	{
		_triggerIndex = 0;
	}

	public TriggerIndex(int triggerIndex)
	{
		_triggerIndex = triggerIndex;
	}

	// Copy Constructor
	public TriggerIndex(TriggerIndex triggerIndex)
		:this(triggerIndex._triggerIndex)
	{
	}
	#endregion

	#region Methods
	public override string ToString ()
	{
		string resultText = "";
		foreach(TriggerType triggerType in System.Enum.GetValues(typeof(TriggerType)))
		{
			if(this == triggerType)
			{
				if(resultText.Length > 0)
				{
					resultText += ", ";
				}

				resultText += triggerType.ToString();
			}
		}

		return resultText;
	}
	#endregion

	#region Relational Operator Overloading
	public static bool operator ==(TriggerType c1, TriggerIndex c2)
	{
		if((System.Object)c1 == null && (System.Object)c2 == null) return true;
		else if((System.Object)c1 == null || (System.Object)c2 == null) return false;
		else
		{
			if(c2._triggerIndex > 0)
			{
				int n = (c2._triggerIndex & ((int)c1));
				if(n > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				if(c1 == TriggerType.Auto)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		}
	}

	public static bool operator !=(TriggerType c1, TriggerIndex c2)
	{
		return !(c1 == c2);
	}

	public static bool operator ==(TriggerIndex c1, TriggerType c2)
	{
		return (c2 == c1);
	}

	public static bool operator !=(TriggerIndex c1, TriggerType c2)
	{
		return !(c2 == c1);
	}

	public static bool operator ==(TriggerIndex c1, TriggerIndex c2)
	{
		if ((System.Object)c1 == null && (System.Object)c2 == null) return true;
		else if ((System.Object)c1 == null || (System.Object)c2 == null) return false;
		else return (c1._triggerIndex == c2._triggerIndex);
	}

	public static bool operator !=(TriggerIndex c1, TriggerIndex c2)
	{
		return !(c1 == c2);
	}

	public override bool Equals(System.Object obj)
	{
		// Check for null values and compare run-time types.
		if (obj == null || GetType() != obj.GetType())
			return false;

		TriggerIndex c = (obj as TriggerIndex);
		return (_triggerIndex == c._triggerIndex);
	}

	public override int GetHashCode()
	{
		return 0;
	}
	#endregion
}
#endregion

#region AbilityData Class
public class AbilityData
{
	#region AbilityParams Class
	public class AbilityParams : IDParams
	{
		#region AbilityParams Constructors
		public AbilityParams(string id, List<string> parameters)
			: base(id, parameters)
		{
		}
		#endregion

		/*
		#region AbilityParams Methods
		public static AbilityData ToAbilityData(BattleCardData ownerUnit, AbilityParams abilityParams)
		{
			int index = ownerUnit.AbilityList.Count;
			return (new AbilityData(ownerUnit, index, abilityParams));
		}
		#endregion
		*/
	}
	#endregion

	#region AbilityData Delegates
	public delegate void OnFinishedPrepare();
	public delegate void OnCanceledPrepare();

	public delegate void OnFinishedAction();
	public delegate void OnCanceledAction();
	#endregion

	#region AbilityData Properties
	private UniqueCardData _uniqueCard = null;
	private BattleCardData _battleCard = null; 

	private int _actionIndex = 0;
    private bool _isRequestPause = false;

	protected int _abilityIndex = -1;
	protected string _abilityID;
	protected TriggerIndex _triggerIndex;
	protected string _description_EN;
	protected string _description_TH;

	protected List<AbilityCondition> _conditionList;
	protected List<AbilityEffect> _effectList;
	protected OnFinishedPrepare _onFinishedPrepare = null;
	protected OnCanceledPrepare _onCanceledPrepare = null;
	protected OnFinishedAction _onFinishedAction = null;
	protected OnCanceledAction _onCanceledAction = null;

    protected bool _isUseByPlayer;
    protected List<string> _paramList;

	public int AbilityIndex 
	{
		get { return _abilityIndex; }	
	}
	public string AbilityID
	{
		get { return _abilityID; }	
	}
	public TriggerIndex TriggerIndex
	{
		get { return _triggerIndex; }
	}
	public string Description_EN
	{
		get { return _description_EN; }
	}
	public string Description_TH
	{
		get { return _description_TH; }
	}
    public List<string> AbilityParamList
    {
        get { return _paramList; }
    }

	public UniqueCardData UniqueCard
	{
		get { return _uniqueCard; }
	}
	public BattleCardData BattleCard
	{
		get { return _battleCard; }
	}

	public PlayerCardData PlayerCardData
	{
		get
		{
			if(IsSummon)
			{
				return _battleCard.PlayerCardData;
			}
			else
			{
				return _uniqueCard.PlayerCardData;
			}
		}
	}
	public PlayerIndex OwnerPlayerIndex
	{
		get
		{
			if(IsSummon)
			{
				return _battleCard.OwnerPlayerIndex;
			}
			else
			{
				return _uniqueCard.OwnerPlayerIndex;
			}
		}
	}

	public bool IsSummon
	{
		get
		{
			if(_battleCard != null)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
    
    public bool IsOwnerTurn { get { return (OwnerPlayerIndex == BattleManager.Instance.CurrentActivePlayerIndex); } }
	#endregion

	#region AbilityData Constructors
	private AbilityData(AbilityParams abilityParams)
	{
		_abilityID = abilityParams.ID;

		//Debug.Log("[" + _abilityID + "] Constructor");

		AbilityDBData abilityDBData;
		bool isFound = AbilityDB.GetData(abilityParams.ID, out abilityDBData);
		if(isFound)
		{
			int paramIndex = 0;

			_triggerIndex = new TriggerIndex(abilityDBData.TriggerIndex);

			// Create Ability Condition Data.
			_conditionList = new List<AbilityCondition>();
			foreach(string conditionID in abilityDBData.AbilityConditionIDList)
			{
				AbilityConditionDBData conditionDBData;
				bool isFoundCondition = AbilityConditionDB.GetData(conditionID, out conditionDBData);
				//Debug.Log("Find: " + conditionID);
				if(isFoundCondition)
				{
					//Debug.Log("Found: " + conditionID);
					List<string> paramsText = new List<string>();
					for(int index = 0; index < conditionDBData.ParameterNum; ++index)
					{
						paramsText.Add(abilityParams.Parameters[paramIndex]);

						//Debug.Log(paramIndex);
						++paramIndex;
					}

					AbilityCondition.AbilityConditionParams conditionParams = new AbilityCondition.AbilityConditionParams(
						conditionDBData.AbilityConditionID
						, paramsText
					);
					AbilityCondition condition = AbilityCondition.AbilityConditionParams.ToAbilityCondition(conditionParams);

					//Debug.Log("Add: " + condition.GetDebugText());
					_conditionList.Add(condition);
				}
			}

			// Create Ability Effect Data.
			_effectList = new List<AbilityEffect>();
			foreach(string effectID in abilityDBData.AbilityEffectIDList)
			{
				AbilityEffectDBData effectDBData;
				bool isFoundEffect = AbilityEffectDB.GetData(effectID, out effectDBData);
				//Debug.Log("Find: " + effectID);
				if(isFoundEffect)
				{
					//Debug.Log("Found: " + effectID);
					List<string> paramsText = new List<string>();
					for(int index = 0; index < effectDBData.ParameterNum; ++index)
					{
						paramsText.Add(abilityParams.Parameters[paramIndex]);

						//Debug.Log(paramIndex);
						++paramIndex;
					}

					AbilityEffect.AbilityEffectParams effectParams = new AbilityEffect.AbilityEffectParams(
						effectDBData.AbilityEffectID
						, paramsText
					);
					AbilityEffect effect = AbilityEffect.AbilityEffectParams.ToAbilityEffect(effectParams);


					//Debug.Log("Add: " + effect.GetDebugText());
					/*
					string text = string.Format("[{0}]{1}\nDescription EN: {2}\nDescription TH: {3}"
						, effect.AbilityEffectID
						, effect.GetType().ToString()
						, effect.Description_EN
						, effect.Description_TH
					);
					Debug.Log (text);
					*/

					_effectList.Add(effect);
				}
				else
				{
					//Debug.Log("AbilityData: Constructor: not found effect data ID " + effectID);
				}
			}

			_description_EN = abilityDBData.Description_EN;
			_description_TH = abilityDBData.Description_TH;
		}
	}
		
	public AbilityData(BattleCardData battleCard, int abilityIndex, AbilityParams abilityParams)
		: this(abilityParams)
	{
		this._abilityIndex = abilityIndex;
		SetBattleCard(battleCard);
		Init();
	}

	public AbilityData(UniqueCardData uniqueCard, int abilityIndex, AbilityParams abilityParams)
		: this(abilityParams)
	{
		this._abilityIndex = abilityIndex;
		SetUniqueCard(uniqueCard);
		Init();
	}
		
	/*
	~AbilityData()
	{
		//Deinit();
	}
	*/
	#endregion

	#region AbilityData Methods
	public bool IsCanUse()
	{
		return (this.IsCanTrigger() && this.IsHasTarget());
	}

	public bool IsCanTrigger()
	{
		if(_conditionList != null && _conditionList.Count > 0)
		{
			bool isTrigger = true;
			foreach(AbilityCondition condition in _conditionList)
			{
				//Debug.Log(condition.AbilityConditionID + " " + condition.IsCanTrigger());
				isTrigger = condition.IsCanTrigger();
				if(!isTrigger) 
				{
					return false;
				}
			}
				
			return isTrigger;
		}

		return true;
	}

	public bool IsHasTarget()
	{
		if(_effectList != null && _effectList.Count > 0)
		{
			bool isHasTarget = true;

			foreach(AbilityEffect effect in _effectList)
			{
				//Debug.Log(effect.AbilityEffectID + " " + effect.IsHasTarget());
				isHasTarget = effect.IsHasTarget();
				if(!isHasTarget) 
				{
					return false;
				}
			}
				
			return isHasTarget;
		}

		return false;
	}

	private void SetBattleCard(BattleCardData battleCard)
	{
		_battleCard = battleCard;

		_uniqueCard = null;
	}

	private void SetUniqueCard(UniqueCardData uniqueCard)
	{
		_uniqueCard = uniqueCard;

		_battleCard = null;
	}
		
    public string PopAbilityParamList()
    {
        string result = "";
        
        if(this._paramList != null && this._paramList.Count > 0)
        {
            result = this._paramList[0];
            this._paramList.RemoveAt(0);
        }
        
        return result;
    }
        
	private void Init()
	{
		//Debug.Log("AbilityData: " + this.AbilityID + " Init");


		if(_conditionList != null && _conditionList.Count > 0)
		{
			//Debug.Log("AbilityData: Condition Init");
			foreach(AbilityCondition condition in _conditionList)
			{
				//Debug.Log(condition.AbilityConditionID + ": Init");
				condition.Init(this);
			}
		}

		if(_effectList != null && _effectList.Count > 0)
		{
			//Debug.Log("AbilityData: Effect Init");
			foreach(AbilityEffect effect in _effectList)
			{
				//Debug.Log(effect.AbilityEffectID + ": Init");
				effect.Init(this);
			}
		}
	}

	public void Deinit()
	{
		if(_conditionList != null && _conditionList.Count > 0)
		{
			foreach(AbilityCondition condition in _conditionList)
			{
				condition.Deinit();
			}

			_conditionList.Clear();
		}

		if(_effectList != null && _effectList.Count > 0)
		{
			foreach(AbilityEffect effect in _effectList)
			{
				effect.Deinit();
			}

			_effectList.Clear();
		}
	}

	public void OnBattleEventTrigger()
	{
		//Debug.Log("Ability Event Trigger.");

		if(this.IsCanUse())
		{
			if(this.TriggerIndex == TriggerIndex.TriggerType.Auto || this.TriggerIndex == TriggerIndex.TriggerType.Ask)
			{
				//Debug.Log("[" + this.TriggerIndex.ToString() + "] " + this.OwnerPlayerIndex.ToString() + " activated\n" + this.GetDebugPrintText());

				this.ActiveAbility(false);
			}
		}
	}

	public void ActiveAbility(bool isUseByPlayer)
	{
		if(this.IsSummon)
		{
			BattleManager.Instance.RequestUseAbilityBattleCard(this.BattleCard.BattleCardID, this.AbilityIndex, isUseByPlayer);
		}
		else
		{
			BattleManager.Instance.RequestUseHandCard(this.UniqueCard.UniqueCardID, isUseByPlayer);
		}
	}

	// Prepare
	public void StartPrepare(bool isUseByPlayer)
	{
		#if UNITY_EDITOR
		{
			string text = "";
			text = string.Format("Start Prepare [{0}] {1}."
				, this.AbilityID
				, this.Description_EN
			);
			//Debug.Log(text);
		}
		#endif

        _isUseByPlayer = isUseByPlayer;
        _paramList = new List<string>();
		_actionIndex = 0;
		this.OnPrepare();
	}

	private void OnPrepare()
	{
		//Debug.Log("Prepare:\n" + this.GetDebugPrintText());

		if(_actionIndex < _effectList.Count)
		{
			// do prepare
			//Debug.Log("Prepare: " + _effectList[_actionIndex].GetDebugText());
			_effectList[_actionIndex].Prepare(this.EndPrepare, this.CancelPrepare);
		}
	}

	private void EndPrepare()
	{
		// next action
		++_actionIndex;

		if(_actionIndex < _effectList.Count)
		{
			this.OnPrepare();
		}
		else
		{
			this.OnFinishPrepare();
		}
	}

	private void CancelPrepare()
	{
		#if UNITY_EDITOR
		{
			string text = "";
			text = string.Format("Cancel Prepare [{0}] {1}."
				, this.AbilityID
				, this.Description_EN
			);
			//Debug.Log(text);
		}
		#endif

		if(_onCanceledPrepare != null)
		{
			_onCanceledPrepare.Invoke();
			_onCanceledPrepare = null;
		}

		OnFinishAction();
	}

	private void OnFinishPrepare()
	{
		#if UNITY_EDITOR
		{
			string text = "";
			text = string.Format("Finish Prepare [{0}] {1}."
				, this.AbilityID
				, this.Description_EN
			);
			//Debug.Log(text);
		}
		#endif

        if((_isUseByPlayer || _paramList.Count > 0) && KOSNetwork.NetworkSetting.IsNetworkEnabled)
        {
            BattleManager.Instance.RequestWaitActiveQueue();
            CancelPrepare();

            if(this.IsSummon)
            {
                string text = "";
                text = string.Format("CmdActionAbilityBattleCard [{0}] {1}."
                    , this.AbilityID
                    , this.Description_EN
                );
                //Debug.Log(text);
            
                KOSNetwork.GamePlayer.localPlayer.RequestActionAbilityBattleCard(
                      this.BattleCard.BattleCardID
                    , this.AbilityIndex
                    , this._paramList
                );
            }
            else
            {
                string text = "";
                text = string.Format("CmdActionAbilityUniqueCard [{0}] {1}."
                    , this.AbilityID
                    , this.Description_EN
                );
                //Debug.Log(text);
            
                KOSNetwork.GamePlayer.localPlayer.RequestActionAbilityUniqueCard(
                      this.UniqueCard.UniqueCardID
                    , this.AbilityIndex
                    , this._paramList
                );
            }           
        }
        else
        {
		    //StartAction(this._paramList);
            
            BattleManager.Instance.RequestWaitActiveQueue();           
            if(this.IsSummon)
            {
                BattleManager.Instance.RequestActionAbilityBattleCard(
                      this.BattleCard.BattleCardID
                    , this.AbilityIndex
                    , this._paramList
                    ,  IsOwnerTurn
                );
            }
            else
            {
                BattleManager.Instance.RequestActionAbilityUniqueCard(
                      this.UniqueCard.UniqueCardID
                    , this.AbilityIndex
                    , this._paramList
                    , IsOwnerTurn
                );
            }
            
            CancelPrepare();
            BattleManager.Instance.RequestResumeActiveQueue();
        }
	}
		
	// Action
	private void StartAction()
	{
		#if UNITY_EDITOR
		{
			string text = "";
			text = string.Format("Start Action [{0}] {1}."
				, this.AbilityID
				, this.Description_EN
			);
			//Debug.Log(text);
		}
        #endif
        
        if(_onFinishedPrepare != null)
        {
            _onFinishedPrepare.Invoke();
            _onFinishedPrepare = null;
        }

        _actionIndex = 0;

        if (string.Compare(_abilityID, "A0001") == 0) // Auto: You recieve {0}G.
        {
            // Skip Preform Use Ability
            this.OnAction();
        }
        else
        {  
            // Preform Use Ability
            /*
            if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
            {
                if(this.IsSummon)
                {
                    KOSNetwork.GamePlayer.localPlayer.CmdPreformAbilityBattleCard(this.BattleCard.BattleCardID);
                }
                else
                {
                    KOSNetwork.GamePlayer.localPlayer.CmdPreformAbilityHandCard(this.UniqueCard.UniqueCardID);
                }
            }
            else
            {   
                if(this.IsSummon)
                {
                    BattleManager.Instance.RequestPreformAbilityBattleCard(this.BattleCard.BattleCardID);
                }
                else
                {
                    BattleManager.Instance.RequestPreformUseHandCard(this.UniqueCard.UniqueCardID);
                }
            }
            this.OnAction();
            */

            List<ShowUseAbilityUI.PerformUseAction> stepList = new List<ShowUseAbilityUI.PerformUseAction>();

            ShowUseAbilityUI.PerformUseAction step = new ShowUseAbilityUI.PerformUseAction();
            step.PopupText = "";

            stepList.Add(step);

            BattleManager.Instance.PreformUseAbility(this.PlayerCardData, stepList, this.OnAction);
        }
	}

    public void StartAction(List<string> paramList)
    {
        _paramList = paramList;
        
        _isRequestPause = true;
        BattleManager.Instance.SetPauseTimeout(true);
        
        StartAction();
    }

	private void OnAction()
	{
		//Debug.Log("Action:\n" + this.GetDebugPrintText());

		if(_actionIndex < _effectList.Count)
		{
			// do action
            //Debug.Log("Action: " + _effectList[_actionIndex].GetDebugText());
			_effectList[_actionIndex].Action(this.EndAction);
		}
	}

	private void EndAction()
	{
		// next action
		++_actionIndex;

		if(_actionIndex < _effectList.Count)
		{
			this.OnAction();
		}
		else
		{
			this.OnFinishAction();
		}
	}

	private void OnFinishAction()
	{
		#if UNITY_EDITOR
		{
			string text = "";
			text = string.Format("Finish Action [{0}] {1}."
				, this.AbilityID
				, this.Description_EN
			);
			//Debug.Log(text);
		}
		#endif
        
        if(_isRequestPause)
        {
            _isRequestPause = false;
            BattleManager.Instance.SetPauseTimeout(false);
        }
        
		if(_onFinishedAction != null)
		{
			_onFinishedAction.Invoke();
			_onFinishedAction = null;
		}
	}

	private void OnCancelAction()
	{
		#if UNITY_EDITOR
		{
			string text = "";
			text = string.Format("Cancel Action [{0}] {1}."
				, this.AbilityID
				, this.Description_EN
			);
			//Debug.Log(text);
		}
		#endif

		if(_onCanceledAction != null)
		{
			_onCanceledAction.Invoke();
			_onCanceledAction = null;
		}
	}

	public void SetOnFinishedPrepareCallback(OnFinishedPrepare onFinishedPrepare)
	{
		_onFinishedPrepare = onFinishedPrepare;
	}

	public void SetOnCanceledPrepareCallback(OnCanceledPrepare onCanceledPrepare)
	{
		_onCanceledPrepare = onCanceledPrepare;
	}
		
	public void SetOnFinishedActionCallback(OnFinishedAction onFinishedAction)
	{
		_onFinishedAction = onFinishedAction;
	}
		
	public void AddOnFinishedActionCallback(OnFinishedAction onFinishedAction)
	{
		_onFinishedAction += onFinishedAction;
	}

	public void SetOnCanceledActionCallback(OnCanceledAction onCanceledAction)
	{
		_onCanceledAction = onCanceledAction;
	}
		
	public string GetDebugPrintText()
	{
		string conditionText = "";
		foreach(AbilityCondition condition in _conditionList)
		{
			if(conditionText.Length == 0)
			{
				conditionText += condition.GetDebugText();
			}
			else
			{
                conditionText += "\n" + condition.GetDebugText();
			}
		}

		string effectText = "";
		foreach(AbilityEffect effect in _effectList)
		{
			if(effectText.Length == 0)
			{
                effectText += effect.GetDebugText();
			}
			else
			{
                effectText += "\n" + effect.GetDebugText();
			}
		}

		string cardDetailText = "";
		cardDetailText = string.Format(
			  "{0}:{1}"
			, PlayerCardData.PlayerCardID 
			, PlayerCardData.Name_EN
		);

		string resultText;
		resultText = string.Format(
			"AbilityID: {0}\nTriggerType: {1}\nMyCard: {2}\nCondition: {3}\nEffect: {4}"
			, AbilityID
			, TriggerIndex.ToString()
			, cardDetailText
			, conditionText
			, effectText
		);

		return resultText;
	}
	#endregion
}
#endregion
