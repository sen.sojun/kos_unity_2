﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CardBuff 
{
	#region Delegates
	public delegate void OnTargetDestroyed();
	#endregion

	#region Private Properties
	private int _buffID;

	private string _abilityEffectID;
	private int _effectorID = -1;
	private int _effectedID = -1;

	private int _shield;
	private int _hp;
	private int _attack;
	private int _speed;
	private int _move;
	private int _attackRange;
	private PassiveIndex _passiveIndex;

	private List<CardSubTypeData> _subTypeList;
	private List<CardJobData> _jobTypeList;

    private int _activeTurn;

	private OnTargetDestroyed _onTargetDestroyed = null;

	private static int _buffIndex = 0; // 1, 2, 3, 4, ...
	#endregion

	#region Public Properties
	public int BuffID
	{
		get { return _buffID; }
	}
	public string AbilityEffectID
	{
		get { return _abilityEffectID; }
	}
	public int EffectorID
	{
		get { return _effectorID; }
	}
	public int EffectedID
	{
		get { return _effectedID; }
	}
	public int Shield
	{
		get { return _shield; }
	}
	public int HP
	{
		get { return _hp; }
	}
	public int Attack
	{
		get { return _attack; }
	}
	public int Speed
	{
		get { return _speed; }
	}
	public int Move
	{
		get { return _move; }
	}
	public int AttackRange
	{
		get { return _attackRange; }
	}
	public PassiveIndex PassiveIndex
	{
		get { return _passiveIndex; }
	}

	public List<CardSubTypeData> SubTypeList { get { return _subTypeList; } }
	public List<CardJobData> JobTypeList { get { return _jobTypeList; } }

	public int ActiveTurn { get { return _activeTurn; } }

	public bool IsActive
	{
		get
		{
			if(_activeTurn != 0)
			{
				return true;
			}

			return false;
		}
	}
	#endregion

	public CardBuff(
		   string abilityEffectID
		 , int effectorID
		 , int effectedID
		 , int shield
		 , int hp
		 , int attack
		 , int speed
		 , int move
		 , int attackRange
		 , PassiveIndex passiveIndex
		 , List<CardSubTypeData> subTypeList
		 , List<CardJobData> jobTypeList
		 , OnTargetDestroyed onTargetDestroyed
		 , int activeTurn = -1)
    {
		_buffID = CardBuff.RequestBuffID();

		_abilityEffectID = abilityEffectID;
		_effectorID = effectorID;
		_effectedID = effectedID;

		_shield = shield;
		_hp = hp;
		_attack = attack;
		_speed = speed;
		_move = move;
		_attackRange = attackRange;
		_passiveIndex = passiveIndex;
		_activeTurn = activeTurn;

		_subTypeList = new List<CardSubTypeData>(subTypeList);
		_jobTypeList = new List<CardJobData>(jobTypeList);

		_onTargetDestroyed = onTargetDestroyed;
    }
		
    public void UpdateTurn()
    {
        if (_activeTurn > 0)
        {
            _activeTurn--;
        }
    }

	public void TargetDestroyed()
	{
		if(_onTargetDestroyed != null)
		{
			_onTargetDestroyed.Invoke();
		}
	}
		
	/*
	public void BindOnOwnerDestroyed(OnOwnerDestroyed callback)
	{
		_onOwnerDestroyed = callback;
	}

	public void BindOnTargetDestroyed(OnTargetDestroyed callback)
	{
		_onTargetDestroyed = callback;
	}
	*/

	public static int RequestBuffID()
	{
		return ++_buffIndex;
	}
}