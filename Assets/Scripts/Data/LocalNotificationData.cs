﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using System;

public class LocalNotificationData 
{
	#region public properties
	public string MessageCode;
	public string MessageEN;
	public string MessageTH;
	public string Condition;
	public string TimeToSend;
	#endregion

	#region Constructors
	public LocalNotificationData()
	{
		MessageCode = "";
		MessageEN   = "";
		MessageTH   = "";
		Condition   = "";
		TimeToSend  = "";
	}


	public LocalNotificationData(string messageCode,string messageEN,string messageTH,
		string condition,string timeToSend)
	{
		MessageCode = messageCode;
		MessageEN = messageEN;
		MessageTH = messageTH;
		Condition = condition;
		TimeToSend = timeToSend;
	}

	public LocalNotificationData(LocalNotificationData localDBData)
	{
		MessageCode = localDBData.MessageCode;
		MessageEN = localDBData.MessageEN;
		MessageTH = localDBData.MessageTH;
		Condition = localDBData.Condition;
		TimeToSend = localDBData.TimeToSend;
	}
	#endregion

	#region Methods
	public string GetDebugPrintText()
	{
		string resultText;
		resultText = string.Format (
			"Message Code: {0}\nMessage (EN): {1}\nMessage (TH): {2}\nCondition: {3}\nTime: {4}"
       		, MessageCode
			, MessageEN
			, MessageTH
			, Condition
			, TimeToSend
		);
		return resultText;
    }

	public void DebugPrintData()
	{
		Debug.Log (GetDebugPrintText ());
	}

	#endregion
}

public class LocalNotificationDB
{
	#region Enums
	private enum LocalNotificationDBIndex : int
	{
		MessageCode = 0
        ,MessageEN 
		,MessageTH
		,Condition
		,Time
	}
	#endregion

	#region private properties
	private static Dictionary<string,LocalNotificationData> _localDBList = null;
	private static bool _isLoadDB = false;
	#endregion

	#region Constructors
	static LocalNotificationDB()
	{
	}
	#endregion

	#region Methods
	public static bool LoadDB()
	{
		TextAsset csvFile = Resources.Load<TextAsset>("Databases/KOS-Local-NotificationsDB");
		if(csvFile != null)
		{
			List<List<string>> rawDBList;
			bool isSuccess = CSVReader.ReadCSV(csvFile, out rawDBList);
			if (isSuccess)
			{
				_localDBList = new Dictionary<string,LocalNotificationData>();

				foreach(List<string> record in rawDBList)
				{
					LocalNotificationData data = new LocalNotificationData(
						record[(int)LocalNotificationDBIndex.MessageCode]
						, record[(int)LocalNotificationDBIndex.MessageEN]
						, record[(int)LocalNotificationDBIndex.MessageTH]
						, record[(int)LocalNotificationDBIndex.Condition]
						, record[(int)LocalNotificationDBIndex.Time]
					);

					_localDBList.Add (data.MessageCode, data);
				}

				Resources.UnloadAsset(csvFile);
				_isLoadDB = true;
				return true;
			}
		}

		_isLoadDB = false;
		return false;
	}

	public static bool GetData(string messageCode, out LocalNotificationData data)
	{
		if(!_isLoadDB)
		{
			LoadDB();
		}

		return (_localDBList.TryGetValue(messageCode, out data));
	}

	public static void GetAllData(out List<LocalNotificationData> dataList)
	{
		if(!_isLoadDB)
		{
			LoadDB();
		}

		dataList = new List<LocalNotificationData>();

		foreach (KeyValuePair<string, LocalNotificationData> data in _localDBList)
		{
			dataList.Add(data.Value);
		}
	}


	public bool SuccessSetNotification(string msgCode)
	{
		
		if(!_isLoadDB)
		{
			LoadDB();
		}
		LocalNotificationData data;

		bool isSuccess = GetData (msgCode, out data);
		if (isSuccess) 
		{
			string msg = "";
			switch(LocalizationManager.CurrentLanguage)
			{
				case LocalizationManager.Language.Thai:
					msg = data.MessageTH;
					break;

				default:
					msg = data.MessageEN;
					break;
			}

			string[] timeText = data.TimeToSend.Split(':');
//			foreach (string word in timeText)
//			{
//				int h = int.Parse (timeText [0]);
//				int m = int.Parse (timeText [1]);
//				int s = int.Parse (timeText [2]);
//
//			}

			int h = int.Parse (timeText [0]);
			int m = int.Parse (timeText [1]);
			int s = int.Parse (timeText [2]);

			//compare current time with h/m/s 
			//delta secod for android ,datetime for ios
			#if UNITY_IOS
			 
			#elif UNITY_ANDROID

			#else
			  //maybe other device than android and ios so no notification 

			#endif

			//if it passed , set the next day noti
			//else set noti
			 

		}
			return isSuccess;


//		return (_localDBList.TryGetValue(messageCode, out data));
//
//		dataList = new List<LocalNotificationData>();
//

		 
	}



	public static void DebugPrintData()
	{
		StringBuilder sb = new StringBuilder();

		foreach (KeyValuePair<string, LocalNotificationData> data in _localDBList)
		{
			sb.Append(data.Value.GetDebugPrintText());
			sb.Append("\n\n");
		}
		Debug.Log(sb.ToString());
	}
	#endregion



}
