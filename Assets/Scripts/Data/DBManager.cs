﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class DBManager : MonoBehaviour
{
    #region Delegates
    public delegate bool LoadDBAction();
    #endregion

	#region Properties
    private static DBManager _instance = null;
    public static DBManager Instance 
    {
        get
        {
            if(_instance == null)
            {
                _instance = GameObject.FindObjectOfType<DBManager>();
             
                 if (_instance == null)
                 {
                     GameObject obj = new GameObject();
                     obj.name = "DBManager";
                     _instance = obj.AddComponent<DBManager>();
                 }
            }
            
            return _instance;
        }
    }
    
    private float _startTime;
    private float _stopTime;

    private UnityAction _onLoadAsyncComplete;
    public bool isPrintResult = false;
	#endregion

	#region Awake
	private void Awake()
	{
		if(_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
	}
    #endregion

	#region Start
	void Start()
	{
		//#if UNITY_EDITOR
		//Profiler.maxNumberOfSamplesPerFrame = 8000000;
		//#endif

        //DBManager.InitDB(isPrintResult);
	}
	#endregion

	#region Methods
    public void InitDB(bool isPrintResult = false)
	{
        _startTime = Time.realtimeSinceStartup;

		bool isSuccess = true;

        if(!CardDB.IsLoadDB)
        {
    		isSuccess = CardDB.LoadDB();
    		if (isSuccess)
    		{
                #if UNITY_EDITOR
                if (isPrintResult)
                {
                    Debug.Log("Successed Load CardDB.");
                    CardDB.DebugPrintData();
                }
                #endif
    		}
    		else
    		{
    			Debug.LogError("Failed Load CardDB.");
    		}
        }

        if(!CardImageDB.IsLoadDB)
        {
    		isSuccess = CardImageDB.LoadDB();
    		if (isSuccess)
            {
                #if UNITY_EDITOR
                if (isPrintResult)
                {
                    Debug.Log("Successed Load CardImageDB.");
                    CardImageDB.DebugPrintData();
                }
                #endif
            }
    		else
    		{
    			Debug.LogError("Failed Load CardImageDB.");
    		}
        }

        if(!CardSymbolDB.IsLoadDB)
        {
    		isSuccess = CardSymbolDB.LoadDB();
    		if (isSuccess)
            {
                #if UNITY_EDITOR
                if (isPrintResult)
                {
                    Debug.Log("Successed Load CardSymbolDB.");
                    CardSymbolDB.DebugPrintData();
                }
                #endif
            }
    		else
    		{
    			Debug.LogError("Failed Load CardSymbolDB.");
    		}
        }

        if(!CardClanDB.IsLoadDB)
        {
    		isSuccess = CardClanDB.LoadDB();
    		if (isSuccess)
    		{
                #if UNITY_EDITOR
                if (isPrintResult)
                {
                    Debug.Log("Successed Load CardClanDB.");
                    CardClanDB.DebugPrintData();
                }
                #endif
            }
    		else
    		{
    			Debug.LogError("Failed Load ClanDB.");
    		}
        }

        if(!CardTypeDB.IsLoadDB)
        {
    		isSuccess = CardTypeDB.LoadDB();
    		if (isSuccess)
    		{
                #if UNITY_EDITOR
                if (isPrintResult)
                {
                    Debug.Log("Successed Load CardTypeDB.");
                    CardTypeDB.DebugPrintData();
                }
                #endif
    		}
    		else
    		{
    			Debug.LogError("Failed Load CardTypeDB.");
    		}
        }

        if(!CardSubTypeDB.IsLoadDB)
        {
    		isSuccess = CardSubTypeDB.LoadDB();
    		if (isSuccess)
    		{
                #if UNITY_EDITOR
                if (isPrintResult)
                {
                    Debug.Log("Successed Load CardSubTypeDB.");
                    CardSubTypeDB.DebugPrintData();
                }
                #endif
    		}
    		else
    		{
    			Debug.LogError("Failed Load CardSubTypeDB.");
    		}
        }

        if(!CardJobDB.IsLoadDB)
        {
    		isSuccess = CardJobDB.LoadDB();
    		if (isSuccess)
    		{
                #if UNITY_EDITOR
                if (isPrintResult)
                {
                    Debug.Log("Successed Load CardJobDB.");
                    CardJobDB.DebugPrintData();
                }
                #endif
    		}
    		else
    		{
    			Debug.LogError("Failed Load CardJobDB.");
    		}
        }

        if(!CardTextureDB.IsLoadDB)
        {
    		isSuccess = CardTextureDB.LoadDB();
    		if (isSuccess)
    		{
                #if UNITY_EDITOR
                if (isPrintResult)
                {
                    Debug.Log("Successed Load CardTextureDB.");
                    CardTextureDB.DebugPrintData();
                }
                #endif
    		}
    		else
    		{
    			Debug.LogError("Failed Load CardTextureDB.");
    		}
        }

        if(!DeckDB.IsLoadDB)
        {
    		isSuccess = DeckDB.LoadDB();
    		if (isSuccess)
    		{
                #if UNITY_EDITOR
                if (isPrintResult)
                {
                    Debug.Log("Successed Load DeckDB.");
                    DeckDB.DebugPrintData();
                }
                #endif
    		}
    		else
    		{
    			Debug.LogError("Failed Load DeckDB.");
    		}
        }
        
        if(!DeckNameDB.IsLoadDB)
        {
    		isSuccess = DeckNameDB.LoadDB();
    		if (isSuccess)
    		{
                #if UNITY_EDITOR
                if (isPrintResult)
                {
                    Debug.Log("Successed Load DeckNameDB.");
                    DeckNameDB.DebugPrintData();
                }
                #endif
    		}
    		else
    		{
    			Debug.LogError("Failed Load DeckNameDB.");
    		}
        }
        
        if(!AbilityDB.IsLoadDB)
        {
    		isSuccess = AbilityDB.LoadDB();
    		if (isSuccess)
    		{
                #if UNITY_EDITOR
                if (isPrintResult)
                {
                    Debug.Log("Successed Load AbilityDB.");
                    AbilityDB.DebugPrintData();
                }
                #endif
    		}
    		else
    		{
    			Debug.LogError("Failed Load AbilityDB.");
    		}
        }

        #if UNITY_EDITOR        
        if(!Ability2DB.IsLoadDB)
        {
            isSuccess = Ability2DB.LoadDB();
            if (isSuccess)
            {
                #if UNITY_EDITOR
                if (isPrintResult)
                {
                    Debug.Log("Successed Load Ability2DB.");
                    Ability2DB.DebugPrintData();
                }
                #endif
            }
            else
            {
                Debug.LogError("Failed Load Ability2DB.");
            }
        }
        #endif
        
        if(!AbilityConditionDB.IsLoadDB)
        {
    		isSuccess = AbilityConditionDB.LoadDB();
    		if (isSuccess)
    		{
                #if UNITY_EDITOR
                if (isPrintResult)
                {
                    Debug.Log("Successed Load AbilityConditionDB.");
                    AbilityConditionDB.DebugPrintData();
                }
                #endif
    		}
    		else
    		{
    			Debug.LogError("Failed Load AbilityConditionDB.");
    		}
        }

        if(!AbilityEffectDB.IsLoadDB)
        {
    		isSuccess = AbilityEffectDB.LoadDB();
    		if (isSuccess)
    		{
                #if UNITY_EDITOR
                if (isPrintResult)
                {
                    Debug.Log("Successed Load AbilityEffectDB.");
                    AbilityEffectDB.DebugPrintData();
                }
                #endif
    		}
    		else
    		{
    			Debug.LogError("Failed Load AbilityEffectDB.");
    		}
        }

        if(!NPCDB.IsLoadDB)
        {
    		isSuccess = NPCDB.LoadDB();
    		if (isSuccess)
    		{
                #if UNITY_EDITOR
                if (isPrintResult)
                {
                    Debug.Log("Successed Load NPCDB.");
                    NPCDB.DebugPrintData();
                }
                #endif
    		}
    		else
    		{
    			Debug.LogError("Failed Load NPCDB.");
    		}
        }

        if(!TutorialDB.IsLoadDB)
        {
    		isSuccess = TutorialDB.LoadDB();
    		if (isSuccess)
    		{
                #if UNITY_EDITOR
                if (isPrintResult)
                {
                    Debug.Log("Successed Load TutorialDB.");
                    TutorialDB.DebugPrintData();
                }
                #endif
    		}
    		else 
    		{
                Debug.LogError("Failed Load TutorialDB.");
    		}
        }

        _stopTime = Time.realtimeSinceStartup;

        Debug.Log("Load time: " + (_stopTime - _startTime).ToString("0.000") + " seconds.");
	}
    
    public void InitDBAsync(UnityAction onComplete)
    {
        _startTime = Time.realtimeSinceStartup;
        _onLoadAsyncComplete = onComplete;
        
        
        OnDBLoading(0);
    }
    
    private void OnDBLoading(int index)
    {
        switch(index)
        {
            case 0: StartCoroutine(LoadingDB(index, CardDB.LoadDB, OnDBLoading)); break;
            case 1: StartCoroutine(LoadingDB(index, CardImageDB.LoadDB, OnDBLoading)); break;
            case 2: StartCoroutine(LoadingDB(index, CardSymbolDB.LoadDB, OnDBLoading)); break;
            case 3: StartCoroutine(LoadingDB(index, CardClanDB.LoadDB, OnDBLoading)); break;
            case 4: StartCoroutine(LoadingDB(index, CardTypeDB.LoadDB, OnDBLoading)); break;
            case 5: StartCoroutine(LoadingDB(index, CardSubTypeDB.LoadDB, OnDBLoading)); break;
            case 6: StartCoroutine(LoadingDB(index, CardJobDB.LoadDB, OnDBLoading)); break;
            case 7: StartCoroutine(LoadingDB(index, CardTextureDB.LoadDB, OnDBLoading)); break;
            case 8: StartCoroutine(LoadingDB(index, DeckDB.LoadDB, OnDBLoading)); break;
            case 9: StartCoroutine(LoadingDB(index, DeckNameDB.LoadDB, OnDBLoading)); break;
            case 10: StartCoroutine(LoadingDB(index, AbilityDB.LoadDB, OnDBLoading)); break;
            case 11: StartCoroutine(LoadingDB(index, AbilityConditionDB.LoadDB, OnDBLoading)); break;
            case 12: StartCoroutine(LoadingDB(index, AbilityEffectDB.LoadDB, OnDBLoading)); break;
            case 13: StartCoroutine(LoadingDB(index, NPCDB.LoadDB, OnDBLoading)); break;
            case 14: StartCoroutine(LoadingDB(index, TutorialDB.LoadDB, OnDBLoading)); break;
            
            default: OnDBLoaded(); break;
        }
    }
    
    private void OnDBLoaded()
    {
        _stopTime = Time.realtimeSinceStartup;
        Debug.Log("Load time: " + (_stopTime - _startTime).ToString("0.000") + " seconds.");
        
        if(_onLoadAsyncComplete != null)
        {
            _onLoadAsyncComplete.Invoke();
        }
    }
    
    private IEnumerator LoadingDB(int index, LoadDBAction action, UnityAction<int> onComplete)
    {
        if(action != null)
        {
            action.Invoke();
        }
        
        yield return new WaitForSeconds(0.1f);
    
        if(onComplete != null)
        {
            onComplete.Invoke(++index);
        }
        
        yield return null;
    }
	#endregion
}
