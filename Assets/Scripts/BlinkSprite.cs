﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

[RequireComponent(typeof(UISprite))]
public class BlinkSprite : MonoBehaviour 
{
	public Color StartColor = Color.white;
	public Color EndColor = Color.white;
	public float BlinkTime = 1.0f;
	public bool PauseWhileDisabling = false;
	public Ease EaseType = Ease.Linear;

	private Tween _t = null;

	// Use this for initialization
	void Start () 
	{
		//StartBlink ();
	}

	// Update is called once per frame
	void Update () 
	{

	}

	void OnEnable()
	{
		if (PauseWhileDisabling || _t == null) 
		{
			StartBlink ();
		} 
	}

	void OnDisable()
	{
		if (PauseWhileDisabling && _t != null) 
		{
			StopBlink ();
		} 
	}

	private void StartBlink()
	{   
		if (_t == null) 
		{
			//Debug.Log("Start Blink.");
			UISprite sprite = GetComponent<UISprite> ();
			sprite.color = StartColor;

			_t = DOTween.To (
				() => sprite.color
				, x => sprite.color = x
				, EndColor
				, BlinkTime
			);

			_t.SetLoops (-1, LoopType.Yoyo);
			_t.SetEase (EaseType);
			_t.SetAutoKill (false);
			_t.Play ();
		} 
		else 
		{
			_t.Restart();
		}
	}

	private void StopBlink()
	{  
		_t.Pause ();
	}
}
