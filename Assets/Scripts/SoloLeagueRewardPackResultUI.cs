﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoloLeagueRewardPackResultUI : MonoBehaviour {

	public List<FullCardUI> FullCardList;
	public List<GameObject> GlowList;
	public List<GameObject> NewLabelList;
    public UILabel lblHeaderText;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ShowUI(string sAmtCard)
	{
        string headText = "";
        string phural = "s";

        if (sAmtCard == "1")
        {
            phural = "";
        }

		gameObject.SetActive (true);

		BattleUIManager.RequestBringForward (gameObject);

        headText = string.Format(Localization.Get("SHOP_NEW_CARD_HEADER"),sAmtCard,phural);
        lblHeaderText.text = headText;
        Debug.Log(headText);
		foreach(FullCardUI card in FullCardList)
		{
			card.gameObject.SetActive (false);
		}

		foreach(GameObject glow in GlowList)
		{
			glow.SetActive (false);
		}

		foreach(GameObject newLabel in NewLabelList)
		{
			newLabel.SetActive (false);
		}


	}

	public void ShowCard(int index, PlayerCardData data, bool isGlow, bool isNew, FullCardUI.OnClickEvent onClickedPreview)
	{
		FullCardList[index].SetCardData (data);
		FullCardList [index].gameObject.SetActive (true);
		GlowList[index].SetActive (isGlow);
		NewLabelList[index].SetActive (isNew);
		FullCardList [index].RequestBringForward (false);

		FullCardList [index].RemoveAllOnClickCallback ();
		FullCardList [index].BindOnClickCallback (onClickedPreview);
	}

	public void HideUI()
	{
		gameObject.SetActive (false);
	}


}
