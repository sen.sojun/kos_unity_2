﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TutorialDetailUI : MonoBehaviour 
{
	#region Delegates 
	public delegate void OnCloseDetail();
	#endregion

	public UITexture TutorialTexture;
	public UIButton NextPageButton;
	public UIButton PrevPageButton;
	public UILabel PageLabel;
	public UILabel TutorialDetail;
	public UILabel TutorialTitle;

	private List<Texture> _tutorialTextureList;
	private List<string> _tutorialText;
	private string _tutorialTitle;
	private OnCloseDetail _onCloseDetail = null;

	private int __pageIndex;
	private int _pageIndex
	{
		get { return __pageIndex; }
		set 
		{
			__pageIndex = value;
			if (PrevPageButton != null) 
			{
				PrevPageButton.gameObject.SetActive ((__pageIndex != 0));
			}
			if (NextPageButton != null)
			{
				if (_tutorialTextureList != null) {
					NextPageButton.gameObject.SetActive ((__pageIndex + 1 < _tutorialTextureList.Count));
				} 
				else {
					NextPageButton.gameObject.SetActive (false);
				}
			}

			ShowCurrentPage();
		}
	}

	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
	}
		
	public void ShowUI(string tutorialTitle, List<Texture> tutorialImageList, List<string> tutorialText, OnCloseDetail onCloseDetail)
	{
		_tutorialTitle = tutorialTitle;
		_tutorialTextureList = new List<Texture>(tutorialImageList);
		_tutorialText = new List<string>(tutorialText);

		_onCloseDetail = onCloseDetail;
		_pageIndex = 0;

		gameObject.SetActive(true);
	}

	public void HideUI()
	{
		gameObject.SetActive (false);
	}

	public void NextPage()
	{
		if (_tutorialTextureList != null) 
		{
			if (_pageIndex < _tutorialTextureList.Count) 
			{
				++_pageIndex;
			}
		} 
		else 
		{
			_pageIndex = 0;
		}

	}

	public void PreviousPage()
	{
		if (_pageIndex > 0) 
		{
			--_pageIndex;
		}
	}

	private void ShowCurrentPage()
	{
		if (_tutorialTextureList != null) 
		{
			TutorialTexture.mainTexture = _tutorialTextureList [_pageIndex];

			string text = _tutorialText [_pageIndex].Replace("\\n", "\n"); // New line
			TutorialDetail.text = text; 
			TutorialTitle.text = _tutorialTitle;
			PageLabel.text = (_pageIndex + 1).ToString () + " / " + (_tutorialTextureList.Count.ToString()); 
		}
	}

	public void HideTutorial()
	{
		HideUI();

		if(_onCloseDetail != null)
		{
			_onCloseDetail.Invoke();
		}
	}
}
