﻿namespace GamePlayerUI
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public class HandUI : MonoBehaviour 
	{
		#region Delegates
		public delegate void OnClickHandCard(int id);
		#endregion

		#region Private Properties
		private List<HandCardUI> _handCardUIList = null;
		private bool _isShowPreview = false;
		private OnClickHandCard _onClickHandCard = null;
		private GameObject _handCardPrefab = null;

		private static HandUI _instance = null;
		#endregion

		#region Public Properties
		public FullCardPreviewUI FullCardPreview;
		public GameObject HandCardPrefab { get { return _handCardPrefab; } }

		public float CardOffset = 5.0f; // Offset between card.
		public bool IsShowPreview { get { return _isShowPreview; } }

		//public static HandUI Instance { get { return _instance; } }
		#endregion

		#region Awake
		void Awake()
		{
			_instance = this;

			_handCardPrefab = Resources.Load("Prefabs/HandCard_2") as GameObject;
			Debug.Assert (_handCardPrefab != null, "HandUI: Not found HandCard prefab."); 
		}
		#endregion

		#region Start
		// Use this for initialization
		void Start () 
		{
			// testing
			/*
			#if UNITY_EDITOR
			for(int i = 1; i <= 5; ++i)
			{
				UniqueCardData data = new UniqueCardData(
					  new PlayerCardData("C" + i.ToString("0000"))
					, PlayerIndex.One
					, CardZoneIndex.Hand
				);

				HandUI.CreateHandCardUI(data);
			}
			#endif
			*/
		}
		#endregion

		#region Update
		// Update is called once per frame
		void Update () 
		{
		}
		#endregion

		#region OnDestroy
		void OnDestroy() 
		{
			_instance = null;
		}
		#endregion

		#region Methods
		private bool _CreateHandCardUI(UniqueCardData data)
		{
			if (_handCardUIList == null)
			{
				_handCardUIList = new List<HandCardUI>();
			}

			GameObject obj = Instantiate(HandCardPrefab, this.transform) as GameObject;
			obj.transform.localScale = Vector3.one;

			HandCardUI ui = obj.GetComponent<HandCardUI>();
			ui.SetCardData(data);
			ui.BindOnClickHandUI(HandUI.OnClickHandCardUI);
			ui.BindOnShowCardPreview(
				  this.ShowFullCardPreview
				, this.HideFullCardPreview
			);

			_handCardUIList.Add(ui);

			GameUtility.RequestBringForward(obj.gameObject, false);

			Reposition();

			return true;
		}

		private bool _DestroyHandCardUI(int uniqueCardID)
		{
			if (_handCardUIList != null && _handCardUIList.Count > 0)
			{
				for (int index = 0; index < _handCardUIList.Count; ++index)
				{
					if(_handCardUIList[index].UniqueCardID == uniqueCardID)
					{
						// found
						_handCardUIList.RemoveAt(index);

						return true;
					}
				}
			}

			return false;
		}

		private bool _GetHandCardUI(int uniqueCardID, out HandCardUI handCardUI)
		{
			if (_handCardUIList != null && _handCardUIList.Count > 0)
			{
				for (int index = 0; index < _handCardUIList.Count; ++index)
				{
					if (_handCardUIList[index].UniqueCardID == uniqueCardID)
					{
						handCardUI = _handCardUIList[index]; 
						return true;
					}
				}
			}

			handCardUI = null;
			return false;
		}

		private List<int> _GetAllHandCardUI()
		{
			List<int> uniqueIDList = new List<int> ();

			if (_handCardUIList != null && _handCardUIList.Count > 0)
			{
				foreach (HandCardUI handCardUI in _handCardUIList)
				{
					uniqueIDList.Add (handCardUI.UniqueCardID);
				}
			}

			return uniqueIDList;
		}

		private void _SetCanUse(int uniqueCardID, bool isCanUse)
		{
			HandCardUI handCardUI;
			if (_GetHandCardUI (uniqueCardID, out handCardUI))
			{
				// found
				handCardUI.SetCanUse(isCanUse);
			}
		}

		private void ShowFullCardPreview(PlayerCardData playerCardData)
		{
			FullCardPreview.ShowUI(playerCardData);
			_isShowPreview = true;
		}

		private void HideFullCardPreview()
		{
			FullCardPreview.HideUI();
			_isShowPreview = false;
		}

		private void Reposition()
		{
			if (_handCardUIList.Count <= 0) return; // Ignore empty hand. 

			float _cardWidth = HandCardPrefab.GetComponent<UIWidget>().width;
			float _cardHeight = HandCardPrefab.GetComponent<UIWidget>().height;

			float overallHandSize = (_handCardUIList.Count * _cardWidth) + ((_handCardUIList.Count - 1) * CardOffset);
			float centerOffset = (overallHandSize * 0.5f) - (_cardWidth * 0.5f);
			//Debug.Log(centerOffset);

			for (int index = 0; index < _handCardUIList.Count; ++index)
			{
				_handCardUIList[index].transform.localPosition = new Vector3((_cardWidth * index) + (CardOffset * index) - centerOffset - (_cardWidth * 0.5f), (_cardHeight * 0.5f), 0);
			}
		}
		#endregion

		#region Static Methods
		public static bool CreateHandCardUI(UniqueCardData data)
		{
			if (_instance == null) return false;

			return _instance._CreateHandCardUI(data);
		}

		public static bool DestroyHandCardUI(int uniqueCardID)
		{
			if (_instance == null) return false;

			return _instance._DestroyHandCardUI(uniqueCardID);
		}

		public static void SetCanUse(int uniqueCardID, bool isCanUse)
		{
			if (_instance == null) return;

			_instance._SetCanUse (uniqueCardID, isCanUse);
		}
			
		public static bool GetHandCardUI(int uniqueCardID, out HandCardUI handCardUI)
		{
			if (_instance == null)
			{
				handCardUI = null;
				return false;
			}

			return _instance._GetHandCardUI(uniqueCardID, out handCardUI);
		}

		public static List<int> GetAllHandCardUI()
		{
			List<int> uniqueIDList = new List<int> ();

			if (_instance == null)
			{
				return uniqueIDList;
			}

			return _instance._GetAllHandCardUI();
		}
		#endregion

		#region Event Methods
		public static void BindOnClickHandCard(OnClickHandCard onClickHandCard)
		{
			if (_instance == null) return;

			_instance._onClickHandCard = onClickHandCard;
		}

		public static void OnClickHandCardUI(int id)
		{
			if (_instance == null) return;

			Debug.Log ("OnClick HandCardUI " + id);

			if (_instance._onClickHandCard != null)
			{
				_instance._onClickHandCard.Invoke(id);
			}
		}
		#endregion
	}
}
