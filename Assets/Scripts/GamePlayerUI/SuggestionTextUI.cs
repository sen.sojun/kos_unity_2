﻿namespace GamePlayerUI
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	[RequireComponent(typeof(UILabel))]
	public class SuggestionTextUI : MonoBehaviour 
	{
		#region Private Properties
		private string text
		{
			get { return GetComponent<UILabel> ().text; }
			set { GetComponent<UILabel> ().text = value; }
		}

		private static SuggestionTextUI _instance = null;
		#endregion

		#region Public Properties
		//public static SuggestionTextUI Instance { get { return _instance; } }
		#endregion

		#region Awake
		void Awake()
		{
			_instance = this;
		}
		#endregion

		#region Start
		// Use this for initialization
		/*
		void Start () 
		{
		}
		*/
		#endregion

		#region Update
		// Update is called once per frame
		/*
		void Update () 
		{
		}
		*/
		#endregion

		#region OnDestroy
		void OnDestroy() 
		{
			_instance = null;
		}
		#endregion

		#region Methods 
		public void SetText(string text)
		{
			this.text = text;
		}

		public void ClearText()
		{
			this.text = "";
		}
		#endregion
	}
}
