﻿namespace GamePlayerUI
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public class PhaseUI : MonoBehaviour 
	{
		#region Delegates
		public delegate void OnClickNextPhase ();
		#endregion

		#region Private Properties
		private OnClickNextPhase _onClickNextPhase = null;

		private static PhaseUI _instance = null;
		#endregion

		#region Public Properties
		public PhaseIndicatorUI PhaseIndicatorUI;
		public UIButton NextPhaseButton;

		//public static PhaseUI Instance { get { return _instance; } }
		#endregion

		#region Awake
		void Awake()
		{
			_instance = this;

			{
				// Bind NextPhaseButton OnClick event to this.OnClickNextPhase
				EventDelegate del = new EventDelegate (this, "OnClickNextPhaseButton");
				EventDelegate.Set (NextPhaseButton.onClick, del);
			}
		}
		#endregion

		#region Start
		// Use this for initialization
		/*
		void Start () 
		{
		}
		*/
		#endregion

		#region Update
		// Update is called once per frame
		/*
		void Update () 
		{
		}
		*/
		#endregion

		#region OnDestroy
		void OnDestroy() 
		{
			_instance = null;
		}
		#endregion

		#region Methods
		public static void SetDisplayPhase(BattlePhase phase, DisplayPlayerIndex sideIndex)
		{
			if (_instance == null) return;

			SetDisplayPhase(GameUtility.BattlePhaseToDisplayBattlePhase(phase), sideIndex);
		}

		public static void SetDisplayPhase(DisplayBattlePhase phase, DisplayPlayerIndex sideIndex)
		{
			if (_instance == null) return;

			_instance.PhaseIndicatorUI.SetPhase(phase, sideIndex);
		}
		#endregion

		#region Event Methods
		public static void BindOnClickNextPhase(OnClickNextPhase onClickNextPhase)
		{
			if (_instance == null) return;

			_instance._onClickNextPhase = onClickNextPhase;
		}

		public void OnClickNextPhaseButton()
		{
			if (_onClickNextPhase != null)
			{
				_onClickNextPhase.Invoke ();
			}
		}
		#endregion
	}
}
