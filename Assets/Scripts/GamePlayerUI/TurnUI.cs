﻿namespace GamePlayerUI
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public class TurnUI : MonoBehaviour 
	{
		#region Private Properties
		private static TurnUI _instance = null;
		#endregion

		#region Public Properties
		public UILabel TurnLabel;

		//public static TurnUI Instance { get { return _instance; } }
		#endregion

		#region Awake
		void Awake()
		{
			_instance = this;
		}
		#endregion

		#region Start
		// Use this for initialization
		/*
		void Start () 
		{
		}
		*/
		#endregion

		#region Update
		// Update is called once per frame
		/*
		void Update () 
		{
		}
		*/
		#endregion

		#region OnDestroy
		void OnDestroy() 
		{
			_instance = null;
		}
		#endregion

		public static void SetTurn(int turnIndex)
		{
			if (_instance == null) return;

			_instance.TurnLabel.text = string.Format(
				  Localization.Get("BATTLE_TURN_INDEX")
				, turnIndex
			);
		}
	}
}
