﻿namespace GamePlayerUI
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
    using UnityEngine.U2D;

	public class HandCardUI : FullCardUI 
	{
		#region Delegates
		public delegate void OnClickHandCard(int uniqueCardID);
		public delegate void OnShowPreview(PlayerCardData playerCardData);
		public delegate void OnHidePreview();
		#endregion

		#region Private Properties
		private UniqueCardData _data = null;
		private float _pressTimer;
		private bool _isPressed = false;
		private bool _isHold = false;
		private bool _isSelect = false;
		private bool _isClickable = true;

		private OnClickHandCard _onClickHandCard = null;
		private OnShowPreview _onShowPreview = null;
		private OnHidePreview _onHidePreview = null;
		#endregion

		#region Public Properties
		public float HoldTime = 0.5f;
		public GameObject CanUseGlow;
		public GameObject DrimOverlay;
		public GameObject Arrow;

		public int UniqueCardID
		{
			get 
			{ 
				if (_data != null)
				{
					return _data.UniqueCardID; 
				} 
				else
				{
					return -1;
				}
			}
		}
		public bool IsClickable
		{
			set { _isClickable = value; }
			get { return _isClickable; }
		}
		public bool IsSelect
		{
			set 
			{ 
				_isSelect = value; 
				SetSelector();
			}
			get { return _isSelect; }
		}
		#endregion

		#region Awake
		void Awake()
		{
			this.ReinitDepth();

			ShowArrow(false);
			SetGlow(false);
			SetDrimOverlay(false);
		}
		#endregion

		#region Start
		// Use this for initialization
		/*
		void Start()
		{
		}
		*/
		#endregion

		#region Update
		// Update is called once per frame
		void Update()
		{
			if(_isPressed)
			{
				_pressTimer += Time.deltaTime;
				if(_pressTimer >= HoldTime)
				{
					_isPressed = false;
					OnStartHold();
				}
			}
		}
			
		public void SetCanUse(bool isActivable)
		{
			SetGlow(isActivable);
			SetDrimOverlay(!isActivable);
		}
		#endregion

		#region Methods
		public void SetCardData(UniqueCardData data)
		{
			_data = data;
			this.SetCardData(data.PlayerCardData);

			this.IsSelect = false;
		}

		private void SetSelector()
		{
		}

		private void SetGlow(bool isShow)
		{
			if(CanUseGlow.activeSelf != isShow)
			{
				CanUseGlow.SetActive(isShow);
				if(isShow)
				{
					GameUtility.RequestBringForward(CanUseGlow);
				}
			}
		}

		private void SetDrimOverlay(bool isShow)
		{
			if(DrimOverlay.activeSelf != isShow)
			{
				DrimOverlay.SetActive(isShow);
				if(isShow)
				{
					GameUtility.RequestBringForward(DrimOverlay);
				}
			}
		}

		public void ShowArrow(bool isShow)
		{
			//Debug.Log("HandCard| ShowArrow");

			if(Arrow == null) return;

			if(Arrow.activeSelf != isShow)
			{
				Arrow.SetActive(isShow);

				if(isShow)
				{
					GameUtility.RequestBringForward(Arrow);
				}
			}
		}
		#endregion

		#region Event Methods
		public void BindOnClickHandUI(OnClickHandCard onClickHandCard)
		{
			_onClickHandCard = onClickHandCard;
		}

		public void BindOnShowCardPreview(OnShowPreview onShowPreviewCallback, OnHidePreview onHidePreviewCallback)
		{
			_onShowPreview = onShowPreviewCallback;
			_onHidePreview = onHidePreviewCallback;
		}

		public override void OnClick()
		{
			/*
			if(!IsClickable || _isHold) return;

	        if (_onClickHandCard != null)
	        {
	            _onClickHandCard.Invoke(this);
	        }
	        */
		}

		private void OnClickHandUI()
		{
			if(!IsClickable || _isHold) return;

			if (_onClickHandCard != null)
			{
				_onClickHandCard.Invoke(UniqueCardID);
			}
		}

		/*
		public void OnHover()
		{
			Debug.Log("Hover" + this.PlayerCardData.Name);
		}
		*/

		/*
		public void OnDrag()
		{
			Debug.Log("OnDrag: " + this.PlayerCardData.Name);
		}
		*/

		public void OnDragOver(GameObject obj)
		{
			//Debug.Log("OnDragOver: me:" + this.PlayerCardData.Name + " obj:" + obj.name);

			if (_onShowPreview != null)
			{
				_onShowPreview.Invoke(this.PlayerCardData);
			}
		}

		public void OnPress(bool isDown)
		{
			if(!IsClickable) return;

			if(isDown)
			{
				// Start Press

				_pressTimer = 0.0f;
				_isPressed = true;
			}
			else
			{
				// Stop Press

				_isPressed = false;

				if(_isHold)
				{
					OnStopHold();
				}
				else
				{
					// Start Click
					OnClickHandUI();
				}
			}
		}

		private void OnStartHold()
		{
			_isHold = true;
			//Debug.Log ("HandCard[" + _myPlayerCardData.CardID + "] OnHold!");

			if(_onShowPreview != null)
			{
				_onShowPreview.Invoke(this.PlayerCardData);
			}
		}

		private void OnHold()
		{

		}

		private void OnStopHold()
		{
			_isHold = false;
			//Debug.Log ("HandCard[" + _myPlayerCardData.CardID + "] StopHold!");

			if(_onHidePreview != null)
			{
				_onHidePreview.Invoke();
			}
		}
		#endregion
	}
}
