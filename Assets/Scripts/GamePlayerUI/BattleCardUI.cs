﻿namespace GamePlayerUI
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public class BattleCardUI : MonoBehaviour
	{
		#region Delegates
		public delegate void OnClickBattleCard(int showBattleCardID);
		#endregion

		#region Private Properties
		private List<BattleCardData> _dataList = null;
		private int _showIndex = -1;
		private DisplayPlayerIndex _side;
		private OnClickBattleCard _onClickBattleCard = null;

		private bool _isClickable = true;
		private bool _isSelect = false;
		#endregion

		#region Public Properties
		public GameObject FrontCardGroup;
		public UIPanel CardImagePanel;

		public GameObject CardCountGroup;
		public UILabel CardCountLabel;

		public UILabel NameLabel;
		public UITexture CardTexture;
		public UISprite GlowBG;
		public UISprite SelectorGlow;

		public ActionIcon ActionIcon;
		public PassiveIconUI PassiveIconUI;

		public UILabel ShieldLabel;
		public UILabel HitPointLabel;
		public UILabel AttackLabel;
		public UILabel SpeedLabel;

		public GameObject BackCardGroup;
		public GameObject TrimCardGroup;
		public GameObject TrimOverlay;
		public GameObject TargetMark;
		public GameObject ArrowPanel;

		public Color PlayerGlowColor;
		public Color EnemyGlowColor;

		public int CardCount
		{
			get 
			{ 
				if(_dataList != null)
				{
					return _dataList.Count; 
				}

				return 0;
			}
		}
		public int ShowBattleCardID
		{
			get 
			{
				if (this.ShowingData != null)
				{
					return ShowingData.BattleCardID; 
				} 
				else
				{
					return -1;
				}
			}
		}
		public BattleCardData ShowingData
		{
			get 
			{
				if (_dataList != null && _dataList.Count > _showIndex)
				{
					return _dataList [_showIndex];
				} 
				else
				{
					return null;
				}
			}
		}
		public DisplayPlayerIndex Side
		{
			get { return _side; }
		}
		public OnClickBattleCard OnClickBattleCardCallback { get { return _onClickBattleCard; } }

		public bool IsFaceDown
		{
			get 
			{
				if (this.ShowingData != null)
				{
					return this.ShowingData.IsFaceDown; 
				} 
				else
				{
					return true;
				}
			}
		}
		public bool IsClickable
		{
			set 
			{ 
				_isClickable = value; 
				ShowTrim(!_isClickable);
			}
			get { return _isClickable; }
		}
		public bool IsSelect
		{
			set 
			{ 
				_isSelect = value; 
				SetSelector();
			}
			get { return _isSelect; }
		}
		#endregion

		#region Start
		// Use this for initialization
		void Start()
		{
			//NGUITools.NormalizePanelDepths();

			ShowSelector(false);
			ShowTrim(false);
		}
		#endregion

		#region Update
		// Update is called once per frame
		/*
		void Update () 
	    {
		
		}
		*/
		#endregion

		#region Methods
		public void SetSide(DisplayPlayerIndex side)
		{
			_side = side;
		}

		public void AddData(BattleCardData data, bool isShowThis = false)
		{
			//Debug.Log("AddBattleCard");
			if (_dataList == null)
			{
				_dataList = new List<BattleCardData>();
			}

			int dataIndex;
			if (IsContainID (data.BattleCardID, out dataIndex))
			{
				_dataList[dataIndex] = data;

				if (isShowThis)
				{
					_showIndex = dataIndex;
				}
			} 
			else
			{
				_dataList.Add (data);

				if (isShowThis)
				{
					_showIndex = (_dataList.Count - 1);
				}
			}

			UpdateDisplay ();
		}

		public bool RemoveData(int battleCardID)
		{
			if(_dataList != null && _dataList.Count > 0)
			{
				for (int index = 0; index < _dataList.Count; ++index)
				{
					if (_dataList[index].BattleCardID == battleCardID)
					{
						// found
						_dataList.RemoveAt(index);

						if (index == _showIndex)
						{
							if(_dataList.Count > 0)
							{
								_showIndex = 0;
							}
							else
							{
								_dataList.Clear();
								_showIndex = -1;
							}
						}

						return true;
					}
				}
			}

			return false;
		}

		public bool IsContainID(int battleCardID)
		{
			if(_dataList != null && _dataList.Count > 0)
			{
				foreach(BattleCardData data in _dataList)
				{
					if(data.BattleCardID == battleCardID) return true;
				}
			}

			return false;
		}

		private bool IsContainID(int battleCardID, out int dataIndex)
		{
			if(_dataList != null && _dataList.Count > 0)
			{
				int index = 0;
				foreach(BattleCardData data in _dataList)
				{
					if (data.BattleCardID == battleCardID)
					{
						dataIndex = index;
						return true;
					}
					++index;
				}
			}

			dataIndex = -1;
			return false;
		}

		public void UpdateData(BattleCardData data, bool isShowThis = false)
		{
			int dataIndex;
			if (IsContainID (data.BattleCardID, out dataIndex))
			{
				_dataList[dataIndex] = data;

				if (isShowThis)
				{
					_showIndex = dataIndex;
				}

				UpdateDisplay();
			} 
			else
			{
				Debug.LogError ("Data not registered to this UI.");
				return;
			}
		}
		#endregion

		#region UI Methods
		private void UpdateDisplay()
		{
			if(ShowingData == null || IsFaceDown)
			{
				FrontCardGroup.SetActive(false);
				CardImagePanel.alpha = 0.0f;

				BackCardGroup.SetActive(true);
			}
			else
			{
				FrontCardGroup.SetActive(true);
				CardImagePanel.alpha = 1.0f;

				BackCardGroup.SetActive(false);

				SetNameLabel();
				SetImageTexture();

				// Shield
				if(ShowingData.CurrentShield > 0)
				{
					ShieldLabel.text = ShowingData.CurrentShield.ToString();
				}
				else
				{
					ShieldLabel.text = (0).ToString();
				}

				// HP
				if(ShowingData.CurrentHP > 0)
				{
					HitPointLabel.text = ShowingData.CurrentHP.ToString();
				}
				else
				{
					HitPointLabel.text = (0).ToString();
				}

				// Attack
				if(ShowingData.CurrentAttack > 0)
				{
					AttackLabel.text = ShowingData.CurrentAttack.ToString();
				}
				else
				{
					AttackLabel.text = (0).ToString();
				}

				// Speed
				if(ShowingData.CurrentSpeed > 0)
				{
					SpeedLabel.text = ShowingData.CurrentSpeed.ToString();
				}
				else
				{
					SpeedLabel.text = (0).ToString();
				}
			}

			if(_side == DisplayPlayerIndex.Player)
			{
				GlowBG.color = PlayerGlowColor;
			}
			else
			{
				GlowBG.color = EnemyGlowColor;
			}

			UpdateCardCount();
			UpdatePassiveUI();
		}
			
		private void SetImageTexture()
		{
			Texture imageTexture;

			bool isSuccess = CardTextureDB.GetTexture(
				  ShowingData.ImageData.CardImageID
				, CardTextureDB.TextureSize.M
				, out imageTexture
			);

			if(isSuccess)
			{
				//Debug.Log(imageTexture.name);
				CardTexture.mainTexture = imageTexture;
			}
		}

		private void SetNameLabel()
		{
			string name = "";
			switch (LocalizationManager.CurrentLanguage)
			{
			case LocalizationManager.Language.Thai:
				{
					name = ShowingData.Name_TH;
					if (ShowingData.Alias_TH.Length > 0)
					{
						name += ", " + ShowingData.Alias_TH;
					}
				}
				break;

			case LocalizationManager.Language.English:
			default:
				{
					name = ShowingData.Name_EN;
					if (ShowingData.Alias_EN.Length > 0)
					{
						name += ", " + ShowingData.Alias_EN;
					}
				}
				break;
			}

			NameLabel.text = name;
		}

		private void UpdateCardCount()
		{
			//Debug.Log("UpdateCardCount : " + _battleCardIDList.Count);
			if(_dataList != null && _dataList.Count > 1)
			{
				// Grouping
				CardCountGroup.SetActive(true);
				CardCountLabel.text = "x" + _dataList.Count.ToString(); 

				GameUtility.RequestBringForward(CardCountGroup);
			}
			else
			{
				// Single
				CardCountGroup.SetActive(false);
			}
		}
			
		public void SetActionIcon(ActionIcon.ActionState actionState)
		{
			//Debug.Log("UpdateActionIcon");

			if (IsFaceDown)
			{
				ActionIcon.ShowActionIcon(ActionIcon.ActionState.None);
			} 
			else
			{
				ActionIcon.ShowActionIcon(actionState);
			}
		}

		private void UpdatePassiveUI()
		{
			if(IsFaceDown)
			{
				PassiveIconUI.gameObject.SetActive(false);
			}
			else
			{
				PassiveIconUI.SetPassiveIndex(ShowingData.CurrentPassiveIndex);
				PassiveIconUI.gameObject.SetActive(true);
			}
		}
			
		private void SetSelector()
		{
			/*
			if(IsSelect)
			{
				Debug.Log("Select: Battle ID[" + this.BattleCardID.ToString() + "].");
			}
			else
			{
				Debug.Log("Unselect: Battle ID[" + this.BattleCardID.ToString() + "].");
			}
			*/

			ShowSelector(IsSelect);
		}

		private void ShowTrim(bool isShow)
		{
			TrimCardGroup.SetActive(isShow);
			TargetMark.SetActive(false);
			TrimOverlay.SetActive(isShow);

			if(isShow)
			{
				BattleUIManager.RequestBringForward(TrimCardGroup);
				BattleUIManager.RequestBringForward(TrimOverlay);

				SetActionIcon(ActionIcon.ActionState.None);
			}
		}

		public void ShowArrow(bool isShow)
		{
			//TrimCardGroup.SetActive(isShow);
			ArrowPanel.gameObject.SetActive(isShow);

			if(isShow)
			{
				GameUtility.RequestBringForward(ArrowPanel.gameObject);
			}

			//Debug.Log("SHOW ARROW " + isShow);
		}

		public void ShowTargetMark(bool isShow)
		{
			TrimCardGroup.SetActive(isShow);
			TargetMark.SetActive(isShow);
			TrimOverlay.SetActive(false);

			if(isShow)
			{
				GameUtility.RequestBringForward(TrimCardGroup);
				GameUtility.RequestBringForward(TargetMark);

				SetActionIcon(ActionIcon.ActionState.None);
			}
		}

		private void ShowSelector(bool isShow)
		{
			SelectorGlow.gameObject.SetActive(isShow);
			if(isShow)
			{
				GameUtility.RequestBringForward(SelectorGlow.gameObject, false);
			}
		}
		#endregion

		#region Event Methods
		private void OnClick()
		{
			if(!IsClickable || (ShowingData == null)) return;

			OnClickBattleCardUI ();
		}

		public void BindOnClickBattleCardUI(OnClickBattleCard onClickBattleCard)
		{
			_onClickBattleCard = onClickBattleCard;
		}

		private void OnClickBattleCardUI()
		{
			if (_onClickBattleCard != null)
			{
				_onClickBattleCard.Invoke (ShowingData.BattleCardID);
			}

			//GamePlayerUI.BattlefieldUI.OnClickBattleCardUI(ShowingData.BattleCardID);
		}
		#endregion
	}
}

