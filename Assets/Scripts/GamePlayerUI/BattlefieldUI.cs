﻿namespace GamePlayerUI
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	#region DisplayBattleIndex Enum
	public enum DisplayBattleIndex : int
	{
		  PlayerBase    = 0
		, Battlefield   = 1
		, EnemyBase     = 2
	}
	#endregion

	#region BattlefieldUI Class
	public class BattlefieldUI : MonoBehaviour 
	{
		#region Delegates
		public delegate void OnClickBattleCard(int id);
		#endregion

		#region Private Properties
		private OnClickBattleCard _onClickBattleCard = null;
		private GameObject _battleCardPrefab = null;

		private static BattlefieldUI _instance = null;
		#endregion

		#region Public Properties
		public FieldUI EnemyBase;
		public FieldUI Battlefield;
		public FieldUI PlayerBase;

		public static GameObject BattleCardPrefab 
		{ 
			get 
			{ 
				if (_instance != null)
				{
					return _instance._battleCardPrefab; 
				} 
				else
				{
					return null;
				}
			} 
		}
		//public static BattlefieldUI Instance { get { return _instance; } }
		#endregion

		#region Awake
		void Awake()
		{
			_instance = this;

			_battleCardPrefab = Resources.Load("Prefabs/BattleCard_2") as GameObject;
			Debug.Assert (_battleCardPrefab != null, "BattlefieldUI: Not found BattleCard prefab."); 
		}
		#endregion

		#region Start
		// Use this for initialization
		void Start () 
		{
			// testing
			/*
			#if UNITY_EDITOR
			for (int i = 1; i <= 2; ++i)
			{
				foreach (DisplayBattleIndex battleIndex in System.Enum.GetValues(typeof(DisplayBattleIndex)))
				{
					foreach (DisplayPlayerIndex sideIndex in System.Enum.GetValues(typeof(DisplayPlayerIndex)))
					{
						BattleZoneIndex battleZone;
						switch(battleIndex)
						{
							case DisplayBattleIndex.PlayerBase:
								battleZone = BattleZoneIndex.BaseP1;
								break;

							case DisplayBattleIndex.EnemyBase:
								battleZone = BattleZoneIndex.BaseP2;
								break;

							case DisplayBattleIndex.Battlefield:
								battleZone = BattleZoneIndex.Battlefield;
								break;

							default:
								return;
						}

						PlayerIndex playerIndex;
						switch(sideIndex)
						{
							case DisplayPlayerIndex.Player:
								playerIndex = PlayerIndex.One;
								break;

							case DisplayPlayerIndex.Enemy:
								playerIndex = PlayerIndex.Two;
								break;

							default:
								return;
						}

						for (int j = 0; j < 2; ++j)
						{
							BattleCardData data = new BattleCardData (
								  new PlayerCardData ("C" + i.ToString ("0000"))
								, playerIndex
								, battleZone
								, false
								, false
	                     	);

							BattlefieldUI.CreateBattleCardUI (data, battleIndex, sideIndex);
						}
					}
				}
			}
			#endif
			*/
		}
		#endregion

		#region Update
		// Update is called once per frame
		void Update () 
		{
		}
		#endregion

		#region OnDestroy
		void OnDestroy() 
		{
			_instance = null;
		}
		#endregion

		#region Methods
		private bool _CreateBattleCardUI(BattleCardData data, DisplayBattleIndex battleIndex, DisplayPlayerIndex playerIndex)
		{
			switch(battleIndex)
			{
				case DisplayBattleIndex.PlayerBase:		return PlayerBase.CreateBattleCardUI (data, playerIndex, BattlefieldUI.OnClickBattleCardUI);
				case DisplayBattleIndex.Battlefield:	return Battlefield.CreateBattleCardUI (data, playerIndex, BattlefieldUI.OnClickBattleCardUI);
				case DisplayBattleIndex.EnemyBase:		return EnemyBase.CreateBattleCardUI (data, playerIndex, BattlefieldUI.OnClickBattleCardUI);
			}

			return false;
		}

		private bool _DestroyBattleCardUI(int battleCardID)
		{
			foreach (DisplayBattleIndex battleIndex in System.Enum.GetValues(typeof(DisplayBattleIndex)))
			{
				bool isDestroy = false;

				switch(battleIndex)
				{
					case DisplayBattleIndex.PlayerBase:		
						isDestroy = PlayerBase.DestroyBattleCardUI (battleCardID);
						break;

					case DisplayBattleIndex.Battlefield:	
						isDestroy = Battlefield.DestroyBattleCardUI (battleCardID);
						break;

					case DisplayBattleIndex.EnemyBase:		
						isDestroy = EnemyBase.DestroyBattleCardUI (battleCardID);
						break;
				}

				if (isDestroy)
				{
					return true;
				}
			}

			return false;
		}

		private bool _GetBattleCardUI(int battleCardID, out BattleCardUI battleCard)
		{
			bool isFound = false;

			isFound = Battlefield.GetBattleCardUI(battleCardID, out battleCard);
			if(isFound) return true;

			isFound = EnemyBase.GetBattleCardUI(battleCardID, out battleCard);
			if(isFound) return true;

			isFound = PlayerBase.GetBattleCardUI(battleCardID, out battleCard);
			if(isFound) return true;

			battleCard = null;
			return false;
		}

		private bool _GetBattleCardUI(int battleCardID, out BattleCardUI battleCard, out FieldUI fieldUI)
		{
			bool isFound = false;

			isFound = Battlefield.GetBattleCardUI(battleCardID, out battleCard);
			if (isFound)
			{
				fieldUI = Battlefield;
				return true;
			}

			isFound = EnemyBase.GetBattleCardUI(battleCardID, out battleCard);
			if (isFound)
			{
				fieldUI = EnemyBase;
				return true;
			}

			isFound = PlayerBase.GetBattleCardUI(battleCardID, out battleCard);
			if (isFound)
			{
				fieldUI = PlayerBase;
				return true;
			}

			battleCard = null;
			fieldUI = null;
			return false;
		}
			
		private bool _MoveBattleCardUI(int battleCardID, DisplayBattleIndex targetZone)
		{
			// Find UI.
			BattleCardUI ui;

			FieldUI currentField = null;
			bool isFound = _GetBattleCardUI(battleCardID, out ui, out currentField);
			if (!isFound)
			{
				Debug.LogError("FieldUIManager/MoveBattleCard: Not found battle card UI.");
				return false;
			}
				
			FieldUI targetField = null;
			switch (targetZone)
			{
				case DisplayBattleIndex.PlayerBase:
					targetField = PlayerBase;
					break;

				case DisplayBattleIndex.Battlefield:
					targetField = Battlefield;
					break;

				case DisplayBattleIndex.EnemyBase:
					targetField = EnemyBase;
					break;

				default:
					return false;
			}

			// Try move card from current field to target field.
			// If success return true, else return false.
			bool isSuccess = currentField.MoveBattleCardUITo(battleCardID, targetField);
			return isSuccess;
		}

		private bool _UpdateBattleCardUI(BattleCardData data, bool isShowThis = false)
		{
			// Find UI.
			BattleCardUI ui;

			bool isFound = _GetBattleCardUI(data.BattleCardID, out ui);
			if (!isFound)
			{
				Debug.LogError("FieldUIManager/MoveBattleCard: Not found battle card UI.");
				return false;
			}

			ui.UpdateData(data, isShowThis);

			return true;
		}
		#endregion

		#region Static Methods
		public static bool CreateBattleCardUI(BattleCardData data, DisplayBattleIndex battleIndex, DisplayPlayerIndex playerIndex)
		{
			if (_instance == null) return false;

			return _instance._CreateBattleCardUI (data, battleIndex, playerIndex);
		}

		public static bool DestroyBattleCardUI(int battleCardID)
		{
			if (_instance == null) return false;

			return _instance._DestroyBattleCardUI (battleCardID);
		}

		public static bool MoveBattleCardUI(int battleCardID, DisplayBattleIndex targetZone)
		{
			if (_instance == null) return false;

			return _instance._MoveBattleCardUI (battleCardID, targetZone);
		}

		public static bool UpdateBattleCardUI(BattleCardData data, bool isShowThis = false)
		{
			if (_instance == null) return false;

			return _instance._UpdateBattleCardUI (data, isShowThis);
		}
		#endregion

		#region Event Methods
		public static void BindOnClickBattleCard(OnClickBattleCard onClickBattleCard)
		{
			if (_instance == null) return;

			_instance._onClickBattleCard = onClickBattleCard;
		}

		public static void OnClickBattleCardUI(int id)
		{
			if (_instance == null) return;

			Debug.Log ("OnClick BattleCardUI " + id);

			if (_instance._onClickBattleCard != null)
			{
				_instance._onClickBattleCard.Invoke(id);
			}
		}
		#endregion
	}
	#endregion
}
