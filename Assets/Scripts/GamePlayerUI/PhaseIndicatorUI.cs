﻿namespace GamePlayerUI
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public class PhaseIndicatorUI : MonoBehaviour 
	{
		#region Public Properties
		public List<PhaseIconUI> PhaseIconList;
		#endregion

		#region Awake
		void Awake()
		{
			SetPhase(DisplayBattlePhase.GameSetup, DisplayPlayerIndex.Player);
		}
		#endregion

		#region Start
		// Use this for initialization
		/*
		void Start () 
		{
		}
		*/
		#endregion

		#region Update
		// Update is called once per frame
		/*
		void Update () 
		{
		}
		*/
		#endregion

		#region Methods
		public void SetPhase(DisplayBattlePhase displayPhase, DisplayPlayerIndex sideIndex)
		{
			int showIndex = 0;

			switch(displayPhase)
			{
				case DisplayBattlePhase.GameSetup:
				case DisplayBattlePhase.Begin:
				{
					showIndex = 0;
				}
				break;

				case DisplayBattlePhase.Draw:
				{
					showIndex = 1;
				}
				break;

				case DisplayBattlePhase.PreBattle:
				{
					showIndex = 2;
				}
				break;

				case DisplayBattlePhase.Battle:
				{
					showIndex = 3;
				}
				break;

				case DisplayBattlePhase.PostBattle:
				{
					showIndex = 4;
				}
				break;

				case DisplayBattlePhase.End:
				{
					showIndex = 5;
				}
				break;

				default:
				return;
			}

			PhaseIconUI.PlayerSide side;

			switch (sideIndex)
			{
				case DisplayPlayerIndex.Player:
					side = PhaseIconUI.PlayerSide.Player;
					break;

				case DisplayPlayerIndex.Enemy:
					side = PhaseIconUI.PlayerSide.Enemy;
					break;

				default:
					return;
			}
				
			for(int index = 0; index < PhaseIconList.Count; ++index)
			{
				if(index == showIndex)
				{
					PhaseIconList[index].SetShowIcon(true, side);
				}
				else
				{
					PhaseIconList[index].SetShowIcon(false, side);
				}
			}
		}
		#endregion
	}
}
