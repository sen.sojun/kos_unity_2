﻿namespace GamePlayerUI
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using DG.Tweening;

	public class PlayerInfoUI : MonoBehaviour 
	{
		#region Delegates
		public delegate void OnClickGraveyard(DisplayPlayerIndex displayPlayerIndex);
		public delegate void OnClickRemoveZone(DisplayPlayerIndex displayPlayerIndex);
		#endregion

		#region Private Properties
		private string _defaultImageName = "white_10x10";

		private int _gold = 0;
		private int _hp = 0;

		private OnClickGraveyard _onClickGraveyard = null;
		private OnClickRemoveZone _onClickRemoveZone = null;

		private Tween _goldTween = null;
		private Tween _hpTween = null;
		#endregion 

		#region Public Properties
		public UISprite PlayerImageSprite;

		public UILabel PlayerNameLabel;

		public UILabel HitPointLabel;
		public UILabel HandCardLabel;
		public UILabel GoldLabel;
		public UILabel DeckCardLabel;

		public UIButton GraveyardButton;
		public UIButton RemoveZoneButton;

		public DisplayPlayerIndex DisplayPlayerIndex;
		#endregion

		#region Awake
		void Awake () 
		{
			{
				// Bind GraveyardButton OnClick event to this.OnClickGraveyardButton
				EventDelegate del = new EventDelegate (this, "OnClickGraveyardButton");
				EventDelegate.Set (GraveyardButton.onClick, del);
			}
			{
				// Bind RemoveZoneButton OnClick event to this.OnClickRemoveButton
				EventDelegate del = new EventDelegate (this, "OnClickRemoveButton");
				EventDelegate.Set (RemoveZoneButton.onClick, del);
			}
		}
		#endregion

		#region Start
		// Use this for initialization
		/*
		void Start () 
		{
		}
		*/
		#endregion

		#region Update
		// Update is called once per frame
		/*
		void Update () 
		{
		}
		*/
		#endregion

		#region Methods
		public void SetProfile(PlayerBattleData playerBattleData)
		{
			SetPlayerName(playerBattleData.Name);
			SetPlayerImage(playerBattleData.ImageName);
			SetHitPoint(playerBattleData.HitPoint);
			SetHandNum(playerBattleData.HandCount);
			SetGoldNum(playerBattleData.Gold);
			SetDeckCardNum(playerBattleData.Deck.Count);
		}

		public void SetPlayerImage(string imageName)
		{
			if(PlayerImageSprite.atlas.GetSprite(imageName) != null)
			{
				PlayerImageSprite.spriteName = imageName;
			}
			else
			{
				PlayerImageSprite.spriteName = _defaultImageName;
			}
		}

		public void SetPlayerName(string playerName)
		{
			PlayerNameLabel.text = playerName.ToUpper();
		}

		public void SetHitPoint(int hitPoint)
		{
			if(_hp != hitPoint)
			{
				_hp = hitPoint;
				HitPointLabel.text = hitPoint.ToString();

				if(_hpTween == null)
				{
					_hpTween = HitPointLabel.transform.DOShakeScale(0.5f);
					_hpTween.SetAutoKill(false);
					_hpTween.Play();
				}
				else
				{
					_hpTween.Restart();
				}
			}
		}

		public void SetHandNum(int handCardAmount)
		{
			HandCardLabel.text = handCardAmount.ToString();
		}

		public void SetGoldNum(int goldCoin)
		{
			if(_gold != goldCoin)
			{
				_gold = goldCoin;
				GoldLabel.text = string.Format("{0:n0}", _gold);

				if(_goldTween == null)
				{
					_goldTween = GoldLabel.transform.DOShakeScale(0.5f);
					_goldTween.SetAutoKill(false);
					_goldTween.Play();
				}
				else
				{
					_goldTween.Restart();
				}
			}
		}

		public void SetDeckCardNum(int deckCardAmount)
		{
			DeckCardLabel.text = deckCardAmount.ToString();
		}
		#endregion

		#region Event Methods
		public void BindOnClickGraveyard(OnClickGraveyard onClickGraveyard)
		{
			_onClickGraveyard = onClickGraveyard;
		}

		public void BindOnClickRemoveZone(OnClickRemoveZone onClickRemoveZone)
		{
			_onClickRemoveZone = onClickRemoveZone;
		}
			
		public void OnClickGraveyardButton()
		{
			if(_onClickGraveyard != null)
			{
				_onClickGraveyard.Invoke(this.DisplayPlayerIndex);
			}
		}

		public void OnClickRemoveButton()
		{
			if(_onClickRemoveZone != null)
			{
				_onClickRemoveZone.Invoke(this.DisplayPlayerIndex);
			}
		}
		#endregion
	}
}
	
