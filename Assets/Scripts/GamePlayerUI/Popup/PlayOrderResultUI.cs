﻿namespace GamePlayerUI
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class PlayOrderResultUI : MonoBehaviour
    {

        #region Delegates
        public delegate void OnFinishShowResult();
        #endregion

        #region Private Properties
        private float _showTime = 1.5f;
        private float _timer = 0.0f;
        private bool _isShow = false;

        private OnFinishShowResult _onFinishShowResult = null;
        #endregion

        #region Public Properties
        public PlayOrderOptionUI MeOrderOption;
        #endregion

        #region Awake
        void Awake()
        {
            MeOrderOption.SetClickable(false);
        }
        #endregion

        #region Start
        // Use this for initialization
        /*
        void Start () 
        {
        }
        */
        #endregion

        #region Update
        // Update is called once per frame
        void Update()
        {
            if (_isShow)
            {
                _timer += Time.deltaTime;
                if (_timer >= _showTime)
                {
                    HideUI();
                }
            }
        }
        #endregion

        #region Methods
        public void ShowUI(PlayOrderType meOptionType, OnFinishShowResult onFinishShowResult)
        {
            // Set me sprite result
            MeOrderOption.SetPlayOrderType(meOptionType);
            MeOrderOption.SetClickable(false);

            _timer = 0.0f;
            _onFinishShowResult = onFinishShowResult;

            StartShowUI();
        }

        private void StartShowUI()
        {
            gameObject.SetActive(true);
            GameUtility.RequestBringForward(this.gameObject);

            _isShow = true;
        }

        private void HideUI()
        {
            StartHideUI();
        }

        private void StartHideUI()
        {
            _isShow = false;
            NGUITools.SetActive(gameObject, false);

            if (_onFinishShowResult != null)
            {
                _onFinishShowResult.Invoke();
            }
        }
        #endregion
    }
}