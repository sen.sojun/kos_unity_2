﻿namespace GamePlayerUI
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public class RPSResultUI : MonoBehaviour 
	{
		#region Delegates
		public delegate void OnFinishShowResult();
		#endregion

		#region Private Properties
		private float _showTime = 1.5f;
		private float _timer = 0.0f;
		private bool _isShow = false;

		private OnFinishShowResult _onFinishShowResult = null;
		#endregion

		#region Public Properties
		public UILabel ResultLabel;
        public RPSOptionUI MeRPSOption;
        public RPSOptionUI EnemyRPSOption;
		#endregion


		#region Awake
		void Awake()
		{
            MeRPSOption.SetClickable(false);
            EnemyRPSOption.SetClickable(false);
		}
		#endregion

		#region Start
		// Use this for initialization
		/*
		void Start () 
		{
		}
		*/
		#endregion

		#region Update
		// Update is called once per frame
		void Update () 
		{
			if (_isShow)
			{
				_timer += Time.deltaTime;
				if (_timer >= _showTime)
				{
					HideUI(); 
				}
			}
		}
		#endregion

		#region Methods
		public void ShowUI(RPSType meOptionType, RPSType enemyOptionType, OnFinishShowResult onFinishShowResult)
		{
			switch (GameUtility.Compare(meOptionType, enemyOptionType))
			{
				case RPSResult.Win: ResultLabel.text = Localization.Get("BATTLE_FINDFIRST_RESULT_WIN"); break;
				case RPSResult.Lose: ResultLabel.text = Localization.Get("BATTLE_FINDFIRST_RESULT_LOSE"); break;
				case RPSResult.Draw: ResultLabel.text = Localization.Get("BATTLE_FINDFIRST_RESULT_DRAW"); break;
			}

            // Set me sprite result
            MeRPSOption.SetRPSType(meOptionType);
            MeRPSOption.SetClickable(false);

            // set enemy sprite result
            EnemyRPSOption.SetRPSType(enemyOptionType);
            EnemyRPSOption.SetClickable(false);

			_timer = 0.0f;
			_onFinishShowResult = onFinishShowResult;

			StartShowUI();
		}

		private void StartShowUI()
		{
			gameObject.SetActive (true);
			GameUtility.RequestBringForward(this.gameObject);

			_isShow = true;
		}

		private void HideUI()
		{
			StartHideUI();
		}

		private void StartHideUI()
		{
			_isShow = false;
			NGUITools.SetActive(gameObject, false);

			if (_onFinishShowResult != null)
			{
				_onFinishShowResult.Invoke();
			}
		}
		#endregion
	}
}
