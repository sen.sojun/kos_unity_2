﻿namespace GamePlayerUI
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public enum PlayOrderType : int
    {
          PlayFirst = 0
        , DrawFirst = 1
    }

    [RequireComponent(typeof(UIButton))]
    public class PlayOrderOptionUI : MonoBehaviour
    {
        #region Delegates
        public delegate void OnClickPlayOrderOption(PlayOrderType optionType);
        #endregion

        #region Private Properties
        private OnClickPlayOrderOption _onClickPlayOrderOption = null;
        #endregion

        #region Public Properties
        public UILabel label;
        public PlayOrderType playOrderType;
        public bool IsClickable { get { return GetComponent<UIButton>().isEnabled; } }
        #endregion

        #region Awake
        void Awake()
        {
            {
                // Bind this UIButton OnClick event to this.OnClickRPSOptionUI
                EventDelegate del = new EventDelegate(this, "OnClickPlayOrderOptionUI");
                EventDelegate.Set(GetComponent<UIButton>().onClick, del);
            }
        }
        #endregion

        #region Start
        // Use this for initialization
        /*
        void Start () 
        {
        }
        */
        #endregion

        #region Update
        // Update is called once per frame
        /*
        void Update () 
        {
        }
        */
        #endregion

        #region Methods
        public void SetPlayOrderType(PlayOrderType playOrderType)
        {
            this.playOrderType = playOrderType;

            UILocalize localize = label.GetComponent<UILocalize>();

            switch (this.playOrderType)
            {
                case PlayOrderType.PlayFirst: localize.key = "BATTLE_FINDFIRST_PLAY_FIRST"; break;
                case PlayOrderType.DrawFirst: localize.key = "BATTLE_FINDFIRST_DRAW_FIRST"; break;
                default: localize.key = ""; break;
            }
        }

        public void SetClickable(bool isEnable)
        {
            GetComponent<UIButton>().isEnabled = isEnable;
        }
        #endregion

        #region Event Methods
        public void BindOnClickPlayOrderOptionUI(OnClickPlayOrderOption onClickPlayOrderOption)
        {
            _onClickPlayOrderOption = onClickPlayOrderOption;
        }

        private void OnClickPlayOrderOptionUI()
        {
            if (_onClickPlayOrderOption != null && IsClickable)
            {
                _onClickPlayOrderOption.Invoke(this.playOrderType);
            }
        }
        #endregion
    }
}
