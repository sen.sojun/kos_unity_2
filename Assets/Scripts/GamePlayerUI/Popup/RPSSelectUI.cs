﻿namespace GamePlayerUI
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public class RPSSelectUI : MonoBehaviour 
	{
		#region Delegates
		public delegate void OnClickRPSOption(RPSType rpsType);
		#endregion

		#region Private Properties
        private float _timer = 0.0f;
        private bool _isHaveTimeout = false;
		private OnClickRPSOption _onClickRPSOption = null;
		#endregion

		#region Public Properties
		public List<RPSOptionUI> OptionList;
		#endregion

		#region Awake
		void Awake()
		{
			foreach (RPSOptionUI rpsOptionUI in OptionList)
			{
				rpsOptionUI.BindOnClickRPSOptionUI(this.OnClickRPSOptionUI);
			}
		}
		#endregion

		#region Start
		// Use this for initialization
		/*
		void Start () 
		{
		}
		*/
		#endregion

		#region Update
		// Update is called once per frame
		void Update () 
		{
            if(_isHaveTimeout)
            {
                _timer -= Time.deltaTime;
                if(_timer < 0.0f)
                {
                    // timeout.                    
                    _isHaveTimeout = false;
                    AutoSelectRPS();
                }
            }
		}
		#endregion

		#region Methods
		public void ShowUI(OnClickRPSOption onClickRPSOption)
		{
			_onClickRPSOption = onClickRPSOption;
            _isHaveTimeout = BattleManager.Instance.IsHaveTimeout;
            _timer = BattleManager.Instance.PopupDecisionTime;

			gameObject.SetActive (true);
		}

		public void HideUI()
		{
			gameObject.SetActive (false);
		}
        
        private void AutoSelectRPS()
        {            
            OnClickRPSOptionUI(GameUtility.RandomRPS());
        }
		#endregion

		#region Event Methods
		public void OnClickRPSOptionUI (RPSType rpsType)
		{
            HideUI();

			if (_onClickRPSOption != null)
			{
				_onClickRPSOption.Invoke(rpsType);
			}
		}
		#endregion
	}
}
