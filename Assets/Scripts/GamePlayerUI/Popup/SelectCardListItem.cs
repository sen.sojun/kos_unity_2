﻿namespace GamePlayerUI
{
    using UnityEngine;
    using System.Collections;
    
    [RequireComponent(typeof(UIButton))]
    public class SelectCardListItem : MonoBehaviour 
    {
        public delegate void OnSelectItem(int index);
    
        private int _index = -1;
        private bool _isHighlight = false;
    
        private UIButton _button { get { return GetComponent<UIButton>(); } }
        private OnSelectItem _onSelectItem = null;
    
        public UILabel NameLabel;
    
        void Awake()
        {
        
        }
    
        // Use this for initialization
        /*
        void Start () 
        {   
        
        }
        */
        
        // Update is called once per frame
        void Update () 
        {   
            if(!_button.isEnabled)
            {
                NameLabel.color = Color.black;
            }
            else
            {
                NameLabel.color = Color.white;
            }
        }
        
        public void SetData(int index, string name, bool isHighlight, OnSelectItem onSelectItem)
        {
            _index = index;
            NameLabel.text = name;
            _isHighlight = isHighlight;
            _onSelectItem = onSelectItem;
        }

        public void SetIndex(int index)
        {
            _index = index;
        }
    
        public void SetCardName(string name)
        {
            NameLabel.text = name;
        }
        
        public void SetHighlight(bool isHighlight)
        {
            _isHighlight = isHighlight;
            UpdateHighlight();
        }
        
        public void BindOnSelectItem(OnSelectItem onSelectItem)
        {
            _onSelectItem = onSelectItem;
        }
    
        public void OnClick()
        {
            if(_onSelectItem != null)
            {
                _onSelectItem.Invoke(_index);
            }
        }
    
        private void UpdateHighlight()
        {
            //Debug.Log("SelectCard[" + _index.ToString() + "]: " + _isHighlight);
    
            _button.isEnabled = !_isHighlight;
        }
    }
}
