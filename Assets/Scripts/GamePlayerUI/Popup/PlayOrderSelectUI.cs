﻿namespace GamePlayerUI
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class PlayOrderSelectUI : MonoBehaviour
    {
        #region Delegates
        public delegate void OnClickPlayOrderOption(PlayOrderType orderType);
        #endregion

        #region Private Properties
        private float _timer = 0.0f;
        private bool _isHaveTimeout = false;
        private OnClickPlayOrderOption _onClickPlayOrderOption = null;
        #endregion

        #region Public Properties
        public PlayOrderOptionUI PlayFirstButton;
        public PlayOrderOptionUI DrawFirstButton;
        #endregion

        #region Awake
        void Awake()
        {
            PlayFirstButton.playOrderType = PlayOrderType.PlayFirst;
            DrawFirstButton.playOrderType = PlayOrderType.DrawFirst;

            PlayFirstButton.BindOnClickPlayOrderOptionUI(this.OnClickPlayOrderOptionUI);
            DrawFirstButton.BindOnClickPlayOrderOptionUI(this.OnClickPlayOrderOptionUI);
        }
        #endregion

        #region Start
        // Use this for initialization
        /*
        void Start () 
        {
        }
        */
        #endregion

        #region Update
        // Update is called once per frame
        void Update () 
        {
             if(_isHaveTimeout)
            {
                _timer -= Time.deltaTime;
                if(_timer < 0.0f)
                {
                    // timeout.   
                    _isHaveTimeout = false;
                    AutoSelectPlayOrderOption();
                }
            }
        }
        #endregion

        #region Methods
        public void ShowUI(bool isSelectable, OnClickPlayOrderOption onClickPlayOrderOption)
        {
            if (isSelectable)
            {
                _isHaveTimeout = BattleManager.Instance.IsHaveTimeout;
                _timer = BattleManager.Instance.PopupDecisionTime;
                
                PlayFirstButton.SetClickable(true);

                if (BattleManager.Instance.IsTutorial)
                {
                    DrawFirstButton.SetClickable(false);

                    Transform glow = PlayFirstButton.transform.Find("Glow");
                    glow.gameObject.SetActive(true);

                    Transform arrow = PlayFirstButton.transform.Find("Arrow");
                    arrow.gameObject.SetActive(true);
                }
                else
                {
                    DrawFirstButton.SetClickable(true);

                    Transform glow = PlayFirstButton.transform.Find("Glow");
                    glow.gameObject.SetActive(false);

                    Transform arrow = PlayFirstButton.transform.Find("Arrow");
                    arrow.gameObject.SetActive(false);
                }

                _onClickPlayOrderOption = onClickPlayOrderOption;
            }
            else
            {
                _isHaveTimeout = false;
                _timer = 0.0f;
                
                PlayFirstButton.SetClickable(false);
                DrawFirstButton.SetClickable(false);
                
                Transform glow = PlayFirstButton.transform.Find("Glow");
                glow.gameObject.SetActive(false);

                Transform arrow = PlayFirstButton.transform.Find("Arrow");
                arrow.gameObject.SetActive(false);
                
                _onClickPlayOrderOption = null;
            }
            gameObject.SetActive(true);
        }

        public void HideUI()
        {
            gameObject.SetActive(false);
        }
        
        private void AutoSelectPlayOrderOption()
        {
            OnClickPlayOrderOptionUI(PlayOrderType.PlayFirst);
        }
        #endregion

        #region Event Methods
        public void OnClickPlayOrderOptionUI(PlayOrderType orderType)
        {
            HideUI();

            if(_onClickPlayOrderOption != null)
            {
                _onClickPlayOrderOption.Invoke(orderType);
            }
        }
        #endregion
    }
}
