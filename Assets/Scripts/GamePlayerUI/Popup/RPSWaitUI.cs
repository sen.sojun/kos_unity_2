﻿namespace GamePlayerUI
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public class RPSWaitUI : MonoBehaviour 
	{
		#region Delegates
		#endregion

		#region Private Properties
		#endregion

		#region Public Properties
        public RPSOptionUI SelectedOption;
		#endregion

		#region Awake
		void Awake()
		{
		}
		#endregion

		#region Start
		// Use this for initialization
		/*
		void Start () 
		{
		}
		*/
		#endregion

		#region Update
		// Update is called once per frame
		/*
		void Update () 
		{
		}
		*/
		#endregion

		#region Methods
		public void ShowUI(RPSType rpsType)
		{
            SelectedOption.SetRPSType(rpsType);
            SelectedOption.SetClickable(false);

			gameObject.SetActive (true);
		}

		public void HideUI()
		{
			gameObject.SetActive (false);
		}
		#endregion
	}
}
