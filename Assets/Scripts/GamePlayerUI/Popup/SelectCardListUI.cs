﻿namespace GamePlayerUI
{
    using UnityEngine;
    using System.Collections;
    using System.Collections.Generic;
    
    [RequireComponent(typeof(UIWidget))]
    public class SelectCardListUI : MonoBehaviour 
    {
        public delegate void OnConfirmSelectCard(UniqueCardData selectedPlayerCard);
    
        private float _timer = 0.0f;
        private bool _isHaveTimeout = false;
        private List<UniqueCardData> _dataList;
        private List<SelectCardListItem> _itemList;
        private int _selectedIndex = -1;
    
        private OnConfirmSelectCard _onConfirmSelectCard;
    
        public UIScrollView ScrollView;
        public UIGrid UIGrid;
        public UILabel HeaderLabel;
        public FullCardUI FullCardPreview;
    
        public GameObject SelectCardButtonPrefab;
    
        void Awake()
        {
            //ClearAllItem();
        }
    
        // Use this for initialization
        /*
        void Start () 
        {
            
        }
        */
        
        // Update is called once per frame
        void Update () 
        {
            if(_isHaveTimeout)
            {
                _timer -= Time.deltaTime;
                if(_timer < 0.0f)
                {
                    // timeout.   
                    _isHaveTimeout = false;
                    AutoSelectCardList();
                }
            }
        }
    
        public void OnEnable()
        {
            ScrollView.ResetPosition();
            //ScrollView.UpdatePosition();
        }
    
        public void ShowUI(string headerTextKey, List<UniqueCardData> dataList, OnConfirmSelectCard callback)
        {
            //Debug.Log("Deck:" + cardDataList.Count.ToString());
    
            SetDataList(dataList);
            SetHeaderLabel(headerTextKey);
            _onConfirmSelectCard = callback;
            _isHaveTimeout = BattleManager.Instance.IsHaveTimeout;
            _timer = BattleManager.Instance.PopupDecisionTime;
    
            NGUITools.SetActive(gameObject, true);
    
            if(BattleManager.Instance.IsTutorial)
            {
                Transform confirmButton = transform.Find("ConfirmButton");
                if(confirmButton != null)
                {
                    Transform arrow = confirmButton.Find("Arrow");
                    if(arrow != null)
                    {
                        arrow.gameObject.SetActive(true);
                    }
                }
            }
            else
            {
                Transform confirmButton = transform.Find("ConfirmButton");
                if(confirmButton != null)
                {
                    Transform arrow = confirmButton.Find("Arrow");
                    if(arrow != null)
                    {
                        arrow.gameObject.SetActive(false);
                    }
                }
            }
    
            BattleUIManager.RequestBringForward(this.gameObject);
        }
    
        private void SetHeaderLabel(string headerTextKey)
        {
            HeaderLabel.GetComponent<UILocalize>().key = headerTextKey;
        }
    
        private void SetDataList(List<UniqueCardData> dataList)
        {
            _selectedIndex = -1;
            _dataList = new List<UniqueCardData>(dataList);
           
            GenerateDisplayList();
            OnSelectCard(0);
        }
        
        public void ClearAllItem()
        {
            foreach (Transform child in UIGrid.transform) 
            {
                GameObject.Destroy(child.gameObject);
            }
            
            if(_itemList != null)
            {
                _itemList.Clear();
            }
        }
    
        private void GenerateDisplayList()
        {
            ClearAllItem();
            
             _itemList = new List<SelectCardListItem>();
        
            ScrollView.ResetPosition();
            ScrollView.UpdatePosition();
    
            for(int index = 0; index < _dataList.Count; ++index)
            {
                GameObject obj = Instantiate(SelectCardButtonPrefab) as GameObject;
                SelectCardListItem item = obj.GetComponent<SelectCardListItem>();
    
                // Init value
                string cardName = "";
                switch (LocalizationManager.CurrentLanguage)
                {
                    case LocalizationManager.Language.Thai:
                    {
                        cardName = _dataList[index].PlayerCardData.Name_TH;
                    }
                    break;
    
                    case LocalizationManager.Language.English:
                    default:
                    {
                        cardName = _dataList[index].PlayerCardData.Name_EN;
                    }
                    break;
                }
    
                item.SetData(index, cardName, false, this.OnSelectCard);
    
                // Set hierarchy
                item.transform.parent = UIGrid.transform;
                item.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
    
                _itemList.Add(item);
            }
    
            this.UIGrid.transform.localPosition = Vector3.zero;
                
            this.UIGrid.Reposition();
        }
    
        public void OnSelectCard(int index)
        {
            //Debug.Log("OnSelectCard:" + index.ToString());
            if(index != _selectedIndex && index < _itemList.Count)
            {
                // Remove prev-highlight.
                if(_selectedIndex >= 0 && _selectedIndex < _itemList.Count)
                {
                    _itemList[_selectedIndex].SetHighlight(false);
                }
    
                // Set new index highlight.
                _selectedIndex = index;
                _itemList[_selectedIndex].SetHighlight(true);
    
                // Set preview card.
                DisplayFullCard(_dataList[_selectedIndex]);
            }
        }
    
        private void DisplayFullCard(UniqueCardData data)
        {
            FullCardPreview.SetCardData(data.PlayerCardData);
            FullCardPreview.RequestBringForward();
        }
    
        public void OnClickConfirm()
        {
            if(_selectedIndex >= 0)
            {
                OnConfirmSelectCardList(_selectedIndex);
            }
            else
            {
                Debug.LogError("SelectCardListUI/OnClickConfirm: Select empty card.");
            }
    
        }
        
        private void OnConfirmSelectCardList(int index)
        {
            HideUI();
    
            if(_onConfirmSelectCard != null)
            {
                _onConfirmSelectCard.Invoke(_dataList[index]);
            }
        }
    
        private void HideUI()
        {
            ScrollView.ResetPosition();
            ScrollView.UpdatePosition();
            ScrollView.currentMomentum = Vector3.zero;
    
            ClearAllItem();
             
            this.UIGrid.transform.localPosition = Vector3.zero;
            //ScrollView.ResetPosition();
    
            NGUITools.SetActive(gameObject, false);
        }
        
        private void AutoSelectCardList()
        {
            OnConfirmSelectCardList(0);
        }
    }
}
