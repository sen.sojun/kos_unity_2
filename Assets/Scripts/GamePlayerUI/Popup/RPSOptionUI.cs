﻿namespace GamePlayerUI
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

    public enum RPSType
    {
          Rock = 0
        , Paper = 1
        , Scissors = 2
    }

    public enum RPSResult : int
    {
          Lose = -1
        , Draw = 0
        , Win = 1
    }

	[RequireComponent(typeof(UIButton))]
	public class RPSOptionUI : MonoBehaviour 
	{
		#region Delegates
		public delegate void OnClickRPSOption(RPSType rpsType);
		#endregion

		#region Private Properties
		private OnClickRPSOption _onClickRPSOption = null;
        #endregion

        #region Public Properties
        public RPSType rpsType;
        public bool IsClickable { get { return GetComponent<UIButton>().isEnabled; } }
        #endregion

        #region Awake
        void Awake()
		{
			{
				// Bind this UIButton OnClick event to this.OnClickRPSOptionUI
				EventDelegate del = new EventDelegate (this, "OnClickRPSOptionUI");
				EventDelegate.Set (GetComponent<UIButton>().onClick, del);
			}
		}
		#endregion

		#region Start
		// Use this for initialization
		/*
		void Start () 
		{
		}
		*/
		#endregion

		#region Update
		// Update is called once per frame
		/*
		void Update () 
		{
		}
		*/
		#endregion

		#region Event Methods
		public void BindOnClickRPSOptionUI(OnClickRPSOption onClickRPSOption)
		{
			_onClickRPSOption = onClickRPSOption;
		}

        public void SetRPSType(RPSType rpsOptionType)
        {
            this.rpsType = rpsOptionType;

            switch (this.rpsType)
            {
                case RPSType.Rock:
                {
                    GetComponent<UISprite>().spriteName = "Rock";
                }
                break;

                case RPSType.Scissors:
                {
                    GetComponent<UISprite>().spriteName = "Scissors";
                }
                break;

                case RPSType.Paper:
                {
                    GetComponent<UISprite>().spriteName = "Paper";
                }
                break;
            }
        }

        public void SetClickable(bool isEnable)
        {
            GetComponent<UIButton>().isEnabled = isEnable;
        }

		private void OnClickRPSOptionUI()
		{
            if (_onClickRPSOption != null && IsClickable)
			{
				_onClickRPSOption.Invoke(this.rpsType);
			}
		}
		#endregion
	}
}
