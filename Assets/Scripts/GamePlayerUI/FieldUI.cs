﻿namespace GamePlayerUI
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public class FieldUI : MonoBehaviour 
	{			
		#region Private Properties
		private List<GamePlayerUI.BattleCardUI> _playerCardObjList; // list of player card object
		private List<GamePlayerUI.BattleCardUI> _enemyCardObjList; // list of enemy card object

		private float _currentWidth = 0.0f;
		private int _tabIndex = 0;
		private bool _isOverflow = false;
		#endregion

		#region Public Properties
		public UIPanel ClippingPanel;
		public GameObject Content;
		public UIButton ShiftLeftButton;
		public UIButton ShiftRightButton;

		public float CardOffset = 5.0f; // Offset between card.
		public float CardWidth = 170.0f;
		public float BorderWidth = 5.0f; // Border of card list.

		private Vector3 ContentLocalPos
		{
			get
			{ 
				return Content.transform.localPosition; 
			}
			set
			{
				Content.transform.localPosition = value;

				// Redraw it clipping panel
				ClippingPanel.Invalidate(true);
			}
		}

		public float ContentMinPosX
		{
			get
			{
				if(_isOverflow)
				{
					return ContentMaxPosX - (_currentWidth - MinSize.x);
				}

				return ContentMaxPosX;
			}
		}

		public float ContentMaxPosX
		{
			get
			{
				return ShiftLeftButton.GetComponent<UISprite>().localSize.x;
			}
		}

		public Vector2 MinSize
		{
			get 
			{
				return ClippingPanel.GetViewSize();
			}
		}
		#endregion

		#region Awake
		void Awake()
		{
			{
				// Bind ShiftLeftButton OnClick event to this.OnGoLeft
				EventDelegate del = new EventDelegate (this, "OnGoLeft");
				EventDelegate.Set (ShiftLeftButton.onClick, del);
			}
			{
				// Bind ShiftRightButton OnClick event to this.OnGoRight
				EventDelegate del = new EventDelegate (this, "OnGoRight");
				EventDelegate.Set (ShiftRightButton.onClick, del);
			}

			_playerCardObjList = new List<BattleCardUI> ();
			_enemyCardObjList = new List<BattleCardUI> ();

			Reposition ();
		}
		#endregion

		#region Start
		// Use this for initialization
		/*
		void Start () 
		{
		}
		*/
		#endregion

		#region Update
		// Update is called once per frame
		void Update () 
		{
			UpdateContentPosition ();
		}
		#endregion

		#region Methods
		public bool CreateBattleCardUI(BattleCardData data, DisplayPlayerIndex playerIndex, BattleCardUI.OnClickBattleCard onClickBattleCard)
		{
			// create-able flag
			bool isCanCreate = true;

			List<BattleCardUI> targetSide = GetSideList(playerIndex);

			// Check can grouping
			if(targetSide != null && targetSide.Count > 0)
			{
				for(int index = 0; index < targetSide.Count; ++index)
				{
					BattleCardData showData = targetSide[index].ShowingData;
					if(showData.TypeData == CardTypeData.CardType.Structure)
					{
						// Structure
						if(showData.PlayerCardData.PlayerCardID == data.PlayerCardData.PlayerCardID)
						{
							// Same card type
							targetSide[index].AddData(data);
							isCanCreate = false;
							break;
						}
					}
				}
			}

			if (isCanCreate)
			{
				_CreateBattleCardUI (data, playerIndex, onClickBattleCard);
				return true;
			} 
			else
			{
				return false;
			}
		}

		private void _CreateBattleCardUI(BattleCardData data, DisplayPlayerIndex playerIndex, BattleCardUI.OnClickBattleCard onClickBattleCard)
		{
			GameObject obj = Instantiate(BattlefieldUI.BattleCardPrefab) as GameObject;
			GamePlayerUI.BattleCardUI ui = obj.GetComponent<GamePlayerUI.BattleCardUI>();
			ui.SetSide(playerIndex);
			ui.BindOnClickBattleCardUI(onClickBattleCard);
			ui.AddData(data, true);

			obj.transform.SetParent(Content.transform);
			ui.transform.localScale = Vector3.one;
			NGUITools.MarkParentAsChanged(obj.gameObject);

			GetSideList(playerIndex).Add(ui);

			Reposition();

			BattleUIManager.RequestBringForward(ui.gameObject, false);
		}

		private bool PopBattleCardUI(int battleCardID, out BattleCardUI battleCardUI)
		{
			// Find in _enemyCardObjList
			for (int index = 0; index < _enemyCardObjList.Count; ++index)
			{
				if (_enemyCardObjList[index].IsContainID(battleCardID))
				{
					BattleCardUI ui = _enemyCardObjList[index];
					if(ui.CardCount > 1)
					{
						ui.RemoveData(battleCardID);

						battleCardUI = null;
						return true; // remove only data.
					}
					else if(ui.CardCount == 1)
					{
						_enemyCardObjList.RemoveAt(index);
						Reposition();

						battleCardUI = ui;
						return true; // remove UI.
					}
				}
			}

			// Find in _playerCardObjList
			for (int index = 0; index < _playerCardObjList.Count; ++index)
			{
				if (_playerCardObjList[index].IsContainID(battleCardID))
				{
					BattleCardUI ui = _playerCardObjList[index];
					if(ui.CardCount > 1)
					{
						ui.RemoveData(battleCardID);

						battleCardUI = null;
						return true; // remove only data.
					}
					else if(ui.CardCount == 1)
					{
						_playerCardObjList.RemoveAt(index);
						Reposition();

						battleCardUI = ui;
						return true; // remove UI.
					}
				}
			}

			battleCardUI = null;
			return false; // return false is not successful remove.
		}
		 
		private void PutBattleCardUI(BattleCardUI battleCardUI, bool isEnemy)
		{
			List<BattleCardUI> targetSide = GetSideList(battleCardUI.Side);

			bool isCanAdd = true;

			if(targetSide != null && targetSide.Count > 0)
			{
				for(int index = 0; index < targetSide.Count; ++index)
				{
					BattleCardData showBattleCardData = targetSide[index].ShowingData;

					if(showBattleCardData.TypeData == CardTypeData.CardType.Structure)
					{
						if(showBattleCardData.PlayerCardData.PlayerCardID == battleCardUI.ShowingData.PlayerCardData.PlayerCardID)
						{
							// Same
							targetSide[index].AddData(battleCardUI.ShowingData);
							Destroy(battleCardUI.gameObject);
							isCanAdd = false;
							break;
						}
					}
				}
			}

			if(isCanAdd)
			{
				if (isEnemy)
				{
					// Enemy
					battleCardUI.gameObject.transform.parent = Content.transform;
					_enemyCardObjList.Add(battleCardUI);
				}
				else
				{
					// Player
					battleCardUI.gameObject.transform.parent = Content.transform;
					_playerCardObjList.Add(battleCardUI);
				}

				GameUtility.RequestBringForward(battleCardUI.gameObject, false);
				NGUITools.MarkParentAsChanged(battleCardUI.gameObject);
				Reposition();
			}

			//NGUITools.ImmediatelyCreateDrawCalls(battleCardUI.gameObject);        
		}
			
		public bool GetBattleCardUI(int battleCardID, out BattleCardUI battleCard)
		{
			// Find in _enemyCardObjList
			for (int index = 0; index < _enemyCardObjList.Count; ++index)
			{
				if (_enemyCardObjList[index].IsContainID(battleCardID))
				{
					// Found in enemy side.
					battleCard = _enemyCardObjList[index];
					return true; 
				}
			}

			// Find in _playerCardObjList
			for (int index = 0; index < _playerCardObjList.Count; ++index)
			{
				if (_playerCardObjList[index].IsContainID(battleCardID))
				{
					// Found in player side.
					battleCard = _playerCardObjList[index];
					return true;
				}
			}

			battleCard = null;
			return false; // return false if not found.
		}

		public bool MoveBattleCardUITo(int battleCardID, FieldUI targetField)
		{
			BattleCardUI ui;
			bool isFound = GetBattleCardUI(battleCardID, out ui);
			if (isFound)
			{
				// GameUtility.RequestBringForward(ui.gameObject, false);
				//BattleUIManager.RequestBringForward(BattleController.Instance.BattleUIManager.PopupPanel.gameObject);
				//NGUITools.AdjustDepth(BattleController.Instance.BattleUIManager.PopupPanel.gameObject, 1001);

				// Remove card from local battle card list.
				bool isRemove = PopBattleCardUI(battleCardID, out ui);
				if(isRemove)
				{
					DisplayPlayerIndex side = ui.Side;

					if(ui != null)
					{
						// Give UI.
						targetField.PutBattleCardUI(ui, (side == DisplayPlayerIndex.Enemy));
						SoundManager.PlayEFXSound(BattleController.Instance.MoveSound);
					}
					else
					{
						// Give just data.
						BattleCardData battleCardData;
						bool isSuccess = BattleManager.Instance.FindBattleCard(battleCardID, out battleCardData);
						if(isSuccess)
						{
							targetField._CreateBattleCardUI(battleCardData, side, ui.OnClickBattleCardCallback);
						}
						SoundManager.PlayEFXSound(BattleController.Instance.MoveSound);
					}

					return true;
				}
			}
			return false;
		}

		public bool DestroyBattleCardUI(int battleCardID)
		{
			foreach (DisplayPlayerIndex sideIndex in System.Enum.GetValues(typeof(DisplayPlayerIndex)))
			{
				List<BattleCardUI> uiList = GetSideList(sideIndex);

				for (int index = 0; index < uiList.Count; ++index)
				{
					if (uiList[index].IsContainID(battleCardID))
					{
						BattleCardUI ui = uiList[index];
						bool isSuccess = ui.RemoveData(battleCardID);
						if(isSuccess)
						{
							if(ui.CardCount <= 0)
							{
								// Empty.
								uiList.RemoveAt(index);
								Destroy(ui.gameObject);
								Reposition();
							}

							// return true is successful destroy.
							return true; 
						}
					}
				}
			}

			// return false is not successful destroy.
			return false; 
		}

		private void UpdateContentPosition()
		{
			if(_isOverflow)
			{
				if(Content.transform.localPosition.x >= ContentMaxPosX)
				{
					// at left
					if(ShiftLeftButton.gameObject.activeSelf) 
					{
						ShiftLeftButton.gameObject.SetActive(false);
					}
				}
				else 
				{
					if(!ShiftLeftButton.gameObject.activeSelf) 
					{
						ShiftLeftButton.gameObject.SetActive(true);
					}
				}

				if(Content.transform.localPosition.x <= ContentMinPosX)
				{
					// at right
					if(ShiftRightButton.gameObject.activeSelf) 
					{
						ShiftRightButton.gameObject.SetActive(false);
					}
				}
				else
				{
					if(!ShiftRightButton.gameObject.activeSelf) 
					{
						ShiftRightButton.gameObject.SetActive(true);
					}
				}
			}
			else
			{
				if(ShiftLeftButton.gameObject.activeSelf) 
					ShiftLeftButton.gameObject.SetActive(false);

				if(ShiftRightButton.gameObject.activeSelf) 
					ShiftRightButton.gameObject.SetActive(false);
			}
		}

		public void OnGoLeft()
		{
			if((Content.transform.localPosition.x + MinSize.x) < ContentMaxPosX)
			{
				// Can shift
				this.ContentLocalPos = new Vector3(Content.transform.localPosition.x + MinSize.x, 0.0f, 0.0f);
			}
			else
			{
				// End 
				//Debug.Log(ContentMaxPosX);

				this.ContentLocalPos = new Vector3(ContentMaxPosX, 0.0f, 0.0f);
				ShiftLeftButton.gameObject.SetActive(false);
			}
		}

		public void OnGoRight()
		{
			if((Content.transform.localPosition.x - MinSize.x) > ContentMinPosX)
			{
				//Debug.Log((Content.transform.localPosition.x - MinSize.x) + " " + ContentMinPosX);

				// Can shift
				this.ContentLocalPos = new Vector3(Content.transform.localPosition.x - MinSize.x, 0.0f, 0.0f);
			}
			else
			{
				// End 
				//Debug.Log(ContentMinPosX);

				this.ContentLocalPos = new Vector3(ContentMinPosX, 0.0f, 0.0f);
				ShiftRightButton.gameObject.SetActive(false);
			}
		}

		public void Reposition()
		{
			float num = _playerCardObjList.Count + _enemyCardObjList.Count;

			if (num > 0)
			{
				_currentWidth = (CardWidth * num) + (CardOffset * (num - 1.0f)) + (BorderWidth * 2.0f);
			} 
			else
			{
				_currentWidth = 0;
			}

			/*
			Content.width = Mathf.CeilToInt(_currentWidth);
			Content.height = Mathf.CeilToInt(MinSize.y);
			*/

			int cardIndex = 0;

			if(_currentWidth <= MinSize.x)
			{
				// Not overflow

				// Player
				for (int index = 0; index < _playerCardObjList.Count; ++index) 
				{
					// Left Side
					float x_offset = (CardWidth * 0.5f) + (CardWidth * cardIndex) + (CardOffset * cardIndex) + BorderWidth;
					_playerCardObjList[index].gameObject.transform.localPosition = new Vector3(x_offset, 0, 0);

					++cardIndex;
				}

				// Enemy
				for (int index = 0; index < _enemyCardObjList.Count; ++index) 
				{
					// Right Side
					float x_offset = (CardWidth * 0.5f) + (CardWidth * index) + (CardOffset * index) + BorderWidth;
					_enemyCardObjList[index].gameObject.transform.localPosition = new Vector3(MinSize.x - x_offset, 0, 0);
				}

				this.ContentLocalPos = new Vector3(ContentMaxPosX, 0.0f, 0.0f);


				ShiftLeftButton.gameObject.SetActive(false);
				ShiftRightButton.gameObject.SetActive(false);

				_isOverflow = false;

			}
			else
			{
				// Overflow
				bool isLeft = false;

				// Player
				for (int index = 0; index < _playerCardObjList.Count; ++index) 
				{
					// Left Side
					float x_offset = (CardWidth * 0.5f) + (CardWidth * cardIndex) + (CardOffset * cardIndex) + BorderWidth;
					_playerCardObjList[index].gameObject.transform.localPosition = new Vector3(x_offset, 0, 0);

					++cardIndex;
					isLeft = true;
				}

				// Enemy
				for (int index = _enemyCardObjList.Count - 1; index >= 0; --index) 
				{
					// Left Side
					float x_offset = (CardWidth * 0.5f) + (CardWidth * cardIndex) + (CardOffset * cardIndex) + BorderWidth;
					_enemyCardObjList[index].gameObject.transform.localPosition = new Vector3(x_offset, 0, 0);

					++cardIndex;
				}

				if(isLeft)
				{
					this.ContentLocalPos = new Vector3(ContentMaxPosX, 0.0f, 0.0f);
				}
				else
				{
					this.ContentLocalPos = new Vector3(ContentMinPosX, 0.0f, 0.0f);
				}

				_isOverflow = true;
			}
		}

		private List<BattleCardUI> GetSideList(DisplayPlayerIndex sideIndex)
		{
			switch(sideIndex)
			{
				case DisplayPlayerIndex.Player:
				{
					return _playerCardObjList;
				}

				case DisplayPlayerIndex.Enemy:
				{
					return _enemyCardObjList;
				}

				default: 
				{
					Debug.LogError ("FieldUI/GetSideList: sideList is null.");
					return null;
				}
			}
		}
		#endregion
	}
}
