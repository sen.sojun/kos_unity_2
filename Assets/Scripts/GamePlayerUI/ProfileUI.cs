﻿namespace GamePlayerUI
{
	using UnityEngine;
	using System.Collections;
	using System.Collections.Generic;

	#region DisplayPlayerIndex Enum
	public enum DisplayPlayerIndex
	{
		  Player
		, Enemy
	}
	#endregion

	#region ProfileUI Class
	public class ProfileUI : MonoBehaviour 
	{
		#region Delegates
		public delegate void OnClickGraveyard(DisplayPlayerIndex displayPlayerIndex);
		public delegate void OnClickRemoveZone(DisplayPlayerIndex displayPlayerIndex);
		#endregion

		#region Private Properties
		private OnClickGraveyard _onClickGraveyard = null;
		private OnClickRemoveZone _onClickRemoveZone = null;

		private static ProfileUI _instance = null;
		#endregion

		#region Public Properties
		public PlayerInfoUI PlayerInfoUI;
		public PlayerInfoUI EnemyInfoUI;

		//public static ProfileUI Instance { get { return _instance; } }
		#endregion

		#region Awake
		void Awake()
		{
			_instance = this;

			this.PlayerInfoUI.BindOnClickGraveyard (ProfileUI.OnClickGraveyardButton);
			this.PlayerInfoUI.BindOnClickRemoveZone (ProfileUI.OnClickRemoveButton);

			this.EnemyInfoUI.BindOnClickGraveyard (ProfileUI.OnClickGraveyardButton);
			this.EnemyInfoUI.BindOnClickRemoveZone (ProfileUI.OnClickRemoveButton);
		}
		#endregion

		#region Start
		// Use this for initialization
		/*
		void Start () 
		{
		}
		*/
		#endregion

		#region Update
		// Update is called once per frame
		/*
		void Update () 
		{
		}
		*/
		#endregion

		#region OnDestroy
		void OnDestroy() 
		{
			_instance = null;
		}
		#endregion

		#region Methods
		public static void SetProfile(DisplayPlayerIndex displayPlayerIndex, PlayerBattleData playerBattleData)
		{
			if(_instance == null) return; 

			switch (displayPlayerIndex)
			{
				case DisplayPlayerIndex.Player:
				{
					_instance.PlayerInfoUI.SetProfile(playerBattleData);
				}
				break;

				case DisplayPlayerIndex.Enemy:
				{
					_instance.EnemyInfoUI.SetProfile(playerBattleData);
				}
				break;
			}
		}

		public static void SetPlayerImage(DisplayPlayerIndex displayPlayerIndex, string imageName)
		{
			if(_instance == null) return; 

			switch (displayPlayerIndex)
			{
				case DisplayPlayerIndex.Player:
				{
					_instance.PlayerInfoUI.SetPlayerImage(imageName);
				}
				break;

				case DisplayPlayerIndex.Enemy:
				{
					_instance.EnemyInfoUI.SetPlayerImage(imageName);
				}
				break;
			}
		}

		public static void SetPlayerName(DisplayPlayerIndex displayPlayerIndex, string playerName)
		{
			if(_instance == null) return; 

			switch (displayPlayerIndex)
			{
				case DisplayPlayerIndex.Player:
				{
					_instance.PlayerInfoUI.SetPlayerName(playerName);
				}
				break;

				case DisplayPlayerIndex.Enemy:
				{
					_instance.EnemyInfoUI.SetPlayerName(playerName);
				}
				break;
			}
		}

		public static void SetHitPoint(DisplayPlayerIndex displayPlayerIndex, int hitPoint)
		{
			if(_instance == null) return; 

			switch (displayPlayerIndex)
			{
				case DisplayPlayerIndex.Player:
				{
					_instance.PlayerInfoUI.SetHitPoint(hitPoint);
				}
				break;

				case DisplayPlayerIndex.Enemy:
				{
					_instance.EnemyInfoUI.SetHitPoint(hitPoint);
				}
				break;
			}
		}

		public static void SetHandNum(DisplayPlayerIndex displayPlayerIndex, int handCardAmount)
		{
			if(_instance == null) return; 

			switch (displayPlayerIndex)
			{
				case DisplayPlayerIndex.Player:
				{
					_instance.PlayerInfoUI.SetHandNum(handCardAmount);
				}
				break;

				case DisplayPlayerIndex.Enemy:
				{
					_instance.EnemyInfoUI.SetHandNum(handCardAmount);
				}
				break;
			}
		}

		public static void SetGoldNum(DisplayPlayerIndex displayPlayerIndex, int goldCoin)
		{
			if(_instance == null) return; 

			switch (displayPlayerIndex)
			{
				case DisplayPlayerIndex.Player:
				{
					_instance.PlayerInfoUI.SetGoldNum(goldCoin);
				}
				break;

				case DisplayPlayerIndex.Enemy:
				{
					_instance.EnemyInfoUI.SetGoldNum(goldCoin);
				}
				break;
			}
		}

		public static void SetDeckCardNum(DisplayPlayerIndex displayPlayerIndex, int deckCardAmount)
		{
			if(_instance == null) return; 

			switch (displayPlayerIndex)
			{
				case DisplayPlayerIndex.Player:
				{
					_instance.PlayerInfoUI.SetDeckCardNum(deckCardAmount);
				}
				break;

				case DisplayPlayerIndex.Enemy:
				{
					_instance.EnemyInfoUI.SetDeckCardNum(deckCardAmount);
				}
				break;
			}
		}
		#endregion

		#region Event Methods
		public static void BindOnClickGraveyard(OnClickGraveyard onClickGraveyard)
		{
			if(_instance == null) return; 

			_instance._onClickGraveyard = onClickGraveyard;
		}

		public static void BindOnClickRemoveZone(OnClickRemoveZone onClickRemoveZone)
		{
			if(_instance == null) return; 

			_instance._onClickRemoveZone = onClickRemoveZone;
		}

		public static void OnClickGraveyardButton(DisplayPlayerIndex displayPlayerIndex)
		{
			if(_instance == null) return; 

			if(_instance._onClickGraveyard != null)
			{
				_instance._onClickGraveyard.Invoke(displayPlayerIndex);
			}
		}

		public static void OnClickRemoveButton(DisplayPlayerIndex displayPlayerIndex)
		{
			if(_instance == null) return; 

			if(_instance._onClickRemoveZone != null)
			{
				_instance._onClickRemoveZone.Invoke(displayPlayerIndex);
			}
		}
		#endregion
	}
	#endregion
}
