﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeUserNameUI : MonoBehaviour {
    
    public delegate void OnChangeName(string newName);
    public delegate void OnCancel();

    private OnChangeName _onChangeName = null;
    private OnCancel _onCancel = null;

    public Button changeNameButton;
    public Button cancelButton;
    public InputField nameInputField;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ShowUI(OnChangeName onChangerName,OnCancel onCancel)
    {
        _onChangeName = onChangerName;
        _onCancel = onCancel;
        changeNameButton.onClick.RemoveAllListeners();
        changeNameButton.onClick.AddListener(this.onChangeNameEvent);

        cancelButton.onClick.RemoveAllListeners();
        cancelButton.onClick.AddListener(this.OnCancelEvent);

        gameObject.SetActive(true);
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }

    private void onChangeNameEvent()
    {
        if (_onChangeName != null)
        {
            _onChangeName.Invoke(nameInputField.text);
        }
    }

    private void OnCancelEvent()
    {
        if (_onCancel != null)
        {
            _onCancel.Invoke();
        }
    }

}
