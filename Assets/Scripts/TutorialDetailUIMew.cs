﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialDetailUIMew : MonoBehaviour {

    #region Delegates 
    public delegate void OnCloseDetail();
    #endregion

    public Image TutorialTexture;
    public Button NextPageButton;
    public Button PrevPageButton;
    public Text PageLabel;
    public Text TutorialDetail;
    public Text TutorialTitle;
    public ScrollRect ScrollView;

    private List<Texture> _tutorialTextureList;
    private List<string> _tutorialText;
    private string _tutorialTitle;
    private OnCloseDetail _onCloseDetail = null;
    private static string _resourcePath = "Images/FirstFlowImage/";
                                               

    private int __pageIndex;
    private int _pageIndex
    {
        get { return __pageIndex; }
        set
        {
            __pageIndex = value;
            if (PrevPageButton != null)
            {
                PrevPageButton.gameObject.SetActive((__pageIndex != 0));
            }
            if (NextPageButton != null)
            {
                if (_tutorialTextureList != null)
                {
                    NextPageButton.gameObject.SetActive((__pageIndex + 1 < _tutorialTextureList.Count));
                }
                else
                {
                    NextPageButton.gameObject.SetActive(false);
                }
            }

            ShowCurrentPage();
        }
    }

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void ShowUI(string tutorialTitle, List<Texture> tutorialImageList, List<string> tutorialText, OnCloseDetail onCloseDetail)
    {
        _tutorialTitle = tutorialTitle;
        _tutorialTextureList = new List<Texture>(tutorialImageList);
        _tutorialText = new List<string>(tutorialText);

        _onCloseDetail = onCloseDetail;
        _pageIndex = 0;

        gameObject.SetActive(true);
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }

    public void NextPage()
    {
        if (_tutorialTextureList != null)
        {
            if (_pageIndex < _tutorialTextureList.Count)
            {
                ++_pageIndex;
            }
        }
        else
        {
            _pageIndex = 0;
        }

    }

    public void PreviousPage()
    {
        if (_pageIndex > 0)
        {
            --_pageIndex;
        }
    }

    private void ShowCurrentPage()
    {
        
        if (_tutorialTextureList != null)
        {
            Texture2D texture2D = _tutorialTextureList[_pageIndex] as Texture2D;
            Sprite mySprite = Sprite.Create(texture2D, new Rect(0.0f, 0.0f, texture2D.width, texture2D.height), new Vector2(0.5f, 0.5f), 100.0f);
            TutorialTexture.sprite = mySprite;

            //string imageFilePath = _resourcePath + _tutorialTextureList[_pageIndex] + ".PNG";
            //Debug.Log("image file path = " + imageFilePath);
            //Texture imageTexture = Resources.Load(imageFilePath, typeof(Texture)) as Texture;

            //if (imageTexture != null)
            //{
                
            //    Texture2D texture2D = imageTexture as Texture2D;
            //    Sprite mySprite = Sprite.Create(texture2D, new Rect(0.0f, 0.0f, texture2D.width, texture2D.height), new Vector2(0.5f, 0.5f), 100.0f);

            //    TutorialTexture.sprite = mySprite;
            //    Debug.Log(mySprite.name);
            //    Resources.UnloadUnusedAssets();
            //    //Resources.UnloadAsset(imageTexture);
            //}
            //else
            //{
            //    Debug.Log("cant load image");
            //}


           // TutorialTexture = _tutorialTextureList[_pageIndex];

            string text = _tutorialText[_pageIndex].Replace("\\n", "\n"); // New line
            TutorialDetail.text = text;
            TutorialTitle.text = _tutorialTitle;
            PageLabel.text = (_pageIndex + 1).ToString() + " / " + (_tutorialTextureList.Count.ToString());
            
            ScrollView.verticalNormalizedPosition = 1.0f;
        }
    }

    public void HideTutorial()
    {
        HideUI();

        if (_onCloseDetail != null)
        {
            _onCloseDetail.Invoke();
        }
    }
}
