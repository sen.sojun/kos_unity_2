﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenGoldUI : MonoBehaviour {

	public GameObject BuyConfirmUI;
	public GoldCoinUI GoldCoinUI;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ShowUI()
	{
		gameObject.SetActive (true);
		BattleUIManager.RequestBringForward (gameObject);
	}

	public void HideUI()
	{
		gameObject.SetActive (false);
	}

	public void OnShowBuyConfirm()
	{
		BuyConfirmUI.SetActive (true);
		BattleUIManager.RequestBringForward (BuyConfirmUI);
	}

	private void AddGoldCoin(int goldCoin)
	{
		int iGoldCoin = PlayerSave.GetPlayerGoldCoin ();

		iGoldCoin += goldCoin;

		// save add coin.
		PlayerSave.SavePlayerSilverCoin(iGoldCoin);

		GoldCoinUI.UpdateGoldCoin ();

		string logText = string.Format(
			Localization.Get("SHOP_GOLD_BUY_CONGRATULATION")
			, goldCoin
		);
		ShowAdsMsg (logText);
	}
	public void ShowAdsMsg(string text)
	{
//		AdsMessageUI.ShowUI (text);
	}
//
//	public void HideAdsMsg()
//	{
//		AdsMessageUI.HideUI ();
//	}
//

}
