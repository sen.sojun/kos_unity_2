﻿using UnityEngine;
using System.Collections;

public class BuyGoldButton : MonoBehaviour {

	public ShopUI ShopUI;
	public int GoldButtonIndex;

	public delegate void onBuyGoldButton(int index);
	private onBuyGoldButton _onBuyGoldButton = null;

	public void BindOnBuyGoldButton(onBuyGoldButton callback)
	{
		_onBuyGoldButton = callback;
	}


	void OnClick () {
		if (_onBuyGoldButton != null)
		{
			_onBuyGoldButton.Invoke (GoldButtonIndex);
		}
	}

	//void Awake()
	//{
	//	BindOnBuyGoldButton (ShopUI.onClickBuyGold);
	//}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
