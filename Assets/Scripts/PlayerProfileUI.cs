﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Firebase.Database;
using Firebase.Unity.Editor;
using Firebase.Auth;
using Firebase;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using System;

public class PlayerProfileUI : MonoBehaviour
{
    void HandleOnChangePasswordFailed(string errorMessage)
    {
    }

    private string _newName;
    private string _currPwd;
    private string _newPwd;

    private string _email;
    private string _password;

    private string currAvatar = "01";
    private static string _rootImagePath = "Images/Avatars/Full/";
    private string _titleScene = "TitleScene2";

    public ChangeUserNameUI changeNameUI;
    public ChangePasswordUI changePasswordUI;
    public ProfileBewareUI bewareUI;
    public ProfileResultUI resultUI;
    public RegisterUI RegisterUI;
    public MessageBoxUI MessageBoxUI;
    public LoginUI LoginUI;
    public GameObject LoadingUI;

    public Text NameText;
    public InputField InputName;
    public Text InputNameShow;
    public Text PasswordText;
    public Text EmailText;

    public Button EditNameButton;
    public Button EditPasswordButton;

    public RectTransform panelPlayer;
    public RectTransform panelGuest;
    public Button ChangeAvatarButton;
    public Button PlayerAvatarButton;
    public Image GuessImage;




    public ConfirmBox ConfirmBoxUI;

    AccountType accountType;
    UserDBData data;

    // Use this for initialization
    void GetProfileCache()
    {
        #if !UNITY_EDITOR        
        data = new UserDBData();

        //RequestCacheData();
        try
        {
         if (accountType == AccountType.Guest)
            {
                    data.uid = PlayerSave.GetPlayerUid();
        ScreenDubugLog.Instance.Print("profile Guest :get data from player save " + data.uid);
                    Debug.Log("get data from player save");
            }
            else
            {
                data = KOSServer.Instance.PlayerData;
                ScreenDubugLog.Instance.Print("get data " + data.uid);
                if (data.uid != "" ) 
                {
                    
                    Debug.Log("get data from kosserver");
                }
                else
                {
                    data.uid = PlayerSave.GetPlayerUid();
        ScreenDubugLog.Instance.Print("profile KOS else:get data from player save " + data.uid);
                    Debug.Log("get data from player save");
                }

               
            }
               
        }
        catch (System.Exception e)
        {
            ScreenDubugLog.Instance.Print("error cached player " + e.Message);
            data.uid = PlayerSave.GetPlayerUid();
            Debug.Log("get data from player save");
        }
        #endif
        
        InputName.text = PlayerSave.GetPlayerName();
        NameText.text = PlayerSave.GetPlayerName();
        InputNameShow.text = PlayerSave.GetPlayerName();
        EmailText.text = PlayerSave.GetPlayerEmail();

        //end comment 09042018
    }

    void Start()
    {
	}
	
	// Update is called once per frame
	void Update () 
    {	
	}

    public void ShowUI()
    {
        EditNameButton.onClick.RemoveAllListeners();
        EditNameButton.onClick.AddListener(this.ShowChangeNameUI);

        EditPasswordButton.onClick.RemoveAllListeners();
        EditPasswordButton.onClick.AddListener(this.ShowPasswordChange);

        gameObject.SetActive(true);

        currAvatar = PlayerSave.GetPlayerAvatar();
        Debug.Log("curravatar = " + currAvatar);
        SetAvatar(currAvatar);

        //Load player Avartar from PlayerSave
        accountType = PlayerSave.GetPlayerAccountType();
        if (accountType == AccountType.Guest)
        {
            panelGuest.gameObject.SetActive(true);
            GuessImage.gameObject.SetActive(true);
            panelPlayer.gameObject.SetActive(false);
            ChangeAvatarButton.gameObject.SetActive(false);
            PlayerAvatarButton.gameObject.SetActive(false);
        }
        else
        {
            panelGuest.gameObject.SetActive(false);
            GuessImage.gameObject.SetActive(false);
            panelPlayer.gameObject.SetActive(true);
            ChangeAvatarButton.gameObject.SetActive(true);
            PlayerAvatarButton.gameObject.SetActive(true);

        }

        GetProfileCache();
    }
    
    #region Change Avatar
    public void SetAvatar(string imagePath) //, string playerName = "")
    {
        StartCoroutine(LoadImage(_rootImagePath + imagePath, imagePath));
        Debug.Log("imag index x= " + imagePath);


        //PlayerName.text = playerName;
    }

    private IEnumerator LoadImage(string imagePath, string imgIndex)
    {

        ResourceRequest request = Resources.LoadAsync<Sprite>(imagePath);
        yield return request;

        Sprite sprite = request.asset as Sprite;
        if (sprite != null)
        {
            PlayerAvatarButton.image.sprite = sprite;
            //update to firebase and player save
            //PlayerSave.SavePlayerAvatar(imgIndex);
           
        }
        else
        {
            Debug.LogError("PlayerAvatarUI/LoadImage: Failed to load image. " + imagePath);
        }
    }
    #endregion

    public void GuestNeedRegister()
    {
        StartCoroutine(_StartCacheData(0.1f));
    }

    private IEnumerator _StartCacheData(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        StartCoroutine(_LoadTitle());
    }

    private IEnumerator _LoadTitle()
    {
        yield return new WaitForSeconds(0.1f);
         
        yield return new WaitForSeconds(1.0f);

        PlayerSave.SaveGuestNeedLogin(true);
        SceneManager.LoadSceneAsync("TitleScene2");
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }

    #region Change Name
    public void ShowChangeNameUI()
    {
        changeNameUI.ShowUI(this.OnClickChangeName,this.OnClickCancelChangeName);
       // HideUI();
    }

    public void HideChangeNameUI()
    {
        changeNameUI.HideUI();
        ShowUI();
    }

    private void OnClickChangeName(string newName)
    {
        if (newName != "")
        {
            _newName = newName;

            HideChangeNameUI();
            ShowBeware(
                Localization.Get("PLAYER_PROFILE_NAME_CHANGE_MSG")
                , this.OnConfirmChangeName
                , this.OnCancelConfirmChangeName
            );
        }
        else
        {
            ShowMessageBox(
                Localization.Get("PLAYER_PROFILE_CHANGE_NAME")
                , Localization.Get("PLAYER_PROFILE_NAME_CHANGE_ERROR")
                , Localization.Get("TITLE_MESSAGE_FAILED_BUTTON")
                , null
            );
        }
    }



    private void OnClickCancelChangeName()
    {
        HideChangeNameUI();
    }

    private void OnConfirmChangeName()
    {
        HideBeware();

        RequestServerChangeName(_newName);
    }

    private void OnCancelConfirmChangeName()
    {
        HideBeware();
    }

    private void RequestServerChangeName(string newName)
    {
        ScreenDubugLog.Instance.Print("uid = " + data.uid + data.UserName);
        ScreenDubugLog.Instance.Print(" new name " + newName);
        try
        {
            // To do request to server to change name.
         
            if (data.uid != "")
            {
                KOSServer.UpdateUserData(data, "UserName", newName);
                OnServerChangeNameResponse(true, "success");
                PlayerSave.SavePlayerName(newName);
                TopBarManager.Instance.ShowPlayerNameLabel(newName);
                NameText.text = newName;
                InputName.text = newName;
                InputNameShow.text = newName;
            }
            else
            {
                ShowResult("no uid found");
            }
            Debug.Log("uid  = " + data.uid);
           
        } 

        catch (System.Exception e)
        {
            ScreenDubugLog.Instance.Print("Error change name " + e.Message);
                          
            OnServerChangeNameResponse(false,e.Message);
        }
      
       
    }

    private void OnServerChangeNameResponse(bool isSuccess,string sResult)
    {
        string resultMsg = "";
        if (isSuccess)
        {
            resultMsg = Localization.Get("PLAYER_PROFILE_NAME_CHANGE_SUCCESS");
        }
        else
        {
            resultMsg = Localization.Get("PLAYER_PROFILE_NAME_CHANGE_ERROR");
            //"Sorry. Failed" + sResult; 
        }

        ShowResult(resultMsg);
    }
    #endregion

    #region Change Password 
    public void ShowPasswordChange()
    {
        changePasswordUI.ShowUI(this.OnClickChangePassword, this.OnCancelChangePassword);
    }

    public void HidePasswordUI()
    {
        changePasswordUI.HideUI();
    }

    private void OnClickChangePassword(string currPwd,string newPwd)
    {
        //--should get password from firebase instead of playersave 
        //in case player play from many devices
        //string currentPwd = PlayerSave.GetPlayerPassword();

        //_currPwd = currPwd;
        _newPwd = newPwd;

        //if (_currPwd == "" || _currPwd != currentPwd)
        //{
        //    // wrong current password
        //    ShowMessageBox(
        //    Localization.Get("PLAYER_PROFILE_PASSWORD_BUTTON")
        //   , Localization.Get("PLAYER_PROFILE_PASSWORD_CHANGE_ERR_DIFF")
        //   , Localization.Get("TITLE_MESSAGE_FAILED_BUTTON")
        //        , null
        //    );

        //}
        //else 
        if (_newPwd == null || _newPwd == "" ) 
        {
            // new password is blank

            ShowMessageBox(
                  Localization.Get("PLAYER_PROFILE_PASSWORD_BUTTON")
                 , Localization.Get("PLAYER_PROFILE_PASSWORD_CHANGE_ERR_INV")
                 , Localization.Get("TITLE_MESSAGE_FAILED_BUTTON")
                      , null
          );
        }
        else
        {
            ScreenDubugLog.Instance.Print(_newPwd);
            HidePasswordUI();
            ShowBeware(
                Localization.Get("PLAYER_PROFILE_PASSWORD_CHANGE_MSG")
                , this.OnConfirmChangePassword
                , this.OnCancelChangePassword
           );
        }
      
    }

    private void OnConfirmChangePassword()
    {
        //HideBeware(); //--move to change password complete
        ScreenDubugLog.Instance.Print("new = " + _newPwd + " old = " + _currPwd);
        //KOSServer.RequestServerChangePassword(
        //      _currPwd
        //    , _newPwd
        //    , this.OnChangePasswordCompleted
        //    , this.OnChangePasswordFailed
        //);

        KOSServer.Instance.CacheChangePassword(_newPwd
                                               , this.OnChangePasswordCompleted
                                               , this.OnChangePasswordFailed);
    }

    private void OnCancelChangePassword()
    {
        //HidePasswordUI(); //-vit19092019
        HideBeware();      //+vit09022019
    }

    private void OnCancelConfirmChangePassword()
    {
        HideBeware();
    }

    //private void OnVerifyPassword()
    //{
        
    //        ShowMessageBox(
    //        Localization.Get("PLAYER_PROFILE_PASSWORD_BUTTON")
    //        , Localization.Get("PLAYER_PROFILE_PASSWORD_CHANGE_SUCCESS")
    //        , Localization.Get("TITLE_MESSAGE_FAILED_BUTTON")
    //            , HidePasswordUI
    //        );

    //}

    private void OnChangePasswordCompleted()
    {
        HideBeware();
        PlayerSave.SavePlayerPassword(_newPwd);
        ShowMessageBox(
             Localization.Get("PLAYER_PROFILE_PASSWORD_BUTTON")
           , Localization.Get("PLAYER_PROFILE_PASSWORD_CHANGE_SUCCESS")
           , Localization.Get("TITLE_MESSAGE_FAILED_BUTTON")
               , HidePasswordUI
           );

    }
    
    private void OnChangePasswordFailed(string errMsg)
    {
        ShowMessageBox(
            Localization.Get("PLAYER_PROFILE_PASSWORD_BUTTON")
           , errMsg
            , Localization.Get("TITLE_MESSAGE_FAILED_BUTTON")
           , null
        );
    }
    #endregion

    #region Beware
    public void ShowBeware(string msg,ProfileBewareUI.OnYes onYes, ProfileBewareUI.OnNo onNo)
    {
        bewareUI.ShowUI(msg, onYes, onNo);
    }

    public void HideBeware()
    {
        bewareUI.HideUI();
    }
    #endregion

    #region Result
    public void ShowResult(string msg)
    {
        resultUI.ShowUI(msg);
    }
    
    public void HideResult()
    {
        resultUI.HideUI();
    }
    #endregion

    #region SignUp Methods
    public void SignUp(string email, string password)
    {
        // ResultText.text = PasswordIn.value + " " + EmailInput.text.ToString() + " " + UserNameInput.text.ToString();
        try
        {
            if (!KOSServer.Instance.IsProcessSignUp)
            {
                _email = email;
                _password = password;
                KOSServer.Instance.SignUp(email, password, email, this.OnSignUpCompleted, this.OnSignUpFailed);
            }
        }
        catch (FirebaseException e)
        {
            ShowMessageBox(
                  "Error Signup"
                , e.Message
                , "Close"
                , ShowRegisterUI
            );
        }
    }

    private void OnSignUpCompleted(string email)
    {
        accountType = AccountType.KOS;
        PlayerSave.SavePlayerAccountType(AccountType.KOS);
        ShowMessageBox(
              "Success"
            , "Your KOS account has been created"
            , "START GAME"
            , () => {
                Login(email, _password);
            }
        );
    }

    private void OnSignUpFailed(string errorMessage)
    {
        ShowMessageBox(
             "Signup Failed"
           , errorMessage
           , "Close"
           , ShowRegisterUI
       );
    }

    private void CompleteSign()
    {
        PlayerSave.SaveGuestNeedLogin(false);
        PlayerSave.SavePlayerEmail(_email);
        PlayerSave.SavePlayerPassword(_password);
        PlayerSave.SavePlayerAccountType(AccountType.KOS);
        ShowLoading();
        SceneManager.LoadSceneAsync("Title2");

                  
    }
    #endregion

    #region Login Methods
    private void Login(string email, string password)
    {
        try
        {
            if (!KOSServer.Instance.IsProcessLogin)
            {
                KOSServer.Instance.Login(email, password, this.OnLoginCompleted, this.OnLoginFailed);
            }
        }
        catch (FirebaseException e)
        {
            ShowMessageBox(
                  "Error Login"
                , "Please correct your email or password"
                , "Close"
                , ShowLoginUI
            );
        }

    }

    private void HideLogin()
    {
        
        LoginUI.HideUI();
        RegisterUI.HideUI();
        HideUI();
    }

    private void OnLoginCompleted()
    {
        accountType = AccountType.KOS;
        InputName.text = PlayerSave.GetPlayerName();
        NameText.text = PlayerSave.GetPlayerName();
        InputNameShow.text = PlayerSave.GetPlayerName();
        EmailText.text = PlayerSave.GetPlayerEmail();
        ShowMessageBox(
              "Success"
            , "You successfully logged in to the game"
            , "START GAME"
            , HideLogin
        );
    }

    private void OnLoginFailed(string errorMessage)
    {
        ShowMessageBox( 
              "Login Failed"
            , errorMessage
            , "Close"
            , ShowLoginUI
        );
    }
    #endregion

    #region RegisterUI Methods
    public void OnClickRegister()
    {
        //ShowRegisterUI();
        PlayerSave.SetAutoLogin(false);
        this.LoadSceneTitle();
    }
    
    private void ShowRegisterUI()
    {
        RegisterUI.ShowUI(OnRegisterUIClickCreate, OnRegisterUIClickCancel);
    }

    private void OnRegisterUIClickCreate(string email, string password)
    {
        if (string.IsNullOrEmpty(email) && string.IsNullOrEmpty(password))
        {
            ShowMessageBox(
                  "Error"
                , "Please Enter Email and Password"
                , "Close"
                , ShowRegisterUI
            );
        }
        else if (string.IsNullOrEmpty(email))
        {
            ShowMessageBox(
                  "Error"
                , "Please Enter Email"
                , "Close"
                , ShowRegisterUI
            );
        }
        else if (string.IsNullOrEmpty(password))
        {
            ShowMessageBox(
                  "Error"
                , "Please Enter Password"
                , "Close"
                , ShowRegisterUI
            );
        }
        else
        {
            Debug.Log(string.Format("Start signup Email : {0}", email));
            SignUp(email, password);
        }
    }

    private void OnRegisterUIClickCancel()
    {
        //ShowLoginUI();
        RegisterUI.HideUI();
    }

    private void OnRegisterUIClickCancelFromGuest()
    {

        RegisterUI.HideUI();
    }
    #endregion

    #region MessageBoxUIMethods
    private void ShowMessageBox(string header, string message, string buttonText, UnityAction onClickButton)
    {
        MessageBoxUI.ShowUI(header, message, buttonText, onClickButton);
    }
    #endregion

    #region LoginUI Methods
    private void ShowLoginUI()
    {
        LoginUI.ShowUI(OnLoginUIClickLogin, OnLoginUIClickCancel, OnLoginUIClickCreate, OnLoginUIClickForgot);
    }
    
    private void OnLoginUIClickLogin(string email, string password)
    {
        if (string.IsNullOrEmpty(email) && string.IsNullOrEmpty(password))
        {
            ShowMessageBox(
                  "Error"
                , "Please Enter Email and Password"
                , "Close"
                , ShowLoginUI
            );
        }
        else if (string.IsNullOrEmpty(email))
        {            
            ShowMessageBox(
                  "Error"
                , "Please Enter Email"
                , "Close"
                , ShowLoginUI
            );
        }
        else if (string.IsNullOrEmpty(password))
        {
            ShowMessageBox(
                  "Error"
                , "Please Enter Password"
                , "Close"
                , ShowLoginUI
            );
        }
        else
        {
            Debug.Log(string.Format("Start login Email : {0}", email));
            //_isAutoLogin = false;
            Login(email, password);
        }
    }
    
    private void OnLoginUIClickCancel()
    {
       
    }
    
    private void OnLoginUIClickCreate()
    {
        ShowRegisterUI();
    }
    
    private void OnLoginUIClickForgot()
    {
       // ShowForgotPasswordUI();
    }
    #endregion
      
    #region LoadingUI Methods
    public void ShowLoading()
    {
        MenuManagerV2.Instance.ShowLoading();
    }
    
    public void HideLoading()
    {
        MenuManagerV2.Instance.HideLoading();
    }
    #endregion

    #region Logout
    public void Logout()
    {
        ConfirmBoxUI.ShowUI(
              Localization.Get("PLAYER_PROFILE_LOGOUT_TITLE")
            , Localization.Get("PLAYER_PROFILE_LOGOUT_MSG")
            , () => ConfirmLogout()
            , () => ConfirmBoxUI.HideUI()
        );
    }

    public void ConfirmLogout()
    {
        if (KOSServer.Instance.IsLogin)
        {
            KOSServer.Instance.Logout();
            PlayerSave.ClearAllSave();
        }
        
        LoadSceneTitle();
    }

    private void LoadSceneTitle()
    {
        ShowLoading();
        SceneManager.LoadSceneAsync(_titleScene);
    }
    #endregion
}
