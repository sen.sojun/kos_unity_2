﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class PlayerGoldUI : MonoBehaviour 
{
    public Button LoginPopupButton;
    public ConfirmBox ConfirmBoxUI;

    public List<Image> imgSelected;
    public List<Image> imgButtonSelected;
    public Text textSelectGold;

	// Use this for initialization
	void Start () 
    {	
	}
	
	// Update is called once per frame
	void Update () 
    {	
	}

    public void ShowUI()
    {
        //disableAll();
        
        #if !UNITY_EDITOR
        if(PlayerSave.GetPlayerAccountType() != AccountType.KOS)        
        {
            LoginPopupButton.interactable = true;
            LoginPopupButton.gameObject.SetActive(true);
        }
        else
        #endif
        {
            LoginPopupButton.interactable = false;
            LoginPopupButton.gameObject.SetActive(false);
        }
    
        gameObject.SetActive(true);
    }

    public void HideUI()
    {
        //disableAll();
        gameObject.SetActive(false);
    }
    
    public void ShowLoginPopup()
    {
        ConfirmBoxUI.ShowUI("ERROR", Localization.Get("SHOP_PLEASE_LOGIN"), null);
    }

    public void SelectedGold(int iSelected)
    {
        imgSelected[iSelected].gameObject.SetActive(true);
        imgButtonSelected[iSelected].gameObject.SetActive(true);
        textSelectGold.text = iSelected.ToString();

        for (int i = 0; i < imgSelected.Count; ++i)
        {
            if (i != iSelected)
            {
                imgSelected[i].gameObject.SetActive(false);
                imgButtonSelected[i].gameObject.SetActive(false);
            }
        }
    }

    public void disableAll()
    {
        for (int i = 0; i < imgSelected.Count; ++i)
        {
            imgSelected[i].gameObject.SetActive(false);
            imgButtonSelected[i].gameObject.SetActive(false);
        }
    }

    void OnApplicationPause(bool pauseStatus)
    {
        try
        {
            Debug.Log("pause stat " + pauseStatus);
            if (pauseStatus)
            {
                disableAll();
            }
        }
        catch (Exception e)
        {
            Debug.Log("Error local 3 " + e.Message);
        }
    }

}
