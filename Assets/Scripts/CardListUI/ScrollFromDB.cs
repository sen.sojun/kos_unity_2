﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScrollFromDB : MonoBehaviour 
{
	public GameObject myGrid;
	public GameObject goPrefab;
	private GameObject item;

	private int i;
	private float xPos;

	private List<GameObject> _myCardDisplayList;

	// Use this for initialization
	void Start () 
	{
		_myCardDisplayList = new List<GameObject> ();

		/*
		List<CardData> cardList = new List<CardData> ();

		cardList.Add (new CardData("C0001"));
		cardList.Add (new CardData("C0002"));
		cardList.Add (new CardData("C0003"));
		cardList.Add (new CardData("C0004"));
		cardList.Add (new CardData("C0005"));

		SetDataList (cardList);
        */
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetDataList(List<CardData> cardDataList)
	{
		if (_myCardDisplayList.Count > cardDataList.Count) 
		{
			int delta = _myCardDisplayList.Count - cardDataList.Count;
			for (int i = 0; i < delta; ++i) 
			{
				GameObject item = _myCardDisplayList [0];
				_myCardDisplayList.RemoveAt (0);
				Destroy(item);
			}
		} 
		else 
		{
			int delta = cardDataList.Count - _myCardDisplayList.Count;

			for (int i = 0; i < delta; ++i) 
			{
				Vector3 newPosition = new Vector3 (0, 0, 0);
				GameObject newItem = Instantiate (goPrefab, newPosition, transform.rotation) as GameObject;
				newItem.transform.parent = myGrid.transform;
				newItem.transform.localScale = new Vector3 (1, 1, 1);

				_myCardDisplayList.Add (newItem);
			}
		}

		for (int i = 0; i < cardDataList.Count; ++i) 
		{
			//_myCardDisplayList [i].GetComponent<HandCardDisplay> ().SetCardData (cardDataList[i]);
		}

		myGrid.GetComponent<UIGrid> ().Reposition ();
	}
}
