﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ActionManager 
{
	private static ActionManager _instance = null;
	public static ActionManager Instance 
	{ 
		get 
		{ 
			if(_instance == null)
			{
                ActionManager.Init();
			}

			return _instance; 
		} 
	}

	private List<CardAction> _actionList = null;
	private bool _isReadyToAction = true;
    private bool _isQueueActive = true;
	private bool _isPause = false;
    private bool _isContinueAfterFinish = true;
	private bool IsPause { get { return _isPause; } }

	public bool IsReadyToAction { get { return (_isReadyToAction && _isQueueActive); } }
    public bool IsQueueActive { get { return _isQueueActive; } }
    public bool IsContinueAfterFinish { get { return _isContinueAfterFinish; } }
    public bool IsEmpty { get { if(_actionList != null) return (_actionList.Count <= 0); else return true; } }

	/*
	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
	}
	*/

	private ActionManager()
	{
		_init();
	}

	public static void Init()
	{
        ActionManager._instance = new ActionManager();
	}
    
    private void _init()
    {
        _actionList = new List<CardAction>();
        _isReadyToAction = true;
        _isQueueActive = true;
        _isPause = false;
        _isContinueAfterFinish = true;
    }
    
    public void OnUpdate()
    {
        if(_actionList.Count > 0)
        {
            if(IsReadyToAction && !IsPause)
            {
                PlayAction();
            }
        }
    }

	public void AddFirstAction(CardAction cardAction, bool isPlayAfterAdd = true)
	{
        /*
        if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
        {
            if(!KOSNetwork.GamePlayer.localPlayer.isServer)
            {
                return;
            }
        }
        */
        
		if(_actionList == null) Init();
		#if UNITY_EDITOR
		//Debug.Log("Action Stack: [Add] " + cardAction.GetDebugText());
		#endif
		_actionList.Insert(0, cardAction);

		if(isPlayAfterAdd && IsReadyToAction && !IsPause)
		{
			PlayAction();
		}
	}
    
    public void SetQueueActive(bool isActive)
    {
        _isQueueActive = isActive;
        
        if(IsReadyToAction && !IsPause)
        {
            PlayAction();
        }
    }
    
     public void SetContinueAfterFinish(bool isContinueAfterFinish)
    {
        _isContinueAfterFinish = isContinueAfterFinish;
    }

	public void AddLastAction(CardAction cardAction, bool isPlayAfterAdd = true)
	{
		if(_actionList == null) Init();

		//Debug.Log("Action Stack: [Add Last] " + + cardAction.GetDebugText());
		_actionList.Add(cardAction);

		if(isPlayAfterAdd && IsReadyToAction && !IsPause)
		{
			PlayAction();
		}
	}

	public bool PlayAction()
	{
		if(_actionList != null && _actionList.Count > 0)
		{
			if(BattleManager.Instance != null)
			{
				if(BattleManager.Instance.IsEndGame)
				{
					_actionList.Clear();
					_isReadyToAction = true;
					return false;
				}
			}

			CardAction cardAction = _actionList[0];
			_actionList.RemoveAt(0);

			_isReadyToAction = false;
			#if UNITY_EDITOR
			//Debug.Log("Action Stack: [Play] " + cardAction.GetDebugText());
			#endif
			cardAction.Action();
            return true;
		}
		else
		{
			#if UNITY_EDITOR
			//Debug.Log("Action Stack: [Ready]");
			#endif
			_isReadyToAction = true;
            return false;
		}
	}
    
	public void OnActionFinish()
	{
        _isReadyToAction = true;
        
        if(!IsContinueAfterFinish)
        {
            _isContinueAfterFinish = true;
            return;
        }
        
		if(IsReadyToAction)
        {
            PlayAction();
        }
	}

	public void SetPauseAction(bool isPause)
	{
		_isPause = isPause;

		if(IsReadyToAction && !IsPause)
        {
            PlayAction();
        }
	}
    
    #region WaitPlayers
    IEnumerator WaitAction()
    {
        while(true)
        {           
            if(IsReadyToAction && !IsPause)
            {
                PlayAction();
            }

            yield return new WaitForSeconds(0.5f);
        }
    }
    #endregion
}
