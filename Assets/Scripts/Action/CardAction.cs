﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class CardAction  
{
	public virtual void Action()
	{
		// do something.
	}

	protected virtual void OnActionFinish()
	{
		ActionManager.Instance.OnActionFinish();
	}

	public virtual string GetDebugText()
	{
		return "";
	}
}

public class RenewHandAction : CardAction
{
	private PlayerIndex _playerIndex;
	private int _handSize;

	public RenewHandAction(PlayerIndex playerIndex, int handSize = 5)
	{
		_playerIndex = playerIndex;
		_handSize = handSize;
	}

	public override void Action()
	{
		BattleManager.Instance.ActionReNewHandCard(_playerIndex, _handSize);
		OnActionFinish();
	}

	public override string GetDebugText()
	{
		string text = "";
		text = string.Format("RenewHandAction: Player {0} {1} card{2}."
			, _playerIndex.ToString()
			, _handSize.ToString()
			, ((_handSize > 1) ? "s" : "")
		);

		return text;
	}
}

public class NextPhaseAction : CardAction
{
	private PlayerIndex _playerIndex;
    private int _turnIndex;
    private BattlePhase _phase;

	public NextPhaseAction(PlayerIndex playerIndex, int turnIndex, BattlePhase phase)
	{
		_playerIndex = playerIndex;
        _turnIndex = turnIndex;
        _phase = phase;
	}

	public override void Action()
	{
		BattleManager.Instance.ActionNextPhase(_playerIndex, _turnIndex, _phase);
		OnActionFinish();
	}

	public override string GetDebugText()
	{
		string text = "";
		text = string.Format("NextPhaseAction: Player {0} turn:{1} phase:{2}."
			, _playerIndex.ToString()
            , _turnIndex.ToString()
            , _phase.ToString()
		);

		return text;
	}
}

public class FaceUpBattleCard : CardAction
{
	private int _faceupID;

	public FaceUpBattleCard(int faceupID)
	{
		_faceupID = faceupID;
	}

	public override void Action()
	{
		BattleManager.Instance.ActionFaceupBattleCard(_faceupID);
		OnActionFinish();
	}

	public override string GetDebugText()
	{
		string text = "";
		text = string.Format("FaceUpBattleCard: BattleID {0}."
			, _faceupID.ToString()
		);

		return text;
	}
}

public class ActBattleCardAction : CardAction
{
	private int _actID;

	public ActBattleCardAction(int actID)
	{
		_actID = actID;
	}

	public override void Action()
	{
		BattleManager.Instance.ActionActBattleCard(_actID);
		OnActionFinish();
	}

	public override string GetDebugText()
	{
		string text = "";
		text = string.Format("ActBattleCardAction: BattleID {0}."
			, _actID.ToString()
		);

		return text;
	}
}

public class GetGoldAction : CardAction
{
	private PlayerIndex _playerIndex;
	private int _gold;

	public GetGoldAction(PlayerIndex playerIndex, int gold)
	{
		_playerIndex = playerIndex;
		_gold = gold;
	}

	public override void Action()
	{
		BattleManager.Instance.ActionGetGold(_playerIndex, _gold);
		OnActionFinish();
	}

	public override string GetDebugText()
	{
		string text = "";
		text = string.Format("GetGoldAction: Player {0} get gold {1}"
			, _playerIndex.ToString()
			, _gold.ToString()
		);

		return text;
	}
}

public class DrawCardAction : CardAction
{
	private PlayerIndex _playerIndex;
	private int _amount;

	public DrawCardAction(PlayerIndex playerIndex, int amount = 1)
	{
		_playerIndex = playerIndex;
		_amount = amount;
	}

	public override void Action()
	{
		BattleManager.Instance.ActionDrawCard(_playerIndex, _amount);
		OnActionFinish();
	}

	public override string GetDebugText()
	{
		string text = "";
		text = string.Format("DrawCardAction: Player {0} draw {1} card{2}."
			, _playerIndex.ToString()
			, _amount.ToString()
			, ((_amount > 1) ? "s" : "")
		);

		return text;
	}
}
	
public class AttackBattleCardAction : CardAction
{
	private int _attackerID;
	private int _targetID;

	public AttackBattleCardAction(int attackerID, int targetID)
	{
		_attackerID = attackerID;
		_targetID = targetID;
	}
		
	public override void Action()
	{
		BattleManager.Instance.ActionAttackBattleCard(_attackerID, _targetID, this.OnActionFinish);
	}

	public override string GetDebugText()
	{
		string text = "";
		text = string.Format("AttackBattleCardAction: Attacker BattleID({0}) -> Target BattleID({1})."
			, _attackerID.ToString()
			, _targetID.ToString()
		);

		return text;
	}
}

public class MoveBattleCardAction : CardAction
{
	private int _battleCardID;
	private BattleZoneIndex _targetZone;
	private bool _isForceMove;

	public MoveBattleCardAction(int battleCardID, BattleZoneIndex targetZone, bool isForceMove = false)
	{
		_battleCardID = battleCardID;
		_targetZone = targetZone;
		_isForceMove = isForceMove;
	}

	public override void Action()
	{
		BattleManager.Instance.ActionMoveBattleCard(_battleCardID, _targetZone, _isForceMove);
		OnActionFinish();
	}

	public override string GetDebugText()
	{
		string text = "";
		text = string.Format("MoveBattleCardToBattleZoneAction: {0} BattleID({1}) to {2} Zone."
			, ((_isForceMove) ? "Force Move" : "Move")
			, _battleCardID.ToString()
			, _targetZone.ToString()
		);

		return text;
	}
}
	
public class MoveUniqueCardAction : CardAction
{
	private int _uniqueCardID;
	private CardZoneIndex _targetZone;

	public MoveUniqueCardAction(int uniqueCardID, CardZoneIndex targetZone)
	{
		_uniqueCardID = uniqueCardID;
		_targetZone = targetZone;
	}

	public override void Action()
	{
		BattleManager.Instance.ActionMoveUniqueCard(_uniqueCardID, _targetZone);
		OnActionFinish();
	}

	public override string GetDebugText()
	{
		string text = "";
		text = string.Format("MoveUniqueCardAction: Move UniqueID({0}) to {1}."
			, _uniqueCardID.ToString()
			, _targetZone.ToString()
		);

		return text;
	}
}

public class ShuffleDeckAction : CardAction
{
	private PlayerIndex _playerIndex;

	public ShuffleDeckAction(PlayerIndex playerIndex)
	{
		_playerIndex = playerIndex;
	}

	public override void Action()
	{
		BattleManager.Instance.ActionShuffleDeck(_playerIndex);
		OnActionFinish();
	}

	public override string GetDebugText()
	{
		string text = "";
		text = string.Format("ShuffleDeckAction: Shuffle deck player {0}." 
			, _playerIndex.ToString()
		);

		return text;
	}
}

public class ShuffleDeckBySeedAction : CardAction
{
    private PlayerIndex _playerIndex;
    private int _seed;

    public ShuffleDeckBySeedAction(PlayerIndex playerIndex, int seed)
    {
        _playerIndex = playerIndex;
        _seed = seed;
    }

    public override void Action()
    {
        BattleManager.Instance.ActionShuffleDeckBySeed(_playerIndex, _seed);
        OnActionFinish();
    }

    public override string GetDebugText()
    {
        string text = "";
        text = string.Format("ShuffleDeckBySeedAction: Shuffle deck player {0} seed({1})." 
            , _playerIndex.ToString()
            , _seed.ToString()
        );

        return text;
    }
}


public class DestroyBattleCardAction : CardAction
{
	private int _destroyID;
	private CardZoneIndex _targetZone;

	public DestroyBattleCardAction(int destroyID, CardZoneIndex targetZone)
	{
		_destroyID = destroyID;
		_targetZone = targetZone;
	}
		
	public override void Action()
	{
		BattleManager.Instance.ActionDestroyBattleCard(_destroyID, _targetZone);
		OnActionFinish();
	}

	public override string GetDebugText()
	{
		string text = "";
		text = string.Format("DestroyBattleCardAction: BattleID({0}) move to {1}." 
			, _destroyID.ToString()
			, _targetZone.ToString()
		);

		return text;
	}
}

public class DealDamageBattleCardAction : CardAction
{
	private int _targetID;
	private int _damage;
	private bool _isPiercing = false;

	public DealDamageBattleCardAction(int targetID, int damage, bool isPiercing)
	{
		_targetID = targetID;
		_damage = damage;
		_isPiercing = isPiercing;
	}

	public override void Action()
	{
		BattleManager.Instance.ActionDealDamage(_targetID, _damage, _isPiercing);
		OnActionFinish();
	}

	public override string GetDebugText()
	{
		string text = "";
		text = string.Format("DealDamageBattleCardAction: TargetID({0}) damage {1} (isPiercing: {2})."
			, _targetID.ToString()
			, _damage.ToString()
			, _isPiercing.ToString()
		);

		return text;
	}
}
	
public class UseAbilityBattleCardAction : CardAction
{
	private int _battleCardID;
	private int _abilityIndex;
    private bool _isUseByPlayer;

	public UseAbilityBattleCardAction(int battleCardID, int abilityIndex, bool isUseByPlayer)
	{
		_battleCardID = battleCardID;
		_abilityIndex = abilityIndex;
        _isUseByPlayer = isUseByPlayer;
	}

	public override void Action()
	{
		BattleManager.Instance.ActionUseAbilityBattleCard(_battleCardID, _abilityIndex, _isUseByPlayer, this.OnActionFinish);
	}

	public override string GetDebugText()
	{
		string text = "";
		text = string.Format("UseAbilityBattleCardAction: BattleID({0}) AbilityIndex({1}) isUseByPlayer({2})"
			, _battleCardID.ToString()
			, _abilityIndex.ToString()
            , _isUseByPlayer.ToString()
		);

		return text;
	}
}

/*
public class PreformAbilityBattleCardAction : CardAction
{
    private int _battleCardID;

    public PreformAbilityBattleCardAction(int battleCardID)
    {
        _battleCardID = battleCardID;
    }

    public override void Action()
    {
        BattleManager.Instance.ActionPreformAbilityBattleCard(_battleCardID);
    }

    public override string GetDebugText()
    {
        string text = "";
        text = string.Format("PreformAbilityBattleCardAction: BattleID({0})"
            , _battleCardID.ToString()
        );

        return text;
    }
}
*/

public class AddBuffBattleCardAction : CardAction
{
	private int _targetID;
	private CardBuff _buff;

	public AddBuffBattleCardAction(int targetID, CardBuff buff)
	{
		_targetID = targetID;
		_buff = buff;
	}

	public override void Action()
	{
		BattleManager.Instance.ActionAddBuffBattleCard(_targetID, _buff);
		OnActionFinish();
	}

	public override string GetDebugText()
	{
		string text = "";
		text = string.Format("AddBuffBattleCardAction: TargetID({0})."
			, _targetID.ToString()
		);

		return text;
	}
}

public class UseHandCardAction : CardAction
{
	private int _uniqueCardID;
    private bool _isUseByPlayer;

	public UseHandCardAction(int uniqueCardID, bool isUseByPlayer)
	{
		_uniqueCardID = uniqueCardID;
        _isUseByPlayer = isUseByPlayer;
	}

	public override void Action()
	{
		BattleManager.Instance.ActionUseHandCard(_uniqueCardID, _isUseByPlayer, this.OnActionFinish);
	}

	public override string GetDebugText()
	{
		string text = "";
		text = string.Format("UseHandCardAction: HandID({0}) isUseByPlayer({1})"
			, _uniqueCardID.ToString()
            , _isUseByPlayer.ToString()
		);

		return text;
	}
}

/*
public class PreformAbilityHandCardAction : CardAction
{
    private int _uniqueCardID;

    public PreformAbilityHandCardAction(int uniqueCardID)
    {
        _uniqueCardID = uniqueCardID;
    }

    public override void Action()
    {
        BattleManager.Instance.ActionPreformUseHandCard(_uniqueCardID);
    }

    public override string GetDebugText()
    {
        string text = "";
        text = string.Format("PreformAbilityHandCardAction: HandID({0})"
            , _uniqueCardID.ToString()
        );

        return text;
    }
}
*/

public class SummonUniqueCardAction : CardAction
{
	private int _uniqueCardID;
	private LocalBattleZoneIndex _localZone;
	private bool _isFree;
    private bool _isLeader;
    private bool _isFacedown;

    public SummonUniqueCardAction(int uniqueCardID, LocalBattleZoneIndex localZone, bool isFree, bool isLeader, bool isFacedown)
	{
		_uniqueCardID = uniqueCardID;
		_localZone = localZone;
		_isFree = isFree;
        _isLeader = isLeader;
        _isFacedown = isFacedown;
	}

	public override void Action()
	{
        BattleManager.Instance.ActionSummonUniqueCard(_uniqueCardID, _localZone, _isFree, _isLeader, _isFacedown);
		OnActionFinish();
	}

	public override string GetDebugText()
	{
		string text = "";
        text = string.Format("SummonUniqueCardAction: {0} HandID({1})"
            , ((_isFacedown) ? "Face down" : "")
			, _uniqueCardID.ToString()
		);

		return text;
	}
}

public class ActionAbilityUniqueCardAction : CardAction
{
    private int _uniqueCardID;
    private int _abilityIndex;
    private List<string> _paramList;

    public ActionAbilityUniqueCardAction(int uniqueCardID, int abilityIndex, List<string> paramList)
    {
        _uniqueCardID = uniqueCardID;
        _abilityIndex = abilityIndex;
        _paramList = paramList;
    }

    public override void Action()
    {
        BattleManager.Instance.ActionActionAbilityUniqueCard(_uniqueCardID, _abilityIndex, _paramList, this.OnActionFinish);
    }

    public override string GetDebugText()
    {
        string text = "";
        text = string.Format("ActionAbilityUniqueCardAction: UniqueID({0}) AbilityIndex({1})"
            , _uniqueCardID.ToString()
            , _abilityIndex.ToString()
        );

        return text;
    }
}

public class ActionAbilityBattleCardAction : CardAction
{
    private int _battleCardID;
    private int _abilityIndex;
    private List<string> _paramList;

    public ActionAbilityBattleCardAction(int battleCardID, int abilityIndex, List<string> paramList)
    {
        _battleCardID = battleCardID;
        _abilityIndex = abilityIndex;
        _paramList = paramList;
    }

    public override void Action()
    {
        BattleManager.Instance.ActionActionAbilityBattleCard(_battleCardID, _abilityIndex, _paramList, this.OnActionFinish);
    }

    public override string GetDebugText()
    {
        string text = "";
        text = string.Format("ActionAbilityBattleCardAction: BattleID({0}) AbilityIndex({1})"
            , _battleCardID.ToString()
            , _abilityIndex.ToString()
        );

        return text;
    }
}

public class WaitActiveQueueAction : CardAction
{
    public WaitActiveQueueAction()
    {
    }

    public override void Action()
    {
        BattleManager.Instance.ActionWaitActiveQueue();
    }

    public override string GetDebugText()
    {
        string text = "";
        text = string.Format("WaitActiveQueueAction: action");

        return text;
    }
}

public class ResumeActiveQueueAction : CardAction
{
    public ResumeActiveQueueAction()
    {
    }

    public override void Action()
    {
        BattleManager.Instance.ActionResumeActiveQueue();
    }

    public override string GetDebugText()
    {
        string text = "";
        text = string.Format("ResumeActiveQueueAction: action");

        return text;
    }
}
