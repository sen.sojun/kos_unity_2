﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoloLeagueLoadingUI : MonoBehaviour 
{
	public UISprite PlayerSprite;
	public UISprite EnemySprite;

	public UILabel PlayerName;
	public UILabel EnemyName;

	public UILabel PlayerLevel;
	public UILabel EnemyLevel;

	// Use this for initialization
	void Start () 
	{
		
	}

	// Update is called once per frame
	void Update () {
		
	}

	public void ShowUI(MatchingData matchingData)
	{
		{
			PlayerData profileData = matchingData.PlayerData [0];
			PlayerSprite.spriteName = profileData.SoloImageName;
			PlayerName.text = profileData.Name;
			PlayerLevel.text = "Level " + profileData.Lv.ToString ();
		}

		{
			PlayerData profileData = matchingData.PlayerData [1];
			EnemySprite.spriteName = profileData.SoloImageName;
			EnemyName.text = profileData.Name;
			EnemyLevel.text = "Level " + profileData.Lv.ToString ();
		}

		gameObject.SetActive(true);
		BattleUIManager.RequestBringForward(gameObject);
	}

	public void HideUI()
	{
		gameObject.SetActive(false);
	}

}
