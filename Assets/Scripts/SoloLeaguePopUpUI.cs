﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoloLeaguePopUpUI : MonoBehaviour {

	public delegate void OnClickPopup();

	public UILabel txtTitle;
	public UILabel txtDesc;
	public UILabel txtButtonText; 

	private SoloLeaguePopUpUI.OnClickPopup _onClickCallback = null;

	public enum PopUpTypeEnum
	{
		Finish
		, Sorry
	}

	public void ShowUI(string titleKey, string msgKey, string btnKey, OnClickPopup callback = null)
	{
		_onClickCallback = callback;

		txtTitle.GetComponent<UILocalize>().key = titleKey;
		txtDesc.GetComponent<UILocalize> ().key = msgKey;
		txtButtonText.GetComponent<UILocalize> ().key = btnKey;
		gameObject.SetActive(true);
	}

	public void HideUI()
	{
		gameObject.SetActive(false);
	}

	public void OnClickButton()
	{
		HideUI ();
		if (_onClickCallback != null)
		{
			_onClickCallback.Invoke();
		}
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
