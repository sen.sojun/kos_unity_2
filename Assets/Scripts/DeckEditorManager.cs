﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using GameAnalyticsSDK;

public class DeckEditorManager : MonoBehaviour 
{
	#region Delegates
	public delegate void OnClickUI(PlayerCardData clickData);
	#endregion

	#region Private Properties
	private List<PlayerCardData> _stashDataList = null;
	private List<PlayerCardData> _deckDataList = null;

	private bool _bIsChange = false;

    private string Tutorial1 = "T0039"; //Deck Editor Tutorial
    private List<TutorialData> _tutorialDataList = null;
	#endregion

	#region Public Properties
   
	#endregion

	#region Public Inspector Properties
	public AudioClip BGMClip;
    public AudioClip CardInsert_EFX;

	public CardEditorListUI StashUI;
	public CardEditorListUI DeckUI;
	public UIDragDropRoot UIDragDropRoot;

	public UIPanel PopupPanel;
	public PopupBoxUI PopupUI;

	public GameObject ShowInfoPopup;
	public GameObject ExitPopup;
	public GameObject LoadingPopup;
	public LoadingUI LoadingUI;

    public bool ForcePlayFirstTime = false;
	#endregion

	#region private interval time for gameAnalytic
	private float startTime;
	private float endTime;
	private float totalTime;
	#endregion

	#region Awake
	// Use this for initialization
	void Awake () 
	{
	}
	#endregion

	#region Start
	// Use this for initialization
	void Start () 
	{
		startTime = Time.time;

		if(!SoundManager.IsPlayBGM(BGMClip))
		{
			SoundManager.PlayBGM(BGMClip);
		}

		LoadSaveData();
	}
	#endregion

	#region showTutorial
	public void CheckDeckEditorFirstTime()
	{
		if (ForcePlayFirstTime || PlayerSave.IsDeckEditorFirstTime()) 
		{
			Debug.Log ("Deckeditor firstime");

			_tutorialDataList = new List<TutorialData>();

			List<TutorialDBData> dataList;
			bool _isSuccess = TutorialDB.GetDataByCode (Tutorial1, out dataList);
			if(_isSuccess)
			{
				List<TutorialDBData> showList = new List<TutorialDBData>();

				// Filter not show in first flow.
				foreach(TutorialDBData data in dataList)
				{
					if(data.ShowInFirstFlow > 0)
					{
						showList.Add(data);
					}
				}

				int page = 0;
				foreach(TutorialDBData data in showList)
				{
					TutorialData tutorialData = new TutorialData(data, (++page).ToString() + "/" + showList.Count.ToString());
					_tutorialDataList.Add(tutorialData);
				}
			}

			PlayerSave.SaveDeckEditorFirstTime(false); //player open deckeditor already then
			ShowFirstTimePopup(0);
		}
	}

	public void ShowFirstTimePopup(int index)
	{
		TutorialData data = _tutorialDataList[index++];

		string title = "";
		string message = "";
		string buttonText = "";
		if(LocalizationManager.IsCurrentLanguage(LocalizationManager.Language.Thai))
		{
			title = data.TitleTh;
			message = data.TextTh;
		}
		else
		{
			title = data.TitleEng;
			message = data.TextEng;
		}

		buttonText = Localization.Get(data.ButtonKey);

		PopupUI.ShowImageUI(
			  data.ImagePath
			, title
			, data.PageText
			, message
			, buttonText
			, () =>
			{
				if(index < _tutorialDataList.Count)
				{
					ShowFirstTimePopup(index);
				}
				else
				{
					this.closePopUpBox();
				}
			}
			, false
		);
		PopupPanel.gameObject.SetActive(true);
		BattleUIManager.RequestBringForward (PopupPanel.gameObject);
	}
	public void closePopUpBox()
	{
		PopupPanel.gameObject.SetActive(false);
	}
	#endregion

	#region Update
	// Update is called once per frame
	void Update () 
	{
		
	}
	#endregion

	#region Methods
	public void LoadSaveData()
	{
		StartCoroutine(_LoadSaveData());
	}

	private IEnumerator _LoadSaveData()
	{
		ShowLoadingPopup();
		StashUI.ClearUI();
		DeckUI.ClearUI();

		yield return new WaitForSeconds(0.1f);

		bool isSuccess = false;

		// Load PlayerSave CardStash 
		isSuccess = PlayerSave.GetPlayerCardStash(out _stashDataList);
        if (isSuccess)
        {
            #if UNITY_EDITOR
            {
                string debugText = "Stash: ";
                foreach (PlayerCardData card in _stashDataList)
                {
                    debugText += string.Format("[{0}]{1}", card.PlayerCardID, card.Name_EN) + ", ";
                }
                Debug.Log(debugText);
            }
            #endif
        }
        else
        {
            _stashDataList = new List<PlayerCardData>();
        }
        StashUI.SetInitDataList(0, _stashDataList, this.OnClickCard, this.OnDropCard);

		// Load PlayerSave Deck 
		DeckData deck;
		isSuccess = PlayerSave.GetPlayerDeck(out deck);
        if (isSuccess)
        {
            _deckDataList = new List<PlayerCardData>();
            for (int index = 0; index < deck.Count; ++index)
            {
                _deckDataList.Add(deck.GetCardAt(index).PlayerCardData);
            }

            #if UNITY_EDITOR
            {
                string debugText = "Deck: ";
                foreach (PlayerCardData card in _deckDataList)
                {
                    debugText += string.Format("[{0}]{1}", card.PlayerCardID, card.Name_EN) + ", ";
                }
                Debug.Log(debugText);
            }
            #endif
        }
        else
        {
            _deckDataList = new List<PlayerCardData>();    
        }
        DeckUI.SetInitDataList(1, _deckDataList, this.OnClickCard, this.OnDropCard);

		BattleUIManager.RequestBringForward(UIDragDropRoot.gameObject);
		_bIsChange = false;

		HideLoadingPopup();

		//put tutorial first time here
		CheckDeckEditorFirstTime();
		//end put tutorial first time

		yield return null;
	}

	public void SaveData()
	{
		StartCoroutine(_SaveData());
	}

	private IEnumerator _SaveData()
	{
		ShowLoadingPopup();
		yield return new WaitForSeconds(0.1f);

		string resultText = Localization.Get("DECK_EDITOR_SAVE_ERROR");

        List<PlayerCardData> stashDataList = _stashDataList;

		//for player's deck
		bool isHasBase = false;
		bool isHasLeader = false;
		bool isDuplicate = false;
        List<PlayerCardData> deckDataList = _deckDataList;

        foreach (PlayerCardData card in deckDataList)
		{	
			// Check Base
            if (card.IsCardType(CardTypeData.CardType.Base)) 
			{
				isHasBase = true;
			}
				
			// Check Leader
            if (card.IsCardType(CardTypeData.CardType.Unit))
			{
                if(card.IsCardSubType(CardSubTypeData.CardSubType.Unique)) 
				{
					isHasLeader = true;
				}
			}
		}       

		if(!isHasBase)
		{
			// No Base	
			resultText += Localization.Get("DECK_EDITOR_SAVE_NO_BASE");

			/*
			HideLoadingPopup();
			ShowMessagePopup(resultText);
			yield return null;
			*/
		}

		if(!isHasLeader)
		{
			// No Leader	
			resultText += Localization.Get("DECK_EDITOR_SAVE_NO_LEADER");

			/*
			HideLoadingPopup();
			ShowMessagePopup(resultText);
			yield return null;
			*/
		}

		if(deckDataList.Count != 40)
		{
			// Not 40	
			resultText += Localization.Get("DECK_EDITOR_SAVE_NOT_40");

			/*
			HideLoadingPopup();
			ShowMessagePopup(resultText);
			yield return null;
			*/
		}

		DeckData deckData = new DeckData (deckDataList);

		// Check duplicate cards in deck not more than 3 
		for (int index = 0; index < deckData.Count; ++index) 
		{
			if (deckData.GetCardAt(index).PlayerCardData.SymbolData != CardSymbolData.CardSymbol.Mass) 
			{
				// Not Mass

				// Check duplicate cards
				List<UniqueCardData> cardList;
				bool isFound = deckData.FindAll(deckData.GetCardAt (index).PlayerCardData.CardID, out cardList);
				if (isFound) 
				{
					if (cardList.Count > 3) 
					{
						isDuplicate = true;

						resultText += Localization.Get("DECK_EDITOR_SAVE_SAME_3");

						/*
						HideLoadingPopup();
						ShowMessagePopup(resultText);
						yield return null;
						*/
					}
				}
			}
		}
            
		if(isHasBase && isHasLeader && deckData.Count == 40 && !isDuplicate)
		{
			PlayerSave.SavePlayerCardStash (stashDataList);
			PlayerSave.SavePlayerDeck (deckData);
			_bIsChange = false;

			resultText = Localization.Get("DECK_EDITOR_SAVE_COMPLETED");

			//Debug.Log(resultText);
		}     

		HideLoadingPopup();
		ShowMessagePopup(resultText);
			
		yield return null;
	}

    public void UpdateUI()
    {
        //StashUI.ReIndexUI();
        StashUI.SetInitDataList(0, _stashDataList, this.OnClickCard, this.OnDropCard);

        //DeckUI.ReIndexUI();
        DeckUI.SetInitDataList(1, _deckDataList, this.OnClickCard, this.OnDropCard);

        BattleUIManager.RequestBringForward(UIDragDropRoot.gameObject);
    }
	#endregion

	#region OnClick Methods
	public void OnSaveClick()
	{
		SaveData();
	}

	public void OnResetClick()
	{
		LoadSaveData();
	}

	public void OnMainMenuClick()
	{
		if (_bIsChange) 
		{
			ShowExitPopup();
		} else 
		{
			ExitWithoutSave();
		}
	}

	public void ExitWithoutSave()
	{
		endTime = Time.time;
		totalTime = endTime - startTime;
		totalTime = totalTime / 60.0f;
		GameAnalytics.NewDesignEvent ("Achievement:EndDeckEditor(Min)", totalTime);

		LoadingUI.ShowUI();
		//SceneManager.LoadSceneAsync("MainMenuScene");
        SceneManager.LoadSceneAsync("MainMenuScene2");
	}

	private void OnClickCard(PlayerCardData data)
	{
		ShowInfoPopup.GetComponentInChildren<FullCardUI>().SetCardData(data);

		ShowCardInfo();
	}

    private void OnDropCard(int listIndex, int cardDataIndex)
    {
        //Debug.Log("OnDropCard " + listIndex.ToString());

        switch (listIndex)
        {
            case 0:
            {
                // Deck -> Stash
                int addIndex = (StashUI.PageIndex * StashUI.CardPerPage);

                _stashDataList.Insert(addIndex, _deckDataList[cardDataIndex]);
                _deckDataList.RemoveAt(cardDataIndex);
            }
            break;

            case 1:
            {
                // Stash -> Deck
                int addIndex = (DeckUI.PageIndex * DeckUI.CardPerPage);

                _deckDataList.Insert(addIndex, _stashDataList[cardDataIndex]);
                _stashDataList.RemoveAt(cardDataIndex);
            }
            break;
        }

        SoundManager.PlayEFXSound(CardInsert_EFX);

        _stashDataList = GameUtility.SortCard(_stashDataList);
        _deckDataList = GameUtility.SortCard(_deckDataList);

        _bIsChange = true;
        //Debug.Log(string.Format("{0} Stash:{1} Deck:{2}", listIndex, _stashDataList.Count, _deckDataList.Count));
        UpdateUI();
    }
	#endregion

	#region UI Methods
	public void ShowCardInfo()
	{
		ShowInfoPopup.SetActive(true);

		BattleUIManager.RequestBringForward (ShowInfoPopup.gameObject);
	}

	public void HideCardInfo()
	{
		ShowInfoPopup.SetActive(false);
	}

	private void ShowExitPopup()
	{
		ExitPopup.SetActive(true);
		BattleUIManager.RequestBringForward (ExitPopup.gameObject);
	}

	public void HideExitPopup()
	{
		ExitPopup.SetActive(false);
	}

	private void ShowLoadingPopup()
	{
		LoadingPopup.SetActive(true);
		BattleUIManager.RequestBringForward(LoadingPopup.gameObject);
	}

	private void HideLoadingPopup()
	{
		LoadingPopup.SetActive(false);
	}

	public void ShowMessagePopup(string text)
	{
		PopupPanel.gameObject.SetActive(true);
		BattleUIManager.RequestBringForward (PopupPanel.gameObject);

		PopupUI.ShowMessageUI(text, Localization.Get("POPUP_OK"), this.HideMessagePopup);
	}

	public void HideMessagePopup()
	{
		PopupUI.HideUI();
		PopupPanel.gameObject.SetActive(false);
	}
	#endregion
}
