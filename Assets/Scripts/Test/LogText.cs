﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(UILabel))]
public class LogText : MonoBehaviour 
{
	private List<string> _logList = null;

	public int MaxLogShow = 23;

	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
	}

	public void PrintLog(string text)
	{
		if (_logList == null)
		{
			_logList = new List<string>(); 
		}

		_logList.Add (text);

		while (_logList.Count > MaxLogShow)
		{
			_logList.RemoveAt(0);
		}

		string logText = "";
		foreach (string log in _logList)
		{
			logText += "\n" + log;
		}

		GetComponent<UILabel>().text = logText;
	}

	public void ClearLog()
	{
		_logList.Clear ();
		GetComponent<UILabel>().text = "";
	}
}
