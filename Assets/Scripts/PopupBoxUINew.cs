﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupBoxUINew : MonoBehaviour {

    public delegate void OnClickPopup();

    public PopupMessageBoxUINew MessageBox;
    public PopupImageBoxUINew ImageBox;

    private PopupBoxUINew.OnClickPopup _onClickCallback = null;

    public void ShowMessageUI(string message, string buttonText = "OK", OnClickPopup callback = null, bool isKey = true)
    {
        ImageBox.HideUI();

        _onClickCallback = callback;
        MessageBox.ShowUI(message, buttonText, isKey);

        gameObject.SetActive(true);
        //BattleUIManager.RequestBringForward(gameObject);
    }

    public void ShowImageUI(string imageFile, string title, string page, string message, string buttonText = "OK", OnClickPopup callback = null, bool isKey = true)
    {
        MessageBox.HideUI();

        _onClickCallback = callback;
        Debug.Log("show image " + imageFile + " " + title + " " + page +
                  " " + message + " " + buttonText + " " + isKey);
        ImageBox.ShowUI(imageFile, title, page, message, buttonText, isKey);

        gameObject.SetActive(true);
        //BattleUIManager.RequestBringForward(gameObject);
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }

    public void OnClickButton()
    {
        if (_onClickCallback != null)
        {
            _onClickCallback.Invoke();
        }
    }
}
