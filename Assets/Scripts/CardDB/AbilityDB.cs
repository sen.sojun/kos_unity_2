﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class AbilityDBData 
{
	#region Public Properties
	public string AbilityID;
	public int TriggerIndex;
	public List<string> AbilityConditionIDList;
	public List<string> AbilityEffectIDList;

	public string Description_EN;
	public string Description_TH;
	#endregion

	#region Constructors
	public AbilityDBData()
	{
		AbilityID = "";
		TriggerIndex = -1;
		AbilityConditionIDList = new List<string>();
		AbilityEffectIDList = new List<string>();

		Description_EN = "";
		Description_TH = "";
	}

	public AbilityDBData(string abilityID, int triggerIndex, List<string> abilityConditionIDList, List<string> abilityEffectIDList, string description_EN, string description_TH
	)
	{
		AbilityID = abilityID;
		TriggerIndex = triggerIndex;
		AbilityConditionIDList = abilityConditionIDList;
		AbilityEffectIDList = abilityEffectIDList;

		Description_EN = description_EN;
		Description_TH = description_TH;
	}

	public AbilityDBData(AbilityDBData abilityDBData)
	{
		AbilityID = abilityDBData.AbilityID;
		TriggerIndex = abilityDBData.TriggerIndex;
		AbilityConditionIDList = abilityDBData.AbilityConditionIDList;
		AbilityEffectIDList = abilityDBData.AbilityEffectIDList;

		Description_EN = abilityDBData.Description_EN;
		Description_TH = abilityDBData.Description_TH;
	}
	#endregion

	#region Methods
	public string GetDebugPrintText()
	{
		string conditionIDText = "";
		string effectIDText = "";

		foreach(string conditionID in AbilityConditionIDList)
		{
			if(conditionIDText.Length == 0)
			{
				conditionIDText += conditionID;
			}
			else
			{
				conditionIDText += ", " + conditionID;
			}
		}

		foreach(string effectID in AbilityEffectIDList)
		{
			if(effectIDText.Length == 0)
			{
				effectIDText += effectID;
			}
			else
			{
				effectIDText += ", " + effectID;
			}
		}

		string resultText;
		resultText = string.Format(
			  "AbilityID: {0}\nTriggerIndex: {1}\nConditionIDList: {2}\nEffectIDList: {3}" 
			+ "\nDescription English: {4}\nDescription Thai: {5}"
			, AbilityID
			, TriggerIndex
			, conditionIDText
			, effectIDText
			, Description_EN
			, Description_TH
		);

		return resultText;
	}

	public void DebugPrintData()
	{
		Debug.Log(GetDebugPrintText());
	}
	#endregion
}

public class AbilityDB
{
	#region Enums
	private enum AbilityDBIndex : int
	{
		  AbilityID = 0
		, TriggerIndex
		, ConditionIDList
		, EffectIDList
		, Description_EN
		, Description_TH
	}
	#endregion

	#region Private Properties
	private static Dictionary<string, AbilityDBData> _abilityDBList = null;
	private static bool _isLoadDB = false;
	#endregion
    
    #region Public Properties
    public static bool IsLoadDB { get {return _isLoadDB; } }
    #endregion

	#region Methods
	public static bool LoadDB()
	{
		TextAsset csvFile = Resources.Load<TextAsset>("Databases/KOS-CardDB - AbilityDB");

		if(csvFile != null)
		{
			List<List<string>> rawDBList;
			bool isSuccess = CSVReader.ReadCSV(csvFile, out rawDBList);
			if (isSuccess)
			{
				_abilityDBList = new Dictionary<string, AbilityDBData>();

				foreach(List<string> record in rawDBList)
				{
					string[] temp;

					List<string> conditionIDList = new List<string>();
					temp = record[(int)AbilityDBIndex.ConditionIDList].Split(',');
					foreach(string conditionID in temp)
					{
						conditionIDList.Add(conditionID.Trim());
					}

					List<string> effectIDList = new List<string>();
					temp = record[(int)AbilityDBIndex.EffectIDList].Split(',');
					foreach(string effectID in temp)
					{
						effectIDList.Add(effectID.Trim());
					}

					AbilityDBData data = new AbilityDBData(
						  record[(int)AbilityDBIndex.AbilityID]
						, int.Parse(record[(int)AbilityDBIndex.TriggerIndex])
						, conditionIDList
						, effectIDList
						, record[(int)AbilityDBIndex.Description_EN]
						, record[(int)AbilityDBIndex.Description_TH]
					);

					_abilityDBList.Add(data.AbilityID, data);
				}

				Resources.UnloadAsset(csvFile);
				_isLoadDB = true;
				return true;
			}
		}

		_isLoadDB = false;
		return false;
	}
		
	public static bool GetData(string abilityID, out AbilityDBData data)
	{
		if(!_isLoadDB)
		{
			LoadDB();
		}

		return (_abilityDBList.TryGetValue(abilityID, out data));
	}

	public static void DebugPrintData()
	{
        StringBuilder sb = new StringBuilder();

        foreach (KeyValuePair<string, AbilityDBData> data in _abilityDBList)
        {
            sb.Append(data.Value.GetDebugPrintText());
            sb.Append("\n\n");
        }
        Debug.Log(sb.ToString());
	}
	#endregion
}
