﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class CardImageDBData
{
    #region Public Properties
    public string CardImageID;
    public string ImageFile;
    public string ArtistName_EN;
    public string ArtistName_TH;
    #endregion

    #region Constructors
    public CardImageDBData()
    {
        CardImageID = "";
        ImageFile = "";
        ArtistName_EN = "";
        ArtistName_TH = "";
    }

    public CardImageDBData(string cardImageID, string imageFile, string artistName_EN, string artistName_TH)
    {
        CardImageID = cardImageID;
        ImageFile = imageFile;
        ArtistName_EN = artistName_EN;
        ArtistName_TH = artistName_TH;
    }
    #endregion

    #region Methods
    public string GetDebugPrintText()
    {
        string resultText;
        resultText = string.Format(
            "CardImageID: {0}\nImageFile: {1}\nArtistName English: {2}\nArtistName Thai: {3}"
            , CardImageID
            , ImageFile
            , ArtistName_EN
            , ArtistName_TH
        );
        return (resultText);
    }

    public void DebugPrintData()
    {
        Debug.Log(GetDebugPrintText());
    }
    #endregion
}

public class CardImageDB
{
    #region Enums
    private enum CardImageDBIndex : int
    {
          CardImageID = 0
        , ImageFile
        , ArtistName_EN
        , ArtistName_TH
    }
    #endregion

    #region Private Properties
    private static Dictionary<string, CardImageDBData> _cardImageDBList = null;
	private static bool _isLoadDB = false;
    #endregion
    
    #region Public Properties
    public static bool IsLoadDB { get {return _isLoadDB; } }
    #endregion

    #region Constructors
    static CardImageDB()
    {
    }
    #endregion

    #region Methods
    public static bool LoadDB()
    {
		TextAsset csvFile = Resources.Load<TextAsset>("Databases/KOS-CardDB - CardImageDB");

		if(csvFile != null)
		{
	        List<List<string>> rawDBList;
	        bool isSuccess = CSVReader.ReadCSV(csvFile, out rawDBList);
	        if (isSuccess)
	        {
	            _cardImageDBList = new Dictionary<string, CardImageDBData>();

	            foreach (List<string> record in rawDBList)
	            {
	                CardImageDBData data = new CardImageDBData(
	                      record[(int)CardImageDBIndex.CardImageID]
	                    , record[(int)CardImageDBIndex.ImageFile]
	                    , record[(int)CardImageDBIndex.ArtistName_EN]
                        , record[(int)CardImageDBIndex.ArtistName_TH]
                    );
	                _cardImageDBList.Add(data.CardImageID, data);
	            }

				Resources.UnloadAsset(csvFile);
				_isLoadDB = true;
	            return true;
	        }
		}

		_isLoadDB = false;
        return false;
    }

    public static bool GetData(string cardImageID, out CardImageDBData data)
    {
		if(!_isLoadDB)
		{
			LoadDB();
		}

        return (_cardImageDBList.TryGetValue(cardImageID, out data));
    }

    public static void DebugPrintData()
    {
        StringBuilder sb = new StringBuilder();

        foreach (KeyValuePair<string, CardImageDBData> data in _cardImageDBList)
        {
            sb.Append(data.Value.GetDebugPrintText());
            sb.Append("\n\n");
        }
        Debug.Log(sb.ToString());
    }
    #endregion
}
