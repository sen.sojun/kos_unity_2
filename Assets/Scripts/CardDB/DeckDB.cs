﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class DeckDBData
{
	#region Public Properties
	public string DeckID;
	public string PlayerCardID;
	public int Qty;
	#endregion

	#region Constructors
	public DeckDBData()
	{
		DeckID = "";
		PlayerCardID = "";
		Qty = 0;
	}

	public DeckDBData(string deckID, string playerCardID, int qty)
	{
		DeckID = deckID;
		PlayerCardID = playerCardID;
		Qty = qty;
	}

	public DeckDBData(DeckDBData deckDBData)
	{
		DeckID = deckDBData.DeckID;
		PlayerCardID = deckDBData.PlayerCardID;
		Qty = deckDBData.Qty;
	}
	#endregion

	#region Methods
	public string GetDebugPrintText()
	{
		string resultText;
		resultText = string.Format(
			"DeckID: {0}\nPlayerCardID: {1}\nQty: {2}"
			, DeckID
			, PlayerCardID
			, Qty
		);

		return resultText;
	}

	public void DebugPrintData()
	{
		Debug.Log(GetDebugPrintText());
	}
	#endregion
}

public class DeckDB 
{
	#region Enums
	private enum DeckDBIndex : int
	{
		  DeckID = 0
		, PlayerCardID
		, Qty
	}
	#endregion

	#region Private Properties
	private static List<DeckDBData> _deckDBList = null;
	private static bool _isLoadDB = false;
	#endregion
    
    #region Public Properties
    public static bool IsLoadDB { get {return _isLoadDB; } }
    #endregion

	#region Constructors
	static DeckDB()
	{
	}
	#endregion

	#region Methods
	public static bool LoadDB()
	{
		TextAsset csvFile = Resources.Load<TextAsset>("Databases/KOS-CardDB - DeckDB");
		if(csvFile != null)
		{
			List<List<string>> rawDBList;
			bool isSuccess = CSVReader.ReadCSV(csvFile, out rawDBList);
			if (isSuccess)
			{
				_deckDBList = new List<DeckDBData>();

				foreach(List<string> record in rawDBList)
				{
					DeckDBData data = new DeckDBData(
						  record[(int)DeckDBIndex.DeckID]
						, record[(int)DeckDBIndex.PlayerCardID]
						, int.Parse(record[(int)DeckDBIndex.Qty])
					);

					_deckDBList.Add(data);
				}

				Resources.UnloadAsset(csvFile);
				_isLoadDB = true;
				return true;
			}
		}

		_isLoadDB = false;
		return false;
	}
		
	public static bool GetData(string deckID, out List<DeckDBData> data)
	{
		if(!_isLoadDB)
		{
			LoadDB();
		}

		data = new List<DeckDBData>();
		foreach (DeckDBData rawData in _deckDBList)
		{
			if(string.Compare(rawData.DeckID, deckID) == 0)
			{
				data.Add(rawData);
			}
		}

		if (data.Count > 0) 
		{
			 
			return true;
		} 
		else 
		{
			data = null;
		 
			return false;
		}
	}

	public static void DebugPrintData()
	{
        StringBuilder sb = new StringBuilder();

        foreach (DeckDBData data in _deckDBList)
        {
            sb.Append(data.GetDebugPrintText());
            sb.Append("\n\n");
        }
        Debug.Log(sb.ToString());
	}
	#endregion
}
