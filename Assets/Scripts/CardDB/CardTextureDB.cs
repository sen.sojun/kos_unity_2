﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;

public class CardTextureDB
{
	#region Enums
	public enum TextureSize
	{
		  S
		, M
		, L
	}
	#endregion

	#region Private Properties
	private static Dictionary<string, Texture> _cardTextureDBList = null;
	private static string _resourcePath = "Images/CardImage";
	private static bool _isLoadDB = false;
	#endregion
    
    #region Public Properties
    public static bool IsLoadDB { get {return _isLoadDB; } }
    #endregion

	#region Constructors
	static CardTextureDB()
	{
		
	}
	#endregion

	#region Methods
	public static bool LoadDB()
	{
        return false; // try to reduce cache.
    
		_cardTextureDBList = new Dictionary<string, Texture>();

		//Debug.Log(_resourcePath);

		// load at "Resources/Images/CardImage"
		Object[] objList = Resources.LoadAll(_resourcePath, typeof(Texture));

		if(objList.Length > 0)
		{
			foreach(Object obj in objList)
			{
				Texture t = (Texture) obj;

				//Debug.Log("Add Texture: " + t.name);
				_cardTextureDBList.Add(t.name, t);
			}

			foreach(Object obj in objList)
			{
				Resources.UnloadAsset(obj);
			}

			_isLoadDB = true;
			return true;
		}		

		_isLoadDB = false;
		return false;
	}
    
    public static bool GetTexture(string cardImageID, TextureSize size, out Texture texture)
    {
        string fileName = cardImageID + "_" + size.ToString();
        
        if(_cardTextureDBList == null)
        {
            _cardTextureDBList = new Dictionary<string, Texture>();
        }
        
        if(_cardTextureDBList.ContainsKey(fileName))
        {
            texture = _cardTextureDBList[fileName];
            return true;
        }
        else
        {
            Texture t = Resources.Load(_resourcePath + "/" + fileName) as Texture;
            if(t != null)
            {
                Debug.Log("Add texture " + fileName);
                _cardTextureDBList.Add(fileName, t);
            }
            else
            {
                Debug.Log("Not found texture " + fileName);
            }
                
            return _cardTextureDBList.TryGetValue(fileName, out texture);
        }
    }

    /*
	public static bool GetTexture(string cardImageID, TextureSize size, out Texture texture)
	{
		string fileName = cardImageID + "_" + size.ToString();

		if(!_isLoadDB)
		{
			LoadDB();
		}
        
		return _cardTextureDBList.TryGetValue(fileName, out texture);
	}
    */
    
    public static void Clear()
    {
        if(_cardTextureDBList != null)
        {
            _cardTextureDBList.Clear();   
        }
        _isLoadDB = false;
    }
    
	public static void DebugPrintData()
	{
        StringBuilder sb = new StringBuilder();

        foreach (KeyValuePair<string, Texture> data in _cardTextureDBList)
        {
            sb.Append(data.Value.name + "\n");
        }
        Debug.Log(sb.ToString());
	}
	#endregion
}
