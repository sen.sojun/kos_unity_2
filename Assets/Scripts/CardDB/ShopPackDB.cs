﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class ShopPackDBData
{
	#region Public Properties
	public string ShopPackID; //DeckID;
	public string CardID; //PlayerCardID;
	public int iChance; //Qty;
	#endregion

	#region Constructors
	public ShopPackDBData()
	{
		ShopPackID = "";
		CardID = ""; //PlayerCardID = "";
		iChance = 0; //Qty = 0;
	}

	public ShopPackDBData(string shopPackID, string cardID, int chance)
	{
		ShopPackID = shopPackID;
		CardID = cardID;
		iChance = chance;
	}

	public ShopPackDBData(ShopPackDBData shopPackDBData)
	{
		ShopPackID = shopPackDBData.ShopPackID;
		CardID = shopPackDBData.CardID;
		iChance = shopPackDBData.iChance;
	}
	#endregion

	#region Methods
	public string GetDebugPrintText()
	{
		string resultText;
		resultText = string.Format(
			"ShopPackID: {0}\nCardID: {1}\nChance: {2}"
			, ShopPackID
			, CardID
			, iChance
		);

		return resultText;
	}

	public void DebugPrintData()
	{
		Debug.Log(GetDebugPrintText());
	}
	#endregion
}

public class ShopPackDB
{
	#region Enums
	private enum ShopPackDBIndex : int //DeckDBIndex : int
	{
		ShopPackID = 0 //DeckID = 0
		,CardID // PlayerCardID
		,Chance  // Qty
	}
	#endregion

	#region Private Properties
	private static List<ShopPackDBData> _shopPackDBList = null; //List<DeckDBData> _deckDBList = null;
	private static bool _isLoadDB = false;
	#endregion

	#region Constructors
	static ShopPackDB()
	{
	}
	#endregion

	#region Methods
	public static bool LoadDB()
	{
		TextAsset csvFile = Resources.Load<TextAsset>("Databases/KOS-CardDB - ShopPackDB");
		if(csvFile != null)
		{
			List<List<string>> rawDBList;
			bool isSuccess = CSVReader.ReadCSV(csvFile, out rawDBList);
			if (isSuccess)
			{
				_shopPackDBList = new List<ShopPackDBData>();

				foreach(List<string> record in rawDBList)
				{
					ShopPackDBData data = new ShopPackDBData(
						record[(int)ShopPackDBIndex.ShopPackID]
						, record[(int)ShopPackDBIndex.CardID]
						, int.Parse(record[(int)ShopPackDBIndex.Chance])
					);

					_shopPackDBList.Add(data);
				}

				Resources.UnloadAsset(csvFile);
				_isLoadDB = true;
				return true;
			}
		}

		_isLoadDB = false;
		return false;
	}
	//deckID
	public static bool GetData(string shopPackID , out List<ShopPackDBData> data)
	{
		if(!_isLoadDB)
		{
			LoadDB();
		}

		data = new List<ShopPackDBData>();
		foreach (ShopPackDBData rawData in _shopPackDBList)
		{
			if(string.Compare(rawData.ShopPackID, shopPackID) == 0)
			{
				data.Add(rawData);
			}
		}

		if (data.Count > 0) 
		{
			return true;
		} 
		else 
		{
			data = null;
			return false;
		}
	}

	public static void DebugPrintData()
	{
        StringBuilder sb = new StringBuilder();

        foreach (ShopPackDBData data in _shopPackDBList)
        {
            sb.Append(data.GetDebugPrintText());
            sb.Append("\n\n");
        }
        Debug.Log(sb.ToString());
	}
	#endregion
}

