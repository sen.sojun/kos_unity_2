﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;

public class SoloLeagueNPCNameDBData 
{
	#region Public Properties
	public string NPCID;
	public string NPCNAME;
	#endregion

	#region Constructor
	public SoloLeagueNPCNameDBData ()
	{
		NPCID = "";
		NPCNAME = "";
	}

	public SoloLeagueNPCNameDBData (string npcID, string npcName)
	{
		NPCID = npcID;
		NPCNAME = npcName;
	}

	public SoloLeagueNPCNameDBData(SoloLeagueNPCNameDBData soloLeagueNPCNameDBData)
	{
		NPCID = soloLeagueNPCNameDBData.NPCID;
		NPCNAME = soloLeagueNPCNameDBData.NPCNAME;
	}
	#endregion

	#region Methods
	public string GetDebugPrintText()
	{

		string resultText;
		resultText = string.Format(
			"NPCID: {0}\nNPCNAME: {1}"
			, NPCID
			, NPCNAME
		);

		return resultText;
	}
	public void DebugPrintData()
	{
		Debug.Log(GetDebugPrintText());
	}
    #endregion
}

public class SoloLeagueNPCDB
{
	#region Enums
	private enum NPCDBIndex : int
	{
		NPCID = 0
	  , NPCNAME

	}
	#endregion

	#region Private Properties
	private static Dictionary<string, SoloLeagueNPCNameDBData> _npcDBList = null;
	private static bool _isLoadDB = false;
	#endregion

	#region Constructors
	static SoloLeagueNPCDB()
	{
	}
	#endregion
	 
	#region Methods
	public static bool LoadDB()
	{
		TextAsset csvFile = Resources.Load<TextAsset>("Databases/SoloDB - NPCDB");
		if(csvFile != null)
		{
			List<List<string>> rawDBList;
			bool isSuccess = CSVReader.ReadCSV(csvFile, out rawDBList);
			if (isSuccess)
			{
				_npcDBList = new Dictionary<string, SoloLeagueNPCNameDBData>();

				foreach(List<string> record in rawDBList)
				{
					SoloLeagueNPCNameDBData data = new SoloLeagueNPCNameDBData(
						record[(int)NPCDBIndex.NPCID]
						, record[(int)NPCDBIndex.NPCNAME]
					);
					_npcDBList.Add(data.NPCID, data);
				}
				Resources.UnloadAsset(csvFile);
				_isLoadDB = true;
				return true;
			}
		}

		_isLoadDB = false;
		return false;
	}
	public static bool GetData(string npcID, out SoloLeagueNPCNameDBData data)
	{
		if(!_isLoadDB)
		{
			LoadDB();
		}

		return (_npcDBList.TryGetValue(npcID, out data));
	}

	public static void GetAllData(out List<SoloLeagueNPCNameDBData> dataList)
	{
		if(!_isLoadDB)
		{
			LoadDB();
		}

		dataList = new List<SoloLeagueNPCNameDBData>();

		foreach (KeyValuePair<string, SoloLeagueNPCNameDBData> data in _npcDBList)
		{
			dataList.Add(data.Value);
		}
	}

	public static void DebugPrintData()
	{
		StringBuilder sb = new StringBuilder();

		foreach (KeyValuePair<string, SoloLeagueNPCNameDBData> data in _npcDBList)
		{
			sb.Append(data.Value.GetDebugPrintText());
			sb.Append("\n\n");
		}
		Debug.Log(sb.ToString());
	}
	#endregion
}
