﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class CardDBData
{
    #region Public Properties
    public string CardID;

    public string ImageID;
    public string SymbolID;
    public string ClanID;
    public string TypeID;
    public List<string> SubTypeList;
    public List<string> JobList;

    public string Name_EN;
    public string Name_TH;

    public string Alias_EN;
    public string Alias_TH;

    public int LV;
    public int Cost;
    public int Shield;
    public int HitPoint;
    public int Attack;
    public int Speed;
    public int PassiveIndex;

    public string DescriptionText_EN;
    public string DescriptionText_TH;

    public List<string> AbilityIDList;
    #endregion

    #region Constructors
    public CardDBData()
    {
        CardID = "";

        ImageID = "";
        SymbolID = "";
        ClanID = "";
        TypeID = "";
        SubTypeList = new List<string>();
        JobList = new List<string>();
      
        Name_EN = "";
        Name_TH = "";
        Alias_EN = "";
        Alias_TH = "";

        LV = 0;
        Cost = 0;
        Shield = 0;
        HitPoint = 0;
        Attack = 0;
        Speed = 0;
        this.PassiveIndex = 0;

        DescriptionText_EN = "";
        DescriptionText_TH = "";

        AbilityIDList = new List<string>();
    }

    public CardDBData(string cardID, string imageID, string symbolID, string clanID, string typeID, List<string> subTypeList, List<string> jobList, string name_EN, string name_TH, string alias_EN, string alias_TH, int lv, int cost, int shield, int hitPoint, int attack, int speed, int passiveIndex, string descriptionText_EN, string descriptionText_TH, List<string> abilityIDList)
    {
        CardID = cardID;

        ImageID = imageID;
        SymbolID = symbolID;
        ClanID = clanID;
        TypeID = typeID;
        SubTypeList = new List<string>(subTypeList);
        JobList = new List<string>(jobList);

        Name_EN = name_EN;
        Name_TH = name_TH;
        Alias_EN = alias_EN;
        Alias_TH = alias_TH;

        LV = lv;
        Cost = cost;
        Shield = shield;
        HitPoint = hitPoint;
        Attack = attack;
        Speed = speed;
        this.PassiveIndex = passiveIndex;

        DescriptionText_EN = descriptionText_EN;
        DescriptionText_TH = descriptionText_TH;

        AbilityIDList = new List<string>(abilityIDList);
    }

    public CardDBData(CardDBData cardDBData)
    {
        CardID = cardDBData.CardID;

        ImageID = cardDBData.ImageID;
        SymbolID = cardDBData.SymbolID;
        ClanID = cardDBData.ClanID;
        TypeID = cardDBData.TypeID;
        SubTypeList = new List<string>(cardDBData.SubTypeList);
        JobList = new List<string>(cardDBData.JobList);

        Name_EN = cardDBData.Name_EN;
        Name_TH = cardDBData.Name_TH;
        Alias_EN = cardDBData.Alias_EN;
        Alias_TH = cardDBData.Alias_TH;

        LV = cardDBData.LV;
        Cost = cardDBData.Cost;
        Shield = cardDBData.Shield;
        HitPoint = cardDBData.HitPoint;
        Attack = cardDBData.Attack;
        Speed = cardDBData.Speed;
        this.PassiveIndex = cardDBData.PassiveIndex;

        DescriptionText_EN = cardDBData.DescriptionText_EN;
        DescriptionText_TH = cardDBData.DescriptionText_TH;

        AbilityIDList = new List<string>(cardDBData.AbilityIDList);
    }
    #endregion

    #region Methods
    public string GetDebugPrintText()
    {
        string subTypeIDText = "";
        foreach (string subTypeID in SubTypeList)
        {
            subTypeIDText += " " + subTypeID;
        }

        string jobIDText = "";
        foreach (string jobID in JobList)
        {
            jobIDText += " " + jobID;
        }

        string abilityIDText = "";
        foreach(string abiltyID in AbilityIDList)
        {
            abilityIDText += "," + abiltyID;
        }

        string resultText;
        resultText = string.Format(
            "CardID: {0}\nImageID: {1}\nSymbolID: {2}\nClanID: {3}\nTypeID: {4}\nSubTypeID: {5}\nJobID: {6}" 
            + "\nName English: {7}\nName Thai: {8}"
            + "\nAlias English:{9}\n Alias Thai:{10}"
            + "LV: {11}\nCost: {12}\n Shield: {13}\nHitPoint: {14}\nAttack: {15}\nSpeed: {16}\nPassiveIndex: {17}"
            + "\nDescription Text English: {18}\n\nDescription Text Thai: {19}"
            + "\nAbilityIDList: {20}"
            , CardID
            , ImageID
            , SymbolID
            , ClanID
            , TypeID
            , subTypeIDText
            , jobIDText
            , Name_EN, Name_TH
            , Alias_EN, Alias_TH
            , LV
            , Cost
            , Shield
            , HitPoint
            , Attack
            , Speed
            , this.PassiveIndex
            , DescriptionText_EN, DescriptionText_TH
            , abilityIDText
        );
        return (resultText);
    }

    public void DebugPrintData()
    {
        Debug.Log(GetDebugPrintText());
    }
    #endregion
}

public class CardDB
{
    #region Enums
    private enum CardDBIndex : int
    {
          CardID = 0
        , ImageID
        , SymbolID
        , ClanID
        , TypeID
        , SubTypeList
        , JobList
        , Name_EN
        , Name_TH
        , Alias_EN
        , Alias_TH
        , LV
        , Cost
        , Shield
        , HitPoint
        , Attack
        , Speed
        , PassiveIndex
        , DescriptionText_EN
        , DescriptionText_TH
        , AbilityList
    }
    #endregion

    #region Private Properties
    private static Dictionary<string, CardDBData> _cardDBList = null;
	private static bool _isLoadDB = false;
    #endregion
    
    #region Public Properties
    public static bool IsLoadDB { get {return _isLoadDB; } }
    #endregion

    #region Constructors
    static CardDB()
    {
    }
    #endregion

    #region Methods
    public static bool LoadDB()
    {
		TextAsset csvFile = Resources.Load<TextAsset>("Databases/KOS-CardDB - CardDB");

		if(csvFile != null)
		{
	        List<List<string>> rawDBList;
	        bool isSuccess = CSVReader.ReadCSV(csvFile, out rawDBList);
	        if (isSuccess)
	        {
	            _cardDBList = new Dictionary<string, CardDBData>();

				/*
	       		// Print Debug
	            string debugText = "";
	            debugText = "[" + rawDBList.Count.ToString() + "]:";
	            int i = 0;
	            foreach (List<string> sList in rawDBList)
	            {
	                string rowText = "";
	                foreach (string s in sList)
	                {
						if(rowText.Length > 0)
						{
							rowText += ", ";
						}
	
						rowText += s;
	                }
	                Debug.Log("[" + i.ToString() + "]:" + rowText);
	                //debugText += "[" + i.ToString() + "]:" + rowText;
	                ++i;
	            }
	            Debug.Log(debugText);
				*/

	            foreach (List<string> record in rawDBList)
	            {
	                //Debug.Log("ID:" + record[(int)CardDBIndex.CardID].ToString());

	                int lv = int.Parse(record[(int)CardDBIndex.LV]);
	                int cost = int.Parse(record[(int)CardDBIndex.Cost]);
	                int shield = int.Parse(record[(int)CardDBIndex.Shield]);
	                int hitPoint = int.Parse(record[(int)CardDBIndex.HitPoint]);
	                int attack = int.Parse(record[(int)CardDBIndex.Attack]);
	                int speed = int.Parse(record[(int)CardDBIndex.Speed]);
	                int passiveIndex = int.Parse(record[(int)CardDBIndex.PassiveIndex]);

	                // Split subTypeID string
	                List<string> subTypeIDList = new List<string>();
	                string[] subTypeList = record[(int)CardDBIndex.SubTypeList].Split(',');
	                foreach (string subType in subTypeList)
	                {
	                    subTypeIDList.Add(subType);
	                }

	                // Split JobID string
	                List<string> jobIDList = new List<string>();
	                string[] jobList = record[(int)CardDBIndex.JobList].Split(',');
	                foreach (string job in jobList)
	                {
	                    jobIDList.Add(job);
	                }

	                // Split abilityID string
	                List<string> abilityIDList = new List<string>();
					string abilityIDListText = record[(int)CardDBIndex.AbilityList];
					string abilityID = "";
					bool isFoundBracket = false;
					for(int index = 0; index < abilityIDListText.Length; ++index)
					{
						char c = abilityIDListText[index];

						if(isFoundBracket)
						{
							if(c == ')')
							{
								isFoundBracket = false;
							}

							abilityID += c;
						}
						else 
						{
							if(c == ',')
							{
								// Cut string
								abilityIDList.Add(abilityID.Trim());
								//Debug.Log(abilityID);
								abilityID = "";
							}
							else
							{
								if(c == '(')
								{
									isFoundBracket = true;
								}

								abilityID += c;
							}
						}

						if(index + 1 >= abilityIDListText.Length && abilityID.Length > 0)
						{
							// Last character.
							abilityIDList.Add(abilityID.Trim());
							abilityID = "";
						}
					}

	                CardDBData data = new CardDBData(
	                      record[(int)CardDBIndex.CardID]
	                    , record[(int)CardDBIndex.ImageID]
	                    , record[(int)CardDBIndex.SymbolID]
	                    , record[(int)CardDBIndex.ClanID]
	                    , record[(int)CardDBIndex.TypeID]
	                    , subTypeIDList
	                    , jobIDList
	                    , record[(int)CardDBIndex.Name_EN]
                        , record[(int)CardDBIndex.Name_TH]
                        , record[(int)CardDBIndex.Alias_EN]
                        , record[(int)CardDBIndex.Alias_TH]
                        , lv
	                    , cost
	                    , shield
	                    , hitPoint
	                    , attack
	                    , speed
	                    , passiveIndex
	                    , record[(int)CardDBIndex.DescriptionText_EN]
                        , record[(int)CardDBIndex.DescriptionText_TH]
                        , abilityIDList
	                );

	                //Debug.Log(data.GetDebugPrintText());
	                
	                _cardDBList.Add(data.CardID, data);
	            }

				Resources.UnloadAsset(csvFile);
				_isLoadDB = true;
	            return true;
	        }
		}

		_isLoadDB = false;
        return false;
    }

    public static bool GetData(string cardID, out CardDBData data)
    {
		if(!_isLoadDB)
		{
			CardDB.LoadDB();
		}

        return (_cardDBList.TryGetValue(cardID, out data));
    }

    public static bool GatAllData(out List<CardDBData> data)
    {
		if(!_isLoadDB)
		{
			CardDB.LoadDB();
		}

        data = new List<CardDBData>();
        if (_cardDBList != null)
        {
            foreach (KeyValuePair<string, CardDBData> entry in _cardDBList)
            {
                data.Add(new CardDBData(entry.Value));
            }

            return true;
        }

        return false;
    }

    public static void DebugPrintData()
    {
        StringBuilder sb = new StringBuilder();

        foreach (KeyValuePair<string, CardDBData> data in _cardDBList)
        {
            sb.Append(data.Value.GetDebugPrintText());
            sb.Append("\n\n");
        }
        Debug.Log(sb.ToString());
    }
    #endregion
}
