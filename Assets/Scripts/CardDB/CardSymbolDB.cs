﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class CardSymbolDBData
{
    #region Public Properties
    public string CardSymbolID;
    public string ImageFile;
    public string SymbolName_EN;
    public string SymbolName_TH;
    #endregion

    #region Constructors
    public CardSymbolDBData()
    {
        CardSymbolID = "";
        ImageFile = "";
        SymbolName_EN = "";
        SymbolName_TH = "";
    }

    public CardSymbolDBData(string cardSymbolID, string imageFile, string symbolName_EN, string symbolName_TH)
    {
        CardSymbolID = cardSymbolID;
        ImageFile = imageFile;
        SymbolName_EN = symbolName_EN;
        SymbolName_TH = symbolName_TH;
    }
    #endregion

    #region Methods
    public string GetDebugPrintText()
    {
        string resultText;
        resultText = string.Format(
            "CardSymbolID: {0}\nImageFile: {1}\nSymbolName English: {2}\nSymbolName Thai: {3}"
            , CardSymbolID
            , ImageFile
            , SymbolName_EN
            , SymbolName_TH
        );
        return (resultText);
    }

    public void DebugPrintData()
    {
        Debug.Log(GetDebugPrintText());
    }
    #endregion
}

public class CardSymbolDB 
{
    #region Enums
    private enum CardSymbolDBIndex : int
    {
          CardSymbolID = 0
        , ImageFile
        , SymbolName_EN
        , SymbolName_TH
    }
    #endregion

    #region Private Properties
    private static Dictionary<string, CardSymbolDBData> _cardSymbolDBList = null;
	private static bool _isLoadDB = false;
    #endregion
    
    #region Public Properties
    public static bool IsLoadDB { get {return _isLoadDB; } }
    #endregion

    #region Constructors
    static CardSymbolDB()
    {
    }
    #endregion

    #region Methods
    public static bool LoadDB()
    {
		TextAsset csvFile = Resources.Load<TextAsset>("Databases/KOS-CardDB - CardSymbolDB");

		if(csvFile != null)
		{
	        List<List<string>> rawDBList;
	        bool isSuccess = CSVReader.ReadCSV(csvFile, out rawDBList);
	        if (isSuccess)
	        {
	            _cardSymbolDBList = new Dictionary<string, CardSymbolDBData>();

	            foreach (List<string> record in rawDBList)
	            {
	                CardSymbolDBData data = new CardSymbolDBData(
	                      record[(int)CardSymbolDBIndex.CardSymbolID]
	                    , record[(int)CardSymbolDBIndex.ImageFile]
                        , record[(int)CardSymbolDBIndex.SymbolName_EN]
                        , record[(int)CardSymbolDBIndex.SymbolName_TH]
                    );
	                _cardSymbolDBList.Add(data.CardSymbolID, data);
	            }

				Resources.UnloadAsset(csvFile);
				_isLoadDB = true;
	            return true;
	        }
		}

		_isLoadDB = false;
        return false;
    }

    public static bool GetData(string cardSymbolID, out CardSymbolDBData data)
    {
		if(!_isLoadDB)
		{
			LoadDB();
		}

        return (_cardSymbolDBList.TryGetValue(cardSymbolID, out data));
    }

    public static void DebugPrintData()
    {
        StringBuilder sb = new StringBuilder();

        foreach (KeyValuePair<string, CardSymbolDBData> data in _cardSymbolDBList)
        {
            sb.Append(data.Value.GetDebugPrintText());
            sb.Append("\n\n");
        }
        Debug.Log(sb.ToString());
    }
    #endregion

}
