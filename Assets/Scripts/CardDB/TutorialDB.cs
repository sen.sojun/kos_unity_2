﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class TutorialDBData
{
    #region Private Properties
	private string _tutorialID;
	private string _tutorialCode;
	private string _displayMode;
	private string _imagePath;
	private int _showInFirstFlow;
	private string _buttonKey;

	private string _titleEng;
	private string _titleTh;

	private string _textEng;
	private string _textTh;
	#endregion

	#region Public Properties
	public string TutorialID { get { return _tutorialID; } }
	public string TutorialCode { get { return _tutorialCode; } }
	public string DisplayMode { get { return _displayMode; } }
    public string ImagePath { get { return _imagePath; } }
	public int ShowInFirstFlow { get { return _showInFirstFlow; } }
	public string ButtonKey { get { return _buttonKey; } }

	public string TitleEng { get { return _titleEng; } }
	public string TitleTh { get { return _titleTh; } }

    public string TextEng { get { return _textEng; } }
	public string TextTh { get { return _textTh; } }
	#endregion

	#region Constructors
	public TutorialDBData()
	{
	    _tutorialID = "";
	    _tutorialCode = "";
		_displayMode = "";
		_imagePath = "";
		_showInFirstFlow = 0;
		_buttonKey = "";

		_titleEng = "";
		_titleTh = "";
	    
	    _textEng = "";
	    _textTh = "";
	}

	public TutorialDBData(string tutorialID, string tutorialCode, string displayMode, string imagePath, int showInFirstFlow, string buttonKey, string titleEng, string titleTh, string textEng, string textTh)
	{
		_tutorialID = tutorialID;
		_tutorialCode = tutorialCode;
		_displayMode = displayMode;
		_imagePath = imagePath;
		_showInFirstFlow = showInFirstFlow;
		_buttonKey = buttonKey;

		_titleEng = titleEng;
		_titleTh = titleTh;

		_textEng = textEng;
		_textTh = textTh;
	}

	// Copy Constructor
	public TutorialDBData(TutorialDBData data)
	{
		_tutorialID = data.TutorialID;
		_tutorialCode = data.TutorialCode;
		_displayMode = data.DisplayMode;
		_imagePath = data.ImagePath;
		_showInFirstFlow = data.ShowInFirstFlow;
		_buttonKey = data.ButtonKey;

		_titleEng = data.TitleEng;
		_titleTh = data.TitleTh;

		_textEng = data.TextEng;
		_textTh = data.TextTh;
	}
	#endregion

	#region Methods
	public string GetDebugPrintText()
	{
		string resultText;
		resultText = string.Format(
			"Tutorial ID: {0}\n" +
			"Tutorial Code: {1}\n" +
			"Diaplay Mode: {2}\n" +
			"Image Path: {3}\n" +
			"Show In First Flow: {4}\n" +
			"Tutorail ButtonText: {5}\n" +

			"Title English: {6}\n" +
			"Title Thai: {7}\n" +
			"Text English: {8}\n" +
			"Text Thai: {9}"

            , TutorialID
            , TutorialCode
			, DisplayMode
			, ImagePath
			, ShowInFirstFlow
			, ButtonKey

			, TitleEng
			, TitleTh      
            , TextEng
            , TextTh
		);

		return resultText;
	}

	public void DebugPrintData()
	{
		Debug.Log(GetDebugPrintText());
	}
	#endregion
}

public class TutorialDB
{
	#region Enums
	private enum TutorialDBIndex : int
	{
		  TutorialID = 0
        , TutorialCode
		, DisplayMode
		, TutorialImageID
		, ShowInFirstFlow
		, ButtonKey
		, TitleEng
		, TitleTh
		, TextEng
		, TextTh
	}
	#endregion

	#region Private Properties
    private static Dictionary<string, TutorialDBData> _tutorialDBList = null;
	private static bool _isLoadDB = false;
	#endregion
    
    #region Public Properties
    public static bool IsLoadDB { get {return _isLoadDB; } }
    #endregion

    /*
	#region Constructors
	static TutorialDB()
	{
	}
	#endregion
    */

	#region Methods
	public static bool LoadDB()
	{
		TextAsset csvFile = Resources.Load<TextAsset>("Databases/KOS-CardDB - TutorialDB");
		if(csvFile != null)
		{
			List<List<string>> rawDBList;
			bool isSuccess = CSVReader.ReadCSV(csvFile, out rawDBList);
			if (isSuccess)
			{
                _tutorialDBList = new Dictionary<string, TutorialDBData>();

				//Debug.Log ("r:" + rawDBList.Count);
				foreach(List<string> record in rawDBList)
				{
					//Debug.Log (record[(int)TutorialDBIndex.TutorialID]);

                    TutorialDBData data = new TutorialDBData(
                          record[(int)TutorialDBIndex.TutorialID]
                        , record[(int)TutorialDBIndex.TutorialCode]
						, record[(int)TutorialDBIndex.DisplayMode]
						, "Images/FirstFlowImage/" + record[(int)TutorialDBIndex.TutorialImageID]
						, int.Parse(record[(int)TutorialDBIndex.ShowInFirstFlow])
						, record[(int)TutorialDBIndex.ButtonKey]

						, record[(int)TutorialDBIndex.TitleEng]
						, record[(int)TutorialDBIndex.TitleTh]

                        , record[(int)TutorialDBIndex.TextEng]
                        , record[(int)TutorialDBIndex.TextTh]
					);

					_tutorialDBList.Add (data.TutorialID, data);
				}

				Resources.UnloadAsset(csvFile);
				_isLoadDB = true;
				return true;
			}
		}
        else {
            Debug.Log("Error Loading Tutorial DB!!");
        }
		_isLoadDB = false;
		return false;
	}

    public static bool GetData(string tutorialID, out TutorialDBData data)
	{
		if(!_isLoadDB)
		{
			TutorialDB.LoadDB();
		}

		return (_tutorialDBList.TryGetValue(tutorialID, out data));
	}

    public static bool GetDataByCode(string tutorialCode, out List<TutorialDBData> data)
    {
        if (!_isLoadDB)
        {
            LoadDB();
        }

        data = new List<TutorialDBData>();

        foreach (KeyValuePair<string, TutorialDBData> rawData in _tutorialDBList)
        {
            if (string.Compare(rawData.Value.TutorialCode, tutorialCode) == 0)
            {
                data.Add(new TutorialDBData(rawData.Value));
            }

        }

        if (data.Count > 0)
        {
            return true;
        }
        else
        {
            data = null;
            return false;
        }
    }

    public static bool GetAllData(out List<TutorialDBData> data)
	{
		if(!_isLoadDB)
		{
			TutorialDB.LoadDB();
		}

        data = new List<TutorialDBData>();
		if (_tutorialDBList != null)
		{
            foreach (KeyValuePair<string, TutorialDBData> entry in _tutorialDBList)
			{
                data.Add(new TutorialDBData(entry.Value));
			}

			return true;
		}

		return false;
	}

	public static void DebugPrintData()
	{
        StringBuilder sb = new StringBuilder();

        foreach (KeyValuePair<string, TutorialDBData> data in _tutorialDBList)
		{
            sb.Append(data.Value.GetDebugPrintText());
            sb.Append("\n\n");
		}
        Debug.Log(sb.ToString());
	}
	#endregion
}
