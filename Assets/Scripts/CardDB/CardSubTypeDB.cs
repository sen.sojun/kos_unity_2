﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class CardSubTypeDBData
{
    #region Public Properties
    public string CardSubTypeID;
    public string SubTypeName_EN;
    public string SubTypeName_TH;
    #endregion

    #region Constructors
    public CardSubTypeDBData()
    {
        CardSubTypeID = "";
        SubTypeName_EN = "";
        SubTypeName_TH = "";
    }

    public CardSubTypeDBData(string cardSubTypeID, string subTypeName_EN, string subTypeName_TH)
    {
        CardSubTypeID = cardSubTypeID;
        SubTypeName_EN = subTypeName_EN;
        SubTypeName_TH = subTypeName_TH;
    }
    #endregion

    #region Methods
    public string GetDebugPrintText()
    {
        string resultText;
        resultText = string.Format(
            "CardSubTypeID: {0}\nSubTypeName English: {1}\nSubTypeName Thai: {2}"
            , CardSubTypeID
            , SubTypeName_EN
            , SubTypeName_TH
        );
        return (resultText);
    }

    public void DebugPrintData()
    {
        Debug.Log(GetDebugPrintText());
    }
    #endregion
}

public class CardSubTypeDB 
{
    #region Enums
    private enum CardSubTypeDBIndex : int
    {
          CardSubTypeID = 0
        , SubTypeName_EN
        , SubTypeName_TH
    }
    #endregion

    #region Private Properties
    private static Dictionary<string, CardSubTypeDBData> _cardSubTypeDBList = null;
	private static bool _isLoadDB = false;
    #endregion
    
    #region Public Properties
    public static bool IsLoadDB { get {return _isLoadDB; } }
    #endregion

    #region Constructors
    static CardSubTypeDB()
    {
    }
    #endregion

    #region Methods
    public static bool LoadDB()
    {
		TextAsset csvFile = Resources.Load<TextAsset>("Databases/KOS-CardDB - CardSubTypeDB");

		if(csvFile != null)
		{
	        List<List<string>> rawDBList;
	        bool isSuccess = CSVReader.ReadCSV(csvFile, out rawDBList);
	        if (isSuccess)
	        {
	            _cardSubTypeDBList = new Dictionary<string, CardSubTypeDBData>();

	            foreach (List<string> record in rawDBList)
	            {
	                CardSubTypeDBData data = new CardSubTypeDBData(
	                      record[(int)CardSubTypeDBIndex.CardSubTypeID]
	                    , record[(int)CardSubTypeDBIndex.SubTypeName_EN]
                        , record[(int)CardSubTypeDBIndex.SubTypeName_TH]
                     );
	                _cardSubTypeDBList.Add(data.CardSubTypeID, data);
	            }

				Resources.UnloadAsset(csvFile);
				_isLoadDB = true;
	            return true;
	        }
		}

		_isLoadDB = false;
        return false;
    }

    public static bool GetData(string cardSubTypeID, out CardSubTypeDBData data)
    {
		if(!_isLoadDB)
		{
			LoadDB();
		}

        return (_cardSubTypeDBList.TryGetValue(cardSubTypeID, out data));
    }

    public static void DebugPrintData()
    {
        StringBuilder sb = new StringBuilder();

        foreach (KeyValuePair<string, CardSubTypeDBData> data in _cardSubTypeDBList)
        {
            sb.Append(data.Value.GetDebugPrintText());
            sb.Append("\n\n");
        }
        Debug.Log(sb.ToString());
    }
    #endregion

}
