﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class CardJobDBData
{
    #region Public Properties
    public string CardJobID;
    public string JobName_EN;
    public string JobName_TH;
    #endregion

    #region Constructors
    public CardJobDBData()
    {
        CardJobID = "";
        JobName_EN = "";
        JobName_TH = "";
    }

    public CardJobDBData(string cardJobID, string jobName_EN, string jobName_TH)
    {
        CardJobID = cardJobID;
        JobName_EN = jobName_EN;
        JobName_TH = jobName_TH;
    }
    #endregion

    #region Methods
    public string GetDebugPrintText()
    {
        string resultText;
        resultText = string.Format(
            "CardJobID: {0}\nJobName English: {1}\nJobName Thai: {2}"
            , CardJobID
            , JobName_EN
            , JobName_TH
        );
        return (resultText);
    }

    public void DebugPrintData()
    {
        Debug.Log(GetDebugPrintText());
    }
    #endregion
}

public class CardJobDB
{
    #region Enums
    private enum CardJobDBIndex : int
    {
          CardJobID = 0
        , JobName_EN
        , JobName_TH
    }
    #endregion

    #region Private Properties
    private static Dictionary<string, CardJobDBData> _cardJobDBList = null;
	private static bool _isLoadDB = false;
    #endregion
    
    #region Public Properties
    public static bool IsLoadDB { get {return _isLoadDB; } }
    #endregion

    #region Constructors
    static CardJobDB()
    {
    }
    #endregion

    #region Methods
    public static bool LoadDB()
    {
		TextAsset csvFile = Resources.Load<TextAsset>("Databases/KOS-CardDB - CardJobDB");

		if(csvFile != null)
		{
	        List<List<string>> rawDBList;
	        bool isSuccess = CSVReader.ReadCSV(csvFile, out rawDBList);
	        if (isSuccess)
	        {
	            _cardJobDBList = new Dictionary<string, CardJobDBData>();

	            foreach (List<string> record in rawDBList)
	            {
	                CardJobDBData data = new CardJobDBData(
	                      record[(int)CardJobDBIndex.CardJobID]
	                    , record[(int)CardJobDBIndex.JobName_EN]
                        , record[(int)CardJobDBIndex.JobName_TH]
                     );
	                _cardJobDBList.Add(data.CardJobID, data);
	            }

				Resources.UnloadAsset(csvFile);
				_isLoadDB = true;
	            return true;
	        }
		}

		_isLoadDB = false;
        return false;
    }

    public static bool GetData(string cardJobID, out CardJobDBData data)
    {
		if(!_isLoadDB)
		{
			LoadDB();
		}

        return (_cardJobDBList.TryGetValue(cardJobID, out data));
    }

    public static void DebugPrintData()
    {
        StringBuilder sb = new StringBuilder();

        foreach (KeyValuePair<string, CardJobDBData> data in _cardJobDBList)
        {
            sb.Append(data.Value.GetDebugPrintText());
            sb.Append("\n\n");
        }
        Debug.Log(sb.ToString());
    }
    #endregion

}
