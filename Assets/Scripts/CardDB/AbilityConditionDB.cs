﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class AbilityConditionDBData 
{
	#region Public Properties
	public string AbilityConditionID;
	public string ConditionName;
	public int ParameterNum;
	public string Description;
	#endregion

	#region Constructors
	public AbilityConditionDBData()
	{
		AbilityConditionID = "";
		ConditionName = "";
		ParameterNum = 0;
		Description = "";
	}

	public AbilityConditionDBData(string abilityConditionID, string conditionName, int parameterNum, string description)
	{
		AbilityConditionID = abilityConditionID;
		ConditionName = conditionName;
		ParameterNum = parameterNum;
		Description = description;
	}

	public AbilityConditionDBData(AbilityConditionDBData abilityConditionDBData)
	{
		AbilityConditionID = abilityConditionDBData.AbilityConditionID;
		ConditionName = abilityConditionDBData.ConditionName;
		ParameterNum = abilityConditionDBData.ParameterNum;
		Description = abilityConditionDBData.Description;
	}
	#endregion

	#region Methods
	public string GetDebugPrintText()
	{
		string resultText;
		resultText = string.Format(
			  "AbilityConditionID: {0}\nConditionName: {1}\nParameterNum: {2}\nDescription: {3}"
			, AbilityConditionID
			, ConditionName
			, ParameterNum
			, Description
		);

		return resultText;
	}

	public void DebugPrintData()
	{
		Debug.Log(GetDebugPrintText());
	}
	#endregion
}

public class AbilityConditionDB
{
	#region Enums
	private enum AbilityConditionDBIndex : int
	{
		  AbilityConditionID = 0
		, ConditionName
		, ParameterNum
		, Description
	}
	#endregion

	#region Private Properties
	private static Dictionary<string, AbilityConditionDBData> _abilityConditionDBList = null;
	private static bool _isLoadDB = false;
	#endregion
    
    #region Public Properties
    public static bool IsLoadDB { get {return _isLoadDB; } }
    #endregion

	#region Methods
	public static bool LoadDB()
	{
		TextAsset csvFile = Resources.Load<TextAsset>("Databases/KOS-CardDB - AbilityConditionDB");

		if(csvFile != null)
		{
			List<List<string>> rawDBList;
			bool isSuccess = CSVReader.ReadCSV(csvFile, out rawDBList);
			if (isSuccess)
			{
				_abilityConditionDBList = new Dictionary<string, AbilityConditionDBData>();

				foreach(List<string> record in rawDBList)
				{
					AbilityConditionDBData data = new AbilityConditionDBData(
						  record[(int)AbilityConditionDBIndex.AbilityConditionID]
						, record[(int)AbilityConditionDBIndex.ConditionName]
						, int.Parse(record[(int)AbilityConditionDBIndex.ParameterNum])
						, record[(int)AbilityConditionDBIndex.Description]
					);

					_abilityConditionDBList.Add(data.AbilityConditionID, data);
				}

				Resources.UnloadAsset(csvFile);
				_isLoadDB = true;
				return true;
			}
		}

		_isLoadDB = false;
		return false;
	}
		
	public static bool GetData(string abilityConditionID, out AbilityConditionDBData data)
	{
		if(!_isLoadDB)
		{
			LoadDB();
		}

		return (_abilityConditionDBList.TryGetValue(abilityConditionID, out data));
	}

	public static void DebugPrintData()
	{
        StringBuilder sb = new StringBuilder();

        foreach (KeyValuePair<string, AbilityConditionDBData> data in _abilityConditionDBList)
        {
            sb.Append(data.Value.GetDebugPrintText());
            sb.Append("\n\n");
        }
        Debug.Log(sb.ToString());
	}
	#endregion
}



