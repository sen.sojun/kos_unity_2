﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class AnnouncementDBData
{
	#region Private Properties
	private string _id;
	private string _code;
	private int _isActive;
	private string _title;
	private string _date;
	private string _buttonTextKey; 
	private string _imageID;
	private string _textEng;
	private string _textTh;
	#endregion

	#region Public Properties
	public string ID { get { return _id; } }
	public int IsActive { get { return _isActive; } }
	public string Code { get { return _code; } }
	public string Title { get { return _title; } }
	public string Date { get { return _date; } }
	public string ButtonTextKey { get { return _buttonTextKey; } }
	public string ImageID { get { return _imageID; } }
	public string TextEng { get { return _textEng; } }
	public string TextTh { get { return _textTh; } }
	#endregion

	#region Constructors
	public AnnouncementDBData()
	{
		_id = "";
		_isActive = 0;
		_code = "";
		_title = ""; 
		_date = "";
		_buttonTextKey = ""; 
		_imageID = "";
		_textEng = "";
		_textTh = "";
	}

	public AnnouncementDBData(string id, int isActive, string code, string title, string date, string buttonTextKey, string imageID, string textEng, string textTh)
	{
		_id = id;
		_isActive = isActive;
		_code = code;
		_title = title;   
		_date = date;
		_buttonTextKey = buttonTextKey; 
		_imageID = imageID;
		_textEng = textEng;
		_textTh = textTh;
	}

	public AnnouncementDBData(AnnouncementDBData announementDBData)
	{
		_id = announementDBData.ID;
		_isActive = announementDBData.IsActive;
		_code = announementDBData.Code;
		_title = announementDBData.Title;   
		_date = announementDBData.Date;
		_buttonTextKey = announementDBData.ButtonTextKey; 
		_imageID = announementDBData.ImageID;
		_textEng = announementDBData.TextEng;
		_textTh = announementDBData.TextTh;
	}
	#endregion

	#region Methods
	public string GetDebugPrintText()
	{
		string resultText;
		resultText = string.Format(
			"Announement ID: {0}\nActive: {1}\nCode: {2}\nTitle: {3}\nDate: {4}\nButtonTextKey: {5}\nImage ID: {6}\nText English: {7}\nText Thai: {8}"
			, ID
			, IsActive
			, Code
			, Title
			, Date
			, ButtonTextKey
			, ImageID
			, TextEng
			, TextTh
		);

		return resultText;
	}

	public void DebugPrintData()
	{
		Debug.Log(GetDebugPrintText());
	}
	#endregion
}

public class AnnouncementDB
{
	#region Enums
	private enum AnnounementDBIndex : int
	{
		  AnnounementID = 0
		, AnnounementActive
        , AnnounementTitle
        , AnnounementCode
		, AnnounementDate
		, AnnounementButtonText
		, AnnounementImageID
		, TextEng
		, TextTh
	}
	#endregion

	#region Private Properties
	private static Dictionary<string, AnnouncementDBData> _announcementDBList = null;
	private static bool _isLoadDB = false;
	#endregion

	/*
	#region Constructors
	static TutorialDB()
	{
	}
	#endregion
    */

	#region Methods
	public static bool LoadDB()
	{
		TextAsset csvFile = Resources.Load<TextAsset>("Databases/KOS-CardDB - AnnouncementDB");
		if(csvFile != null)
		{
			List<List<string>> rawDBList;
			bool isSuccess = CSVReader.ReadCSV(csvFile, out rawDBList);
			if (isSuccess)
			{
				_announcementDBList = new Dictionary<string, AnnouncementDBData>();
                string imagePath = "Images/AnnouncementImage/";

                //Debug.Log ("r:" + rawDBList.Count);
                foreach (List<string> record in rawDBList)
				{
					//Debug.Log (record[(int)AnnounementDBIndex.AnnounementID]);

					AnnouncementDBData data = new AnnouncementDBData(
						  record[(int)AnnounementDBIndex.AnnounementID]
						, int.Parse(record[(int)AnnounementDBIndex.AnnounementActive])
						, record[(int)AnnounementDBIndex.AnnounementCode]
						, record[(int)AnnounementDBIndex.AnnounementTitle]
						, record[(int)AnnounementDBIndex.AnnounementDate]
						, record[(int) AnnounementDBIndex.AnnounementButtonText]
						, imagePath + record[(int) AnnounementDBIndex.AnnounementImageID]
						, record[(int)AnnounementDBIndex.TextEng]
						, record[(int)AnnounementDBIndex.TextTh]
					);

					_announcementDBList.Add (data.ID, data);
				}

				Resources.UnloadAsset(csvFile);
				_isLoadDB = true;
				return true;
			}
		}

		_isLoadDB = false;
		return false;
	}

	public static bool GetData(string announcementID, out AnnouncementDBData data)
	{
		if(!_isLoadDB)
		{
			AnnouncementDB.LoadDB();
		}

		return (_announcementDBList.TryGetValue(announcementID, out data));
	}

	public static bool GetDataByCode(string announcementCode, out List<AnnouncementDBData> data)
	{
		if (!_isLoadDB)
		{
			LoadDB();
		}

		data = new List<AnnouncementDBData>();

		foreach (KeyValuePair<string, AnnouncementDBData> rawData in _announcementDBList)
		{
			if (string.Compare(rawData.Value.Code, announcementCode) == 0)
			{
				data.Add(new AnnouncementDBData(rawData.Value));
			}

		}

		if (data.Count > 0)
		{
			return true;
		}
		else
		{
			data = null;
			return false;
		}
	}

	public static bool GetAllData(out List<AnnouncementDBData> data)
	{
		if(!_isLoadDB)
		{
			AnnouncementDB.LoadDB();
		}

		data = new List<AnnouncementDBData>();
		if (_announcementDBList != null)
		{
			foreach (KeyValuePair<string, AnnouncementDBData> entry in _announcementDBList)
			{
				data.Add(new AnnouncementDBData(entry.Value));
			}

			return true;
		}

		return false;
	}

	public static void DebugPrintData()
	{
		StringBuilder sb = new StringBuilder();

		foreach (KeyValuePair<string, AnnouncementDBData> data in _announcementDBList)
		{
			sb.Append(data.Value.GetDebugPrintText());
			sb.Append("\n\n");
		}
		Debug.Log(sb.ToString());
	}
	#endregion
}

