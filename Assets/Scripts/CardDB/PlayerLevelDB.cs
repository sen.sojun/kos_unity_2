﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class PlayerLevelDBData
{
	#region Public Properties
	public string ExpID;
	public int Level;
	public int Exp;

	#endregion

	#region Constructors
	public PlayerLevelDBData()
	{
		ExpID = "";
		Level = 0;
		Exp = 0;
	}

	public PlayerLevelDBData(string sExpID,int iLevel, int iExp)
	{
		ExpID = sExpID;
		Level = iLevel;
		Exp = iExp;
	}

	public PlayerLevelDBData(PlayerLevelDBData playerLevelDBData)
	{
		ExpID = playerLevelDBData.ExpID;
		Level = playerLevelDBData.Level;
		Exp = playerLevelDBData.Exp;
	}
	#endregion


	#region Methods
	public string GetDebugPrintText()
	{
		string resultText;
		resultText = string.Format(
			"ExpID: {0}\nLevel: {1}\nExp: {2}"
			, ExpID
			, Level
			, Exp
		);

		return resultText;
	}

	public void DebugPrintData()
	{
		Debug.Log(GetDebugPrintText());
	}
	#endregion
}

public class PlayerLevelDB
{
	#region Enums
	private enum PlayerLevelDBIndex : int
	{
		ExpID = 0
		, Level	
		, Exp
	}
	#endregion

	#region Private Properties
	private static List<PlayerLevelDBData> _pleyerLevelDBList = null;   //List<DeckDBData> _deckDBList = null;
	//private static Dictionary<string, PlayerLevelDBData> _pleyerLevelDBList = null;
	private static bool _isLoadDB = false;
	#endregion

	#region Constructors
//	static LevelDB()
//	{
//	}
	#endregion

	#region Methods

	public static bool LoadDB()
	{
		TextAsset csvFile = Resources.Load<TextAsset>("Databases/SoloDB - PlayerExp");
		if(csvFile != null)
		{
			List<List<string>> rawDBList;
			bool isSuccess = CSVReader.ReadCSV(csvFile, out rawDBList);
			if (isSuccess)
			{
				_pleyerLevelDBList = new List <PlayerLevelDBData>();
				//_pleyerLevelDBList = new Dictionary<string, PlayerLevelDBData>();

				foreach(List<string> record in rawDBList)
				{
					PlayerLevelDBData data = new PlayerLevelDBData(
						record[(int)PlayerLevelDBIndex.ExpID]
						, int.Parse(record[(int)PlayerLevelDBIndex.Level])
						, int.Parse(record[(int)PlayerLevelDBIndex.Exp])
					);

					//_pleyerLevelDBList.Add(data.ExpID,data);
					_pleyerLevelDBList.Add (data);
				}

				Resources.UnloadAsset(csvFile);
				_isLoadDB = true;
				return true;
			}
		}

		_isLoadDB = false;
		return false;
	}

//	public static bool GetData(string levelID, out PlayerLevelDB data)
//	{
//		if(!_isLoadDB)
//		{
//			LoadDB();
//		}
//
//		return (_pleyerLevelDBList.TryGetValue(levelID, out data));
//	}


	public static int GetLevel(int expCom)
	{
		int level = 0;

		if (!_isLoadDB) 
		{
			PlayerLevelDB.LoadDB ();
		}

		foreach (PlayerLevelDBData data in _pleyerLevelDBList) 
		{
			if (data.Exp <= expCom) {
				level = data.Level;
			} else 
			{
				break;
			}
		}
			
		return level; 
		 
	}

	public static int GetExpCom(int level)
	{
		int expCom = 0;

		if (!_isLoadDB) 
		{
			PlayerLevelDB.LoadDB ();
		}

		foreach (PlayerLevelDBData data in _pleyerLevelDBList) 
		{
			if (data.Level == level) 
			{
				expCom = data.Exp;
				break;
			}
		}

		return expCom; 

	}



//	public static bool GetData(int iExp , out List<PlayerLevelDB> data)
//	{
//		if(!_isLoadDB)
//		{
//			LoadDB();
//		}
//
//		data = new List<PlayerLevelDB>();
//		foreach (PlayerLevelDB rawData in _pleyerLevelDBList)
//		{
//			if ( rawData.Exp == iExp )
//			{
//				data.Add(rawData);
//			}
//		}
//
//		if (data.Count > 0) 
//		{
//			return true;
//		} 
//		else 
//		{
//			data = null;
//			return false;
//		}
//	}


//	public int getLevelByExp(int iExp , out PlayerLevelDB data)
//	{
//		int iLevel = 0;
//
//
//
//		return iLevel;
//	}


	public static void DebugPrintData()
	{
		StringBuilder sb = new StringBuilder();

		foreach (PlayerLevelDBData data in _pleyerLevelDBList)
		{
			sb.Append(data.GetDebugPrintText());
			sb.Append("\n\n");
		}
		Debug.Log(sb.ToString());
	}
	#endregion
}