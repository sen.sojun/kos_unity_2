﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class DeckNameDBData
{
	#region Public Properties
	public string DeckID;
	public string Name;
	#endregion

	#region Constructors
	public DeckNameDBData()
	{
		DeckID = "";
		Name = "";
	}

	public DeckNameDBData(string deckID, string name)
	{
		DeckID = deckID;
		Name = name;
	}

	public DeckNameDBData(DeckNameDBData deckNameDBData)
	{
		DeckID = deckNameDBData.DeckID;
		Name = deckNameDBData.Name;
	}
	#endregion

	#region Methods
	public string GetDebugPrintText()
	{
		string resultText;
		resultText = string.Format(
			"DeckID: {0}\nName: {1}"
			, DeckID
			, Name
		);

		return resultText;
	}

	public void DebugPrintData()
	{
		Debug.Log(GetDebugPrintText());
	}
	#endregion
}

public class DeckNameDB 
{
	#region Enums
	private enum DeckNameDBIndex : int
	{
		  DeckID = 0
		, Name
	}
	#endregion

	#region Private Properties
	private static Dictionary<string, DeckNameDBData> _deckNameDBList = null;
	private static bool _isLoadDB = false;
	#endregion
    
    #region Public Properties
    public static bool IsLoadDB { get {return _isLoadDB; } }
    #endregion

	#region Constructors
	static DeckNameDB()
	{
	}
	#endregion

	#region Methods
	public static bool LoadDB()
	{
		TextAsset csvFile = Resources.Load<TextAsset>("Databases/KOS-CardDB - DeckNameDB");

		if(csvFile != null)
		{
			List<List<string>> rawDBList;
			bool isSuccess = CSVReader.ReadCSV(csvFile, out rawDBList);
			if (isSuccess)
			{
				_deckNameDBList = new Dictionary<string, DeckNameDBData>();

				foreach(List<string> record in rawDBList)
				{
					DeckNameDBData data = new DeckNameDBData(
						  record[(int)DeckNameDBIndex.DeckID]
						, record[(int)DeckNameDBIndex.Name]
					);

					_deckNameDBList.Add(data.DeckID, data);
				}

				Resources.UnloadAsset(csvFile);
				_isLoadDB = true;
				return true;
			}
		}

		_isLoadDB = false;
		return false;
	}

	public static bool GetData(string deckID, out DeckNameDBData data)
	{
		if(!_isLoadDB)
		{
			LoadDB();
		}

		return (_deckNameDBList.TryGetValue(deckID, out data));
	}

	public static void DebugPrintData()
	{
        StringBuilder sb = new StringBuilder();

        foreach (KeyValuePair<string, DeckNameDBData> data in _deckNameDBList)
        {
            sb.Append(data.Value.GetDebugPrintText());
            sb.Append("\n\n");
        }
        Debug.Log(sb.ToString());
	}
	#endregion

}
