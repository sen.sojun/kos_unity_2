﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class NPCDBData
{
	#region Public Properties
	public string NPCID;
	public string Name_EN;
    public string Name_TH;
    public string ImageName;
	public string BattleImageName;
	public string DeckID;
	public string RewardCardID;
	public int BattleBGIndex;
	public int Difficulty;
    public string DeckStyle_EN;
	public string DeckStyle_TH;
    public string Description_EN;
    public string Description_TH;
	#endregion

	#region Constructors
	public NPCDBData()
	{
		NPCID = "";
		Name_EN = "";
        Name_TH = "";
        ImageName = "";
		BattleImageName = "";
		DeckID = "";
		RewardCardID = "";
		BattleBGIndex = 0;
		Difficulty = 0;
        DeckStyle_EN = "";
		DeckStyle_TH = "";
        Description_EN = "";
        Description_TH = "";
	}

	public NPCDBData(string npcID, string name_EN, string name_TH, string imageName, string battleImageName, string deckID, string rewardCardID, int battleBGIndex, int difficulty, string deckStyle_EN, string deckStyle_TH, string descriptionEN, string descriptionTH)
	{
		NPCID = npcID;
		Name_EN = name_EN;
        Name_TH = name_TH;
        ImageName = imageName;
		BattleImageName = battleImageName;
		DeckID = deckID;
		RewardCardID = rewardCardID;
		BattleBGIndex = battleBGIndex;
		Difficulty = difficulty;
        DeckStyle_EN = deckStyle_EN;
		DeckStyle_TH = deckStyle_TH;
        Description_EN = descriptionEN;
        Description_TH = descriptionTH;
	}

	public NPCDBData(NPCDBData npcDBData)
	{
		NPCID = npcDBData.NPCID;
		Name_EN = npcDBData.Name_EN;
        Name_TH = npcDBData.Name_TH;
        ImageName = npcDBData.ImageName;
		BattleImageName = npcDBData.BattleImageName;
		DeckID = npcDBData.DeckID;
		RewardCardID = npcDBData.RewardCardID;
		BattleBGIndex = npcDBData.BattleBGIndex;
		Difficulty = npcDBData.Difficulty;
        DeckStyle_EN = npcDBData.DeckStyle_EN;
		DeckStyle_TH = npcDBData.DeckStyle_TH;
        Description_EN = npcDBData.Description_EN;
        Description_TH = npcDBData.Description_TH;
	}
	#endregion

	#region Methods
	public string GetDebugPrintText()
	{
		string resultText;
		resultText = string.Format(
			"NPCID: {0}\nName English: {1}\nName Thai: {2}\nImageName: {3}\nBattleImageName: {4}\nDeckID: {5}\nReward Card ID: {6}\nBattle Background Index: {7}\nDifficulty: {8}\nDeck Style EN: {9}\nDeck Style TH: {10}\nDescription English: {11}\nDescription Thai: {12}"
            , NPCID
			, Name_EN
            , Name_TH
            , ImageName
			, BattleImageName
			, DeckID
			, RewardCardID
			, BattleBGIndex
			, Difficulty.ToString()
            , DeckStyle_EN
			, DeckStyle_TH
            , Description_EN
            , Description_TH
		);

		return resultText;
	}

	public void DebugPrintData()
	{
		Debug.Log(GetDebugPrintText());
	}
	#endregion
}

public class NPCDB 
{
	#region Enums
	private enum NPCDBIndex : int
	{
		  NPCID = 0
		, Name_EN
        , Name_TH
        , ImageName
		, BattleImageName
		, DeckID
		, RewardCardID
		, BattleBGIndex
		, Difficulty
        , DeckStyle_EN
		, DeckStyle_TH
		, Description_EN
        , Description_TH
	}
	#endregion

	#region Private Properties
	private static Dictionary<string, NPCDBData> _npcDBList = null;
	private static bool _isLoadDB = false;
	#endregion
    
    #region Public Properties
    public static bool IsLoadDB { get {return _isLoadDB; } }
    #endregion

	#region Constructors
	static NPCDB()
	{
	}
	#endregion

	#region Methods
	public static bool LoadDB()
	{
		TextAsset csvFile = Resources.Load<TextAsset>("Databases/KOS-CardDB - NPCDB");
		if(csvFile != null)
		{
			List<List<string>> rawDBList;
			bool isSuccess = CSVReader.ReadCSV(csvFile, out rawDBList);
			if (isSuccess)
			{
				_npcDBList = new Dictionary<string, NPCDBData>();

				foreach(List<string> record in rawDBList)
				{
					NPCDBData data = new NPCDBData(
						  record[(int)NPCDBIndex.NPCID]
						, record[(int)NPCDBIndex.Name_EN]
                        , record[(int)NPCDBIndex.Name_TH]
                        , record[(int)NPCDBIndex.ImageName]
						, record[(int)NPCDBIndex.BattleImageName]
						, record[(int)NPCDBIndex.DeckID]
						, record[(int)NPCDBIndex.RewardCardID]
						, int.Parse(record[(int)NPCDBIndex.BattleBGIndex])
						, int.Parse(record[(int)NPCDBIndex.Difficulty])
                        , record[(int)NPCDBIndex.DeckStyle_EN]
						, record[(int)NPCDBIndex.DeckStyle_TH]
                        , record[(int)NPCDBIndex.Description_EN]
						, record[(int)NPCDBIndex.Description_TH]
					);

					_npcDBList.Add(data.NPCID, data);
				}

				Resources.UnloadAsset(csvFile);
				_isLoadDB = true;
				return true;
			}
		}

		_isLoadDB = false;
		return false;
	}

	public static bool GetData(string npcID, out NPCDBData data)
	{
		if(!_isLoadDB)
		{
			LoadDB();
		}

		return (_npcDBList.TryGetValue(npcID, out data));
	}

	public static void GetAllData(out List<NPCDBData> dataList)
	{
		if(!_isLoadDB)
		{
			LoadDB();
		}

		dataList = new List<NPCDBData>();

		foreach (KeyValuePair<string, NPCDBData> data in _npcDBList)
		{
			dataList.Add(data.Value);
		}
	}

	public static void DebugPrintData()
	{
        StringBuilder sb = new StringBuilder();

        foreach (KeyValuePair<string, NPCDBData> data in _npcDBList)
        {
            sb.Append(data.Value.GetDebugPrintText());
            sb.Append("\n\n");
        }
        Debug.Log(sb.ToString());
	}
	#endregion
}
