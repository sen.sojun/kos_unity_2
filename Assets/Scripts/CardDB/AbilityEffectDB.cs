﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class AbilityEffectDBData 
{
	#region Public Properties
	public string AbilityEffectID;
	public string EffectName;
	public int ParameterNum;
	public string Description_EN;
	public string Description_TH;
	#endregion

	#region Constructors
	public AbilityEffectDBData()
	{
		AbilityEffectID = "";
		EffectName = "";
		ParameterNum = 0;
		Description_EN = "";
		Description_TH = "";
	}

	public AbilityEffectDBData(string abilityEffectID, string effectName, int parameterNum, string description_EN, string description_TH)
	{
		AbilityEffectID = abilityEffectID;
		EffectName = effectName;
		ParameterNum = parameterNum;
		Description_EN = description_EN;
		Description_TH = description_TH;
	}

	public AbilityEffectDBData(AbilityEffectDBData abilityEffectDBData)
	{
		AbilityEffectID = abilityEffectDBData.AbilityEffectID;
		EffectName = abilityEffectDBData.EffectName;
		ParameterNum = abilityEffectDBData.ParameterNum;
		Description_EN = abilityEffectDBData.Description_EN;
		Description_TH = abilityEffectDBData.Description_TH;
	}
	#endregion

	#region Methods
	public string GetDebugPrintText()
	{
		string resultText;
		resultText = string.Format(
			"AbilityEffectID: {0}\nEffectName: {1}\nParameterNum: {2}\nDescription_EN: {3}\nDescription_TH: {4}"
			, AbilityEffectID
			, EffectName
			, ParameterNum
			, Description_EN
			, Description_TH
		);

		return resultText;
	}

	public void DebugPrintData()
	{
		Debug.Log(GetDebugPrintText());
	}
	#endregion
}

public class AbilityEffectDB
{
	#region Enums
	private enum AbilityEffectDBIndex : int
	{
		  AbilityEffectID = 0
		, EffectName
		, ParameterNum
		, Description_EN
		, Description_TH
	}
	#endregion

	#region Private Properties
	private static Dictionary<string, AbilityEffectDBData> _abilityEffectDBList = null;
	private static bool _isLoadDB = false;
	#endregion
    
    #region Public Properties
    public static bool IsLoadDB { get {return _isLoadDB; } }
    #endregion

	#region Methods
	public static bool LoadDB()
	{
		TextAsset csvFile = Resources.Load<TextAsset>("Databases/KOS-CardDB - AbilityEffectDB");

		if(csvFile != null)
		{
			List<List<string>> rawDBList;
			bool isSuccess = CSVReader.ReadCSV(csvFile, out rawDBList);
			if (isSuccess)
			{
				_abilityEffectDBList = new Dictionary<string, AbilityEffectDBData>();

				foreach(List<string> record in rawDBList)
				{
					AbilityEffectDBData data = new AbilityEffectDBData(
						  record[(int)AbilityEffectDBIndex.AbilityEffectID]
						, record[(int)AbilityEffectDBIndex.EffectName]
						, int.Parse(record[(int)AbilityEffectDBIndex.ParameterNum])
						, record[(int)AbilityEffectDBIndex.Description_EN]
						, record[(int)AbilityEffectDBIndex.Description_TH]
					);

					_abilityEffectDBList.Add(data.AbilityEffectID, data);
				}

				Resources.UnloadAsset(csvFile);
				_isLoadDB = true;
				return true;
			}
		}

		_isLoadDB = false;
		return false;
	}
		
	public static bool GetData(string abilityEffectID, out AbilityEffectDBData data)
	{
		if(!_isLoadDB)
		{
			LoadDB();
		}

		return (_abilityEffectDBList.TryGetValue(abilityEffectID, out data));
	}

	public static void DebugPrintData()
	{
        StringBuilder sb = new StringBuilder();

        foreach (KeyValuePair<string, AbilityEffectDBData> data in _abilityEffectDBList)
        {
            sb.Append(data.Value.GetDebugPrintText());
            sb.Append("\n\n");

			//Debug.Log(data.Value.GetDebugPrintText());
        }
        //Debug.Log(sb.ToString());
	}
	#endregion
}




