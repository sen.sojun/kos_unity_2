﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class CardClanDBData
{
    #region Public Properties
    public string CardClanID;
    public string ImageFile;
    public string ClanName_EN;
    public string ClanName_TH;
    #endregion

    #region Constructors
    public CardClanDBData()
    {
        CardClanID = "";
        ImageFile = "";
        ClanName_EN = "";
        ClanName_TH = "";
    }

    public CardClanDBData(string clanID, string imageFile, string clanName_EN, string clanName_TH)
    {
        CardClanID = clanID;
        ImageFile = imageFile;
        ClanName_EN = clanName_EN;
        ClanName_TH = clanName_TH;
    }
    #endregion

    #region Methods
    public string GetDebugPrintText()
    {
        string resultText;
        resultText = string.Format(
            "CardClanID: {0}\nImageFile: {1}\nClanName English: {2}\nClanName Thai: {3}"
            , CardClanID  
            , ImageFile
            , ClanName_EN
            , ClanName_TH
        );
        return (resultText);
    }

    public void DebugPrintData()
    {
        Debug.Log(GetDebugPrintText());
    }
    #endregion
}

public class CardClanDB
{
    #region Enums
    private enum CardClanDBIndex : int
    {
          CardClanID = 0  
        , ImageFile
        , ClanName_EN
        , ClanName_TH
    }
    #endregion

    #region Private Properties
    private static Dictionary<string, CardClanDBData> _cardClanDBList = null;
	private static bool _isLoadDB = false;
    #endregion
    
    #region Public Properties
    public static bool IsLoadDB { get {return _isLoadDB; } }
    #endregion

    #region Constructors
    static CardClanDB()
    {
    }
    #endregion

    #region Methods
    public static bool LoadDB()
    {
		TextAsset csvFile = Resources.Load<TextAsset>("Databases/KOS-CardDB - CardClanDB");

		if(csvFile != null)
		{
	        List<List<string>> rawDBList;
	        bool isSuccess = CSVReader.ReadCSV(csvFile, out rawDBList);
	        if (isSuccess)
	        {
	            _cardClanDBList = new Dictionary<string, CardClanDBData>();

	            foreach (List<string> record in rawDBList)
	            {
	                CardClanDBData data = new CardClanDBData(
	                      record[(int)CardClanDBIndex.CardClanID]
	                    , record[(int)CardClanDBIndex.ImageFile]
                        , record[(int)CardClanDBIndex.ClanName_EN]
                        , record[(int)CardClanDBIndex.ClanName_TH]
                    );
	                _cardClanDBList.Add(data.CardClanID, data);
	            }

				Resources.UnloadAsset(csvFile);
				_isLoadDB = true;
	            return true;
	        }
		}

		_isLoadDB = false;
        return false;
    }

    public static bool GetData(string cardClanID, out CardClanDBData data)
    {
		if(!_isLoadDB)
		{
			LoadDB();
		}

        return (_cardClanDBList.TryGetValue(cardClanID, out data));
    }

    public static void DebugPrintData()
    {
        StringBuilder sb = new StringBuilder();

        foreach (KeyValuePair<string, CardClanDBData> data in _cardClanDBList)
        {
            sb.Append(data.Value.GetDebugPrintText());
            sb.Append("\n\n");
        }
        Debug.Log(sb.ToString());
    }
    #endregion
}
