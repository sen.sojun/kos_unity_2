﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class Ability2DBData
{
    #region Public Properties
    public string AbilityID;

    public string Description_EN;
    public string Description_TH;

    public int ActionZone;
    public int ActivePhase;

    public string Type;
    public string Trigger;
    public string TargetType;
    public string Target;
    public string Cost;
    public string Condition;
    public string Effect;
    public string Benefit;
    #endregion

    #region Constructors
    public Ability2DBData()
    {
        AbilityID = "";

        ActionZone = 0;
        ActivePhase = 0;

        Type = "";
        Trigger = "";
        TargetType = "";
        Target = "";
        Cost = "";
        Condition = "";
        Effect = "";
        Benefit = "";

        Description_EN = "";
        Description_TH = "";
    }

    public Ability2DBData(
          string abilityID
        , int actionZone
        , int activePhase
        , string type
        , string trigger
        , string targetType
        , string target
        , string cost
        , string condition
        , string effect
        , string benefit
        , string description_EN
        , string description_TH
    )
    {
        AbilityID = abilityID;

        ActionZone = actionZone;
        ActivePhase = activePhase;

        Type = type;
        Trigger = trigger;
        TargetType = targetType;
        Target = target;
        Cost = cost;
        Condition = condition;
        Effect = effect;
        Benefit = benefit;

        Description_EN = description_EN;
        Description_TH = description_TH;
    }
    #endregion

    #region Methods
    public string GetDebugPrintText()
    {
        string resultText;
        resultText = string.Format(
              "AbilityID: {0}\n"
            + "ActionZone: {1}\n"
            + "ActivePhase: {2}\n"
            + "Type: {3}\n"
            + "Trigger: {4}\n"  
            + "TargetType: {5}\n"
            + "Target: {6}\n"
            + "Cost: {7}\n"
            + "Condition: {8}\n"
            + "Effect: {9}\n"
            + "Benefit: {10}\n"
            + "Description EN:\n{11}\nDescription TH:\n{12}"
            , AbilityID
            , ActionZone
            , ActivePhase
            , Type
            , Trigger
            , TargetType
            , Target
            , Cost
            , Condition
            , Effect
            , Benefit
            , Description_EN
            , Description_TH
        );

        return resultText;
    }

    public void DebugPrintData()
    {
        Debug.Log(GetDebugPrintText());
    }
    #endregion
}
    
public class Ability2DB
{
    #region Enums
    private enum Ability2DBIndex : int
    {
          AbilityID         = 0
        , Description_EN
        , Description_TH

        , ActionZone_RemoveZone      
        , ActionZone_Graveyard
        , ActionZone_Enemy
        , ActionZone_Battlefield
        , ActionZone_Player
        , ActionZone_Hand
        , ActionZone_Deck

        , ActivePhase_Owner_Begin
        , ActivePhase_Owner_PostBegin
        , ActivePhase_Owner_Draw
        , ActivePhase_Owner_PreBattle
        , ActivePhase_Owner_Battle
        , ActivePhase_Owner_PostBattle
        , ActivePhase_Owner_End
        , ActivePhase_Owner_ClearUp

        , ActivePhase_Other_Begin
        , ActivePhase_Other_PostBegin
        , ActivePhase_Other_Draw
        , ActivePhase_Other_PreBattle
        , ActivePhase_Other_Battle
        , ActivePhase_Other_PostBattle
        , ActivePhase_Other_End
        , ActivePhase_Other_ClearUp

        , Type
        , Trigger
        , TargetType
        , Target
        , Cost
        , Condition
        , Effect
        , Benefit
    }
    #endregion

    #region Private Properties
    private static Dictionary<string, Ability2DBData> _abilityDBList = null;
    private static bool _isLoadDB = false;
    #endregion
    
    #region Public Properties
    public static bool IsLoadDB { get {return _isLoadDB; } }
    #endregion

    #region Methods
    public static bool LoadDB()
    {
        TextAsset csvFile = Resources.Load<TextAsset>("Databases/KOS-CardDB - Ability2DB");

        if(csvFile != null)
        {
            List<List<string>> rawDBList;
            bool isSuccess = CSVReader.ReadCSV(csvFile, out rawDBList);
            if (isSuccess)
            {
                _abilityDBList = new Dictionary<string, Ability2DBData>();

                foreach(List<string> record in rawDBList)
                {
                    // Active Zone
                    int activeZone = 0;
                    for(Ability2DBIndex index = Ability2DBIndex.ActionZone_RemoveZone; index <=  Ability2DBIndex.ActionZone_Deck; ++index)
                    {
                        activeZone = activeZone << 1;

                        int value = int.Parse(record[(int)index]);
                        if (value == 1)
                        {
                            activeZone += 1; 
                        }
                    }

                    // Active Phave
                    int activePhase = 0;
                    for(Ability2DBIndex index = Ability2DBIndex.ActivePhase_Owner_Begin; index <=  Ability2DBIndex.ActivePhase_Other_ClearUp; ++index)
                    {
                        activePhase = activePhase << 1;

                        int value = int.Parse(record[(int)index]);
                        if (value == 1)
                        {
                            activePhase += 1; 
                        }
                    }
                        
                    Ability2DBData data = new Ability2DBData(
                          record[(int)Ability2DBIndex.AbilityID]
                        , activeZone
                        , activePhase
                        , record[(int)Ability2DBIndex.Type]
                        , record[(int)Ability2DBIndex.Trigger]
                        , record[(int)Ability2DBIndex.TargetType]
                        , record[(int)Ability2DBIndex.Target]
                        , record[(int)Ability2DBIndex.Cost]
                        , record[(int)Ability2DBIndex.Condition]
                        , record[(int)Ability2DBIndex.Effect]
                        , record[(int)Ability2DBIndex.Benefit]
                        , record[(int)Ability2DBIndex.Description_EN]
                        , record[(int)Ability2DBIndex.Description_TH]
                    );

                    _abilityDBList.Add(data.AbilityID, data);

                    //Debug.Log(data.GetDebugPrintText());
                }

                Resources.UnloadAsset(csvFile);
                _isLoadDB = true;
                return true;
            }
        }

        _isLoadDB = false;
        return false;
    }

    public static bool GetData(string abilityID, out Ability2DBData data)
    {
        if(!_isLoadDB)
        {
            LoadDB();
        }

        return (_abilityDBList.TryGetValue(abilityID, out data));
    }

    public static void DebugPrintData()
    {
        StringBuilder sb = new StringBuilder();

        foreach (KeyValuePair<string, Ability2DBData> data in _abilityDBList)
        {
            sb.Append(data.Value.GetDebugPrintText());
            sb.Append("\n\n");
        }
        Debug.Log(sb.ToString());
    }
    #endregion
}
