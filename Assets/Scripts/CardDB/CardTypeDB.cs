﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class CardTypeDBData
{
    #region Public Properties
    public string CardTypeID;
    public string TypeName_EN;
    public string TypeName_TH;
    #endregion

    #region Constructors
    public CardTypeDBData()
    {
        CardTypeID = "";
        TypeName_EN = "";
        TypeName_TH = "";
    }

    public CardTypeDBData(string cardTypeID, string typeName_EN, string typeName_TH)
    {
        CardTypeID = cardTypeID;
        TypeName_EN = typeName_EN;
        TypeName_TH = typeName_TH;
    }
    #endregion

    #region Methods
    public string GetDebugPrintText()
    {
        string resultText;
        resultText = string.Format(
            "CardTypeID: {0}\nTypeName English: {1}\nTypeName Thai: {2}"
            , CardTypeID
            , TypeName_EN
            , TypeName_TH
        );
        return (resultText);
    }

    public void DebugPrintData()
    {
        Debug.Log(GetDebugPrintText());
    }
    #endregion
}

public class CardTypeDB 
{
    #region Enums
    private enum CardTypeDBIndex : int
    {
          CardTypeID = 0
        , TypeName_EN
        , TypeName_TH
    }
    #endregion

    #region Private Properties
    private static Dictionary<string, CardTypeDBData> _cardTypeDBList = null;
	private static bool _isLoadDB = false;
    #endregion
    
    #region Public Properties
    public static bool IsLoadDB { get {return _isLoadDB; } }
    #endregion

    #region Constructors
    static CardTypeDB()
    {
    }
    #endregion

    #region Methods
    public static bool LoadDB()
    {
		TextAsset csvFile = Resources.Load<TextAsset>("Databases/KOS-CardDB - CardTypeDB");

		if(csvFile != null)
		{
	        List<List<string>> rawDBList;
	        bool isSuccess = CSVReader.ReadCSV(csvFile, out rawDBList);
	        if (isSuccess)
	        {
	            _cardTypeDBList = new Dictionary<string, CardTypeDBData>();

	            foreach (List<string> record in rawDBList)
	            {
	                CardTypeDBData data = new CardTypeDBData(
	                      record[(int)CardTypeDBIndex.CardTypeID]
	                    , record[(int)CardTypeDBIndex.TypeName_EN]
                        , record[(int)CardTypeDBIndex.TypeName_TH]
                    );
	                _cardTypeDBList.Add(data.CardTypeID, data);
	            }

				Resources.UnloadAsset(csvFile);
				_isLoadDB = true;
	            return true;
	        }
		}

		_isLoadDB = false;
        return false;
    }

    public static bool GetData(string cardTypeID, out CardTypeDBData data)
    {
		if(!_isLoadDB)
		{
			LoadDB();
		}

        return (_cardTypeDBList.TryGetValue(cardTypeID, out data));
    }

    public static void DebugPrintData()
    {
        StringBuilder sb = new StringBuilder();

        foreach (KeyValuePair<string, CardTypeDBData> data in _cardTypeDBList)
        {
            sb.Append(data.Value.GetDebugPrintText());
            sb.Append("\n\n");
        }
        Debug.Log(sb.ToString());
    }
    #endregion
}
