﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class CSVReader
{
    public static bool ReadCSV(TextAsset csvFile, out List<List<string>> result)
    {
        List<List<string>> data = new List<List<string>>();

        if (csvFile != null)
        {
            string[] records = SplitCSVRow(csvFile.text);

            /*
            // Print Debug
            string debugText = "";
            debugText = "CSVReader[" + records.Length.ToString() + "]:";
            int i = 0;
            foreach (string s in records)
            {
                debugText += "\n["+ i.ToString() + "]:" + s;
                ++i;
            }
            Debug.Log(debugText);
            */

            for (int index = 1; index < records.Length; ++index)
            {
                List<string> row = new List<string>();
                string[] fields = SplitCSVColumn(records[index]);
                foreach (string field in fields)
                {
                    row.Add(field);
                }
                data.Add(row);
            }

            result = data;
            return true;
        }

        result = null;
        return false;
    }

    // split CSV row 
    private static string[] SplitCSVRow(string text)
    {
        List<string> result = new List<string>();
        StringBuilder sbRowText = new StringBuilder();
        bool isFoundQuote = false;

        for (int index = 0; index < text.Length; ++index)
        {
            char c = text[index];
            if (!isFoundQuote)
            {
                if (c == '\"')
                {
                    // Start Qoute
                    isFoundQuote = true;
                }
                else if (c == '\r')
                {
                    // Ignore \r
                    continue;
                }
                else if (c == '\n')
                {
                    // Split line
                    result.Add(sbRowText.ToString());
                    ClearStringBuilder(sbRowText);
                    continue;
                }
            }
            else
            {
                if (c == '\"')
                {
                    // End Qoute
                    isFoundQuote = false;
                }
                /*
                else if(c == '\r')
                {
                    // Ignore \r
                    continue;
                }
                */
            }

            //rowText += c;
            sbRowText.Append(c);

            if (index + 1 >= text.Length && sbRowText.Length > 0)
            {
                result.Add(sbRowText.ToString());
                ClearStringBuilder(sbRowText);
                continue;
            }
        }

        /*
        // Print Debug
        string debugText = "";
        for (int i = 0; i < result.Count; ++i)
        {
            debugText += "[" + result[i] + "]";
        }
         //Debug.Log(text +"\n" + debugText);
        */

        return (result.ToArray());
    }

    // split CSV column 
    private static string[] SplitCSVColumn(string text)
    {
        List<string> result = new List<string>();
        StringBuilder sbColumnText = new StringBuilder();
        //string columnText = "";
        bool isFoundQuote = false;

        for (int index = 0; index < text.Length; ++index)
        {
            char c = text[index];
            if (!isFoundQuote)
            {
                if (c == ',')
                {
                    // Split column
                    result.Add(sbColumnText.ToString());
                    ClearStringBuilder(sbColumnText);
                    continue;
                }
                else if (c == '\"')
                {
                    isFoundQuote = true;
                    continue;

                }
                else if (c == '\n' && index == 0)
                {
                    // remove first \n
                    continue;
                }
            }
            else
            {
                if (c == '\"')
                {
                    // end quote
                    isFoundQuote = false;

                    if (index + 1 >= text.Length && sbColumnText.Length > 0)
                    {
                        // last character
                        result.Add(sbColumnText.ToString());
                        ClearStringBuilder(sbColumnText);
                        continue;
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            sbColumnText.Append(c);
            // //Debug.Log(columnText);

            if (index + 1 >= text.Length && sbColumnText.Length > 0)
            {
                // last character
                result.Add(sbColumnText.ToString());
                ClearStringBuilder(sbColumnText);
                continue;
            }
        }

        /*
        // Print Debug
        string debugText = "";
        for (int i = 0; i < result.Count; ++i)
        {
            debugText += "[" + result[i] + "]";
        }
         //Debug.Log("Column:" +"\n" + debugText);
		*/

        return (result.ToArray());
    }

    public static bool ReadAbilityText(string text, out CardAbility.AbilityParams abParams)
    {
        string abilityKey;
        List<string> paramsList;

        bool isSuccess = ReadAbilityText(text, out abilityKey, out paramsList);
        if (isSuccess)
        {
            abParams = new CardAbility.AbilityParams(abilityKey, paramsList);
        }
        else
        {
            abParams = null;
        }

        return isSuccess;
    }

    public static bool ReadAbilityText(string text, out string abilityKey, out List<string> paramsList)
    {
        abilityKey = "";
        paramsList = new List<string>();

        bool isFoundKey = false;
        StringBuilder strText = new StringBuilder();

        // find key
        for (int index = 0; index < text.Length; ++index)
        {
            char c = text[index];

            if (!isFoundKey)
            {
                // Find Key
                if (c == '(')
                {
                    abilityKey = strText.ToString();
                    ClearStringBuilder(strText);
                }
                else if (index + 1 >= text.Length)
                {
                    // This is last character
                    strText.Append(c);
                    abilityKey = strText.ToString();
                }
                else
                {
                    strText.Append(c);
                }
                continue;
            }
            else
            {
                ClearStringBuilder(strText);
                strText.Append(text.Substring(index, text.Length - index - 1));

                break;
            }
        }

        // split param
        isFoundKey = false;
        int blanketCount = 0;
        string paramStr = strText.ToString();
        for (int index = 0; index < paramStr.Length; ++index)
        {
            char c = text[index];

            if (blanketCount > 0)
            {
                if (c == '(')
                {
                    ++blanketCount;
                }
                else if (c == ')')
                {
                    --blanketCount;
                }

                strText.Append(c);
                continue;
            }
            else
            {
                if (c == '(')
                {
                    ++blanketCount;
                    strText.Append(c);
                    continue;
                }
                else if (c == ')')
                {
                    --blanketCount;
                    strText.Append(c);
                    continue;
                }
                else if (c == ',')
                {
                    // split param
                    paramsList.Add(strText.ToString());
                    ClearStringBuilder(strText);
                    continue;
                }
            }
        }
            
        if (isFoundKey)
        {
            return true;
        }
            
        abilityKey = "";
        paramsList = new List<string>();
        return false;
    }

    public static bool ReadAbilityText(string text, out List<CardAbility.AbilityParams> abParamsList)
    {
        abParamsList = new List<CardAbility.AbilityParams>();

        List<string> paramsTextList = new List<string>();
        StringBuilder strText = new StringBuilder();
        int blanketCount = 0;
        int prevCutIndex = 0;

        // find comma
        for (int index = 0; index < text.Length; ++index)
        {
            char c = text[index];

            if (c == '(')
            {
                ++blanketCount;
            }
            else if (c == ')')
            {
                if(blanketCount > 0)
                {
                    --blanketCount;
                }
            }
            else if(c == ',')
            {
                if(blanketCount <= 0)
                {
                    // cut string.
                    paramsTextList.Add(text.Substring(prevCutIndex, index - prevCutIndex));
                    prevCutIndex = index + 1;
                    continue;
                }
            }
        }

        if (prevCutIndex < text.Length)
        {
            paramsTextList.Add(text.Substring(prevCutIndex, text.Length - prevCutIndex));
        }

        foreach(string paramText in paramsTextList)
        {
            CardAbility.AbilityParams abParam;
            bool isSuccess = ReadAbilityText(paramText, out abParam);
            if (isSuccess)
            {
                abParamsList.Add(abParam);
            }
        }

        return false;
    }
        
    private static void ClearStringBuilder(StringBuilder value)
    {
        value.Length = 0;
        value.Capacity = 0;
    }
}
