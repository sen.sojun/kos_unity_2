﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialMenuUINew : MonoBehaviour {

    public TutorialDetailUIMew tutorialDetailUINew;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public void ShowUI()
    {
        TopBarManager.Instance.SetUpBar(
             true
            , Localization.Get("TUTORIAL_MENU_HEADER") //"Tutorial Menu"
           , MenuManagerV2.Instance.ShowSettingUI);

        gameObject.SetActive(true);
    }

    public void HideUI(bool isHideDetail = true)
    {
        gameObject.SetActive(false);
        if (isHideDetail) tutorialDetailUINew.HideUI();
    }

    public void ShowTutorialDetail(string codelist)
    {
        List<string> tutorialCodeList = new List<string>();
        string[] tokenstutorialCodeList = codelist.Split(new string[] { "," },System.StringSplitOptions.None);
        foreach (string str in tokenstutorialCodeList)
        {
            tutorialCodeList.Add(str);
        }
        if (tutorialCodeList.Count > 0)
        {
            ShowTutorialDetail(tutorialCodeList);
        }
    }


    public void ShowTutorialDetail(List<string> tutorialCodeList)
    {
        List<TutorialDBData> dataList = new List<TutorialDBData>();

        bool isFound = true;
        foreach (string tutorialCode in tutorialCodeList)
        {
            List<TutorialDBData> list = null;
            isFound |= TutorialDB.GetDataByCode(tutorialCode, out list);
            if (isFound)
            {
                dataList.AddRange(list);
            }
        }

        if (isFound)
        {
            string title = "";
            string message = "";

            if (LocalizationManager.IsCurrentLanguage(LocalizationManager.Language.Thai))
            {
                title = dataList[0].TitleTh;
            }
            else
            {
                title = dataList[0].TitleEng;
            }

            List<Texture> imageList = new List<Texture>();
            List<string> descriptions = new List<string>();

            foreach (TutorialDBData data in dataList)
            {
                Texture imageTexture = Resources.Load(data.ImagePath, typeof(Texture)) as Texture;

                if (imageTexture != null)
                {
                    imageList.Add(imageTexture);
                    Resources.UnloadUnusedAssets();
                    //Resources.UnloadAsset(imageTexture);
                }

                if (LocalizationManager.IsCurrentLanguage(LocalizationManager.Language.Thai))
                {
                    descriptions.Add(data.TextTh);
                }
                else
                {
                    descriptions.Add(data.TextEng);
                }
            }

            this.HideUI(false);
            tutorialDetailUINew.ShowUI(title, imageList, descriptions, this.ShowUI);
        }
    }


}
