﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CardPopupUI : MonoBehaviour 
{
    public delegate void OnButtonClick();
    //private List<OnButtonClick> _buttonCallbackList;

    public GameObject ButtonPrefab;
    public GameObject ButtonParent;

    public Vector2 ButtonSize;
    public float ButtonOffset = 10.0f;

    public int createButton = 3;

    private List<GameObject> _buttonList;

    private bool _isStart = false;
    private bool isCreate = false;

	// Use this for initialization
	private void Start () 
    {
        _buttonList = new List<GameObject>();

        _isStart = true;
	}
	
	// Update is called once per frame
	private void Update () 
    {
        if (_isStart && !isCreate)
        {
            //Debug.Log(GetComponent<UIWidget>().panel.name);

            for (int index = 0; index < createButton; ++index)
            {
                AddButton(index.ToString(), null);
            }
            isCreate = true;
        }
	}

    public void AddButton(string buttonText, OnButtonClick callback)
    {
        if (!_isStart) return;

        NGUITools.NormalizeDepths();

        GameObject buttonObj = NGUITools.AddChild(ButtonParent, ButtonPrefab);

        //Debug.Log("ADD BUTTON!!!");

        // assign parent panel
        UIWidget btnWidget = buttonObj.GetComponent<UIWidget>();
        btnWidget.panel = GetComponent<UIWidget>().panel;

        UIWidget[] widgets = buttonObj.GetComponentsInChildren<UIWidget>(true);
        for (int i = 0; i < widgets.Length; ++i)
        {
            widgets[i].panel = GetComponent<UIWidget>().panel;
        }

        //print("Panel: " + btnWidget.panel.name);

        //print("Before Button Depth: " + buttonObj.GetComponent<UIWidget>().depth.ToString());
        NGUITools.BringForward(buttonObj);
        //print("After Button Depth: " + buttonObj.GetComponent<UIWidget>().depth.ToString());      

        // Set Button Label
        buttonObj.GetComponent<CardPopupButton>().SetButtonLabel(buttonText);

        _buttonList.Add(buttonObj);
        Reposition();
    }

    public void Reposition()
    {
        for (int index = 0; index < _buttonList.Count; ++index)
        {
            _buttonList[index].transform.localPosition = new Vector3(0.0f, -((index * ButtonSize.y) + (index * ButtonOffset)), 0.0f);
        }
    }
}
