﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class AnnouncementUINew : MonoBehaviour {


    public Image ImageAnnouncement;
    public Text TextTopic;
    public Text TextID;
    public Text TextTitle;
    public Text TextAnnouncement;
    public Text TextButton;

    public static bool IsShowAnnouncement = true;

    private List<AnnouncementDBData> _announcementDataList = null;
    private int _currentIndex = 0;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void Init()
    {

        bool isSuccess = AnnouncementDB.GetAllData(out _announcementDataList);
        if (isSuccess)
        {
            ShowCurrentData();
        }
        else
        {
            Debug.LogError("AnnouncementScript/Init: AnnouncementDB.GetAllData Failed!!");
        }
    }

    public void ShowCurrentData()
    {
        ShowData(_currentIndex);
    }

    public void ShowAnnouncementUI()
    {
        //Load announcement data from csv file
        Init();
        IsShowAnnouncement = false;
        gameObject.SetActive(true);
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }

    public void ShowData(int index)
    {
        try
        {


            if (_announcementDataList != null && _announcementDataList.Count > index)
            {
                AnnouncementDBData announcementData = _announcementDataList[index];
                //tutorialData.DebugPrintData();

                if (announcementData.IsActive == 1)
                {
                    //PopupBoxUI.ShowMessageUI(tutorialData.TextEng,tutorialData.TutorialButtonText);
                    Texture imageTexture = Resources.Load(announcementData.ImageID, typeof(Texture)) as Texture;
                    if (imageTexture != null)
                    {
                        Texture2D texture2D = imageTexture as Texture2D;
                        Sprite mySprite = Sprite.Create(texture2D, new Rect(0.0f, 0.0f, texture2D.width, texture2D.height), new Vector2(0.5f, 0.5f), 100.0f);

                        ImageAnnouncement.sprite = mySprite;

                    }
                    else
                    {
                        Debug.LogError(
                            string.Format(
                                  "Announcement Image not found! ({0}"
                                , announcementData.ImageID
                            )
                        );
                    }
                    TextTopic.text = announcementData.Title;
                    TextID.text = announcementData.Code;
                    TextTitle.text = announcementData.Date;
                  //  TextButton.GetComponent<UILocalize>().key = announcementData.ButtonTextKey;


                   
                    string announcementText = "";
                    switch (LocalizationManager.CurrentLanguage)
                    {
                        case LocalizationManager.Language.Thai:
                            {
                                announcementText = announcementData.TextTh;
                            }
                            break;

                        case LocalizationManager.Language.English:
                        default:
                            {
                                announcementText = announcementData.TextEng;
                            }
                            break;
                    }
                    Debug.Log(announcementData.TextTh + " " + announcementData.TextEng);
                    announcementText = announcementData.TextEng;
                    TextAnnouncement.text = "test" + announcementText;
                }

            }
        }
        catch (System.Exception e)
        {
            Debug.Log("Error Open Announcement " + e.ToString());
        }
    }


}
