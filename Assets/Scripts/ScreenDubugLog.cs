﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class ScreenDubugLog : MonoBehaviour
{
    #if UNITY_EDITOR
    private static readonly bool _isShowLog = true;
    #else
    private static readonly bool _isShowLog = false;
    #endif

    private static ScreenDubugLog _instance = null;
    public static ScreenDubugLog Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<ScreenDubugLog>();

                if (_instance == null)
                {

                    GameObject go = new GameObject();
                    go.name = "ScreenDubugLog";
                    _instance = go.AddComponent<ScreenDubugLog>();

                    DontDestroyOnLoad(go);
                }
            }

            return _instance;
        }
    }

    private int _maxCount = 20;
    private List<string> _texts;
    private StringBuilder _sb;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void Print(string text)
    {
        if(_isShowLog)
        {
            if (_texts == null) _texts = new List<string>();
    
            _texts.Add(text);
    
            if (_texts.Count > _maxCount)
            {
                _texts.RemoveAt(0);
            }
        }
        
        Debug.Log(text);
    }

    private void OnGUI()
    {
        if(_isShowLog)
        {
            if (_texts != null && _texts.Count > 0)
            {
                if (_sb == null) _sb = new StringBuilder();
                _sb.Length = 0;
                _sb.Capacity = 0;
    
                for (int index = _texts.Count - 1; index >= 0; --index)
                {
                    if (_sb.Length > 0)
                    {
                        _sb.Append("\n" + _texts[index]);
                    }
                    else
                    {
                        _sb.Append(_texts[index]);
                    }
                }
    
                GUI.Label(new Rect(10, 10, Screen.width - 10, Screen.height - 10), _sb.ToString());
            }
        }
    }
}
