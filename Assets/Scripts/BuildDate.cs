﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class BuildDate : MonoBehaviour 
{    
    void Start() 
    {        
        GetComponent<Text>().text = "KOS-" + PlayerSave.GetBuildVersion();
        //Debug.Log(BuildVersion.GetBuildTime());
    }
}
