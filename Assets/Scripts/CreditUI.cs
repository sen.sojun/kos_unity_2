﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditUI : MonoBehaviour {

	// Use this for initialization
	void Start () 
    {
		
	}
	
	// Update is called once per frame
	void Update () 
    {
		
	}

    public void ShowUI()
    {
        TopBarManager.Instance.SetUpBar(
            true
            , Localization.Get("CREDIT_HEADER") // "CREDITS"
            , MenuManagerV2.Instance.ShowSettingUI
        );
        gameObject.SetActive(true);
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }


}
