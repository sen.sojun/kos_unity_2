﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

[RequireComponent(typeof(UITexture))]
public class BlinkTexture : MonoBehaviour 
{
	public Color StartColor = Color.white;
	public Color EndColor = Color.white;
	public float BlinkTime = 1.0f;
	public bool PauseWhileDisabling = false;
	public Ease EaseType = Ease.Linear;

	private Tween _t = null;

	// Use this for initialization
	void Start () {
		//StartBlink ();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void OnEnable()
	{
		if (PauseWhileDisabling || _t == null) 
		{
			StartBlink ();
		} 
	}

	void OnDisable()
	{
		if (PauseWhileDisabling && _t != null) 
		{
			StopBlink ();
		} 
	}

	private void StartBlink()
	{   
		if (_t == null) 
		{
			UITexture texture = GetComponent<UITexture> ();
			texture.color = StartColor;

			_t = DOTween.To (
				() => texture.color
				, x => texture.color = x
				, EndColor
				, BlinkTime
			);

			_t.SetLoops (-1, LoopType.Yoyo);
			_t.SetEase (EaseType);
			_t.SetAutoKill (false);
			_t.Play ();
		} 
		else 
		{
			_t.Restart ();
		}
	}

	private void StopBlink()
	{  
		_t.Pause ();
	}
}
