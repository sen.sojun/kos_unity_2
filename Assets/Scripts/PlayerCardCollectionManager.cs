﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using GameAnalyticsSDK;
using UnityEngine.UI;

public class PlayerCardCollectionManager : MonoBehaviour 
{

    public int MaxCardPerPage = 10; //8;
    public AudioClip BGMClip;

    public List<FullCardUI2> FullCardList;
    public List<Image>       BackCardList;

    public Button NextButton;
    public Button PrevButton;

    public Text PageLabel;
    public Text PercentLabel;
    //public UILabel PercentLabel;
    //public BattleCardUI BattleCard;

    public GameObject ShowCardUIView;//added 30062016 vit
    public FullCardUI2 FullCard_L;    //added 30062016 vit
    //public LoadingUI LoadingUI;

    private float startTime;
    private float endTime;
    private float totalTime;

    private List<CardData> myCollection = new List<CardData>();

    //public UILabel IndexLabel;
    private List<CardDBData> _database;
    private Dictionary<string, CardData> _myCollection; //<LIST> -> array like ,with memory allocation
    private int _maxPageIndex = 0;

    private int __pageIndex = 0;
    private int _pageIndex
    {
        get { return __pageIndex; }
        set
        {
            __pageIndex = value;

            if (__pageIndex <= 0)
            {
                // first page
                PrevButton.gameObject.SetActive(false);
                Debug.Log("first page");
            }
            else
            {
                PrevButton.gameObject.SetActive(true);
                Debug.Log("Next page" + __pageIndex);
            }

            if (__pageIndex >= _maxPageIndex)
            {
                // last page
                NextButton.gameObject.SetActive(false);
            }
            else
            {
                NextButton.gameObject.SetActive(true);
            }

            ShowCurrentPage();
        }
    }

    public void ShowUI()
    {
        TopBarManager.Instance.SetUpBar(
            true
            , Localization.Get("COLLECTION_HEADER") //"CARD COLLECTION"
            , MenuManagerV2.Instance.ShowPlayerDeckCollectionUI
        );
        gameObject.SetActive(true);


        if (!SoundManager.IsPlayBGM(BGMClip))
        {
            SoundManager.PlayBGM(BGMClip);
        }

        //start counting time
        startTime = Time.time;

        //GameAnalytics.NewProgressionEvent(progressionStart,"Start Card Collection",30);
        //PlayerSave.GenerateDummySave (); //generate new dummy card for initial run

        //GameAnalytics.NewDesignEvent ("Achievement:StartCardCollection", 1);
        foreach (FullCardUI2 fullCardUI in FullCardList)
        {
            fullCardUI.BindOnClickCallback(this.OnClickedPreview);

        }


        LoadCollectionData();
        //LoadingUI.HideUI();

    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }

	// Use this for initialization
	void Start ()
    {
        
	}
	
    void LoadCollectionData()
    {
        //List<CardData> myCollection = new List<CardData>();
        bool isSuccess = PlayerSave.GetPlayerCardCollection(out myCollection);

        if (isSuccess)
        {
            _myCollection = new Dictionary<string, CardData>();
            foreach (CardData data in myCollection)
            {
                _myCollection.Add(data.CardID, data);
                //GameAnalytics.NewProgressionEvent(progressionStart,"Card Collection",30);

            }
        }
        else
        {
            Debug.LogError("CardCollectionSceneManager/LoadCollectionData: No save player collection!!");
            //return;
        }

        isSuccess = CardDB.GatAllData(out _database);
        if (isSuccess)
        {
            _maxPageIndex = Mathf.CeilToInt((float)_database.Count / (float)MaxCardPerPage) - 1;
            _pageIndex = 0;
        }
        else
        {
            Debug.LogError("CardCollectionSceneManager/LoadCollectionData: can't load CardDBData!!");
            return;
        }
    }

    void ReloadCollectionData()
    {
        bool isSuccess = CardDB.GatAllData(out _database);
        if (isSuccess)
        {
            _maxPageIndex = Mathf.CeilToInt((float)_database.Count / (float)MaxCardPerPage) - 1;
            _pageIndex = 0;
        }
        else
        {
            Debug.LogError("CardCollectionSceneManager/LoadCollectionData: can't load CardDBData!!");
            return;
        }
    }

    void ShowCurrentPage()
    {
        int startIndex = _pageIndex * MaxCardPerPage;
        int stopIndex = startIndex + (MaxCardPerPage - 1);

        for (int index = startIndex; index <= stopIndex; ++index)
        {
            CardData cardData;

            if (index < _database.Count)
            {
                if (_myCollection.TryGetValue(_database[index].CardID, out cardData))
                {
                    // Found data
                    FullCardList[index - startIndex].SetData(new PlayerCardData(cardData));

                    FullCardList[index - startIndex].gameObject.SetActive(true);
                    BackCardList[index - startIndex].gameObject.SetActive(false);
                }
                else
                {
                    // not found
                    FullCardList[index - startIndex].gameObject.SetActive(false);
                    BackCardList[index - startIndex].gameObject.SetActive(true);

                }
            }
            else
            {
                // last page
                FullCardList[index - startIndex].gameObject.SetActive(false);
                BackCardList[index - startIndex].gameObject.SetActive(true);

            }
        }

        PageLabel.text = (_pageIndex + 1).ToString() + "/" + (_maxPageIndex + 1).ToString();

        //PercentLabel.text = Localization.Get("COLLECTION_PROGESS") + " " + (((float)_myCollection.Count / (float)_database.Count) * 100.0f).ToString("0.0") + " %";
        PercentLabel.text = Localization.Get("COLLECTION_PROGESS") + " " + (((float)_myCollection.Count / (float)_database.Count) * 100.0f).ToString("0") + " %";
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log ("update card Tutorial");
        //GameAnalytics.NewDesignEvent ("Update Card Collection Event", 2.0f);

    }

    public void ShowNextPage()
    {
        //Debug.Log("Show Next Card");
        //GameAnalytics.NewDesignEvent ("Next Page");
        ++_pageIndex;
    }

    public void ShowPrevPage()
    {
        //Debug.Log("Show Prev Card");
        //GameAnalytics.NewDesignEvent ("Prev Page");
        --_pageIndex;
    }

    void OnClickedPreview(FullCardUI2 fullCardUI)
    {
        //Debug.Log ("Name:" + fullCardUI.PlayerCardData.Name_EN);
        //GameAnalytics.NewProgressionEvent(progressionStart,"Start Card Collection",30);
        //GameAnalytics.print ("From Analytics!");
        //GameAnalytics.NewDesignEvent ("PreviewCard");
        //GameAnalytics.SetGender (GAGender.Male);

        FullCard_L.SetData(fullCardUI.Data);
        ShowCardUIView.SetActive(true);

        //try remove card after click
        //Debug.Log("Count = " + _myCollection.Count + " Remove card At " + fullCardUI.Data.Name_EN + fullCardUI.Data.CardID);
        //_myCollection.Remove(fullCardUI.Data.CardID);
        //Debug.Log("after delete " + _myCollection.Count);
        //ShowCurrentPage();
        //ReloadCollectionData();

       // BattleUIManager.RequestBringForward(ShowCardUIView.gameObject);
    }

    public void BackToMenu()
    {
        //--start of insertion vit04102016
        endTime = Time.time;
        totalTime = (endTime - startTime) / 60;
        GameAnalytics.NewDesignEvent("Achievement:EndCardCollection", totalTime);


        //LoadingUI.ShowUI();

        //SceneManager.LoadSceneAsync("MainMenuScene");
        SceneManager.LoadSceneAsync("MainMenuScene2");
    }
}
