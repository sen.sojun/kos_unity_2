﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class RatingManager : MonoBehaviour
{
	public PopupMessageUI PopupMessageUI;

	// Use this for initialization
	void Start ()
	{

	}

	// Update is called once per frame
	/*
	void Update ()
	{
	}
	*/

	public void ShowRatingPopup()
	{
		PopupMessageUI.ShowUI2(
			  "RATING_TEXT"
			, "RATING_5_STARS"
			, "RATING_SUGGESTION"
			, this.GiveFiveStar
			, this.SendEmail
		);
	}

	public void SendEmail ()
	{
		string email = "hello@koscardgame.com";
		string subject = MyEscapeURL("Suggestion for KOS");
		string body = MyEscapeURL("");
		Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);
	}

	string MyEscapeURL (string url)
	{
		return WWW.EscapeURL(url).Replace("+","%20");
	}

	public void GiveFiveStar()
	{
		#if UNITY_EDITOR
		Application.OpenURL("https://www.facebook.com/koscardgameth/");
		#elif UNITY_ANDROID
        // market://details?id=<package_name>
        Application.OpenURL("market://details?id=com.lunarstudio.koscard");
		#elif UNITY_IOS
		Application.OpenURL("itms-apps://itunes.apple.com/app/id1166255327");
		#else
		Application.OpenURL("https://www.facebook.com/koscardgameth/");
		#endif

		//https://itunes.apple.com/th/app/id1166255327
	}
}
