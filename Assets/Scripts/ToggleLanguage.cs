﻿using UnityEngine;
using System.Collections;

public class ToggleLanguage : MonoBehaviour 
{
	// Use this for initialization
	void Start () 
	{
		if (!LocalizationManager.IsLoad)
		{
			LocalizationManager.Init ();
		}
	}

	/*
	// Update is called once per frame
	void Update () 
	{
	}
	*/

	public void OnToggleLanguage()
	{
		switch (LocalizationManager.CurrentLanguage)
		{
			case LocalizationManager.Language.Thai:
			{
				LocalizationManager.SetLanguage (LocalizationManager.Language.English);
			}
			break;

			case LocalizationManager.Language.English:
			default:
			{
				LocalizationManager.SetLanguage (LocalizationManager.Language.Thai);
			}
			break;
		}
	}
}
