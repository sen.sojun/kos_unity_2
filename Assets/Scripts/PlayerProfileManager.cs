﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerProfileManager : MonoBehaviour
{
    #region Private
    private string __currentPlayerName = "";
    private string __currentPlayerEmail = "";
    private string __currentPlayerPassword = "";

    public enum ProfileField
    {
        playerName,
        playerEmail,
        playerPassword
    }

    private string _currentPlayerName
    {
        get { return __currentPlayerName; }
        set
        {
            __currentPlayerName = value;

           // UpdatePlayerProfile(ProfileField.playerName);
        }
    }

    private string _currentPlayerEmail
    {
        get { return __currentPlayerEmail; }
        set
        {
            __currentPlayerEmail = value;

          //  UpdatePlayerProfile(ProfileField.playerEmail);
        }
    }

    private string _currentPlayerPassword
    {
        get { return __currentPlayerPassword; }
        set
        {
            __currentPlayerPassword = value;

          //  UpdatePlayerProfile(ProfileField.playerPassword);
        }
    }
    #endregion

    #region Public Inspector Properties
    public Text txtName;
    public Text txtEmail;
    public Text txtPassword;
    public InputField InputName;
    //public InputField InputEmail;
    public InputField InputCurrPassword;
    public InputField InputNewPassword;
    #endregion

    // Use this for initialization
    void OnEnable () 
    {
        _currentPlayerName = PlayerSave.GetPlayerName();
        _currentPlayerEmail = PlayerSave.GetPlayerEmail();
        _currentPlayerPassword = PlayerSave.GetPlayerPassword();
	}
	
	// Update is called once per frame
    public void UpdatePlayerProfile (ProfileField selected) 
    {
		switch (selected)
        {
            case ProfileField.playerName:
                txtName.text = __currentPlayerName;
                PlayerSave.SavePlayerName(__currentPlayerName);
                break;
            case ProfileField.playerEmail:
                txtEmail.text = __currentPlayerEmail;
                PlayerSave.SavePlayerEmail(__currentPlayerEmail);
                break;
            case ProfileField.playerPassword:
                txtPassword.text = __currentPlayerPassword;
                PlayerSave.SavePlayerPassword(__currentPlayerPassword);
                break;
           
        }
	}

    /*
    public void ShowUI()
    {
        TopBarManager.Instance.SetBackButton(
              true
            , MenuManagerV2.Instance.ShowMainMenu
        );

        gameObject.SetActive(true);
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }
    */
}
