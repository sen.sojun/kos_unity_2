﻿using UnityEngine;
using System.Collections;

public class ActionButton : MonoBehaviour 
{
    public delegate void OnClickActionButtonEvent(int actionID);

    public UILabel Label;
    public UIButton Button;

    private int _actionID = -1;
    private OnClickActionButtonEvent _onClickActionButton = null;

	// Use this for initialization
	void Start () 
    {
	}
	
	// Update is called once per frame
    /*
	void Update () {
	
	}
    */

	public void SetActionButton(int actionID, CardPopupMenuUI.CardPopupActionType actionType, OnClickActionButtonEvent onClickActionButton)
    {
        _actionID = actionID;
        _onClickActionButton = onClickActionButton;

		string text = "";
		switch (actionType)
		{
			case CardPopupMenuUI.CardPopupActionType.Info: 		text = Localization.Get("BATTLE_CARD_POPUP_ACTION_INFO"); break;
			case CardPopupMenuUI.CardPopupActionType.Summon:	text = Localization.Get("BATTLE_CARD_POPUP_ACTION_SUMMON"); break;
			case CardPopupMenuUI.CardPopupActionType.Facedown: 	text = Localization.Get("BATTLE_CARD_POPUP_ACTION_FACEDOWN"); break;
			case CardPopupMenuUI.CardPopupActionType.Equip: 	text = Localization.Get("BATTLE_CARD_POPUP_ACTION_EQUIP"); break;
			case CardPopupMenuUI.CardPopupActionType.Use: 		text = Localization.Get("BATTLE_CARD_POPUP_ACTION_USE"); break;
			case CardPopupMenuUI.CardPopupActionType.Discard: 	text = Localization.Get("BATTLE_CARD_POPUP_ACTION_DISCARD"); break;
			case CardPopupMenuUI.CardPopupActionType.Attack: 	text = Localization.Get("BATTLE_CARD_POPUP_ACTION_ATTACK"); break;
			case CardPopupMenuUI.CardPopupActionType.Move: 		text = Localization.Get("BATTLE_CARD_POPUP_ACTION_MOVE"); break;
			case CardPopupMenuUI.CardPopupActionType.Ability: 	text = Localization.Get("BATTLE_CARD_POPUP_ACTION_ABILITY"); break;
			case CardPopupMenuUI.CardPopupActionType.Back: 		text = Localization.Get("BATTLE_CARD_POPUP_ACTION_BACK"); break;

			case CardPopupMenuUI.CardPopupActionType.None:
			default:
			text = Localization.Get("BATTLE_CARD_POPUP_ACTION_NONE"); break;
		}
			
		Label.text = text;
    }

    public void OnClickActionButton()
    {
        if (_onClickActionButton != null)
        {
            _onClickActionButton.Invoke(_actionID);
        }
    }
}
