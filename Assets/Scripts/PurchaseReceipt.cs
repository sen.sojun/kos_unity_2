﻿using System;
using System.Collections;
using System.Collections.Generic;
using MiniJSON;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;

[Serializable]
public class PurchaseReceipt
{
    public string TransactionPath;
    public string TransactionID;
    public string Receipt;
    
    public string DefinitionID;
    public string StoreSpecificID;
    
    public int Amount;
    public bool IsRedeem;
    public bool IsValid;
    public string PlayerID;
    public string Email;
    public string PurchaseTime;
         
    public PurchaseReceipt(Product product, string playerID, string email, bool isValid)
    {
        string transactionPath = product.transactionID;
        transactionPath = transactionPath.Replace(".", "");
        transactionPath = transactionPath.Replace("#", "");
        transactionPath = transactionPath.Replace("$", "");
        transactionPath = transactionPath.Replace("[", "");
        transactionPath = transactionPath.Replace("]", "");
        
        /*
        Firebase Database paths must not contain 
        '.', '#', '$', '[', or ']'
        */
        
        TransactionPath = transactionPath;      
        TransactionID = product.transactionID;
        
        Receipt = product.receipt;
        
        DefinitionID = product.definition.id;
        StoreSpecificID = product.definition.storeSpecificId;
        PlayerID = playerID;
        Email = email;
        PurchaseTime = System.DateTime.UtcNow.ToString(MatchLogData.TimeFormat);
        
        switch(StoreSpecificID)
        {
            case "com.lunarstudio.koscard.gold15": Amount = 15; break;
            case "com.lunarstudio.koscard.gold30": Amount = 30; break;
            case "com.lunarstudio.koscard.gold105": Amount = 105; break;
            case "com.lunarstudio.koscard.gold375": Amount = 375; break;
            case "com.lunarstudio.koscard.gold750": Amount = 750; break;
                        
            default: Amount = 0; break;
        }
        
        IsRedeem = false;
        IsValid = isValid;
    }
}