﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Firebase.Auth;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using UnityEngine.UI;


public class ShopBuyManager : MonoBehaviour 
{

	// insertion VIT
	#region Delegates
	public delegate void OnClickCallback(PlayerCardData clickData);
	#endregion

	#region Private Properties
	private OnClickCallback _onClickCallback = null;
	private int _iIndex = 0;
	#endregion

	#region Public Inspector Properties
	public UIScrollView ScrollView;
	public UIGrid Grid;
	public UILabel CardQtyLabel;
	public GameObject CardEditorUIPrefab;
	public List<FullCardUI> FullCardList;

	public GameObject ShowCardUIView;
	public GameObject CardContainer; 
	public FullCardUI FullCard_L;    
	public LoadingUI LoadingUI;
	#endregion
	// end of insertion VIT


    // Use this for initialization
    static Dictionary<string, int> card = new Dictionary<string, int>();
    StashFirebaseData stashdb;
    private string userID;


	#region Public
	public UILabel silverCoinLabel;

	public int NormalPrice =  1500; //500;
	public int PremiumPrice = 15;  //50;

	//insertion vit
	public int childCount { get { return Grid.transform.childCount; } }
	public int MaxDeck = 0;
	//end insertion vit

	#endregion


	#region Private
	private int __currentSilverCoin = 0;
	private Dictionary<string, ShopPackData> _shopList;

	private int _currentSilverCoin
	{
		get { return __currentSilverCoin; }
		set 
		{
			__currentSilverCoin = value;

			UpdateSilverCoinLabel ();
		}
	}

	private int __currentGoldCoin = 0;
	private int _currentGoldCoin
	{
		get { return __currentGoldCoin; }
		set 
		{
			__currentGoldCoin = value;

			UpdateSilverCoinLabel ();
		}
	}
	#endregion


	#region Methods
	//insertion vit
	public void ClearUI()
	{
		Grid.transform.DestroyChildren();
	}
	//end of insertion vit


	// Use this for initialization
	void Start () 
	{
		_shopList = new Dictionary<string, ShopPackData> ();
		_shopList.Add ("SP0001", new ShopPackData("SP0001"));

		// Load coin.
		_currentSilverCoin = PlayerSave.GetPlayerSilverCoin();
		_currentGoldCoin = PlayerSave.GetPlayerGoldCoin();

        userID = PlayerSave.GetPlayerUid();
        stashdb = new StashFirebaseData(userID, card);

        Debug.Log("Start Shop uid = " + userID);

		//temp
		//_currentSilverCoin = 350;
//		_currentGoldCoin = 0;
		//PlayerSave.SavePlayerSilverCoin(_currentSilverCoin);
//		PlayerSave.SavePlayerGoldCoin(_currentGoldCoin);

		Debug.Log ("Silver Coin = " + _currentSilverCoin);
		Debug.Log ("Gold Coin = " + _currentGoldCoin);

		//insertion vit
		foreach (FullCardUI fullCardUI in FullCardList) 
		{
			fullCardUI.BindOnClickCallback (this.OnClickedPreview);
		}
		//end insertion vit
	
	}

 	
	// Update is called once per frame
	/*
	void Update () 
	{
	}
	*/

	public void BuyStarterSet_5()
	{
		BuyStarterSet(NormalPrice);
	}

	public void BuyPremiumSet_5()
	{
		BuyPremiumSet(PremiumPrice);
	}
		
	private void BuyStarterSet(int iSilverToPaid)
	{
		Debug.Log ("Buy: " + iSilverToPaid.ToString());

		if (_currentSilverCoin >= iSilverToPaid) 
		{
			_currentSilverCoin -= iSilverToPaid;

			// save coin.
			PlayerSave.SavePlayerSilverCoin(_currentSilverCoin);

			List<string> resultCardList; 
			RandomNormal("SP0001", out resultCardList);
			DebugPrintText (resultCardList);

		} 
		else 
		{
			Debug.Log ("Not enough silver coin.");
		}
	}

	private void BuyPremiumSet(int iGoldToPaid)
	{
		//Debug.Log ("Buy: " + iGoldToPaid.ToString());

		if (_currentGoldCoin >= iGoldToPaid) 
		{
			_currentGoldCoin -= iGoldToPaid;

			// save coin
			PlayerSave.SavePlayerGoldCoin(_currentGoldCoin);

			List<string> resultCardList; 
			RandomPremium("SP0001",out resultCardList);
			DebugPrintText (resultCardList);
		} 
		else 
		{
			Debug.Log ("Not enough gold coin.");
		}
	}

//	public void UpdateGoldCoinLabel()
//	{
//		goldCoinLabel.text = "YOUR MONEY : " + _currentSilverCoin.ToString () + " SILVER " + _currentGoldCoin.ToString () + " GOLD ";
//	}

	public void UpdateSilverCoinLabel()
	{
		silverCoinLabel.text = "YOUR MONEY : " + _currentSilverCoin.ToString () + " SILVER " + _currentGoldCoin.ToString () + " GOLD ";
	}


    private void SaveCardToDB(string cardID)
    {
        string text = "";
        bool foundCard = false;
        int currQty = 1;
        Debug.Log("on save stash cashed");
        try
        {
            Debug.Log("on stash cashed");
            Debug.Log("on stash cashed successfully");

            stashdb = KOSServer.Instance.StashData;

            Debug.Log("stashDB");


            Debug.Log("uid =  " + stashdb.UID);

            foundCard = stashdb.Card.ContainsKey(cardID);


            if (foundCard)
            {
                currQty = stashdb.Card[cardID];
                ++currQty;
            }
            else
            {
                stashdb.Card.Add(cardID, 1);
            }

            KOSServer.UpdateStashData(stashdb, cardID, currQty);
        }

        catch (SystemException e)
        {
            Debug.Log("cash stash erro " + e.Message);
        }
    }

	private void RandomNormal(string shopPackID, out List<string> resultList)
	{
		
		ShopPackData shop = null;
		bool isFound = _shopList.TryGetValue (shopPackID, out shop);
		if (isFound) 
		{
			CardContainer.SetActive (true); //insert vit

			shop.RandomNormal(out resultList);
			_iIndex = 0;
			foreach (string id in resultList) 
			{
				PlayerCardData card = new PlayerCardData (new CardData(id));

				// add new card to stash
				bool isSuccess = AddCardToStash (card.PlayerCardID);
				if(isSuccess)
				{
                    // save card to filrebase
                    SaveCardToDB(card.PlayerCardID);

					// save new card to collection
					PlayerSave.SavePlayerCardCollection (card.PlayerCardID);
				
					//insertion vit

					FullCardList [_iIndex].SetCardData (new PlayerCardData (card));
					FullCardList [_iIndex].gameObject.SetActive (true);

					GameObject obj = Instantiate(CardEditorUIPrefab) as GameObject;
					obj.transform.SetParent(Grid.transform);
					obj.transform.localScale = Vector3.one;
					NGUITools.SetLayer(obj, Grid.gameObject.layer);

					CardEditorUI cardUI = obj.GetComponent<CardEditorUI>();
					cardUI.SetData(card, this.OnClickUI);

					obj.name = string.Format("{0}{1}{2}"
						, cardUI.Data.TypeData.CardTypeID
						, cardUI.Data.SymbolData.CardSymbolID
						, cardUI.Data.Name_EN
					);
					//ResetPosition(false);
					//end insertion vit
				
				}
				++_iIndex;
			}

			return;
		}

		resultList = null;
	}

	private void RandomPremium(string shopPackID, out List<string> resultList)
	{

		ShopPackData shop = null;
		bool isFound = _shopList.TryGetValue (shopPackID, out shop);
		if (isFound) 
		{
			

			shop.RandomPremium(out resultList);
			_iIndex = 0;
			foreach (string id in resultList) 
			{
				PlayerCardData card = new PlayerCardData (new CardData(id));

				// add new card to stash
				bool isSuccess = AddCardToStash (card.PlayerCardID);
				if(isSuccess)
				{
					// save new card to collection
					PlayerSave.SavePlayerCardCollection (card.PlayerCardID);


					//insertion vit
					FullCardList [_iIndex].SetCardData (card);
					FullCardList [_iIndex].gameObject.SetActive (true);


					GameObject obj = Instantiate(CardEditorUIPrefab) as GameObject;
					obj.transform.SetParent(Grid.transform);
					obj.transform.localScale = Vector3.one;
					NGUITools.SetLayer(obj, Grid.gameObject.layer);

					CardEditorUI cardUI = obj.GetComponent<CardEditorUI>();
					cardUI.SetData(card, this.OnClickUI);

					obj.name = string.Format("{0}{1}{2}"
						, cardUI.Data.TypeData.CardTypeID
						, cardUI.Data.SymbolData.CardSymbolID
						, cardUI.Data.Name_EN
					);
					//ResetPosition(false);
					//end insertion vit


					CardContainer.SetActive (true); //insert vit



				}
				++_iIndex;
			}

			return;
		}

		resultList = null;
	}

	private bool AddCardToStash(string playerCardID)
	{
		List<PlayerCardData> stash;
		bool isSuccess = PlayerSave.GetPlayerCardStash(out stash);
		if(isSuccess)
		{
			stash.Add(new PlayerCardData(playerCardID));
			PlayerSave.SavePlayerCardStash (stash);

			return true;
		}
		else
		{
			stash = new List<PlayerCardData>();
			stash.Add(new PlayerCardData(playerCardID));
			PlayerSave.SavePlayerCardStash (stash);

			return true;
		}

		return false;
	}

	private void DebugPrintText(List<string> cardIDList)
	{
		string resultText = "";
		foreach(string id in cardIDList)
		{
			CardData data = new CardData (id);
			resultText += data.SymbolData.SymbolName_EN + " " + id + ":" + data.Name_EN + "\n";
		}

		Debug.Log (resultText);
	}


	public void SetInitDataList(List<PlayerCardData> cardDataList, OnClickCallback onClickCallback)
	{
		// Clear old UI.
		ClearUI();

		_onClickCallback = onClickCallback;
		if(cardDataList != null)
		{
			CardQtyLabel.text = cardDataList.Count.ToString();
		}
		else
		{
			CardQtyLabel.text = "0";
		}

		if(MaxDeck > 0)
		{
			CardQtyLabel.text += "/" + MaxDeck.ToString();
		}

		foreach(PlayerCardData data in cardDataList)
		{
			GameObject obj = Instantiate(CardEditorUIPrefab) as GameObject;
			obj.transform.SetParent(Grid.transform);
			obj.transform.localScale = Vector3.one;
			NGUITools.SetLayer(obj, Grid.gameObject.layer);

			CardEditorUI cardUI = obj.GetComponent<CardEditorUI>();
			cardUI.SetData(data, this.OnClickUI);

			obj.name = string.Format("{0}{1}{2}"
				, cardUI.Data.TypeData.CardTypeID
				, cardUI.Data.SymbolData.CardSymbolID
				, cardUI.Data.Name_EN
			);
		}

		ResetPosition(false);
	}

	public void ResetPosition(bool isSmooth = true)
	{
		foreach (Transform child in Grid.transform)
		{
			CardEditorUI cardUI = child.GetComponent<CardEditorUI>();

			child.name = string.Format("{0}{1}{2}"
				, cardUI.Data.TypeData.CardTypeID
				, cardUI.Data.SymbolData.CardSymbolID
				, cardUI.Data.Name_EN
			);
		}

		CardQtyLabel.text = Grid.transform.childCount.ToString();
		if(MaxDeck > 0)
		{
			CardQtyLabel.text += "/" + MaxDeck.ToString();
		}

		Grid.animateSmoothly = isSmooth;
		Grid.Reposition();
		Grid.animateSmoothly = true;

		ScrollView.ResetPosition();
		ScrollView.UpdatePosition();
	}

	private void OnClickUI(PlayerCardData data)
	{
		if(_onClickCallback != null)
		{
			_onClickCallback.Invoke(data);
		}
	}

	void OnClickedPreview(FullCardUI fullCardUI)
	{
		Debug.Log ("Name:" + fullCardUI.PlayerCardData.Name_EN);

	
		FullCard_L.SetCardData(fullCardUI.PlayerCardData);
		ShowCardUIView.SetActive (true);

		BattleUIManager.RequestBringForward (ShowCardUIView.gameObject);
	}

	public void onCloseCardWidget()
	{
		CardContainer.SetActive (false);
	}

	public void GetFreeSilver()
	{
		int SilverNeed = 350;
		_currentSilverCoin += SilverNeed;
        PlayerSave.SavePlayerSilverCoin(_currentSilverCoin);
		UpdateSilverCoinLabel ();
	}

	public void GetFreeGold()
	{
		int GoldNeed = 350;
		_currentGoldCoin += GoldNeed;
		PlayerSave.SavePlayerGoldCoin(_currentGoldCoin);
		//UpdateGoldCoinLabel ();
	}

	#endregion



}
