﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerAvatarUI : MonoBehaviour {

    public List<Image> imgAvatar;
    private string currAvatar = "01";
    private static string _rootImagePath = "Images/Avatars/Full/";
    private static string _bannerImagePath = "Images/Avatars/Banner/";//+VIT17102018

    public Button PlayerImage; //Image PlayerImage;
    public Button PlayerImage2;
    public Text Choosendid;

    public int currImgIndex = 0;
    public bool bRequestToCancel = false;

    public Image PlayerMatchListImage; //+VIT17102018
    //public Text PlayerName;
    UserDBData data;
    AccountType accountType;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private int getIndexForFrame(int imgidx)
    {
        int returnval = 0;
        switch (imgidx)
        {
            case 7: returnval = 0; break;
            case 8: returnval = 1; break;
            case 9: returnval = 2; break;
            case 10: returnval = 3; break;
            case 11: returnval = 4; break;
            case 12: returnval = 5; break;
        }
        return returnval;
    }

    public void selectAvatar(int selected)
    {
        string newAvatar = "";
        int imgIndex = 0;
        int imgIndexFrame = 0;

        imgIndex = selected + 1;

        imgIndexFrame = getIndexForFrame(imgIndex);

        //switch (imgIndex)
        //{
        //    case 7: imgIndexFrame = 0; break;
        //    case 8: imgIndexFrame = 1; break;
        //    case 9: imgIndexFrame = 2; break;
        //    case 10: imgIndexFrame = 3; break;
        //    case 11: imgIndexFrame = 4; break;
        //    case 12: imgIndexFrame = 5; break;
                
        //}
        //Debug.Log("frame index = " + imgIndexFrame);
             

        newAvatar = imgIndex.ToString("00");
        //currAvatar = imgIndex.ToString("00");
        //SetData(newAvatar);
        LoadImg(newAvatar);
        Choosendid.text = newAvatar;
        //imgAvatar[selected].gameObject.SetActive(true);
        imgAvatar[imgIndexFrame].gameObject.SetActive(true);

        for (int i = 0; i < imgAvatar.Count ; ++i )
        {
            if (i != imgIndexFrame) //selected)
            {
                imgAvatar[i].gameObject.SetActive(false);   
            }
        }
        //foreach (Image pic in imgAvatar)
        //{
            
        //}
        //switch(selectedAvatar)
        //{
        //    case '01' 
        //}
    }

    public void LoadImg(string imagePath)
    {
        StartCoroutine(LoadImage(_rootImagePath + imagePath, imagePath));
        //Debug.Log("imag index x= " + imagePath);
    }

   

    private IEnumerator LoadImage(string imagePath, string imgIndex)
    {
        Choosendid.text = imgIndex;
        ResourceRequest request = Resources.LoadAsync<Sprite>(imagePath);
        yield return request;

        Sprite sprite = request.asset as Sprite;
        if (sprite != null)
        {
            //update to firebase and player save

            if(PlayerImage != null)
            {
                PlayerImage.image.sprite = sprite;
            }
            
            if(PlayerImage2 != null)
            {
                PlayerImage2.image.sprite = sprite;
            }



            Debug.Log("finish LoadImg");
            if (bRequestToCancel)
            {
                gameObject.SetActive(false);
            }
            bRequestToCancel = false;
            //PlayerSave.SavePlayerAvatar(imgIndex);
            //#if !UNITY_EDITOR
            //RequestServerChangeAvatar(imgIndex);
            //#endif

        }
        else
        {
            Debug.LogError("PlayerAvatarUI/LoadImage: Failed to load image. " + imagePath);
        }
    }

    public void SetData()//(string imagePath) //, string playerName = "")
    {
        Debug.Log("set data!" + Choosendid.text);
        string imagePath = Choosendid.text;
        PlayerSave.SavePlayerAvatar(Choosendid.text);
        

        StartCoroutine(SaveImage(_rootImagePath + imagePath, imagePath));
        //this.HideUI();
        //PlayerName.text = playerName;
    }

    private IEnumerator SaveImage(string imagePath, string imgIndex)
    {
      
            Choosendid.text = imgIndex;
        //Debug.Log("imagePath= " + imagePath);
            ResourceRequest request = Resources.LoadAsync<Sprite>(imagePath);
            yield return request;

            Sprite sprite = request.asset as Sprite;
        //Debug.Log("sprite = " + sprite.ToString());
            if (sprite != null)
            {
                //update to firebase and player save
               
            Debug.Log("save avatar++");
                    if (PlayerImage != null)
                    {
                        PlayerImage.image.sprite = sprite;
                    }

                    //if (PlayerImage2 != null)
                    //{
                    //    PlayerImage2.image.sprite = sprite;
                    //}

                   
                    PlayerSave.SavePlayerAvatar(imgIndex);

            //--start of insertion vit 17102018
            //--to update player avatar in match list pvp
            if ( this.PlayerMatchListImage != null )
            {
                string imgBanner = _bannerImagePath + imgIndex;
                Debug.Log("Player banner = " + imgBanner);
                ResourceRequest request_ = Resources.LoadAsync<Sprite>(imgBanner);
                yield return request_;

                Sprite sprite_ = request_.asset as Sprite;
                if (sprite_ != null)
                {
                    PlayerMatchListImage.sprite = sprite_;
                }

            }
            //--end of insertion vit 17102018

                    //string test = PlayerSave.GetPlayerAvatar();
                    //Debug.Log("prepare save avatar = " + test);
    #if !UNITY_EDITOR
                RequestServerChangeAvatar(imgIndex);
    #endif
            }
               
    
            else
            {
                Debug.LogError("PlayerAvatarUI/LoadImage: Failed to load image. " + imagePath);
            }
        this.HideFromSave();
    }

#region change avatar
    private void RequestServerChangeAvatar(string newimage)
    {
        ScreenDubugLog.Instance.Print("uid = " + data.uid + data.Userphoto);
        ScreenDubugLog.Instance.Print(" new name " + newimage);
        try
        {
            // To do request to server to change name.

            if (data.uid != "")
            {
                KOSServer.UpdateUserData(data, "Userphoto", newimage);
                OnServerChangeImageResponse(true, "success");

            }
            else
            {
               // ShowResult("no uid found");
            }
            // Debug.Log("uid  = " + data.uid);

        }

        catch (System.Exception e)
        {
            ScreenDubugLog.Instance.Print("Error change image " + e.Message);

            OnServerChangeImageResponse(false, e.Message);
        }


    }

    private void OnServerChangeImageResponse(bool isSuccess, string sResult)
    {
        string resultMsg = "";
        if (isSuccess)
        {
            resultMsg = "Successful Change Avatar";
        }
        else
        {
            resultMsg = "Sorry. Failed" + sResult;
        }

       
    }
    #endregion


    public void ShowUI()
    {
        //TopBarManager.Instance.SetBackButton(
        //    true
        //    , TopBarManager.Instance.hideAvatarUI
        //);
        int imgIndexFrame = 0;
        int imgIndex = 0;
        accountType = PlayerSave.GetPlayerAccountType();
        gameObject.SetActive(true);
       

#if !UNITY_EDITOR
        
        data = new UserDBData();

        //RequestCacheData();
        try
        {
            if (accountType == AccountType.Guest)
            {
                   data.uid = PlayerSave.GetPlayerUid();
        ScreenDubugLog.Instance.Print("avatar guest:get data from player save " + data.uid);
                    Debug.Log("get data from player save");
            }
            else
            {
                data = KOSServer.Instance.PlayerData;
                ScreenDubugLog.Instance.Print("get data " + data.uid);
                if (data.uid != "")
                {
                    
                    Debug.Log("get data from kosserver");
                }
                else
                {
                    data.uid = PlayerSave.GetPlayerUid();
        ScreenDubugLog.Instance.Print("player avatar else :get data from player save " + data.uid);
                    Debug.Log("get data from player save");
                }
            }
          
        }
        catch (System.Exception e)
        {
            ScreenDubugLog.Instance.Print("error cached player " + e.Message);
            data.uid = PlayerSave.GetPlayerUid();
            Debug.Log("get data from player save");
        }
       
#endif
        currAvatar = PlayerSave.GetPlayerAvatar();

        imgIndex = int.Parse(currAvatar);// - 1;

        currImgIndex = imgIndex - 1; //imgIndex;

        Debug.Log("curr avatar = " + currAvatar + " " + imgIndex);

        imgIndexFrame = getIndexForFrame(imgIndex);

       // imgAvatar[imgIndex].gameObject.SetActive(true);
        imgAvatar[imgIndexFrame].gameObject.SetActive(true);

        Choosendid.text = currAvatar.ToString();

        for (int i = 0; i < imgAvatar.Count; ++i)
        {
            if (i != imgIndexFrame) //imgIndex)
            {
                imgAvatar[i].gameObject.SetActive(false);
            }
        }

    }

    public void HideFromSave()
    {
        gameObject.SetActive(false);
    }


    public void HideUI()
    {
        Debug.Log("cancel to the original " + currImgIndex);
        bRequestToCancel = true;
        selectAvatar(currImgIndex);

        //gameObject.SetActive(false);
    }
}
