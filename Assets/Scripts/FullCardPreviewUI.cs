﻿using UnityEngine;
using System.Collections;

public class FullCardPreviewUI : MonoBehaviour 
{
	private bool _isReady = false;
	public FullCardUI FullCardUI;

	// Use this for initialization
	void Start () 
	{
		//NGUITools.SetActive(gameObject, false);
		_isReady = true;
	}
	
	// Update is called once per frame
	/*
	void Update () 
	{
	
	}
	*/

	public void ShowUI(PlayerCardData playerCardData)
	{
		if(gameObject.activeSelf)
		{
			FullCardUI.SetCardData(playerCardData);
			FullCardUI.RequestBringForward();
		}
		else
		{
			FullCardUI.SetCardData(playerCardData);

			NGUITools.SetActive(gameObject, true);
			FullCardUI.RequestBringForward();
		}
	}

	public void HideUI()
	{
		NGUITools.SetActive(gameObject, false);
	}
}
