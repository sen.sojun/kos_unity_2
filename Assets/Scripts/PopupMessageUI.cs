﻿using UnityEngine;
using System.Collections;

public class PopupMessageUI : MonoBehaviour 
{
	public delegate void OnClickPopup();

	public GameObject ButtonGroup;
	public GameObject ButtonGroup2;

	public UILabel MessageLabel;
	public UILabel ButtonLabel;
	public UILabel ButtonLabel2;

	private OnClickPopup _onClickCallback = null;
	private OnClickPopup _onClickCallback2 = null;

	public void ShowUI(string message, string buttonText = "OK", OnClickPopup callback = null)
	{
		_onClickCallback = callback;
		MessageLabel.GetComponent<UILocalize>().key = message;
		ButtonLabel.GetComponent<UILocalize>().key = buttonText;

		ButtonGroup.SetActive (true);
		ButtonGroup2.SetActive (false);

		gameObject.SetActive (true);
	}

	public void ShowUI2(string message, string buttonText = "OK", string buttonText2 = "Cancel" ,OnClickPopup callback = null, OnClickPopup callback2 = null)
	{
		_onClickCallback = callback;
		_onClickCallback2 = callback2;

		MessageLabel.GetComponent<UILocalize>().key = message;
		ButtonLabel.GetComponent<UILocalize>().key = buttonText;
		ButtonLabel2.GetComponent<UILocalize>().key = buttonText2;

		ButtonGroup.SetActive (true);
		ButtonGroup2.SetActive (true);

		gameObject.SetActive (true);
	}

	public void OnConfirmPopup()
	{
		gameObject.SetActive (false);

		if (_onClickCallback != null) 
		{
			_onClickCallback.Invoke ();
		}
	}

	public void OnConfirmPopup2()
	{
		gameObject.SetActive (false);

		if (_onClickCallback2 != null) 
		{
			_onClickCallback2.Invoke ();
		}
	}
}
