﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangePasswordUI : MonoBehaviour {

    public delegate void OnChangePassword(string currPwd,string newPwd);
    public delegate void OnCancel();

    private OnChangePassword _onChangePassword = null;
    private OnCancel _onCancel = null;

    public Button ChangePasswordButton;
    public Button CancelButton;
    public InputField CurrPwdInputField;
    public InputField NewPwdInputField;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ShowUI(OnChangePassword onChangerPassword, OnCancel onCancel)
    {
        _onChangePassword = onChangerPassword;
        _onCancel = onCancel;
        ChangePasswordButton.onClick.RemoveAllListeners();
        ChangePasswordButton.onClick.AddListener(this.onChangePasswordEvent);

        CancelButton.onClick.RemoveAllListeners();
        CancelButton.onClick.AddListener(this.OnCancelEvent);

        gameObject.SetActive(true);
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }

    private void onChangePasswordEvent()
    {
        if (_onChangePassword != null)
        {
            _onChangePassword.Invoke(CurrPwdInputField.text,NewPwdInputField.text);
        }
    }

    private void OnCancelEvent()
    {
        if (_onCancel != null)
        {
            _onCancel.Invoke();
        }
    }
}
