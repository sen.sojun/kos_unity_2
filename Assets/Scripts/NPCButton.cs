﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(UIButton))]
public class NPCButton : MonoBehaviour 
{
	public string NPCID = "";

	public UISprite NPCImage;
	public UILabel NameLabel;
	public GameObject GlowEffect;

	private bool _isInit = false;
	private NPCDBData _npcDBData;


	// Use this for initialization
	void Start () 
	{
		UpdateUI();
	}

    void OnEnable()
    {
        UpdateUI();
    }

	// Update is called once per frame
	void Update () 
	{
		if(!_isInit || string.Compare(NPCID, _npcDBData.NPCID) != 0)
		{
			UpdateUI();
		}
	}
		
	public void UpdateUI()
	{
		bool isSuccess = NPCDB.GetData(NPCID, out _npcDBData);
		if(isSuccess)
		{
			NPCImage.spriteName = _npcDBData.ImageName;

            string name = "";
            switch (LocalizationManager.CurrentLanguage)
            {
                case LocalizationManager.Language.Thai:
                {
                    name = _npcDBData.Name_TH;
                }
                break;

                case LocalizationManager.Language.English:
                default:
                {
                    name = _npcDBData.Name_EN;
                }
                break;
            }

            NameLabel.text = name;
			_isInit = true;
		}
	}

	public void SetEnableButton(bool isEnable)
	{
		GetComponent<UIButton>().isEnabled = isEnable;
	}

	public void SetShowGlowEffect(bool isShow)
	{
		GlowEffect.SetActive(isShow);
	}
}
