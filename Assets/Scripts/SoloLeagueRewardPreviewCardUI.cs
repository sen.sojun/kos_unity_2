﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoloLeagueRewardPreviewCardUI : MonoBehaviour {
	public FullCardUI PreviewCard;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ShowUI(PlayerCardData playerCardData)
	{
		PreviewCard.SetCardData (playerCardData);
		gameObject.SetActive (true);
		BattleUIManager.RequestBringForward (gameObject);
	}

	public void HideUI()
	{
		gameObject.SetActive (false);
	}
}
