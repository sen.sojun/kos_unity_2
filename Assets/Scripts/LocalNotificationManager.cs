﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class LocalNotificationManager : MonoBehaviour
{
    private static LocalNotificationManager _instance = null;
    public static LocalNotificationManager Instance { get { return _instance; } }

    private List<LocalNotificationData> _dataList = null;
    private LocalNotificationData _data = null;
    private string sMsgCode = "";
    private string sMsg = "";
    private DateTime dNotiDT;

    private long dNotiDTAndroid;

    private string getPlayerSaveNotiTime = "";
    private string sNotificationTime = "";
    
    // Use this for initialization
    void Start()
    {
        _instance = this;
        DontDestroyOnLoad(this.gameObject);

        RegisterForNotifications();

        //sMsgCode = getMsgCode();
        //if (sMsgCode != "")
        //{
        //    DateTime nowTime = DateTime.Now;
        //    dNotiDT = getNotiTime(sMsgCode);
        //    dNotiDTAndroid = Convert.ToInt64((dNotiDT - nowTime).TotalMilliseconds);
        //    sMsg = getNotiMsg(sMsgCode);

        //    //Debug.Log("MshCode = " + sMsgCode + "Msg = " + sMsg + " Ios Date = " + dNotiDT.ToString() + " noti android = " + dNotiDTAndroid.ToString());
        //}

        //callNoti (getMsgCode ());
    }

    // Update is called once per frame
    void Update()
    {
    }

    void Awake()
    {
        //MyLocalNotification.ClearNotifications();
    }

	private void OnDestroy()
	{
		_instance = null;
	}

	void StartNotification()
    {
        try
        {
            sMsgCode = getMsgCode();
            if (sMsgCode != "")
            {
                DateTime nowTime = DateTime.Now;
                dNotiDT = getNotiTime(sMsgCode);
                sNotificationTime = dNotiDT.ToString("dd/MM/yyyy HH:mm:ss");  //set notification for the first time ever
                dNotiDTAndroid = Convert.ToInt64((dNotiDT - nowTime).TotalMilliseconds);
                sMsg = getNotiMsg(sMsgCode);
                //MyLocalNotification.SendNotification(1, 5000, "KOS", sMsg, new Color32(0xff, 0x44, 0x44, 255), true, true, true, "notify_icon_big"); //"app_icon");
                MyLocalNotification.SendNotification(1, dNotiDTAndroid, "KOS", sMsg, new Color32(0xff, 0x44, 0x44, 255), true, true, true, "notify_icon_big"); //"app_icon");

                PlayerSave.SaveNotificationTime(sNotificationTime);
                PlayerSave.SaveNotificationRun(true);
                Debug.Log("MsgCode = " + sMsgCode + "Msg = " + sMsg + " Ios Date = " + dNotiDT.ToString() + " noti android = " + dNotiDTAndroid.ToString());
            }
        }
        catch (Exception e)
        {
            Debug.Log("Error local 1 " + e.Message);
        }

    }

    void ToSetNewNoti()
    {
        try
        {
            double deltaTime = 0;
            DateTime nowTime = DateTime.Now;

            DateTime dtGetTimeFromSave = nowTime;
            dtGetTimeFromSave = DateTime.ParseExact(sNotificationTime, "dd/MM/yyyy HH:mm:ss", null);
            deltaTime = (dtGetTimeFromSave - nowTime).TotalMilliseconds;

            Debug.Log("Notime = " + dtGetTimeFromSave.ToString() + "Delta Time = " + deltaTime.ToString());

            if (deltaTime <= 0.0f)
            {
                // already passed
                Debug.Log("Set new Noti time = " + dtGetTimeFromSave.ToString() + "Delta Time = " + deltaTime.ToString());
                StartNotification();

            }
        }
        catch (Exception e)
        {
            Debug.Log("Error local 2 " + e.Message);
        }

    }

    void OnApplicationPause(bool pauseStatus)
    {
        try
        {
            if (pauseStatus)
            {
                Debug.Log("start onapp pause " + PlayerSave.IsNotificationRun().ToString());

                //check if the first time app start
                if (!PlayerSave.IsNotificationRun())
                {
                    StartNotification();
                }
                else
                {
                    //compare time to set new notification
                    Debug.Log("set noti player save already check if set new!");
                   // ToSetNewNoti(); --> vit17022018
                }

                //  callNoti (getMsgCode ());
                // MyLocalNotification.SendNotification(1, dNotiDTAndroid, "KOS", sMsg, new Color32(0xff, 0x44, 0x44, 255), true, true, true, "notify_icon_big"); //"app_icon");

            }
            else
            {
                Debug.Log("cant start onapp");
            }
        }
        catch (Exception e)
        {
            Debug.Log("Error local 3 " + e.Message);
        }
    }

    bool IsSetNotification()
    {
        return PlayerSave.IsNotificationRun();
    }

    private void LoadData()
    {
        LocalNotificationDB.GetAllData(out _dataList);
    }

    public string getMsgCode()
    {
        string msgCode = "";
        try
        {
            LoadData();
            int totalNotiList = _dataList.Count;
            //Debug.Log ("msgcount = " + totalNotiList);
            msgCode = UnityEngine.Random.Range(1, totalNotiList).ToString("0000");
            //Debug.Log ("msg code = " + msgCode );
        }
        catch (Exception e)
        {
            Debug.Log("Error local 4 " + e.Message);
        }
        return msgCode;
    }

    public DateTime getNotiTime(string msgCode)
    {
        DateTime nowTime = DateTime.Now;
        DateTime notiTime = nowTime;
        try
        {
            LoadData();
            bool isSuccessLoad = LocalNotificationDB.GetData(msgCode, out _data);
            if (isSuccessLoad)
            {
                string[] timeText = _data.TimeToSend.Split(':');

                int hDb = int.Parse(timeText[0]);   //get hour to int
                int mDb = int.Parse(timeText[1]);   //get minutes to int
                int sDb = int.Parse(timeText[2]);   //get second to int

                notiTime = new DateTime(nowTime.Year, nowTime.Month, nowTime.Day, hDb, mDb, sDb);

                double deltaTime = (notiTime - nowTime).TotalMilliseconds;

                if (deltaTime <= 0.0f)
                {
                    // already passed
                    notiTime = notiTime.AddDays(1);
                }
            }
        }
        catch (Exception e)
        {
            Debug.Log("Error local 5 " + e.Message);
        }
        return notiTime;
    }

    public string getNotiMsg(string msgCode)
    {
        bool isSuccessLoad = false;
        string msg = "";
        try
        {
            LoadData();
            isSuccessLoad = LocalNotificationDB.GetData(msgCode, out _data);
            if (isSuccessLoad)
            {
                switch (LocalizationManager.CurrentLanguage)
                {
                    case LocalizationManager.Language.Thai:
                        msg = _data.MessageTH;
                        break;

                    default:
                        msg = _data.MessageEN;
                        break;
                }
            }
        }
        catch (Exception e)
        {
            Debug.Log("Error local 6 " + e.Message);
        }
        return msg;
    }

    public void callNoti(string msgCode)
    {
        try
        {
            bool isSuccessLoad = false;
            string msg = "";

            LoadData();
            isSuccessLoad = LocalNotificationDB.GetData(msgCode, out _data);
            if (isSuccessLoad)
            {
                switch (LocalizationManager.CurrentLanguage)
                {
                    case LocalizationManager.Language.Thai:
                        msg = _data.MessageTH;
                        break;

                    default:
                        msg = _data.MessageEN;
                        break;
                }

                string[] timeText = _data.TimeToSend.Split(':');
                int hh = int.Parse(timeText[0]);
                int mm = int.Parse(timeText[1]);
                int ss = int.Parse(timeText[2]);

                mm = System.DateTime.Now.Minute + 5;
                Debug.Log("hh " + DateTime.Now.Hour + " mm = " + mm);

#if UNITY_IOS
                UnityEngine.iOS.LocalNotification notif = new UnityEngine.iOS.LocalNotification();
                DateTime dateValue = new DateTime(System.DateTime.Now.Year, System.DateTime.Now.Month, System.DateTime.Now.Day, System.DateTime.Now.Hour, mm, ss);

                notif.fireDate = dateValue;  //System.DateTime . //.Now.AddSeconds(5f);
                                             //notif.fireDate = DateTime.Now.AddSeconds(35);
                notif.alertBody = msg;
                UnityEngine.iOS.NotificationServices.ScheduleLocalNotification(notif);

                //
                //			UnityEngine.iOS.LocalNotification notif2 = new UnityEngine.iOS.LocalNotification();
                //			DateTime dateValue2 = new DateTime(System.DateTime.Now.Year, System.DateTime.Now.Month, System.DateTime.Now.Day, 24, 05, ss);
                //			notif2.fireDate          =  dateValue;  //System.DateTime . //.Now.AddSeconds(5f);
                //			notif2.alertBody         =  msg;
                //			UnityEngine.iOS.NotificationServices.ScheduleLocalNotification(notif2);
#endif

                //Debug.Log ("text = " + _data.MessageTH);
                //Debug.Log ("text = " + _data.MessageEN);
            }
        }
        catch (Exception e)
        {
            Debug.Log("Error local 7 " + e.Message);
        }
    }

    void RegisterForNotifications()
    {
#if UNITY_IOS
        UnityEngine.iOS.NotificationServices.RegisterForNotifications (UnityEngine.iOS.NotificationType.Alert |
		UnityEngine.iOS.NotificationType.Badge |
		UnityEngine.iOS.NotificationType.Sound);
#endif
    }

	void OnApplicationQuit()
	{
		//Savee the current system time as a string in the player prefs class
		PlayerPrefs.SetString("sysString", System.DateTime.Now.ToBinary().ToString());

		print("Saving this date to prefs: " + System.DateTime.Now);
	}
}