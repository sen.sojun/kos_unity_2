﻿using UnityEngine;
using System.Collections;

public class ActiveInactiveGameObjectScript : MonoBehaviour {

	public GameObject myGameObject;
	public bool bSetActive = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ActiveInactiveGameObject()
	{
		myGameObject.SetActive (bSetActive);
	}

}
