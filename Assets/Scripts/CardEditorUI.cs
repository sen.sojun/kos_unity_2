﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class CardEditorUI : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler, IPointerClickHandler
{
    #region Delegates
    public delegate void OnClickCallback(PlayerCardData clickedCardData);
    #endregion

    #region Private Properties 

    private int _cardlistIndex = -1;
    private int _cardDataIndex = -1;
    private PlayerCardData _data = null;
    private OnClickCallback _onClickCallback = null;
    #endregion

    #region Private Properties 
    public PlayerCardData Data { get { return _data; } }
    public int CardDataIndex { get { return _cardDataIndex; } }

    protected static Canvas canvas;
    private static string canvasName = "DeckEditorDragAndDropCanvas";
    private static int canvasSortOrder = 101;
    #endregion

    #region Public Inspector Properties
    public FullCardUI2 FullCardUI;
    #endregion

    #region Awake
    void Awake()
    {
        if (canvas == null)
        {
            GameObject canvasObj = new GameObject(canvasName);
            canvas = canvasObj.AddComponent<Canvas>();
            canvas.renderMode = RenderMode.ScreenSpaceOverlay;
            canvas.sortingOrder = canvasSortOrder;
        }
    }
    #endregion

    #region Start
    // Use this for initialization
    void Start()
    {
    }
    #endregion

    #region Update
    // Update is called once per frame
    void Update()
    {
    }
    #endregion

    #region Methods
    public void SetData(PlayerCardData data, OnClickCallback onClickCallback)
    {
        _cardlistIndex = -1;
        _cardDataIndex = -1;
        _data = data;
        _onClickCallback = onClickCallback;

        /*
        FullCardUI.SetCardData(data);
        FullCardUI.RequestBringForward(false);
        */
    }

    public void SetData(int cardListIndex, int cardDataIndex, PlayerCardData data, OnClickCallback onClickCallback)
    {
        _cardlistIndex = cardListIndex;
        _cardDataIndex = cardDataIndex;
        _data = data;
        _onClickCallback = onClickCallback;

        /*
        FullCardUI.SetCardData(data);
        FullCardUI.RequestBringForward(false);
        */
    }
    #endregion

    #region Event Methods
    private Vector3 _originPosition;
    private Transform _originParent;
    public void OnBeginDrag(PointerEventData eventData)
    {
        //transform.SetParent(null);

        _originPosition = transform.position;
        _originParent = transform.parent;
        transform.SetParent(canvas.transform);
    }

    public void OnDrag(PointerEventData eventData)
    {
        //Debug.Log("OnDrag");

        transform.position = eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Debug.Log("OnEndDrag");

        transform.position = _originPosition;
        transform.SetParent(_originParent);

        /*
        if (surface != null)
        {
            CardEditorListUI cardListUI = null;
            cardListUI = surface.GetComponent<CardEditorListUI>();

            if (cardListUI == null)
            {
                cardListUI = surface.GetComponentInParent<CardEditorListUI>();
            }

            if (cardListUI != null)
            {
                //Debug.Log("name: " + cardListUI.name);

                base.OnDragDropRelease(surface);
                if (this._cardlistIndex != cardListUI.CardListIndex)
                {
                    //Debug.Log(this._cardlistIndex + "," + cardListUI.CardListIndex);
                    cardListUI.OnDropCard(this._cardDataIndex);
                }

                // Destroy this icon as it's no longer needed
                //NGUITools.Destroy(gameObject);
                return;
            }
        }
        */
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Debug.Log("OnPointerClick");
        if (_onClickCallback != null)
        {
            _onClickCallback.Invoke(_data);
        }
    }

    public void OnDropCardCallBack()
    {
        
    }
    #endregion
}
