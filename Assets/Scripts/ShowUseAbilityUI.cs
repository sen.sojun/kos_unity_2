﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShowUseAbilityUI : MonoBehaviour 
{
	public delegate void OnFinishShow();

	public struct PerformUseAction
	{
		public string PopupText;
	};

	public struct PerformAtkStep
	{
		public List<PerformUseAction> ActionList;
	};

	public FullCardUI FullCard;
	public UILabel PopupText;

	private PlayerCardData _playerCardData;

	private float _timer = 0.0f;
	private float _waitStepTime = 0.75f;
	private bool _isStartShow = false;
	private OnFinishShow _onFinishShow = null;

	private List<PerformUseAction> _stepList;

	void Awake()
	{
		//NGUITools.SetActive(gameObject, false);
	}

	// Use this for initialization
	/*
	void Start () 
	{
	}
	*/
	
	// Update is called once per frame
	void Update () 
	{
		if(_isStartShow)
		{
			_timer += Time.deltaTime;
			if(_timer >= _waitStepTime)
			{
				_timer -= _waitStepTime;
				OnShowStep();
			}
		}
	}

	public void ShowUI(PlayerCardData playerCardData, OnFinishShow onFinishShowCallback)
	{
		_playerCardData = playerCardData;

		// Set show fullcard value
		FullCard.SetCardData(playerCardData);
        FullCard.RequestBringForward();
        
        PopupText.text = "";

		_onFinishShow = onFinishShowCallback;

		_isStartShow = false;
	}

	public void SetStepList(List<PerformUseAction> stepList)
	{
		_stepList = stepList;

		NGUITools.SetActive(gameObject, true);
		BattleUIManager.RequestBringForward(gameObject, true);

		UIWidget widget = gameObject.GetComponent<UIWidget>();
		if(widget)
		{
			NGUITools.AdjustDepth(widget.panel.gameObject, 1001);
		}

		SoundManager.PlayEFXSound(BattleController.Instance.AbilitySound);
			
		_timer = 0.0f;
		_isStartShow = true;
	}

	private void OnShowStep()
	{
		if(_stepList.Count > 0)
		{
			PerformUseAction step = _stepList[0];

			ShowPopupText(step.PopupText);

			_stepList.RemoveAt(0);
		}	
		else
		{
			HideUI();
		}
	}

	private void ShowPopupText(string popupText)
	{
		PopupText.text = popupText;
	}

	private void ClearPopupText()
	{
		PopupText.text = "";
	}

	private void HideUI()
	{
		_isStartShow = false;

		ClearPopupText();

		NGUITools.SetActive(gameObject, false);

		if(_onFinishShow != null)
		{
			_onFinishShow.Invoke();
		}
	}

}
