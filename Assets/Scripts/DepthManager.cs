﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DepthManager : MonoBehaviour 
{
	List<UIWidget> _widgetList;
	List<int>	_initDepthList;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void Awake()
	{
		InitDepth();
	}

	public void OnEnable()
	{
		ResetDepth();
	}
		
	private void InitDepth()
	{
		_widgetList = new List<UIWidget>();
		_initDepthList = new List<int>();

		UIWidget[] childs = gameObject.GetComponentsInChildren<UIWidget>();

		foreach(UIWidget child in childs)
		{
			_initDepthList.Add(child.depth);
			_widgetList.Add(child);
		}
	}

	public void ResetDepth()
	{
		for(int index = 0; index < _widgetList.Count; ++index)
		{
			_widgetList[index].depth = _initDepthList[index];
		}
	}
	public void ReinitDepth()
	{
		InitDepth();
	}
}
