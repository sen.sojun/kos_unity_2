﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Purchaser : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void get15Gold()
    {
        
       // iAPManager.Instance.InitializePurchasing();
        iAPManager.Instance.BuyGold15();
    }

    public void get30Gold()
    {
       // iAPManager.Instance.InitializePurchasing();
        iAPManager.Instance.BuyGold30();
    }

    public void get105Gold()
    {
        //iAPManager.Instance.InitializePurchasing();
        iAPManager.Instance.BuyGold105();
    }

    public void get375Gold()
    {
        //iAPManager.Instance.InitializePurchasing();
        iAPManager.Instance.BuyGold375();
    }

    public void get759Gold()
    {
        //iAPManager.Instance.InitializePurchasing();
        iAPManager.Instance.BuyGold750();
    }



}
