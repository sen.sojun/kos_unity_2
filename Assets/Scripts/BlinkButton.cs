﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

[RequireComponent(typeof(UIButton))]
public class BlinkButton : MonoBehaviour 
{
	public Color StartColor = Color.white;
	public Color EndColor = Color.white;
	public float BlinkTime = 1.0f;
	public bool PauseWhileDisabling = false;
	public Ease EaseType = Ease.Linear;

	/*
	private Sequence _s = null;

	private Tweener _t = null;
	private Tweener _tH = null;
	private Tweener _tP = null;
	private Tweener _tD = null;
	private Tweener _tN = null;
	*/

	private Tween _t;
	private UIButton _button;
	private UISprite _sprite;
	private Color _color;

	void Awake()
	{
		_button = GetComponent<UIButton>();
		_sprite = GetComponent<UISprite>();
	}

	// Use this for initialization
	void Start () 
	{
	}

	// Update is called once per frame
	void Update () 
	{
		_sprite.color = _color;
		_button.defaultColor = _color;
		_button.pressed = _color;
		_button.hover = _color;
		_button.disabledColor = _color;
	}

	void OnEnable()
	{
		if (PauseWhileDisabling || _t == null) 
		{
			StartBlink ();
		} 
	}

	void OnDisable()
	{
		if (PauseWhileDisabling && _t != null) 
		{
			StopBlink ();
		} 
	}

	private void StartBlink()
	{   
		if (_t == null) 
		{	
			_color = StartColor;
			_t = DOTween.To (
				  () => _color
				, x => _color = x
				, EndColor
				, BlinkTime
			);

			_t.SetLoops (-1, LoopType.Yoyo);
			_t.SetEase (EaseType);
			_t.SetAutoKill(false);
			_t.Play ();
		} 
		else 
		{
			_t.Restart ();
		}
	}

	private void StopBlink()
	{  
		_t.Pause ();
	}
}
