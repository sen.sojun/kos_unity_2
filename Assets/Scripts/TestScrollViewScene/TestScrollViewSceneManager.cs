﻿using UnityEngine;
using System.Collections;

public class TestScrollViewSceneManager : MonoBehaviour 
{
	public GameObject ItemPrefab;

	public ScrollViewUI TopScrollView;


	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void AddTop(int num = 1)
	{
		for(int i = 0; i < num; ++i)
		{
			GameObject item = Instantiate(ItemPrefab) as GameObject;
			item.transform.parent = TopScrollView.Grid.transform;
			item.transform.localScale = Vector3.one;
		}

		ResetScrollView();
		//TopScrollView.Grid.Reposition();
	}

	public void AddTop1()
	{
		AddTop(1);
	}

	public void AddTop10()
	{
		AddTop(10);
	}

	public void RemoveTop()
	{
		if(TopScrollView.Grid.transform.childCount > 0)
		{
			Transform item = TopScrollView.Grid.transform.GetChild(0);
			Destroy(item.gameObject);
		}

		ResetScrollView();
	}

	public void RemoveTopAll()
	{
		//ResetScrollView();

		TopScrollView.Grid.transform.DestroyChildren();

		//ResetScrollView();
	}

	public void ResetScrollView()
	{
		TopScrollView.Grid.Reposition();
		TopScrollView.ScrollView.ResetPosition();
	}

	public void Action_A()
	{
		AddTop(10);
	}

	public void Action_B()
	{
		RemoveTopAll();
		ResetScrollView();
		Action_A();
	}

}
