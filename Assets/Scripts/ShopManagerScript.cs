﻿using UnityEngine;
using System.Collections;

public class ShopManagerScript : MonoBehaviour 
{
	public AudioClip BGMClip;
	public ShopFreeSilverManager ShopFreeSilverWindow;

	void Start()
	{
		if(!SoundManager.IsPlayBGM(BGMClip))
		{
			SoundManager.PlayBGM(BGMClip);
		}
	}

	public void ShowCurrentCoin(int iCurrentCoin)
	{
		Debug.Log (iCurrentCoin);
		ShopFreeSilverWindow.ShowCurrentCoin(iCurrentCoin);
	}
}
