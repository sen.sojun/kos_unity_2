﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoloLeagueRewardButtonUI : MonoBehaviour {

	public  UILabel RankLabel;
	public  UILabel RewardTitle;
	public  UILabel LevelUseLabel;
	public  UILabel DiamondUseLabel;
	public  FullCardUI CardUI;
	public  UISprite PackImgUI;

	private int _buttonRankIndex;
	//private RewardTypeEnum RewardType;
	public int ButtonRankIndex { get { return _buttonRankIndex; } }

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void ShowUI(SoloLeagueRewardDBData data)
	{
		string titleText = "";
		RankLabel.text = "Rank " + data.PositionIndex.ToString ();
		DiamondUseLabel.text = data.DiamondUse.ToString ();
		LevelUseLabel.text = "LV. " + data.LevelUse.ToString ();

		switch (LocalizationManager.CurrentLanguage)
		{
		case LocalizationManager.Language.Thai:
			{
				titleText  = data.RewardText_Thai;
			}
			break;

		case LocalizationManager.Language.English:
		default:
			{
				titleText = data.RewardText_Eng;
			}
			break;
		}

		RewardTitle.text = titleText;


		switch(data.RewardType)
		{
			case SoloLeagueRewardDBData.RewardTypeEnum.Card:
			{
				CardUI.SetCardData (new PlayerCardData(data.RewardKey));

				CardUI.gameObject.SetActive (true);
				PackImgUI.gameObject.SetActive (false);
			}
			break;

			case SoloLeagueRewardDBData.RewardTypeEnum.PackNormal:
			{
				PackImgUI.spriteName = data.RewardKey;

				CardUI.gameObject.SetActive (false);
				PackImgUI.gameObject.SetActive (true);
			}
			break;
		    case SoloLeagueRewardDBData.RewardTypeEnum.PackPremium:
			{
				PackImgUI.spriteName = data.RewardKey;

				CardUI.gameObject.SetActive (false);
				PackImgUI.gameObject.SetActive (true);
			}
			break;
		}

		_buttonRankIndex = data.PositionIndex;
	}
}
