﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class ConfirmBox : MonoBehaviour 
{    
    [Header("Frame Yes No")]
    public GameObject FrameYesNo;
    public Text HeaderText_2;
    public Text MessageText_2;
    public Button YesButton;
    public Button NoButton;
    
    [Header("Frame Close")]
    public GameObject FrameClose;
    public Text HeaderText_1;
    public Text MessageText_1;
    public Button CloseButton;

    private UnityAction _onConfirm = null;
    private UnityAction _onCancel = null;

	// Use this for initialization
	void Start () 
    {
	}
	
	// Update is called once per frame
	void Update ()
    {
    }

    public void ShowUI(string title , string message, UnityAction onConfirm, UnityAction onCancel)
    {
        _onConfirm = onConfirm;
        _onCancel = onCancel;
        HeaderText_2.text = title;
        MessageText_2.text = message;
        
        YesButton.onClick.RemoveAllListeners();
        YesButton.onClick.AddListener(this.OnClickOk);
        
        NoButton.onClick.RemoveAllListeners();
        NoButton.onClick.AddListener(this.OnClickCancel);
        
        FrameYesNo.SetActive(true);
        FrameClose.SetActive(false);
        gameObject.SetActive(true);
    }
    
    public void ShowUI(string title , string message, UnityAction onClose)
    {
        _onConfirm = onClose;
        _onCancel = null;
        HeaderText_1.text = title;
        MessageText_1.text = message;
        
        CloseButton.onClick.RemoveAllListeners();
        CloseButton.onClick.AddListener(this.OnClickOk);
        
        FrameYesNo.SetActive(false);
        FrameClose.SetActive(true);
        gameObject.SetActive(true);
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }

    public void OnClickOk()
    {
        HideUI();
        
        if (_onConfirm != null)
        {
            _onConfirm.Invoke();
        }
    }

    public void OnClickCancel()
    {
        HideUI();
        
        if (_onCancel != null)
        {
            _onCancel.Invoke();
        }
    }
}
