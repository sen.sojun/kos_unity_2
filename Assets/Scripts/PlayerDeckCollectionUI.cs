﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeckCollectionUI : MonoBehaviour {

    public AudioClip TitleBGM;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ShowUI()
    {
        //TopBarManager.Instance.SetBackButton(
        //      true
        //    , MenuManagerV2.Instance.ShowMainMenu
        //);

        TopBarManager.Instance.SetUpBar(
             true
            , Localization.Get("MAINMENU_YOURDECK_TITLE") //"Deck & Collection"
           , MenuManagerV2.Instance.ShowMainMenu
       );
         
        if (!SoundManager.IsPlayBGM(TitleBGM))
        {
            SoundManager.PlayBGM(TitleBGM);
        }



        gameObject.SetActive(true);
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }
}
