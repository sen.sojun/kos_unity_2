﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
//using GoogleMobileAds.Api;

public class AdsUI : MonoBehaviour 
{
	private int RewardSilverCoin = 45;
    private bool _isSuccessAds = false;

	public GameObject AdsConfirmUI;
	public AdsMessageUI AdsMessageUI;

	public SilverCoinUI SilverCoinUI;

	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
	}

	public void ShowUI()
	{
		OnShowAdsConfirm ();
		HideAdsMsg ();

		gameObject.SetActive (true);
		BattleUIManager.RequestBringForward (gameObject);
	}

	public void HideUI()
	{
		gameObject.SetActive (false);
	}
		
	public void OnShowAdsConfirm()
	{
		AdsConfirmUI.SetActive (true);
		BattleUIManager.RequestBringForward (AdsConfirmUI);
	}

	public void OnHideAdsConfirm()
	{
		AdsConfirmUI.SetActive (false);
	}

	public void ShowAdsMsg(string text)
	{
		AdsMessageUI.ShowUI (text);
	}

	public void HideAdsMsg()
	{
		AdsMessageUI.HideUI ();
	}

	public void OnConfirmShowAds()
	{
		StartVideoAds ();
	}

	private void StartVideoAds()
	{  
		OnHideAdsConfirm();

		if (MyAdmobManager.IsRewardedVideoReady)
		{
            HideAllPanel();
            MyAdmobManager.ShowRewardedVideo(this.OnVideoAdsComplete);
		} 
		else
        { 
            string logText = Localization.Get("ADS_NOT_READY");
            ShowAdsMsg (logText);
        }
	}
		
	private void OnVideoAdsComplete(bool isSuccess)
	{
        _isSuccessAds = isSuccess;
        StartCoroutine(this.WaitVideoAdsComplete());
    }

    private IEnumerator WaitVideoAdsComplete()
    {
        yield return new WaitForSeconds(1.0f);
        ShowAllPanel();

        if (_isSuccessAds)
        {
            AddRewardSilverCoinByAds(this.RewardSilverCoin);
        }
        else
        {
            string logText = Localization.Get("ADS_NOT_COMPLETE");
            ShowAdsMsg(logText);
        }
    }

    private void AddRewardSilverCoinByAds(int rewardSilverCoin)
	{
		int silverCoin = PlayerSave.GetPlayerSilverCoin ();

		silverCoin += rewardSilverCoin;

		// save add coin.
		PlayerSave.SavePlayerSilverCoin(silverCoin);

		SilverCoinUI.UpdateSilverCoin ();

        string logText = string.Format(
              Localization.Get("ADS_GOT_REWARD")
			, rewardSilverCoin
        );
		ShowAdsMsg (logText);
	}

    private UIPanel[] _panelList = null;  
    private void HideAllPanel()
    {
        // Redraw root.
        _panelList = GameObject.FindObjectsOfType<UIPanel>();
        if (_panelList != null && _panelList.Length > 0)
        {
            foreach (UIPanel panel in _panelList)
            {
                panel.enabled = false;
            }
        }
    }

    private void ShowAllPanel()
    {
        if (_panelList != null && _panelList.Length > 0)
        {
            foreach (UIPanel panel in _panelList)
            {
                panel.enabled = true;
                panel.Refresh();
                panel.RebuildAllDrawCalls();
            }
        }
    }
}
