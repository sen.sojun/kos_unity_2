﻿using UnityEngine;
using System.Collections;

public class ShowPhasePopupUI : MonoBehaviour 
{
	public delegate void OnFinishShowEvent();

	Animator animator;
	AnimationClip clip;

	public UILabel PlayerLabel;
	public UILabel PhaseLabel;

	public UISprite SpriteBG;

	private string _playerBGSpriteName = "change_phase_bg_player";
	private string _enemyBGSpriteName = "change_phase_bg_enemy";

	private OnFinishShowEvent _OnFinishShow = null;

	// Use this for initialization
	void Start () 
	{
		//NGUITools.SetActive(gameObject, false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ShowUI(bool isMe, DisplayBattlePhase displayBattlePhase, OnFinishShowEvent onFinishShowCallback)
	{
		/*
		switch(displayBattlePhase)
		{
			case DisplayBattlePhase.GameSetup:
			{
				PhaseLabel.text = "GAME SETUP PHASE";
			}
			break;

			case DisplayBattlePhase.Begin:
			{
				PhaseLabel.text = "BEGIN PHASE";
			}
			break;

			case DisplayBattlePhase.Draw:
			{
				PhaseLabel.text = "DRAW PHASE";
			}
			break;

			case DisplayBattlePhase.PreBattle:
			{
				PhaseLabel.text = "PRE BATTLE STEP";
			}
			break;

			case DisplayBattlePhase.Battle:
			{
				PhaseLabel.text = "BATTLE STEP";
			}
			break;

			case DisplayBattlePhase.PostBattle:
			{
				PhaseLabel.text = "POST BATTLE STEP";
			}
			break;

			case DisplayBattlePhase.End:
			{
				PhaseLabel.text = "END PHASE";
			}
			break;
		}

		PhaseLabel.text = "END PHASE";
		*/

		_OnFinishShow = onFinishShowCallback;

		if(isMe)
		{
			SpriteBG.spriteName = _playerBGSpriteName;

		}
		else
		{
			SpriteBG.spriteName = _enemyBGSpriteName;
		}

		if(BattleManager.Instance != null && displayBattlePhase != DisplayBattlePhase.GameSetup)
		{
			IBattleControlController player = BattleManager.Instance.GetPlayer(BattleManager.Instance.CurrentActivePlayerIndex);
			if(player != null)
			{
				//PlayerLabel.text = player.GetPlayerBattleData().Name.ToUpper() + "'S TURN";

				string phaseText = "";

				phaseText = string.Format (Localization.Get ("BATTLE_PHASE_SWITCH_PLAYER")
					, player.GetPlayerBattleData().Name.ToUpper()
				);
					
				PhaseLabel.text = phaseText;
			}
			else
			{
				//PlayerLabel.text = "PHASE";
			}
		}
		else
		{
			//PlayerLabel.text = "PHASE";
		}

		NGUITools.SetActive(gameObject, true);
		BattleUIManager.RequestBringForward(gameObject, true);
	}

	public void OnFinishShow()
	{
		//Debug.Log("Finished show");
		NGUITools.SetActive(gameObject, false);

		if(_OnFinishShow != null)
		{
			_OnFinishShow.Invoke();
		}
	}
}
