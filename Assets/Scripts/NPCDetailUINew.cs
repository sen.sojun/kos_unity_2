﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class NPCDetailUINew : MonoBehaviour
{
    
	public MenuManagerV2 MenuManager;
    public Image NPCImage;
    public Text NameLabel;
    public Text DeckStyleLabel;
    public Text WinLabel;
    public Text DescriptionLabel;
    public Text RewardLabel;

    public Button StartButton;
    public Button CloseButton;

    private NPCData _npcData;
    public bool IsPlayFirstTime { get { return (MenuManager.ForcePlayFirstTime || PlayerSave.IsPlayerFirstTime()); } }

    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ShowUI(string npcID)
    {
        MenuManagerV2.Instance.ShowNPCDetailUINew();
        TopBarManager.Instance.SetUpBar(
              true
            , Localization.Get("MAINMENU_SELECT_ENEMY") //"SELECT ENEMY"
            , MenuManagerV2.Instance.ShowSinglePlayerUI);


        _npcData = new NPCData(npcID);
        //Debug.Log(_npcData.NPCID);

        string name = "";
        string description = "";
        string deckStyle = "";

        string reward_head = Localization.Get("NPC_REWARD_HEADER");
        string reward_card_detail = Localization.Get("NPC_REWARD_CARD_DETAIL");
        string reward_card_silver = Localization.Get("NPC_REWARD_SILVER");
        string reward_detail = Localization.Get("NPC_REWARD_DETAIL");
        string reward_card_rare = "";

        switch (LocalizationManager.CurrentLanguage)
        {
            case LocalizationManager.Language.Thai:
                {
                    name = _npcData.Name_TH;
                    description = _npcData.Description_TH;
                    deckStyle = _npcData.DeckStyle_TH;
                }
                break;

            case LocalizationManager.Language.English:
            default:
                {
                    name = _npcData.Name_EN;
                    description = _npcData.Description_EN;
                    deckStyle = _npcData.DeckStyle_EN;
                }
                break;
        }

        switch(_npcData.NPCID)
        {
            case "NPC0001":
                {
                    reward_card_rare = Localization.Get("NPC_REWARD_DAVOX");
                }
                break;

            case "NPC0002":
                {
                    reward_card_rare = Localization.Get("NPC_REWARD_ETHAN");
                }
                break;

            case "NPC0003":
                {
                    reward_card_rare = Localization.Get("NPC_REWARD_LAKURAS");
                }
                break;

            case "NPC0004":
                {
                    reward_card_rare = Localization.Get("NPC_REWARD_XERAPH");
                }
                break;

            case "NPC0005":
                {
                    reward_card_rare = Localization.Get("NPC_REWARD_MARRON");
                }
                break;

            case "NPC0006":
                {
                    reward_card_rare = Localization.Get("NPC_REWARD_KANO");
                }
                break;

        }


        RewardLabel.text = reward_head + '\n' + reward_card_detail + '\n' + reward_card_silver + '\n' + reward_detail + '\n' + reward_card_rare;

        NameLabel.text = name;
        DeckStyleLabel.text = deckStyle;


		Debug.Log("image name = " + _npcData.ImageName);
		string imagename = "Images/Avatars/Campaign/"  + _npcData.ImageName;
 
		Texture imageTexture = Resources.Load(imagename, typeof(Texture)) as Texture;
        if (imageTexture != null)
		{
			Texture2D texture2D = imageTexture as Texture2D;
            Sprite mySprite = Sprite.Create(texture2D, new Rect(0.0f, 0.0f, texture2D.width, texture2D.height), new Vector2(0.5f, 0.5f), 100.0f);

            NPCImage.sprite = mySprite;
			//NPCButton.image.sprite = mySprite;
		}
        else
		{
			Debug.LogError(
                           string.Format(
                                 "NPCDetailUINew Image not found! ({0}"
                            , imagename
                           )
                       );
		}
		        
        //DiffLabel.text = deckStyle;

        WinLabel.text = Localization.Get("NPC_DETAIL_WIN") + ": " + PlayerSave.GetWinCount(_npcData.NPCID).ToString() + " " + Localization.Get("NPC_DETAIL_WIN_TIMES");
        DescriptionLabel.text = description;

        //hide close button if play first time
        //CloseButton.gameObject.SetActive(!IsPlayFirstTime);

        gameObject.SetActive(true);
        //BattleUIManager.RequestBringForward(gameObject);
    }

    #region LoadingUI Methods
    public void ShowLoading()
    {
        MenuManagerV2.Instance.ShowLoading();
    }

    public void HideLoading()
    {
        MenuManagerV2.Instance.HideLoading();
    }
    #endregion

    public void OnStartClicked()
    {
        ShowLoading();
        MatchMakingManager mmm = GameObject.FindObjectOfType<MatchMakingManager>();

        bool isPlayFirstTime = (MenuManager.ForcePlayFirstTime || PlayerSave.IsPlayerFirstTime());
        if (IsPlayFirstTime)
        {
            bool isSuccess = mmm.CreateTutorialData(_npcData);
            if (isSuccess)
            {
                // Load BattleScene
                PlayerSave.SavePlayFromSoloLeague(false); //+vit13052017
                KOSNetwork.NetworkSetting.IsNetworkEnabled = false;
                MenuManager.LoadScene("NetworkBattleScene");
            }
        }
        else
        {
            bool isSuccess = mmm.CreateVSBotData(_npcData, 1);
            if (isSuccess)
            {
                // Load BattleScene
                PlayerSave.SavePlayFromSoloLeague(false); //+vit13052017
                KOSNetwork.NetworkSetting.IsNetworkEnabled = false;
                MenuManagerV2.Instance.LoadScene("NetworkBattleScene");
            }
        }
    }
    
    //public void ShowNPCReward()
    //{
    //    //Debug.Log(_npcData.NPCID);
    //    MenuManager.ShowNPCReward(_npcData.NPCID);
    //}

    public void HideUI()
    {
        gameObject.SetActive(false);
    }
}


