﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Firebase.Auth;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using UnityEngine.UI;
using UnityEngine.Purchasing;

public class ShopPackUI : MonoBehaviour 
{
    #region public variables
    public ConfirmBuyCardUI ConfirmPurchaseUI;
    public OpenPackUI ShopCardResultUI;

    public int NormalPrice = 150; // 150 silver
    public int GoldPrice = 15; //15 gold
    #endregion

    // Use this for initialization
    static Dictionary<string, int> card = new Dictionary<string, int>();
    private Dictionary<string, ShopPackData> _shopList;
    StashFirebaseData stashdb;
    private string userID;
    private int _shopPackID; //1 = silver ,2 gold
    private int currentSilverCoin = 0;
    private int currentGoldCoin = 0;
    private int _silverToPaid = 0;
    private int _goldToPaid = 0;

	// Use this for initialization
	void Start () 
    {
        _shopList = new Dictionary<string, ShopPackData>();
        _shopList.Add("SP0001", new ShopPackData("SP0001"));

        userID = PlayerSave.GetPlayerUid();
        stashdb = new StashFirebaseData(userID, card);

        //Debug.Log("Start Shop uid = " + userID);
	}
	
	// Update is called once per frame
	void Update () 
    {	
	}

    public void ShowUI()
    {
        TopBarManager.Instance.SetUpBar(
              true
            , Localization.Get("SHOP_OPEN_SHOP_PACK") //"SHOP PACK"
            , MenuManagerV2.Instance.ShowShopUI
        );
        
        gameObject.SetActive(true);
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }

    public void HideConfirmBuyCard()
    {
        ConfirmPurchaseUI.HideUI();
    }

    public void BuyStarterSet_5()
    {
        currentSilverCoin = PlayerSave.GetPlayerSilverCoin();
        if (currentSilverCoin >= NormalPrice)
        {
            if(PlayerSave.GetPlayerAccountType() == AccountType.KOS)
            {
                string descText;
                string headText;
                descText = string.Format(Localization.Get("SHOP_CONFIRMATION_DESCRIPTION"));
                headText = string.Format(Localization.Get("SHOP_CONFIRMATION_HEADER"));
    
                ConfirmPurchaseUI.ShowUI(
                      headText
                    , descText
                    , this.ConfirmBuyStarterSet_5
                    , ConfirmPurchaseUI.HideUI
                    , true
                    , true
                    , false
                );
            }
            else
            {
                ConfirmPurchaseUI.HideUI();
                SetShowNotEnoughUI(Localization.Get("SHOP_PLEASE_LOGIN"));
            }
        }
        else
        {
            ConfirmPurchaseUI.HideUI();
            SetShowNotEnoughUI(Localization.Get("SHOP_NOT_ENOUGH_DESCRIPTION"));
        }

        //SoundManager.PlayEFXSound (OpenPackSFX);
    }

    private void SetShowNotEnoughUI(string desc)
    {
        string descText;
        string headText;
        descText = string.Format(desc);
        headText = string.Format(Localization.Get("SHOP_CONFIRMATION_HEADER"));

        ConfirmPurchaseUI.ShowUI(
              headText
            , descText
            , this.ConfirmBuyStarterSet_5
            , ConfirmPurchaseUI.HideUI
            , false
            , false
            , true
        );

        //NotEnoughUI.SetActive(isShow);
        //if (isShow)
        //{
        //    BattleUIManager.RequestBringForward(NotEnoughUI);
        //}
    }

    public void ConfirmBuyStarterSet_5()
    {
        BuyStarterSet(NormalPrice);
    }

    private void BuyStarterSet(int iSilverToPaid)
    {
        currentSilverCoin = PlayerSave.GetPlayerSilverCoin();
        _silverToPaid = iSilverToPaid;
        
        Debug.Log(string.Format("BuyPremiumSet: price={0} current={1}", _goldToPaid, currentGoldCoin));

        if (currentSilverCoin >= iSilverToPaid)
        {
            //currentSilverCoin -= iSilverToPaid;

            //// Save coin.
            //PlayerSave.SavePlayerSilverCoin(currentSilverCoin);  //comment when done
            //TopBarManager.Instance.UpdateSilverAmount(currentSilverCoin); //comment when done
            ////SilverCoinUI.UpdateSilverCoin();

            //List<string> resultCardList; //comment when done
            //RandomNormal("SP0001", out resultCardList); //comment when done
            //DebugPrintText (resultCardList);

            // uncomment 2 lines below when fin test
            _shopPackID = 1;
            BuyShopServer(_shopPackID);
        }
        else
        {
            //Debug.Log("Not enough silver coin.");
            
            ConfirmPurchaseUI.HideUI();
            SetShowNotEnoughUI(Localization.Get("SHOP_NOT_ENOUGH_DESCRIPTION"));
        }
    }

    private void BuyShopServer(int shopPackID)
    {
        // TODO: SHOW LOADING UI         
        KOSServer.RequestBuyShopPack(shopPackID, OnBuyShopServerComplete);
    }

    private void OnBuyShopServerComplete(bool isSuccess, string result)
    {
        Debug.Log("OnBuyShopServerComplete : " + isSuccess + " " + result);

        if(isSuccess)
        {
            string[] response = result.Split(' ');
           
            if(response[0] == "1")
            {                
                // Request Buy Successful
                if (_shopPackID == 1)
                {
                    //Debug.Log("will deduct  ");
                    currentSilverCoin -= _silverToPaid;

                    // Save coin.
                    PlayerSave.SavePlayerSilverCoin(currentSilverCoin);
                    TopBarManager.Instance.UpdateSilverAmount(currentSilverCoin);
                }
                else
                {
                    currentGoldCoin -= _goldToPaid;

                    // Save coin.
                    PlayerSave.SavePlayerGoldCoin(currentGoldCoin);
                    TopBarManager.Instance.UpdateGoldAmount(currentGoldCoin);
                }

                ConfirmPurchaseUI.HideUI();
                SetShowCardResultUI(true);

                string[] cardIDList = response[1].Split(',');

                int index = 0;

                // Add new card to stash.
                foreach (string id in cardIDList)
                {
                    PlayerCardData card = new PlayerCardData(new CardData(id));

                    // add new card to stash
                    bool isAddSuccess = AddCardToStash(card.PlayerCardID);
                    if (isAddSuccess)
                    {
                        bool isNew = false;
                        if (PlayerSave.SavePlayerCardCollection(card.PlayerCardID))
                        {
                            // New card
                            isNew = true;
                        }

                        bool isGlow = false;
                        if ((card.SymbolData == CardSymbolData.CardSymbol.Rare) ||
                            (card.SymbolData == CardSymbolData.CardSymbol.SuperRare) ||
                            (card.SymbolData == CardSymbolData.CardSymbol.Forbidden))
                        {
                            isGlow = true;
                        }

                        ShopCardResultUI.ShowCard(
                              index
                            , new PlayerCardData(card)
                            , isGlow
                            , isNew
                        );
                    }
                    ++index;
                }

                return;
            }
            else
            {
                // Buy Failed
                //Debug.Log("buy failed  ");
                
                ConfirmPurchaseUI.HideUI();
                SetShowNotEnoughUI(Localization.Get("CONNECT_SERVER_FAILED"));

                // TO DO: reload player data.
            }
        }
        else
        {
            // Unknown error or timeout
            ConfirmPurchaseUI.HideUI();
            SetShowNotEnoughUI(Localization.Get("CONNECT_SERVER_TIMEOUT"));
        }
    }

    /*
    private void RandomNormal(string shopPackID, out List<string> resultList)
    {
        ShopPackData shop = null;
        bool isFound = _shopList.TryGetValue(shopPackID, out shop);
        if (isFound)
        {
            ConfirmPurchaseUI.HideUI();
            SetShowCardResultUI(true);

            shop.RandomNormal(out resultList);
            int index = 0;
            foreach (string id in resultList)
            {
                PlayerCardData card = new PlayerCardData(new CardData(id));

                // add new card to stash
                bool isSuccess = AddCardToStash(card.PlayerCardID);
                if (isSuccess)
                {
                    SaveCardToDB(card.PlayerCardID);
                    bool isNew = false;
                    if (PlayerSave.SavePlayerCardCollection(card.PlayerCardID))
                    {
                        // New card
                        isNew = true;
                    }

                    bool isGlow = false;
                    if ((card.SymbolData == CardSymbolData.CardSymbol.Rare) ||
                        (card.SymbolData == CardSymbolData.CardSymbol.SuperRare) ||
                        (card.SymbolData == CardSymbolData.CardSymbol.Forbidden))
                    {
                        isGlow = true;
                    }

                    ShopCardResultUI.ShowCard(index,
                              new PlayerCardData(card),
                                              isGlow, isNew);
                             //, this.OnClickedPreview);
                }
                ++index;
            }

            return;
        }

        resultList = null;
    }
   */

    /*
    private void RandomPremium(string shopPackID, out List<string> resultList)
    {
        ShopPackData shop = null;
        bool isFound = _shopList.TryGetValue (shopPackID, out shop);
        if (isFound) 
        {
            ConfirmPurchaseUI.HideUI();
            SetShowCardResultUI (true);

            shop.RandomPremium(out resultList);
            int index = 0;
            foreach (string id in resultList) 
            {
                PlayerCardData card = new PlayerCardData (new CardData(id));

                // add new card to stash
                bool isSuccess = AddCardToStash(card.PlayerCardID);
                if(isSuccess)
                {
                    bool isNew = false;
                    if (PlayerSave.SavePlayerCardCollection(card.PlayerCardID)) 
                    {
                        // New card
                        isNew = true;
                    } 

                    bool isGlow = false;
                    if ( (card.SymbolData == CardSymbolData.CardSymbol.Rare) ||
                        (card.SymbolData == CardSymbolData.CardSymbol.SuperRare) ||
                        (card.SymbolData == CardSymbolData.CardSymbol.Forbidden)) 
                    {
                        isGlow = true;
                    }

                    ShopCardResultUI.ShowCard(index,
                                               new PlayerCardData(card),
                                               isGlow, isNew);
                                              // ,this.OnClickedPreview);
                }
                ++index;
            }

            return;
        }

        resultList = null;
    }
    */

    public void BuyPremiumSet_5()
    {
        currentGoldCoin = PlayerSave.GetPlayerGoldCoin();

        if (currentGoldCoin >= GoldPrice)
        {
            if(PlayerSave.GetPlayerAccountType() == AccountType.KOS)
            {
                string descText;
                string headText;
                descText = string.Format(Localization.Get("SHOP_PREMIUM_CONFIRMATION_DESCRIPTION"));
                headText = string.Format(Localization.Get("SHOP_CONFIRMATION_HEADER"));
    
                ConfirmPurchaseUI.ShowUI(
                      headText
                    , descText
                    , this.ConfirmBuyPremiumSet_5
                    , ConfirmPurchaseUI.HideUI
                    , true
                    , true
                    , false
                );
            }
            else
            {
                ConfirmPurchaseUI.HideUI();
                SetShowNotEnoughUI(Localization.Get("SHOP_PLEASE_LOGIN"));
            }
        }
        else
        {
            ConfirmPurchaseUI.HideUI();
            SetShowNotEnoughUI(Localization.Get("SHOP_NOT_ENOUGH_GOLD_DESCRIPTION"));
        }

        //SoundManager.PlayEFXSound (OpenPackSFX);
    }

    public void ConfirmBuyPremiumSet_5()
    {
        BuyPremiumSet(GoldPrice);
    }

    private void BuyPremiumSet(int iGoldToPaid)
    {
        currentGoldCoin = PlayerSave.GetPlayerGoldCoin();
        _goldToPaid = iGoldToPaid;
        
        Debug.Log(string.Format("BuyPremiumSet: price={0} current={1}", _goldToPaid, currentGoldCoin));

        if (currentGoldCoin >= iGoldToPaid)
        {
            //currentGoldCoin -= iGoldToPaid;

            //// Save coin.
            //PlayerSave.SavePlayerGoldCoin(currentGoldCoin);
            //TopBarManager.Instance.UpdateGoldAmount(currentGoldCoin);

            //List<string> resultCardList;
            //RandomNormal("SP0001", out resultCardList);
            //RandomPremium("SP0001", out resultCardList);
            //DebugPrintText (resultCardList);
            
            _shopPackID = 2;
            BuyShopServer(_shopPackID);
        }
        else
        {
            //Debug.Log("Not enough silver coin.");
            ConfirmPurchaseUI.HideUI();
            SetShowNotEnoughUI(Localization.Get("SHOP_NOT_ENOUGH_GOLD_DESCRIPTION"));
        }
    }
        
    void OnClickedPreview(FullCardUI fullCardUI)
    {
        Debug.Log("Name:" + fullCardUI.PlayerCardData.Name_EN);

        //ShopPreviewCardUI.ShowUI(fullCardUI.PlayerCardData);
    }

    private bool AddCardToStash(string playerCardID)
    {
        List<PlayerCardData> stash;
        bool isSuccess = PlayerSave.GetPlayerCardStash(out stash);
       
        if (isSuccess)
        {
            stash.Add(new PlayerCardData(playerCardID));
            PlayerSave.SavePlayerCardStash(stash);

            return true;
        }

        return false;
    }

    private void SaveCardToDB(string cardID)
    {
        bool foundCard = false;
        int currQty = 1;
        Debug.Log("on save stash cashed");
        try
        {
            Debug.Log("on stash cashed");
            Debug.Log("on stash cashed successfully");

            stashdb = KOSServer.Instance.StashData;

            Debug.Log("stashDB");


            Debug.Log("uid =  " + stashdb.UID);

            foundCard = stashdb.Card.ContainsKey(cardID);


            if (foundCard)
            {
                currQty = stashdb.Card[cardID];
                ++currQty;
            }
            else
            {
                stashdb.Card.Add(cardID, 1);
            }

            KOSServer.UpdateStashData(stashdb, cardID, currQty);
        }

        catch (SystemException e)
        {
            Debug.Log("cash stash erro " + e.Message);
        }
    }

    private void SetShowCardResultUI(bool isShow)
    {
        if (isShow)
        {
            ShopCardResultUI.ShowUI("5");
        }
        else
        {
            ShopCardResultUI.HideUI();
        }
    }

}
