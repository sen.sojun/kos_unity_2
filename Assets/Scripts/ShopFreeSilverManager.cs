﻿using UnityEngine;
using System.Collections;

public class ShopFreeSilverManager : MonoBehaviour {

	public UIButton WatchVideoButton;
	public UIButton FaceBookButton;
	public UIButton LikeKosPageButton;
	public UIButton TwitterButton;
	private int iCurrentSilverCoin;



	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ShowCurrentCoin(int iCurrentCoin)
	{
		iCurrentSilverCoin = iCurrentCoin;  
	}

	public void WatchVideo()
	{
		iCurrentSilverCoin += 50;
		SavePlayerSilverCoin ();
	}

	public void FaceBook()
	{
		iCurrentSilverCoin += 50;
		SavePlayerSilverCoin ();
	}

	public void LikeKosPage()
	{
		iCurrentSilverCoin += 100;
		SavePlayerSilverCoin ();
	}

	public void TwitterShare()
	{
		iCurrentSilverCoin += 100;
		SavePlayerSilverCoin ();
	} 

	public void SavePlayerSilverCoin()
	{
		PlayerSave.SavePlayerSilverCoin (iCurrentSilverCoin);
	}


}

