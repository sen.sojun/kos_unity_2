﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using GameAnalyticsSDK;

public class CardCollectionSceneManager : MonoBehaviour 
{
	public int MaxCardPerPage = 8;
	public AudioClip BGMClip;

	public List<FullCardUI> FullCardList;
	public List<GameObject> BackCardList;

	public UIButton NextButton;
	public UIButton PrevButton;

	public UILabel PageLabel;
	public UILabel PercentLabel;
	//public BattleCardUI BattleCard;

	public GameObject ShowCardUIView;//added 30062016 vit
	public FullCardUI FullCard_L;    //added 30062016 vit
	public LoadingUI LoadingUI;

	//private GAProgressionStatus progressionStart = GAProgressionStatus.Start;
	//private GAProgressionStatus progressionComplete = GAProgressionStatus.Complete;

	private float startTime;
	private float endTime;
	private float totalTime;

	//public UILabel IndexLabel;
	private List<CardDBData> _database;
	private Dictionary<string, CardData> _myCollection; //<LIST> -> array like ,with memory allocation
	private int _maxPageIndex = 0;

	private int __pageIndex = 0;
	private int _pageIndex
	{
		get { return __pageIndex; }
		set { 
			__pageIndex = value;

			if (__pageIndex <= 0) { 
				// first page
				PrevButton.gameObject.SetActive (false);				
			} else {
				PrevButton.gameObject.SetActive (true);
			}

			if (__pageIndex >= _maxPageIndex) { 
				// last page
				NextButton.gameObject.SetActive (false);
			} else {
				NextButton.gameObject.SetActive (true);
			}

			ShowCurrentPage ();
		}
	}


	// Use this for initialization
	void Start () 
	{
		if(!SoundManager.IsPlayBGM(BGMClip))
		{
			SoundManager.PlayBGM(BGMClip);
		}
			
		//start counting time
		startTime = Time.time;

		//GameAnalytics.NewProgressionEvent(progressionStart,"Start Card Collection",30);
		//PlayerSave.GenerateDummySave (); //generate new dummy card for initial run

		//GameAnalytics.NewDesignEvent ("Achievement:StartCardCollection", 1);
		foreach (FullCardUI fullCardUI in FullCardList) 
		{
			fullCardUI.BindOnClickCallback (this.OnClickedPreview);
		}

		//LoadingUI.ShowUI();
		LoadCollectionData ();
		LoadingUI.HideUI();
	}

	void LoadCollectionData()
	{
		List<CardData> myCollection = new List<CardData> ();
		bool isSuccess = PlayerSave.GetPlayerCardCollection (out myCollection);

		if (isSuccess) 
		{
			_myCollection = new Dictionary<string, CardData> ();
			foreach (CardData data in myCollection) 
			{
				_myCollection.Add (data.CardID, data);
				//GameAnalytics.NewProgressionEvent(progressionStart,"Card Collection",30);

			}
		} 
		else 
		{
			//Debug.LogError("CardCollectionSceneManager/LoadCollectionData: No save player collection!!");
			//return;
		}

		isSuccess = CardDB.GatAllData(out _database);
		if (isSuccess) 
		{
			_maxPageIndex = Mathf.CeilToInt((float)_database.Count / (float)MaxCardPerPage) - 1;
			_pageIndex = 0;
		} 
		else 
		{
			Debug.LogError("CardCollectionSceneManager/LoadCollectionData: can't load CardDBData!!");
			return;
		}
	}

	void ShowCurrentPage()
	{
		int startIndex = _pageIndex * MaxCardPerPage;
		int stopIndex = startIndex + (MaxCardPerPage - 1);

		for (int index = startIndex; index <= stopIndex; ++index) {
			CardData cardData;

			if (index < _database.Count) {
				if (_myCollection.TryGetValue (_database [index].CardID, out cardData)) {
					// Found data
					FullCardList [index - startIndex].SetCardData (new PlayerCardData (cardData));

					FullCardList [index - startIndex].gameObject.SetActive (true);
					BackCardList [index - startIndex].SetActive (false);
				} else {
					// not found
					FullCardList [index - startIndex].gameObject.SetActive (false);
					BackCardList [index - startIndex].SetActive (true);

				}
			} else {
				// last page
				FullCardList [index - startIndex].gameObject.SetActive (false);
				BackCardList [index - startIndex].SetActive (true);

			}
		}

		PageLabel.text = (_pageIndex + 1).ToString() + "/" + (_maxPageIndex + 1).ToString ();

		PercentLabel.text = Localization.Get("COLLECTION_PROGESS") + " " + (((float)_myCollection.Count / (float)_database.Count) * 100.0f).ToString ("0.0") + " %";
	}

	// Update is called once per frame
	void Update () 
	{
		//Debug.Log ("update card Tutorial");
		//GameAnalytics.NewDesignEvent ("Update Card Collection Event", 2.0f);

	}

	public void ShowNextPage()
	{
		//Debug.Log("Show Next Card");
		//GameAnalytics.NewDesignEvent ("Next Page");
		++_pageIndex;
	}

	public void ShowPrevPage()
	{
		//Debug.Log("Show Prev Card");
		//GameAnalytics.NewDesignEvent ("Prev Page");
		--_pageIndex;
	}

	void OnClickedPreview(FullCardUI fullCardUI)
	{
		//Debug.Log ("Name:" + fullCardUI.PlayerCardData.Name_EN);
		//GameAnalytics.NewProgressionEvent(progressionStart,"Start Card Collection",30);
		//GameAnalytics.print ("From Analytics!");
		//GameAnalytics.NewDesignEvent ("PreviewCard");
		//GameAnalytics.SetGender (GAGender.Male);


		FullCard_L.SetCardData(fullCardUI.PlayerCardData);
		ShowCardUIView.SetActive (true);

		BattleUIManager.RequestBringForward (ShowCardUIView.gameObject);
	}

	public void BackToMenu()
	{
		//--start of insertion vit04102016
		endTime = Time.time;
		totalTime = ( endTime - startTime ) / 60;
		GameAnalytics.NewDesignEvent ("Achievement:EndCardCollection", totalTime);


		LoadingUI.ShowUI();

		//SceneManager.LoadSceneAsync("MainMenuScene");
        SceneManager.LoadSceneAsync("MainMenuScene2");
	}
}
