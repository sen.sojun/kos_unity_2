﻿using UnityEngine;
using System.Collections;

public class LocalizationManager : MonoBehaviour 
{
	public enum Language : int
	{
		  English = 0
		, Thai = 1
	}

	//public TextAsset LocalizationMenuDB;
	private static bool _IsLoad = false;
	public static bool IsLoad { get { return _IsLoad; } }

	void Awake()
	{
		if (!LocalizationManager.IsLoad)
		{
			LocalizationManager.Init();
		}
	}

	// Use this for initialization
	/*
	void Start () 
	{
    }
    */

	/*
	// Update is called once per frame
	void Update () 
	{
	}
	*/

	public static void Init()
	{
        if (!IsLoad)
        {
            TextAsset csvFile = Resources.Load<TextAsset>("Databases/KOS-CardDB - LocalizationDB");
            Localization.LoadCSV(csvFile);
            Resources.UnloadAsset(csvFile);

            Language currentLanguage = PlayerSave.GetLanguage();
            //Debug.Log(currentLanguage);
            LocalizationManager.SetLanguage(currentLanguage, false);

            _IsLoad = true;
        }
	}

	public static Language CurrentLanguage
	{
		get 
		{
			foreach(Language language in System.Enum.GetValues(typeof(Language)))
			{
				if(IsCurrentLanguage(language))
				{
					return language;
				}
			}
				
			return Language.English;
		}
	}

	public static bool IsCurrentLanguage(Language language)
	{
		switch(language)
		{
			case Language.English: return (string.Compare(Localization.language, "English") == 0);
			case Language.Thai: return (string.Compare(Localization.language, "Thai") == 0);
		}

		return false;
	}

	public static void SetLanguage(Language language, bool isSave = true)
	{
        //Debug.Log("SetLanguage: " + language);
        switch (language)
		{
			case Language.Thai: 
			{
				Localization.language = "Thai";
			}
			break;

			case Language.English:
			default:
			{
				Localization.language = "English";
			}
			break;
		}

        if(isSave)
        {
            PlayerSave.SaveLanguage(language);
        }

        //Localization.onLocalize ();
    }
    
    public static void ToggleLanguage()
    {    
        switch(CurrentLanguage)
        {
            case Language.Thai:     SetLanguage(Language.English); break;
            case Language.English:  SetLanguage(Language.Thai); break;
        }
        
        MonoBehaviour[] objects = (MonoBehaviour[])GameObject.FindObjectsOfType(typeof(MonoBehaviour));
        foreach(MonoBehaviour obj in objects)
        {
            if (obj is ILocalize)
            {
                ILocalize localize = (ILocalize)obj;
                localize.OnUpdateLanguage();
            }
        }
    }
    
    public static string GetText(string key)
    {
        if(!IsLoad)
        {
            Init();
        }
        
        return Localization.Get(key);
    }
 
	public static string CardZoneToText(CardZoneIndex cardZone)
	{
		switch(cardZone)
		{
			case CardZoneIndex.Deck: return Localization.Get ("CARDZONE_DECK");
			case CardZoneIndex.Hand: return Localization.Get ("CARDZONE_HAND");
			case CardZoneIndex.Graveyard: return Localization.Get ("CARDZONE_GRAVEYARD");
			case CardZoneIndex.RemoveZone: return Localization.Get ("CARDZONE_REMOVE_ZONE");
		}
		return "";
	}

	public static string LocalBattleZoneToText(LocalBattleZoneIndex localBattleZone)
	{
		switch(localBattleZone)
		{
			case LocalBattleZoneIndex.PlayerBase: return Localization.Get ("LOCAL_BATTLEZONE_PLAYER_BASE");
			case LocalBattleZoneIndex.Battlefield: return Localization.Get ("LOCAL_BATTLEZONE_BATTLEFIELD");
			case LocalBattleZoneIndex.EnemyBase: return Localization.Get ("LOCAL_BATTLEZONE_ENEMY_BASE");
		}
		return "";
	}

	public static string LocalBattleZoneToText(BattleZoneIndex battleZone)
	{
		switch(battleZone)
		{
			case BattleZoneIndex.BaseP1: return Localization.Get ("BATTLEZONE_P1");
			case BattleZoneIndex.Battlefield: return Localization.Get ("BATTLEZONE_BATTLEFIELD");
			case BattleZoneIndex.BaseP2: return Localization.Get ("BATTLEZONE_P2");
		}
		return "";
	}
}
