﻿using UnityEngine;
using System.Collections;

public class YesNoPopupUI : MonoBehaviour {

    public delegate void OnSelectYes();
    public delegate void OnSelectNo();
    public delegate void OnCancel();

    public UILabel TextLabel;
    public UISlider TimeSlider;

    private float _timer = 0.0f;
    private bool _isHaveTimeout = false;
    private bool _isPauseTimeout = false;
    
    private OnSelectYes _onSelectYes;
    private OnSelectNo _onSelectNo;

	// Use this for initialization
	void Start () 
    {
	}
	
	// Update is called once per frame
	void Update () 
    {
        if(_isHaveTimeout)
        {
            _timer -= Time.deltaTime;
             UpdateTimeSlider();
            if(_timer < 0.0f)
            {
                // timeout.                    
                _isHaveTimeout = false;
                AutoSelectYesNo();
            }
        }
	}

    public void ShowUI(string text, OnSelectYes onSelectYes, OnSelectNo onSelectNo, bool isHaveTimeout)
    {
        TextLabel.text = text;

        _onSelectYes = onSelectYes;
        _onSelectNo = onSelectNo;
        
        _isHaveTimeout = (BattleManager.Instance.IsHaveTimeout && isHaveTimeout);
        _timer = BattleManager.Instance.PopupDecisionTime;   
        _isPauseTimeout = _isHaveTimeout;
        if(_isPauseTimeout)
        {
            if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
            {
                KOSNetwork.GamePlayer.localPlayer.RequestPauseTimer(true, BattleManager.Instance.TurnTimer);
            }
            else
            {
                BattleManager.Instance.SetPauseTimeout(true);
            }
            TimeSlider.gameObject.SetActive(true);
        }
        else
        {
            TimeSlider.gameObject.SetActive(false);
        }

        NGUITools.SetActive(gameObject, true);
        BattleUIManager.RequestBringForward(gameObject, true);

		UIWidget widget = gameObject.GetComponent<UIWidget>();
		if(widget)
		{
			NGUITools.AdjustDepth(widget.panel.gameObject, 1001);
		}
    }

    public void OnClickYes()
    {
		HideUI();

        if (_onSelectYes != null)
        {
            _onSelectYes.Invoke();
        }
    }

    public void OnClickNo()
    {
        HideUI();

		if (_onSelectNo != null)
		{
			_onSelectNo.Invoke();
		}
    }

    public void HideUI()
    {
        if(_isPauseTimeout)
        {
            if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
            {
                KOSNetwork.GamePlayer.localPlayer.RequestPauseTimer(false, BattleManager.Instance.TurnTimer);
            }
            else
            {
                BattleManager.Instance.SetPauseTimeout(false);
            }
        }
        NGUITools.SetActive(gameObject, false);
    }
    
    public void AutoSelectYesNo()
    {
        Debug.Log("YesNoPopupUI/AutoSelectYesNo: No");
        OnClickNo();
    }
    
    private void UpdateTimeSlider()
    {
        if(TimeSlider != null)
        {
            float ratio = _timer/BattleManager.Instance.PopupDecisionTime;
            
            TimeSlider.value = Mathf.Clamp(ratio, 0.0f, 1.0f);
        }
    }
}
