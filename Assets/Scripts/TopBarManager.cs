﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class TopBarManager : MonoBehaviour 
{
    
    private static TopBarManager _instance = null;
    public static TopBarManager Instance { get { return _instance; } }

    #region Public
    public Text GoldCoinLabel;
    public Text SilverCoinLabel;
    public Text PlayerName;
    public Text MenuTitle;
    public Button BackToPreviousMenu;
    public Button PlayerProfileButton;
    public Button SilverCoinButton;
    public Button GoldCoinButton;
    public PlayerProfileUI playerProfileUI;
    public PlayerAvatarUI playerAvatar;
    public PlayerGoldUI playerGoldUI;
    public PlayerSilverUI playerSilverUI;
    #endregion

    private GameObject currentUI;
    public bool isShowUI = false;
    public bool isShowAvatar = false;
    public bool isShowGoldUI = false;
    public bool isShowSilverUI = false;
    public bool isFromMenu = true;
    public bool isMustSaveDeckEditor = false;

    private AccountType _accType;
    private string _currentTitleBar = "";

    //public enum menuType
    //{
    //    Mainmenu = 1,
    //    PlayerProfile,
    //    SingleBattle,
    //    Multiplayer,
    //    DeckEditor,
    //    CardCollection,
    //}

    private void Awake()
    {
        _instance = this;
    }

    // Use this for initialization
    void Start () 
    {
        _accType = PlayerSave.GetPlayerAccountType();
        string playerName = PlayerSave.GetPlayerName(); //KOSServer.GetNewPlayerNameUniqueID();
        Debug.Log("Player name = " + playerName);
        TopBarManager.Instance.ShowPlayerNameLabel(playerName);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnDestroy()
    {
        _instance = null;
    }

    //public void CheckMenu(menuType checkmenu,string menuName)
    //{
    //    if (checkmenu == menuType.Mainmenu)
    //    {
    //        BackToPreviousMenu.gameObject.SetActive(false);
    //    }
    //    else 
    //    {
    //        BackToPreviousMenu.gameObject.SetActive(true);
    //    }

    //    MenuTitle.text = menuName;

    //}

    public void DisableTopbarButtonButton()
    {
        PlayerProfileButton.gameObject.SetActive(false);
        SilverCoinButton.gameObject.SetActive(false);
        GoldCoinButton.gameObject.SetActive(false);
    }

    public void EnableTopbarButtonButton()
    {
        PlayerProfileButton.gameObject.SetActive(true);
        SilverCoinButton.gameObject.SetActive(true);
        GoldCoinButton.gameObject.SetActive(true);
    }

    public void DisableBackButtonButton()
    {
        BackToPreviousMenu.gameObject.SetActive(false);   
    }

    public void EnableBackButtonButton()
    {
        BackToPreviousMenu.gameObject.SetActive(true);
    }

    public void SetBackButton(bool isShow, UnityAction action = null)
    {
        BackToPreviousMenu.gameObject.SetActive(isShow);

        if(isShow)
        {
            BackToPreviousMenu.onClick.RemoveAllListeners();
            if (action != null)
            {
                BackToPreviousMenu.onClick.AddListener(action);
            }
            
            ButtonSound buttonSound = BackToPreviousMenu.GetComponent<ButtonSound>();
            if(buttonSound)
            {
                buttonSound.Init();
            }
        }
    }

    public void SetUpBar(bool isShowBackButton, string titleBar, UnityAction action = null)
    {    
        SetBackButton(isShowBackButton, action);
    
        BackToPreviousMenu.gameObject.SetActive(isShowBackButton);

        /*
        if (isShowBackButton)
        {
            BackToPreviousMenu.onClick.RemoveAllListeners();
            if (action != null)
            {
                BackToPreviousMenu.onClick.AddListener(action);
            }
            
            ButtonSound buttonSound = BackToPreviousMenu.GetComponent<ButtonSound>();
            if(buttonSound)
            {
                buttonSound.Init();
            }
        }
        */

        MenuTitle.text = titleBar;
    }


    public void SetUpBarFromDeckEditor(bool isShowBackButton, string titleBar, UnityAction action = null,bool isHaveToSaveFirst = false )
    {

        SetBackButton(isShowBackButton, action);

        BackToPreviousMenu.gameObject.SetActive(isShowBackButton);


        MenuTitle.text = titleBar;
    }


    public void TogglePlayerProfile()//bool isShow,UnityAction action = null)
    {
        //if (_accType == "User")
        //{
     

           if (isShowUI)
            {
                isShowUI = false;
                playerAvatar.gameObject.SetActive(false);
                GoldCoinButton.gameObject.SetActive(true);
                SilverCoinButton.gameObject.SetActive(true);

                if (isFromMenu)
                {
                    BackToPreviousMenu.gameObject.SetActive(false);

                }
                else
                {
                    BackToPreviousMenu.gameObject.SetActive(true);

                }
          
            }
            else
            {
                isShowUI = true;
                BackToPreviousMenu.gameObject.SetActive(false);
                GoldCoinButton.gameObject.SetActive(false);
                SilverCoinButton.gameObject.SetActive(false);    
            }

            if (isShowUI)
            {
               _currentTitleBar = MenuTitle.text;  
                playerProfileUI.ShowUI();
                MenuTitle.text = Localization.Get("LOBBY_PLAYER_PROFILE_BUTTON"); //  "PLAYER PROFILE";
            }
            else
            {
                playerProfileUI.HideUI();
                MenuTitle.text = _currentTitleBar;
            }
            Debug.Log("Show Menu = " + isFromMenu);
        //BackToPreviousMenu.gameObject.SetActive(!isFromMenu);

        //BackToPreviousMenu.gameObject.SetActive(!isShowUI);


        //}
        //else
        //{
        //   //not allow to change user profile
        //}
    }

#region Gold UI
    public void ToggleGoldUI()//bool isShow,UnityAction action = null)
    {
        
        if (isShowGoldUI)
        {
            PlayerProfileButton.gameObject.SetActive(true);
            SilverCoinButton.gameObject.SetActive(true);

            isShowGoldUI = false;
            if (isFromMenu)
            {
                BackToPreviousMenu.gameObject.SetActive(false);
            }
            else
            {
                BackToPreviousMenu.gameObject.SetActive(true);
            }
        }
        else
        {
            isShowGoldUI = true;
            BackToPreviousMenu.gameObject.SetActive(false);
            PlayerProfileButton.gameObject.SetActive(false);
            SilverCoinButton.gameObject.SetActive(false);
        }

        if (isShowGoldUI)
        {
            _currentTitleBar = MenuTitle.text;
            playerGoldUI.ShowUI();
            MenuTitle.text = Localization.Get("SHOP_GOLD_TITLE"); // "GOLD SHOP";
        }
        else
        {
            playerGoldUI.HideUI();
            MenuTitle.text = _currentTitleBar;
        }
        Debug.Log("Show Menu = " + isFromMenu);
        //BackToPreviousMenu.gameObject.SetActive(!isFromMenu);
        //BackToPreviousMenu.gameObject.SetActive(!isShowUI);
    }

    //public void OpenGoldFromShop()
    //{
    //    _currentTitleBar = MenuTitle.text;
    //    if (isShowGoldUI)
    //    {
    //        PlayerProfileButton.gameObject.SetActive(true);
    //        SilverCoinButton.gameObject.SetActive(true);

    //        isShowGoldUI = false;
    //        if (isFromMenu)
    //        {
    //            BackToPreviousMenu.gameObject.SetActive(true);
    //        }
    //        else
    //        {
    //            BackToPreviousMenu.gameObject.SetActive(false);
    //        }
    //    }
    //    else
    //    {
    //        isShowGoldUI = true;
    //        BackToPreviousMenu.gameObject.SetActive(false);
    //        PlayerProfileButton.gameObject.SetActive(false);
    //        SilverCoinButton.gameObject.SetActive(false);
    //        GoldCoinButton.gameObject.SetActive(false);
    //    }

    //    if (isShowGoldUI)
    //    {
    //        playerGoldUI.ShowUI();
    //        MenuTitle.text = "GOLD SHOP";
    //    }
    //    else
    //    {
    //        playerGoldUI.HideUI();
    //        MenuTitle.text = _currentTitleBar;
    //    }
    //    Debug.Log("Show Menu = " + isFromMenu);
    //    //BackToPreviousMenu.gameObject.SetActive(!isFromMenu);
    //    //BackToPreviousMenu.gameObject.SetActive(!isShowUI);
    //}

    public void ShowGoldUIFromShop()
    {
        TopBarManager.Instance.SetUpBar(
         true
            , Localization.Get("SHOP_GOLD_TITLE") //"GOLD SHOP"
         , MenuManagerV2.Instance.ShowShopUI
     );
        PlayerProfileButton.gameObject.SetActive(false);
        SilverCoinButton.gameObject.SetActive(false);
        GoldCoinButton.gameObject.SetActive(false);
        playerGoldUI.ShowUI();
    }

    public void HideGoldUIFromShop()
    {
        PlayerProfileButton.gameObject.SetActive(true);
        SilverCoinButton.gameObject.SetActive(true);
        GoldCoinButton.gameObject.SetActive(true);
        playerGoldUI.HideUI();
    }

    #endregion


    #region Silver UI
    public void ToggleSilverUI()
    {
        if (isShowSilverUI)
        {
            PlayerProfileButton.gameObject.SetActive(true);
            GoldCoinButton.gameObject.SetActive(true);

            isShowSilverUI = false;
            if (isFromMenu)
            {
                BackToPreviousMenu.gameObject.SetActive(false);

            }
            else
            {
                BackToPreviousMenu.gameObject.SetActive(true);

            }

        }
        else
        {
            isShowSilverUI = true;
            BackToPreviousMenu.gameObject.SetActive(false);
            PlayerProfileButton.gameObject.SetActive(false);
            GoldCoinButton.gameObject.SetActive(false);
        }

        if (isShowSilverUI)
        {
            playerSilverUI.ShowUI();
        }
        else
        {
            playerSilverUI.HideUI();
        }
        Debug.Log("Show Menu = " + isFromMenu);
    }

    #endregion

    public void OpenAvatar()//bool isShow,UnityAction action = null)
    {
        if (isShowAvatar)
        {
            hideAvatarUI();
        }
        else
        {
            showAvatarUI();
        }
        //playerAvatar.gameObject.SetActive(isShowAvatar);
        //Debug.Log(isShowAvatar);
    }
    
    public void showAvatarUI()
    {
        isShowAvatar = true;
        playerAvatar.ShowUI();
    }

    public void hideAvatarUI()
    {
        isShowAvatar = false;
        playerAvatar.HideUI();
    }

    public void ShowPlayerNameLabel(string playerName)
    {
        int maxlen = 0;
        string newName = "";
        maxlen = playerName.Length;
        if ( maxlen > 8 )
        {
            newName = playerName.Substring(0, 8) + "...";
        }
        else 
        {
            newName = playerName;
        }
        PlayerName.text = newName;
    }


    public void UpdateSilverAmount(int silverAmt)
    {
        SilverCoinLabel.text = silverAmt.ToString();
    }

    public void UpdateGoldAmount(int goldAmt)
    {
        GoldCoinLabel.text = goldAmt.ToString();
    }

    public void UpdateTitle(string title)
    {
        MenuTitle.text = title;
    }
}
