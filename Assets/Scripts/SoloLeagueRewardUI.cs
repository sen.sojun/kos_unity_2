﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoloLeagueRewardUI : MonoBehaviour {

	#region Delegates 
	public delegate void OnCloseDetail();
	#endregion
	public UILabel LevelLabel;
	public UILabel DiamondLabel;

	public List<UILabel> RankLabel;
	public List<UILabel> LevelUseLabel;
	public List<UILabel> DiamondUseLabel;
	public List<FullCardUI> CardList;
	public List<UISprite> PackImgList;

	public List<SoloLeagueRewardButtonUI> RewardButtonList;

	private OnCloseDetail _onCloseDetail = null;


	public void HideUI()
	{
		gameObject.SetActive (false);
	}

	public void ShowUI(int level, int diamond, List<SoloLeagueRewardDBData> rewardDataList)
	{
		ShowDetail(level, diamond, rewardDataList);
		gameObject.SetActive(true);
	}

	private void ShowDetail(int level, int diamond, List<SoloLeagueRewardDBData> rewardDataList)
	{
		LevelLabel.text = level.ToString();
		DiamondLabel.text = diamond.ToString();

		foreach(SoloLeagueRewardDBData data in rewardDataList) 
		{
			int index = data.PositionIndex - 1;
			if (index < RewardButtonList.Count) 
			{
				RewardButtonList [index].ShowUI (data);
			}
		}
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
