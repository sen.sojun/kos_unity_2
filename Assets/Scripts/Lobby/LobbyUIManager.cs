﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.Networking.Types;
using UnityEngine.Networking.Match;
using UnityEngine.SceneManagement;

public class LobbyUIManager : MonoBehaviour 
{
    public delegate void BackButtonDelegate();
    public BackButtonDelegate BackDelegate;

    #region Public Properties
    public static LobbyUIManager Instance;
    
    public MainLobbyUI MainUI;
    public OnlineLobbyUI OnlineUI;
    public LocalLobbyUI LocalUI;

    public LobbyPlayerListUI PlayerListUI;
    public LobbyServerListUI ServerListUI;
    public LobbyStatusUI StatusUI;
    public LobbyVSUI VSUI;
    
    public LobbyMatchUI MatchUI;
    public TopBarManager TopBar;

    public GameObject LoadingUI;
    
    public AudioClip MainMenuClip;
    public bool IsTestNotGuest = false;
    #endregion
    
	// Use this for initialization
	void Start () 
    {	
        Instance = this;    
        KOSNetwork.NetworkSetting.IsNetworkEnabled = true;
        KOSNetwork.NetworkSetting.IsHaveTimeout = true;
        PlayerSave.SavePlayFromSoloLeague(false);
        
        LocalizationManager.Init();

        if(KOSLobbyManager.Instance != null)
        {
            KOSLobbyManager.Instance.CancelMatching();
            KOSLobbyManager.Instance.Init();
        }
        
        KOSNetwork.GamePlayer[] players = GameObject.FindObjectsOfType<KOSNetwork.GamePlayer>();
        foreach(KOSNetwork.GamePlayer gamePlayer in players)
        {
            Destroy(gamePlayer.gameObject);
        }
        
        MatchingData[] matchingDatas = GameObject.FindObjectsOfType<MatchingData>();
        foreach(MatchingData matchingData in matchingDatas)
        {
            Destroy(matchingData.gameObject);
        }
        
        #if UNITY_EDITOR
        if(IsTestNotGuest)
        {
            PlayerSave.SavePlayerAccountType(AccountType.KOS);
        }
        #endif
        
        if(!SoundManager.IsPlayBGM(MainMenuClip))
        {
            SoundManager.PlayBGM(MainMenuClip);
        }
        
        UpdateTopBarText();        
        ShowMainLobby();
	}
	
    /*
	// Update is called once per frame
	void Update () 
    {	
	}
    */
    
    public void OnDestroy()
    {
        Instance = null;
    }
    
    #region MainLobby Methods
    public void ShowMainLobby()
    {
        OnlineUI.HideUI();
        LocalUI.HideUI();
        ShowLoading(false);
        VSUI.HideUI();
        MatchUI.HideUI();
    
        MainUI.ShowUI();
        
        TopBar.SetUpBar(
              true
            , LocalizationManager.GetText("LOBBY_MAIN_HEADER")
            , BackToMenu
        );
    }
    
    public void HideMainLobby()
    {
        MainUI.HideUI();
    }
    #endregion
    
    #region OnlineLobby Methods
    public void ShowOnlineLobby()
    {
        Debug.Log("ShowOnlineLobby");
    
        MainUI.HideUI();
        LocalUI.HideUI();       
        ShowLoading(false);
        
        //ServerListUI.HideUI();
        StatusUI.HideUI();
        
        PlayerListUI.HideUI();
        VSUI.HideUI();
    
        OnlineUI.ShowUI();
    }
    
    public void HideOnlineLobby()
    {
        OnlineUI.HideUI();
    }
    #endregion
    
    #region LocalLobby Methods
    public void ShowLocalLobby()
    {
        Debug.Log("ShowLocalLobby");
    
        MainUI.HideUI();
        OnlineUI.HideUI();
        ShowLoading(false);
        
        //ServerListUI.HideUI();
        StatusUI.HideUI();
        
        PlayerListUI.HideUI();
        VSUI.HideUI();
        MatchUI.HideUI();

        LocalUI.ShowUI();
    }
    
    public void HideLocalLobby()
    {
        LocalUI.HideUI();
    }
    
    public void ShowStatusMessage(string message)
    {
        if(this.BackDelegate != null)
        {
            StatusUI.ShowUI(message, this.BackDelegate.Invoke);
        }
        else
        {
            StatusUI.ShowUI(message, null);
        }
    }
    
    public void ShowStatusMessage(string message, UnityAction onClickCancel)
    {
        if(onClickCancel != null)
        {
            StatusUI.ShowUI(message, onClickCancel);
        }
        else
        {
            StatusUI.ShowUI(message, null);
        }
    }
    
    public void HideStatusMessage()
    {
        StatusUI.HideUI();
    }
    #endregion
    
    #region LobbyPlayerList Methods
    public void ShowPlayerList(string roomStatus, UnityAction onClickBack)
    {
        MainUI.HideUI();
        LocalUI.HideUI();
        OnlineUI.HideUI();
        
        ShowLoading(false);
    
        PlayerListUI.ShowUI(roomStatus, onClickBack);
    }
    
    public void HidePlayerList()
    {
        PlayerListUI.HideUI();
    }
    #endregion
    
    #region LobbyServerList Methods
    public void ShowServerList()
    {    
        ServerListUI.ShowUI();
    }
    
    public void UpdateRoomList(List<MatchInfoSnapshot> matchList)
    {    
        ServerListUI.UpdateRoomList(matchList);
    }
    
    public void HideSeverList()
    {    
        ServerListUI.HideUI();
    }
    #endregion
    
    #region Online Match Methods
    public void ShowOnlineMatch()
    {
        Debug.Log("ShowOnlineMatch");
    
        MainUI.HideUI();
        LocalUI.HideUI(); 
        PlayerListUI.HideUI();  
        VSUI.HideUI();    
    
        MatchUI.ShowUI();
    }
    
    public void HideOnlineMatch()
    {
        MatchUI.HideUI();
    }
    #endregion
    
    public void ShowVS()
    {    
        ShowVS(
              KOSLobbyManager.Instance.KOSPlayers[0]
            , KOSLobbyManager.Instance.KOSPlayers[1]
        );
    }
    
    private void ShowVS(KOSLobbyPlayer player1, KOSLobbyPlayer player2)
    {
        VSUI.ShowUI(player1, player2);
        HidePlayerList();
    }
    
    public void ShowVS(string imagePath_L, string imagePath_R, string name_L, string name_R)
    {
        VSUI.ShowUI(imagePath_L, imagePath_R, name_L, name_R);
        HidePlayerList();
    }
    
    public void UpdateVSTime(int second)
    {    
        VSUI.SetTime(second);
    }
    
    public void HideVS()
    {
        VSUI.HideUI();
    }
    
    public void UpdateTopBarText()
    {
        TopBar.UpdateSilverAmount(PlayerSave.GetPlayerSilverCoin());
        TopBar.UpdateGoldAmount(PlayerSave.GetPlayerGoldCoin());
    }
    
    public void BackToMenu()
    {
        KOSNetwork.NetworkSetting.IsHaveTimeout = false;
        
        if(KOSLobbyManager.Instance != null)
        {
            KOSLobbyManager.Instance.FullyShutdown();
        }
        
        LoadScene("MainMenuScene2");
    }

    #region Methods
    public void LoadScene(string sceneName)
    {
        StartCoroutine(LoadingScene(sceneName));
    }

    IEnumerator LoadingScene(string sceneName)
    {
        ShowLoading(true);
        
        yield return new WaitForSeconds(0.01f);
        SceneManager.LoadSceneAsync(sceneName);
    }
    
    public void ShowLoading(bool isShow)
    {
        if(isShow)
        {
            LoadingUI.gameObject.SetActive(true);
        }
        else
        {
            LoadingUI.gameObject.SetActive(false);
        }
    }
    #endregion
}
