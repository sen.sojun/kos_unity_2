﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeckEditorResultUI : MonoBehaviour {
    public delegate void OnClickPopup();
    public Text Title;
    public Text TextDesc;
    public Text TextButton;

    private DeckEditorResultUI.OnClickPopup _onClickCallback = null;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void onHideUI()
    {
        gameObject.SetActive(false);
    }

    public void onShowUI(string title, string desc, string popuptext,OnClickPopup callback = null)
    {
        _onClickCallback = callback;
        gameObject.SetActive(true);
        Title.text = title;
        TextDesc.text = desc;
        TextButton.text = popuptext;
    }

    public void OnClickButton()
    {
        if (_onClickCallback != null)
        {
            _onClickCallback.Invoke();
        }
    }
}
