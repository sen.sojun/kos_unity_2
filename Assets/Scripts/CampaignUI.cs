﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CampaignUI : MonoBehaviour 
{
	public MenuManager MenuManager;
	public List<NPCButton> NPCButtonList;

	public bool IsPlayFirstTime { get { return (MenuManager.ForcePlayFirstTime || PlayerSave.IsPlayerFirstTime()); } }


	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	/*
	void Update () {
	
	}
	*/

	public void ShowUI()
	{
		// Setup button.
		for(int index = 0; index < NPCButtonList.Count; ++index)
		{
			if(index != 0)
			{
				//uncomment when real use
//				SetEnableButton(index, !PlayerSave.IsPlayerFirstTime());
//				SetShowGlowEffect(index, !PlayerSave.IsPlayerFirstTime());

				//comment both lines below if wanna real use
				SetEnableButton(index,!IsPlayFirstTime);
				SetShowGlowEffect(index, !IsPlayFirstTime);
			}
			else
			{
				SetEnableButton(index, true);
				SetShowGlowEffect(index, true);
			}
		}
	}

	private void SetEnableButton(int index, bool isEnable)
	{
		NPCButtonList[index].SetEnableButton(isEnable);
	}
	private void SetShowGlowEffect(int index, bool isShow)
	{
		NPCButtonList[index].SetShowGlowEffect(isShow);
	}
}
