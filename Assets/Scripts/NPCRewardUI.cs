﻿using UnityEngine;
using System.Collections;

public class NPCRewardUI : MonoBehaviour 
{
	public FullCardUI FullCardUI;
	public UILabel CardLabel;

	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
	}

	public void ShowUI(PlayerCardData data)
	{
        string cardName = "";
        switch (LocalizationManager.CurrentLanguage)
        {
            case LocalizationManager.Language.Thai:
            {
                cardName = data.Name_TH;
                if (data.Alias_TH.Length > 0)
                {
                    cardName += ", " + data.Alias_TH;
                }
            }
            break;

            case LocalizationManager.Language.English:
            default:
            {
                cardName = data.Name_EN;
                if (data.Alias_EN.Length > 0)
                {
                    cardName += ", " + data.Alias_EN;
                }
            }
            break;
        }

        CardSymbolData symbolData;
        if (data.SymbolData == CardSymbolData.CardSymbol.SuperRare)
        {
            symbolData = new CardSymbolData(CardSymbolData.EnumCardSymbolToID(CardSymbolData.CardSymbol.Rare));
        }
        else
        {
            symbolData = data.SymbolData;
        }

        string cardSymbol = "";
        switch (LocalizationManager.CurrentLanguage)
        {
            case LocalizationManager.Language.Thai:
            {
                cardSymbol = symbolData.SymbolName_TH;
            }
            break;

            case LocalizationManager.Language.English:
            default:
            {
                cardSymbol = symbolData.SymbolName_EN;
            }
            break;
        }
        //Debug.Log("cardSymbol: " + cardSymbol);

        string rewardDescription = string.Format("{0}\n\n{1} ({2})"
			, Localization.Get("NPC_REWARD_CARD_DETAIL")
            , cardName
            , cardSymbol
        );

        FullCardUI.SetCardData(data);
        CardLabel.text = rewardDescription;

		gameObject.SetActive(true);
		BattleUIManager.RequestBringForward(gameObject);
	}

	public void HideUI()
	{
		gameObject.SetActive(false);
	}
}
