﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using AppAdvisory.Ads;

public class TestAdsManager : MonoBehaviour 
{
    public UILabel StatusLabel;
	public LogText LogText;

	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
        if (MyAdmobManager.Instance != null)
        {
            StatusLabel.text = "Status: " + MyAdmobManager.IsRewardedVideoReady.ToString();
        }
	}

    public void PrintStatus()
	{
        PrintLog("Status: " + MyAdmobManager.IsRewardedVideoReady.ToString());
	}

    public void PreloadRewardedVideo()
	{
        MyAdmobManager.PreloadRewardedVideo();
	}

    public void ShowRewardedVideo()
	{
        MyAdmobManager.ShowRewardedVideo();
	}
        
	public void PrintLog(string text)
	{
		if (LogText != null)
		{
			LogText.PrintLog (text);
		}

		Debug.Log (text);
	}
}
