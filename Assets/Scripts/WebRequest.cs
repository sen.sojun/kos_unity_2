﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class WebRequest : MonoBehaviour
{
    public delegate void OnRequestCompleted(bool isSuccess, string result);
    
    
    #region Methods
    public static void RequestUrl(string url, OnRequestCompleted onComplete)
    {
        Debug.Log("request URL : " + url);
        
        GameObject obj = new GameObject();
        WebRequest request = obj.AddComponent<WebRequest>() as WebRequest;

        request.StartRequestUrl(url, onComplete);
    }
    
    private void StartRequestUrl(string url, OnRequestCompleted onComplete)
    {
        StartCoroutine(OnRequestUrl(url, onComplete));
    }

    private IEnumerator OnRequestUrl(string url, OnRequestCompleted onComplete)
    {
        WWW www = new WWW(url);
        yield return www;

        string text = "";
        if (string.IsNullOrEmpty(www.error))
        {
            text = System.Text.Encoding.UTF8.GetString(www.bytes, 0, www.bytes.Length); // Convert to UTF-8
        }
        else
        {
            text = www.text;
        }
        
        if(onComplete != null)
        {
            onComplete.Invoke((www.error == null), text);
        }
        
        Destroy(gameObject);
    }
    #endregion
}