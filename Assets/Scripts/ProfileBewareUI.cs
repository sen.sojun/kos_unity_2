﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProfileBewareUI : MonoBehaviour {

    public delegate void OnYes();
    public delegate void OnNo();

    private OnYes _onYes = null;
    private OnNo _onNo = null;

    public Text MsgText;
    public Button YesButton;
    public Button NoButton;


    // Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public void ShowUI(string msg,OnYes onYes, OnNo onNo)
    {
        MsgText.text = msg;
        _onYes = onYes;
        _onNo = onNo;
        YesButton.onClick.RemoveAllListeners();
        YesButton.onClick.AddListener(this.onYesEvent);

        NoButton.onClick.RemoveAllListeners();
        NoButton.onClick.AddListener(this.OnNoEvent);

        gameObject.SetActive(true);
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }

    private void onYesEvent()
    {
        if (_onYes != null)
        {
            _onYes.Invoke();
        }
    }

    private void OnNoEvent()
    {
        if (_onNo != null)
        {
            _onNo.Invoke();
        }
    }
}
