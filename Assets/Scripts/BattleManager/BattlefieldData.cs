﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

public class BattlefieldData 
{
	#region Private Properties
    private BattlePhase __currentPhase;
    private BattlePhase _currentPhase
    {
        get { return __currentPhase; }
        set 
        { 
            __currentPhase = value;
            //Debug.Log("Phase : " + _currentPhase.ToString());
        }
    }

    private int _turnIndex;  
    private PlayerBattleData[] _playersData;

	private PlayerIndex _firstPlayer;
	#endregion

	#region Public Properties
	public BattlePhase CurrentPhase
	{
		get { return _currentPhase; }
	}

    public int Turn
    {
        get { return _turnIndex; }
    }

    public PlayerBattleData[] PlayersData
    {
		get { return _playersData; }
    }

	public PlayerIndex FirstPlayer
	{
		get { return _firstPlayer; }
	}
	#endregion

	#region Constructors
	public BattlefieldData()
	{
		_currentPhase = BattlePhase.BeginGameSetup;
        _turnIndex = 1;
	}
	#endregion

	#region Methods
	public void SetPhase(BattlePhase phase)
    {
        _currentPhase = phase;
    }

    public void SetTurn(int turnIndex)
    {
        _turnIndex = turnIndex;
    }

	public void SetFirstPlayer(PlayerIndex firstPlayer)
	{
		_firstPlayer = firstPlayer;
	}

	/*
    public void SetPlayerDataList(PlayerBattleData[] playersData)
    {
        if (playersData.Length > 0)
        {
            _playersData = new PlayerBattleData[playersData.Length];
            for (int index = 0; index < playersData.Length; ++index)
            {
                _playersData[index] = new PlayerBattleData(playersData[index]);
            }
        }
    }
    */

    public void SetPlayerDataList(List<PlayerData> playerDataList)
    {
		if (playerDataList.Count > 0)
        {
            _playersData = new PlayerBattleData[playerDataList.Count];

			for (int index = 0; index < playerDataList.Count; ++index)
            {
				_playersData[index] = new PlayerBattleData(playerDataList[index], (PlayerIndex)index);
            }
        }
    }

	public void NextPhase()
	{
		if(_currentPhase != BattlePhase.EndEnd)
		{
			++_currentPhase;
		}
		else
		{
            ++_turnIndex;
			_currentPhase = BattlePhase.BeginBegin;
		}
	}

	public bool FindBattleCard(int battleCardID, out BattleCardData resultCard)
	{
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			bool isFound = PlayersData[(int)playerIndex].FindBattleCardByID(battleCardID, out resultCard);
			if(isFound)
			{
				return true;
			}
		}

		resultCard = null;
		return false;
	}

	/*
	public bool FindAllBattleCard(string cardID, out List<BattleCardData> resultList)
	{
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			bool isFound = PlayersData[(int)playerIndex].FindAll(cardID, out resultList);
			if(isFound)
			{
				return true;
			}
		}

		resultList = null;
		return false;
	}
	*/

	public bool FindAllBattleCard(PlayerIndex playerIndex, string cardID, out List<BattleCardData> resultList)
	{
		//foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			bool isFound = PlayersData[(int)playerIndex].FindAll(cardID, out resultList);
			if(isFound)
			{
				return true;
			}
		}

		resultList = null;
		return false;
	}

	public bool FindCard(int uniqueCardID, out UniqueCardData resultCard, out PlayerIndex ownerPlayer)
	{
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			bool isFound = PlayersData[(int)playerIndex].FindUniqueCardByID(uniqueCardID, out resultCard);
			if(isFound)
			{
				ownerPlayer = playerIndex;
				return true;
			}
		}

		ownerPlayer = PlayerIndex.One;
		resultCard = null;
		return false;
	}

	public bool FindCardAtZone(int uniqueCardID, CardZoneIndex targetZone, out UniqueCardData resultCard, out PlayerIndex ownerPlayer)
	{
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			bool isFound = PlayersData[(int)playerIndex].FindUniqueCardByID(uniqueCardID, targetZone, out resultCard);
			if(isFound)
			{
				ownerPlayer = playerIndex;
				return true;
			}
		}

		ownerPlayer = PlayerIndex.One;
		resultCard = null;
		return false;
	}

	public bool FindAllCardAtZone(string cardID, PlayerIndex playerIndex, CardZoneIndex cardZoneIndex, out List<UniqueCardData> resultList)
	{
		//foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			bool isFound = PlayersData[(int)playerIndex].FindUniqueCardIn(cardID, cardZoneIndex, out resultList);
			if(isFound)
			{
				return true;
			}
		}

		resultList = null;
		return false;
	}

    /*
	public bool MoveBattleCard(int battleCardID, BattleZoneIndex targetZone, bool isForceMove = false)
	{
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			bool isMoved = PlayersData[(int)playerIndex].MoveBattleCard(battleCardID, targetZone, isForceMove);
			if(isMoved) return true;
		}

		return false;
	}

	public bool MoveBattleCard(int battleCardID, CardZoneIndex targetZone, out UniqueCardData uniqueCard)
	{
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			bool isMoved = PlayersData[(int)playerIndex].MoveBattleCard(battleCardID, targetZone, out uniqueCard);
			if(isMoved)
			{
				return true;
			}
		}

        uniqueCard = null;
		return false;
	}
    */

	public bool IsCanAttack(int battleCardID)
	{
		BattleCardData battleCard;
		bool isFound = FindBattleCard(battleCardID, out battleCard);
		if(isFound)
		{
			if(battleCard.IsCanAttack)
			{
				foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
				{
					// skip owner.
					if(battleCard.OwnerPlayerIndex == playerIndex) continue;

					List<BattleCardData> result;
					PlayersData[(int)playerIndex].GetAllSummonedCard(out result);

					if(result.Count > 0)
					{
						//Debug.Log(result.Count);
						foreach(BattleCardData target in result)
						{
							if(battleCard.IsCanAttackTo(target))
							{
								return true;
							}
						}
					}
				}
			}
		}
			
		return false;
	}

	public bool IsCanAttack(int battleCardID, out List<BattleCardData> attackableList)
	{
		BattleCardData battleCard;
		bool isFound = FindBattleCard(battleCardID, out battleCard);
		if(isFound)
		{
			if(battleCard.IsCanAttack)
			{
				bool isAtkable = false;
				attackableList = new List<BattleCardData>();
				foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
				{
					// skip owner.
					if(battleCard.OwnerPlayerIndex == playerIndex) continue;

					List<BattleCardData> result;
					PlayersData[(int)playerIndex].GetAllSummonedCard(out result);

					if(result.Count > 0)
					{
						//Debug.Log(result.Count);
						foreach(BattleCardData target in result)
						{
							if(battleCard.IsCanAttackTo(target))
							{
								isAtkable = true;
								attackableList.Add(target);
							}
						}
					}
				}

				if(isAtkable)
				{
					return true;
				}
			}
		}

		attackableList = null;
		return false;
	}

	public void ReactiveAllBattleCard(PlayerIndex playerIndex)
	{
		PlayersData[(int)playerIndex].ReactiveAllBattleCard();
	}

	public void ResetAllBattleCard()
	{
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			PlayersData[(int)playerIndex].ResetAllBattleCard();
		}
	}

	public void UpdateAllBuffBattleCard()
	{
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			PlayersData[(int)playerIndex].UpdateAllBuffBattleCard();
		}
	}

	public void UpdateAllBattleCardUI()
	{
		foreach(PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
		{
			PlayersData[(int)playerIndex].UpdateAllBattleCardUI();
		}
	}
    #endregion
}
