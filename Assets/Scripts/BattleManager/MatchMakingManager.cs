﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class MatchMakingManager : MonoBehaviour
{
    public GameObject MatchingDataPrefab;

    // Use this for initialization
    void Start()
    {
#if UNITY_EDITOR
        if (    (string.Compare(SceneManager.GetActiveScene().name, "NetworkBattleScene") == 0)
             && (string.Compare(SceneManager.GetActiveScene().name, "BattleScene") == 0)
        )
        {
            // In battle scene.

            // Try to find MatchingData.
            MatchingData md = GameObject.FindObjectOfType<MatchingData>();
            if (md == null)
            {
                // Not found any MatchingData.

                // Create new one.
                if (BattleManager.Instance.IsTutorial)
                {
                    this.CreateTutorialData("NPC0001");
                }
                else
                {
                    this.CreateVSBotData("NPC0001", 1);
                }
                KOSNetwork.NetworkSetting.IsNetworkEnabled = false;
                Destroy(gameObject);
            }
        }
#endif
    }

	/*
	// Update is called once per frame
	void Update () {
	
	}
	*/

	public bool CreateVSBotData(string npcID, int npcLv)
	{
		NPCData npcData = new NPCData(npcID);

		if(npcData.NPCID.Length > 0)
		{
			return CreateVSBotData(npcData, npcLv);
		}

		return false;
	}

	public bool CreateVSBotData(NPCData npcData, int npcLv)
	{
		bool isSuccess = false;

		// Create player data.
		DeckData deck;

		isSuccess = PlayerSave.GetPlayerDeck(out deck);
		if(!isSuccess)
		{
			Debug.LogError("MatchMakingManager/CreateVSBotData: Failed to create player data.");
			return false;	
		}
			
		/*
#if UNITY_EDITOR
		// For Debug
		deck = new DeckData();
		for(int i=0; i<20; ++i)
		{
			//deck.Add(new PlayerCardData("C0001")); // Airport
			//deck.Add(new PlayerCardData("C0002")); // Angel of Earth
			//deck.Add(new PlayerCardData("C0004")); // Hydro Power Plant
			//deck.Add(new PlayerCardData("C0005")); // Service Bot
			//deck.Add(new PlayerCardData("C0006")); // Warp Gate
			//deck.Add(new PlayerCardData("C0007")); // Di alno Troop
			//deck.Add(new PlayerCardData("C0008")); // Energy Recharger
			deck.Add(new PlayerCardData("C0011")); // Mutant Drone
			//deck.Add(new PlayerCardData("C0012")); // Soul Crystal Mine
			//deck.Add(new PlayerCardData("C0014")); // Detonation
			deck.Add(new PlayerCardData("C0015")); // Exploration
			//deck.Add(new PlayerCardData("C0016")); // Fire Slash
			//deck.Add(new PlayerCardData("C0018")); // Fusion Power Plant
			//deck.Add(new PlayerCardData("C0019")); // Giant Rabbit
			//deck.Add(new PlayerCardData("C0022")); // Rebirth
			//deck.Add(new PlayerCardData("C0025")); // Angel of Dew
			//deck.Add(new PlayerCardData("C0024")); // Tornado
			//deck.Add(new PlayerCardData("C0026")); // Angel of Fire
			//deck.Add(new PlayerCardData("C0027")); // Angel of Wind
			//deck.Add(new PlayerCardData("C0028")); // Harvester
			//deck.Add(new PlayerCardData("C0029")); // Interceptor
			//deck.Add(new PlayerCardData("C0030")); // Di alno Crusader
			//deck.Add(new PlayerCardData("C0031")); // Di alno Scout
			//deck.Add(new PlayerCardData("C0033")); // Razor Hound
			//deck.Add(new PlayerCardData("C0034")); // Corpse Eater
			//deck.Add(new PlayerCardData("C0035")); // Mutant Soldier
			//deck.Add(new PlayerCardData("C0036")); // Dam Failure
            //deck.Add(new PlayerCardData("C0037")); // Construction Plan
			//deck.Add(new PlayerCardData("C0038")); // Item Hunter
			//deck.Add(new PlayerCardData("C0039")); // Laser Turret
			//deck.Add(new PlayerCardData("C0040")); // Mist Wall
            //deck.Add(new PlayerCardData("C0041")); // Berondo Starport
			//deck.Add(new PlayerCardData("C0042")); // Prototype Xeraph
			//deck.Add(new PlayerCardData("C0044")); // Davox
			//deck.Add(new PlayerCardData("C0046")); // Imperial Knight
			//deck.Add(new PlayerCardData("C0047")); // Nuclear Silo
			//deck.Add(new PlayerCardData("C0048")); // Kano
			//deck.Add(new PlayerCardData("C0049")); // Lakuras
            //deck.Add(new PlayerCardData("C0051")); // Lakuras's Order
            //deck.Add(new PlayerCardData("C0052")); // Recruitment
			//deck.Add(new PlayerCardData("C0053")); // Ethan Wright
			//deck.Add(new PlayerCardData("C0054")); // Marron
			//deck.Add(new PlayerCardData("C0055")); // Ashura
			//deck.Add(new PlayerCardData("C0056")); // Sonata Di alno
            //deck.Add(new PlayerCardData("C0057")); // Lidia Berondo
			//deck.Add(new PlayerCardData("C0058")); // Maggie
			//deck.Add(new PlayerCardData("C0059")); // Amie
			//deck.Add(new PlayerCardData("C0062")); // Spector
			//deck.Add(new PlayerCardData("C0064")); // Diseased Bug
			//deck.Add(new PlayerCardData("C0065")); // Green Eyes
			//deck.Add(new PlayerCardData("C0066")); // Strength Seed
			//deck.Add(new PlayerCardData("C0068")); // Fire Twist
			//deck.Add(new PlayerCardData("C0069")); // Jungle Trap
			//deck.Add(new PlayerCardData("C0070")); // Marron's Order
			//deck.Add(new PlayerCardData("C0071")); // Mist Skira
			//deck.Add(new PlayerCardData("C0072")); // Mutant Spirit
			//deck.Add(new PlayerCardData("C0073")); // Plaque Troops
			//deck.Add(new PlayerCardData("C0074")); // Forbidden Cave
			//deck.Add(new PlayerCardData("C0075")); // Heat Ray
			//deck.Add(new PlayerCardData("C0076")); // Hellflame Skira
			deck.Add(new PlayerCardData("C0079")); // Undertaker
			//deck.Add(new PlayerCardData("C0080")); // Shadow Apprentice
			//deck.Add(new PlayerCardData("C0082")); // Wild Skira 
			//deck.Add(new PlayerCardData("C0083")); // Lidia Berondo
			//deck.Add(new PlayerCardData("C0084")); // Sonata Di alno
			//deck.Add(new PlayerCardData("C0085")); // Ashura
			//deck.Add(new PlayerCardData("C0086")); // Crystalline Dragon
			//deck.Add(new PlayerCardData("C0088")); // Lunar Rosette
			//deck.Add(new PlayerCardData("C0089")); // Garbage Thrower
			//deck.Add(new PlayerCardData("C0090")); // Psychic Soldier
			//deck.Add(new PlayerCardData("C0092")); // Haunted Doll
			//deck.Add(new PlayerCardData("C0093")); // Razor Panther
			//deck.Add(new PlayerCardData("C0094")); // Mirror Path
		}
		// Base
        //deck.Add(new PlayerCardData("C0041")); // Berondo Starport
		//deck.Add(new PlayerCardData("C0043")); // Robotic Factory
		//deck.Add(new PlayerCardData("C0050")); // Mutant Lair
		deck.Add(new PlayerCardData("C0060")); // Fortress
		//deck.Add(new PlayerCardData("C0074")); // Forbidden Cave
		// Leader
		deck.Add(new PlayerCardData("C0062")); // Spector
		//deck.Add(new PlayerCardData("C0088")); // Lunar Rosette
#endif
		*/

        // Create player data.
        string localPlayerName = PlayerSave.GetPlayerName();
        string playerName = "";
        switch (LocalizationManager.CurrentLanguage)
        {
            case LocalizationManager.Language.Thai:
            {
                playerName = "ผู้เล่น";
            }
            break;

            case LocalizationManager.Language.English:
            default:
            {
                playerName = "Player";
            }
            break;
        }

		PlayerData playerData = new PlayerData(
			  "PLAYER"
			, localPlayerName.Length > 0 ? localPlayerName : playerName
			, PlayerSave.GetPlayerAvatar()
			, PlayerSave.GetPlayerAvatar()
			, PlayerSave.GetPlayerLevel()
			, deck
		);

		/*
		// For Debug
		DeckData deck2 = new DeckData();
		for(int i=0; i<20; ++i)
		{
			//deck2.Add(new PlayerCardData("C0004")); // Hydro Power Plant
			//deck2.Add(new PlayerCardData("C0005")); // Service Bot
			deck2.Add(new PlayerCardData("C0011")); // Mutant Drone
			//deck2.Add(new PlayerCardData("C0019")); // Giant Rabbit
			//deck2.Add(new PlayerCardData("C0024")); // Tornado
			//deck2.Add(new PlayerCardData("C0025")); // Angel of Dew
			//deck2.Add(new PlayerCardData("C0028")); // Harvester
			//deck2.Add(new PlayerCardData("C0031")); // Di alno Scout
			//deck2.Add(new PlayerCardData("C0033")); // Razor Hound
			//deck2.Add(new PlayerCardData("C0035")); // Mutant Soldier
			//deck2.Add(new PlayerCardData("C0038")); // Item Hunter
			//deck2.Add(new PlayerCardData("C0044")); // Davox
			//deck2.Add(new PlayerCardData("C0046")); // Imperial Knight
			//deck2.Add(new PlayerCardData("C0047")); // Nuclear Silo
			//deck2.Add(new PlayerCardData("C0062")); // Spector
			//deck2.Add(new PlayerCardData("C0064")); // Diseased Bug
			//deck2.Add(new PlayerCardData("C0065")); // Green Eyes
			//deck2.Add(new PlayerCardData("C0093")); // Razor Panther
			deck2.Add(new PlayerCardData("C0094")); // Mirror Path
		}
		//deck2.Add(new PlayerCardData("C0060")); // Fortress
		deck2.Add(new PlayerCardData("C0074")); // Forbidden Cave
		deck2.Add(new PlayerCardData("C0088")); // Lunar Rosette
		//deck2.Add(new PlayerCardData("C0062")); // Spector
		*/

        // Create bot data.
        string botName = "";
        switch (LocalizationManager.CurrentLanguage)
        {
            case LocalizationManager.Language.Thai:
            {
                botName = npcData.Name_TH;
            }
            break;

            case LocalizationManager.Language.English:
            default:
            {
                botName = npcData.Name_EN;
            }
            break;
        }

		PlayerData botData = new PlayerData (
			  npcData.NPCID
			, botName
			, npcData.ImageName
			, npcData.BattleImageName
			, npcLv
			, npcData.DeckData
		);

		CreateMatchingData(playerData, botData, npcData.BGIndex, true);

		return true;
	}

	public void CreateMatchingData(PlayerData player1, PlayerData player2, int bgIndex, bool isVSBot = true, bool isTutorial = false)
	{
		GameObject obj = GameObject.Instantiate(MatchingDataPrefab) as GameObject;
		MatchingData matchingData = obj.GetComponent<MatchingData>();

		matchingData.SetPlayersData(player1, player2, bgIndex, isVSBot, isTutorial);

		DontDestroyOnLoad(obj);
	}
		
	public bool CreateSoloLeagueData(List<string> cardIDList, string enemyNameID, int enemyLv)
	{
		// Create player data.
		DeckData deck;
		bool isSuccess = PlayerSave.GetPlayerDeck(out deck);
		if(!isSuccess)
		{
			Debug.LogError("MatchMakingManager/CreateSoloLeagueData: Failed to create player data.");
			return false;	
		}
			
		string playerName = "";
		switch (LocalizationManager.CurrentLanguage)
		{
			case LocalizationManager.Language.Thai:
			{
				playerName = "ผู้เล่น";
			}
			break;

			case LocalizationManager.Language.English:
			default:
			{
				playerName = "Player";
			}
			break;
		}

		PlayerData playerData = new PlayerData(
			  "PLAYER"
			, PlayerSave.GetPlayerName()
			, PlayerSave.GetPlayerAvatar()
			, PlayerSave.GetPlayerAvatar()
			, PlayerSave.GetPlayerLevel()
			, deck
		);

		// Create bot data.
		DeckData botDeck = new DeckData ();
		foreach (string id in cardIDList)
		{
			PlayerCardData card = new PlayerCardData (id);
			botDeck.Add (card);
			//Debug.Log (id + ":" + card.Name_EN);
		}
        Debug.Log("Bot Deck's = " + botDeck.ToString());

		List<NPCDBData> npcList;
		NPCDB.GetAllData (out npcList);

		NPCData npcData = new NPCData(npcList [Random.Range (0, npcList.Count)].NPCID);
			
		string botName = "";
		SoloLeagueNPCNameDBData npcNameData;
		SoloLeagueNPCDB.GetData (enemyNameID, out npcNameData);

		switch (LocalizationManager.CurrentLanguage)
		{
			case LocalizationManager.Language.Thai:
			{
				botName = npcNameData.NPCNAME;
			}
			break;

			case LocalizationManager.Language.English:
			default:
			{
				botName = npcNameData.NPCNAME;
			}
			break;
		}

		PlayerData botData = new PlayerData(
			  npcData.NPCID
			, botName
			, npcData.ImageName
			, npcData.BattleImageName
			, enemyLv
			, botDeck
		);

		CreateMatchingData(playerData, botData, 3, true, false);

		return true;
	}

    public bool CreateVSBotNetworkData()
    {
        PlayerData playerData = null;
        bool isSuccess = CreatePlayerData(out playerData);
        
        if(!isSuccess)
        {
            Debug.LogError("MatchMakingManager/CreateVSBotNetworkData: Failed to create player data.");
            return false;
        }
        
        // Create bot data.
         List<string> cardIDList = DeckData.CreateRandomDeck();
        DeckData botDeck = new DeckData();
        foreach (string id in cardIDList)
        {
            PlayerCardData card = new PlayerCardData (id);
            botDeck.Add (card);
            //Debug.Log (id + ":" + card.Name_EN);
        }
        //Debug.Log("Bot Deck's = " + botDeck.ToString());

        //bot image

        //---start of deletion vit 16102018
        //---using new avatar image begin with 07-12
        //---so not using 1-6 anymore

        //int avatarIndex = Random.Range(0, 6);
        //---end of deletion

        //Range(inc,excl)
        int avatarIndex = Random.Range(7, 13); //ins vit 16102018
        string imageName = avatarIndex.ToString("00");
        string imageName_b = avatarIndex.ToString("00");
        
        // bot name
        string botName = "";
        List<SoloLeagueNPCNameDBData> nameList;        
        SoloLeagueNPCDB.GetAllData(out nameList);        
        botName = nameList[Random.Range (0, nameList.Count)].NPCNAME;
        
        // bot data
        PlayerData botData = new PlayerData(
              "Bot"
            , botName
            , imageName
            , imageName_b
            , 1
            , botDeck
        );

        CreateMatchingData(playerData, botData, Random.Range(0, 3), true, false);
        
        return true;
    }
    
    private bool CreatePlayerData(out PlayerData data)
    {
        // Create player data.
        DeckData deck;
        bool isSuccess = PlayerSave.GetPlayerDeck(out deck);
        if(!isSuccess)
        {
            Debug.LogError("MatchMakingManager/CreatePlayerData: Failed to create player data.");
            data = null;
            return false;   
        }
 
        PlayerData playerData = new PlayerData(
              "PLAYER"
            , PlayerSave.GetPlayerName()
            , PlayerSave.GetPlayerAvatar()
            , PlayerSave.GetPlayerAvatar()
            , PlayerSave.GetPlayerLevel()
            , deck
        );
        
        data = playerData;
        return true;
    }

	public bool CreateTutorialData(string npcID)
	{
		NPCData npcData = new NPCData(npcID);

		if(npcData.NPCID.Length > 0)
		{
			return CreateTutorialData(npcData);
		}

		return false;
	}

	public bool CreateTutorialData(NPCData npcData)
	{
		bool isSuccess = false;

		// Create player data.
		DeckData deck = new DeckData();

		// Player first hand
		deck.Add(new PlayerCardData("C0061"));
		deck.Add(new PlayerCardData("C0019")); 
		deck.Add(new PlayerCardData("C0034")); 
		deck.Add(new PlayerCardData("C0019")); 
		deck.Add(new PlayerCardData("C0034")); 

		// Player deck
		deck.Add(new PlayerCardData("C0007")); 
		deck.Add(new PlayerCardData("C0016")); 
		deck.Add(new PlayerCardData("C0030")); 

		for(int i=0; i<15; ++i)
		{
			deck.Add(new PlayerCardData("C0019")); 
			deck.Add(new PlayerCardData("C0034")); 
		}

		// Base
		deck.Add(new PlayerCardData("C0060")); // Fortress

		// Leader
		deck.Add(new PlayerCardData("C0062")); // Spector

        // Create player data.
        string playerName = "";
        switch (LocalizationManager.CurrentLanguage)
        {
            case LocalizationManager.Language.Thai:
            {
                playerName = "ผู้เล่น";
            }
            break;

            case LocalizationManager.Language.English:
            default:
            {
                playerName = "Player";
            }
            break;
        }

        PlayerData playerData = new PlayerData(
			"PLAYER"
			, playerName
			, "NPC_Spector_01"
			, "NPC_Spector_b01"
			, PlayerSave.GetPlayerLevel()
			, deck
		);

		DeckData deck2 = new DeckData();

		// Bot first hand
		deck2.Add(new PlayerCardData("C0011"));
		deck2.Add(new PlayerCardData("C0019"));
		deck2.Add(new PlayerCardData("C0034"));
		deck2.Add(new PlayerCardData("C0019"));
		deck2.Add(new PlayerCardData("C0034")); 
		deck2.Add(new PlayerCardData("C0019"));

		// Bot deck
		deck2.Add(new PlayerCardData("C0009"));
		deck2.Add(new PlayerCardData("C0061"));
		deck2.Add(new PlayerCardData("C0009")); 

		for(int i=0; i<15; ++i)
		{
			deck2.Add(new PlayerCardData("C0019")); 
			deck2.Add(new PlayerCardData("C0034")); 
		}

		// Base
		deck2.Add(new PlayerCardData("C0060")); // Fortress

		// Leader
		deck2.Add(new PlayerCardData("C0044")); // Davox

        // Create bot data.
        string botName = "";
        switch (LocalizationManager.CurrentLanguage)
        {
            case LocalizationManager.Language.Thai:
                {
                    botName = npcData.Name_TH;
                }
                break;

            case LocalizationManager.Language.English:
            default:
                {
                    botName = npcData.Name_EN;
                }
                break;
        }

        PlayerData botData = new PlayerData(
			  npcData.NPCID
			, botName
			, npcData.ImageName
			, npcData.BattleImageName
			, 1
			, deck2
		);

		CreateMatchingData(playerData, botData, npcData.BGIndex, true, true);

		return true;
	}
}
