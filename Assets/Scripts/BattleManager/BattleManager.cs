﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using GamePlayerUI;
using GameAnalyticsSDK;
using KOSNetwork;
using System;

#region DisplayBattlePhase Enum
public enum DisplayBattlePhase : int
{
    GameSetup = 0
    , Begin
    , Reactive
    , Reiterate
    , Draw
    , PreBattle
    , Battle
    , PostBattle
    , End
}
#endregion

#region BattlePhase Enum
public enum BattlePhase : int
{
    BeginGameSetup = 0
    , SetupData
    , WaitSetupData
    , TutorIntro
    , WaitTutorIntro

    // RPS
    , StartRPSSelect
    , WaitRPSSelect
    , ResultRPS
    , WaitRPSResult

    // Play Order
    , StartPlayOrderSelect
    , WaitPlayOrderSelect
    , ResultPlayOrder
    , WaitPlayOrderResult

    , TutorSetupCard
    , WaitTutorSetupCard
    , SetupBase
    , WaitSetupBase
    , SetupLeader
    , WaitSetupLeader

    , TutorBeforePlay
    , WaitTutorBeforePlay
    , InitDrawCard
    , InitReHand
    , WaitReHand

    , EndGameSetup

    , BeginBegin
    , WaitReadyBegin
    , ReadyBegin
    , TutorBeginPlay
    , WaitTutorBeginPlay
    , BeginInactive
    , EndInactive
    , BeginReiterate
    , EndReiterate
    , EndBegin

    , BeginPostBegin
    , EndPostBegin

    , BeginDraw
    , EndDraw

    , BeginPreBattle
    , PreBattle
    , EndPreBattle
    , WaitReadyEndPreBattle
    , ReadyPreBattle

    , BeginBattle
    , Battle
    , EndBattle
    , WaitReadyEndBattle
    , ReadyBattle

    , BeginPostBattle
    , PostBattle
    , EndPostBattle
    , WaitReadyEndPostBattle
    , ReadyPostBattle

    , BeginEnd
    , WaitDiscardOverflow
    , BeginCleanUp
    , EndCleanUp
    , EndEnd
}
#endregion

#region PlayerIndex Enum
public enum PlayerIndex : int
{
    One = 0
    , Two = 1
}
#endregion

#region LocalPlayerIndex Enum
public enum LocalPlayerIndex
{
    Owner
    , Enemy
}
#endregion

#region BattlefieldIndex Enum
public enum BattleZoneIndex
{
    BaseP1
    , Battlefield
    , BaseP2
}
#endregion

#region CardZoneIndex Enum
public enum CardZoneIndex : int
{
      Deck = 0
    , Hand = 1
    , Graveyard = 2
    , RemoveZone = 3
}
#endregion

public class BattleManager
{
    #region Delagates
    public delegate void OnBattlePhaseUpdateDelagate(BattlePhase battlePhase);
    public delegate void OnTurnIndexUpdateDelagate(int turnIndex);
    public delegate void OnPlayersInfoUpdateDelagate(PlayerBattleData[] playersData);

    public delegate bool CheckIsBattleCardCanBeTarget(BattleCardData data);
    public delegate bool CheckIsUniqueCardCanBeTarget(UniqueCardData data);
    #endregion

    #region Private Properties
    private BattlefieldData _battlefieldData = null;

    private IBattleControlController[] _players = null;
    private IBattleManagerController _battleManagerController;
    private BattleUIManager _battleUIManager;
    private BattleEventTrigger[] _battleEventTriggers = null;

    private OnBattlePhaseUpdateDelagate _onBattlePhaseUpdateDelagate;
    private OnTurnIndexUpdateDelagate _onTurnIndexUpdateDelagate;
    private OnPlayersInfoUpdateDelagate _onPlayersInfoUpdateDelagate;

    private PlayerIndex _currentActivePlayerIndex;
    private bool _isTutorial = false;
    private bool _isTutorialCanNextPhase = false;
    private bool _isShowTutorBeginPlay = false;

    private bool _isStartPlayGame = false;
    private bool _isShowChangePhase = false;
    private bool _isPerformBattle = false;
    private bool _isSelectTarget = false;
    private bool _isSelectMove = false;
    private bool _isEndGame = false;
    private bool _isCantDraw = false;
    private PlayerIndex _cantDrawPlayerIndex;
    private bool _isShowEndGame = false;
    private bool _isSkipReHand = true;
    private bool _isRequestNextPhase = false;
    private DisplayBattlePhase _displayBattlePhase = DisplayBattlePhase.End;
    private int _randomSeed;
    
    private bool _isEnableTimeout = false;
    private bool _isTimeout = false;
    private int _pauseTimerCount = 0;
    private bool _isPauseTimeout = false;
    
    private float _turnTimer = 0.0f;
    private float _turnMasterTimer = 0.0f;
    
    private bool _isForceWin = false;
    private PlayerIndex _gameWinPlayerIndex;

    public bool IsEndGame
    {
        get
        {
            if (_isEndGame)
            {
                return true;
            }

            if (_isStartPlayGame)
            {
                foreach (PlayerIndex pIndex in System.Enum.GetValues(typeof(PlayerIndex)))
                {
                    if (_battlefieldData.PlayersData[(int)pIndex].HitPoint <= 0)
                    {
                        _isEndGame = true;
                        return true;
                    }

                    /*
					if(_battlefieldData.PlayersData[(int)pIndex].Deck.Count <= 0)
					{
						_isEndGame = true;
						return true;
					}
					*/
                }
            }

            _isEndGame = false;
            return false;
        }
    }

    private float _startPlayTime = 0.0f;
    private float _stopPlayTime = 0.0f;

    public static BattleManager Instance
    {
        get
        {
            /*
			if (IsReFindInstance || _instance == null)
            {
				GameObject battleController = BattleController.Instance;
                if (battleController != null)
                {
					_instance = battleController.GetComponent<BattleController>().BattleManager;
					IsReFindInstance = false;
                }
				else
				{
					return null;
				}
            }
			*/

            if (BattleController.Instance != null)
            {
                return BattleController.Instance.BattleManager;
            }
            else
            {
                return null;
            }
        }
    }
    #endregion

    #region Public Properties
    /*
    public BattlefieldData BattlefieldData
	{
        get { return _battlefieldData; }
	}
    */

    public BattlePhase CurrentBattlePhase { get { return _battlefieldData.CurrentPhase; } }
    public int Turn { get { return _battlefieldData.Turn; } }
    public float TurnTimer { get { return _turnTimer; } }

    public PlayerBattleData[] PlayersData { get { return _battlefieldData.PlayersData; } }
    public PlayerIndex CurrentActivePlayerIndex { get { return _currentActivePlayerIndex; } }
    public bool IsTutorial { get { return (_isTutorial || BattleController.Instance.IsTestTutorial); } }
    public bool IsPerformBattle { get { return _isPerformBattle; } }
    public bool IsPlayerCanAction
    {
        get
        {
            if (IsPerformBattle || _isShowTutorial || _isShowChangePhase || _isSelectTarget || _isSelectMove) return false;
            else if (!IsReadyToAction) return false;
            else if(IsTimeout) return false;
            else return true;
        }
    }
    
    public bool IsCanUpdateTimeoutTimer
    {
        get
        {
            if(IsHaveTimeout)
            {
                if(!IsPerformBattle && !_isShowTutorial && !_isShowChangePhase)
                {
                    if(IsEnableTimeout && !IsPauseTimeout && !_isTimeout)
                    {
                        return true;
                    }
                }
            }
            
            return false;
        }
    }

    public bool IsCanUpdatePhase
    {
        get
        {
            if (IsPerformBattle || _isShowTutorial || _isShowChangePhase) return false;
            else if (!IsReadyToAction) return false;
            else return true;
        }
    }
    
    public bool IsReadyToAction
    {
        get
        {
            /*
            if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
            {
                return true;
            }
            else
            */
            {
                return ActionManager.Instance.IsReadyToAction;
            }
        }
    }
    public bool IsRequestNextPhase { get { return _isRequestNextPhase; } }
    public bool IsHaveTimeout { get { return (KOSNetwork.NetworkSetting.IsNetworkEnabled || KOSNetwork.NetworkSetting.IsHaveTimeout); } }
    public bool IsEnableTimeout { get { return _isEnableTimeout; } }
    public bool IsTimeout { get { return IsHaveTimeout && IsEnableTimeout && _isTimeout; } }
    public bool IsPauseTimeout { get { return _isPauseTimeout; } }
    #endregion

    #region Constructor
    public BattleManager(IBattleManagerController battleManagerController, BattleUIManager battleUIManager)
    {
        _battlefieldData = new BattlefieldData();
        _battleManagerController = battleManagerController;
        _battleUIManager = battleUIManager;

        _battlefieldData.SetPhase(BattlePhase.BeginGameSetup);
        _battleEventTriggers = new BattleEventTrigger[2];
        _battleEventTriggers[0] = new BattleEventTrigger();
        _battleEventTriggers[1] = new BattleEventTrigger();
    }

    ~BattleManager()
    {
    }
    #endregion

    #region Start
    public void OnStart()
    {
        ActionManager.Init();
        _battleUIManager.ShowLoading(true);
        _battleUIManager.ShowWaiting(false);

        _startPlayTime = Time.time;
        SubscribeBattleUI();
	}
#endregion

#region Update
    public void OnUpdate()
	{
        if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
        {
            // Wait for local player authorize
            if(KOSNetwork.GamePlayer.localPlayer == null)
                return;
                
            if(!KOSNetwork.GamePlayer.localPlayer.IsReadyStartGame)
                return;

			/* 
			if(!KOSNetwork.GamePlayer.localPlayer.isServer)
				return;
			*/
        }
        
        if(IsCanUpdateTimeoutTimer)
        {
            _turnTimer -= Time.deltaTime;
            
            if(TurnTimer < 0.0f)
            {
                OnTurnTimeout();
            }
        }
        
        if(IsTimeout)
        {
            if(!IsCloseAllSelectionUI())
            {
                CloseAllSelectionUI();
            }
        }
        
        if(IsHaveTimeout)
        {            
            if(_isEnableSelectTargetTimer)
            {
                _selectTargetTimer -= Time.deltaTime;
                if(_selectTargetTimer < 0.0f)
                {
                    OnSelectTargetTimeout();
                }
            }
        }
        
		if(!IsCanUpdatePhase) return;

		if(_isShowChangePhase) return;

        if (IsPerformBattle) return;

		if (IsEndGame)
        {
            ShowGameResult();
            return;
        }

        if(!ActionManager.Instance.IsReadyToAction) return;

        switch (CurrentBattlePhase)
        {
			case BattlePhase.BeginGameSetup:
			{
				NextPhase();
			}
			break;

			case BattlePhase.SetupData:
			{
				StartSetupData();
			}
			break;

			case BattlePhase.WaitSetupData:
			{
				// Waiting for setting data. 
				// After setup continue to FindFirst phase.
			}
			break;

			case BattlePhase.TutorIntro:
			{
				StartTutorIntro();
			}
			break;

			case BattlePhase.WaitTutorIntro:
			{
				// Waiting show tutorial. 
			}
			break;

            case BattlePhase.StartRPSSelect:
			{
				StartSelectRPS();
			}
			break;

            case BattlePhase.WaitRPSSelect:
			{
				// Waiting for find who can choose first. 
				// After setup continue to WaitChooseFindFirst phase.
			}
			break;

            case BattlePhase.ResultRPS:
            {
                ShowRPSResult();
            }
            break;

            case BattlePhase.WaitRPSResult:
            {
                // Waiting show RPS result. 
            }
            break;

            case BattlePhase.StartPlayOrderSelect:
			{
                StartSelectPlayOrder();
            }
			break;

            case BattlePhase.WaitPlayOrderSelect:
			{
				// Waiting for find who play first. 
				// After setup continue to SetupBase phase.
			}
			break;

            case BattlePhase.ResultPlayOrder:
            {
                ShowPlayOrderResult();
            }
            break;

            case BattlePhase.WaitPlayOrderResult:
            {
                // Waiting show Player Order result. 
            }
            break;

			case BattlePhase.TutorSetupCard:
			{
				StartTutorSetupCard();
			}
			break;

			case BattlePhase.WaitTutorSetupCard:
			{
				// Waiting show tutorial. 
			}
			break;

			case BattlePhase.SetupBase:
			{
				StartSetupBase();
			}
			break;

			case BattlePhase.WaitSetupBase:
			{
				// Wait player select base card.
			}
			break;

			case BattlePhase.SetupLeader:
			{
				StartSetupLeader();
			}
			break;

			case BattlePhase.WaitSetupLeader:
			{
				// Wait player select leader card.
			}
			break;

			case BattlePhase.TutorBeforePlay:
			{
				StartTutorBeforePlay();
			}
			break;

			case BattlePhase.WaitTutorBeforePlay:
			{
				// Waiting show tutorial. 
			}
			break;

			case BattlePhase.InitDrawCard:
			{
				OnInitDrawCard();
			}
			break;

            case BattlePhase.InitReHand:
            {
                OnStartReHand();
            }
            break;

            case BattlePhase.WaitReHand:
            {
                // Wait player to re hand.
            }
            break;

			case BattlePhase.EndGameSetup:
			{
				// Start play.
				_isStartPlayGame = true;
				NextPhase();
			}
			break;

			case BattlePhase.BeginBegin:
			{
                ReadyBegin();
			}
			break;
            
            case BattlePhase.WaitReadyBegin:
            {
                // Wait player to reach this phase.
            }
            break;
            
            case BattlePhase.ReadyBegin:
            {
                CompleteReadyBegin();
            }
            break;

			case BattlePhase.TutorBeginPlay:
			{
				StartTutorBeginPlay();
			}
			break;

			case BattlePhase.WaitTutorBeginPlay:
			{
				// Wait show tutorial.
			}
			break;

			case BattlePhase.BeginInactive:
			{
				OnStartInactive();
			}
			break;

			case BattlePhase.EndInactive:
			{
				NextPhase();
			}
			break;

			case BattlePhase.BeginReiterate:
			{
				OnStartReiterate();
			}
			break;

			case BattlePhase.EndReiterate:
			{
				NextPhase();
			}
			break;

			case BattlePhase.EndBegin:
			{
				NextPhase();
			}
			break;

            case BattlePhase.BeginPostBegin:
            {
                NextPhase();
            }
            break;

            case BattlePhase.EndPostBegin:
            {
                
                NextPhase();
            }
            break;

			case BattlePhase.BeginDraw:
			{
				OnStartDraw();
			}
			break;

			case BattlePhase.EndDraw:
			{
				NextPhase();
			}
			break;

			case BattlePhase.BeginPreBattle:
			{
				this.ClearSuggestText();
				this.AddSuggestText(Localization.Get("BATTLE_SUGGESTION_PRE_PHASE"));
				OnStartPhasePreBattle();
                SetEnableTimeout(true);
                SetPauseTimeout(false);
			}
			break;

			case BattlePhase.PreBattle:
			{
				// Wait player end phase;
				OnPhasePreBattle();
			}
			break;

			case BattlePhase.EndPreBattle:
			{
                SetPauseTimeout(true);
				ReadyEndPreBattle();
			}
			break;
            
            case BattlePhase.WaitReadyEndPreBattle:
            {
                // Wait player to reach this phase.
            }
            break;
            
            case BattlePhase.ReadyPreBattle:
            {
                CompleteEndPreBattle();
            }
            break;

			case BattlePhase.BeginBattle:
			{
				this.AddSuggestText(Localization.Get("BATTLE_SUGGESTION_BATTLE_PHASE"));
				OnStartPhaseBattle();
                SetPauseTimeout(false);
			}
			break;

			case BattlePhase.Battle:
			{
				// Wait player end phase;
				OnPhaseBattle();
			}
			break;

			case BattlePhase.EndBattle:
			{
                SetPauseTimeout(true);
                ReadyEndBattle();
				//NextPhase();
			}
			break;
            
            case BattlePhase.WaitReadyEndBattle:
            {
                // Wait player to reach this phase.
            }
            break;
            
            case BattlePhase.ReadyBattle:
            {
                CompleteEndBattle();
            }
            break;

			case BattlePhase.BeginPostBattle:
			{
				this.AddSuggestText(Localization.Get("BATTLE_SUGGESTION_POST_PHASE"));
				OnStartPhasePostBattle();
                SetPauseTimeout(false);
			}
			break;

			case BattlePhase.PostBattle:
			{
				// Wait player end phase;
				OnPhasePostBattle();
			}
			break;

			case BattlePhase.EndPostBattle:
			{
                SetPauseTimeout(true);
                SetEnableTimeout(false);
                
                ReadyEndPostBattle();
				//NextPhase();
			}
			break;
            
            case BattlePhase.WaitReadyEndPostBattle:
            {
                // Wait player to reach this phase.
            }
            break;
            
            case BattlePhase.ReadyPostBattle:
            {
                CompleteEndPostBattle();
            }
            break;

			case BattlePhase.BeginEnd:
			{
				OnStartPhaseEnd();
			}
			break;

			case BattlePhase.WaitDiscardOverflow:
			{
				// Wait player discard overflow hand size limit.
			}
			break;

            case BattlePhase.BeginCleanUp:
            {
                CleanUp();
            }
            break;

            case BattlePhase.EndCleanUp:
            {
                OnEndCleanUp();
            }
            break;

			case BattlePhase.EndEnd:
			{
				this.ClearSuggestText();
				OnEndPhaseEnd();
			}
			break;
        }
        
        
        if(this.IsLocalPlayer(CurrentActivePlayerIndex))
        {
            if(IsTimeout)
            {
                // if timeout.
                CloseAllSelectionUI();
            
                if(IsLocalCanRequestNextPhase())
                {               
                    if(IsCloseAllSelectionUI())
                    {     
                        LocalRequestNextPhase(true);
                    }
                }
            }
        
            if(IsTutorial)
            {
                if(IsPlayerCanAction && _isTutorialCanNextPhase && IsLocalCanRequestNextPhase())
                {
                    _battleUIManager.NextPhaseButton.enabled = true;
                }
                else
                {
                    _battleUIManager.NextPhaseButton.enabled = false;
                }
            }
            else
            {
                if(IsPlayerCanAction && IsLocalCanRequestNextPhase())
                {
                    _battleUIManager.NextPhaseButton.enabled = true;
                }
                else
                {
                    _battleUIManager.NextPhaseButton.enabled = false;
                }
            }
        }
        else
        {
            _battleUIManager.NextPhaseButton.enabled = false;
        }
        
        ActionManager.Instance.OnUpdate();
	}
#endregion

#region BattleManager Methods
    private void CloseAllSelectionUI()
    {
        if(this.IsLocalPlayer(CurrentActivePlayerIndex))
        {
            // if my turn.
            if(_isSelectTarget)
            {
                // if show select target battle card.
                AutoSelectOrCancelTarget();
            }
            
            if(_isSelectCardPage)
            {
                // if show select card page.
                AutoSelectOrCancelCardPage();
            }
            
            if(_isAskYesNo)
            {
                // if show yes no.
                AutoSelectYesNo();
            }
            
            if(_isSelectMove)
            {
                // if show select move
                AutoSelectOrCancelMove();
            }
            
            if(_battleUIManager.CardPopupMenuUI.IsShow)
            {
                 // if open menu popup.
                ClosePopupMenuUI();
            }
        
            if(_battleUIManager.FullCardInfoUI.IsShow)
            {
                // if open card info.
                CloseCardInfo();
            }
            
            if(_battleUIManager.AbilitySelectUI.IsShow)
            {
                // if open ability select
                CloseAbilitySelectUI();
            }
        }
    }
    
    private bool IsCloseAllSelectionUI()
    {
        return (
               !_isSelectTarget 
            && !_isSelectCardPage 
            && !_isAskYesNo
            && !_isSelectMove
            && !_battleUIManager.CardPopupMenuUI.IsShow
            && !_battleUIManager.FullCardInfoUI.IsShow
            && !_battleUIManager.AbilitySelectUI.IsShow
        );
    }

	public DisplayBattlePhase GetDisplayBattlePhase()
	{
        return (BattleManager.ToDisplayBattlePhase(CurrentBattlePhase));	
	}

    public static DisplayBattlePhase ToDisplayBattlePhase(BattlePhase battlePhase)
    {
        //Debug.Log("ToDisplayBattlePhase " + battlePhase.ToString());

        if (battlePhase >= BattlePhase.BeginGameSetup && battlePhase <= BattlePhase.EndGameSetup)
        {
            return DisplayBattlePhase.GameSetup;
        }
        else if (battlePhase >= BattlePhase.BeginBegin && battlePhase <= BattlePhase.EndPostBegin)
        {
            return DisplayBattlePhase.Begin;
        }
		/*
        else if (battlePhase >= BattlePhase.BeginInactive && battlePhase <= BattlePhase.EndInactive)
        {
            return DisplayBattlePhase.Inactive;
        }
        else if (battlePhase >= BattlePhase.BeginReiterate && battlePhase <= BattlePhase.EndReiterate)
        {
            return DisplayBattlePhase.Reiterate;
        }
		*/
        else if (battlePhase >= BattlePhase.BeginDraw && battlePhase <= BattlePhase.EndDraw)
        {
            return DisplayBattlePhase.Draw;
        }
        else if (battlePhase >= BattlePhase.BeginPreBattle && battlePhase <= BattlePhase.ReadyPreBattle)
        {
            return DisplayBattlePhase.PreBattle;
        }
        else if (battlePhase >= BattlePhase.BeginBattle && battlePhase <= BattlePhase.ReadyBattle)
        {
            return DisplayBattlePhase.Battle;
        }
        else if (battlePhase >= BattlePhase.BeginPostBattle && battlePhase <= BattlePhase.ReadyPostBattle)
        {
            return DisplayBattlePhase.PostBattle;
        }
        else if (battlePhase >= BattlePhase.BeginEnd && battlePhase <= BattlePhase.EndEnd)
        {
            return DisplayBattlePhase.End;
        }

        Debug.LogError("BattleManager/GetDisplayBattlePhase: CurrentBattlePhase not in any phase. " + battlePhase.ToString());
        return DisplayBattlePhase.End;
    }

    public IBattleControlController GetPlayer(PlayerIndex playerIndex)
	{
		if(_players != null && _players.Length > (int)playerIndex && _players.Length > 0)
		{
			//Debug.Log("L:" + _players.Length.ToString() + ", " + playerIndex.ToString());
			return _players[(int)playerIndex];
		}

		return null;
	}

	public bool GetLocalPlayer(out IBattleControlController localPlayer)
	{
		if(_players != null && _players.Length > 0)
		{
			foreach(IBattleControlController player in _players)
			{
				if(player.IsLocalPlayer())
				{
					localPlayer = player;
					return true;	
				}
			}
		}

		localPlayer = null;
		return false;
	}

	public bool IsLocalPlayer(PlayerIndex playerIndex)
	{
        if (KOSNetwork.NetworkSetting.IsNetworkEnabled)
        {
            if(KOSNetwork.GamePlayer.localPlayer.playerIndex == playerIndex)
            {
                return true;
            }
        }
        else
        {
            if(_players != null && _players.Length > 0)
            {
                foreach(IBattleControlController player in _players)
                {
                    if(player.IsLocalPlayer() && player.GetPlayerIndex() == playerIndex)
                    {
                        return true;	
                    }
                }
            }
        }
			
		return false;
	}
    
    public bool IsLocalTurn()
    {
        return IsLocalPlayer(CurrentActivePlayerIndex);
    }

    public PlayerIndex GetEnemyIndex(PlayerIndex playerIndex)
    {
        if (playerIndex == PlayerIndex.One)
            return PlayerIndex.Two;
        else
            return PlayerIndex.One;
    }

	public bool IsCanSummonCard(PlayerIndex playerIndex)
	{
		List<UniqueCardData> handList = _battlefieldData.PlayersData[(int)playerIndex].GetZoneDataList(CardZoneIndex.Hand);

		if(handList != null && handList.Count > 0)
		{
			foreach(UniqueCardData card in handList)
			{
				bool isCanUseGold = BattleManager.Instance.IsCanUseGold(playerIndex, card.PlayerCardData.Cost);	
				if(isCanUseGold) return true;
			}
		}

		return false;
	}

	public bool IsCanMoveBattleCard(PlayerIndex playerIndex)
	{
		foreach(BattleZoneIndex battleZoneIndex in System.Enum.GetValues(typeof(BattleZoneIndex)))
		{
			List<BattleCardData> battleCardList = _battlefieldData.PlayersData[(int)playerIndex].GetFieldDataList(battleZoneIndex);

			foreach(BattleCardData card in battleCardList)
			{
				if(card.IsCanMove)
				{
					return true;
				}
			}
		}

		return false;
	}

	public bool IsCanAttackBattleCard(PlayerIndex playerIndex)
	{
		foreach(BattleZoneIndex battleZoneIndex in System.Enum.GetValues(typeof(BattleZoneIndex)))
		{
			List<BattleCardData> cardList = _battlefieldData.PlayersData[(int)playerIndex].GetFieldDataList(battleZoneIndex);

			foreach(BattleCardData card in cardList)
			{
				if(_battlefieldData.IsCanAttack(card.BattleCardID))
				{
					return true;
				}
			}
		}

		return false;
	}

	public bool IsCanFaceupBattleCard(PlayerIndex playerIndex)
	{
		foreach(BattleZoneIndex battleZoneIndex in System.Enum.GetValues(typeof(BattleZoneIndex)))
		{
			List<BattleCardData> battleCardList = _battlefieldData.PlayersData[(int)playerIndex].GetFieldDataList(battleZoneIndex);

			foreach(BattleCardData card in battleCardList)
			{
				if(card.IsFaceDown)
				{
					bool isCanUseGold = BattleManager.Instance.IsCanUseGold(playerIndex, card.PlayerCardData.Cost);	
					if(isCanUseGold) return true;
				}
			}
		}

		return false;
	}

	public bool IsSelectHandCard(string playerCardID)
	{
		UniqueCardData uniqueCard;
		PlayerIndex playerIndex;
		bool isFound = FindUniqueCard(_HandCardPopupUI_UniqueCardID, out uniqueCard, out playerIndex);
		if(isFound)
		{
			if(string.Compare(uniqueCard.PlayerCardData.PlayerCardID, playerCardID) == 0)
			{
				return true;
			}	
		}

		return false;
	}

	public bool IsSelectBattleCard(string playerCardID)
	{
		BattleCardData battleCard;
		bool isFound = FindBattleCard(_BattleCardPopupUI_BattleCardID, out battleCard);
		if(isFound)
		{
			if(string.Compare(battleCard.PlayerCardData.PlayerCardID, playerCardID) == 0)
			{
				return true;
			}	
		}

		return false;
	}

	public bool IsShowInfoCard(string playerCardID)
	{
		if(_battleUIManager.FullCardInfoUI.IsShowCard (playerCardID))
		{
			return true;
		}

		return false;
	}

	private PlayerIndex GetNextPlayerIndex()
	{
		switch(CurrentActivePlayerIndex)
		{
			case PlayerIndex.One: return PlayerIndex.Two;
			case PlayerIndex.Two: return PlayerIndex.One;
		}

		return PlayerIndex.One;
	}

	public void LocalRequestNextPhase(bool isForceNextPhase = false)
	{        
		_battleUIManager.NextPhaseButton.enabled = false;
		
		IBattleControlController localPlayer;
		bool isFound = GetLocalPlayer(out localPlayer);
	
		if(isFound)
		{
			if(CurrentActivePlayerIndex == localPlayer.GetPlayerIndex())
			{
				if(((IsTimeout || IsPlayerCanAction) && IsLocalCanRequestNextPhase()))
				{
					bool isSuccess = RequestNextPhase(
                          localPlayer.GetPlayerIndex()
                        , BattleManager.Instance.Turn
                        , BattleManager.Instance.CurrentBattlePhase
                    );
                    
                    if(isSuccess)
                    {
                        SoundManager.PlayEFXSound(BattleController.Instance.ButtonSound);
                    }
                    else
                    {
                        Debug.Log("BattleManager/LocalRequestNextPhase: Failed to request next phase.");
                    }
				}
			}
            else
            {
                Debug.LogWarning("BattleManager/LocalRequestNextPhase: Not player turn.");
            }
		}
		else
		{
			Debug.LogError("BattleManager/LocalRequestNextPhase: Not found local player.");
		}

		//_battleUIManager.NextPhaseButton.enabled = true;
	}
    
    private bool IsLocalCanRequestNextPhase()
    {
        if(KOSNetwork.NetworkSetting.IsNetworkEnabled 
           && KOSNetwork.GamePlayer.localPlayer.IsCmdNextPhase)
        {
            return false;
        }
        
        if(!ActionManager.Instance.IsEmpty)
        {
            return false;
        }
    
        if(!IsRequestNextPhase)
        {
            switch(CurrentBattlePhase)
            {
                case BattlePhase.PreBattle:
                case BattlePhase.Battle:
                case BattlePhase.PostBattle:
                {
                    return true;
                }
            }
        }
    
        return false;
    }
		
	private void NextPhase()
	{
        _battlefieldData.NextPhase();
		ShowChangePhase();

        //OnBattlePhaseUpdateEvent();
	}

	public bool FindUniqueCard(int uniqueCardID, out UniqueCardData resultCard, out PlayerIndex ownerPlayer)
	{
		return _battlefieldData.FindCard(uniqueCardID, out resultCard, out ownerPlayer);
	}

	public bool FindBattleCard(int battleCardID, out BattleCardData battleCardData)
	{
		return _battlefieldData.FindBattleCard(battleCardID, out battleCardData);
	}

	/*
	public bool FindBattleCard(string cardID, out List<BattleCardData> resultList)
	{
		return _battlefieldData.FindAllBattleCard(cardID, out resultList);
	}

	public bool FindBattleCard(PlayerIndex playerIndex, string cardID, out List<BattleCardData> resultList)
	{
		return _battlefieldData.FindAllBattleCard(playerIndex, cardID, out resultList);
	}
	*/
		
	public bool GetTargetList(PlayerIndex targetPlayerIndex, out List<BattleCardData> targetList, CheckIsBattleCardCanBeTarget checkIsCanBeTarget = null)
	{
		targetList = new List<BattleCardData>();

		List<BattleCardData> checkList;
		_battlefieldData.PlayersData[(int)targetPlayerIndex].GetAllSummonedCard(out checkList);
		if(checkList.Count > 0)
		{
			foreach(BattleCardData card in checkList)
			{
				if(checkIsCanBeTarget != null)
				{
					bool isOK = checkIsCanBeTarget.Invoke(card);
					if(isOK)
					{
						targetList.Add(card);
					}
				}
				else
				{
					targetList.Add(card);
				}
			}
		}

		if(targetList.Count > 0)
		{
			return true;
		}

		targetList = null;
		return false;
	}

	public bool GetTargetList(PlayerIndex targetPlayerIndex, CardZoneIndex cardZone, out List<UniqueCardData> targetList, CheckIsUniqueCardCanBeTarget checkIsCanBeTarget = null)
	{
		targetList = new List<UniqueCardData>();
		PlayerBattleData playerBattleData = _battlefieldData.PlayersData[(int)targetPlayerIndex];

		if(cardZone == CardZoneIndex.Deck)
		{
			for(int index = 0; index <playerBattleData.Deck.Count; ++index)
			{
				if(checkIsCanBeTarget != null)
				{
					if(checkIsCanBeTarget.Invoke(playerBattleData.Deck.GetCardAt(index)))
					{
						targetList.Add(playerBattleData.Deck.GetCardAt(index));
					}
				}
				else
				{
					targetList.Add(playerBattleData.Deck.GetCardAt(index));
				}
			}
		}
		else
		{
			List<UniqueCardData> dataList = playerBattleData.GetZoneDataList(cardZone);
			if(dataList != null)
			{
				foreach(UniqueCardData card in dataList)
				{
					if(checkIsCanBeTarget != null)
					{
						if(checkIsCanBeTarget.Invoke(card))
						{
							targetList.Add(card);
						}
					}
					else
					{
						targetList.Add(card);
					}
				}
			}
		}


		if(targetList != null && targetList.Count > 0)
		{
			return true;
		}

		targetList = null;
		return false;
	}

    public bool GetAllBattleCard(out List<BattleCardData> battleCardList, BattleCardFilter filter = null)
    {
        battleCardList = new List<BattleCardData>();

        foreach (PlayerIndex targetPlayerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
        {
            PlayerBattleData playerBattleData = _battlefieldData.PlayersData[(int)targetPlayerIndex];

            foreach(BattleZoneIndex battleZone in System.Enum.GetValues(typeof(BattleZoneIndex)))
            {
                List<BattleCardData> dataList = playerBattleData.GetFieldDataList(battleZone);
                if (dataList != null)
                {
                    foreach (BattleCardData card in dataList)
                    {
                        if(filter != null)
                        {
                            if(filter.FilterResult(card))
                            {
                                battleCardList.Add(card);
                            }
                        }
                        else
                        {
                            battleCardList.Add(card);
                        }
                    }
                }
            }
        }

        if(battleCardList != null && battleCardList.Count > 0)
        {
            return true;
        }

        battleCardList = new List<BattleCardData>();
        return false;
    }

    public bool GetAllUniqueCard(out List<UniqueCardData> uniqueCardList, UniqueCardFilter filter = null)
    {
        uniqueCardList = new List<UniqueCardData>();

        foreach (PlayerIndex targetPlayerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
        {
            PlayerBattleData playerBattleData = _battlefieldData.PlayersData[(int)targetPlayerIndex];

            foreach(CardZoneIndex cardZone in System.Enum.GetValues(typeof(CardZoneIndex)))
            {
                if (cardZone == CardZoneIndex.Deck)
                {
                    for (int index = 0; index < playerBattleData.Deck.Count; ++index)
                    {
                        if (filter != null)
                        {
                            if (filter.FilterResult(playerBattleData.Deck.GetCardAt(index)))
                            {
                                uniqueCardList.Add(playerBattleData.Deck.GetCardAt(index));
                            }
                        }
                        else
                        {
                            uniqueCardList.Add(playerBattleData.Deck.GetCardAt(index));
                        }
                    }
                }
                else
                {
                    List<UniqueCardData> dataList = playerBattleData.GetZoneDataList(cardZone);
                    if (dataList != null)
                    {
                        foreach (UniqueCardData card in dataList)
                        {
                            if (filter != null)
                            {
                                if (filter.FilterResult(card))
                                {
                                    uniqueCardList.Add(card);
                                }
                            }
                            else
                            {
                                uniqueCardList.Add(card);
                            }
                        }
                    }
                }
            }
        }

        if(uniqueCardList != null && uniqueCardList.Count > 0)
        {
            return true;
        }

        uniqueCardList = new List<UniqueCardData>();
        return false;
    }

	public bool CheckHasTargetList(PlayerIndex targetPlayerIndex, CheckIsBattleCardCanBeTarget checkIsCanBeTarget = null)
	{
		List<BattleCardData> checkList;
		_battlefieldData.PlayersData[(int)targetPlayerIndex].GetAllSummonedCard(out checkList);
		if(checkList.Count > 0)
		{
			foreach(BattleCardData card in checkList)
			{
				if(checkIsCanBeTarget != null)
				{
					bool isOK = checkIsCanBeTarget.Invoke(card);
					if(isOK)
					{
						return true;
					}
				}
				else
				{
					return true;
				}
			}
		}

		return false;
	}

	public bool CheckHasTargetList(PlayerIndex targetPlayerIndex, CardZoneIndex cardZone, CheckIsUniqueCardCanBeTarget checkIsCanBeTarget = null)
	{
		PlayerBattleData playerBattleData = _battlefieldData.PlayersData[(int)targetPlayerIndex];

        if (cardZone == CardZoneIndex.Deck)
        {
            for (int index = 0; index < playerBattleData.Deck.Count; ++index)
            {
                if (checkIsCanBeTarget != null)
                {
                    if (checkIsCanBeTarget.Invoke(playerBattleData.Deck.GetCardAt(index)))
                    {
                        return true;
                    }
                }
                else
                {
                    return true;
                }
            }
        }
        else
        {
            List<UniqueCardData> dataList = playerBattleData.GetZoneDataList(cardZone);
            if (dataList != null)
            {
                foreach (UniqueCardData card in dataList)
                {
                    if (checkIsCanBeTarget != null)
                    {
                        if (checkIsCanBeTarget.Invoke(card))
                        {
                            return true;
                        }
                    }
                    else
                    {
                        return true;
                    }
                }
            }
        }

		return false;
	}

	public void AskYesNo(PlayerIndex playerIndex, string question, YesNoPopupUI.OnSelectYes onSelectYes, YesNoPopupUI.OnSelectNo onSelectNo, YesNoPopupUI.OnCancel onCancel, bool isHaveTimeout)
	{
		IBattleControlController player = GetPlayer(playerIndex);
        
        if( (KOSNetwork.NetworkSetting.IsNetworkEnabled) && 
            (playerIndex != KOSNetwork.GamePlayer.localPlayer.playerIndex)
        )
        {
            BattleManager.Instance.RequestWaitActiveQueue();
        
            if(onCancel != null)
            {
                onCancel.Invoke();
            }
            return;
        }

		player.StartAskYesNo(question, onSelectYes, onSelectNo, isHaveTimeout);
	}

    
    private bool _isAskYesNo = false;
    private YesNoPopupUI.OnSelectYes _onSelectYes = null;
    private YesNoPopupUI.OnSelectNo _onSelectNo = null;
	public void ShowAskYesNoUI(string question, YesNoPopupUI.OnSelectYes onSelectYes, YesNoPopupUI.OnSelectNo onSelectNo, bool isHaveTimeout)
	{
        _isAskYesNo = true;
        
        _onSelectYes = onSelectYes;
        _onSelectNo = onSelectNo;
        
		// Show ask yes no popup.
		_battleUIManager.YesNoPopupUI.ShowUI(
			  question
			, this.OnSelectYes
			, this.OnSelectNo
            , isHaveTimeout
		);
	}
    private void OnSelectYes()
    {
        _isAskYesNo = false;
        if(_onSelectYes != null)
        {
            _onSelectYes.Invoke();
        }
    }
    private void OnSelectNo()
    {
        _isAskYesNo = false;
        if(_onSelectNo != null)
        {
            _onSelectNo.Invoke();
        }
    }
    private void AutoSelectYesNo()
    {
        if(_isAskYesNo)
        {        
            _battleUIManager.YesNoPopupUI.AutoSelectYesNo();
        }
    }

	public void AddSuggestText(string text)
	{
		_battleUIManager.SuggestText.AddText(text);
	}

	public void RepeatSuggestText()
	{
		_battleUIManager.SuggestText.RepeatText();
	}

	public void RemoveSuggestText()
	{
		_battleUIManager.SuggestText.RemoveText();
	}

	public void ClearSuggestText()
	{
		_battleUIManager.SuggestText.ClearText();
	}

    private void SubscribeBattleUI()
    {
        _battleUIManager.SetBattleManager(this);
    }

	private void UpdateBattleUI()
	{
        OnBattlePhaseUpdateEvent();
        OnTurnIndexUpdateEvent();
        OnPlayersInfoUpdateEvent();
	}

	private void ShowChangePhase()
	{
		if(_displayBattlePhase != GetDisplayBattlePhase())
		{
			_displayBattlePhase = GetDisplayBattlePhase();

			if(_displayBattlePhase == DisplayBattlePhase.Begin)
			{
				_isShowChangePhase = true;

                bool isMeActive;
				if(_isStartPlayGame)
				{
                    isMeActive = IsLocalPlayer(this.CurrentActivePlayerIndex);
				}
				else
				{
                    isMeActive = true;
				}

				if(IsTutorial) _battleUIManager.ShowNextPhaseArrow(false);
                _battleUIManager.ShowPhaseUI(isMeActive, _displayBattlePhase, this.FinishedShowChangePhase);

				return;
			}
		}

		OnBattlePhaseUpdateEvent();
	}

	public void ShowGraveyard(PlayerIndex playerIndex)
	{
		if(!IsLocalPlayer(CurrentActivePlayerIndex)) return;

		List<UniqueCardData> graveyardList = _battlefieldData.PlayersData[(int)playerIndex].GetZoneDataList(CardZoneIndex.Graveyard);

		/*
		string text = "Graveyard player " + playerIndex.ToString() + "\n";
		foreach(UniqueCardData card in graveyardList)
		{
			text += string.Format("{0}:{1}[{2}] "
				, card.UniqueCardID
				, card.PlayerCardData.Name
				, card.PlayerCardData.PlayerCardID
			);
		}

		Debug.Log(text);
		*/

		this.OnShowSelectCardPageUI(
			Localization.Get("BATTLE_GRAVEYARD_HEADER")
			, Localization.Get("BATTLE_GRAVEYARD_DESCRIPTION")
			, graveyardList
			, 0
			, null
			, null
            , false
			, true
		);
	}

	public void ShowRemoveZone(PlayerIndex playerIndex)
	{
		if(!IsLocalPlayer(CurrentActivePlayerIndex)) return;

		List<UniqueCardData> removeZoneList = _battlefieldData.PlayersData[(int)playerIndex].GetZoneDataList(CardZoneIndex.RemoveZone);

		/*
		string text = "Remove zone player " + playerIndex.ToString() + "\n";
		foreach(UniqueCardData card in removeZoneList)
		{
			text += string.Format("{0}:{1}[{2}] "
				, card.UniqueCardID
				, card.PlayerCardData.Name
				, card.PlayerCardData.PlayerCardID
			);
		}

		Debug.Log(text);
		*/

		this.OnShowSelectCardPageUI(
			Localization.Get("BATTLE_REMOVE_HEADER")
			, Localization.Get("BATTLE_REMOVE_DESCRIPTION")
			, removeZoneList
			, 0
			, null
			, null
            , false
			, true
		);
	}

	private void FinishedShowChangePhase()
	{
		_isShowChangePhase = false;
		OnBattlePhaseUpdateEvent();
	}

    //start of insertion vit for save card to firebase server
    private void SaveCardToDB(string cardID)
    {
        StashFirebaseData stashdb;
        string text = "";
        bool foundCard = false;
        int currQty = 1;
        Debug.Log("on save stash cashed");
        try
        {
            Debug.Log("on stash cashed");
            Debug.Log("on stash cashed successfully");

            stashdb = KOSServer.Instance.StashData;

            Debug.Log("stashDB");


            Debug.Log("uid =  " + stashdb.UID);

            foundCard = stashdb.Card.ContainsKey(cardID);


            if (foundCard)
            {
                currQty = stashdb.Card[cardID];
                ++currQty;
            }
            else
            {
                stashdb.Card.Add(cardID, 1);
            }

            KOSServer.UpdateStashData(stashdb, cardID, currQty);
        }

        catch (SystemException e)
        {
            Debug.Log("cash stash erro " + e.Message);
        }
    }
    //end of insertion vit for save card to firebase server

	private void ShowGameResult()
	{
		if(!_isShowEndGame)
		{
			Debug.Log("ShowGameResult");
			if(IsEndGame)
			{
				PlayerIndex winPlayerIndex = PlayerIndex.One;
				bool isWin1 = false;
				bool isWin2 = false;

                if(_isForceWin)
                {
                    switch(_gameWinPlayerIndex)
                    {
                        case PlayerIndex.One: 
                        {
                            isWin1 = true;
                            isWin2 = false;
                            winPlayerIndex = PlayerIndex.One;
                        }
                        break;
                        
                        case PlayerIndex.Two:
                        {
                            isWin1 = false;
                            isWin2 = true;
                            winPlayerIndex = PlayerIndex.Two;
                        }
                        break;
                    }
                }
				else if(_isCantDraw)
				{
					if(_cantDrawPlayerIndex == PlayerIndex.One)
					{
						isWin2 = true;
						winPlayerIndex = PlayerIndex.Two;
					}
					else if(_cantDrawPlayerIndex == PlayerIndex.Two)
					{
						isWin1 = true;
						winPlayerIndex = PlayerIndex.One;
					}
				}
				else
				{
					if(    _battlefieldData.PlayersData[(int)PlayerIndex.One].HitPoint > 0 
						&& _battlefieldData.PlayersData[(int)PlayerIndex.One].Deck.Count > 0
					)
					{
						isWin1 = true;
						winPlayerIndex = PlayerIndex.One;
					}
					else
					{
						isWin1 = false;
					}

					if(    _battlefieldData.PlayersData[(int)PlayerIndex.Two].HitPoint > 0 
						&& _battlefieldData.PlayersData[(int)PlayerIndex.Two].Deck.Count > 0
					)
					{
						isWin2 = true;
						winPlayerIndex = PlayerIndex.Two;
					}
					else
					{
						isWin2 = false;
					}


				}

				if((isWin1 && isWin2) || (!isWin1 && !isWin2))
				{
					//Debug.LogError("BattleManager/ShowGameResult: Both win both lose.");
					return;
				}

				IBattleControlController localPlayer = null;
				bool isFound = GetLocalPlayer(out localPlayer);
				if(!isFound)
				{
					return;
				}

				GameResultUI.GameResultType resultType;
				if(winPlayerIndex == localPlayer.GetPlayerIndex())
				{
					resultType = GameResultUI.GameResultType.Win;
				}
				else 
				{
					if(!isWin1 && !isWin2)
					{
						resultType = GameResultUI.GameResultType.Draw;
					}
					else
					{
						resultType = GameResultUI.GameResultType.Lose;
					}
				}

				//start divide case from here betweeen sololeage and campaigne
				PlayerCardData rewardCard = null;

				PlayerIndex opponentIndex = PlayerIndex.One;
				if(winPlayerIndex == PlayerIndex.One) 	opponentIndex = PlayerIndex.Two;
				else 									opponentIndex = PlayerIndex.One;

				string npcID = _battlefieldData.PlayersData[(int)opponentIndex].ID;
				int winCount = 0;
				int rewardSilverCoin = 0;

				// GA send play time.
				_stopPlayTime = Time.time;
				float playTime = _stopPlayTime - _startPlayTime;
				playTime = playTime / 60.0f;

				GameAnalytics.NewDesignEvent ("BattleWith:"+ npcID +"Interval(Min)", playTime);

				//from solo league
				if (PlayerSave.IsFromSoloLeague ())
				{
					int npcLevel = 0;
					int exp = 0;
					int diamond = 0;
					bool isLevelUp = false;
					int currentExp = PlayerSave.GetPlayerExp ();

					//calculate exp
					MatchingData mm = GameObject.FindObjectOfType<MatchingData> ();
					if (mm != null)
					{
						npcLevel = mm.PlayerData [1].Lv;
						GameObject.Destroy (mm.gameObject); 
					}

					if (resultType == GameResultUI.GameResultType.Win)
					{
						SoundManager.PlayBGM (BattleController.Instance.WinMusic);
						GameAnalytics.NewDesignEvent ("BattleWith:" + npcID + ":Win");

						exp = (npcLevel * 10);
						diamond = (npcLevel * 10);
					} 
					else 
					{
						SoundManager.PlayBGM (BattleController.Instance.LoseMusic);
						GameAnalytics.NewDesignEvent ("BattleWith:" + npcID + ":Lose");

						exp = (npcLevel * 10) / 2;
						diamond = (npcLevel * 10) / 2;
					}

					//calculate level
					int newExp = currentExp + exp;
					int newLv = PlayerLevelDB.GetLevel (newExp);
					int currentLv = PlayerSave.GetPlayerLevel ();

					if (currentLv != newLv) {
						isLevelUp = true;
					} else {
						isLevelUp = false;
					}

					PlayerSave.SavePlayerExp (newExp);

					int currentDiamond = PlayerSave.GetPlayerDiamond() + diamond;
					PlayerSave.SavePlayerDiamond (currentDiamond);

					_battleUIManager.ShowGameSoloResult (
						resultType
						, exp
						, diamond
						, newLv
						, isLevelUp
						, GoToMainMenu
					);
				}                
                else if(
                       KOSNetwork.NetworkSetting.IsNetworkEnabled 
                    || KOSNetwork.NetworkSetting.IsHaveTimeout
                )
                {
                    string imageKey_L = PlayersData[0].ImageName;
                    string imageKey_R = PlayersData[1].ImageName;

                    MatchLogData logData = null;


                    if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
                    {
                        // Normal case
                        logData = new MatchLogData(
                              System.DateTime.UtcNow
                            , PlayersData[0].Name
                            , PlayersData[1].Name
                            , imageKey_L
                            , imageKey_R
                        );
                    }
                    else
                    {
                        // vs bot
                        System.Random r = new System.Random();
                        if (r.Next(0, 100) % 2 == 0)
                        {
                            logData = new MatchLogData(
                                  System.DateTime.UtcNow
                                , PlayersData[0].Name
                                , PlayersData[1].Name
                                , imageKey_L
                                , imageKey_R
                            );
                        }
                        else
                        {
                            logData = new MatchLogData(
                                  System.DateTime.UtcNow
                                , PlayersData[1].Name
                                , PlayersData[0].Name
                                , imageKey_R
                                , imageKey_L
                            );
                        }
                    }
                            
                    if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
                    {
                        if(KOSNetwork.GamePlayer.localPlayer.isServer)
                        {            
                            KOSServer.RequestSubmitMatchLog(logData);
                            
                            KOSLobbyManager.Instance.RequestStopHost(); 
                        }
                        else
                        {                
                            KOSLobbyManager.Instance.RequestStopClient();
                        }
                    }
                    else
                    {
                        KOSServer.RequestSubmitMatchLog(logData);
                    }
                    
                    // Hide Wwaiting ui.
                    _battleUIManager.WaitingUI.HideUI();
                    
                    // Get Silver
                    {
                        int currentSilverCoin = PlayerSave.GetPlayerSilverCoin();                        
                        if(resultType == GameResultUI.GameResultType.Win)
                        {
                            rewardSilverCoin = 40;
                        }
                        else
                        {
                            rewardSilverCoin = 20;
                        }
                        
                        PlayerSave.SavePlayerSilverCoin (currentSilverCoin + rewardSilverCoin);
                        KOSServer.RequestAddSilver(rewardSilverCoin, null, null); //add silver to firebase server
                    }
                    
                    // PVP
                    _battleUIManager.ShowPVPResult (resultType, rewardSilverCoin, GoToLobby);
                }				
				else 
                {
                    // Campaign
					if (resultType == GameResultUI.GameResultType.Win) 
                    {
						SoundManager.PlayBGM (BattleController.Instance.WinMusic);
						GameAnalytics.NewDesignEvent ("BattleWith:" + npcID + ":Win");

						// Get Silver
						int currentSilverCoin = PlayerSave.GetPlayerSilverCoin ();
						rewardSilverCoin = 35;
						PlayerSave.SavePlayerSilverCoin (currentSilverCoin + rewardSilverCoin);
                        KOSServer.RequestAddSilver(rewardSilverCoin, null, null); //add silver to firebase server


						if (npcID.Contains ("NPC")) {
							winCount = PlayerSave.GetWinCount (npcID);
							++winCount;
							PlayerSave.SetWinCount (npcID, winCount);
                            KOSServer.RequestAddWinCount(npcID, null, null);

							if (winCount == 1) {
								GameAnalytics.NewDesignEvent ("BattleWith:" + npcID + ":1Win");
							} else if (winCount == 5) {
								GameAnalytics.NewDesignEvent ("BattleWith:" + npcID + ":5Win");
							} else if (winCount == 10) {
								GameAnalytics.NewDesignEvent ("BattleWith:" + npcID + ":10Win");
							} else if (winCount == 15) {
								GameAnalytics.NewDesignEvent ("BattleWith:" + npcID + ":15Win");
							} else if (winCount == 25) {
								GameAnalytics.NewDesignEvent ("BattleWith:" + npcID + ":25Win");
							} else if (winCount == 50) {
								GameAnalytics.NewDesignEvent ("BattleWith:" + npcID + ":50Win");
							}

							Debug.Log ("Wintime: " + winCount);
							if (winCount % 5 == 0 && winCount != 0 && winCount != 50) {
							
								ShopPackData shop = new ShopPackData ("SP0001");

								List<string> resultList;
								shop.RandomByCondition (out resultList, 1, this.IsMassOrCommonOrUncommon);

								rewardCard = new PlayerCardData (new CardData (resultList [0]));

								// add new card to stash
								bool isSuccess = PlayerSave.SavePlayerCardStash (rewardCard.PlayerCardID);
								if (isSuccess) {
									// save new card to collection
									PlayerSave.SavePlayerCardCollection (rewardCard.PlayerCardID);
                                    SaveCardToDB(rewardCard.PlayerCardID);
								}
							} else if (winCount == 50) {
								NPCDBData npcData;
								NPCDB.GetData (npcID, out npcData);

								rewardCard = new PlayerCardData (new CardData (npcData.RewardCardID));

								if (rewardCard != null) {
									// add new card to stash
									bool isSuccess = PlayerSave.SavePlayerCardStash (rewardCard.PlayerCardID);
									if (isSuccess) {
										// save new card to collection
										PlayerSave.SavePlayerCardCollection (rewardCard.PlayerCardID);
                                        SaveCardToDB(rewardCard.PlayerCardID);
									}
								}
							}
						}
					} else {
						// Get Silver
						int currentSilverCoin = PlayerSave.GetPlayerSilverCoin ();
						rewardSilverCoin = 15;
						PlayerSave.SavePlayerSilverCoin (currentSilverCoin + rewardSilverCoin);
                        KOSServer.RequestAddSilver(rewardSilverCoin, null, null); //add silver to firebase server

						SoundManager.PlayBGM (BattleController.Instance.LoseMusic);
						GameAnalytics.NewDesignEvent ("BattleWith:" + npcID + ":Lose");
					}
				
					_battleUIManager.ShowGameResult (
						resultType
					, winCount
					, rewardSilverCoin
					, rewardCard
					, GoToMainMenu
					, GoToRetry
					);
				}

				//end of divide case

				_isShowEndGame = true;
			}
		}
	}

	private List<TutorialData> _tutorialDataList = null;
	private PopupBoxUI.OnClickPopup _onShowFinish = null;
	private bool _isShowTutorial = false;
	private int _showIndex = 0;
	private void ShowTutorialUI(List<TutorialData> tutorialDataList, PopupBoxUI.OnClickPopup onShowFinish)
	{
		_tutorialDataList = tutorialDataList;
		_onShowFinish = onShowFinish;

		_showIndex = 0;
		_isShowTutorial = true;
		ShowTutorialIndex();
	}

	private void ShowTutorialIndex()
	{
		if(_tutorialDataList != null && _tutorialDataList.Count > _showIndex)
		{
			TutorialData data = _tutorialDataList[_showIndex];
			++_showIndex;

			string title = "";
			string message = "";
			string buttonText = "";
			if(LocalizationManager.IsCurrentLanguage(LocalizationManager.Language.Thai))
			{
				title = data.TitleTh;
				message = data.TextTh;
			}
			else
			{
				title = data.TitleEng;
				message = data.TextEng;
			}

			//Debug.Log(message);

			buttonText = Localization.Get(data.ButtonKey);

			if(string.Compare(data.DisplayMode, "DIALOG") == 0)
			{
				ShowPopupMessageUI(
					  message
					, buttonText
					, this.ShowTutorialIndex
					, false
				);
			}
			else
			{
				ShowPopupImageUI(
					  data.ImagePath
					, title
					, data.PageText
					, message
					, buttonText
					, this.ShowTutorialIndex
					, false
				);
			}
		}
		else
		{
			HidePopupUI();
			_isShowTutorial = false;

			if(_onShowFinish != null)
			{
				_onShowFinish.Invoke();
			}
		}
	}

	private void ShowPopupImageUI(string imageFile, string title, string page, string message, string buttonText, PopupBoxUI.OnClickPopup onClick, bool isKey)
	{
		_battleUIManager.PopupBoxUI.ShowImageUI(imageFile, title, page, message, buttonText, onClick, isKey);
	}

	public void ShowPopupMessageUI(string message, string buttonText, PopupBoxUI.OnClickPopup onClick, bool isKey)
	{
		_battleUIManager.PopupBoxUI.ShowMessageUI(message, buttonText, onClick, isKey);
	}

	private void HidePopupUI()
	{
		_battleUIManager.PopupBoxUI.HideUI();
	}

	private bool IsMassOrCommonOrUncommon(CardData data)
	{
		if(data != null)
		{
			if(
				   data.SymbolData == CardSymbolData.CardSymbol.Mass 
				|| data.SymbolData == CardSymbolData.CardSymbol.Common 
				|| data.SymbolData == CardSymbolData.CardSymbol.Uncommon
			)
			{
				return true;
			}
		}

		return false;
	}

	public void GoToMainMenu()
	{
		_battleUIManager.ShowLoading(true);

		MatchingData mm = GameObject.FindObjectOfType<MatchingData>();
		if(mm != null)
		{
			GameObject.Destroy(mm.gameObject); 
		}
        
        CardTextureDB.Clear();			
		LoadScene("MainMenuScene2");
	}
    
    public void GoToLobby()
    {
        _battleUIManager.ShowLoading(true);

        MatchingData mm = GameObject.FindObjectOfType<MatchingData>();
        if(mm != null)
        {
            GameObject.Destroy(mm.gameObject); 
        }
            
        CardTextureDB.Clear();
        
        if(KOSLobbyManager.Instance != null)
        {
            KOSLobbyManager.Instance.FullyShutdown();
        }
        
        LoadScene("LobbyScene");
    }
		
	public void GoToRetry()
	{
		_battleUIManager.ShowLoading(true);

        CardTextureDB.Clear();
		LoadScene(SceneManager.GetActiveScene().name);
	}

	public void LoadScene(string sceneName)
	{
		BattleController.Instance.LoadScene(sceneName);
	}

	public bool MoveAllHandCardTo(PlayerIndex playerIndex, CardZoneIndex targetZone)
	{
		if (targetZone != CardZoneIndex.Hand)
		{
			PlayerBattleData playerData = PlayersData[(int)playerIndex];

			bool isMoved = playerData.MoveAllHandCardTo(targetZone);

			if(isMoved)
			{
				RemoveAllHandCardUI();
				return true;
			}
		}

		return false;
	}

	public List<TutorialData> LoadTutorialDataList(List<string> tutorialCodeList, bool isFirstFlow = true)
	{
		List<TutorialData> tutorialList = new List<TutorialData>();

		foreach(string tutorialCode in tutorialCodeList)
		{
			List<TutorialDBData> dataList;
			bool _isSuccess = TutorialDB.GetDataByCode (tutorialCode, out dataList);
			if (_isSuccess) 
			{
				List<TutorialDBData> showList = new List<TutorialDBData>();

				// Filter not show in first flow.
				foreach(TutorialDBData data in dataList)
				{
					if(isFirstFlow)
					{
						if(data.ShowInFirstFlow > 0)
						{
							showList.Add(data);
						}
					}
					else
					{
						showList.Add(data);
					}
				}

				int page = 0;
				foreach(TutorialDBData data in showList)
				{
					TutorialData tutorialData = new TutorialData(data, (++page).ToString() + "/" + showList.Count.ToString());
					tutorialList.Add(tutorialData);
				}
			}
		}
			
		return tutorialList;
	}
#endregion

#region Request Methods
	public void RequestReNewHandCard(PlayerIndex playerIndex, int handSize = 5)
	{
		RenewHandAction cardAction = new RenewHandAction(playerIndex, handSize);
		ActionManager.Instance.AddFirstAction(cardAction);
	}

	public bool RequestNextPhase(PlayerIndex playerIndex, int turnIndex, BattlePhase phase)
	{
        if(!IsRequestNextPhase)
        {        
    		if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
            {
                if(!KOSNetwork.GamePlayer.localPlayer.IsCmdNextPhase)
                {   
                    /*
                    {
                        if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
                        {
                            KOSNetwork.GamePlayer.localPlayer.CmdPrintLog(string.Format("[{0}]RequestNextPhase: Cmd {0} {1}"
                                , KOSNetwork.GamePlayer.localPlayer.playerIndex.ToString()
                                , BattleManager.Instance.CurrentBattlePhase.ToString()
                            )); 
                        }
                    }
                    */

                    _isRequestNextPhase = true;
                    KOSNetwork.GamePlayer.localPlayer.RequestNextPhase(turnIndex, phase);
                    return true;
                }
                else
                {
                    Debug.Log("BattleManager/RequestNextPhase: Already command request next phase.");
                }
            }
            else
            {
                _isRequestNextPhase = true;

                RequestNextPhase_Local(playerIndex, turnIndex, phase, KOSNetwork.NetworkSetting.IsHaveTimeout);
                return true;
            }
        }
        else
        {
            Debug.Log("BattleManager/RequestNextPhase: Already request next phase.");
        }
        
        return false;
	}
    
    public void RequestNextPhase_Local(PlayerIndex playerIndex, int turnIndex, BattlePhase phase, bool isAddTurnTime, bool isPlayAfterAdd = true)
    {   
        //if(!IsRequestNextPhase)
        {
            _isRequestNextPhase = true;
            
            /*
            {
                if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
                {
                    KOSNetwork.GamePlayer.localPlayer.CmdPrintLog(string.Format("[{0}]RequestNextPhase_Local: AddQueue {1} turn:{2} phase:{2}, current:{3}"
                        , KOSNetwork.GamePlayer.localPlayer.playerIndex.ToString()
                        , playerIndex.ToString()
                        , turnIndex.ToString()
                        , phase.ToString()
                        , CurrentBattlePhase.ToString()
                    )); 
                }
            }
            */
            
            if(isAddTurnTime)
            {
                AddTurnTime();
            }
            
            NextPhaseAction cardAction = new NextPhaseAction(playerIndex, turnIndex, phase);
            ActionManager.Instance.AddLastAction(cardAction, isPlayAfterAdd);
        }
    }

	public void RequestFaceupBattleCard(int battleCardID, bool isAddTurnTime)
	{
        if(isAddTurnTime)
        {
            AddTurnTime();
        }
    
		FaceUpBattleCard cardAction = new FaceUpBattleCard(battleCardID);
		ActionManager.Instance.AddFirstAction(cardAction);
	}

	public void RequestActBattleCard(int battleCardID)
	{
		ActionActBattleCard(battleCardID);

		/*
		ActBattleCardAction cardAction = new ActBattleCardAction(battleCardID);
		ActionManager.Instance.AddFirstAction(cardAction);
		*/
	}

	public void RequestGetGold(PlayerIndex playerIndex, int gold)
	{
        ActionGetGold(playerIndex, gold);
	}
    
    public void RequestDrawCard(PlayerIndex playerIndex, int amount = 1)
    {
        DrawCardAction cardAction = new DrawCardAction(playerIndex, amount);
        ActionManager.Instance.AddFirstAction(cardAction);
    }

	public void RequestAttackBattleCard(int attackerID, int targetID, bool isAddTurnTime)
	{
        if(isAddTurnTime)
        {
            AddTurnTime();
        }
    
		AttackBattleCardAction cardAction = new AttackBattleCardAction(attackerID, targetID);
		ActionManager.Instance.AddFirstAction(cardAction);
	}

	public void RequestMoveBattleCard(int moveID, BattleZoneIndex targetZone, bool isAddTurnTime, bool isForceMove = false)
	{        
        if(isAddTurnTime)
        {
            AddTurnTime();
        }
    
        ActionMoveBattleCard(moveID, targetZone, isForceMove);
        
        /*
        MoveBattleCardAction cardAction = new MoveBattleCardAction(moveID, targetZone, isForceMove);
        ActionManager.Instance.AddFirstAction(cardAction);
        */
	}
		
	public void RequestMoveUniqueCard(int moveUniqueID, CardZoneIndex targetZone)
	{
		ActionMoveUniqueCard(moveUniqueID, targetZone);

		/*
		MoveUniqueCardAction cardAction = new MoveUniqueCardAction(moveUniqueID, targetZone);
		ActionManager.Instance.AddFirstAction(cardAction);
		*/
	}
		
	public void RequestShuffleDeckBySeed(PlayerIndex playerIndex, int seed)
	{
		ShuffleDeckBySeedAction cardAction = new ShuffleDeckBySeedAction(playerIndex, seed);
		ActionManager.Instance.AddFirstAction(cardAction);
	}
    
    public void RequestShuffleDeck(PlayerIndex playerIndex)
    {
        ShuffleDeckAction cardAction = new ShuffleDeckAction(playerIndex);
        ActionManager.Instance.AddFirstAction(cardAction);
    }

	public void RequestDestroyBattleCard(int destroyID, CardZoneIndex targetZone)
	{
        //ActionDestroyBattleCard(destroyID);

		BattleCardData card;
		FindBattleCard(destroyID, out card);

		if(card != null)
		{
			ActionManager.Instance.SetPauseAction(true);

			DestroyBattleCardAction cardAction = new DestroyBattleCardAction(destroyID, targetZone);
			ActionManager.Instance.AddFirstAction(cardAction);

			if(!card.IsDestroy)
			{
				card.IsDestroy = true;
				if(targetZone == CardZoneIndex.Graveyard)
				{
					card.TriggerOnDestroyEvent();
				}

				card.TriggerOnLeaveEvent();
			}

			ActionManager.Instance.SetPauseAction(false);
		}
		else
		{
			Debug.LogError("not found destroy card.");
		}
	}
    
    public void PopupUseAbilityBattleCard(int battleCardID, int abilityIndex)
    {
        /*
        if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
        {
            KOSNetwork.GamePlayer.localPlayer.CmdUseAbilityBattleCard(battleCardID, abilityIndex);
        }
        else
        */
        {
            RequestUseAbilityBattleCard(battleCardID, abilityIndex, true);
        }
    }
    
    public void RequestUseAbilityBattleCard(int battleCardID, int abilityIndex, bool isUseByPlayer)
    {
        UseAbilityBattleCardAction cardAction = new UseAbilityBattleCardAction(battleCardID, abilityIndex, isUseByPlayer);
        ActionManager.Instance.AddFirstAction(cardAction);
    }
    
    /*
	public void RequestPreformAbilityBattleCard(int battleCardID)
	{
		PreformAbilityBattleCardAction cardAction = new PreformAbilityBattleCardAction(battleCardID);
		ActionManager.Instance.AddFirstAction(cardAction);
	}
    */

	public void RequestAddBuffBattleCard(int battleCardID, CardBuff buff)
	{
		ActionAddBuffBattleCard(battleCardID, buff);

		/*
		AddBuffBattleCardAction cardAction = new AddBuffBattleCardAction(battleCardID, buff);
		ActionManager.Instance.AddFirstAction(cardAction);
		*/
	}

	public void RequestDealDamageBattleCard(int targetID, int damage, bool isPiercing)
	{
		DealDamageBattleCardAction cardAction = new DealDamageBattleCardAction(targetID, damage, isPiercing);
		ActionManager.Instance.AddFirstAction(cardAction);
	}

	public void RequestUseHandCard(int uniqueCardID, bool isUseByPlayer, bool isAddFirst = true)
	{
		UseHandCardAction cardAction = new UseHandCardAction(uniqueCardID, isUseByPlayer);
        if(isAddFirst)
        {
		    ActionManager.Instance.AddFirstAction(cardAction);
        }
        else
        {
            ActionManager.Instance.AddLastAction(cardAction);
        }
	}
    
    /*
    public void RequestPreformUseHandCard(int uniqueCardID, bool isAddFirst = true)
    {
        PreformAbilityHandCardAction cardAction = new PreformAbilityHandCardAction(uniqueCardID);
        if(isAddFirst)
        {
            ActionManager.Instance.AddFirstAction(cardAction);
        }
        else
        {
            ActionManager.Instance.AddLastAction(cardAction);
        }
    }
    */
    
    public void RequestSummonUniqueCard(int uniqueCardID, bool isAddTurnTime, LocalBattleZoneIndex localZone = LocalBattleZoneIndex.PlayerBase, bool isFree = false, bool isFacedown = false)
    {     
        //Debug.Log("RequestSummonUniqueCard : " + uniqueCardID);    
   
        if(isAddTurnTime)
        {
            AddTurnTime();
        }
   
        SummonUniqueCardAction cardAction = new SummonUniqueCardAction(uniqueCardID, localZone, isFree, false, isFacedown);
        ActionManager.Instance.AddFirstAction(cardAction);
    }
    
    public void RequestActionAbilityUniqueCard(int uniqueCardID, int abilityIndex, List<string> paramList, bool isAddTurnTime)
    {
        Debug.Log("RequestActionAbilityUniqueCard : " + uniqueCardID);    
   
        if(isAddTurnTime)
        {
            AddTurnTime();
        }
   
        ActionAbilityUniqueCardAction cardAction = new ActionAbilityUniqueCardAction(uniqueCardID, abilityIndex, paramList);
        //cardAction.Action();
        ActionManager.Instance.AddFirstAction(cardAction);
    }
    
    public void RequestActionAbilityBattleCard(int battleCardID, int abilityIndex, List<string> paramList, bool isAddTurnTime)
    {
        //Debug.Log("RequestActionAbilityBattleCard : " + battleCardID);    
   
        if(isAddTurnTime)
        {
            AddTurnTime();
        }
        ActionAbilityBattleCardAction cardAction = new ActionAbilityBattleCardAction(battleCardID, abilityIndex, paramList);
        //cardAction.Action();
        ActionManager.Instance.AddFirstAction(cardAction);
    }

    public void RequestWaitActiveQueue()
    {
        //Debug.Log("RequestActionAbilityBattleCard : " + battleCardID);    
        
        //SetPauseTimeout(true);
        ActionWaitActiveQueue();
   
        /*
        WaitActiveQueueAction cardAction = new WaitActiveQueueAction();
        ActionManager.Instance.AddFirstAction(cardAction);
        */
    }
    
    public void RequestResumeActiveQueue()
    {
        //Debug.Log("RequestActionAbilityBattleCard : " + battleCardID);   
        
        //SetPauseTimeout(false);
        ActionResumeActiveQueue(); 
   
        /*
        ResumeActiveQueueAction cardAction = new ResumeActiveQueueAction();
        ActionManager.Instance.AddFirstAction(cardAction);
        */
    }

	public bool RequestUpdateBattleCardUI(int battleCardID)
	{
		// update new data.
		BattleCardUI targetUI;
		bool isSuccess = _battleUIManager.FieldUIManager.GetBattleCardUI(battleCardID, out targetUI);
		if(!isSuccess)
		{
			return false;
		}

		targetUI.UpdateBattleCardData();
		return true;
	}

	public void RequestUpdateAllBattleCardUI()
	{
		_battlefieldData.UpdateAllBattleCardUI();
	}
    
    public void RequestAddAction(List<CardAction> cardActionList, bool isAddFirst = true)
    {
        if(isAddFirst)
        {
            for(int index = cardActionList.Count - 1; index >= 0; --index)
            {
                ActionManager.Instance.AddFirstAction(cardActionList[index]);
            }
        }
        else
        {
            for(int index = 0; index < cardActionList.Count; ++index)
            {
                ActionManager.Instance.AddLastAction(cardActionList[index]);
            }
        }
    }
#endregion

#region Action Methods
	public void ActionNextPhase(PlayerIndex playerIndex, int turnIndex, BattlePhase phase)
	{
		if(CurrentActivePlayerIndex == playerIndex 
            && IsCanActionNextPhase() 
            && turnIndex == Turn
            && phase == CurrentBattlePhase
        ) 
		{    
            // Preform next phase.      
            /*  
            {
                if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
                {
                    KOSNetwork.GamePlayer.localPlayer.CmdPrintLog(string.Format("[{0}]ActionNextPhase: Success {1} turn:{2} phase:{3}, @{4}"
                        , KOSNetwork.GamePlayer.localPlayer.playerIndex.ToString()
                        , playerIndex.ToString()
                        , turnIndex.ToString()
                        , phase.ToString()
                        , CurrentBattlePhase.ToString()
                    )); 
                }
            }
            */
            
			NextPhase();
            _isRequestNextPhase = false;
		}
		else
		{   
            if(Turn <= turnIndex)
            {
                // Re-inqueue.
                {
                    if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
                    {
                        KOSNetwork.GamePlayer.localPlayer.CmdPrintLog(string.Format("[{0}]ActionNextPhase: Failed Re-inqueue {1} turn:{2} phase:{3}, @{4}"
                            , KOSNetwork.GamePlayer.localPlayer.playerIndex.ToString()
                            , playerIndex.ToString()
                            , turnIndex.ToString()
                            , phase.ToString()
                            , CurrentBattlePhase.ToString()
                        )); 
                    }
                }
                
                ActionManager.Instance.SetContinueAfterFinish(false);
                RequestNextPhase_Local(playerIndex, turnIndex, phase, false);
            }
            else
            {
                // Reject
                {
                    if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
                    {
                        KOSNetwork.GamePlayer.localPlayer.CmdPrintLog(string.Format("[{0}]ActionNextPhase: Failed Rejected {1} turn:{2} phase:{3}, @{4}"
                            , KOSNetwork.GamePlayer.localPlayer.playerIndex.ToString()
                            , playerIndex.ToString()
                            , turnIndex.ToString()
                            , phase.ToString()
                            , CurrentBattlePhase.ToString()
                        )); 
                    }
                }
                
                _isRequestNextPhase = false;
            }
		}
	}
    
    private bool IsCanActionNextPhase()
    {
        switch(CurrentBattlePhase)
        {
            case BattlePhase.PreBattle:
            case BattlePhase.Battle:
            case BattlePhase.PostBattle:
            {
                return true;
            }
        }
        
        return false;
    }

	public void ActionFaceupBattleCard(int battleCardID)
	{
		BattleCardData battleCard;
		bool isFound = _battlefieldData.FindBattleCard(battleCardID, out battleCard);
		if(isFound)
		{
			this.ActionGetGold(battleCard.OwnerPlayerIndex, -(battleCard.Cost));

			SoundManager.PlayEFXSound(BattleController.Instance.SummonSound);
			battleCard.FaceUp();
			RequestUpdateBattleCardUI(battleCard.BattleCardID);

            if(battleCard.IsCardSubType(CardSubTypeData.CardSubType.Unique))
			{
				List<BattleCardData> summonedList;
				_battlefieldData.PlayersData[(int)battleCard.OwnerPlayerIndex].GetAllSummonedCard(out summonedList);
				foreach(BattleCardData card in summonedList)
				{
					if(
						(battleCard.BattleCardID != card.BattleCardID)
						&& (!card.IsFaceDown)
						&& (string.Compare(card.Name_EN, battleCard.Name_EN) == 0) // Change is same name use english name for all language.
                        && (card.IsCardSubType(CardSubTypeData.CardSubType.Unique))
					)
					{
						this.RequestDestroyBattleCard(card.BattleCardID, CardZoneIndex.Graveyard);
					}
				}
			}
		}
	}

	public bool ActionActBattleCard(int battleCardID)
	{
		BattleCardData card;
		bool isFound = FindBattleCard(battleCardID, out card);
		if(isFound)
		{
			if(card.IsActive)
			{
				card.SetCardActive(false);
				return true;
			}
			else
			{
				Debug.LogError("BattleManager/ActBattleCard: Battle card not active.");
			}
		}
		else
		{
			Debug.LogError("BattleManager/ActBattleCard: Not found battle card.");
		}

		return false;
	}

	public bool ActionGetGold(PlayerIndex playerIndex, int gold)
	{
		PlayerBattleData playerData = _battlefieldData.PlayersData[(int)playerIndex];
		if (playerData.Gold + gold >= 0)
		{
			_battlefieldData.PlayersData[(int)playerIndex].AddGold(gold);

			OnPlayersInfoUpdateEvent();
			return true;
		}

		return false;
	}

	public void ActionReNewHandCard(PlayerIndex playerIndex, int reNewCardNum = 5)
	{
		//IBattleControlController player = GetPlayer(playerIndex);
		PlayerBattleData playerData = PlayersData[(int)playerIndex];

		// Move all hand card to deck (data & UI).
		MoveAllHandCardTo(playerIndex, CardZoneIndex.Deck);

		ActionShuffleDeck(playerIndex);

		for (int index = 0; index < reNewCardNum; ++index)
		{
			// Draw card also create HandCardUI.
			// And move card data to hand card data container.
			bool isSuccess = ActionDrawCard(playerIndex);

			if (!isSuccess)
			{
				Debug.LogError("BattleManager/RequestReNewHandCard: Player " +playerIndex.ToString() + "Can't redraw card.");
			}
		}
	}

	public bool ActionDrawCard(PlayerIndex playerIndex, int amount = 1)
	{
		//if (IsCanDrawCard(playerIndex, amount))
		{
			IBattleControlController player = GetPlayer(playerIndex);
			PlayerBattleData playerBattleData = PlayersData[(int)playerIndex];

			for(int i = 0; i < amount; ++i)
			{
				UniqueCardData card = playerBattleData.Deck.GetCardAt(0);
				if(card != null)
				{
                    /*
                    if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
                    {
                        string text = string.Format("[{0}]{1}: ActionDrawCard : uid({2}), {3}"
                            , (KOSNetwork.GamePlayer.localPlayer.isServer)? "Server" : "Client"
                            , KOSNetwork.GamePlayer.localPlayer.playerIndex.ToString()
                            , card.UniqueCardID
                            , card.PlayerCardData.PlayerCardID
                        );
                        
                        KOSNetwork.GamePlayer.localPlayer.CmdPrintLog(text);
                    }
                    */
                
	                bool isSuccess = playerBattleData.MoveUniqueCard(card.UniqueCardID, CardZoneIndex.Hand);
					if (isSuccess)
					{
						// If local player.
						if (player.IsLocalPlayer())
						{
	                        _battleUIManager.HandUIManager.CreateHandCard(card, OnClickHandCard);
						}

						SoundManager.PlayEFXSound(BattleController.Instance.DrawSound);

						OnPlayersInfoUpdateEvent();
					}
					else
					{
						Debug.LogError("BattleManager/ActionDrawCard: Can't move draw card to hand.");
					}
				}
				else
				{
					// Can't draw end game.
					_isEndGame = true;
					_isCantDraw = true;
					_cantDrawPlayerIndex = playerIndex;

					return false;
				}
			}
			return true;
		}
	}
		
	public void ActionAttackBattleCard(int attackerID, int targetID, OnAttackFinish onAttackFinish)
	{
        //SetPauseTimeout(true);
        
		_onAttackFinish = onAttackFinish;

		BattleCardData attacker;
		BattleCardData target;
		bool isFound = false;

		isFound = _battlefieldData.FindBattleCard(attackerID, out attacker);
		if(!isFound)
		{
			Debug.LogError("BattleManager/OnSelectAttackTarget: not found attacker battle card data.");
			return;
		}


		isFound = _battlefieldData.FindBattleCard(targetID, out target);
		if(!isFound)
		{
			Debug.LogError("BattleManager/OnSelectAttackTarget: not found target battle card data.");
			return;
		}

		_PreformAttackBattleCard(attacker, target);
	}

	public bool ActionMoveBattleCard(int battleCardID, BattleZoneIndex targetZone, bool isForceMove = false)
	{
        BattleCardData battleCard;
        bool isFound = _battlefieldData.FindBattleCard(battleCardID, out battleCard);
        if (isFound)
        {
            PlayerBattleData playerBattleData = _battlefieldData.PlayersData[(int)battleCard.OwnerPlayerIndex];

            bool isSuccess = playerBattleData.MoveBattleCard(battleCardID, targetZone, isForceMove);
            if (!isSuccess)
            {
                Debug.LogError("BattleManager/ActionMoveBattleCard: Failed to move battle card[" + battleCardID.ToString() + "] to " + targetZone.ToString());
                return false;
            }

            bool isMovedUI = _battleUIManager.FieldUIManager.MoveBattleCardUI(battleCardID, targetZone);
            if (!isMovedUI)
            {
                Debug.LogError("BattleManager/ActionMoveBattleCard: Failed to move battle card UI[" + battleCardID.ToString() + "] to " + targetZone.ToString());
                return false;
            }

            RequestUpdateBattleCardUI(battleCardID);
			//RequestUpdateAllBattleCardUI();
            
            return true;
        }
        else
        {
            Debug.LogError("BattleManager/ActionMoveBattleCard: not found battle card[" + battleCardID.ToString() + "]");
            return false;
        }
	}
		
	public bool ActionMoveUniqueCard(int uniqueCardID, CardZoneIndex targetZone)
	{
		UniqueCardData card;
		PlayerIndex playerIndex;

        //Debug.Log("ActionMoveUniqueCard " + uniqueCardID + " " + targetZone);

		bool isFound = this.FindUniqueCard(uniqueCardID, out card, out playerIndex); 
		if(isFound)
		{
			CardZoneIndex sourceZone = card.CurrentZone;
		
			PlayerBattleData playerBattleData = _battlefieldData.PlayersData[(int)playerIndex];

			bool isSuccess = playerBattleData.MoveUniqueCard(uniqueCardID, targetZone);
			if(isSuccess)
			{
				UniqueCardData movedCard;
				playerBattleData.FindUniqueCardByID(uniqueCardID, out movedCard);

				if(sourceZone == CardZoneIndex.Hand)
				{
					// Remove hand card.
					if(IsLocalPlayer(playerIndex))
					{
						// Remove local hand card UI.
						bool isDestroyed = _battleUIManager.HandUIManager.DestroyCardByID(uniqueCardID);
						if(!isDestroyed)
						{
							Debug.LogWarning("BattleManager/ActionMoveUniqueCard: Failed to remove hand card UI.");
							return false;
						}
					}

					OnPlayersInfoUpdateEvent();
				}

				if(movedCard.CurrentZone == CardZoneIndex.Hand)
				{
                    if (IsLocalPlayer(movedCard.OwnerPlayerIndex))
                    {
                        // Create hand card.
                        _CreateHandCardUI(movedCard);
                    }
					OnPlayersInfoUpdateEvent();
				}

				PlayerIndex ownerIndex = movedCard.OwnerPlayerIndex;
				PlayerIndex enemyIndex = PlayerIndex.One;
				if (ownerIndex == PlayerIndex.One)
				{
					enemyIndex = PlayerIndex.Two;
				} 
				else
				{
					enemyIndex = PlayerIndex.One;
				}

				switch(movedCard.CurrentZone)
				{
					case CardZoneIndex.Deck:
					{
						BattleManager.Instance.OnBattleEventTrigger (
							  ownerIndex
							, BattleEventTrigger.BattleTriggerType.PLAYER_CARD_TO_DECK
							, BattleEventTrigger.BattleSide.Friendly
						);

						BattleManager.Instance.OnBattleEventTrigger (
							  enemyIndex
							, BattleEventTrigger.BattleTriggerType.PLAYER_CARD_TO_DECK
							, BattleEventTrigger.BattleSide.Enemy
						);
					}
					break;

					case CardZoneIndex.Hand:
					{
						BattleManager.Instance.OnBattleEventTrigger (
							  ownerIndex
							, BattleEventTrigger.BattleTriggerType.PLAYER_CARD_TO_HAND
							, BattleEventTrigger.BattleSide.Friendly
						);

						BattleManager.Instance.OnBattleEventTrigger (
							  enemyIndex
							, BattleEventTrigger.BattleTriggerType.PLAYER_CARD_TO_HAND
							, BattleEventTrigger.BattleSide.Enemy
						);
					}
					break;

					case CardZoneIndex.Graveyard:
					{
						BattleManager.Instance.OnBattleEventTrigger (
							  ownerIndex
							, BattleEventTrigger.BattleTriggerType.PLAYER_CARD_TO_GRAVEYARD
							, BattleEventTrigger.BattleSide.Friendly
						);

						BattleManager.Instance.OnBattleEventTrigger (
							  enemyIndex
							, BattleEventTrigger.BattleTriggerType.PLAYER_CARD_TO_GRAVEYARD
							, BattleEventTrigger.BattleSide.Enemy
						);
					}
					break;

					case CardZoneIndex.RemoveZone:
					{
						BattleManager.Instance.OnBattleEventTrigger (
							  ownerIndex
							, BattleEventTrigger.BattleTriggerType.PLAYER_CARD_TO_REMOVE_ZONE
							, BattleEventTrigger.BattleSide.Friendly
						);

						BattleManager.Instance.OnBattleEventTrigger (
							  enemyIndex
							, BattleEventTrigger.BattleTriggerType.PLAYER_CARD_TO_REMOVE_ZONE
							, BattleEventTrigger.BattleSide.Enemy
						);
					}
					break;
				}
			}
			else
			{
				Debug.LogWarning("ActionMoveUniqueCard: Error fail to move unique card.");
			}
		}
		else
		{
			Debug.LogWarning("ActionMoveUniqueCard: Error not found unique card.");
		}

		return false;
	}

	public void ActionShuffleDeckBySeed(PlayerIndex playerIndex, int seed)
	{
        UnityEngine.Random.InitState(seed);
        ActionShuffleDeck(playerIndex);
	}
    
    public void ActionShuffleDeck(PlayerIndex playerIndex)
    {
        SoundManager.PlayEFXSound(BattleController.Instance.ShuffleSound);
        _battlefieldData.PlayersData[(int)playerIndex].Deck.Shuffle();
    }

	public bool ActionDestroyBattleCard(int battleCardID, CardZoneIndex targetZone)
	{
		BattleCardData battleCard;
		bool isFound = _battlefieldData.FindBattleCard(battleCardID, out battleCard);
		if(isFound)
		{
            PlayerBattleData playerBattleData = _battlefieldData.PlayersData[(int)battleCard.OwnerPlayerIndex];

            bool isSuccess;

			battleCard.DeinitCard();

            // destroy UI
            isSuccess = this._DestroyBattleCardUI(battleCard.BattleCardID);
            if (!isSuccess)
            {
                Debug.LogError("BattleManager/ActionDestroyBattleCard: Failed to destroy battle card UI.");
                return false;
            }

			if(targetZone == CardZoneIndex.Graveyard)
			{
				SoundManager.PlayEFXSound(BattleController.Instance.DestroySound);
			}
			else if(targetZone == CardZoneIndex.Hand)
			{
				SoundManager.PlayEFXSound(BattleController.Instance.ToHandSound);
			}
			else if(targetZone == CardZoneIndex.RemoveZone)
			{
				SoundManager.PlayEFXSound(BattleController.Instance.RemoveSound);
			}

            // move data to card zone.
            UniqueCardData uniqueCard;
			isSuccess = playerBattleData.MoveBattleCard(battleCard.BattleCardID, targetZone, out uniqueCard);
            if (!isSuccess)
            {
				Debug.LogError("BattleManager/ActionDestroyBattleCard: Failed to move battle card data to ." + targetZone);
                return false;
            }

			if (targetZone == CardZoneIndex.Hand)
			{
				if (IsLocalPlayer(uniqueCard.OwnerPlayerIndex))
				{
					// create hand UI
					this._CreateHandCardUI(uniqueCard);
				}
			}

			RequestUpdateAllBattleCardUI();
			OnPlayersInfoUpdateEvent();

            return true;
		}
		else
		{
			//Debug.Log("BattleManager/ActionDestroyBattleCard: Not found destroy battle card. ID: " + battleCardID.ToString());
		}

		return false;
	}

	public bool ActionDealDamage(int targetID, int damage, bool isPiercing)
	{
		BattleCardData target;
		bool isFound = _battlefieldData.FindBattleCard(targetID, out target);
		if(isFound)
		{
			target.GetDamaged(damage, isPiercing);

			CheckIsDead(target.BattleCardID);

			OnPlayersInfoUpdateEvent();

			return true;
		}
		else
		{
			Debug.LogError("BattleManager/ActionDealDamage: Not found target battle card. ID: " + targetID.ToString());
		}

		return false;
	}

	public void ActionUseAbilityBattleCard(int battleCardID, int abilityIndex, bool isUseByPlayer, AbilityData.OnFinishedAction onUseAbilityFinish)
	{
		//Debug.Log("ActionUseAbilityBattleCard: " + battleCardID.ToString() + "[" + abilityIndex.ToString() + "]");

		BattleCardData battleCard;
		bool isFound = _battlefieldData.FindBattleCard(battleCardID, out battleCard);
		if(isFound) 
		{
			AbilityData ability;
			battleCard.GetAbility(abilityIndex, out ability);

			//Debug.Log("IsCanTrigger: " + ability.IsCanTrigger());
			//Debug.Log("IsHasTarget: " + ability.IsHasTarget());

			if(ability.IsCanUse())
			{
				/*
				_onFinishUseAbilityBattleCard = onUseAbilityFinish;
				_onCancelUseAbilityBattleCard = onUseAbilityFinish;
				*/
				//ability.SetOnFinishedPrepareCallback(this.OnConfirmedUseHandCard);

				ability.SetOnCanceledPrepareCallback(new AbilityData.OnCanceledPrepare(onUseAbilityFinish));
				ability.SetOnFinishedActionCallback(new AbilityData.OnFinishedAction(onUseAbilityFinish));

				ability.StartPrepare(isUseByPlayer);
				return;
			}
			else
			{
				//Debug.LogError("BattleManager/ActionUseAbilityBattleCard: ability can't use. Index: " + abilityIndex.ToString());
			}
		}
		else
		{
			Debug.LogError("BattleManager/ActionUseAbilityBattleCard: Not found battle card data. ID: " + battleCardID.ToString());
		}

		if(onUseAbilityFinish != null)
		{
			onUseAbilityFinish.Invoke();
		}
	}

    /*
    public void ActionPreformAbilityBattleCard(int battleCardID)
    {
        //Debug.Log("ActionUseAbilityBattleCard: " + battleCardID.ToString() + "[" + abilityIndex.ToString() + "]");

        BattleCardData battleCard;
        bool isFound = _battlefieldData.FindBattleCard(battleCardID, out battleCard);
        if(isFound) 
        {
            // Preform Use Ability
            List<ShowUseAbilityUI.PerformUseAction> stepList = new List<ShowUseAbilityUI.PerformUseAction>();

            ShowUseAbilityUI.PerformUseAction step = new ShowUseAbilityUI.PerformUseAction();
            step.PopupText = "";

            stepList.Add(step);

            BattleManager.Instance.PreformUseAbility(battleCard.PlayerCardData, stepList, null);
        }
        else
        {
            Debug.LogError("BattleManager/ActionPreformAbilityBattleCard: Not found battle card data. ID: " + battleCardID.ToString());
        }
    }
    */
    
	public void ActionAddBuffBattleCard(int battleCardID, CardBuff buff)
	{
		BattleCardData battleCard;
		bool isFound = _battlefieldData.FindBattleCard(battleCardID, out battleCard);
		if(isFound) 
		{
			battleCard.AddBuff(buff);
		}
	}
		
	public void ActionUseHandCard(int uniqueCardID, bool isUseByPlayer, AbilityData.OnFinishedAction onUseAbilityFinish)
	{
		PlayerIndex ownerPlayer;
		UniqueCardData uniqueCardData;

		// Find use hand card data.
		bool isFound = _battlefieldData.FindCardAtZone(uniqueCardID, CardZoneIndex.Hand, out uniqueCardData, out ownerPlayer);
		if(isFound)
		{
			if(IsCanUseHandCard(uniqueCardID))
			{
				// Action
				AbilityData ability = uniqueCardData.AbilityList[0];
				_useHandCardUniqueID = uniqueCardData.UniqueCardID;

				_onFinishUseHandCard = new OnFinishUseHandCard(onUseAbilityFinish);
				_onCancelUseHandCard = new OnCancelUseHandCard(onUseAbilityFinish);

				ability.SetOnFinishedPrepareCallback(this.ConfirmUseHandCard);
				ability.SetOnCanceledPrepareCallback(this.CancelUseHandCard);

				ability.SetOnFinishedActionCallback(this.AfterUseHandCard);

				ability.StartPrepare(isUseByPlayer);
				return;
			}
			else
			{
				Debug.LogError("BattleManager/ActionUseHandCard:Can't use hand card data.");
			}
		}
		else
		{
			Debug.LogError("BattleManager/ActionUseHandCard: Not found hand card data.");
		}

		if(onUseAbilityFinish != null)
		{
			onUseAbilityFinish.Invoke();
		}
	}

    /*
    public void ActionPreformUseHandCard(int uniqueCardID)
    {
        //Debug.Log("ActionUseAbilityBattleCard: " + battleCardID.ToString() + "[" + abilityIndex.ToString() + "]");

        PlayerIndex ownerPlayer;
        UniqueCardData uniqueCardData;

        // Find use hand card data.
        bool isFound = _battlefieldData.FindCardAtZone(uniqueCardID, CardZoneIndex.Hand, out uniqueCardData, out ownerPlayer);
        if(isFound)
        {
            // Preform Use Ability
            List<ShowUseAbilityUI.PerformUseAction> stepList = new List<ShowUseAbilityUI.PerformUseAction>();

            ShowUseAbilityUI.PerformUseAction step = new ShowUseAbilityUI.PerformUseAction();
            step.PopupText = "";

            stepList.Add(step);

            BattleManager.Instance.PreformUseAbility(uniqueCardData.PlayerCardData, stepList, null);
        }
        else
        {
            Debug.LogError("BattleManager/ActionPreformUseHandCard: Not found unique card data. ID: " + uniqueCardID.ToString());
        }
    }
    */

	public void ActionSummonUniqueCard(int uniqueCardID, LocalBattleZoneIndex localZone, bool isFree, bool isLeader, bool isFacedown, bool isPlaySound = true)
	{
        //Debug.Log("ActionSummonUniqueCard " + uniqueCardID + " " + localZone);

		PlayerIndex ownerIndex;
		UniqueCardData uniqueCard;
        CardZoneIndex sourceZone;

		// Find summon hand card data.
		bool isFound = _battlefieldData.FindCard(uniqueCardID, out uniqueCard, out ownerIndex);
		if(!isFound)
		{
			Debug.LogError("BattleManager/ActionSummonHandCard: Not found summon hand card data.");
            return;
		}

        //Debug.Log(uniqueCard.UniqueCardID + " " + ownerIndex);

        sourceZone = uniqueCard.CurrentZone;

        // Find target field.
		BattleZoneIndex targetField = BattleZoneIndex.Battlefield;
		switch(localZone)
		{
			case LocalBattleZoneIndex.PlayerBase:
			{
				switch(ownerIndex)
				{
					case PlayerIndex.One: targetField = BattleZoneIndex.BaseP1; break;
					case PlayerIndex.Two: targetField = BattleZoneIndex.BaseP2; break;
					default: 
					{
						Debug.LogError("BattleManager/ActionSummonHandCard: targetfield is not basefield.");
						return; 
					}
				}
			}
			break;

			case LocalBattleZoneIndex.Battlefield:
			{
				targetField = BattleZoneIndex.Battlefield;
			}
			break;

			case LocalBattleZoneIndex.EnemyBase:
			{
				switch(ownerIndex)
				{
					case PlayerIndex.One: targetField = BattleZoneIndex.BaseP2; break;
					case PlayerIndex.Two: targetField = BattleZoneIndex.BaseP1; break;
					default: 
					{
						Debug.LogError("BattleManager/ActionSummonHandCard: targetfield is not basefield.");
						return; 
					}
				}
			}
			break;
		}

        PlayerBattleData playerBattleData = _battlefieldData.PlayersData[(int)ownerIndex];
        BattleCardData battleCard;
        bool isSuccess = playerBattleData.MoveUniqueCard(uniqueCard.UniqueCardID, targetField, isLeader, isFacedown, out battleCard);
        if (isSuccess)
        { 
            if(!isFree)
			{
				this.ActionGetGold(battleCard.OwnerPlayerIndex, -(battleCard.Cost));
			}

            if (sourceZone == CardZoneIndex.Hand)
            {
                // Remove hand card UI.
                bool isRemoved = this._DestroyHandCardUI(uniqueCard.UniqueCardID, ownerIndex);

                if (!isRemoved)
                {
                    Debug.LogError("BattleManager/ActionSummonHandCard: Failed to remove hand card UI.");
                    return;
                }
            }

            //Create battle card UI.
            bool isCreated = this._CreateBattleCardUI(battleCard, targetField);
            if (!isCreated)
            {
                Debug.LogError("BattleManager/ActionSummonHandCard: Failed to create battle card UI.");
                return;
            }

			if(isPlaySound)
			{
				SoundManager.PlayEFXSound(BattleController.Instance.SummonSound);
			}

            if(battleCard.IsCardSubType(CardSubTypeData.CardSubType.Unique))
			{
				List<BattleCardData> summonedList;
				_battlefieldData.PlayersData[(int)battleCard.OwnerPlayerIndex].GetAllSummonedCard(out summonedList);
				foreach(BattleCardData card in summonedList)
				{
					if(
						   (battleCard.BattleCardID != card.BattleCardID)
						&& (!card.IsFaceDown)
						&& (string.Compare(card.Name_EN, battleCard.Name_EN) == 0) // Change is same name use english name for all language.
                        && (card.IsCardSubType(CardSubTypeData.CardSubType.Unique))
					)
					{
						this.RequestDestroyBattleCard(card.BattleCardID, CardZoneIndex.Graveyard);
					}
				}
			}

            // Trigger summoned event.
			BattleManager.Instance.OnBattleEventTrigger(
			      battleCard.OwnerPlayerIndex
				, BattleEventTrigger.BattleTriggerType.CARD_BE_SUMMONED
				, battleCard.BattleCardID
			);

			BattleManager.Instance.OnBattleEventTrigger(
				  battleCard.OwnerPlayerIndex
				, BattleEventTrigger.BattleTriggerType.CARD_BE_SUMMONED
				, BattleEventTrigger.BattleSide.Friendly
			);


			PlayerIndex enemyIndex;
			if(battleCard.OwnerPlayerIndex == PlayerIndex.One)
			{
				enemyIndex = PlayerIndex.Two;
			}
			else
			{
				enemyIndex = PlayerIndex.One;
			}

			BattleManager.Instance.OnBattleEventTrigger(
			      enemyIndex
				, BattleEventTrigger.BattleTriggerType.CARD_BE_SUMMONED
				, BattleEventTrigger.BattleSide.Enemy
			);
            
            OnPlayersInfoUpdateEvent();
        }
		else
		{
			Debug.LogError("BattleManager/ActionSummonHandCard: Failed to summon battle card.");
		}
	}
    
    public void ActionActionAbilityUniqueCard(int uniqueCardID, int abilityIndex, List<string> paramList, AbilityData.OnFinishedAction onUseAbilityFinish)
    {
        PlayerIndex ownerIndex;
        UniqueCardData uniqueCard;

        // Find summon hand card data.
        bool isFound = _battlefieldData.FindCard(uniqueCardID, out uniqueCard, out ownerIndex);
        if(!isFound)
        {
            Debug.LogError("BattleManager/ActionAbilityUniqueCard: Not found unique card data.");
            return;
        }
        if(!uniqueCard.IsInitAbility)
        {
            uniqueCard.InitAbility();
        }
        AbilityData ability = uniqueCard.AbilityList[abilityIndex];
        
        // Action
        _useHandCardUniqueID = uniqueCard.UniqueCardID;

        _onFinishUseHandCard = new OnFinishUseHandCard(onUseAbilityFinish);
        _onCancelUseHandCard = new OnCancelUseHandCard(onUseAbilityFinish);

        ability.SetOnFinishedPrepareCallback(this.ConfirmUseHandCard);
        ability.SetOnCanceledPrepareCallback(this.CancelUseHandCard);

        ability.SetOnFinishedActionCallback(this.AfterUseHandCard);

        ability.StartAction(paramList);
    }
    
    public void ActionActionAbilityBattleCard(int battleCardID, int abilityIndex, List<string> paramList, AbilityData.OnFinishedAction onUseAbilityFinish)
    {
        BattleCardData battleCard;
        bool isFound = _battlefieldData.FindBattleCard(battleCardID, out battleCard);
        if(!isFound)
        {
            Debug.Log("BattleManager/ActionAbilityBattleCard: Not found battle card data.");
            
            if(onUseAbilityFinish != null)
            {
                onUseAbilityFinish.Invoke();
            }
            return;
        }

        AbilityData ability = battleCard.AbilityList[abilityIndex];
        
        ability.SetOnCanceledActionCallback(new AbilityData.OnCanceledAction(onUseAbilityFinish));
        ability.SetOnFinishedActionCallback(new AbilityData.OnFinishedAction(onUseAbilityFinish));
        
        ability.StartAction(paramList);
    }
    
    public void ActionWaitActiveQueue()
    {
        ActionManager.Instance.SetQueueActive(false);
    }
    
    public void ActionResumeActiveQueue()
    {
        ActionManager.Instance.SetQueueActive(true);
    }
#endregion

#region BattleManagerEvent Properties
	private DisplayBattlePhase _currentDisplayPhase = DisplayBattlePhase.End;
#endregion

#region BattleManagerEvent Methods
    private void OnBattlePhaseUpdateEvent()
    {
		//Debug.Log("Phase Update!");
        _onBattlePhaseUpdateDelagate.Invoke(CurrentBattlePhase);
		OnUpdate();
    }

    public void SubscribeBattlePhaseUpdateEvent(OnBattlePhaseUpdateDelagate action, bool callAfterSubscribe = false)
    {
        _onBattlePhaseUpdateDelagate += action;

        if (callAfterSubscribe)
        {
            action.Invoke(CurrentBattlePhase);
        }
    }

    public void UnSubscribeBattlePhaseUpdateEvent(OnBattlePhaseUpdateDelagate action)
    {
        _onBattlePhaseUpdateDelagate -= action;
    }
		
    private void OnTurnIndexUpdateEvent()
    {
        _onTurnIndexUpdateDelagate.Invoke(Turn);
    }

    public void SubscribeTurnIndexUpdateEvent(OnTurnIndexUpdateDelagate action, bool callAfterSubscribe = false)
    {
        _onTurnIndexUpdateDelagate += action;

        if (callAfterSubscribe)
        {
            action.Invoke(Turn);
        }
    }

    public void UnSubscribeTurnIndexUpdateEvent(OnTurnIndexUpdateDelagate action)
    {
        _onTurnIndexUpdateDelagate -= action;
    }

    private void OnPlayersInfoUpdateEvent()
    {
        _onPlayersInfoUpdateDelagate.Invoke(PlayersData);
    }

    public void SubscribePlayersInfoUpdateEvent(OnPlayersInfoUpdateDelagate action, bool callAfterSubscribe = false)
    {
        _onPlayersInfoUpdateDelagate += action;

        if (callAfterSubscribe)
        {
            action.Invoke(PlayersData);
        }
    }

    public void UnSubscribePlayersInfoUpdateEvent(OnPlayersInfoUpdateDelagate action)
    {
        _onPlayersInfoUpdateDelagate -= action;    
    }
#endregion

#region SetupData Properties
    private PlayerData[] _playerData = null;
    private bool[] _isSetPlayerData = null;
#endregion

#region SetupData Methods
	private void StartSetupData()
	{
		/*
        _playerData = new PlayerData[2];
        _isSetPlayerData = new bool[2];
        _isSetPlayerData[(int)PlayerIndex.One] = false;
        _isSetPlayerData[(int)PlayerIndex.Two] = false;
    	*/

        //Debug.Log("StartSetupData");
		NextPhase(); // Change to WaitSetupData. For Ready to setup data. 

        // Create Dummy Player Data for network
        if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
        {
            //if(KOSNetwork.GamePlayer.localPlayer.isServer)
            {
				DeckData deck;
				bool isSuccess = PlayerSave.GetPlayerDeck(out deck);
				string name = "";
                int randomSeed = UnityEngine.Random.Range(0, System.DateTime.Now.Millisecond);
                
                /*
                // For Debug
                {
                    deck = new DeckData();
                    for(int i=0; i<20; ++i)
                    {
                        deck.Add(new PlayerCardData("C0001")); // Airport
                        //deck.Add(new PlayerCardData("C0002")); // Angel of Earth
                        //deck.Add(new PlayerCardData("C0004")); // Hydro Power Plant
                        //deck.Add(new PlayerCardData("C0005")); // Service Bot
                        //deck.Add(new PlayerCardData("C0006")); // Warp Gate
                        //deck.Add(new PlayerCardData("C0007")); // Di alno Troop
                        //deck.Add(new PlayerCardData("C0008")); // Energy Recharger
                        //deck.Add(new PlayerCardData("C0011")); // Mutant Drone
                        //deck.Add(new PlayerCardData("C0012")); // Soul Crystal Mine
                        //deck.Add(new PlayerCardData("C0014")); // Detonation
                        //deck.Add(new PlayerCardData("C0015")); // Exploration
                        //deck.Add(new PlayerCardData("C0016")); // Fire Slash
                        //deck.Add(new PlayerCardData("C0018")); // Fusion Power Plant
                        //deck.Add(new PlayerCardData("C0019")); // Giant Rabbit
                        //deck.Add(new PlayerCardData("C0022")); // Rebirth
                        //deck.Add(new PlayerCardData("C0025")); // Angel of Dew
                        //deck.Add(new PlayerCardData("C0024")); // Tornado
                        //deck.Add(new PlayerCardData("C0026")); // Angel of Fire
                        //deck.Add(new PlayerCardData("C0027")); // Angel of Wind
                        //deck.Add(new PlayerCardData("C0028")); // Harvester
                        //deck.Add(new PlayerCardData("C0029")); // Interceptor
                        //deck.Add(new PlayerCardData("C0030")); // Di alno Crusader
                        //deck.Add(new PlayerCardData("C0031")); // Di alno Scout
                        //deck.Add(new PlayerCardData("C0033")); // Razor Hound
                        //deck.Add(new PlayerCardData("C0034")); // Corpse Eater
                        //deck.Add(new PlayerCardData("C0035")); // Mutant Soldier
                        //deck.Add(new PlayerCardData("C0036")); // Dam Failure
                        //deck.Add(new PlayerCardData("C0037")); // Construction Plan
                        //deck.Add(new PlayerCardData("C0038")); // Item Hunter
                        //deck.Add(new PlayerCardData("C0039")); // Laser Turret
                        //deck.Add(new PlayerCardData("C0040")); // Mist Wall
                        //deck.Add(new PlayerCardData("C0041")); // Berondo Starport
                        //deck.Add(new PlayerCardData("C0042")); // Prototype Xeraph
                        //deck.Add(new PlayerCardData("C0044")); // Davox
                        //deck.Add(new PlayerCardData("C0046")); // Imperial Knight
                        //deck.Add(new PlayerCardData("C0047")); // Nuclear Silo
                        //deck.Add(new PlayerCardData("C0048")); // Kano
                        //deck.Add(new PlayerCardData("C0049")); // Lakuras
                        //deck.Add(new PlayerCardData("C0051")); // Lakuras's Order
                        //deck.Add(new PlayerCardData("C0052")); // Recruitment
                        //deck.Add(new PlayerCardData("C0053")); // Ethan Wright
                        //deck.Add(new PlayerCardData("C0054")); // Marron
                        //deck.Add(new PlayerCardData("C0055")); // Ashura
                        //deck.Add(new PlayerCardData("C0056")); // Sonata Di alno
                        //deck.Add(new PlayerCardData("C0057")); // Lidia Berondo
                        //deck.Add(new PlayerCardData("C0058")); // Maggie
                        //deck.Add(new PlayerCardData("C0059")); // Amie
                        //deck.Add(new PlayerCardData("C0062")); // Spector
                        //deck.Add(new PlayerCardData("C0063")); // Ai Fornax
                        //deck.Add(new PlayerCardData("C0064")); // Diseased Bug
                        //deck.Add(new PlayerCardData("C0065")); // Green Eyes
                        //deck.Add(new PlayerCardData("C0066")); // Strength Seed
                        //deck.Add(new PlayerCardData("C0068")); // Fire Twist
                        //deck.Add(new PlayerCardData("C0069")); // Jungle Trap
                        //deck.Add(new PlayerCardData("C0070")); // Marron's Order
                        //deck.Add(new PlayerCardData("C0071")); // Mist Skira
                        //deck.Add(new PlayerCardData("C0072")); // Mutant Spirit
                        //deck.Add(new PlayerCardData("C0073")); // Plaque Troops
                        //deck.Add(new PlayerCardData("C0074")); // Forbidden Cave
                        //deck.Add(new PlayerCardData("C0075")); // Heat Ray
                        //deck.Add(new PlayerCardData("C0076")); // Hellflame Skira
                        //deck.Add(new PlayerCardData("C0079")); // Undertaker
                        deck.Add(new PlayerCardData("C0080")); // Shadow Apprentice
                        deck.Add(new PlayerCardData("C0082")); // Wild Skira 
                        //deck.Add(new PlayerCardData("C0083")); // Lidia Berondo
                        //deck.Add(new PlayerCardData("C0084")); // Sonata Di alno
                        //deck.Add(new PlayerCardData("C0085")); // Ashura
                        //deck.Add(new PlayerCardData("C0086")); // Crystalline Dragon
                        //deck.Add(new PlayerCardData("C0088")); // Lunar Rosette
                        //deck.Add(new PlayerCardData("C0089")); // Garbage Thrower
                        //deck.Add(new PlayerCardData("C0090")); // Psychic Soldier
                        //deck.Add(new PlayerCardData("C0092")); // Haunted Doll
                        //deck.Add(new PlayerCardData("C0093")); // Razor Panther
                        //deck.Add(new PlayerCardData("C0094")); // Mirror Path
                    }
                    
                    // Base
                    //deck.Add(new PlayerCardData("C0041")); // Berondo Starport
                    //deck.Add(new PlayerCardData("C0043")); // Robotic Factory
                    //deck.Add(new PlayerCardData("C0050")); // Mutant Lair
                    deck.Add(new PlayerCardData("C0060")); // Fortress
                    //deck.Add(new PlayerCardData("C0074")); // Forbidden Cave
                    
                    // Leader
                    deck.Add(new PlayerCardData("C0062")); // Spector
                    //deck.Add(new PlayerCardData("C0088")); // Lunar Rosette
                }
                */
              
                /*
                if(KOSNetwork.GamePlayer.localPlayer.isServer)
                {
                    name = "HOST";
                }
                else
                {
                    name = "CLIENT";
                }
                */
                
                name = PlayerSave.GetPlayerName();
                
                string text = string.Format("[{0}]Before Seed: {1}"
                    , KOSNetwork.GamePlayer.localPlayer.playerIndex.ToString()
                    , randomSeed
                );
                KOSNetwork.GamePlayer.localPlayer.CmdPrintLog(text);
                
				PlayerData profile = new PlayerData(
					  name
					, name
					, PlayerSave.GetPlayerAvatar()
					, PlayerSave.GetPlayerAvatar()
					, PlayerSave.GetPlayerLevel()
					, deck
				);

                KOSNetwork.GamePlayer.localPlayer.RequestSyncPlayerProfile(profile.ToPlayerDataText(), randomSeed);
            }
        }
		else
		{
			MatchingData mm = GameObject.FindObjectOfType<MatchingData>();

			if (mm != null)
			{
				// Create BattleControl in this scene.
				_isTutorial = mm.IsTutorial;
				_battleUIManager.BattleBGUI.SetBGTexture(mm.BGIndex);

				mm.CreateBattleControl();
				SetPlayers(mm.Players, mm.PlayerData);

				// Destroy MatchingManager. It's done its job. RIP for 1 sec.
				//GameObject.Destroy(mm.gameObject, 1.0f); 
			}
			else
			{
                #if UNITY_EDITOR
                MatchMakingManager mmm = GameObject.FindObjectOfType<MatchMakingManager>();              
                mmm.CreateTutorialData("NPC0001");
                
                mm = GameObject.FindObjectOfType<MatchingData>();
                mm.CreateBattleControl();
                SetPlayers(mm.Players, mm.PlayerData);
                #else
				Debug.LogError("BattleManager/StartSetupData: Can't start game. MatchingManager not found in scene.");
                #endif
			}
		}
	}
    
    public void OnSyncPlayerProfile(PlayerIndex playerIndex, string profileDataText)
    {
        //Debug.Log("OnSyncPlayerProfile: " + profileDataText);
		if(_isSetPlayerData == null || _playerData == null)
		{
			 _playerData = new PlayerData[2];
			_isSetPlayerData = new bool[2];
			_isSetPlayerData[(int)PlayerIndex.One] = false;
			_isSetPlayerData[(int)PlayerIndex.Two] = false;
		}

        //if(CurrentBattlePhase == BattlePhase.WaitSetupData)
        {
            if (_isSetPlayerData[(int)playerIndex])
            {
                Debug.LogError("Error: Player(" + playerIndex.ToString() + ") Already set data.");
                return;
            }
            else
            {
				PlayerData data = PlayerData.StrToPlayerData(profileDataText);

                _playerData[(int)playerIndex] = data;
                _isSetPlayerData[(int)playerIndex] = true;

				//Debug.Log("Got player " + playerIndex.ToString() + " data.");
            }
            
            if(_isSetPlayerData[(int)PlayerIndex.One] && _isSetPlayerData[(int)PlayerIndex.Two])
            {            
                MatchMakingManager mmm = GameObject.FindObjectOfType<MatchMakingManager>();
                mmm.CreateMatchingData(
                      _playerData[(int)PlayerIndex.One]
                    , _playerData[(int)PlayerIndex.Two]
                    , 0
                    , false
                    , false
                );
                
                MatchingData mm = GameObject.FindObjectOfType<MatchingData>();

                if (mm != null)
                {
                    // Create BattleControl in this scene.
                    _isTutorial = mm.IsTutorial;
                    _battleUIManager.BattleBGUI.SetBGTexture(mm.BGIndex);
        
                    mm.CreateBattleControl();
                    SetPlayers(mm.Players, mm.PlayerData);
        
                    // Destroy MatchingManager. It's done its job. RIP for 1 sec.
                    //GameObject.Destroy(mm.gameObject, 1.0f); 
                }
                else
                {
                    Debug.LogError("BattleManager/StartSetupData: Can't start game. MatchingManager not found in scene.");
                }
            }
        }
    }

    public void SetRandomSeed(int randomSeed)
    {
        _randomSeed = randomSeed;
        UnityEngine.Random.InitState(_randomSeed);
        
        {
            string text = string.Format("[{0}]Assign Seed: {1}"
                , KOSNetwork.GamePlayer.localPlayer.playerIndex.ToString()
                , _randomSeed
            );
            KOSNetwork.GamePlayer.localPlayer.CmdPrintLog(text);
        }
    }

	public void SetPlayers(IBattleControlController[] players, PlayerData[] playerData)
	{
		if(CurrentBattlePhase == BattlePhase.WaitSetupData)
		{
            //Debug.Log("SetPlayers");

			// TODO: In Multiplayer feature. 
			// Have to find who is Host and who is Client.

			// Set players battle control.
            _players = new IBattleControlController[2]; // two players.
			_players[(int)PlayerIndex.One] = players[(int)PlayerIndex.One];
            _players[(int)PlayerIndex.Two] = players[(int)PlayerIndex.Two];

			// Assign who is host, who is client.
            _players[(int)PlayerIndex.One].SetPlayerIndex(PlayerIndex.One);
            _players[(int)PlayerIndex.Two].SetPlayerIndex(PlayerIndex.Two);

            // Subscribe BattlePhase update event.
            SubscribeBattlePhaseUpdateEvent(_players[(int)PlayerIndex.One].SetBattlePhase, true);
            SubscribeBattlePhaseUpdateEvent(_players[(int)PlayerIndex.Two].SetBattlePhase, true);

			// Set players data.
			List<PlayerData> playerDataList = new List<PlayerData>();
            playerDataList.Add(playerData[(int)PlayerIndex.One]);
            playerDataList.Add(playerData[(int)PlayerIndex.Two]);

            _battlefieldData.SetPlayerDataList(playerDataList);
			SubscribeBattlePhaseUpdateEvent(this.BattleCardPhaseChangeTrigger, false);

            OnPlayersInfoUpdateEvent();
            
            //Debug.Log("SetPlayers 0: " + playerDataList[0].ToPlayerDataText());
            //Debug.Log("SetPlayers 1: " + playerDataList[1].ToPlayerDataText());

			_battleUIManager.ShowLoading(false);
			NextPhase();
		}
		else
		{
			Debug.LogError("BattleManager/SetPlayers: Have setup player data not in WaitSetupData phase.");
		}
	}
#endregion

#region StartTutorIntro Methods
	private void StartTutorIntro()
	{
		//Debug.Log("StartTutorIntro");
		if(IsTutorial)
		{
			//Debug.Log("Show...");
			List<string> tutorialCodeList = new List<string>();
			//tutorialCodeList.Add("T0006");

			List<TutorialData> tutorialDataList = LoadTutorialDataList(tutorialCodeList);

			if(tutorialDataList != null && tutorialDataList.Count > 0)
			{
				ShowTutorialUI(tutorialDataList, this.OnEndTutorIntro);

				NextPhase();
				OnBattlePhaseUpdateEvent();
				return;
			}
		}

		//Debug.Log("Hide...");
		OnEndTutorIntro();
	}

	private void OnEndTutorIntro()
	{
        _battlefieldData.SetPhase(BattlePhase.StartRPSSelect);
		OnBattlePhaseUpdateEvent();
	}
#endregion

#region FindFirst Properties
    private RPSType[] _selectedRPSType = null;
	private bool[] _isSelectedOptionType = null;

	private PlayerIndex _winPlayerIndex;
	private PlayerIndex _playFirstPlayerIndex;
#endregion

#region FindFirst Methods
    private void StartSelectRPS()
	{
		//Debug.Log("FindFirst: StartSelectFindFirstOption");

		// Init FindFirst 
        _selectedRPSType = new RPSType[2];
		_isSelectedOptionType = new bool[2];
		_isSelectedOptionType[(int)PlayerIndex.One] = false;
		_isSelectedOptionType[(int)PlayerIndex.Two] = false;

		// Change to WaitSelectFindfirstOption. For Ready to select findfirst option.
        _battlefieldData.SetPhase(BattlePhase.WaitRPSSelect);
        OnBattlePhaseUpdateEvent();
	}

    public void OnSelectRPS(PlayerIndex playerIndex, RPSType rpsType)
    {
        if (CurrentBattlePhase == BattlePhase.WaitRPSSelect)
        {
            //Debug.Log("OnSelectFindFirstOption");

            if (_isSelectedOptionType[(int)playerIndex])
            {
                Debug.LogError("Error: Player(" + playerIndex.ToString() + ") Already selected.");
                return;
            }
            else
            {
                _battlefieldData.PlayersData[(int)playerIndex].SelectedRPSType = rpsType;
                _isSelectedOptionType[(int)playerIndex] = true;
            }

            if (_isSelectedOptionType[(int)PlayerIndex.One] && _isSelectedOptionType[(int)PlayerIndex.Two])
            {
                // Both player selected option
                RPSResult result = GameUtility.Compare(
                      _battlefieldData.PlayersData[(int)PlayerIndex.One].SelectedRPSType
                    , _battlefieldData.PlayersData[(int)PlayerIndex.Two].SelectedRPSType
                );

                if (IsTutorial)
                {
                    if (result != RPSResult.Win)
                    {
                        switch (_battlefieldData.PlayersData[(int)PlayerIndex.One].SelectedRPSType)
                        {
                            case RPSType.Paper:
                            {
                                _battlefieldData.PlayersData[(int)PlayerIndex.Two].SelectedRPSType = RPSType.Rock;
                            }
                            break;

                            case RPSType.Rock:
                            {
                                _battlefieldData.PlayersData[(int)PlayerIndex.Two].SelectedRPSType = RPSType.Scissors;
                            }
                            break;

                            case RPSType.Scissors:
                            {
                                _battlefieldData.PlayersData[(int)PlayerIndex.Two].SelectedRPSType = RPSType.Paper;
                            }
                            break;
                        }

                        result = RPSResult.Win;
                    }
                }

                _battlefieldData.SetPhase(BattlePhase.ResultRPS);
                OnBattlePhaseUpdateEvent();
            }
            else
            {
                if (BattleManager.Instance.IsLocalPlayer(playerIndex))
                    _battleUIManager.ShowWaitRPSUI(rpsType);
            }
        }
    }

    public void ShowRPSResult()
    {
        if (CurrentBattlePhase == BattlePhase.ResultRPS)
        {
            _battlefieldData.SetPhase(BattlePhase.WaitRPSResult);
            OnBattlePhaseUpdateEvent();

            // Both player selected option
            RPSResult result = GameUtility.Compare(
                  _battlefieldData.PlayersData[(int)PlayerIndex.One].SelectedRPSType
                , _battlefieldData.PlayersData[(int)PlayerIndex.Two].SelectedRPSType
            );

            switch (result)
            {
                case RPSResult.Draw:
                {
                    //Debug.Log("FindFirst Result: Draw!");
                    _isSelectedOptionType[(int)PlayerIndex.One] = false;
                    _isSelectedOptionType[(int)PlayerIndex.Two] = false;

                    // Restart StartSelectFindFirstOption
                    if (BattleManager.Instance.IsLocalPlayer(PlayerIndex.One))
                    {
                        this._battleManagerController.ShowResultRPSUI(
                              _battlefieldData.PlayersData[(int)PlayerIndex.One].SelectedRPSType
                            , _battlefieldData.PlayersData[(int)PlayerIndex.Two].SelectedRPSType
                            , StartSelectRPS
                        );
                    }
                    else
                    {
                        this._battleManagerController.ShowResultRPSUI(
                              _battlefieldData.PlayersData[(int)PlayerIndex.Two].SelectedRPSType
                            , _battlefieldData.PlayersData[(int)PlayerIndex.One].SelectedRPSType
                            , StartSelectRPS
                        );
                    }
                }
                break;

                case RPSResult.Win:
                {
                    // Boardcast OptionResult to all player.
                    foreach (IBattleControlController player in _players)
                    {
                        _winPlayerIndex = PlayerIndex.One;
                        if (player.GetPlayerIndex() == _winPlayerIndex)
                        {
                            player.SetRPSResult(RPSResult.Win);
                        }
                        else
                        {
                            player.SetRPSResult(RPSResult.Lose);
                        }
                    }

                    if (BattleManager.Instance.IsLocalPlayer(PlayerIndex.One))
                    {
                        this._battleManagerController.ShowResultRPSUI(
                              _battlefieldData.PlayersData[(int)PlayerIndex.One].SelectedRPSType
                            , _battlefieldData.PlayersData[(int)PlayerIndex.Two].SelectedRPSType
                            , this.NextPhase
                        );
                    }
                    else
                    {
                        this._battleManagerController.ShowResultRPSUI(
                              _battlefieldData.PlayersData[(int)PlayerIndex.Two].SelectedRPSType
                            , _battlefieldData.PlayersData[(int)PlayerIndex.One].SelectedRPSType
                            , this.NextPhase
                        );
                    }
                }
                break;

                case RPSResult.Lose:
                {
                    // Boardcast OptionResult to all player.
                    foreach (IBattleControlController player in _players)
                    {
                        _winPlayerIndex = PlayerIndex.Two;
                        if (player.GetPlayerIndex() == _winPlayerIndex)
                        {
                            player.SetRPSResult(RPSResult.Win);
                        }
                        else
                        {
                            player.SetRPSResult(RPSResult.Lose);
                        }
                    }

                    if (BattleManager.Instance.IsLocalPlayer(PlayerIndex.One))
                    {
                        this._battleManagerController.ShowResultRPSUI(
                              _battlefieldData.PlayersData[(int)PlayerIndex.One].SelectedRPSType
                            , _battlefieldData.PlayersData[(int)PlayerIndex.Two].SelectedRPSType
                            , this.NextPhase
                        );
                    }
                    else
                    {
                        this._battleManagerController.ShowResultRPSUI(
                              _battlefieldData.PlayersData[(int)PlayerIndex.Two].SelectedRPSType
                            , _battlefieldData.PlayersData[(int)PlayerIndex.One].SelectedRPSType
                            , this.NextPhase
                        );
                    }
                }
                break;
            }
        }
	}

    private void StartSelectPlayOrder()
	{
		//Debug.Log("FindFirst: StartSelectFindFirstPlay");

		// Boardcast show FindFirstPlayUI to all player.
		foreach(IBattleControlController player in _players)
		{
			player.StartSelectPlayOrder();
		}

        // Change to WaitRPSSelect. For Ready to select findfirst play.
        _battlefieldData.SetPhase(BattlePhase.WaitPlayOrderSelect);
		OnBattlePhaseUpdateEvent();
	}

    public void OnSelectPlayOrder(PlayOrderType orderType)
	{
        if(CurrentBattlePhase == BattlePhase.WaitPlayOrderSelect)
		{
			//Debug.Log("OnSelectFindFirstChoosePlay");

			// Find play first player.
			switch(orderType)
			{
                case PlayOrderType.PlayFirst:
				{
					if(_winPlayerIndex == PlayerIndex.One) 	
					{
						_playFirstPlayerIndex = PlayerIndex.One;
					}
					else 									
					{
						_playFirstPlayerIndex = PlayerIndex.Two;
					}
				}
				break;

                case PlayOrderType.DrawFirst:
				{
					if(_winPlayerIndex == PlayerIndex.One) 	
					{
						_playFirstPlayerIndex = PlayerIndex.Two;
					}
					else 									
					{
						_playFirstPlayerIndex = PlayerIndex.One;
					}
				}
				break;
			}

			// Set current active player.
			_currentActivePlayerIndex = _playFirstPlayerIndex;

            _battlefieldData.SetPhase(BattlePhase.ResultPlayOrder);
            OnBattlePhaseUpdateEvent();
		}
		else
		{
			Debug.LogError("BattleManager/OnSelectFindFirstOption: Select RPS when not in WaitRPS phase.");
		}
	}

    private void ShowPlayOrderResult()
    {
        if (CurrentBattlePhase == BattlePhase.ResultPlayOrder)
        {
            _battlefieldData.SetPhase(BattlePhase.WaitPlayOrderResult);
            OnBattlePhaseUpdateEvent();

            if (_playFirstPlayerIndex == PlayerIndex.One)
            {
                if (BattleManager.Instance.IsLocalPlayer(PlayerIndex.One))
                {
                    _battleUIManager.ShowPlayOrderResultUI(
                          PlayOrderType.PlayFirst
                        , NextPhase
                    );
                }
                else
                {
                    _battleUIManager.ShowPlayOrderResultUI(
                          PlayOrderType.DrawFirst
                        , NextPhase
                    );
                }
            }
            else
            {
                if (BattleManager.Instance.IsLocalPlayer(PlayerIndex.One))
                {
                    _battleUIManager.ShowPlayOrderResultUI(
                          PlayOrderType.DrawFirst
                        , NextPhase
                    );
                }
                else
                {
                    _battleUIManager.ShowPlayOrderResultUI(
                          PlayOrderType.PlayFirst
                        , NextPhase
                    );
                }
            }
        }
    }
#endregion

#region SetupBase Properties
	private bool[] _isSelectedBase = null;
    private int[] _selectBaseID = null;
#endregion

#region SetupBase Methods
	private void StartTutorSetupCard()
	{
		if(IsTutorial)
		{
			List<string> tutorialCodeList = new List<string>();
			//tutorialCodeList.Add("T0007");

			List<TutorialData> tutorialDataList = LoadTutorialDataList(tutorialCodeList);

			if(tutorialDataList != null && tutorialDataList.Count > 0)
			{
				Debug.Log("tutorialDataList.Count " + tutorialDataList.Count.ToString());
				ShowTutorialUI(tutorialDataList, this.OnEndTutorSetupCard);

				NextPhase();
				OnBattlePhaseUpdateEvent();
				return;
			}
		}

		OnEndTutorSetupCard();
	}

	private void OnEndTutorSetupCard()
	{
		_battlefieldData.SetPhase(BattlePhase.SetupBase);
		OnBattlePhaseUpdateEvent();
	}

	private void StartSetupBase()
	{
        /*
		_isSelectedBase = new bool[2];
		_isSelectedBase[(int)PlayerIndex.One] = false;
		_isSelectedBase[(int)PlayerIndex.Two] = false;

		_selectBaseID = new int[2];
        */

		_battlefieldData.SetPhase(BattlePhase.WaitSetupBase);
		OnBattlePhaseUpdateEvent();
	}

	public void OnSelectBase(IBattleControlController player, UniqueCardData selectedBaseCard)
	{
		//Debug.Log("SetupBase: OnSelectBase[" + player.GetPlayerIndex().ToString() + "]: " + selectedBaseCard.PlayerCardData.Name);

        if(_isSelectedBase == null || _selectBaseID == null)
        {
            _isSelectedBase = new bool[2];
            _isSelectedBase[(int)PlayerIndex.One] = false;
            _isSelectedBase[(int)PlayerIndex.Two] = false;

            _selectBaseID = new int[2];
        }

		//if(CurrentBattlePhase == BattlePhase.WaitSetupBase)
		{
            PlayerIndex playerIndex = player.GetPlayerIndex();
            if (!_isSelectedBase[(int)playerIndex])
			{
                _selectBaseID[(int)playerIndex] = selectedBaseCard.UniqueCardID;
                _isSelectedBase[(int)playerIndex] = true;

                OnPlayersInfoUpdateEvent();
			}
			else
			{
				Debug.LogError("BattleManager/OnSelectBase: already select base card.");
			}

			if (_isSelectedBase[(int)PlayerIndex.One] && _isSelectedBase[(int)PlayerIndex.Two])
			{
				// Start create base card in battlefield.
				foreach(PlayerIndex pIndex in System.Enum.GetValues(typeof(PlayerIndex)))
				{					
					// summon card.
                    Debug.Log("Select base: " + pIndex.ToString());
					ActionSummonUniqueCard(_selectBaseID[(int)pIndex], LocalBattleZoneIndex.PlayerBase, true, false, false, false);
				}

                _battleUIManager.ShowWaiting(false);
                
				NextPhase();
			}
            else
            {
                // show waiting
                 if (BattleManager.Instance.IsLocalPlayer(playerIndex))
                    _battleUIManager.ShowWaiting(true, true);
            }
		}
	}
#endregion

#region SetupLeader Properties
	private bool[] _isSelectedLeader;
    private int[] _selectLeaderID;
#endregion

#region SetupLeader Methods
	private void StartSetupLeader()
	{
		//Debug.Log("SetupLeader: StartSetupLeader");

        /*
		_isSelectedLeader = new bool[2];
		_isSelectedLeader[(int)PlayerIndex.One] = false;
		_isSelectedLeader[(int)PlayerIndex.Two] = false;

        _selectLeaderID = new int[2];
        */

		_battlefieldData.SetPhase(BattlePhase.WaitSetupLeader);
		OnBattlePhaseUpdateEvent();
	}

	public void OnSelectLeader(IBattleControlController player, UniqueCardData selectedLeaderCard)
	{
		//Debug.Log("SetupLeader: OnSelectLeader[" + player.GetPlayerIndex().ToString() + "]: " + selectedLeaderCard.PlayerCardData.Name);

        if(_isSelectedLeader == null || _selectLeaderID == null)
        {
            _isSelectedLeader = new bool[2];
            _isSelectedLeader[(int)PlayerIndex.One] = false;
            _isSelectedLeader[(int)PlayerIndex.Two] = false;

            _selectLeaderID = new int[2];
        }

		//if(CurrentBattlePhase == BattlePhase.WaitSetupLeader)
		{
            PlayerIndex playerIndex = player.GetPlayerIndex();
            if (!_isSelectedLeader[(int)playerIndex])
			{
                _selectLeaderID[(int)playerIndex] = selectedLeaderCard.UniqueCardID;
                _isSelectedLeader[(int)playerIndex] = true;

                OnPlayersInfoUpdateEvent();
			}
			else
			{
				Debug.LogError("BattleManager/OnSelectLeader: already select leader card.");
			}
				
			if (_isSelectedLeader[(int)PlayerIndex.One] && _isSelectedLeader[(int)PlayerIndex.Two])
			{
                // Start create leader card in battlefield.
                foreach (PlayerIndex pIndex in System.Enum.GetValues(typeof(PlayerIndex)))
                {
                    // summon card.
				    ActionSummonUniqueCard(
                          _selectLeaderID[(int)pIndex]
                        , LocalBattleZoneIndex.PlayerBase
                        , true
                        , true
                        , true
                        , false
                    );  
                }
                
                _battleUIManager.ShowWaiting(false);

				NextPhase();
			}
            else
            {
                // show waiting
                 if (BattleManager.Instance.IsLocalPlayer(playerIndex))
                    _battleUIManager.ShowWaiting(true, true);
            }
		}
	}
#endregion

#region TutorBeforePlay Methods
	private void StartTutorBeforePlay()
	{
		if(IsTutorial)
		{
			List<string> tutorialCodeList = new List<string>();
			//tutorialCodeList.Add("T0008");

			List<TutorialData> tutorialDataList = LoadTutorialDataList(tutorialCodeList);

			if(tutorialDataList != null && tutorialDataList.Count > 0)
			{
				ShowTutorialUI(tutorialDataList, this.OnEndTutorBeforePlay);

				NextPhase();
				OnBattlePhaseUpdateEvent();
				return;
			}
		}

		OnEndTutorBeforePlay();
	}

	private void OnEndTutorBeforePlay()
	{
		_battlefieldData.SetPhase(BattlePhase.InitDrawCard);
		OnBattlePhaseUpdateEvent();
	}
#endregion

#region InitDrawCard Methods
    private void OnInitDrawCard()
	{
		//Debug.Log("InitDrawCard: OnInitDrawCard");

        foreach (IBattleControlController player in _players)
		{
            PlayerBattleData playerBattleData = _battlefieldData.PlayersData[(int)player.GetPlayerIndex()];

			if(!IsTutorial)
			{
				ActionShuffleDeck(player.GetPlayerIndex());
			}
			else
			{
				SoundManager.PlayEFXSound(BattleController.Instance.ShuffleSound);
			}

			for (int index = 0; index < 5; ++index)
			{
				if(this.IsCanDrawCard(player.GetPlayerIndex(), 1))
				{
					ActionDrawCard(player.GetPlayerIndex());
				}
                else
                {
                    Debug.LogError("BattleManager/OnInitDrawCard: Player " + player.GetPlayerIndex().ToString() + "Can't draw card.");
					return;
                }
			}
		}
			
		// Boardcast update player card info.
		OnPlayersInfoUpdateEvent();

		NextPhase();
	}
#endregion

#region Rehand Properties
    bool[] _isReHand; 
#endregion

#region Rehand Methods
    private void OnStartReHand()
    {
		if(_isSkipReHand)
		{
			_battlefieldData.SetPhase(BattlePhase.EndGameSetup);
			OnBattlePhaseUpdateEvent();

			return;
		}

        //Debug.Log("InitDrawCard: OnStartRehand");

        _isReHand = new bool[2];
        _isReHand[(int)PlayerIndex.One] = false;
        _isReHand[(int)PlayerIndex.Two] = false;

        _battlefieldData.SetPhase(BattlePhase.WaitReHand);
        OnBattlePhaseUpdateEvent();

		IBattleControlController localPlayer;
		GetLocalPlayer(out localPlayer);

		// Show ask re-hand popup.
		this.AskYesNo(
			  localPlayer.GetPlayerIndex()
			, "Do you want to mulligan?\n(Renew your hands.)"
			, this.OnYesRehand
			, this.OnNoReHand
            , null
            , true
		);
    }

	private void OnYesRehand()
	{
		IBattleControlController player;
		GetLocalPlayer(out player);
		OnSelectReHand(player, true);
	}

	private void OnNoReHand()
	{
		IBattleControlController player;
		GetLocalPlayer(out player);
		OnSelectReHand(player, false);
	}

    public void OnSelectReHand(IBattleControlController player, bool isReHand)
    {
        if(CurrentBattlePhase == BattlePhase.WaitReHand)
        {
            if (!_isReHand[(int)player.GetPlayerIndex()])
            {
                //Debug.Log("Player " + player.GetPlayerIndex().ToString() + " re-hand : " + isReHand.ToString());

                _isReHand[(int)player.GetPlayerIndex()] = true;

                if (isReHand)
                {
					RequestReNewHandCard(player.GetPlayerIndex());
                }

                if (_isReHand[(int)PlayerIndex.One] && _isReHand[(int)PlayerIndex.Two])
                {
                    NextPhase();
                }
            }
            else
            {
                Debug.LogError("BattleManager/OnSelectReHand: Already re-hand.");
            }
        }
    }
#endregion

#region TutorBeginPlay Methods
	private void StartTutorBeginPlay()
	{
		if(IsTutorial && !_isShowTutorBeginPlay)
		{
			List<string> tutorialCodeList = new List<string>();
			//tutorialCodeList.Add("T0009");
			//tutorialCodeList.Add("T0010");
			//tutorialCodeList.Add("T0011");

			List<TutorialData> tutorialDataList = LoadTutorialDataList(tutorialCodeList);

			if(tutorialDataList != null && tutorialDataList.Count > 0)
			{
				_isShowTutorBeginPlay = true;
				ShowTutorialUI(tutorialDataList, this.OnEndTutorBeginPlay);

				NextPhase();
				OnBattlePhaseUpdateEvent();
				return;
			}
		}

		_isShowTutorBeginPlay = true;
		OnEndTutorBeginPlay();
	}

	private void OnEndTutorBeginPlay()
	{
		_battlefieldData.SetPhase(BattlePhase.BeginInactive);
		OnBattlePhaseUpdateEvent();
	}
#endregion

#region Inactive Phase Methods
	private void OnStartInactive()
	{
		//Debug.Log("Inactive: OnStartInactive");

		ReactiveCard(CurrentActivePlayerIndex);
		NextPhase();
	}

	private void ReactiveCard(PlayerIndex playerIndex)
	{
		//Debug.Log("Inactive: ReactiveCard");

		_battlefieldData.ReactiveAllBattleCard(CurrentActivePlayerIndex);
	}
#endregion

#region Reinterate Phase Methods
	private void OnStartReiterate()
    { 
		//Debug.Log("Reinterate: OnStartReiterate");

		NextPhase();
    }
#endregion

#region Draw Phase Properties
	private bool _isFirstTurn = true;
#endregion

#region Draw Phase Methods
	private void OnStartDraw()
	{
		//Debug.Log("Draw: OnStartDraw");

		if(_isFirstTurn)
		{
			_isFirstTurn = false;
		}
		else
		{
			this.RequestDrawCard(CurrentActivePlayerIndex);
		}
		NextPhase();
	}
#endregion

#region PreBattle Phase Methods
	private void OnStartPhasePreBattle()
	{
		RequestUpdateAllBattleCardUI();
		NextPhase();
	}

	private void OnPhasePreBattle()
	{
		if(IsTutorial)
		{
			if(this.Turn == 1)
			{
				if(!_isShowTutorBeforePlayStructure)
				{
					StartTutorBeforePlayStructure();
				}
				else if(   
					//!_isShowArrowDeimanistFoundry
					_isShowTutorBeforePlayStructure 
					&& this.IsCanSummonCard(CurrentActivePlayerIndex) 
				)
				{
					if(!this.IsSelectHandCard("C0061") && !IsShowInfoCard("C0061"))
					{
						this.ShowHandCardArrow(CurrentActivePlayerIndex, "C0061", true);
					}
					else
					{
						this.ShowHandCardArrow(CurrentActivePlayerIndex, "C0061", false);
					}

					_isShowArrowDeimanistFoundry = true;
				}
				else if(
					   !_isShowTutorAfterPlayStructure
					&& _isShowArrowDeimanistFoundry 
					&& !this.IsCanSummonCard(CurrentActivePlayerIndex) 
				)
				{
					StartTutorAfterPlayStructure();
				}
				else if(_isShowTutorAfterPlayStructure)
				{
					_battleUIManager.ShowNextPhaseArrow(true);
				}
			}

			if(this.Turn == 2)
			{
				if(_isShowTutorAfterPlayStructure)
				{
					_battleUIManager.ShowNextPhaseArrow(false);
				}
				else if(
					!_isShowTutorBotPlayedUnit_1
					&& !this.IsCanSummonCard(CurrentActivePlayerIndex) 
				)
				{
					StartTutorBotPlayedUnit_1();
				}
				else if(
					!_isShowTutorBotMovedUnit_1
					&& _isShowTutorBotPlayedUnit_1 
					&& !this.IsCanMoveBattleCard(CurrentActivePlayerIndex) 

				)
				{
					StartTutorBotMovedUnit_1();
				}
			}

			if(this.Turn == 3)
			{
				if(!_isShowTutorPlayerTurn3)
				{
					StartTutorPlayerTurn3();
				}
				else if(   
					_isShowTutorPlayerTurn3 
					&& this.IsCanSummonCard(CurrentActivePlayerIndex) 
				)
				{
					if(!this.IsSelectHandCard("C0007") && !IsShowInfoCard("C0007"))
					{
						this.ShowHandCardArrow(CurrentActivePlayerIndex, "C0007", true);
					}
					else
					{
						this.ShowHandCardArrow(CurrentActivePlayerIndex, "C0007", false);
					}

					_isShowArrowDialnoTroop_1 = true;
				}
				else if(
					!_isShowTutorPlayerPlayedUnit_1
					&& _isShowArrowDialnoTroop_1 
					&& !this.IsCanSummonCard(CurrentActivePlayerIndex) 
				)
				{
					StartTutorPlayerPlayedUnit_1();
					//this.ShowBattleCardArrow(CurrentActivePlayerIndex, "C0007", true);
				}
				else if(
					!_isShowTutorPlayerSelectedUnit_1
					&& _isShowTutorPlayerPlayedUnit_1 
					&& (!this.IsSelectBattleCard("C0007") && !this.IsShowInfoCard("C0007"))
				)
				{
					this.ShowBattleCardArrow(CurrentActivePlayerIndex, "C0007", true);
				}
				else if(
					!_isShowTutorPlayerSelectedUnit_1
					&& _isShowTutorPlayerPlayedUnit_1 
					&& (this.IsSelectBattleCard("C0007") || this.IsShowInfoCard("C0007"))
				)
				{
					//Debug.Log("StartTutorPlayerSelectedUnit_1");
					this.ShowBattleCardArrow(CurrentActivePlayerIndex, "C0007", false);
					StartTutorPlayerSelectedUnit_1();
				}
				else if(
					!_isShowTutorPlayerSelectMoveUnit_1
					&& _isShowTutorPlayerSelectedUnit_1
					&& !this._battleUIManager.IsShowMoveCardUI() 
				)
				{
					if(!this.IsSelectBattleCard("C0007") && !this.IsShowInfoCard("C0007"))
					{
						this.ShowBattleCardArrow(CurrentActivePlayerIndex, "C0007", true);
					}
					else
					{
						this.ShowBattleCardArrow(CurrentActivePlayerIndex, "C0007", false);
					}

				}
				else if(
					!_isShowTutorPlayerSelectMoveUnit_1
					&& _isShowTutorPlayerSelectedUnit_1 
					&& this._battleUIManager.IsShowMoveCardUI() 
				)
				{
					//Debug.Log("StartTutorPlayerSelectMoveUnit_1");
					this.ShowBattleCardArrow(CurrentActivePlayerIndex, "C0007", false);
					StartTutorPlayerSelectMoveUnit_1();
				}
				else if(
					!_isShowTutorPlayerMovedUnit_1
					&& _isShowTutorPlayerSelectMoveUnit_1 
					&& this.IsCanAttackBattleCard(CurrentActivePlayerIndex)
				)
				{
					//Debug.Log("StartTutorPlayerMovedUnit_1");
					StartTutorPlayerMovedUnit_1();
				}
				else if(_isShowTutorPlayerMovedUnit_1)
				{
					_battleUIManager.ShowNextPhaseArrow(true);
				}
			}

			if(this.Turn == 5)
			{
				if(!_isShowTutorPlayerTurn5)
				{
					StartTutorPlayerTurn5();
				}
				else if(
					!_isShowTutorPlayerSelectEventTarget_1
					&& _isShowTutorPlayerTurn5 
					&& this.IsCanSummonCard(CurrentActivePlayerIndex) 
					&& _clickBattleCardState == ClickBattleCardState.ShowActionMenu
				)
				{
					if(!this.IsSelectHandCard("C0016") && !IsShowInfoCard("C0016"))
					{
						this.ShowHandCardArrow(CurrentActivePlayerIndex, "C0016", true);
					}
					else
					{
						this.ShowHandCardArrow(CurrentActivePlayerIndex, "C0016", false);
					}
				}
				/*
				else if(
					!_isShowTutorPlayerSelectEventTarget_1
					&& _isShowTutorPlayerTurn5 
					&& this.IsCanSummonCard(CurrentActivePlayerIndex) 
					&& _clickBattleCardState == ClickBattleCardState.SelectTarget
				)
				{
					Debug.Log(">C0009");
					this.ShowBattleCardArrow(PlayerIndex.Two, "C0009", true);

					_isShowTutorPlayerSelectEventTarget_1 = true;
				}
				*/
				else if(
					!_isShowTutorPlayerUsedEvent_1
					&& _isShowTutorPlayerTurn5 
					&& !this.IsCanSummonCard(CurrentActivePlayerIndex) 
				)
				{
					StartTutorPlayerUsedEvent_1();
				}
				else if(_isShowTutorPlayerUsedEvent_1)
				{
					_battleUIManager.ShowNextPhaseArrow(true);
				}
			}

			if(this.Turn == 7)
			{
				if(!_isShowTutorPlayerTurn7)
				{
					StartTutorPlayerTurn7();
				}
				else if(
					!_isShowTutorPlayerPlayedUnit_2
					&& _isShowTutorPlayerTurn7 
					&& this.IsCanSummonCard(CurrentActivePlayerIndex) 
				)
				{
					if(!this.IsSelectHandCard("C0030") && !IsShowInfoCard("C0030"))
					{
						this.ShowHandCardArrow(CurrentActivePlayerIndex, "C0030", true);
					}
					else
					{
						this.ShowHandCardArrow(CurrentActivePlayerIndex, "C0030", false);
					}
				}
				else if(
					!_isShowTutorPlayerPlayedUnit_2
					&& _isShowTutorPlayerTurn7 
					&& !this.IsCanSummonCard(CurrentActivePlayerIndex) 
				)
				{
					StartTutorPlayerPlayedUnit_2();
				}
				else if(
					!_isShowTutorPlayerMovedUnit_2
					&& _isShowTutorPlayerPlayedUnit_2 
					&& this.IsCanMoveBattleCard(CurrentActivePlayerIndex) 
				)
				{
					if(!this.IsSelectBattleCard("C0030") && !this.IsShowInfoCard("C0030"))
					{
						if(!this._isSelectMove)
						{
							this.ShowBattleCardArrow(CurrentActivePlayerIndex, "C0030", true);
						}
						else
						{
							this.ShowBattleCardArrow(CurrentActivePlayerIndex, "C0030", false);
						}
					}
					else
					{
						this.ShowBattleCardArrow(CurrentActivePlayerIndex, "C0030", false);
					}
				}
				else if(
					!_isShowTutorPlayerMovedUnit_2
					&& _isShowTutorPlayerPlayedUnit_2 
					&& !this.IsCanMoveBattleCard(CurrentActivePlayerIndex) 
				)
				{
					this.ShowBattleCardArrow(CurrentActivePlayerIndex, "C0030", false);
					StartTutorPlayerMovedUnit_2();
				}
				else if(_isShowTutorPlayerMovedUnit_2)
				{
					_battleUIManager.ShowNextPhaseArrow(true);
				}
			}

			if(this.Turn == 9)
			{
				if(!_isShowTutorPlayerTurn9)
				{
					StartTutorPlayerTurn9();
				}
				else if(
					!_isShowTutorPlayerPlayedLeader
					&& _isShowTutorPlayerTurn9 
					&& this.IsCanFaceupBattleCard(CurrentActivePlayerIndex) 
				)
				{
					if(!this.IsSelectBattleCard("C0062") && !this.IsShowInfoCard("C0062"))
					{
						this.ShowBattleCardArrow(CurrentActivePlayerIndex, "C0062", true);
					}
					else
					{
						this.ShowBattleCardArrow(CurrentActivePlayerIndex, "C0062", false);
					}
				}
				else if(
					!_isShowTutorPlayerPlayedLeader
					&& _isShowTutorPlayerTurn9 
					&& !this.IsCanFaceupBattleCard(CurrentActivePlayerIndex) 
				)
				{
					StartTutorPlayerPlayedLeader();
				}
			}
		}
	}
#endregion

#region Tutorial PreBattle Properties
	private bool _isShowTutorBeforePlayStructure = false;
	private bool _isShowArrowDeimanistFoundry = false;
	private bool _isShowTutorAfterPlayStructure = false;
	private bool _isShowTutorBotPlayedUnit_1 = false;
	private bool _isShowTutorBotMovedUnit_1 = false;
	private bool _isShowTutorPlayerTurn3 = false;
	private bool _isShowArrowDialnoTroop_1 = false;
	private bool _isShowArrowDialnoTroop_2 = false;
	private bool _isShowTutorPlayerPlayedUnit_1 = false;
	private bool _isShowTutorPlayerSelectedUnit_1 = false;
	private bool _isShowTutorPlayerSelectMoveUnit_1 = false;
	private bool _isShowTutorPlayerMovedUnit_1 = false;
	private bool _isShowTutorPlayerTurn5 = false;
	private bool _isShowTutorPlayerSelectEventTarget_1 = false;
	private bool _isShowTutorPlayerUsedEvent_1 = false;
	private bool _isShowTutorPlayerTurn7 = false;
	private bool _isShowTutorPlayerPlayedUnit_2 = false;
	private bool _isShowTutorPlayerMovedUnit_2 = false;
	private bool _isShowTutorPlayerTurn9 = false;
	private bool _isShowTutorPlayerPlayedLeader = false;

#endregion

#region Tutorial PreBattle Methods
	private void StartTutorBeforePlayStructure()
	{
		if(IsTutorial && !_isShowTutorBeforePlayStructure)
		{
			List<string> tutorialCodeList = new List<string>();
			tutorialCodeList.Add("T0012");

			List<TutorialData> tutorialDataList = LoadTutorialDataList(tutorialCodeList);

			if(tutorialDataList != null && tutorialDataList.Count > 0)
			{
				_isShowTutorBeforePlayStructure = true;
				_isTutorialCanNextPhase = false;

				ShowTutorialUI(tutorialDataList, null);
				return;
			}
		}
	}

	private void StartTutorAfterPlayStructure()
	{
		if(IsTutorial && !_isShowTutorAfterPlayStructure)
		{
			List<string> tutorialCodeList = new List<string>();
			//tutorialCodeList.Add("T0013");
			tutorialCodeList.Add("T0014");

			List<TutorialData> tutorialDataList = LoadTutorialDataList(tutorialCodeList);

			if(tutorialDataList != null && tutorialDataList.Count > 0)
			{
				_isShowTutorAfterPlayStructure = true;
				_isTutorialCanNextPhase = true;

				ShowTutorialUI(tutorialDataList, null);
				return;
			}

			_isShowTutorAfterPlayStructure = true;
			_isTutorialCanNextPhase = true;
		}
	}

	private void StartTutorBotPlayedUnit_1()
	{
		if(IsTutorial && !_isShowTutorBotPlayedUnit_1)
		{
			List<string> tutorialCodeList = new List<string>();
			tutorialCodeList.Add("T0017");

			List<TutorialData> tutorialDataList = LoadTutorialDataList(tutorialCodeList);

			if(tutorialDataList != null && tutorialDataList.Count > 0)
			{
				_isShowTutorBotPlayedUnit_1 = true;

				ShowTutorialUI(tutorialDataList, null);
				return;
			}

			_isShowTutorBotPlayedUnit_1 = true;
		}
	}

	private void StartTutorBotMovedUnit_1()
	{
		if(IsTutorial && !_isShowTutorBotMovedUnit_1)
		{
			List<string> tutorialCodeList = new List<string>();
			//tutorialCodeList.Add("T0018");

			List<TutorialData> tutorialDataList = LoadTutorialDataList(tutorialCodeList);

			if(tutorialDataList != null && tutorialDataList.Count > 0)
			{
				_isShowTutorBotMovedUnit_1 = true;

				ShowTutorialUI(tutorialDataList, null);
				return;
			}

			_isShowTutorBotMovedUnit_1 = true;
		}
	}

	private void StartTutorPlayerTurn3()
	{
		if(IsTutorial && !_isShowTutorPlayerTurn3)
		{
			List<string> tutorialCodeList = new List<string>();
			tutorialCodeList.Add("T0019");
			tutorialCodeList.Add("T0020");

			List<TutorialData> tutorialDataList = LoadTutorialDataList(tutorialCodeList);

			if(tutorialDataList != null && tutorialDataList.Count > 0)
			{
				_isShowTutorPlayerTurn3 = true;
				_isTutorialCanNextPhase = false;

				ShowTutorialUI(tutorialDataList, null);
				return;
			}

			_isShowTutorPlayerTurn3 = true;
			_isTutorialCanNextPhase = false;
		}
	}

	private void StartTutorPlayerPlayedUnit_1()
	{
		if(IsTutorial && !_isShowTutorPlayerPlayedUnit_1)
		{
			List<string> tutorialCodeList = new List<string>();
			//tutorialCodeList.Add("T0021");
			tutorialCodeList.Add("T0022");

			List<TutorialData> tutorialDataList = LoadTutorialDataList(tutorialCodeList);

			if(tutorialDataList != null && tutorialDataList.Count > 0)
			{
				_isShowTutorPlayerPlayedUnit_1 = true;
				_isTutorialCanNextPhase = false;

				ShowTutorialUI(tutorialDataList, null);
				return;
			}

			_isShowTutorPlayerPlayedUnit_1 = true;
			_isTutorialCanNextPhase = false;
		}
	}

	private void StartTutorPlayerSelectedUnit_1()
	{
		if(IsTutorial && !_isShowTutorPlayerSelectedUnit_1)
		{
			List<string> tutorialCodeList = new List<string>();
			//tutorialCodeList.Add("T0023");

			List<TutorialData> tutorialDataList = LoadTutorialDataList(tutorialCodeList);

			if(tutorialDataList != null && tutorialDataList.Count > 0)
			{
				_isShowTutorPlayerSelectedUnit_1 = true;
				_isTutorialCanNextPhase = false;

				ShowTutorialUI(tutorialDataList, null);
				return;
			}

			_isShowTutorPlayerSelectedUnit_1 = true;
			_isTutorialCanNextPhase = false;
		}
	}

	private void StartTutorPlayerSelectMoveUnit_1()
	{
		if(IsTutorial && !_isShowTutorPlayerSelectMoveUnit_1)
		{
			List<string> tutorialCodeList = new List<string>();
			tutorialCodeList.Add("T0024");

			List<TutorialData> tutorialDataList = LoadTutorialDataList(tutorialCodeList);

			if(tutorialDataList != null && tutorialDataList.Count > 0)
			{
				_isShowTutorPlayerSelectMoveUnit_1 = true;
				_isTutorialCanNextPhase = false;

				ShowTutorialUI(tutorialDataList, null);
				return;
			}

			_isShowTutorPlayerSelectMoveUnit_1 = true;
			_isTutorialCanNextPhase = false;
		}
	}

	private void StartTutorPlayerMovedUnit_1()
	{
		if(IsTutorial && !_isShowTutorPlayerMovedUnit_1)
		{
			List<string> tutorialCodeList = new List<string>();
			tutorialCodeList.Add("T0025");
			//tutorialCodeList.Add("T0026");
			tutorialCodeList.Add("T0026_1");
			tutorialCodeList.Add("T0026_2");

			List<TutorialData> tutorialDataList = LoadTutorialDataList(tutorialCodeList);

			if(tutorialDataList != null && tutorialDataList.Count > 0)
			{
				_isShowTutorPlayerMovedUnit_1 = true;
				_isTutorialCanNextPhase = true;

				ShowTutorialUI(tutorialDataList, null);
				return;
			}

			_isShowTutorPlayerMovedUnit_1 = true;
			_isTutorialCanNextPhase = true;
		}
	}

	private void StartTutorPlayerTurn5()
	{
		if(IsTutorial && !_isShowTutorPlayerTurn5)
		{
			List<string> tutorialCodeList = new List<string>();
			tutorialCodeList.Add("T0029");
			//tutorialCodeList.Add("T0030");
			tutorialCodeList.Add("T0031");

			List<TutorialData> tutorialDataList = LoadTutorialDataList(tutorialCodeList);

			if(tutorialDataList != null && tutorialDataList.Count > 0)
			{
				_isShowTutorPlayerTurn5 = true;
				_isTutorialCanNextPhase = false;

				ShowTutorialUI(tutorialDataList, null);
				return;
			}

			_isShowTutorPlayerTurn5 = true;
			_isTutorialCanNextPhase = false;
		}
	}

	private void StartTutorPlayerUsedEvent_1()
	{
		if(IsTutorial && !_isShowTutorPlayerUsedEvent_1)
		{
			List<string> tutorialCodeList = new List<string>();
			tutorialCodeList.Add("T0032");
			tutorialCodeList.Add("T0032_2");

			List<TutorialData> tutorialDataList = LoadTutorialDataList(tutorialCodeList);

			if(tutorialDataList != null && tutorialDataList.Count > 0)
			{
				_isShowTutorPlayerUsedEvent_1 = true;
				_isTutorialCanNextPhase = true;

				ShowTutorialUI(tutorialDataList, null);
				return;
			}

			_isShowTutorPlayerUsedEvent_1 = true;
			_isTutorialCanNextPhase = true;
		}
	}

	private void StartTutorPlayerTurn7()
	{
		if(IsTutorial && !_isShowTutorPlayerTurn7)
		{
			List<string> tutorialCodeList = new List<string>();
			tutorialCodeList.Add("T0033");

			List<TutorialData> tutorialDataList = LoadTutorialDataList(tutorialCodeList);

			if(tutorialDataList != null && tutorialDataList.Count > 0)
			{
				_isShowTutorPlayerTurn7 = true;
				_isTutorialCanNextPhase = false;

				ShowTutorialUI(tutorialDataList, null);
				return;
			}

			_isShowTutorPlayerTurn7 = true;
			_isTutorialCanNextPhase = false;
		}
	}

	private void StartTutorPlayerPlayedUnit_2()
	{
		if(IsTutorial && !_isShowTutorPlayerPlayedUnit_2)
		{
			List<string> tutorialCodeList = new List<string>();
			//tutorialCodeList.Add("T0034");
			tutorialCodeList.Add("T0035");

			List<TutorialData> tutorialDataList = LoadTutorialDataList(tutorialCodeList);

			if(tutorialDataList != null && tutorialDataList.Count > 0)
			{
				_isShowTutorPlayerPlayedUnit_2 = true;
				_isTutorialCanNextPhase = false;

				ShowTutorialUI(tutorialDataList, null);
				return;
			}

			_isShowTutorPlayerPlayedUnit_2 = true;
			_isTutorialCanNextPhase = false;
		}
	}

	private void StartTutorPlayerMovedUnit_2()
	{
		if(IsTutorial && !_isShowTutorPlayerMovedUnit_2)
		{
			_isShowTutorPlayerMovedUnit_2 = true;
			_isTutorialCanNextPhase = true;
		}
	}

	private void StartTutorPlayerTurn9()
	{
		if(IsTutorial && !_isShowTutorPlayerTurn9)
		{
			List<string> tutorialCodeList = new List<string>();
			tutorialCodeList.Add("T0036");

			List<TutorialData> tutorialDataList = LoadTutorialDataList(tutorialCodeList);

			if(tutorialDataList != null && tutorialDataList.Count > 0)
			{
				_isShowTutorPlayerTurn9 = true;
				_isTutorialCanNextPhase = false;

				ShowTutorialUI(tutorialDataList, null);
				return;
			}

			_isShowTutorPlayerTurn9 = true;
			_isTutorialCanNextPhase = false;
		}
	}
	private void StartTutorPlayerPlayedLeader()
	{
		if(IsTutorial && !_isShowTutorPlayerPlayedLeader)
		{
			List<string> tutorialCodeList = new List<string>();
			tutorialCodeList.Add("T0037");
			tutorialCodeList.Add("T0038");
			tutorialCodeList.Add("T0038_2");

			List<TutorialData> tutorialDataList = LoadTutorialDataList(tutorialCodeList);

			if(tutorialDataList != null && tutorialDataList.Count > 0)
			{
				_isShowTutorPlayerPlayedLeader = true;
				_isTutorialCanNextPhase = false;

				ShowTutorialUI(tutorialDataList, this.OnCompletedTutorial);
				return;
			}

			_isShowTutorPlayerPlayedLeader = true;
			_isTutorialCanNextPhase = false;
		}
	}

	private void OnCompletedTutorial()
	{
		_battleUIManager.ShowLoading(true);

		PlayerSave.SavePlayerFirstTime(false);
        KOSServer.RequestCompleteFirstFlow(null, null);
        
		GameAnalytics.NewDesignEvent ("Tutorial:Completed");

		GoToMainMenu();
	}
#endregion

#region Battle Phase Methods
	private void OnStartPhaseBattle()
	{
		RequestUpdateAllBattleCardUI();
		NextPhase();
	}

	private void OnPhaseBattle()
	{
		if(IsTutorial)
		{
			if(this.Turn == 1)
			{
				if(!_isShowTutorBattle_1)
				{
					_battleUIManager.ShowNextPhaseArrow(false);
					StartTutorBattle_1();
				}
				else if(_isShowTutorBattle_1)
				{
					_battleUIManager.ShowNextPhaseArrow(true);
				}
			}

			if(this.Turn == 3)
			{
				if(!_isShowTutorPlayerBattleTurn3)
				{
					_battleUIManager.ShowNextPhaseArrow(false);
					StartTutorPlayerBattleTurn3();
				}
				else if(
					!_isShowTutorPlayerAttackedUnit_1
					&& _isShowTutorPlayerBattleTurn3 
					&& this.IsCanAttackBattleCard(CurrentActivePlayerIndex)
					&& _clickBattleCardState == ClickBattleCardState.ShowActionMenu
				)
				{
					if(!this.IsSelectBattleCard("C0007") && !this.IsShowInfoCard("C0007"))
					{
						this.ShowBattleCardArrow(CurrentActivePlayerIndex, "C0007", true);
					}
					else
					{
						this.ShowBattleCardArrow(CurrentActivePlayerIndex, "C0007", false);
					}
				}
				else if(
					!_isShowTutorPlayerSelectTarget_1
					&& _isShowTutorPlayerBattleTurn3 
					&& this.IsCanAttackBattleCard(CurrentActivePlayerIndex)
					&& _clickBattleCardState == ClickBattleCardState.SelectTarget
				)
				{
					this.ShowBattleCardArrow(CurrentActivePlayerIndex, "C0007", false);
					this.ShowBattleCardArrow(PlayerIndex.Two, "C0011", true);

					_isShowTutorPlayerSelectTarget_1 = true;
				}
				else if(
					!_isShowTutorPlayerAttackedUnit_1
					&& _isShowTutorPlayerSelectTarget_1 
					&& !this.IsCanAttackBattleCard(CurrentActivePlayerIndex)
				)
				{
					StartTutorPlayerAttackedUnit_1();
				}
				else if(_isShowTutorPlayerAttackedUnit_1)
				{
					_battleUIManager.ShowNextPhaseArrow(true);
				}
			}

			if(this.Turn == 5)
			{
				_battleUIManager.ShowNextPhaseArrow(true);
			}

			if(this.Turn == 7)
			{
				_battleUIManager.ShowNextPhaseArrow(true);
			}
		}
	}
#endregion

#region Tutorial Battle Properties
	private bool _isShowTutorBattle_1 = false;
	private bool _isShowTutorPlayerBattleTurn3 = false;
	private bool _isShowTutorPlayerSelectTarget_1 = false;
	private bool _isShowTutorPlayerAttackedUnit_1 = false;
#endregion

#region Tutorial Battle Methods
	private void StartTutorBattle_1()
	{
		if(IsTutorial && !_isShowTutorBattle_1)
		{
			List<string> tutorialCodeList = new List<string>();
			tutorialCodeList.Add("T0015");

			List<TutorialData> tutorialDataList = LoadTutorialDataList(tutorialCodeList);

			if(tutorialDataList != null && tutorialDataList.Count > 0)
			{
				_isShowTutorBattle_1 = true;
				ShowTutorialUI(tutorialDataList, null);
				return;
			}
		}
	}

	private void StartTutorPlayerBattleTurn3()
	{
		if(IsTutorial && !_isShowTutorPlayerBattleTurn3)
		{
			List<string> tutorialCodeList = new List<string>();
			tutorialCodeList.Add("T0027");

			List<TutorialData> tutorialDataList = LoadTutorialDataList(tutorialCodeList);

			if(tutorialDataList != null && tutorialDataList.Count > 0)
			{
				_isShowTutorPlayerBattleTurn3 = true;
				_isTutorialCanNextPhase = false;

				ShowTutorialUI(tutorialDataList, null);
				return;
			}
		}
	}

	private void StartTutorPlayerAttackedUnit_1()
	{
		if(IsTutorial && !_isShowTutorPlayerAttackedUnit_1)
		{
			List<string> tutorialCodeList = new List<string>();
			tutorialCodeList.Add("T0028");

			List<TutorialData> tutorialDataList = LoadTutorialDataList(tutorialCodeList);

			if(tutorialDataList != null && tutorialDataList.Count > 0)
			{
				_isShowTutorPlayerAttackedUnit_1 = true;
				_isTutorialCanNextPhase = true;

				ShowTutorialUI(tutorialDataList, null);
				return;
			}
		}
	}
#endregion

#region PostBattle Phase Methods
	private void OnStartPhasePostBattle()
	{
		RequestUpdateAllBattleCardUI();
		NextPhase();
	}

	private void OnPhasePostBattle()
	{
		if(IsTutorial)
		{
			if(this.Turn == 1)
			{
				if(!_isShowTutorPostBattle_1)
				{
					_battleUIManager.ShowNextPhaseArrow(false);
					StartTutorPostBattle_1();
				}
				else if(_isShowTutorPostBattle_1)
				{
					_battleUIManager.ShowNextPhaseArrow(true);
				}
			}

			if(this.Turn == 3)
			{
				_battleUIManager.ShowNextPhaseArrow(true);
			}

			if(this.Turn == 5)
			{
				_battleUIManager.ShowNextPhaseArrow(true);
			}

			if(this.Turn == 7)
			{
				_battleUIManager.ShowNextPhaseArrow(true);
			}
		}
	}
#endregion

#region Tutorial PostBattle Properties
	private bool _isShowTutorPostBattle_1 = false;
#endregion

#region Tutorial PostBattle Methods
	private void StartTutorPostBattle_1()
	{
		if(IsTutorial && !_isShowTutorPostBattle_1)
		{
			List<string> tutorialCodeList = new List<string>();
			tutorialCodeList.Add("T0016");

			List<TutorialData> tutorialDataList = LoadTutorialDataList(tutorialCodeList);

			if(tutorialDataList != null && tutorialDataList.Count > 0)
			{
				_isShowTutorPostBattle_1 = true;
				_isTutorialCanNextPhase = true;

				ShowTutorialUI(tutorialDataList, null);
				return;
			}
		}
	}
#endregion

#region End Phase Methods
	private void OnStartPhaseEnd()
	{
		StartCheckLimitHandCard();
		//NextPhase();
	}

	private void StartCheckLimitHandCard()
	{
		int overFlow = (GetHandCount(CurrentActivePlayerIndex) - this._limitHandCardNum);
		if(overFlow > 0)
		{
			// More than limit number.
			// Have to discard.
			NextPhase();
                   
            if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
            {
                if(CurrentActivePlayerIndex != KOSNetwork.GamePlayer.localPlayer.playerIndex)
                {
                    return;
                }
            }
            
			this.RequestDisHandCardOverflow(CurrentActivePlayerIndex, overFlow, this.NextPhase, null, false);
		}
		else
		{
            //NextPhase();
            _battlefieldData.SetPhase(BattlePhase.BeginCleanUp);
			OnBattlePhaseUpdateEvent();
		}
	}

	public int GetHandCount(PlayerIndex playerIndex)
	{
		return this._battlefieldData.PlayersData[(int)playerIndex].HandCount;
	}
        
	private void OnEndPhaseEnd()
	{
		_currentActivePlayerIndex = GetNextPlayerIndex();
		NextPhase();
	}

    private void CleanUp()
    {
        //Debug.Log("the CleanUp");

        ResetAllBattleCard();
        UpdateAllBuffBattleCard();

        NextPhase();
    }

    private void OnEndCleanUp()
    {
        /*
        if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
        {
            KOSNetwork.GamePlayer.localPlayer.CmdReadyEndPhase();
        }
        else
        {
            _battlefieldData.SetPhase(BattlePhase.WaitReadyEnd);
        }
        */
        
        NextPhase();
    }
    
    bool _isP1ReadyBeginTurn = false;
    bool _isP2ReadyBeginTurn = false;
    private void ReadyBegin()
    {
        if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
        {
            KOSNetwork.GamePlayer.localPlayer.RequestReadyBeginTurn();
        }
        else
        {
           _battlefieldData.SetPhase(BattlePhase.WaitReadyBegin);
        }
        
        NextPhase();
    }
    public void OnReadyBegin(PlayerIndex playerIndex)
    {
        if(playerIndex == PlayerIndex.One) 
        {
            _isP1ReadyBeginTurn = true;
        }
        else if (playerIndex == PlayerIndex.Two) 
        {
            _isP2ReadyBeginTurn = true;
        }
            
        if(_isP1ReadyBeginTurn && _isP2ReadyBeginTurn)
        {
            _isP1ReadyBeginTurn = false;
            _isP2ReadyBeginTurn = false;
        
            NextPhase();
        }
    }
    private void CompleteReadyBegin()
    {   
        /*     
        {
            if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
            {
                string text = string.Format(
                    "[{0}]CompleteReadyBegin !!!"
                    , KOSNetwork.GamePlayer.localPlayer.playerIndex.ToString()
                );
            
                KOSNetwork.GamePlayer.localPlayer.CmdPrintLog(text); 
            }
        }
        */
        
        OnTurnIndexUpdateEvent();
        ClearPlayerGold(this.CurrentActivePlayerIndex);
        NextPhase();
    }
    
    bool _isP1ReadyEndPreBattle = false;
    bool _isP2ReadyEndPreBattle = false;
    private void ReadyEndPreBattle()
    {
        if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
        {
            KOSNetwork.GamePlayer.localPlayer.RequestReadyEndPreBattle();
        }
        else
        {
           _battlefieldData.SetPhase(BattlePhase.WaitReadyEndPreBattle);
        }
        
        NextPhase();
    }
    public void OnReadyEndPreBattle(PlayerIndex playerIndex)
    {
        if(playerIndex == PlayerIndex.One) 
        {
            _isP1ReadyEndPreBattle = true;
        }
        else if (playerIndex == PlayerIndex.Two) 
        {
            _isP2ReadyEndPreBattle = true;
        }
            
        if(_isP1ReadyEndPreBattle && _isP2ReadyEndPreBattle)
        {
            _isP1ReadyEndPreBattle = false;
            _isP2ReadyEndPreBattle = false;
        
            NextPhase();
        }
    }
    private void CompleteEndPreBattle()
    {
        /*
        {
            if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
            {
                string text = string.Format(
                    "[{0}]CompleteEndPreBattle !!!"
                    , KOSNetwork.GamePlayer.localPlayer.playerIndex.ToString()
                );
            
                KOSNetwork.GamePlayer.localPlayer.CmdPrintLog(text); 
            }
        }
        */
        NextPhase();
    }
    
    bool _isP1ReadyEndBattle = false;
    bool _isP2ReadyEndBattle = false;
    private void ReadyEndBattle()
    {
        if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
        {
            KOSNetwork.GamePlayer.localPlayer.RequestReadyEndBattle();
        }
        else
        {
           _battlefieldData.SetPhase(BattlePhase.WaitReadyEndBattle);
        }
        
        NextPhase();
    }
    public void OnReadyEndBattle(PlayerIndex playerIndex)
    {
        if(playerIndex == PlayerIndex.One) 
        {
            _isP1ReadyEndBattle = true;
        }
        else if (playerIndex == PlayerIndex.Two) 
        {
            _isP2ReadyEndBattle = true;
        }
            
        if(_isP1ReadyEndBattle && _isP2ReadyEndBattle)
        {
            _isP1ReadyEndBattle = false;
            _isP2ReadyEndBattle = false;
            
            NextPhase();
        }
    }
    private void CompleteEndBattle()
    {
        /*
        {
            if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
            {
                string text = string.Format(
                    "[{0}]CompleteEndBattle !!!"
                    , KOSNetwork.GamePlayer.localPlayer.playerIndex.ToString()
                );
            
                KOSNetwork.GamePlayer.localPlayer.CmdPrintLog(text); 
            }
        }
        */
        NextPhase();
    }
    
    bool _isP1ReadyEndPostBattle = false;
    bool _isP2ReadyEndPostBattle = false;
    private void ReadyEndPostBattle()
    {
        if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
        {
            KOSNetwork.GamePlayer.localPlayer.RequestReadyEndPostBattle();
        }
        else
        {
           _battlefieldData.SetPhase(BattlePhase.WaitReadyEndPostBattle);
        }
        
        NextPhase();
    }
    public void OnReadyEndPostBattle(PlayerIndex playerIndex)
    {
        if(playerIndex == PlayerIndex.One) 
        {
            _isP1ReadyEndPostBattle = true;
        }
        else if (playerIndex == PlayerIndex.Two) 
        {
            _isP2ReadyEndPostBattle = true;
        }
            
        if(_isP1ReadyEndPostBattle && _isP2ReadyEndPostBattle)
        {
            _isP1ReadyEndPostBattle = false;
            _isP2ReadyEndPostBattle = false;
        
            NextPhase();
        }
    }
    private void CompleteEndPostBattle()
    {
        /*
        {
            if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
            {
                string text = string.Format(
                    "[{0}]CompleteEndPostBattle !!!"
                    , KOSNetwork.GamePlayer.localPlayer.playerIndex.ToString()
                );
            
                KOSNetwork.GamePlayer.localPlayer.CmdPrintLog(text); 
            }
        }
        */
        NextPhase();
    }

	private void ResetAllBattleCard()
	{
		_battlefieldData.ResetAllBattleCard();
	}

	private void UpdateAllBuffBattleCard()
	{
		_battlefieldData.UpdateAllBuffBattleCard();
	}

#endregion
		
	// UI

#region ActionMenu Selector
	BattleCardUI _selectedActionMenuBattleCardUI = null;
	HandCardUI _selectedActionMenuHandCardUI = null;

	private void SetActionMenuSelectCard(BattleCardUI selectedActionMenuBattleCardUI)
	{
		// Unselect prev battle card.
		if(_selectedActionMenuBattleCardUI != null)
		{
			_selectedActionMenuBattleCardUI.IsSelect = false;
			_selectedActionMenuBattleCardUI = null;
		}
			
		// Unselect prev hand card.
		if(_selectedActionMenuHandCardUI != null)
		{
			_selectedActionMenuHandCardUI.IsSelect = false;
			_selectedActionMenuHandCardUI = null;
		}

		// Select battle card.
		_selectedActionMenuBattleCardUI = selectedActionMenuBattleCardUI;
		_selectedActionMenuBattleCardUI.IsSelect = true;

		_BattleCardPopupUI_BattleCardID = _selectedActionMenuBattleCardUI.ShowBattleCardData.BattleCardID;
	}

	private void SetActionMenuSelectCard(HandCardUI selectedActionMenuHandCardUI)
	{
		// Unselect prev battle card.
		if(_selectedActionMenuBattleCardUI != null)
		{
			_selectedActionMenuBattleCardUI.IsSelect = false;
			_selectedActionMenuBattleCardUI = null;
		}
			
		// Unselect prev hand card.
		if(_selectedActionMenuHandCardUI != null)
		{
			_selectedActionMenuHandCardUI.IsSelect = false;
			_selectedActionMenuHandCardUI = null;
		}

		// Select hand card.
		_selectedActionMenuHandCardUI = selectedActionMenuHandCardUI;
		_selectedActionMenuHandCardUI.IsSelect = true;

		_HandCardPopupUI_UniqueCardID = _selectedActionMenuHandCardUI.UniqueCardID;
	}

	private void DeselectCard()
	{
		if(_selectedActionMenuBattleCardUI != null)
		{
			_selectedActionMenuBattleCardUI.IsSelect = false;
			_selectedActionMenuBattleCardUI = null;
		}
		_BattleCardPopupUI_BattleCardID = -1;

		if(_selectedActionMenuHandCardUI != null)
		{
			_selectedActionMenuHandCardUI.IsSelect = false;
			_selectedActionMenuHandCardUI = null;
		}
		_HandCardPopupUI_UniqueCardID = -1;
	}
#endregion

#region BattleCardUI Properties
	public enum ClickBattleCardState
	{
		  ShowActionMenu
		, SelectTarget
	}

	public delegate void OnFinishUseAbilityBattleCard();
	public delegate void OnCancelUseAbilityBattleCard();

	private OnFinishUseAbilityBattleCard _onFinishUseAbilityBattleCard = null;
	private OnCancelUseAbilityBattleCard _onCancelUseAbilityBattleCard = null;

	private int _BattleCardPopupUI_BattleCardID = -1;

	ClickBattleCardState _clickBattleCardState = ClickBattleCardState.ShowActionMenu;
#endregion

#region BattleCardUI Methods
    private bool _CreateBattleCardUI(BattleCardData battleCard, BattleZoneIndex battleZoneIndex)
    {
        bool isCreated = _battleUIManager.FieldUIManager.CreateBattleCardUI(battleCard, battleZoneIndex, this.OnClickBattleCard);
        if(isCreated)
        {
            OnPlayersInfoUpdateEvent();
            return true;
        }

        return false;
    }

    private bool _DestroyBattleCardUI(int battleCardID)
    {
        return _battleUIManager.FieldUIManager.DestroyBattleCardUI(battleCardID);
    }

	public void OnClickBattleCard(BattleCardUI battleCardUI, int battleCardID)
    {
		//Debug.Log("Clicked BattleCard : " + battleCardID.ToString());

		// Find local player.
		IBattleControlController localPlayer = null;
		bool isFound = this.GetLocalPlayer(out localPlayer);
		if(!isFound)
		{
			Debug.LogError("BattleManager/OnClickBattleCard: Not found local player.");
			return;
		}
			
		switch(_clickBattleCardState)
		{
			case ClickBattleCardState.ShowActionMenu:
			{
				if (localPlayer.GetPlayerIndex() != CurrentActivePlayerIndex) return; // not my turn.
                if (!IsPlayerCanAction) return;

				RequestShowActionMenu(battleCardUI, battleCardID);
			}
			break;

			case ClickBattleCardState.SelectTarget:
			{
				SoundManager.PlayEFXSound(BattleController.Instance.SelectSound);
				OnSelectTarget(battleCardUI, battleCardID);
			}
			break;
		}
    }

	// Show action menu

	private void RequestShowActionMenu(BattleCardUI battleCardUI, int battleCardID)
	{
#if (UNITY_IOS && !UNITY_EDITOR)
		// iOS

		if((UnityEngine.iOS.Device.generation.ToString()).IndexOf("iPad") > -1)
		{
			// iPad
			List<CardPopupMenuUI.CardPopupActionType> actionTypeList;

			bool isActionable = GetBattleCardActionableList(battleCardID, out actionTypeList);
			if(isActionable)
			{
				RequestShowBattleCardPopupUI(actionTypeList, battleCardUI, battleCardID);
			}
		}
		else
		{
			// iPhone
			List<CardPopupMenuUI.CardPopupActionType> actionTypeList;

			bool isActionable = GetBattleCardActionableList(battleCardID, out actionTypeList);
			if(isActionable)
			{
				SoundManager.PlayEFXSound(BattleController.Instance.SelectSound);
				PopupShowInfoBattleCard(battleCardID);
			}
		}
        
#elif (UNITY_ANDROID && !UNITY_EDITOR)
		// Android
        List<CardPopupMenuUI.CardPopupActionType> actionTypeList;

        bool isActionable = GetBattleCardActionableList(battleCardID, out actionTypeList);
        if(isActionable)
        {
            SoundManager.PlayEFXSound(BattleController.Instance.SelectSound);
            PopupShowInfoBattleCard(battleCardID);
        }
        
#elif (UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_EDITOR)       
        // Computer
        List<CardPopupMenuUI.CardPopupActionType> actionTypeList;

        bool isActionable = GetBattleCardActionableList(battleCardID, out actionTypeList);
        if(isActionable)
        {
            RequestShowBattleCardPopupUI(actionTypeList, battleCardUI, battleCardID);
        }
        
#else        
        // Other
        List<CardPopupMenuUI.CardPopupActionType> actionTypeList;

        bool isActionable = GetBattleCardActionableList(battleCardID, out actionTypeList);
        if(isActionable)
        {
            SoundManager.PlayEFXSound(BattleController.Instance.SelectSound);
            PopupShowInfoBattleCard(battleCardID);
        }
 
#endif

	}

	public bool GetBattleCardActionableList(int battleCardID, out List<CardPopupMenuUI.CardPopupActionType> actionTypeList)
	{
		actionTypeList = new List<CardPopupMenuUI.CardPopupActionType>();

		bool isFound = false;

		// Find clicked battle card data.
		BattleCardData battleCard;
		isFound = _battlefieldData.FindBattleCard(battleCardID, out battleCard);
		if(!isFound)
		{
			Debug.LogError("BattleManager/RequestShowActionMenu: Not found battle card data at ID[" + battleCardID.ToString() + "].");
			actionTypeList = null;
			return false;
		}

		// Find local player.
		IBattleControlController localPlayer = null;
		isFound = this.GetLocalPlayer(out localPlayer);
		if(!isFound)
		{
			Debug.LogError("BattleManager/RequestShowActionMenu: Not found local player.");
			actionTypeList = null;
			return false;
		}

		if (localPlayer.GetPlayerIndex() == battleCard.OwnerPlayerIndex)
		{
			// Clicked by owner.
			if(battleCard.IsFaceDown)
			{
				if(IsTutorial && this.Turn < 9) // Tutorial
				{
					actionTypeList = null;
					return false;
				}

				actionTypeList.Add(CardPopupMenuUI.CardPopupActionType.Info);

				if(	   (battleCard.TypeData == CardTypeData.CardType.Base)
					|| (battleCard.TypeData == CardTypeData.CardType.Structure)
					|| (battleCard.TypeData == CardTypeData.CardType.Unit)
				)
				{
					if(IsCanUseGold(battleCard.OwnerPlayerIndex, battleCard.Cost))
					{
						if(this.GetDisplayBattlePhase() != DisplayBattlePhase.Battle) 
						{
							actionTypeList.Add(CardPopupMenuUI.CardPopupActionType.Summon);
						}
					}
				}
				else
				{
					actionTypeList.Add(CardPopupMenuUI.CardPopupActionType.Use);
				}
			}
			else
			{
				actionTypeList.Add(CardPopupMenuUI.CardPopupActionType.Info);

				if(this.GetDisplayBattlePhase() == DisplayBattlePhase.Battle) 
				{
					if(_battlefieldData.IsCanAttack(battleCardID))
					{
						actionTypeList.Add(CardPopupMenuUI.CardPopupActionType.Attack);
					}
				}
				else
				{
					if(battleCard.IsCanMove)
					{
						actionTypeList.Add(CardPopupMenuUI.CardPopupActionType.Move);
					}
				}

				if(battleCard.IsCanUseAbility())
				{
					actionTypeList.Add(CardPopupMenuUI.CardPopupActionType.Ability);
				}
			}
		}
		else
		{
			// Clicked by enemy.

			if(battleCard.IsFaceDown)
			{
				actionTypeList = null;
				return false;
			}
			else
			{
				actionTypeList.Add(CardPopupMenuUI.CardPopupActionType.Info);
			}
		}

		return true;
	}

	private void RequestShowBattleCardPopupUI(List<CardPopupMenuUI.CardPopupActionType> actionList, BattleCardUI battleCardUI, int battleCardID)
    {
		// TODO: Find good popup position in game window.

		SoundManager.PlayEFXSound(BattleController.Instance.SelectSound);
		_BattleCardPopupUI_BattleCardID = battleCardID;

        if (true)
        {
			SetActionMenuSelectCard(battleCardUI);

			if(GetDisplayBattlePhase() == DisplayBattlePhase.PreBattle || GetDisplayBattlePhase() == DisplayBattlePhase.PostBattle)
			{
				if(battleCardUI.ShowBattleCardData.IsFaceDown)
				{
					this.AddSuggestText(Localization.Get("BATTLE_SUGGESTION_FACEUP"));
				}
				else if(battleCardUI.ShowBattleCardData.IsCanMove)
				{
					this.AddSuggestText(Localization.Get("BATTLE_SUGGESTION_MOVE"));
				}
				else
				{
					this.AddSuggestText("");
				}
			}
			else if(GetDisplayBattlePhase() == DisplayBattlePhase.Battle)
			{
				this.AddSuggestText(Localization.Get("BATTLE_SUGGESTION_ATTACK"));
			}
			else
			{
				this.AddSuggestText("");
			}

            _battleUIManager.CardPopupMenuUI.ShowUI(
                  actionList
				, battleCardID
                , battleCardUI.GameObject
				, battleCardUI.GameObject.transform.position
                , this.OnSelectActionBattleCard
				, this.OnCloseActionMenu
                , UIWidget.Pivot.Right
            );
        }
    }
		
	public void OnSelectActionBattleCard(CardPopupMenuUI.CardPopupActionType action, int battleCardID)
    {
		this.RemoveSuggestText();

		// Chose action. Unselect card.
		DeselectCard();

		BattleCardData actionBattleCardData;
		bool isFound = _battlefieldData.FindBattleCard(battleCardID, out actionBattleCardData);
		if(isFound)
		{
			//battleCardUI.BattleCardData.BattleCardID;

			switch(action)
			{
				case CardPopupMenuUI.CardPopupActionType.Info:
				{
					PopupShowInfoBattleCard(actionBattleCardData.BattleCardID);
				}
				break;

				case CardPopupMenuUI.CardPopupActionType.Attack:
				{
					PopupRequestAttackBattleCard(actionBattleCardData.BattleCardID);
				}
				break;

				case CardPopupMenuUI.CardPopupActionType.Move:
				{
					PopupRequestMoveBattleCard(actionBattleCardData.BattleCardID);
				}
				break;

				case CardPopupMenuUI.CardPopupActionType.Summon:
				{
					PopupRequestSummonBattleCard(actionBattleCardData.BattleCardID);
				}
				break;

				case CardPopupMenuUI.CardPopupActionType.Ability:
				{
					PopupShowUseAbilityBattleCard(actionBattleCardData.BattleCardID);
				}
				break;
			}
		}
		else
		{
			Debug.LogError("BattleManager/OnActionBattleCard: Not found battle card data at ID[" + battleCardID.ToString() + "].");
		}
    }

	public void OnCloseActionMenu()
	{
		// close action with no action.

		this.RemoveSuggestText();
		_BattleCardPopupUI_BattleCardID = -1;
		DeselectCard();
	}

	// Attack action

#region Attack Action BattleCardUI Properties
	public delegate void OnAttackFinish();
	private int _attackerID = -1;
	private int _targeterID = -1;
	private OnAttackFinish _onAttackFinish = null;
#endregion

	private void PopupRequestAttackBattleCard(int battleCardID)
	{
		BattleCardData attacker;
		bool isFound = _battlefieldData.FindBattleCard(battleCardID, out attacker);
		if(isFound)
		{
			if(attacker.IsActive)
			{
				List<BattleCardData> atkableList;
				if(this.IsCanAttack(attacker.BattleCardID, out atkableList))
				{
					_attackerID = attacker.BattleCardID;

						this.RequestSelectTarget(
							  Localization.Get("BATTLE_SELECT_ATTACK_TARGET")
							, attacker.OwnerPlayerIndex
							, atkableList
							, 1
							, this.OnSelectAttackTarget
							, null
                            , false
							, true
						);

					/*
					// diable click all hand & battle card.
					_battleUIManager.FieldUIManager.SetAllBattleCardUIClickable(false);
					_battleUIManager.HandUIManager.SetAllHandCardClickable(false);


					//string debugText = "Target: ";

					// enable click attackable battle card.
					foreach(BattleCardData target in atkableList)
					{
						_battleUIManager.FieldUIManager.SetBattleCardUIClickable(target.BattleCardID, true);

						//debugText += "[" + target.Name + "] ";
					}
					//Debug.Log(debugText);

					attackingBattleCard = atkBattleCardData;
					_clickBattleCardState = ClickBattleCardState.SelectAttackTarget;
					*/
				}
				else
				{
					Debug.LogError("BattleManager/RequestAttackBattleCard: can't attack.");
				}
			}
			else
			{
				Debug.LogError("BattleManager/RequestAttackBattleCard: attacking battle card is not active.");
			}
		}
		else
		{
			Debug.LogError("BattleManager/RequestAttackBattleCard: Not found battle card data at ID[" + battleCardID.ToString() + "].");
		}
	}

	private void OnSelectAttackTarget(List<int> targetIDList)
	{
		_targeterID = targetIDList[0];

		/*
		BattleCardData attacker;
		BattleCardData target;
		bool isFound = false;

		isFound = _battlefieldData.FindBattleCard(_attackerID, out attacker);
		if(!isFound)
		{
			Debug.LogError("BattleManager/OnSelectAttackTarget: not found attacker battle card data.");
			return;
		}
			
		isFound = _battlefieldData.FindBattleCard(_targeterID, out target);
		if(!isFound)
		{
			Debug.LogError("BattleManager/OnSelectAttackTarget: not found target battle card data.");
			return;
		}
		*/
        
        if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
        {
            KOSNetwork.GamePlayer.localPlayer.RequestAttackBattleCard(_attackerID, _targeterID);
        }
        else
        {
		    RequestAttackBattleCard(_attackerID, _targeterID, KOSNetwork.NetworkSetting.IsHaveTimeout);
        }
	}
		
	private void _PreformAttackBattleCard(BattleCardData attacker, BattleCardData target)
	{
		List<BattleCompareUI.PerformAtkStep> stepList;
	
		_isPerformBattle = true;

		_battleUIManager.BattleCompareUI.ShowUI(
			  attacker
			, target
			, this.AfterAttackBattleCard
		);

		attacker.AttackTo(target, out stepList);

		_battleUIManager.BattleCompareUI.SetStepList(stepList);
	}

#region Use Ability Properties
	private ShowUseAbilityUI.OnFinishShow _onFinishPreformUseAbility = null;
#endregion

	public void PreformUseAbility(PlayerCardData cardData, List<ShowUseAbilityUI.PerformUseAction> stepList, ShowUseAbilityUI.OnFinishShow callback)
	{
		_isPerformBattle = true;

		_onFinishPreformUseAbility = callback;

		_battleUIManager.ShowUseAbilityUI.ShowUI(cardData, this.AfterPreformUseAbility);
		_battleUIManager.ShowUseAbilityUI.SetStepList(stepList);
	}

	private void AfterPreformUseAbility()
	{
		_isPerformBattle = false;

		if(_onFinishPreformUseAbility != null)
		{
			_onFinishPreformUseAbility.Invoke();
		}
	}

	private void AfterAttackBattleCard(int attackerID, int targetID)
	{
		BattleCardData attacker;
		BattleCardData target;
		bool isFound = false;

		isFound = _battlefieldData.FindBattleCard(attackerID, out attacker);
		if(!isFound)
		{
			Debug.LogError("BattleManager/AfterAttackBattleCard: not found attacker battle card data.");
			return;
		}


		isFound = _battlefieldData.FindBattleCard(targetID, out target);
		if(!isFound)
		{
			Debug.LogError("BattleManager/AfterAttackBattleCard: not found target battle card data.");
			return;
		}

		AfterAttackBattleCard(attacker, target);
	}

	private void AfterAttackBattleCard(BattleCardData attacker, BattleCardData target)
	{
		// Check attacker
		CheckIsDead(attacker.BattleCardID);
		CheckIsDead(target.BattleCardID);

		_attackerID = -1;
		_targeterID = -1;

		OnPlayersInfoUpdateEvent();
		_battlefieldData.UpdateAllBattleCardUI();
		_isPerformBattle = false;

		if(_onAttackFinish != null)
		{
			_onAttackFinish.Invoke();
		}
        
        SetPauseTimeout(false);
	}

	public bool IsCanAttack(int battleCardID, out List<BattleCardData> atkableList)
	{
		return _battlefieldData.IsCanAttack(battleCardID, out atkableList);
	}

	public bool IsCanAttack(int battleCardID)
	{
		return _battlefieldData.IsCanAttack(battleCardID);
	}
		
	// Move action
	private void PopupRequestMoveBattleCard(int battleCardID)
	{
		this.AddSuggestText(Localization.Get("BATTLE_SUGGESTION_SELECT_FIELD"));

		BattleCardData actionBattleCardData;
		bool isFound = _battlefieldData.FindBattleCard(battleCardID, out actionBattleCardData);
		if(isFound && actionBattleCardData.IsCanMove)
		{
			_isSelectMove = true;
			_battleUIManager.ShowMoveCardUI(
                  actionBattleCardData
                , false
                , true
                , this.SelectedMoveCard
                , this.OnCancelMoveCard
            );

			if (!IsTutorial)
			{
				_battleUIManager.ShowSelectBattleCardUI (this.OnCancelMoveCard);
			}
		}
	}

	private void SelectedMoveCard(BattleCardData battleCardData, LocalBattleZoneIndex moveFieldIndex)
	{
		_isSelectMove = false;
		this.RemoveSuggestText();
		DeselectCard();
		_battleUIManager.SelectBattleCardUI.HideUI();

		// Find local player.
		IBattleControlController localPlayer = null;
		bool isFound = this.GetLocalPlayer(out localPlayer);
		if(!isFound)
		{
			Debug.LogError("BattleManager/SelectedMoveCard: Not found local player.");
			return;
		}

		// Find target field.
		BattleZoneIndex targetZoneIndex;
		switch(moveFieldIndex)
		{
			case LocalBattleZoneIndex.PlayerBase: 
			{
				if(battleCardData.OwnerPlayerIndex == PlayerIndex.One)
				{
					targetZoneIndex = BattleZoneIndex.BaseP1;
				}
				else
				{
					targetZoneIndex = BattleZoneIndex.BaseP2;
				}
			}
			break;

			case LocalBattleZoneIndex.Battlefield:
			{
				targetZoneIndex = BattleZoneIndex.Battlefield;
			}
			break;

			case LocalBattleZoneIndex.EnemyBase:
			{
				if(battleCardData.OwnerPlayerIndex == PlayerIndex.One)
				{
					targetZoneIndex = BattleZoneIndex.BaseP2;
				}
				else
				{
					targetZoneIndex = BattleZoneIndex.BaseP1;
				}
			}
			break;

			default:
			{
				Debug.LogError("BattleManager/SelectedMoveCard: Can't find move target field.");
			}
			return;
		}
		
        if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
        {	
            KOSNetwork.GamePlayer.localPlayer.RequestMoveBattleCard(battleCardData.BattleCardID, targetZoneIndex, false);
        }
        else
        {
		    RequestMoveBattleCard(battleCardData.BattleCardID, targetZoneIndex, true);
        }   
	}

	private void OnCancelMoveCard()
	{
		_isSelectMove = false;
		this.RemoveSuggestText();
		DeselectCard();

		_battleUIManager.MoveCardUI.HideUI();
	}
    
    private void AutoSelectOrCancelMove()
    {
        if(_isSelectMove)
        {
            _battleUIManager.SelectBattleCardUI.HideUI();
            _battleUIManager.MoveCardUI.AutoSelectOrCancelMove();
        }
    }
	// Show info

	private void PopupShowInfoBattleCard(int battleCardID)
	{
		//Debug.Log("Show info battle card[" + battleCardID + "]");

		BattleCardData actionBattleCardData;
		bool isFound = _battlefieldData.FindBattleCard(battleCardID, out actionBattleCardData);
		if(isFound)
		{
			DeselectCard();

			_battleUIManager.FullCardInfoUI.ShowUI(actionBattleCardData);
		}
	}

	private void PopupRequestSummonBattleCard(int battleCardID)
	{
        if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
        {
            KOSNetwork.GamePlayer.localPlayer.RequestSummonBattleCard(battleCardID);
        }
        else
        {
		    this.RequestFaceupBattleCard(battleCardID, KOSNetwork.NetworkSetting.IsHaveTimeout);
        }
	}

	// Ability

	public void PopupShowUseAbilityBattleCard(int battleCardID)
	{
		BattleCardData battleCard;
		bool isFound = _battlefieldData.FindBattleCard(battleCardID, out battleCard);
		if(isFound)
		{
			_battleUIManager.ShowAbilitySelectUI(battleCard);
		}
	}
    
    private void CloseAbilitySelectUI()
    {
        if(_battleUIManager.AbilitySelectUI.IsShow)
        {
            _battleUIManager.AbilitySelectUI.CloseUI();
        }
    }

	public void ShowBattleCardArrow(int battleCardID, bool isShow)
	{
		BattleCardUI battleCardUI;
		bool isFound = _battleUIManager.FieldUIManager.GetBattleCardUI(battleCardID, out battleCardUI);

		if(isFound && battleCardUI != null)
		{
			battleCardUI.ShowArrow(isShow);
		}
	}

	public void ShowBattleCardArrow(PlayerIndex playerIndex, string cardID, bool isShow)
	{
		List<BattleCardData> battleCardList; 
		bool isSuccess = _battlefieldData.FindAllBattleCard(playerIndex, cardID, out battleCardList);
		if(isSuccess)
		{
			foreach(BattleCardData battleCard in battleCardList)
			{
				BattleCardUI battleCardUI;
				bool isFound = _battleUIManager.FieldUIManager.GetBattleCardUI(battleCard.BattleCardID, out battleCardUI);

				if(isFound && battleCardUI != null)
				{
					battleCardUI.ShowArrow(isShow);
				}
			}
		}
	}
#endregion

#region HandCardUI Properties
    public delegate void OnFinishDiscardCallback();
    public delegate void OnCancelDiscardCallback();
	public delegate void OnCompleteDiscardCallback();

	HandCardUI showActionMenuHandCardUI = null;
	private int _HandCardPopupUI_UniqueCardID = -1;

	private int _limitHandCardNum = 5;

    private int _removeHandCardNum = 0;
	private PlayerIndex _removeHandCardPlayerIndex;
    OnFinishDiscardCallback _onFinishDiscardCallback;
    OnCancelDiscardCallback _onCancelDiscardCallback; 
#endregion

#region HandCardUI Methods
    public void _CreateHandCardUI(UniqueCardData uniquePlayerCardData)
    {
        _battleUIManager.HandUIManager.CreateHandCard(uniquePlayerCardData, this.OnClickHandCard);

        OnPlayersInfoUpdateEvent();
    }

	private bool _DestroyHandCardUI(int uniqueCardID, PlayerIndex playerIndex)
	{
        PlayerBattleData playerBattleData = PlayersData[(int)playerIndex];
        IBattleControlController player = GetPlayer(playerIndex);

        if (IsLocalPlayer(playerIndex))
		{
			// Remove local hand card UI.
			bool isDestroyed = _battleUIManager.HandUIManager.DestroyCardByID(uniqueCardID);
			if(isDestroyed)
			{
				// Me summon unit.
				// boardcast to update new handCount amount.
				OnPlayersInfoUpdateEvent();
				return true;
			}
			else
			{
				Debug.LogError("BattleManager/RemoveHandCard: Failed to remove hand card UI.");
				return false;
			}
		}
		else
		{
			// boardcast to update new handCount amount.
			OnPlayersInfoUpdateEvent();
			return true;
		}
	}

    public void OnClickHandCard(HandCardUI handCardUI)
    {
        if (!IsPlayerCanAction) return;

		List<CardPopupMenuUI.CardPopupActionType> actionList;
		bool isActionable = GetHandCardActionableList(handCardUI.UniqueCardID, out actionList);
		if(isActionable)
		{
#if (UNITY_IOS && !UNITY_EDITOR)
			// iOS
			if((UnityEngine.iOS.Device.generation.ToString()).IndexOf("iPad") > -1)
			{
				// iPad
				RequestShowHandCardPopupUI(actionList, handCardUI);
			}
			else
			{
				// iPhone
				SoundManager.PlayEFXSound(BattleController.Instance.SelectSound);
				PopupRequestInfoHandCard(handCardUI.UniqueCardID);
			}
            
#elif (UNITY_ANDROID && !UNITY_EDITOR)
			// Android
            SoundManager.PlayEFXSound(BattleController.Instance.SelectSound);
            PopupRequestInfoHandCard(handCardUI.UniqueCardID);
            
#elif (UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_EDITOR)
            // Computer
            RequestShowHandCardPopupUI(actionList, handCardUI);

#else
            // Other
            SoundManager.PlayEFXSound(BattleController.Instance.SelectSound);
            PopupRequestInfoHandCard(handCardUI.UniqueCardID);
#endif
        }
    }

	public bool GetHandCardActionableList(int uniqueCardID, out List<CardPopupMenuUI.CardPopupActionType> actionList)
	{
		UniqueCardData uniqueCardData;
		PlayerIndex ownerPlayer;

		bool isFound = _battlefieldData.FindCardAtZone(
			  uniqueCardID
			, CardZoneIndex.Hand
			, out uniqueCardData
			, out ownerPlayer
		);

		if(!isFound)
		{
			Debug.LogError("BattleManager/GetHandCardActionableList: Not found clicked hand card data. [" + uniqueCardID.ToString() + "]");
			actionList = null;
			return false;
		}

		// Find local player.
		IBattleControlController localPlayer = null;
		isFound = this.GetLocalPlayer(out localPlayer);
		if(!isFound)
		{
			Debug.LogError("BattleManager/GetHandCardActionableList: Not found local player.");
			actionList = null;
			return false;
		}

		if(CurrentActivePlayerIndex != localPlayer.GetPlayerIndex())
		{
			// Not my turn.
			actionList = null;
			return false;
		}

		if (ownerPlayer == localPlayer.GetPlayerIndex())
		{
			// Clicked by owner.
			actionList = new List<CardPopupMenuUI.CardPopupActionType>();

			actionList.Add(CardPopupMenuUI.CardPopupActionType.Info);

			if( (  (uniqueCardData.PlayerCardData.TypeData == CardTypeData.CardType.Base)
				|| (uniqueCardData.PlayerCardData.TypeData == CardTypeData.CardType.Structure)
				|| (uniqueCardData.PlayerCardData.TypeData == CardTypeData.CardType.Unit))
				&& IsCanUseGold(ownerPlayer, uniqueCardData.PlayerCardData.Cost)
			)
			{
				if(
					   (BattleManager.ToDisplayBattlePhase(CurrentBattlePhase) == DisplayBattlePhase.PreBattle)
					|| (BattleManager.ToDisplayBattlePhase(CurrentBattlePhase) == DisplayBattlePhase.PostBattle)
				)
				{
					actionList.Add(CardPopupMenuUI.CardPopupActionType.Summon);
				}
			}

			if(uniqueCardData.PlayerCardData.TypeData == CardTypeData.CardType.Event)
			{
				if( IsCanUseGold(ownerPlayer, uniqueCardData.PlayerCardData.Cost)
					&& this.IsCanUseHandCard(uniqueCardData.UniqueCardID)
				)
				{
					if(    (BattleManager.ToDisplayBattlePhase(CurrentBattlePhase) == DisplayBattlePhase.PreBattle)
						|| (BattleManager.ToDisplayBattlePhase(CurrentBattlePhase) == DisplayBattlePhase.PostBattle)
					)
					{
						actionList.Add(CardPopupMenuUI.CardPopupActionType.Use);
					}
					else if(BattleManager.ToDisplayBattlePhase(CurrentBattlePhase) == DisplayBattlePhase.Battle)
					{
						foreach(CardSubTypeData subType in uniqueCardData.PlayerCardData.SubTypeDataList)
						{
							if(subType == CardSubTypeData.CardSubType.Sudden)
							{
								actionList.Add(CardPopupMenuUI.CardPopupActionType.Use);
								break;
							}
						}
					}
				}
			}

			if(uniqueCardData.PlayerCardData.TypeData == CardTypeData.CardType.Technic)
			{
				if( IsCanUseGold(ownerPlayer, uniqueCardData.PlayerCardData.Cost)
					&& this.IsCanUseHandCard(uniqueCardData.UniqueCardID)
				)
				{
					actionList.Add(CardPopupMenuUI.CardPopupActionType.Use);
				}
				//actionList.Add(CardPopupMenuUI.CardPopupActionType.Facedown);
			}

			if( (uniqueCardData.PlayerCardData.TypeData == CardTypeData.CardType.Item)
				&& IsCanUseGold(ownerPlayer, uniqueCardData.PlayerCardData.Cost)
			)
			{
				if(
					(BattleManager.ToDisplayBattlePhase(CurrentBattlePhase) == DisplayBattlePhase.PreBattle)
					|| (BattleManager.ToDisplayBattlePhase(CurrentBattlePhase) == DisplayBattlePhase.PostBattle)
				)
				{
					actionList.Add(CardPopupMenuUI.CardPopupActionType.Equip);
				}
			}
				
			return true;
		}
		else
		{
			actionList = null;
			return false;
		}
	}

    public void RequestShowHandCardPopupUI(List<CardPopupMenuUI.CardPopupActionType> actionList, HandCardUI handCardUI)
    {
        if (true)
        {
			SoundManager.PlayEFXSound(BattleController.Instance.SelectSound);
			this.RepeatSuggestText();

			SetActionMenuSelectCard(handCardUI);
			_HandCardPopupUI_UniqueCardID = handCardUI.UniqueCardID;

            _battleUIManager.CardPopupMenuUI.ShowUI(
                  actionList
				, handCardUI.UniqueCardID
                , handCardUI.gameObject
                , handCardUI.transform.position
                , this.OnActionHandCard
				, this.OnCloseActionMenu
                , UIWidget.Pivot.BottomLeft
            );
        }
    }
    
    private void ClosePopupMenuUI()
    {
        if(_battleUIManager.CardPopupMenuUI.IsShow)
        {
            _battleUIManager.CardPopupMenuUI.CloseUI();
        }
    }

	public void OnActionHandCard(CardPopupMenuUI.CardPopupActionType action, int uniqueCardID)
    {
		UniqueCardData uniqueCardData;
		PlayerIndex playerIndex;

		bool isFound = FindUniqueCard(uniqueCardID, out uniqueCardData, out playerIndex);
		if(isFound)
		{
			// Chose action. Unselect card.
			DeselectCard();

			switch(action)
			{
				case CardPopupMenuUI.CardPopupActionType.Info:
				{
					PopupRequestInfoHandCard(uniqueCardData.UniqueCardID);
				}
				break;

				case CardPopupMenuUI.CardPopupActionType.Summon:
				{
					PopupRequestSummonHandCard(uniqueCardData.UniqueCardID);
				}
				break;

				case CardPopupMenuUI.CardPopupActionType.Use:
				{
					PopupRequestUseHandCard(uniqueCardData.UniqueCardID);
				}
				break;

				default:
				{
					
				}
				break;
			}
		}
    }  

	public void PopupRequestInfoHandCard(int uniqueCardID)
	{
		ShowInfoHandCard(uniqueCardID);
	}

	public void PopupRequestSummonHandCard(int uniqueCardID)
	{
        //Debug.Log("PopupRequestSummonHandCard : " + uniqueCardID);
    
        if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
        {
            KOSNetwork.GamePlayer.localPlayer.RequestSummonUniqueCard(uniqueCardID, LocalBattleZoneIndex.PlayerBase, false, false);
        }
        else
        {
	        RequestSummonUniqueCard(uniqueCardID, KOSNetwork.NetworkSetting.IsHaveTimeout);
        }
	}

	public void PopupRequestUseHandCard(int uniqueCardID)
	{
        /*
        if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
        {
            KOSNetwork.GamePlayer.localPlayer.CmdUseHandCard(uniqueCardID);
        }
        else
        */
        {
		    RequestUseHandCard(uniqueCardID, true);
        }
	}

	private void ShowInfoHandCard(int uniqueCardID)
	{
		PlayerIndex ownerPlayer;
		UniqueCardData uniqueCardData;

		// Find hand card data.
		bool isFound = _battlefieldData.FindCardAtZone(uniqueCardID, CardZoneIndex.Hand, out uniqueCardData, out ownerPlayer);
		if(!isFound)
		{
			Debug.LogError("BattleManager/ShowInfoHandCard: Not found hand card data.");
			return ;
		}
        
        Debug.Log("ShowInfoHandCard: " + uniqueCardID);

		DeselectCard();
		_battleUIManager.ShowFullCardInfoUI(uniqueCardData);
	}
    
    private void CloseCardInfo()
    {
        if(_battleUIManager.FullCardInfoUI.IsShow)
        {
            _battleUIManager.FullCardInfoUI.CloseUI();
        }
    }
		
	public bool IsCanUseHandCard(int uniqueCardID)
	{
		PlayerIndex ownerPlayer;
		UniqueCardData uniqueCardData;

		// Find use hand card data.
		bool isFound = _battlefieldData.FindCardAtZone(uniqueCardID, CardZoneIndex.Hand, out uniqueCardData, out ownerPlayer);
		if(!isFound)
		{
			Debug.LogError("BattleManager/IsCanUseHandCard: Not found hand card data.");
			return false;
		}
			
		if(	   (uniqueCardData.PlayerCardData.TypeData == CardTypeData.CardType.Event)
			|| (uniqueCardData.PlayerCardData.TypeData == CardTypeData.CardType.Technic)
		)
		{
			// Event and Technic
			if(!uniqueCardData.IsInitAbility)
			{
				uniqueCardData.InitAbility();
			}

			return uniqueCardData.IsCanUse();
		}
			
		return false;
	}

	public void ShowHandCardArrow(PlayerIndex playerIndex, int handIndex, bool isShow)
	{
		List<UniqueCardData> handCardList = _battlefieldData.PlayersData[(int)playerIndex].GetZoneDataList(CardZoneIndex.Hand);
		if(handCardList != null && handIndex < handCardList.Count)
		{
			HandCardUI handUI = _battleUIManager.HandUIManager.GetCardByID(handCardList[handIndex].UniqueCardID);

			if(handUI != null)
			{
				handUI.ShowArrow(isShow);
			}
		}
	}

	public void ShowHandCardArrow(PlayerIndex playerIndex, string cardID, bool isShow)
	{
		//Debug.Log("ShowHandCardArrow");

		List<UniqueCardData> handCardList; 
		bool isSuccess = _battlefieldData.FindAllCardAtZone(cardID, playerIndex, CardZoneIndex.Hand, out handCardList);
		if(isSuccess)
		{
			foreach(UniqueCardData uniqueCard in handCardList)
			{
				HandCardUI handUI = _battleUIManager.HandUIManager.GetCardByID(uniqueCard.UniqueCardID);

				if(handUI != null)
				{
					handUI.ShowArrow(isShow);
				}
			}
		}
	}
#endregion

#region UseHandCard Properties
	private bool _isUsingHandCard = false;
	private int _useHandCardUniqueID = -1;
	public delegate void OnFinishUseHandCard();
	public delegate void OnCancelUseHandCard();

	private OnFinishUseHandCard _onFinishUseHandCard = null;
	private OnCancelUseHandCard _onCancelUseHandCard = null;
#endregion

#region UseHandCard Methods
	private void ConfirmUseHandCard()
	{
        PlayerIndex ownerPlayer;
        UniqueCardData uniqueCardData;

        // Find summon hand card data.
        bool isFound = _battlefieldData.FindCardAtZone(_useHandCardUniqueID, CardZoneIndex.Hand, out uniqueCardData, out ownerPlayer);
        if (!isFound)
        {
            Debug.LogError("BattleManager/OnConfirmedUseHandCard: Not found hand card data.");
            return;
        }

		// Action
		ActionGetGold(ownerPlayer, -uniqueCardData.PlayerCardData.Cost);
	}
		
    private void AfterUseHandCard()
    {
        PlayerIndex ownerIndex;
        UniqueCardData uniqueCard;

        // Find summon hand card data.
        bool isFound = _battlefieldData.FindCardAtZone(_useHandCardUniqueID, CardZoneIndex.Hand, out uniqueCard, out ownerIndex);
        if (!isFound)
        {
            Debug.LogFormat("BattleManager/OnAfterUseHandCard: <color=red>[Error]</color> Not found hand card data. {0}", _useHandCardUniqueID);
            //return;
        }

        if (uniqueCard != null)
        {
            // Move data to graveyard
            if (_battlefieldData.PlayersData != null && _battlefieldData.PlayersData.Length > (int)ownerIndex)
            {
                _battlefieldData.PlayersData[(int)ownerIndex].MoveUniqueCard(uniqueCard.UniqueCardID, CardZoneIndex.Graveyard);
            }
            else
            {
                Debug.LogFormat("BattleManager/OnAfterUseHandCard: <color=red>[Error]</color> Not found player data. {0}", ownerIndex);               
            }

            // Remove hand card UI
            bool isRemoved = this._DestroyHandCardUI(uniqueCard.UniqueCardID, ownerIndex);
            if (!isRemoved)
            {
                Debug.LogWarning("BattleManager/AfterUseHandCard: <color=red>[Error]</color> Failed to remove used hand card.");
                //return;
            }
        }

		if(_onFinishUseHandCard != null)
		{
			_onFinishUseHandCard.Invoke();
		}
    }

	private void CancelUseHandCard()
	{
        _useHandCardUniqueID = -1;

		if(_onCancelUseHandCard != null)
		{
			_onCancelUseHandCard.Invoke();
		}
	}
    #endregion

    public void RequestDisHandCardOverflow(PlayerIndex playerIndex, int amountCard, OnFinishDiscardCallback onFinishDiscardCallback = null, OnCancelDiscardCallback onCancelDiscardCallback = null, bool isCanCancel = false)
    {
        IBattleControlController player = _players[(int)playerIndex];

        player.StartDiscard(
              amountCard
            , this.OnConfirmDisHandCard
            , null
            , true
            , false
        );       
    }
    
    private void OnConfirmDisHandCard(List<int> uniqueCardIDList)
    {
        if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
        {
            KOSNetwork.GamePlayer.localPlayer.RequestDisHandCardOverflow(uniqueCardIDList);
        }
        else
        {
            DisHandCardOverflow(uniqueCardIDList);
        }
    }
    
    public void DisHandCardOverflow(List<int> uniqueCardIDList)
    {
        foreach(int uniqueCardID in uniqueCardIDList)
        {
            ActionMoveUniqueCard(uniqueCardID, CardZoneIndex.Graveyard);
        }
        
        NextPhase();
    }

    public void RequestDiscard(PlayerIndex playerIndex, int amountCard, OnFinishDiscardCallback onFinishDiscardCallback = null, OnCancelDiscardCallback onCancelDiscardCallback = null, bool isCanCancel = false)
    {
        if (IsCanDiscard(playerIndex, amountCard))
        {
            _removeHandCardNum = amountCard;
            _onFinishDiscardCallback = onFinishDiscardCallback;
            _onCancelDiscardCallback = onCancelDiscardCallback;

			_removeHandCardPlayerIndex = playerIndex;
			IBattleControlController player = _players[(int)_removeHandCardPlayerIndex];

            // Can't discard together. (BUG)
            player.StartDiscard(
                  amountCard
                , this.OnConfirmDiscard
                , this.OnCancelDiscard
                , false
                , isCanCancel
            );       
        }
		else
		{
			this.OnCancelDiscard();
		}

    }
		
	private void OnConfirmDiscard(List<int> uniqueCardIDList)
    {
        foreach(int uniqueCardID in uniqueCardIDList)
        {
            ActionMoveUniqueCard(uniqueCardID, CardZoneIndex.Graveyard);
        }

        if (_onFinishDiscardCallback != null)
        {
            _onFinishDiscardCallback.Invoke();
            _onFinishDiscardCallback = null;
        }
    }

    /*
	public bool DiscardHandCard(PlayerIndex playerIndex, List<int> uniqueCardIDList)
	{
		if(uniqueCardIDList.Count > 0)
		{
			IBattleControlController player = _players[(int)playerIndex];

			foreach(int uniqueCardID in uniqueCardIDList)
			{
				this._RemoveHandCardUI(uniqueCardID, );
			}
				
			return true;
		}

		return false;
	}
    */

    private void OnCancelDiscard()
    {
        if (_onCancelDiscardCallback != null)
        {
            _onCancelDiscardCallback.Invoke();
            _onCancelDiscardCallback = null;
        }
    }
    
	/*
    public void OnShowSelectCardGridUI(string headerText, List<UniqueCardData> cardList, int amountCard, SelectCardGridUI.OnConfirmEvent onConfirmCallback, SelectCardGridUI.OnCancelEvent onCancelCallback, bool isCanCancel)
    {
        _battleUIManager.ShowSelectCardGridUI(headerText, cardList, amountCard, onConfirmCallback, onCancelCallback, isCanCancel);
    }
	*/
    
#region SelectCardPageUI Properties
    private bool _isSelectCardPage = false;
    private bool _isCanCancelSelectCardPage = false;
    
    private List<UniqueCardData> _cardPageList;
    private int _selectCardPageNum = 1;
    
    private SelectCardPageUI.OnConfirmEvent _onConfirmSelectCardPage;
    private SelectCardPageUI.OnCancelEvent _onCancelSelectCardPage;
#endregion
    
#region SelectCardPageUI Methods

	public void OnShowSelectCardPageUI(string topicText, string headerText, List<UniqueCardData> cardList, int amountCard, SelectCardPageUI.OnConfirmEvent onConfirmCallback, SelectCardPageUI.OnCancelEvent onCancelCallback, bool isHaveTimeout, bool isCanCancel)
	{
        _isSelectCardPage = true;
        _isCanCancelSelectCardPage = isCanCancel;
        
        _cardPageList = cardList;
        _selectCardPageNum = amountCard;
    
        _onConfirmSelectCardPage = onConfirmCallback;
        _onCancelSelectCardPage = onCancelCallback;
    
		_battleUIManager.ShowSelectCardPageUI(
              topicText
            , headerText
            , cardList
            , amountCard
            , this.OnConfirmSelectCardPage
            , this.OnCancelSelectCardPage
            , isHaveTimeout
            , isCanCancel
        );
	}
    
    private void OnConfirmSelectCardPage(List<int> uniqueCardIDList)
    {
        _isSelectCardPage = false;
        
        if(_onConfirmSelectCardPage != null)
        {
            _onConfirmSelectCardPage.Invoke(uniqueCardIDList);
        }
    }
    
    private void OnCancelSelectCardPage()
    {
        _isSelectCardPage = false;
    
        if(_onCancelSelectCardPage != null)
        {
            _onCancelSelectCardPage.Invoke();
        }
    }
    
    private void AutoSelectOrCancelCardPage()
    {
        if(_isSelectCardPage)
        {    
            _battleUIManager.SelectCardPageUI.AutoSelectOrCancelSelectCard();
            
            /*        
            _battleUIManager.SelectCardPageUI.HideUI();
             
            if(_isCanCancelSelectCardPage)
            {
                // if can cancel will cancel.     
                Debug.Log("AutoSelectOrCancelCardPage: Cancel");    
                         
                OnCancelSelectCardPage();
            }
            else
            {
                // Auto select card.
                Debug.Log("AutoSelectOrCancelCardPage: Auto Select");   
                
                _selectList = new List<int>();
                for(int index = 0; index < _selectCardPageNum; ++index)
                {
                    _selectList.Add(_cardPageList[index].UniqueCardID);
                }
                
                OnConfirmSelectCardPage(_selectList);
            }
            */
        }
    }
#endregion

    private void RemoveAllHandCardUI()
    {
        _battleUIManager.HandUIManager.DestroyAllCard();
    }

#region Select HandCard Properties
	public delegate bool IsHandCardCanSelect(PlayerCardData playerCardData);
	public delegate void OnFinishSelectHandCardCallback(List<int> handCardIDList);
	public delegate void OnCancelSelectHandCardCallback();

	public delegate void OnFinishSelectUniqueCardCallback(List<int> uniqueCardIDList);
	public delegate void OnCancelSelectUniqueCardCallback();
#endregion

#region Select HandCard Methods
	public void StartSelectHandCard(
		  string headerText
		, PlayerIndex playerIndex
		, int selectNum
		, OnFinishSelectHandCardCallback onFinish
		, OnCancelSelectHandCardCallback onCancel
		, IsHandCardCanSelect isHandCardCanSelect //= null
        , bool isHaveTimeout
		, bool isCanCancel //= false
	)
	{
        if( (KOSNetwork.NetworkSetting.IsNetworkEnabled) && 
            (playerIndex != KOSNetwork.GamePlayer.localPlayer.playerIndex)
        )
        {
            BattleManager.Instance.RequestWaitActiveQueue();
        
            if(onCancel != null)
            {
                onCancel.Invoke();
            }
            return;
        }
    
		IBattleControlController player = GetPlayer(playerIndex);
		List<UniqueCardData> handCardList = PlayersData[(int)playerIndex].GetZoneDataList(CardZoneIndex.Hand);
        if (handCardList != null && handCardList.Count > 0)
		{
			List<UniqueCardData> selectCardList = new List<UniqueCardData>();

			if(isHandCardCanSelect != null)
			{
				foreach(UniqueCardData card in handCardList)
				{
					if(isHandCardCanSelect.Invoke(card.PlayerCardData))
					{
						selectCardList.Add(card);
					}
				}
			}
			else
			{
				selectCardList.AddRange(handCardList);
			}

			if(selectCardList.Count > 0)
			{
				player.StartSelectHandCard(
					  headerText
					, selectCardList
					, selectNum
					, onFinish
					, onCancel
                    , isHaveTimeout
					, isCanCancel
				);

				return;
			}
		}

		if(isCanCancel)
		{
			if(onCancel != null)
			{
				onCancel.Invoke();
			}
		}
		else
		{
			if(onFinish != null)
			{
				List<int> emptyList = new List<int>();
				onFinish.Invoke(emptyList);
			}
		}
	}

	public void StartSelectUniqueCard(
		string headerText
		, PlayerIndex playerIndex
		, List<UniqueCardData> selectCardList
		, int selectNum
		, OnFinishSelectUniqueCardCallback onFinish
		, OnCancelSelectUniqueCardCallback onCancel
		, IsHandCardCanSelect isHandCardCanSelect //= null
        , bool isHaveTimeout
		, bool isCanCancel //= false
	)
	{
        if( (KOSNetwork.NetworkSetting.IsNetworkEnabled) && 
            (playerIndex != KOSNetwork.GamePlayer.localPlayer.playerIndex)
        )
        {
            BattleManager.Instance.RequestWaitActiveQueue();
        
            if(onCancel != null)
            {
                onCancel.Invoke();
            }
            
            return;
        }

		IBattleControlController player = GetPlayer(playerIndex);

        if (selectCardList != null && selectCardList.Count > 0)
		{
			player.StartSelectUniqueCard(
				  headerText
				, selectCardList
				, selectNum
				, onFinish
				, onCancel
                , isHaveTimeout
				, isCanCancel
			);

			return;
		}

		if(isCanCancel)
		{
			if(onCancel != null)
			{
				onCancel.Invoke();
			}
		}
		else
		{
			if(onFinish != null)
			{
				List<int> emptyList = new List<int>();
				onFinish.Invoke(emptyList);
			}
		}
	}

	public void StartSelectHandCardRandom(
          PlayerIndex requestIndex
		, PlayerIndex playerIndex
		, int selectNum
		, OnFinishSelectHandCardCallback onFinish
        , OnCancelSelectHandCardCallback onCancel
		, IsHandCardCanSelect isHandCardCanSelect = null
	)
	{
        if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
        {        
            if(requestIndex != KOSNetwork.GamePlayer.localPlayer.playerIndex)      
            {
                BattleManager.Instance.RequestWaitActiveQueue();
            
                if(onCancel != null)
                {
                    onCancel.Invoke();
                }
                
                return;
            }
        }
    
		IBattleControlController player = GetPlayer(playerIndex);
		List<UniqueCardData> handCardList = PlayersData[(int)playerIndex].GetZoneDataList(CardZoneIndex.Hand);
        
		if (handCardList != null && handCardList.Count > 0)
		{
			List<UniqueCardData> selectCardList = new List<UniqueCardData>();

			if(isHandCardCanSelect != null)
			{
				foreach(UniqueCardData card in handCardList)
				{
					if(isHandCardCanSelect.Invoke(card.PlayerCardData))
					{
						selectCardList.Add(card);
					}
				}
			}
			else
			{
				selectCardList.AddRange(handCardList);
			}

			List<int> selectedList = new List<int>();
            System.Random r = new System.Random();

			while (selectCardList.Count > 0 && selectedList.Count < selectNum)
			{
				int selectIndex = r.Next(0, selectCardList.Count);
				selectedList.Add (selectCardList[selectIndex].UniqueCardID);
				selectCardList.RemoveAt (selectIndex);
			}

			onFinish.Invoke(selectedList);
		}
        else
        {
            if(onCancel != null)
            {
                onCancel.Invoke();
            }
        }
	}
#endregion

#region SelectTarget Properties
	public delegate void OnFinishSelectTargetCallback(List<int> battleIDList);
	public delegate void OnCancelSelectTargetCallback();

	private List<BattleCardData> _selectTargetList;
	private List<int> _selectList;
	private int _selectTargetNum = 1;
    private bool _isCanCancelSelectTarget = false;
    private bool _isFinishSelectTarget = false;
    
    private bool _isEnableSelectTargetTimer = false;
    private bool _isSelectTargetPauseTimeout = false;
    private float _selectTargetTimer = 0.0f;
    
	private OnFinishSelectTargetCallback _onFinishSelectTarget;
	private OnCancelSelectTargetCallback _onCancelSelectTarget;
#endregion

#region SelectTarget Methods
	public void RequestSelectTarget(
		  string headerText
		, PlayerIndex playerIndex
		, List<BattleCardData> targetList
		, int targetNum
		, OnFinishSelectTargetCallback onFinish
		, OnCancelSelectTargetCallback onCancel
        , bool isHaveTimeout
		, bool isCanCancel
	)
	{
		IBattleControlController player = GetPlayer(playerIndex);

		player.StartSelectTarget(
			  headerText
			, targetList
			, targetNum
			, onFinish
			, onCancel
            , isHaveTimeout
			, isCanCancel
		);
	}

	public void ShowSelectTargetUI(
		  string headerText
        , PlayerIndex requesterIndex
		, List<BattleCardData> selectableList
		, int num
		, OnFinishSelectTargetCallback onFinish
		, OnCancelSelectTargetCallback onCancel
        , bool isHaveTimeout
		, bool isCanCancel
	)
	{
        if( (KOSNetwork.NetworkSetting.IsNetworkEnabled) && 
            (requesterIndex != KOSNetwork.GamePlayer.localPlayer.playerIndex)
        )
        {
            BattleManager.Instance.RequestWaitActiveQueue();
        
            if(onCancel != null)
            {
                onCancel.Invoke();
            }
            return;
        }
    
		_isSelectTarget = true;
        _isCanCancelSelectTarget = isCanCancel;
        
        _isEnableSelectTargetTimer = isHaveTimeout;
        _selectTargetTimer = PopupDecisionTime;
        _isSelectTargetPauseTimeout = _isEnableSelectTargetTimer;
        if(_isSelectTargetPauseTimeout)
        {
            if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
            {
                KOSNetwork.GamePlayer.localPlayer.RequestPauseTimer(true, this.TurnTimer);
            }
            else
            {
                SetPauseTimeout(true);
            }
        }
        
		//Debug.Log("BattleManager: ShowSelectTargetUI");
		_selectList = new List<int>();
		_selectTargetList = selectableList;
		_selectTargetNum = num;

		_onFinishSelectTarget = onFinish;
		_onCancelSelectTarget = onCancel;
               
        this.AddSuggestText(headerText);
        
        if(_selectTargetList == null || _selectTargetList.Count <= 0)
        {
            if(!isCanCancel)
            {
                OnFinishSelectTarget(_selectList);
            }
            else
            {
                OnCancelSelectTarget();
            }
        }
        else
        {
            /*
            if(_selectTargetList.Count <= _selectTargetNum && !isCanCancel)
            {
                foreach(BattleCardData card in _selectTargetList)
                {
                    _selectList.Add(card.BattleCardID);
                }
            
                OnFinishSelectTarget(_selectList);
            }
            else
            */
            {
                ShowSelectableTarget(_selectTargetList);
                
                if(!IsTutorial && isCanCancel)
                {
                    _battleUIManager.SelectBattleCardUI.ShowUI(this.OnCancelSelectTarget);
                }
                else
                {
                    _battleUIManager.SelectBattleCardUI.HideUI();
                }
            }
        }
	}

	private void ShowSelectableTarget(List<BattleCardData> targetList)
	{
		//Debug.Log("BattleManager: ShowSelectableTarget");
		// diable click all hand & battle card.
       
		_battleUIManager.FieldUIManager.SetAllBattleCardUIClickable(false);
		_battleUIManager.HandUIManager.SetAllHandCardClickable(false);

		// enable click battle card.
		foreach(BattleCardData target in targetList)
		{
			_battleUIManager.FieldUIManager.SetBattleCardUIClickable(target.BattleCardID, true);
		}

		_clickBattleCardState = ClickBattleCardState.SelectTarget;

		if(this.IsTutorial)
		{
			if(this.Turn == 5)
			{
				this.ShowBattleCardArrow(PlayerIndex.Two, "C0009", true);
			}
		}
	}
    
    private void AutoSelectOrCancelTarget()
    {
        if(_isSelectTarget)
        {
            _battleUIManager.SelectBattleCardUI.HideUI();
             
            if(_isCanCancelSelectTarget)
            {
                // if can cancel will cancel.       
                Debug.Log("AutoSelectOrCancelTarget: Cancel");      
                
                this.OnCancelSelectTarget();
            }
            else
            {
                // Auto select target.
                Debug.Log("AutoSelectOrCancelTarget: Auto Select");      
                
                _selectList = new List<int>();
                for(int index = 0; index < _selectTargetNum; ++index)
                {
                    if(_selectTargetList != null && index < _selectTargetList.Count)
                    {
                        _selectList.Add(_selectTargetList[index].BattleCardID);
                    }
                }
                
                OnFinishSelectTarget(_selectList);
            }
        }
    }

	private void OnSelectTarget(BattleCardUI battleCardUI, int battleCardID)
	{
		if(IsTutorial)
		{
			if(this.Turn == 5)
			{
				if(string.Compare(battleCardUI.ShowBattleCardData.PlayerCardData.PlayerCardID, "C0009") != 0)
				{
					return;
				}
				else
				{
					this.ShowBattleCardArrow(PlayerIndex.Two, "C0009", false);
				}
			}
		}

		if(_selectList.Count > 0)
		{
			foreach(int id in _selectList)
			{
				if(battleCardID == id)
				{
					return;
				}
			}
		}

		_selectList.Add(battleCardID);

		if(_selectList.Count == _selectTargetNum || _selectList.Count == _selectTargetList.Count)
		{
			this.OnFinishSelectTarget(_selectList);
		}
	}

	private void ClearSelectableTarget()
	{
		//Debug.Log("BattleManager: ClearSelectableTarget");

		// enable click all hand & battle card.
		_battleUIManager.FieldUIManager.SetAllBattleCardUIClickable(true);
		_battleUIManager.HandUIManager.SetAllHandCardClickable(true);

		_clickBattleCardState = ClickBattleCardState.ShowActionMenu;
	}
    
    private void OnFinishSelectTarget(List<int> battleIDList) 
    {
        Debug.Log("BattleManager: OnFinishSelectTarget");
        
        _isSelectTarget = false;
        if(_isSelectTargetPauseTimeout)
        {
            _isSelectTargetPauseTimeout = false;
            if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
            {
                KOSNetwork.GamePlayer.localPlayer.RequestPauseTimer(false, this.TurnTimer);
            }
            else
            {
                SetPauseTimeout(false);
            }
        }
        
        this.RemoveSuggestText();
        ClearSelectableTarget();
        _battleUIManager.SelectBattleCardUI.HideUI();

        /*
        if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
        {            
            KOSNetwork.GamePlayer.localPlayer.CmdFinishSelectBattleCard(battleIDList);
        }
        else
        */
        {
            FinishSelectTarget(battleIDList);
        }    
    }
    
    public void FinishSelectTarget(List<int> battleIDList)
    {
        _selectList = battleIDList;
        _isFinishSelectTarget = true;
       
        if(_onFinishSelectTarget != null)
        {
            _selectList = null;
            _isFinishSelectTarget = false;
            _onFinishSelectTarget.Invoke(battleIDList);
        }
    }
    
	public void OnCancelSelectTarget()
	{
		_isSelectTarget = false;
        if(_isSelectTargetPauseTimeout)
        {
            _isSelectTargetPauseTimeout = false;
            if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
            {
                KOSNetwork.GamePlayer.localPlayer.RequestPauseTimer(false, this.TurnTimer);
            }
            else
            {
                SetPauseTimeout(false);
            }
        }
        //Debug.Log("BattleManager: OnCancelSelectTarget");
        
		this.RemoveSuggestText();
		ClearSelectableTarget();

        /*
		if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
        {            
            KOSNetwork.GamePlayer.localPlayer.CmdCancelSelectBattleCard();
        }
        else
        */
        {
            CancelSelectTarget();
        }
        
	}
    
    public void CancelSelectTarget() 
    { 
        if(_onCancelSelectTarget != null)
        {
            _selectList = null;
            _isFinishSelectTarget = false;
            _onCancelSelectTarget.Invoke();
        }
    }
    
    private void OnSelectTargetTimeout()
    {
        if(_isEnableSelectTargetTimer)
        {
            _isEnableSelectTargetTimer = false;
            
            AutoSelectOrCancelTarget();
        }
    }
#endregion

#region Ability Properties
	// Deal damage.
	public delegate void OnFinishDealDamage(List<int> selectedTargetList);
	public delegate void OnCancelDealDamage();

	// Back to owner hand.
	public delegate void OnFinishBackToOwnerHand(List<int> selectedTargetList);
	public delegate void OnCancelBackToOwnerHand();
#endregion

#region Ability Methods
    public bool IsCanDrawCard(PlayerIndex playerIndex, int amountCard)
    { 
        PlayerBattleData playerBattleData = PlayersData[(int)playerIndex];

        if (playerBattleData.Deck.Count >= amountCard)
        {
            return true;
        }

        return false;
    }

    public bool IsCanDiscard(PlayerIndex playerIndex, int amountCard)
    {
		if (GetHandCount(playerIndex) >= amountCard && amountCard > 0)
        {
            return true;
        }

        return false;
    }

	public bool IsCanSelectHandCard(PlayerIndex playerIndex, int amountCard)
	{
		if (amountCard <= GetHandCount(playerIndex))
		{
			return true;
		}

		return false;
	}

    public bool IsCanUseGold(PlayerIndex playerIndex, int amountGold)
    {
        if(_battlefieldData.PlayersData[(int)playerIndex].Gold >= amountGold)
        {
            return true;
        }

        return false;
    }

	public void ClearPlayerGold(PlayerIndex playerIndex)
	{
		_battlefieldData.PlayersData[(int)playerIndex].ClearGold();

        // For Debug
		//if(this.CurrentActivePlayerIndex == PlayerIndex.One) 
		//ActionGetGold(playerIndex, 1000);

        OnPlayersInfoUpdateEvent();
	}
		
	public void RequestDealDamage(
		  PlayerIndex requesterIndex
		, List<BattleCardData> targetList
		, int damage
		, int num
		, OnFinishDealDamage onFinish
		, OnCancelDealDamage onCancel //= null
        , bool isHaveTimeout
        , bool isCanCancel
	)
	{
        if( (KOSNetwork.NetworkSetting.IsNetworkEnabled) && 
            (requesterIndex != KOSNetwork.GamePlayer.localPlayer.playerIndex)
        )
        {
            BattleManager.Instance.RequestWaitActiveQueue();
        
            if(onCancel != null)
            {
                onCancel.Invoke();
            }
            return;
        }
    
		//Debug.Log("RequestDealDamage : " + requesterIndex.ToString());
		IBattleControlController player = GetPlayer(requesterIndex);

		player.StartSelectDealDamage(
			  targetList
			, damage
			, num
			, onFinish
			, onCancel
            , isHaveTimeout
            , isCanCancel
		);
	}

	public bool CheckIsDead(int battleCardID)
	{
		BattleCardData target;
		bool isFound = _battlefieldData.FindBattleCard(battleCardID, out target);
		if(isFound)
		{
			// Check target
			if(target.IsDead && !target.IsDestroy)
			{
				RequestDestroyBattleCard(battleCardID, CardZoneIndex.Graveyard);
			}
			else
			{
				// update new data.
				bool isSuccess = RequestUpdateBattleCardUI(target.BattleCardID);
				if(!isSuccess)
				{
					//Debug.LogError("BattleManager/DealDamageTo: not found target battle card UI");
					return false;
				}
			}

			return true;
		}

		return false;
	}

	public void RequestBackToOwnerHand(
		  PlayerIndex requesterIndex
		, List<BattleCardData> targetList
		, int num
		, OnFinishBackToOwnerHand onFinish
		, OnCancelBackToOwnerHand onCancel
        , bool isHaveTimeout
        , bool isCanCancel
	)
	{
        if( (KOSNetwork.NetworkSetting.IsNetworkEnabled) && 
            (requesterIndex != KOSNetwork.GamePlayer.localPlayer.playerIndex)
        )
        {
            BattleManager.Instance.RequestWaitActiveQueue();
        
            if(onCancel != null)
            {
                onCancel.Invoke();
            }
            return;
        }
    
		IBattleControlController player = GetPlayer(requesterIndex);
        
		player.StartSelectBackToOwnerHand(
			  targetList
			, num
			, onFinish
			, onCancel
            , isHaveTimeout
            , isCanCancel
		);
	}

	/*
	public void ConfirmedBackToOwnerHand(List<int> battleCardIDList)
	{
		foreach(int id in battleCardIDList)
		{
			bool isSuccess = BackToOwnerHand(id);
			if(!isSuccess)
			{
				Debug.LogError("Failed to back to owner hand to battleID:" + id.ToString());
				return;
			}
		}

		if(_onFinishBackToOwnerHand != null)
		{
			_onFinishBackToOwnerHand.Invoke();
		}
	}

	public void CancelBackToOwnerHand()
	{
		// Do nothing
	}
	*/

	/*
	public bool BackToOwnerHand(int battleCardID)
	{
		BattleCardData target;
		bool isFound = _battlefieldData.FindBattleCard(battleCardID, out target);
		if(isFound)
		{
			// destroy UI
			bool isSuccess = _battleUIManager.FieldUIManager.DestroyBattleCardUI(target.BattleCardID);
			if(isSuccess)
			{
				_battlefieldData.MoveBattleCard(target.BattleCardID, CardZoneIndex.Hand);
			}
			else
			{
				Debug.LogError("BattleManager/DealDamageTo: Failed to destroy target BattleCardUI.");
				return false;
			}

			return true;
		}

		return false;	
	}
	*/

	private void BattleCardPhaseChangeTrigger(BattlePhase battlePhase)
	{
		//Debug.Log("BattleCardPhaseChange Trigger!");

		PlayerIndex currentplayerIndex = this.CurrentActivePlayerIndex;
		PlayerIndex enemyPlayerIndex;
		if(currentplayerIndex == PlayerIndex.One)
		{
			enemyPlayerIndex = PlayerIndex.Two;
		}
		else
		{
			enemyPlayerIndex = PlayerIndex.One;
		}

		// Trigger at phase.
		OnBattleEventTrigger(
			  currentplayerIndex
			, BattleEventTrigger.BattleTriggerType.BEGIN_OF_PHASE
			, BattleEventTrigger.BattleSide.Friendly
		);

		OnBattleEventTrigger(
			  currentplayerIndex
			, BattleEventTrigger.BattleTriggerType.END_OF_PHASE
			, BattleEventTrigger.BattleSide.Friendly
		);

		OnBattleEventTrigger(
			  currentplayerIndex
			, BattleEventTrigger.BattleTriggerType.BEGIN_OF_PHASE
			, BattleEventTrigger.BattleSide.Enemy
		);

		OnBattleEventTrigger(
			  currentplayerIndex
			, BattleEventTrigger.BattleTriggerType.END_OF_PHASE
			, BattleEventTrigger.BattleSide.Enemy
		);
	}
#endregion

#region BattleEvent Trigger Methods
	public void BindBattleEventTrigger(
		  BattleEventTrigger.BattleTriggerEvent triggerCallback
		, PlayerIndex playerIndex
		, BattleEventTrigger.BattleTriggerType battleTriggerType
		, BattleEventTrigger.BattleSide battleSide
	)
	{
		//Debug.Log("Bind:" + playerIndex.ToString() + ":" + battleTriggerType.ToString() + ":" + battleSide.ToString());
		_battleEventTriggers[(int)playerIndex].BindBattleTrigger(triggerCallback, battleTriggerType, battleSide);
	}

	public void BindBattleEventTrigger(
		BattleEventTrigger.BattleTriggerEvent triggerCallback
		, PlayerIndex playerIndex
		, BattleEventTrigger.BattleTriggerType battleTriggerType
		, int battleCardID
	)
	{
		//Debug.Log("Bind:" + playerIndex.ToString() + ":" + battleTriggerType.ToString() + ":" + battleCardID.ToString());
		_battleEventTriggers[(int)playerIndex].BindBattleTrigger(triggerCallback, battleTriggerType, battleCardID);
	}

	public bool UnbindBattleEventTrigger(
		BattleEventTrigger.BattleTriggerEvent triggerCallback
		, PlayerIndex playerIndex
		, BattleEventTrigger.BattleTriggerType battleTriggerType
		, BattleEventTrigger.BattleSide battleSide
	)
	{
		//Debug.Log("Unbind:" + playerIndex.ToString() + ":" + battleTriggerType.ToString() + ":" + battleSide.ToString());
		return _battleEventTriggers[(int)playerIndex].UnbindBattleTrigger(triggerCallback, battleTriggerType, battleSide);
	}

	public bool UnbindBattleEventTrigger(
		  BattleEventTrigger.BattleTriggerEvent triggerCallback
		, PlayerIndex playerIndex
		, BattleEventTrigger.BattleTriggerType battleTriggerType
		, int battleCardID
	)
	{
		//Debug.Log("Unbind:" + playerIndex.ToString() + ":" + battleTriggerType.ToString() + ":" + battleCardID.ToString());
		return _battleEventTriggers[(int)playerIndex].UnbindBattleTrigger(triggerCallback, battleTriggerType, battleCardID);
	}

	public void OnBattleEventTrigger(
		  PlayerIndex playerIndex
		, BattleEventTrigger.BattleTriggerType battleTriggerType
		, BattleEventTrigger.BattleSide battleSide
	)
	{
		//Debug.Log("Trigger:" + playerIndex.ToString() + ":" + battleTriggerType.ToString() + ":" + battleSide.ToString());
		_battleEventTriggers[(int)playerIndex].OnBattleEventTrigger(battleTriggerType, battleSide);
	}

	public void OnBattleEventTrigger(
		  PlayerIndex playerIndex
		, BattleEventTrigger.BattleTriggerType battleTriggerType
		, int battleCardID
	)
	{
		//Debug.Log("Trigger:" + playerIndex.ToString() + ":" + battleTriggerType.ToString() + ":" + battleCardID.ToString());
		_battleEventTriggers[(int)playerIndex].OnBattleEventTrigger(battleTriggerType, battleCardID);
	}
#endregion

#region Turn Time
    private void SetEnableTimeout(bool isEnable)
    {
        if(IsHaveTimeout)
        {
            _isEnableTimeout = isEnable;
            
            if(_isEnableTimeout)
            {
                SetRemainTurnTime(PlayTimePerTurn);
                _isTimeout = false;
                _isPauseTimeout = false;
                _pauseTimerCount = 0;
                return;
            }
        }

        SetRemainTurnTime(0.0f);
        _isEnableTimeout = false;
        _isTimeout = false;
        _isPauseTimeout = false;
        _pauseTimerCount = 0;
    }
    
    public void SetPauseTimeout(bool isPause)
    {
        //_isPauseTimeout = isPause;
        
        if(isPause)
        {
            ++_pauseTimerCount;
        }
        else
        {
            --_pauseTimerCount;
            if(_pauseTimerCount < 0)
            {
                _pauseTimerCount = 0;
            }
        }
        
        if(_pauseTimerCount > 0)
        {
            _isPauseTimeout = true;
        }
        else
        {
            _isPauseTimeout = false;
        }
        
        //Debug.Log(string.Format("Pause Timer({0}) : {1}" , _pauseTimerCount, _isPauseTimeout.ToString()));
    }
    
    public void AddTurnTime()
    {
        AddTurnTime(PlayTimePerAction);
    }
    
    public void AddTurnTime(float second)
    {
        if(IsHaveTimeout)
        {
            if(IsEnableTimeout && !_isTimeout)
            {
                //Debug.Log("AddTurnTime : " + second);
                SetRemainTurnTime(TurnTimer + second);
            }
        }
    }   
    
    private void OnTurnTimeout()
    {
        if(!_isTimeout)
        {
            // time out.
            _isTimeout = true;
            
            if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
            {
                if(KOSNetwork.GamePlayer.localPlayer.IsMyTurn)
                {
                    KOSNetwork.GamePlayer.localPlayer.RequestSetTimeout(true);
                }
            }
        }
    }
    
    public void SetTimeout(bool isTimeout)
    {
        // time out.
        _isTimeout = isTimeout;
        if(_isTimeout)
        {
            _turnTimer = 0.0f;
        }
    }
    
    public void SetRemainTurnTime(float remainTime)
    {
        _turnTimer = remainTime;
        _turnTimer = Mathf.Min(MaxPlayTime, _turnTimer);
    }
    
    public float PlayTimePerTurn { get { return BattleController.Instance.PlayTimePerTurn; } }
    public float PlayTimePerAction { get { return BattleController.Instance.PlayTimePerAction; } }
    public float MaxPlayTime { get { return BattleController.Instance.MaxPlayTime; } }
    public float PopupDecisionTime { get { return BattleController.Instance.PopupDecisionTime; } }
#endregion

    public void OnPlayerDisconnect()
    {   
        _isEndGame = true;
        
        if(KOSNetwork.NetworkSetting.IsNetworkEnabled)
        {
            _isForceWin = true;     
            _gameWinPlayerIndex = KOSNetwork.GamePlayer.localPlayer.playerIndex;
        }
    }
}