﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using GamePlayerUI;

public interface IBattleManagerController
{
    #region BattleUIManager Methods
    void ShowSelectRPSUI(RPSSelectUI.OnClickRPSOption onSelectRPS);
    void ShowResultRPSUI(RPSType selectedRPS_P1, RPSType selectedRPS_P2, RPSResultUI.OnFinishShowResult onFinishShowCallback);

    void ShowFindFirstPlayUI(bool isActivable, PlayOrderSelectUI.OnClickPlayOrderOption onSelectedFindFirstPlayCallback);    
    #endregion
}


public class BattleController : MonoBehaviour, IBattleManagerController
{
    #region Private Properties
    private BattleManager _battleManager;
    private UnityEvent OnUpdateEvent;
    private bool _IsUpdate = false; // use for control update loop
    private float _timer = 0.0f;
    private bool _isStartTimer = false;

    //private UnityEvent OnStartEvent;
    #endregion

    #region Public Properties
	public BattleUIManager BattleUIManager;

	public BattleManager BattleManager
	{
		get { return _battleManager; }
	}

	public AudioClip ButtonSound;
	public AudioClip DrawSound;
	public AudioClip SelectSound;
	public AudioClip MoveSound;
	public AudioClip AttackSound;
	public AudioClip DestroySound;
	public AudioClip ToHandSound;
	public AudioClip RemoveSound;
	public AudioClip AbilitySound;
	public AudioClip SummonSound;
	public AudioClip ShuffleSound;
	public AudioClip GoldSound;

	public AudioClip BattleMusic;
	public AudioClip WinMusic;
	public AudioClip LoseMusic;

    public int InitDrawCard = 5;
    public float PlayTimePerTurn = 50.0f;
    public float PlayTimePerAction = 5.0f;
    public float PopupDecisionTime = 15.0f;
    public float MaxPlayTime = 75.0f;
    public float WaitPlayerTime = 60.0f;
	public bool IsTestTutorial = true;

	private static BattleController _instance = null;
	public static BattleController Instance
	{
		get
		{
			return _instance;
		}
	}
    #endregion

	#region Awake
	void Awake()
	{
		#if UNITY_EDITOR
		if (!LocalizationManager.IsLoad)
		{
			LocalizationManager.Init ();
		}
		#endif
              
        DeckData.ResetDeckIndex();
        UniqueCardData.ResetUniqueCardIndex();
        BattleCardData.ResetBattleCardIndex();

		_battleManager = new BattleManager(this, this.BattleUIManager); 

		BattleController._instance = this;
		SoundManager.PlayBGM(BattleMusic);

		_battleManager.OnStart();
	}
    #endregion

    #region Start
    // Use this for initialization
    void Start()
    {
        if (KOSNetwork.NetworkSetting.IsNetworkEnabled)
        {
            _timer = 0.0f;
            StartCoroutine("WaitPlayers");
        }
        else
        {
            _IsUpdate = true;
        }
    }
    #endregion

    #region Update
    // Update is called once per frame
	void Update () 
	{
        if(_isStartTimer)
        {
            _timer += Time.deltaTime;
        }
    
        if(_IsUpdate)
            _battleManager.OnUpdate();
	}
    #endregion

    #region Methods
    public void SubscribeUpdateEvent(UnityAction action)
    {
        OnUpdateEvent.AddListener(action);
    }

    public void UnsubscribeUpdateEvent(UnityAction action)
    {
        OnUpdateEvent.RemoveListener(action);
    }
    
    public void OnEndPhaseClick()
	{
		_battleManager.LocalRequestNextPhase();
	}
    
    public void OnPlayerDisconnect()
    {           
        if(!_IsUpdate)
        {
            BattleManager.Instance.GoToLobby();
        }
    }

    public void PrintDebug(string debugText)
    {
        #if UNITY_EDITOR
            print(debugText);
        #endif
    }
    #endregion

    #region WaitPlayers
    IEnumerator WaitPlayers()
    {
        while(true)
        {
            if(!KOSNetwork.NetworkSetting.IsNetworkEnabled)
            {
                break;
            }
            
            _isStartTimer = true;
        
            KOSNetwork.GamePlayer[] players = GameObject.FindObjectsOfType<KOSNetwork.GamePlayer>();
            if(players.GetLength(0) >= 2)
            {
                int bIsReadyCount = 0;
                foreach (KOSNetwork.GamePlayer player in players)
                {
                    if (player.IsReadyStartGame)
                    {
                        bIsReadyCount++;
                    }
                }
                if(bIsReadyCount == 2)
                {
                    break;
                }
            }
            
            if(_timer < WaitPlayerTime)
            {
                Debug.Log("Waiting for players...");
            }
            else
            {
                Debug.Log("Waiting time out.");
                
                _isStartTimer = false;
                // reject this matching.
                _battleManager.ShowPopupMessageUI(
                      "Waiting timeout."
                    , "Lobby"
                    , _battleManager.GoToLobby
                    , false                   
                );
                
                yield return null;
            }
            yield return new WaitForSeconds(1.0f);
        }
        
        Debug.Log("Players ready");
        _IsUpdate = true;
        KOSLobbyManager.Instance.SetLobbyScene("");
        KOSLobbyManager.Instance.gameObject.name = "KOSLobbyManager_Used";
    }
    #endregion

    #region IBattleManagerController Methods
    public void ShowSelectRPSUI(RPSSelectUI.OnClickRPSOption onSelectRPS)
    {
        this.BattleUIManager.ShowSelectRPSUI(onSelectRPS);
    }

    public void ShowResultRPSUI(RPSType selectedRPS_P1, RPSType selectedRPS_P2, RPSResultUI.OnFinishShowResult onFinishShowCallback)
    {
        this.BattleUIManager.ShowResultRPSUI(
              selectedRPS_P1
            , selectedRPS_P2
            , onFinishShowCallback
        );
    }

    public void ShowFindFirstPlayUI(bool isActivable, PlayOrderSelectUI.OnClickPlayOrderOption onSelectedFindFirstPlayCallback)
	{
		this.BattleUIManager.ShowPlayOrderSelectUI(
			  isActivable
			, onSelectedFindFirstPlayCallback
		);
	}

	public void LoadScene(string sceneName)
	{
		StartCoroutine(LoadingScene(sceneName));
	}

	IEnumerator LoadingScene(string sceneName)
	{
		this.BattleUIManager.ShowLoading(true);
		yield return new WaitForSeconds(0.1f);
		SceneManager.LoadSceneAsync(sceneName);
	}
    #endregion
}
