﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#region BattleEvent Trigger
public class BattleEventTrigger
{
	public delegate void BattleTriggerEvent();

	public enum BattleSide : int
	{
		  Friendly	= 1
		, Enemy		= 2
	}

	public enum BattleTriggerType : int
	{
		// Player = 1
		  BEGIN_OF_PHASE
		, END_OF_PHASE
		, PLAYER_DRAWED_CARD
		, PLAYER_DISCARDED

		, PLAYER_CARD_TO_HAND
		, PLAYER_CARD_TO_DECK
		, PLAYER_CARD_TO_GRAVEYARD
		, PLAYER_CARD_TO_REMOVE_ZONE

		// Base = 2
		, BASE_BE_TARGETED
		, BASE_BE_DAMAGED

		// Card = 3
		, CARD_SUMMONING
		, CARD_BE_SUMMONED

		, CARD_MOVING
		, CARD_BE_MOVED

		, CARD_ATTACK_SUCCESSED
		, CARD_ATTACK_BASE_SUCCESSED
		, CARD_BE_TAGETED
		, CARD_BE_DAMAGED
		, CARD_BE_DESTROYED
		, CARD_LEAVED

		, CARD_TO_HAND
		, CARD_TO_DECK
		, CARD_TO_GRAVEYARD
		, CARD_TO_REMOVE_ZONE
	}

	private Dictionary<string, BattleTriggerEvent> _battleCardEventTriggerList;

	public BattleEventTrigger()
	{
		Init();
	}

	public void Init()
	{
		_battleCardEventTriggerList = new Dictionary<string, BattleTriggerEvent>();
	}

	public void BindBattleTrigger(BattleTriggerEvent triggerCallback, BattleTriggerType battleTriggerType, BattleSide battleSide)
	{
		string key = battleTriggerType.ToString() + "_" + battleSide.ToString();

		bool isFound = _battleCardEventTriggerList.ContainsKey(key);
		if(isFound)
		{
			// Add new trigger.
			_battleCardEventTriggerList[key] += triggerCallback;
		}
		else
		{
			// Create new trigger.
			BattleTriggerEvent triggerEvent = triggerCallback;
			_battleCardEventTriggerList.Add(key, triggerEvent); 
		}
	}

	public void BindBattleTrigger(BattleTriggerEvent triggerCallback, BattleTriggerType battleTriggerType, int battleCardID)
	{

		string key = battleTriggerType.ToString() + "_" + battleCardID.ToString();

		bool isFound = _battleCardEventTriggerList.ContainsKey(key);
		if(isFound)
		{
			// Add new trigger.
			_battleCardEventTriggerList[key] += triggerCallback;
		}
		else
		{
			// Create new trigger.
			BattleTriggerEvent triggerEvent = triggerCallback;
			_battleCardEventTriggerList.Add(key, triggerEvent); 
		}
	}

	public bool UnbindBattleTrigger(BattleTriggerEvent triggerCallback, BattleTriggerType battleTriggerType, BattleSide battleSide)
	{
		string key = battleTriggerType.ToString() + "_" + battleSide.ToString();

		bool isFound = _battleCardEventTriggerList.ContainsKey(key);
		if(isFound)
		{
			// Remove trigger.
			_battleCardEventTriggerList[key] -= triggerCallback;
			return true;
		}

		return false;
	}

	public bool UnbindBattleTrigger(BattleTriggerEvent triggerCallback, BattleTriggerType battleTriggerType, int battleCardID)
	{
		string key = battleTriggerType.ToString() + "_" + battleCardID.ToString();

		bool isFound = _battleCardEventTriggerList.ContainsKey(key);
		if(isFound)
		{
			// Remove trigger.
			_battleCardEventTriggerList[key] -= triggerCallback;
			return true;
		}

		return false;
	}

	public void OnBattleEventTrigger(BattleTriggerType battleTriggerType, BattleSide battleSide)
	{
		//Debug.Log("OnBattleEventTrigger:" + battleTriggerType.ToString() + ":" + battleSide.ToString());

		string key = battleTriggerType.ToString() + "_" + battleSide.ToString();

		bool isFound = _battleCardEventTriggerList.ContainsKey(key);
		if(isFound)
		{
			if(_battleCardEventTriggerList[key] != null)
			{
				// Excecute trigger.
				_battleCardEventTriggerList[key].Invoke();
			}
		}
	}

	public void OnBattleEventTrigger(BattleTriggerType battleTriggerType, int battleCardID)
	{
		string key = battleTriggerType.ToString() + "_" + battleCardID.ToString();

		bool isFound = _battleCardEventTriggerList.ContainsKey(key);
		if(isFound)
		{
			if(_battleCardEventTriggerList[key] != null)
			{
				// Excecute trigger.
				_battleCardEventTriggerList[key].Invoke();
			}
		}
	}
}
#endregion
