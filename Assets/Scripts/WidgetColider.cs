﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(UIWidget), typeof(BoxCollider))]
public class WidgetColider : MonoBehaviour
{
	Vector3 _size;
	Vector3 _center;
	UIWidget.Pivot _pivot;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		UpdateColider();
	}

	void OnEnable()
	{
		UpdateColider();
	}

	void UpdateColider()
	{
		UIWidget widget = GetComponent<UIWidget>();
		if(widget.width != _size.x || widget.height != _size.y || widget.pivot != _pivot)
		{
			Vector3 size = Vector3.zero;
			size.Set(widget.width, widget.height, 1.0f);

			Vector3 offset = Vector3.zero;

			switch(widget.pivot)
			{
				case UIWidget.Pivot.TopLeft:
				{
					offset = new Vector3((widget.width * 0.5f), -(widget.height * 0.5f), 0.0f);
				}
				break;

				case UIWidget.Pivot.Top:
				{
					offset = new Vector3(0.0f, -(widget.height * 0.5f), 0.0f);
				}
				break;

				case UIWidget.Pivot.TopRight:
				{
					offset = new Vector3(-(widget.width * 0.5f), -(widget.height * 0.5f), 0.0f);
				}
				break;

				case UIWidget.Pivot.Left:
				{
					offset = new Vector3((widget.width * 0.5f), 0.0f, 0.0f);
				}
				break;

				case UIWidget.Pivot.Center:
				{
					offset = Vector3.zero;
				}
				break;
			
				case UIWidget.Pivot.Right:
				{
					offset = new Vector3(-(widget.width * 0.5f), 0.0f, 0.0f);
				}
				break;

				case UIWidget.Pivot.BottomLeft:
				{
					offset = new Vector3((widget.width * 0.5f), (widget.height * 0.5f), 0.0f);
				}
				break;

				case UIWidget.Pivot.Bottom:
				{
					offset = new Vector3(0.0f, (widget.height * 0.5f), 0.0f);
				}
				break;

				case UIWidget.Pivot.BottomRight:
				{
					offset = new Vector3(-(widget.width * 0.5f), (widget.height * 0.5f), 0.0f);
				}
				break;

				default:
				{
					offset = Vector3.zero;
				}
				break;
			}

			BoxCollider boxColider = GetComponent<BoxCollider>();

			boxColider.size = size;
			boxColider.center = offset;

			_pivot = widget.pivot;
			_size = size;
			_center = offset;
		}
	}
}
