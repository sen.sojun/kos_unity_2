﻿using UnityEngine;
using System.Collections;

public class PassiveIconUI : MonoBehaviour 
{
	public UIGrid grid;
	public GameObject CardIconPrefab;

	private string _airSpriteName = "p-air";
	private string _knockSpriteName = "p-knock";
	private string _pierceSpriteName = "p-pierce";
	private string _sentrySpriteName = "p-sentry";

	/*
	// Use this for initialization
	void Start () {
	
	}
	*/

	/*
	// Update is called once per frame
	void Update () {
	
	}
	*/

	public void Clear()
	{
		grid.transform.DestroyChildren();
	}

	public void SetPassiveIndex(PassiveIndex passiveIndex)
	{
		Clear();

		if(passiveIndex == PassiveIndex.PassiveType.AirUnit)
		{
			CreateIcon(PassiveIndex.PassiveType.AirUnit);
		}

		if(passiveIndex == PassiveIndex.PassiveType.Knockback)
		{
			CreateIcon(PassiveIndex.PassiveType.Knockback);
		}

		if(passiveIndex == PassiveIndex.PassiveType.Piercing)
		{
			CreateIcon(PassiveIndex.PassiveType.Piercing);
		}
	}

	private bool CreateIcon(PassiveIndex.PassiveType passiveType)
	{
		string spriteName = "";
		switch(passiveType)
		{
			case PassiveIndex.PassiveType.AirUnit: 
			{
				spriteName = _airSpriteName; 
			}
			break;

			case PassiveIndex.PassiveType.Knockback: 
			{
				spriteName = _knockSpriteName;
			}
			break;

			case PassiveIndex.PassiveType.Piercing: 
			{
				spriteName = _pierceSpriteName;
			} 
			break;
			
			default: 
				return false;
		}

		GameObject obj = Instantiate(CardIconPrefab) as GameObject;
		obj.GetComponent<UISprite>().spriteName = spriteName;

		obj.transform.parent = grid.transform;
		obj.transform.localScale = Vector3.one;

		BattleUIManager.RequestBringForward(obj, true);

		grid.Reposition();

		return true;
	}
}
