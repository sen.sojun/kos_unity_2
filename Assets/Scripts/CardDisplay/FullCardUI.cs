﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

[RequireComponent(typeof(DepthManager))]
public class FullCardUI : MonoBehaviour
{
	public enum CardUISize
	{
		  S
		, M
		, L
	}

	public delegate void OnClickEvent (FullCardUI fullcardUI);

	#region Private Properties
	protected PlayerCardData _myPlayerCardData = null;
	protected bool _isNoStat = false;

	private OnClickEvent _onClickCallback = null;
	#endregion

	#region Public Properties
	public CardUISize Size;

	public UISprite BGSprite;
	public UILabel CostLabel;
	public UILabel NameLabel;

	public CardSymbolUI SymbolIcon;

	public UITexture CardTexture;
	public UILabel TypeLabel;

	public GameObject NoStatUIGroup;
	public UILabel NoStatDescriptionLabel;

	public GameObject StatUIGroup;
	public UILabel StatDescriptionLabel;

	public UILabel ShieldLabel;
	public UILabel HitPointLabel;
	public UILabel AttackLabel;
	public UILabel SpeedLabel;
	public UILabel ArtistLabel;

	public PlayerCardData PlayerCardData
	{
		get { return _myPlayerCardData; }
	}
	#endregion

	#region Start
	// Use this for initialization
	void Start()
	{
	}
	#endregion

	#region Update
	// Update is called once per frame
	void Update()
	{
	}
	#endregion

	#region Methods
	public virtual void SetCardData(PlayerCardData data)
	{
		_myPlayerCardData = new PlayerCardData(data);
		//Debug.Log(_myPlayerCardData.TypeData.CardTypeID);

        if (   string.Compare(_myPlayerCardData.TypeData.CardTypeID, CardTypeData.EnumCardTypeToID(CardTypeData.CardType.Unit)) == 0 // Unit
			|| string.Compare(_myPlayerCardData.TypeData.CardTypeID, CardTypeData.EnumCardTypeToID(CardTypeData.CardType.Structure)) == 0 // Structure
            || string.Compare(_myPlayerCardData.TypeData.CardTypeID, CardTypeData.EnumCardTypeToID(CardTypeData.CardType.Base)) == 0 // Base
		) 
		{
			_isNoStat = false;
		}
		else
		{
			_isNoStat = true;
		}

		DisplayData();
	}

    private void DisplayData()
    {
        SetCardBG();

        CostLabel.text = _myPlayerCardData.Cost.ToString();

        SetNameLabel();

        SymbolIcon.SetSymbolData(_myPlayerCardData.SymbolData);

        SetImageTexture();

        SetTypeLabel();

        SetDescriptionStat();

        switch (LocalizationManager.CurrentLanguage)
        {
            case LocalizationManager.Language.Thai:
            {
                ArtistLabel.text = _myPlayerCardData.ImageData.ArtistName_TH;
            }
            break;

            case LocalizationManager.Language.English:
            default:
            {
                ArtistLabel.text = _myPlayerCardData.ImageData.ArtistName_EN;
            }
            break;
        }
	}

	private void SetCardBG()
	{
		string fullArtSuffix = "";
        if(PlayerCardData.IsCardSubType(CardSubTypeData.CardSubType.Unique))
        {
            fullArtSuffix = "F"; // Full Art
        }
        
        string spellSuffix = "";
        if(_isNoStat)
        {
            spellSuffix = "S"; // Spell
        }
        
        string filePath = string.Format("{0}{1}{2}_{3}"
            , PlayerCardData.ClanData.CardClanID
            , fullArtSuffix
            , spellSuffix
            , Size.ToString().ToUpper()
        );

		//Debug.Log(bgSpriteName);
		BGSprite.spriteName = filePath;
	}

    private void SetNameLabel()
    {
        string name = "";
        switch (LocalizationManager.CurrentLanguage)
        {
            case LocalizationManager.Language.Thai:
            {
                name = _myPlayerCardData.Name_TH;
                if (_myPlayerCardData.Alias_TH.Length > 0)
                {
                     name += ", " + _myPlayerCardData.Alias_TH;
                }
            }
            break;

            case LocalizationManager.Language.English:
            default:
            {
                name = _myPlayerCardData.Name_EN;
                if (_myPlayerCardData.Alias_EN.Length > 0)
                {
                    name += ", " + _myPlayerCardData.Alias_EN;
                }
            }
            break;
        }

        NameLabel.text = name;
    }

	private void SetImageTexture()
	{
        CardTextureDB.TextureSize textureSize = CardTextureDB.TextureSize.S;
        switch (Size)
        {
            case CardUISize.S:
                {
                    textureSize = CardTextureDB.TextureSize.S;
                }
                break;

                case CardUISize.M:
                {
                    textureSize = CardTextureDB.TextureSize.M;
                }
                break;
 
                case CardUISize.L:
                {
                    textureSize = CardTextureDB.TextureSize.L;
                }
                break;
        }
			
		Texture imageTexture;

		bool isSuccess = CardTextureDB.GetTexture(
			  _myPlayerCardData.ImageData.CardImageID
            , textureSize
			, out imageTexture
		);
		if (isSuccess) 
		{
			//Debug.Log(imageTexture.name);
			CardTexture.mainTexture = imageTexture;
			Resources.UnloadUnusedAssets();
		} 

		// Self Load Image.
		/*
		string fileName = "Images/CardImage/" + _myPlayerCardData.ImageData.CardImageID + "_" + textureSize.ToString();
		Debug.Log ("ImagePath: " + fileName);
		Texture imageTexture = Resources.Load<Texture>(fileName);

		if (imageTexture != null) 
		{
			//Debug.Log(imageTexture.name);
			CardTexture.mainTexture = imageTexture;
			Resources.UnloadUnusedAssets();
		} 
		else 
		{
			Debug.LogError ("Not found image.");
		}
		*/
	}

    private void SetTypeLabel()
    {
        string typeName = "";
        switch (LocalizationManager.CurrentLanguage)
        {
            case LocalizationManager.Language.Thai:
            {
                typeName = _myPlayerCardData.TypeData.TypeName_TH;
            }
            break;

            case LocalizationManager.Language.English:
            default:
            {
                typeName = _myPlayerCardData.TypeData.TypeName_EN;
            }
            break;
        }

        TypeLabel.text = typeName;

        string typeSubfixText = "";
        if (_myPlayerCardData.SubTypeDataList.Count > 0)
        {
            foreach (CardSubTypeData subTypeData in _myPlayerCardData.SubTypeDataList)
            {
                if (string.Compare(subTypeData.CardSubTypeID, "ST0000") != 0) // not "None" subType
                {
                    string subTypeName = "";
                    switch (LocalizationManager.CurrentLanguage)
                    {
                        case LocalizationManager.Language.Thai:
                        {
                            subTypeName = subTypeData.SubTypeName_TH;
                        }
                        break;

                        case LocalizationManager.Language.English:
                        default:
                        {
                            subTypeName = subTypeData.SubTypeName_EN;
                        }
                        break;
                    }

                    typeSubfixText += " " + subTypeName;
                }
            }
        }

        if (_myPlayerCardData.JobDataList.Count > 0)
        {
            foreach (CardJobData jobData in _myPlayerCardData.JobDataList)
            {
                if (string.Compare(jobData.CardJobID, "J0000") != 0) // not "None" job
                {
                    string jobName = "";
                    switch (LocalizationManager.CurrentLanguage)
                    {
                        case LocalizationManager.Language.Thai:
                        {
                            jobName = jobData.JobName_TH;
                        }
                        break;

                        case LocalizationManager.Language.English:
                        default:
                        {
                            jobName = jobData.JobName_EN;
                        }
                        break;
                    }

                    typeSubfixText += " " + jobName;
                }
            }
        }

        // if have subfix text
        if (typeSubfixText.Length > 0)
        {
            TypeLabel.text += " -" + typeSubfixText;
        }
    }

	private void SetDescriptionStat()
	{
        string description = "";
        switch (LocalizationManager.CurrentLanguage)
        {
            case LocalizationManager.Language.Thai:
            {
                description = _myPlayerCardData.DescriptionText_TH;
            }
            break;

            case LocalizationManager.Language.English:
            default:
            {
                description = _myPlayerCardData.DescriptionText_EN;
            }
            break;
        }
        
        /*
        Auto: You receive 10G. <color=#404040><i>(This ability triggers at the beginning of your turn)</i></color> Air units you control get an additional move. 
        */
        
        description = description.Replace("<color=#", "[");
        description = description.Replace("</color>", "[-]");
        description = description.Replace("<i>", "[i]");
        description = description.Replace("</i>", "[/i]");
        description = description.Replace(">", "]");

        if (_isNoStat)
		{
			// Not Show Stat
			StatUIGroup.SetActive(false);
			NoStatUIGroup.SetActive(true);

			//description = description.Replace ("\\n", "\n");
			NoStatDescriptionLabel.text = description;
		}
		else
		{
			// Show Stat
			StatUIGroup.SetActive(true);
			NoStatUIGroup.SetActive(false);

			//description = description.Replace ("\\n", "\n");
			StatDescriptionLabel.text = description;

			ShieldLabel.text = _myPlayerCardData.Shield.ToString();
			HitPointLabel.text = _myPlayerCardData.HitPoint.ToString();
			AttackLabel.text = _myPlayerCardData.Attack.ToString();
			SpeedLabel.text = _myPlayerCardData.Speed.ToString();
		}
	}

	public void BindOnClickCallback(OnClickEvent callback)
	{
		_onClickCallback += callback;
	}

	public void UnbindOnClickCallback(OnClickEvent callback)
	{
		_onClickCallback -= callback;
	}

	public void RemoveAllOnClickCallback()
	{
		_onClickCallback = null;
	}

	public virtual void OnClick()
	{
		//Debug.Log("FullCardUI: OnClick");

		if(_onClickCallback != null)
		{
			_onClickCallback.Invoke(this);
		}
	}

	public void RequestBringForward(bool isIncludeParentPanel = true)
	{
		DepthManager depthManager = GetComponent<DepthManager>();

		depthManager.enabled = false;

		BattleUIManager.RequestBringForward(gameObject, isIncludeParentPanel);

		//NoStatUIGroup.SetActive(true);
		BattleUIManager.RequestBringForward(NoStatUIGroup, isIncludeParentPanel);

		//StatUIGroup.SetActive(true);
		BattleUIManager.RequestBringForward(StatUIGroup, isIncludeParentPanel);

		depthManager.ReinitDepth();

		depthManager.enabled = true;
	}

	public void ReinitDepth()
	{
		DepthManager depthManager = GetComponent<DepthManager>();

		depthManager.enabled = false;

		depthManager.ReinitDepth();

		depthManager.enabled = true;
	}
	#endregion
}
