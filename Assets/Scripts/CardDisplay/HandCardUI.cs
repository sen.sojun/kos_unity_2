﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HandCardUI : FullCardUI
{
	public delegate void OnShowPreview(PlayerCardData playerCardData);
	public delegate void OnHidePreview();
    public delegate void OnClickHandCard(HandCardUI clickedHandUI);
	public delegate void OnHoldHandCard(HandCardUI clickedHandUI);

	#region Private Properties
	private int _uniqueCardID = -1;
	private UniqueCardData _data = null;
	private float _pressTimer;
	private bool _isPressed = false;
	private bool _isHold = false;
	private bool _isSelect = false;
	private bool _isClickable = true;
	private HandUIManager _handUIManager = null;

	private OnShowPreview _onShowPreview;
	private OnHidePreview _onHidePreview;

    private OnClickHandCard _onClickHandCard;
	#endregion

	#region Public Properties
	public float HoldTime = 0.5f;
	public GameObject CanUseGlow;
	public GameObject DrimOverlay;
	public GameObject Arrow;

	public int UniqueCardID
    {
		get { return _uniqueCardID; }
    }
	public bool IsClickable
	{
		set { _isClickable = value; }
		get { return _isClickable; }
	}
	public bool IsSelect
	{
		set 
		{ 
			_isSelect = value; 
			SetSelector();
		}
		get { return _isSelect; }
	}
	#endregion

	#region Awake
	void Awake()
	{
		/*
		DepthManager depthManager = GetComponent<DepthManager>();
		depthManager.enabled = false;
		*/

		if(BattleManager.Instance != null && !BattleManager.Instance.IsTutorial)
		{
			Destroy(Arrow);
			Arrow = null;

			this.ReinitDepth();
		}
		else
		{
			this.ReinitDepth();
			ShowArrow(false);
		}
	}
	#endregion

	#region Start
    // Use this for initialization
    void Start()
    {
		/*
		SetGlow(false);
		SetDrimOverlay(false);
		*/

		ShowArrow(false);
    }
	#endregion

	#region Update
    // Update is called once per frame
    void Update()
    {
		if(_isPressed)
		{
			_pressTimer += Time.deltaTime;
			if(_pressTimer >= HoldTime)
			{
				_isPressed = false;
				OnStartHold();
			}
		}

		//UpdateCanUse();
    }

	public void UpdateCanUse()
	{
		SetGlow(IsCanActiveHandCard());
		SetDrimOverlay(!IsCanActiveHandCard());
        
        //GetComponent<DepthManager>().ResetDepth();
	}

	private bool IsCanActiveHandCard()
	{
		if(_data != null && BattleManager.Instance != null)
		{
			if(BattleManager.Instance.IsCanUseGold(_data.OwnerPlayerIndex, _data.PlayerCardData.Cost))
			{
				if( (  (_data.PlayerCardData.TypeData == CardTypeData.CardType.Base)
					|| (_data.PlayerCardData.TypeData == CardTypeData.CardType.Structure)
					|| (_data.PlayerCardData.TypeData == CardTypeData.CardType.Unit))
				)
				{
					/*if(
						(BattleManager.ToDisplayBattlePhase(BattleManager.Instance.CurrentBattlePhase) == DisplayBattlePhase.PreBattle)
						|| (BattleManager.ToDisplayBattlePhase(BattleManager.Instance.CurrentBattlePhase) == DisplayBattlePhase.PostBattle)
					)*/
					{
						return true;
					}
				}

				if(_data.PlayerCardData.TypeData == CardTypeData.CardType.Event)
				{
					if(BattleManager.Instance.IsCanUseHandCard(_data.UniqueCardID))
					{
						/*if(    (BattleManager.ToDisplayBattlePhase(BattleManager.Instance.CurrentBattlePhase) == DisplayBattlePhase.PreBattle)
							|| (BattleManager.ToDisplayBattlePhase(BattleManager.Instance.CurrentBattlePhase) == DisplayBattlePhase.PostBattle)
						)*/
						{
							return true;
						}

						/*else if(BattleManager.ToDisplayBattlePhase(BattleManager.Instance.CurrentBattlePhase) == DisplayBattlePhase.Battle)
						{
							foreach(CardSubTypeData subType in _data.PlayerCardData.SubTypeDataList)
							{
								if(subType == CardSubTypeData.CardSubType.Sudden)
								{
									return true;
									break;
								}
							}
						}*/
					}
				}

				if(_data.PlayerCardData.TypeData == CardTypeData.CardType.Technic)
				{
					if(BattleManager.Instance.IsCanUseHandCard(_data.UniqueCardID))
					{
						return true;
					}
					//actionList.Add(CardPopupMenuUI.CardPopupActionType.Facedown);
				}

				if(_data.PlayerCardData.TypeData == CardTypeData.CardType.Item)
				{
					return true;
				}
			}
		}
		return false;
	}
	#endregion

	#region Methods
	public void SetCardData(UniqueCardData data)
    {
		_data = data;
		_uniqueCardID = data.UniqueCardID;

		this.SetCardData(data.PlayerCardData);
		this.IsSelect = false;
    }

	public void SetHandUIManager(HandUIManager handUIManager)
	{
		_handUIManager = handUIManager;
	}

    public void SetOnClickCallback(OnClickHandCard onClickHandCardCallback)
    {
        _onClickHandCard = onClickHandCardCallback;
    }
	public void SetShowCardPreviewCallback(OnShowPreview onShowPreviewCallback, OnHidePreview onHidePreviewCallback)
	{
		_onShowPreview = onShowPreviewCallback;
		_onHidePreview = onHidePreviewCallback;
	}
		
	private void SetSelector()
	{
	}

	private void SetGlow(bool isShow)
	{
		if(CanUseGlow.activeSelf != isShow)
		{
			CanUseGlow.SetActive(isShow);
			if(isShow)
			{
				BattleUIManager.RequestBringForward(CanUseGlow);
			}
		}
	}

	private void SetDrimOverlay(bool isShow)
	{
		if(DrimOverlay.activeSelf != isShow)
		{
			DrimOverlay.SetActive(isShow);
			if(isShow)
			{
				BattleUIManager.RequestBringForward(DrimOverlay);
			}
		}
	}

	public void ShowArrow(bool isShow)
	{
		//Debug.Log("HandCard| ShowArrow");

		if(Arrow == null) return;

		if(Arrow.activeSelf != isShow)
		{
			Arrow.SetActive(isShow);

			if(isShow)
			{
				BattleUIManager.RequestBringForward(Arrow);
			}
		}
	}
		
	public override void OnClick()
	{
		/*
		if(!IsClickable || _isHold) return;

        if (_onClickHandCard != null)
        {
            _onClickHandCard.Invoke(this);
        }
        */
	}

	private void OnClickHandUI()
	{
		if(!IsClickable || _isHold) return;

		if (_onClickHandCard != null)
		{
			_onClickHandCard.Invoke(this);
		}
	}

	/*
	public void OnHover()
	{
		Debug.Log("Hover" + this.PlayerCardData.Name);
	}
	*/

	/*
	public void OnDrag()
	{
		Debug.Log("OnDrag: " + this.PlayerCardData.Name);
	}
	*/

	public void OnDragOver(GameObject obj)
	{
		//Debug.Log("OnDragOver: me:" + this.PlayerCardData.Name + " obj:" + obj.name);

		if(_handUIManager != null)
		{
			if (_onShowPreview != null && _handUIManager.IsShowPreview)
			{
				_onShowPreview.Invoke(this.PlayerCardData);
			}
		}
	}

	public void OnPress(bool isDown)
	{
		if(!IsClickable) return;

		if(isDown)
		{
			// Start Press

			_pressTimer = 0.0f;
			_isPressed = true;
		}
		else
		{
			// Stop Press

			_isPressed = false;

			if(_isHold)
			{
				OnStopHold();
			}
			else
			{
				// Start Click
				OnClickHandUI();
			}
		}
	}

	private void OnStartHold()
	{
		_isHold = true;
		//Debug.Log ("HandCard[" + _myPlayerCardData.CardID + "] OnHold!");

		if(_onShowPreview != null)
		{
			_onShowPreview.Invoke(this.PlayerCardData);
		}
	}

	private void OnHold()
	{
		
	}

	private void OnStopHold()
	{
		_isHold = false;
		//Debug.Log ("HandCard[" + _myPlayerCardData.CardID + "] StopHold!");

		if(_onHidePreview != null)
		{
			_onHidePreview.Invoke();
		}
	}
	#endregion
}
