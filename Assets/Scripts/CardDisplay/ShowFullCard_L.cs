﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShowFullCard_L : MonoBehaviour {

	public FullCardUI FullCard_L;
	public UILabel IndexLabel;
	public GameObject widgetShowCard;

	private List<CardData> _deck; //<LIST> -> array like ,with memory allocation
	private int cardIndex = 0;

	// Use this for initialization
	void Start () {
		_deck = new List<CardData>(); //create empty list

		List<CardDBData> rawDataList;
		bool isSuccess = CardDB.GatAllData(out rawDataList);
		if (isSuccess)
		{
			foreach (CardDBData rawData in rawDataList)
			{
				CardData cardData = new CardData(rawData.CardID);
				_deck.Add(cardData);
			}
		}
		else
		{
			Debug.LogError("CardDisplay/ShowFullCard: CardDB.GatAllData Failed!!");
		}

		ShowCardData(cardIndex);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void ShowNextCard()
	{
		//Debug.Log("Show Next Card");
		++cardIndex;

		if (cardIndex >= _deck.Count)
		{
			cardIndex = 0;
		}

		IndexLabel.text = (cardIndex + 1).ToString();

		ShowCardData(cardIndex);
	}

	public void ShowPrevCard()
	{
		//Debug.Log("Show Prev Card");
		--cardIndex;

		if (cardIndex < 0)
		{
			cardIndex = _deck.Count - 1;
		}

		IndexLabel.text = (cardIndex + 1).ToString();

		ShowCardData(cardIndex);
	}

	public void ShowCardData(int index)
	{
		if(index >= 0 && index < _deck.Count)
		{
			PlayerCardData playerCardData = new PlayerCardData(_deck[index]);
			FullCard_L.SetCardData(playerCardData);
		}
	}

	public void CloseScreen()
	{
		widgetShowCard.SetActive (false);
	}
}
