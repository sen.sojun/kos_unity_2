﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(UISprite))]
public class CardSymbolUI : MonoBehaviour 
{
    public enum CardSymbolSize
    {
          S
        , M
        , L
    }

    private CardSymbolData _mySymbolData;
    public CardSymbolSize SymbolSize;
	public UIAtlas SymbolAtlas;

	// Use this for initialization
	void Start () 
    {
	}
	
	// Update is called once per frame
	void Update () 
    {
	}

    public void SetSymbolData(CardSymbolData symbolData)
    {
        _mySymbolData = symbolData;
        DisplayData();
    }

    private void DisplayData()
    {
        //Debug.Log(_mySymbolData.SymbolImageFile);
		UISprite mySprite = this.GetComponent<UISprite>();
		mySprite.atlas = SymbolAtlas;
        mySprite.spriteName = _mySymbolData.SymbolImageFile + "_" + SymbolSize.ToString();
    }
}
