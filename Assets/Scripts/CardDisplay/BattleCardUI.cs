﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*
public interface IBattleCardUI
{
	int BattleCardID { get; }
	BattleCardData BattleCardData { get; }
	bool IsFaceDown { get; }
	FieldUI.FieldSide Side { get; }
	FieldUI CurrentField { get; }
	bool IsClickable { get; set; }
	bool IsSelect { get; set; }
	bool IsCanAddNewCard { get; }

	GameObject GameObject { get; }

	bool IsBattleID(int battleCardID);


	//bool IsRemoveAndDestroy(int battleCardID);
	void UpdateBattleCardData(BattleCardData battleCardData);
	void SetCurrentField(FieldUI currentField);
}
*/

public class BattleCardUI : MonoBehaviour//, IBattleCardUI
{
    #region Delegates
	public delegate void OnClickBattleCard(BattleCardUI clickedBattleCardUI, int showBattleCardID);
    #endregion

    #region Private Properties
	private string _playerCardID;
	private List<int> _battleCardIDList = null;
	private int _showIndex = -1;
	private BattleCardData __showingBattleCardData = null;
	private BattleCardData _showingBattleCardData
	{
		get 
		{ 
			return __showingBattleCardData; 
		}
		set 
		{ 
			__showingBattleCardData = value; 
			UpdateDisplayData();
		}
	}

	private bool _isFaceDown = false;
	private FieldUI.FieldSide _side;
    private FieldUI _currentField;
    private OnClickBattleCard _onClickBattleCard = null;
	private bool _isClickable = true;
	private bool _isSelect = false;
    #endregion

    #region Public Properties
	public GameObject FrontCardGroup;
	public UIPanel CardImagePanel;

	public GameObject CardCountGroup;
	public UILabel CardCountLabel;

    public UILabel NameLabel;
	public UITexture CardTexture;
	public UISprite GlowBG;
	public UISprite SelectorGlow;

	public ActionIcon ActionIcon;
	public PassiveIconUI PassiveIconUI;

    public UILabel ShieldLabel;
    public UILabel HitPointLabel;
    public UILabel AttackLabel;
    public UILabel SpeedLabel;

	public GameObject BackCardGroup;
	public GameObject TrimCardGroup;
	public GameObject TrimOverlay;
	public GameObject TargetMark;
	public GameObject ArrowPanel;

	public Color PlayerGlowColor;
	public Color EnemyGlowColor;

    public List<int> BattleCardIDList
    {
		get { return _battleCardIDList; }
    }
	public int CardCount
	{
		get 
		{ 
			if(_battleCardIDList != null)
			{
				return _battleCardIDList.Count; 
			}

			return 0;
		}
	}
	public int ShowBattleCardID
	{
		get { return _showingBattleCardData.BattleCardID; }
	}
	public BattleCardData ShowBattleCardData
	{
		get { return _showingBattleCardData; }
	}
    public bool IsFaceDown
    {
		get { return _isFaceDown; }
    }

    public FieldUI.FieldSide Side
    {
        get { return _side; }
    }
    public FieldUI CurrentField
    {
        get { return _currentField; }
    }
	public OnClickBattleCard OnClickBattleCardCallback { get { return _onClickBattleCard; } }
	public bool IsClickable
	{
		set 
		{ 
			_isClickable = value; 
			ShowTrim(!_isClickable);
		}
		get { return _isClickable; }
	}
	public bool IsSelect
	{
		set 
		{ 
			_isSelect = value; 
			SetSelector();
		}
		get { return _isSelect; }
	}
	public GameObject GameObject
	{
		get { return gameObject; }
	}
    #endregion

    #region Start
    // Use this for initialization
    void Start()
    {
		//NGUITools.NormalizePanelDepths();

		ShowSelector(false);
		ShowTrim(false);
    }
    #endregion

    #region Update
    // Update is called once per frame
	/*
	void Update () 
    {
	
	}
	*/
    #endregion

    #region Methods
	public bool IsHasBattleID(int battleCardID)
	{
		if(_battleCardIDList != null && _battleCardIDList.Count > 0)
		{
			foreach(int id in _battleCardIDList)
			{
				if(id == battleCardID) return true;
			}
		}

		return false;
	}

	public void SetInitData(BattleCardData battleCardData, FieldUI.FieldSide side, FieldUI currentField)
	{
		_battleCardIDList = new List<int>();
		_battleCardIDList.Add(battleCardData.BattleCardID);

		_isFaceDown = battleCardData.IsFaceDown;
		_side = side;
		_currentField = currentField;
		_showIndex = 0;

		_showingBattleCardData = new BattleCardData(battleCardData);
	}

	public void AddBattleCard(BattleCardData battleCardData, bool isShowThis = false)
	{
		//Debug.Log("AddBattleCard");

		_battleCardIDList.Add(battleCardData.BattleCardID);

		if(isShowThis)
		{
			ShowCardDetail(_battleCardIDList.Count - 1);
		}
		else
		{
			UpdateBattleCardData();

			//UpdateDisplayData();
		}
	}

	public bool RemoveBattleCard(int battleCardID)
	{
		if(_battleCardIDList != null && _battleCardIDList.Count > 0)
		{
			BattleCardData battleCardData;
			bool isFound = BattleManager.Instance.FindBattleCard(battleCardID, out battleCardData);
			if(isFound)
			{
				return RemoveBattleCard(battleCardData);
			}
		}

		return false;
	}

	public bool RemoveBattleCard(BattleCardData battleCardData)
	{
		if(_battleCardIDList != null && _battleCardIDList.Count > 0)
		{
			int removeIndex = -1;
			for(int index = 0; index < _battleCardIDList.Count; ++index)
			{
				if(battleCardData.BattleCardID == _battleCardIDList[index])
				{
					// found
					removeIndex = index;
					break;
				}
			}

			if(removeIndex >= 0 && removeIndex < _battleCardIDList.Count)
			{
				_battleCardIDList.RemoveAt(removeIndex);

				if(removeIndex == _showIndex)
				{
					ShowCardDetail(0);
				}

				return true;
			}
		}

		return false;
	}

	public void ShowCardDetail(int index = 0)
	{
		_showIndex = index;
		UpdateBattleCardData();
	}

	public void UpdateBattleCardData()
	{
		if(_battleCardIDList != null && _battleCardIDList.Count > 0)
		{
			BattleCardData battleCard;
			bool isFound = BattleManager.Instance.FindBattleCard(_battleCardIDList[_showIndex], out battleCard);
			if(isFound)
			{
				_showingBattleCardData = battleCard;
			}
		}
	}

    public void SetOnClickCallback(OnClickBattleCard onClickBattleCardCallback)
    {
        _onClickBattleCard = onClickBattleCardCallback;
    }

	public void SetSide(FieldUI.FieldSide side)
	{
		_side = side;
	}

    public void SetCurrentField(FieldUI currentField)
    {
        _currentField = currentField;
    }

	/*
	public void FlipCard()
	{
		if(_isFaceDown)
		{
			_isFaceDown = false;
			UpdateDisplayData();
		}
	}
	*/

    private void UpdateDisplayData()
    {
		if(_showingBattleCardData.IsFaceDown)
		{
			FrontCardGroup.SetActive(false);
			CardImagePanel.alpha = 0.0f;

			BackCardGroup.SetActive(true);
		}
		else
		{
			FrontCardGroup.SetActive(true);
			CardImagePanel.alpha = 1.0f;

			BackCardGroup.SetActive(false);

			SetNameLabel();
			SetImageTexture();

			// Shield
			if(_showingBattleCardData.CurrentShield > 0)
			{
				ShieldLabel.text = _showingBattleCardData.CurrentShield.ToString();
			}
			else
			{
				ShieldLabel.text = (0).ToString();
			}

			// HP
			if(_showingBattleCardData.CurrentHP > 0)
			{
				HitPointLabel.text = _showingBattleCardData.CurrentHP.ToString();
			}
			else
			{
				HitPointLabel.text = (0).ToString();
			}

			// Attack
			if(_showingBattleCardData.CurrentAttack > 0)
			{
				AttackLabel.text = _showingBattleCardData.CurrentAttack.ToString();
			}
			else
			{
				AttackLabel.text = (0).ToString();
			}

			// Speed
			if(_showingBattleCardData.CurrentSpeed > 0)
			{
				SpeedLabel.text = _showingBattleCardData.CurrentSpeed.ToString();
			}
			else
			{
				SpeedLabel.text = (0).ToString();
			}
		}

		if(_side == FieldUI.FieldSide.Player)
		{
			GlowBG.color = PlayerGlowColor;
		}
		else
		{
			GlowBG.color = EnemyGlowColor;
		}

		UpdateCardCount();
		UpdateCardIcon();
    }

	private void SetImageTexture()
	{
		Texture imageTexture;

		bool isSuccess = CardTextureDB.GetTexture(
			_showingBattleCardData.ImageData.CardImageID
			, CardTextureDB.TextureSize.S
			, out imageTexture
		);

		if(isSuccess)
		{
			//Debug.Log(imageTexture.name);
			CardTexture.mainTexture = imageTexture;
		}
	}
		
	private void UpdateCardIcon()
	{
		UpdateActionIcon();
		UpdatePassiveUI();
	}

	private void UpdateActionIcon()
	{
		//Debug.Log("UpdateActionIcon");

		if(BattleManager.Instance != null)
		{
			if(!_showingBattleCardData.IsFaceDown)
			{
				if(_showingBattleCardData.OwnerPlayerIndex == BattleManager.Instance.CurrentActivePlayerIndex)
				{
					// My Turn
					if(BattleManager.Instance.GetDisplayBattlePhase() == DisplayBattlePhase.Battle)
					{
						// At Battle Phase
						if(BattleManager.Instance.IsCanAttack(_showingBattleCardData.BattleCardID))
						{
							//Debug.Log("Attack");
							ActionIcon.ShowActionIcon(ActionIcon.ActionState.Attack);
							return;
						}
						else if(!_showingBattleCardData.IsActive)
						{
							ActionIcon.ShowActionIcon(ActionIcon.ActionState.Acted);
							return;
						}
					}
					else if(
						(BattleManager.Instance.GetDisplayBattlePhase() == DisplayBattlePhase.PreBattle) 
						|| (BattleManager.Instance.GetDisplayBattlePhase() == DisplayBattlePhase.PostBattle)
					)
					{
						// At Pre-Battle or Post-Battle.
						if(_showingBattleCardData.IsCanMove)
						{
							if(_showingBattleCardData.OwnerPlayerIndex == BattleManager.Instance.CurrentActivePlayerIndex)
							{
								//Debug.Log("Move");
								ActionIcon.ShowActionIcon(ActionIcon.ActionState.Move);
								return;
							}
						}
						else if(!_showingBattleCardData.IsActive)
						{
							//Debug.Log("Acted");
							ActionIcon.ShowActionIcon(ActionIcon.ActionState.Acted);
							return;
						}
					}
				}
				else
				{
					// Enemy Turn
					if(!_showingBattleCardData.IsActive)
					{
						ActionIcon.ShowActionIcon(ActionIcon.ActionState.Acted);
						return;
					}
				}
			}
		}

		//Debug.Log("None");
		ActionIcon.ShowActionIcon(ActionIcon.ActionState.None);
	}

	private void UpdatePassiveUI()
	{
		if(_showingBattleCardData.IsFaceDown)
		{
			PassiveIconUI.gameObject.SetActive(false);
		}
		else
		{
			PassiveIconUI.SetPassiveIndex(_showingBattleCardData.CurrentPassiveIndex);
			PassiveIconUI.gameObject.SetActive(true);
		}
	}

	private void SetNameLabel()
	{
        string name = "";
        switch (LocalizationManager.CurrentLanguage)
        {
            case LocalizationManager.Language.Thai:
            {
                name = _showingBattleCardData.Name_TH;
                if (_showingBattleCardData.Alias_TH.Length > 0)
                {
                    name += ", " + _showingBattleCardData.Alias_TH;
                }
            }
            break;

            case LocalizationManager.Language.English:
            default:
            {
                name = _showingBattleCardData.Name_EN;
                if (_showingBattleCardData.Alias_EN.Length > 0)
                {
                    name += ", " + _showingBattleCardData.Alias_EN;
                }
            }
            break;
        }

        NameLabel.text = name;
	}

	private void UpdateCardCount()
	{
		//Debug.Log("UpdateCardCount : " + _battleCardIDList.Count);
		if(_battleCardIDList.Count > 1)
		{
			// Grouping
			CardCountGroup.SetActive(true);
			CardCountLabel.text = "x" + _battleCardIDList.Count.ToString(); 

			BattleUIManager.RequestBringForward(CardCountGroup);
		}
		else
		{
			// Single
			CardCountGroup.SetActive(false);
		}
	}

	private void SetSelector()
	{
		/*
		if(IsSelect)
		{
			Debug.Log("Select: Battle ID[" + this.BattleCardID.ToString() + "].");
		}
		else
		{
			Debug.Log("Unselect: Battle ID[" + this.BattleCardID.ToString() + "].");
		}
		*/

		ShowSelector(IsSelect);
	}

	private void ShowTrim(bool isShow)
	{
		TrimCardGroup.SetActive(isShow);
		TargetMark.SetActive(false);
		TrimOverlay.SetActive(isShow);

		if(isShow)
		{
			BattleUIManager.RequestBringForward(TrimCardGroup);
			BattleUIManager.RequestBringForward(TrimOverlay);

			ActionIcon.ShowActionIcon(ActionIcon.ActionState.None);
		}
		else
		{
			UpdateCardIcon();
		}
	}

	public void ShowArrow(bool isShow)
	{
		//TrimCardGroup.SetActive(isShow);
		ArrowPanel.gameObject.SetActive(isShow);

		if(isShow)
		{
			//BattleUIManager.RequestBringForward(TrimCardGroup);
			BattleUIManager.RequestBringForward(ArrowPanel.gameObject);
		}

		//Debug.Log("SHOW ARROW " + isShow);
	}

	public void ShowTargetMark(bool isShow)
	{
		TrimCardGroup.SetActive(isShow);
		TargetMark.SetActive(isShow);
		TrimOverlay.SetActive(false);

		if(isShow)
		{
			BattleUIManager.RequestBringForward(TrimCardGroup);
			BattleUIManager.RequestBringForward(TargetMark);

			ActionIcon.ShowActionIcon(ActionIcon.ActionState.None);
		}
		else
		{
			UpdateCardIcon();
		}
	}

	private void ShowSelector(bool isShow)
	{
		SelectorGlow.gameObject.SetActive(isShow);
		if(isShow)
		{
			BattleUIManager.RequestBringForward(SelectorGlow.gameObject, false);
		}
	}

	public void OnClick()
	{
		if(!IsClickable) return;

        if (_onClickBattleCard != null)
        {
			_onClickBattleCard.Invoke(this, _battleCardIDList[_showIndex]);
        }
	}
    #endregion
}
