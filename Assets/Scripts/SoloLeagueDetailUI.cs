﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoloLeagueDetailUI : MonoBehaviour {

	#region Delegates 
	public delegate void OnCloseDetail();
	#endregion

	public UITexture RewardTexture;
	public UILabel LevelLabel;
	public UILabel ExpLabel;
	public UILabel UIDiamondLabel;
	public UIButton DeckEditorButton;
	public UIButton ShopButton;
	public UIButton BackButton;
    public UISlider ExpSlider;

	private OnCloseDetail _onCloseDetail = null;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ShowUI(int level, int expCom, int diamond)
	{
		ShowDetail(level, expCom, diamond);
		gameObject.SetActive(true);
	}

	public void HideUI()
	{
		gameObject.SetActive (false);
	}

	private void ShowDetail(int level, int expCom, int diamond)
	{
        float expBarValue = 0.0f;
        float expCom_ = expCom;


        //RewardTexture.mainTexture = 
        //public UITexture RewardTexture;
        float expComNextLv = PlayerLevelDB.GetExpCom(level + 1);
        float expComCurLv = PlayerLevelDB.GetExpCom(level);

		LevelLabel.text = level.ToString();
		ExpLabel.text = (expCom - expComCurLv).ToString() + "/" + (expComNextLv - expComCurLv).ToString();
		UIDiamondLabel.text = diamond.ToString();
        expBarValue = ((expCom_ - expComCurLv) / (expComNextLv - expComCurLv));
        Debug.Log("expbarvalue = " + expBarValue.ToString());
        ExpSlider.value = expBarValue;

	}

}
