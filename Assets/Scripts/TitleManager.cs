﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using GameAnalyticsSDK;
using UnityEngine.UI;
using UnityEngine.Events;
using System;
using Firebase.Auth;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;


public class TitleManager : MonoBehaviour 
{
    private static TitleManager _instance = null;
    public static TitleManager Instance { get { return _instance; } }

    private string _email;
    private string _password;
    private bool _isAutoLogin = false;

	//public RatingManager RatingManager;
	public GameObject LoadingUI;
    public AudioClip TitleBGM;  
    
    public TitleButtonUI TitleButtonUI;
    public DataFoundUI DataFoundUI;
    public ForgotPasswordUI ForgotPasswordUI;
    public GuestBewareUI GuestBewareUI;
    public MessageBoxUI MessageBoxUI;
    public LoginUI LoginUI;
    public RegisterUI RegisterUI;

    
    public bool IsDiableAutoLogin = true;
    bool isFromGuestBewareUI = false;

	private float _startTime;
	private float _endTime;
			
	void Awake()
	{
        _instance = this;
	}

	// Use this for initialization
	void Start () 
	{
        //PlayerSave.ClearAllSave();
        Initialization();
	}

	/*
    // Update is called once per frame
    void Update () 
    {
    }
    */

	private void OnDestroy()
	{
		_instance = null;
	}

	public void OnTitleAnimationCompleted()
    {
        //ShowRating();
        LoadSound();
        //bool isGuestNeedLogin = PlayerSave.isGuestNeedLogin();
        ////isGuestNeedLogin = false;
        ////PlayerSave.SaveGuestNeedLogin(isGuestNeedLogin);

        ////ScreenDubugLog.Instance.Print("check is guessneedlogin = " + isGuestNeedLogin);
        //if (isGuestNeedLogin)
        //{
        //    ShowTitleUI();
        //    RegisterUI.ShowUI(OnRegisterUIClickCreate, OnRegisterUIClickCancelFromGuest);
        //    isGuestNeedLogin = false;
        //    PlayerSave.SaveGuestNeedLogin(isGuestNeedLogin);
        //    Debug.Log("animate complete");

        //}
        //else
        //{
            
        // Check auto login.

        string email = PlayerSave.GetPlayerEmail();
        string password = PlayerSave.GetPlayerPassword();

        //will check internet connection around here

        AccountType accountType = PlayerSave.GetPlayerAccountType();
        ScreenDubugLog.Instance.Print("check Account Type = " + accountType);
        ScreenDubugLog.Instance.Print("email & Password = " + email + " " + password );

        //TitleButtonUI.HideAllButton(); 
        ShowTitleUI(PlayerSave.IsAutoLogin());
    }
    
    #region TitleUI Methods
    //private void ShowTitleUI()
    //{
    //    TitleButtonUI.ShowUI(OnTitleUIClickGuest, OnTitleUIClickKOS);
    //}

    private void ShowTitleUI(bool showStartButton)
    {
        if (showStartButton)
        {
            //TitleButtonUI.ShowStartButton(
            TitleButtonUI.ShowStartButton(OnTitleClickStartButton);
        }
        else
        {

            TitleButtonUI.ShowUI(OnTitleUIClickGuest, OnTitleUIClickKOS); 
        }

    }

    private void OnTitleUIClickGuest()
    {
        ShowGuestBewareUI();
    }
    
    private void OnTitleUIClickKOS()
    {
        ShowLoginUI();
    }

    private void OnTitleClickStartButton()
    {
        TitleButtonUI.Instance.HideStartButton();
        AccountType accountType = PlayerSave.GetPlayerAccountType();
        string email = PlayerSave.GetPlayerEmail();
        string password = PlayerSave.GetPlayerPassword();
        ScreenDubugLog.Instance.Print("email = " + email + " password " + password);
                                       
        ScreenDubugLog.Instance.Print("Acctount type = " + accountType);
        if (accountType == AccountType.KOS)
        {
            ScreenDubugLog.Instance.Print("first flow ?" + PlayerSave.IsPlayerFirstTime());
            //Logout();
            OnLoginUIClickLogin(email, password);
        }
        else
        {
            LoadMainMenu();
        }

    }
    #endregion
    
    #region GuestBewareUI Methods
    private void ShowGuestBewareUI()
    {
        GuestBewareUI.ShowUI(OnGuestBewareUIClickContinue, OnGuestBewareUIClickCancel);
    }
    
    private void OnGuestBewareUIClickContinue()
    {
        GuestLogin();
    }
    
    private void OnGuestBewareUIClickCancel()
    {
        //ShowTitleUI();
        isFromGuestBewareUI = true;
        ShowRegisterUI();
    }
    #endregion
    
    #region LoginUI Methods
    private void ShowLoginUI()
    {
        LoginUI.ShowUI(OnLoginUIClickLogin, OnLoginUIClickCancel, OnLoginUIClickCreate, OnLoginUIClickForgot);
    }
    
    private void OnLoginUIClickLogin(string email, string password)
    {
        if (string.IsNullOrEmpty(email) && string.IsNullOrEmpty(password))
        {
            ShowMessageBox(
                  "Error"
                , "Please Enter Email and Password"
                , "Close"
                , ShowLoginUI
            );
        }
        else if (string.IsNullOrEmpty(email))
        {            
            ShowMessageBox(
                  "Error"
                , "Please Enter Email"
                , "Close"
                , ShowLoginUI
            );
        }
        else if (string.IsNullOrEmpty(password))
        {
            ShowMessageBox(
                  "Error"
                , "Please Enter Password"
                , "Close"
                , ShowLoginUI
            );
        }
        else
        {
            ScreenDubugLog.Instance.Print(string.Format("Start login Email : {0}", email));
            Debug.Log(string.Format("Start login Email : {0}", email));
            _isAutoLogin = false;

            Login(email, password);
        }
    }
    
    private void OnLoginUIClickCancel()
    {
        ShowTitleUI(false);
    }
    
    private void OnLoginUIClickCreate()
    {
        ShowRegisterUI();
    }
    
    private void OnLoginUIClickForgot()
    {
        ShowForgotPasswordUI();
    }
    #endregion
    
    #region RegisterUI Methods
    private void ShowRegisterUI()
    {
        RegisterUI.ShowUI(OnRegisterUIClickCreate, OnRegisterUIClickCancel);
    }
    
    private void OnRegisterUIClickCreate(string email, string password)
    {
        if (string.IsNullOrEmpty(email) && string.IsNullOrEmpty(password))
        {
            ShowMessageBox(
                  "Error"
                , "Please Enter Email and Password"
                , "Close"
                , ShowRegisterUI
            );
        }
        else if (string.IsNullOrEmpty(email))
        {            
            ShowMessageBox(
                  "Error"
                , "Please Enter Email"
                , "Close"
                , ShowRegisterUI
            );
        }
        else if (string.IsNullOrEmpty(password))
        {
            ShowMessageBox(
                  "Error"
                , "Please Enter Password"
                , "Close"
                , ShowRegisterUI
            );
        }
        else
        {
            ScreenDubugLog.Instance.Print(string.Format("Start signup Email : {0}", email));
            Debug.Log(string.Format("Start signup Email : {0}", email));
            SignUp(email, password);
        }
    }
    
    private void OnRegisterUIClickCancel()
    {
        if ( isFromGuestBewareUI )
        {
            isFromGuestBewareUI = false;
            ShowTitleUI(false);
            //ShowGuestBewareUI();

           // RegisterUI.HideUI();
        }
        else
        {
            isFromGuestBewareUI = false;
            ShowLoginUI();   
        }

        //
    }

    private void OnRegisterUIClickCancelFromGuest()
    {
         
        RegisterUI.HideUI();
    }
    #endregion
    
    #region ForgotPasswordUI Methods
    private void ShowForgotPasswordUI()
    {
        ForgotPasswordUI.ShowUI(OnForgotPasswordUIClickSend, OnForgotPasswordUIClickCancel);
    }
    
    private void OnForgotPasswordUIClickSend(string email)
    {
        ForgetPassword(email, ShowLoginUI, ShowForgotPasswordUI);
    }
    
    private void OnForgotPasswordUIClickCancel()
    {
        ShowLoginUI();
    }
    #endregion
    
    #region MessageBoxUI Methods
    private void ShowMessageBox(string header,string message, string buttonText, UnityAction onClickButton)
    {
        MessageBoxUI.ShowUI(header, message, buttonText, onClickButton);
    }
    #endregion
    
    #region LoadingUI Methods
    public void ShowLoading()
    {
        LoadingUI.gameObject.SetActive(true);
    }
    
    public void HideLoading()
    {
        LoadingUI.gameObject.SetActive(false);
    }
    #endregion

    private void Initialization()
    {
        //Debug.Log ("TITLE MANAGER START!!");
        //Logout();
        ScreenDubugLog.Instance.Print("Initialization...");
        GameAnalytics.NewDesignEvent ("Achievement:StartTitle", 1);
        _startTime = Time.time;
    }

    private void LoadSound()
    {
        if (!SoundManager.IsPlayBGM(TitleBGM))
        {
            //Debug.Log(TitleBGM);
            SoundManager.PlayBGM(TitleBGM);
        }
    }

    private void GenerateNewPlayer()
    {
        // Clear save.
        //PlayerSave.ClearAllSave();

        ////// Load default deck.
        ////DeckPackData pack = new DeckPackData("D0001");

        ////// Save to player local save.
        ////PlayerSave.SavePlayerDeck(pack.DeckData);

        ////// Update new card to card collection to default.
        ////for (int index = 0; index < pack.DeckData.Count; ++index)
        ////{
        ////    // Save new card to card collection.
        ////    PlayerSave.SavePlayerCardCollection(pack.DeckData.GetCardAt(index).PlayerCardData.CardData.CardID);
        ////}

        //// for play the first time.
        ////PlayerSave.IsPlayerFirstTime
        //PlayerSave.SavePlayerFirstTime(true);

        ////Solo League
        //PlayerSave.SavePlayFromSoloLeague(false);
        //PlayerSave.SaveSoloDraw(0);
        //PlayerSave.SaveSoloWin(0);
        //PlayerSave.SaveSoloLose(0);
        //PlayerSave.SavePlayerExp(0);
        //PlayerSave.SavePlayerDiamond(0);

        //// Reset coin.
        //PlayerSave.SavePlayerSilverCoin(500);
        //PlayerSave.SavePlayerGoldCoin(0);

        PlayerSave.ClearAllSave();

        PlayerSave.SavePlayerFirstTime(true);
        PlayerSave.SavePlayerAvatar("07"); //01

        PlayerSave.SavePlayerGoldCoin(0);
        PlayerSave.SavePlayerSilverCoin(500);
        PlayerSave.SavePlayerDiamond(0);
        PlayerSave.SaveGot500SilverFirstTime(true);

        PlayerSave.SavePlayerExp(0);

        PlayerSave.SetWinCount("NPC0001", 0);
        PlayerSave.SetWinCount("NPC0002", 0);
        PlayerSave.SetWinCount("NPC0003", 0);
        PlayerSave.SetWinCount("NPC0004", 0);
        PlayerSave.SetWinCount("NPC0005", 0);
        PlayerSave.SetWinCount("NPC0006", 0);

        PlayerSave.SaveSoloWin(0);
        PlayerSave.SaveSoloDraw(0);
        PlayerSave.SaveSoloLose(0);

        PlayerSave.SavePlayFromSoloLeague(false);

      
        // Load default deck.
        DeckPackData pack = new DeckPackData("D0001");
        PlayerSave.SavePlayerDeck(pack.DeckData);

        // Save stash.
        List<PlayerCardData> stashDeck = new List<PlayerCardData>();
        for (int index = 0; index < pack.DeckData.Count; ++index)
        {
            stashDeck.Add(pack.DeckData.GetCardAt(index).PlayerCardData);
        }
        PlayerSave.SavePlayerCardStash(stashDeck);

        // Update stash to card collection.
        for (int index = 0; index < stashDeck.Count; ++index)
        {
            PlayerSave.SavePlayerCardCollection(stashDeck[index].CardData.CardID);
        }

        // No need to merge.
        PlayerSave.SetMergeStashDeck(true);

        // Allow guest to submit data when signup.
        PlayerSave.SetSubmitGameData(false);
    }

    /*
    private void ShowRating()
    {
        if (ForceShowRating) 
        {
            ShowRatingPopup();
        }
        else
        {
            bool isRatingShown = PlayerSave.IsRatingShownFinish();

            if (!isRatingShown) 
            {
                _iGetRatingCount = PlayerSave.GetRatingAtTitle();

                //Debug.Log ("In Game count" + _iGetRatingCount);

                if (_iGetRatingCount == 4) 
                {
                    ShowRatingPopup ();
                    PlayerSave.SaveRatingShownFinish(true);
                }

                ++_iGetRatingCount;
                PlayerSave.SaveRatingAtTitle (_iGetRatingCount);
            }
        } 
    }
    
	public void ShowRatingPopup()
	{
		RatingManager.ShowRatingPopup();
		//popupMessageUI.ShowUI ("Welcome to KOS!!", "Understood", this.ShowFirstTimePopup2);
	}

//	public static int GetRatingCount()
//	{
//		int getRating;
//		getRating = PlayerSave.GetRatingAtTitle;
//		return getRating;
//	}
    */
    
    private void GuestLogin()
    {
       // PlayerSave.ClearAllSave();
        if (PlayerSave.IsPlayerFirstTime())
        {
            GenerateNewPlayer();
        }
        else
        {
            // Merge stash deck.
            if (!PlayerSave.IsMergeStashDeck())
            {
                DeckData deckData;
                List<PlayerCardData> stashList;
                PlayerSave.GetPlayerCardStash(out stashList);
                PlayerSave.GetPlayerDeck(out deckData);
                List<PlayerCardData> stashDeck = new List<PlayerCardData>(stashList);
                if (deckData != null && deckData.Count > 0)
                {
                    // Merge deck into stash.
                    for (int index = 0; index < deckData.Count; ++index)
                    {
                        stashDeck.Add(deckData.GetCardAt(index).PlayerCardData);
                    }
                }
                PlayerSave.SavePlayerCardStash(stashDeck);
                PlayerSave.SetMergeStashDeck(true);
            }
        }
        PlayerSave.SavePlayerAccountType(AccountType.Guest);

        ////Solo League
        //PlayerSave.SavePlayFromSoloLeague(false);
        //PlayerSave.SaveSoloDraw(0);
        //PlayerSave.SaveSoloWin(0);
        //PlayerSave.SaveSoloLose(0);
        //PlayerSave.SavePlayerExp(0);
        //PlayerSave.SavePlayerDiamond(0);

        LoadMainMenu();
    }

    private void ForgetPassword(string email, UnityAction onSuccess, UnityAction onFail)
    {
        //Debug.Log("ForgetPassword Email = " + email);
    
        FirebaseAuth auth = FirebaseAuth.DefaultInstance;
        
        if (!string.IsNullOrEmpty(email))
        {
            auth.SendPasswordResetEmailAsync(email).ContinueWith(task => {
                if (task.IsCanceled)
                {
                    ShowMessageBox(
                          "Forget Password"
                        , "Password reset has been canceled."
                        , "Close"
                        , onFail
                    );
                    //Debug.LogError("SendPasswordResetEmailAsync was canceled.");
                    return;
                }
                if (task.IsFaulted)
                {
                    ShowMessageBox(
                          "Forget Password"
                        , "Password reset has failed. " + task.Exception
                        , "Close"
                        , onFail
                    );
                    //Debug.LogError("SendPasswordResetEmailAsync encountered an error: " + task.Exception);
                    return;
                }
                if (CheckEmail.IsEmail(email)) 
                {
                    ShowMessageBox(
                          "Forget Password"
                        , "Password reset email sent successfully."
                        , "Close"
                        , onSuccess
                    );
                    //Debug.Log("Password reset email sent successfully.");
                }
                else
                {
                    ShowMessageBox(
                          "Forget Password"
                        , "Password reset has unknown error."
                        , "Close"
                        , onFail
                    );
                    //Debug.LogError("SendPasswordResetEmailAsync encountered an error.");
                }
            });
        }
        else
        {
            ShowMessageBox(
                  "Forget Password"
                , "Please enter an email address"
                , "Close"
                , onFail
            );
            //Debug.Log("no email");        
        }
    }

    #region SignUp Methods
    public void SignUp(string email, string password)
    {
       // ResultText.text = PasswordIn.value + " " + EmailInput.text.ToString() + " " + UserNameInput.text.ToString();
        try
        {
            if (!KOSServer.Instance.IsProcessSignUp)
            {
                _email = email;
                _password = password;
                ScreenDubugLog.Instance.Print("Processing signup..");
                KOSServer.Instance.SignUp(email, password, email, this.OnSignUpCompleted, this.OnSignUpFailed);
            }
        }
        catch (FirebaseException e)
        {            
            ShowMessageBox(
                  "Error Signup"
                , e.Message
                , "Close"
                , ShowRegisterUI
            );
        }
    }

    private void OnSignUpCompleted(string email)
    {
        PlayerSave.SavePlayerFirstTime(true);
        ShowMessageBox(
              "Success"
            , "Your KOS account has been created"
            , "START GAME"
            , () => {
                Login(email, _password);
            }
        );
    }

    private void OnSignUpFailed(string errorMessage)
    {        
         ShowMessageBox(
              "Signup Failed"
            , errorMessage
            , "Close"
            , ShowRegisterUI
        );
    }
    #endregion

    #region Login Methods
    private void Login(string email, string password)
    {
        try
        {
            if (!KOSServer.Instance.IsProcessLogin)
            {
                ScreenDubugLog.Instance.Print("Processing login..");
                PlayerSave.SavePlayerPassword(password);
                KOSServer.Instance.Login(email, password, this.OnLoginCompleted, this.OnLoginFailed);
            } 
        }
        catch (FirebaseException e)
        {   
            ShowMessageBox(
                  "Error Login"
                , "Please correct your email or password"
                , "Close"
                , ShowLoginUI
            );
        } 

    }

    private void OnLoginCompleted()
    {
        if(_isAutoLogin)
        {
            LoadMainMenu();
        }
        else
        {
            ShowMessageBox(
                  "Success"
                , "You successfully logged in to the game"
                , "START GAME"
                , LoadMainMenu
            );
        }
    }

    private void OnLoginFailed(string errorMessage)
    {        
        ShowMessageBox(
              "Login Failed"
            , errorMessage
            , "Close"
            , ShowLoginUI
        );
    }
    #endregion

    #region Logout
    public void Logout()
    {
        //if (KOSServer.Instance.IsLogin)
        //{
            PlayerSave.ClearAllSave();
            KOSServer.Instance.Logout();
       // }
    }
    #endregion

    private void LoadMainMenu()
    {   
        TitleButtonUI.Instance.ShowOnlyStartButton();
        ShowLoading();

        ScreenDubugLog.Instance.Print("going to request cache data." + _isRequestCacheData );
        if(!_isRequestCacheData)
        {
            /*
            #if UNITY_EDITOR
            OnCacheDataCompleted();
            #else
            */


            //if(KOSServer.Instance.IsLogin)
            //{
            //    ScreenDubugLog.Instance.Print("before start cachedata Kosserver.instamce.isLogin");
            //    StartCoroutine(_StartCacheData(0.1f));
            //}
            //else
            {
                OnCacheDataCompleted();
            }
            //#endif
        }
        else
        {
            OnCacheDataFailed("cacheing faile");
        }
    }
    
    private IEnumerator _StartCacheData(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        RequestCacheData(OnCacheDataCompleted, OnCacheDataFailed);
    }
    
    private void OnCacheDataCompleted()
    {
        StartCoroutine(_LoadMainMenu());
    }
    
    private void OnCacheDataFailed(string error)
    {
        // retry.
        ShowMessageBox(
              "Error"
            , error
            , "Retry"
            , () => { StartCoroutine(_StartCacheData(3.0f)); }
        );
    }

    private IEnumerator _LoadMainMenu()
    {
        yield return new WaitForSeconds(0.1f);
        
        DBManager.Instance.InitDBAsync(delegate()
        {
            _endTime = Time.time;
            float playTime = _endTime - _startTime;
            playTime = playTime / 60.0f;
            
            GameAnalytics.NewDesignEvent ("Achievement:EndTitle(Min)", playTime);   
            SceneManager.LoadSceneAsync ("MainMenuScene2");
        });
    }

    #region CacheData Methods
    private bool _isRequestCacheData = false;
    private UnityAction _onCacheSuccess = null;
    private UnityAction<string> _onCacheFail = null;

    private void RequestCacheData(UnityAction onSuccess, UnityAction<string> onFail)
    {
        if(!_isRequestCacheData)
        {
            _isRequestCacheData = true;
            _onCacheSuccess = onSuccess;
            _onCacheFail = onFail;
            ScreenDubugLog.Instance.Print("Rquest cached data..");
            Debug.Log("Start Cache data...");
            KOSServer.Instance.CacheData(this.OnCacheCompleted, this.OnCacheFailed);
        }
        else
        {
            if(onFail != null)
            {
                onFail.Invoke("Already request cache data.");
                ScreenDubugLog.Instance.Print("already request cached data");
            }
        }
    }

    private void OnCacheCompleted()
    {
        ScreenDubugLog.Instance.Print("cache successful");
        Debug.Log("Cache Successful.");
        string text = "";
        try
        {
            UserDBData data = KOSServer.Instance.PlayerData;

            text = string.Format("Cache Successful.\n{0}", data.GetDebugPrintText());
            Debug.Log(text);
        }
        catch (System.Exception e)
        {
            Debug.Log(e.Message);
        }
        
        _isRequestCacheData = false;
        if(_onCacheSuccess != null)
        {
            _onCacheSuccess.Invoke();
        }
    }

    private void OnCacheFailed(string errorMessage)
    {        
        Debug.Log(errorMessage);
        
        _isRequestCacheData = false;
        if(_onCacheFail != null)
        {
            _onCacheFail.Invoke(errorMessage);
        }
    }
    #endregion
}
