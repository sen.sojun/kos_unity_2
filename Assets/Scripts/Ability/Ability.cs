﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardAbility
{
    #region AbilityParams Class
    public class AbilityParams
    {
        #region Enums
        #endregion

        #region Private Properties
        private string _key = "";
        private List<string> _paramsList = null;
        #endregion

        #region Public Properties
        public string Key { get { return _key; } }
        public List<string> ParamsList { get { return _paramsList; } }
        #endregion

        #region Constructors
        public AbilityParams()
        {
            _key = "";
            _paramsList = new List<string>();
        }

        public AbilityParams(string key, List<string> paramsList)
        {
            _key = key;
            _paramsList = paramsList;
        }
            
        public AbilityParams(string abilityText)
        {
            bool isSuccess = CSVReader.ReadAbilityText(abilityText, out _key, out _paramsList);
            if (!isSuccess)
            {
                _key = "";
                _paramsList = new List<string>();
            }
        }
        #endregion

        #region Static Methods
        public static AbilityParams GetAbilityParams(string abilityText)
        {
            AbilityParams abParam = null;
            bool isSuccess = CSVReader.ReadAbilityText(abilityText, out abParam);

            return abParam;
        }

        public static List<AbilityParams> GetAbilityParamsList(string abilityText)
        {
            List<CardAbility.AbilityParams> abParamsList = null;
            bool isSuccess = CSVReader.ReadAbilityText(abilityText, out abParamsList);

            return abParamsList;
        }

        public static bool ParamToInt(AbilityParams abParam, Ability ability, out int result)
        {
            bool isSuccess = int.TryParse(abParam.Key, out result);

            if (isSuccess)
            {
                return true;
            }
            else
            {
                switch (abParam.Key)
                {
                    case "BATTLECARDCOUNT": result = IntParser.BattleCardCount(abParam, ability); return true;
                    case "UNIQUECARDCOUNT": result = IntParser.UniqueCardCount(abParam, ability); return true;
                    default: break;
                }
            }     

            result = 0;
            return false;
        }

        public static bool ParamToBool(AbilityParams abParam, Ability ability, out bool result)
        {
            bool isSuccess = bool.TryParse(abParam.Key, out result);

            if (isSuccess)
            {
                return true;
            }
            else
            {
                switch (abParam.Key.ToUpper())
                {
                    case "ISEQUAL":         result = BoolParser.IsEqual(abParam, ability);          return true;
                    case "ISMORE":          result = BoolParser.IsMore(abParam, ability);           return true;
                    case "ISLESS":          result = BoolParser.IsLess(abParam, ability);           return true;
                    case "ISMOREEQUAL":     result = BoolParser.IsMoreEqual(abParam, ability);      return true;
                    case "ISLESSEQUAL":     result = BoolParser.IsLessEqual(abParam, ability);      return true;
                    case "ISNOTEQUAL":      result = BoolParser.IsNotEqual(abParam, ability);       return true;
                    case "ISHANDEQUAL":     result = BoolParser.IsHandEqual(abParam, ability);      return true;
                    case "ISHANDMORE":      result = BoolParser.IsHandMore(abParam, ability);       return true;
                    case "ISHANDLESS":      result = BoolParser.IsHandLess(abParam, ability);       return true;
                    case "ISHANDMOREEQUAL": result = BoolParser.IsHandMoreEqual(abParam, ability);  return true;
                    case "ISHANDLESSEQUAL": result = BoolParser.IsHandLessEqual(abParam, ability);  return true;
                    case "ISHANDNOTEQUAL":  result = BoolParser.IsHandNotEqual(abParam, ability);   return true;
                    case "ISCANACT":        result = BoolParser.IsCanAct(abParam, ability);         return true;
                    case "CONDITION":       result = BoolParser.Condition(abParam, ability);        return true;
                    default: break;
                }
            }

            result = false;
            return false;
        }

        public static bool ParamToPlayerIndex(AbilityParams abParam, Ability ability, out PlayerIndex result)
        {
            if(ability != null && ability.IsHasOwner)
            {
                switch (abParam.Key)
                {
                    case "OWNER": result = PlayerIndexParser.GetOwnerIndex(ability); return true;
                    case "ENEMY": result = PlayerIndexParser.GetEnemyIndex(ability); return true;
                    default: break;
                }
                        
            }

            result = PlayerIndex.One;
            return false;
        }

        public static bool ParamToLocalPlayerIndex(AbilityParams abParam, Ability ability, out LocalPlayerIndex result)
        {
            if(ability != null && ability.IsHasOwner)
            {
                foreach (LocalPlayerIndex lpIndex in System.Enum.GetValues(typeof(LocalPlayerIndex)))
                {
                    if(string.Compare(abParam.Key.ToUpper(), lpIndex.ToString().ToUpper()) == 0)
                    {
                        result = lpIndex;
                        return true;
                    }
                } 
            }

            result = LocalPlayerIndex.Owner;
            return false;
        }

        public static bool ParamToBattleZone(AbilityParams abParam, Ability ability, out BattleZoneIndex result)
        {
            return BattleZoneIndexParser.GetBattleZoneIndex(abParam.Key, ability, out result);
        }

        public static bool ParamToCardZone(AbilityParams abParam, Ability ability, out CardZoneIndex result)
        {
            return CardZoneIndexParser.GetCardZoneIndex(abParam.Key, ability, out result);
        }
            
        public static bool ParamToBattleCardList(AbilityParams abParam, Ability ability, out List<BattleCardData> result)
        {
            switch (abParam.Key.ToUpper())
            {
                case "GETBATTLECARD": 
                {
                    Equation equa = Equation.BoolEquaTextToVar(abParam.ParamsList[0], Equation.EquationType.BattleCardFilter);
                    result = equa.GetBattleCard(ability);
                    return true;
                }

                case "SELECTBATTLECARD":
                {
                    result = new List<BattleCardData>();
                    List<int> idList = AESelectBattleCard.SelectBattleCardList;
                    foreach (int id in idList)
                    {
                        BattleCardData card;
                        bool isFound = BattleManager.Instance.FindBattleCard(id, out card);
                        if (isFound)
                        {
                            result.Add(card);
                        }

                    }
                    return true;     
                }

                default: break;
            }
                    
            result = new List<BattleCardData>();
            return false;
        }

        public static bool TextToBattleCardList(string paramText, Ability ability, out List<BattleCardData> result)
        {
            Equation equa = Equation.BoolEquaTextToVar(paramText, Equation.EquationType.BattleCardFilter);
            result = equa.GetBattleCard(ability);
            return true;
        }

        public static bool ParamToUniqueCardList(AbilityParams abParam, Ability ability, out List<UniqueCardData> result)
        {
            switch (abParam.Key.ToUpper())
            {
                case "GETUNIQUECARD": 
                {
                    Equation equa = Equation.BoolEquaTextToVar(abParam.ParamsList[0], Equation.EquationType.UniqueCardFilter);
                    result = equa.GetUniqueCard(ability);
                    return true;
                }

                case "GETHANDCARD": 
                {
                    Equation equa = Equation.BoolEquaTextToVar(abParam.ParamsList[0], Equation.EquationType.UniqueCardFilter);
                    result = equa.GetHandCard(ability);
                    return true;
                }

                case "SELECTUNIQUECARD":
                {
                    result = new List<UniqueCardData>();
                    List<int> idList = AESelectUniqueCard.SelectUniqueCardList;
                    foreach (int id in idList)
                    {
                        UniqueCardData card;
                        PlayerIndex owner;
                        bool isFound = BattleManager.Instance.FindUniqueCard(id, out card, out owner);
                        if (isFound)
                        {
                            result.Add(card);
                        }

                    }
                    return true;     
                }

                default: break;
            }

            result = new List<UniqueCardData>();
            return false;
        }
          
        public static bool TextToUniqueCardList(string paramText, Ability ability, out List<UniqueCardData> result)
        {
            Equation equa = Equation.BoolEquaTextToVar(paramText, Equation.EquationType.UniqueCardFilter);
            result = equa.GetUniqueCard(ability);
            return true;
        }

        public static bool TextToHandCardList(string paramText, Ability ability, out List<UniqueCardData> result)
        {
            Equation equa = Equation.BoolEquaTextToVar(paramText, Equation.EquationType.UniqueCardFilter);
            result = equa.GetHandCard(ability);
            return true;
        }

        public static bool ParamToBattleCardFilter(AbilityParams abParam , Ability ability, out BattleCardFilter result)
        {
            bool isSuccess = BattleCardFilter.CreateBattleCardFilter(abParam, ability, out result);
            return isSuccess;
        }

        public static bool ParamToUniqueCardFilter(AbilityParams abParam , Ability ability, out UniqueCardFilter result)
        {
            bool isSuccess = UniqueCardFilter.CreateUniqueCardFilter(abParam, ability, out result);
            return isSuccess;
        }

        public static bool ParamToCardType(AbilityParams abParam, Ability ability, out CardTypeData.CardType result)
        {
            result = CardTypeData.StringToCardType(abParam.Key);
            return true;
        }

        public static bool ParamToCardSubType(AbilityParams abParam, Ability ability, out CardSubTypeData.CardSubType result)
        {
            result = CardSubTypeData.StringToCardSubType(abParam.Key);
            return true;
        }

        public static bool ParamToCardJob(AbilityParams abParam, Ability ability, out CardJobData.JobType result)
        {
            result = CardJobData.StringToCardJob(abParam.Key);
            return true;
        }

        public static bool ParamToPassiveIndex(AbilityParams abParam, Ability ability, out PassiveIndex result)
        {
            result = PassiveIndex.StringToPassiveIndex(abParam.Key);
            return true;
        }

        public static bool ParamToCardClan(AbilityParams abParam, Ability ability, out CardClanData.CardClanType result)
        {
            result = CardClanData.StringToCardClan(abParam.Key);
            return true;
        }

        public static bool ParamToCardSymbol(AbilityParams abParam, Ability ability, out CardSymbolData.CardSymbol result)
        {
            result = CardSymbolData.StringToCardSymbol(abParam.Key);
            return true;
        }
        #endregion
    }
    #endregion

    #region Ability BoolParser Class
    public class BoolParser
    {
        public static bool IsEqual(AbilityParams abParam, Ability ability)
        {
            AbilityParams abParam_0 = new AbilityParams(abParam.ParamsList[0]);
            AbilityParams abParam_1 = new AbilityParams(abParam.ParamsList[1]);

            bool isSuccess = true;

            int value1 = 0;
            isSuccess &= AbilityParams.ParamToInt(abParam_0, ability, out value1);

            int value2 = 0;
            isSuccess &= AbilityParams.ParamToInt(abParam_1, ability, out value2);

            if (isSuccess)
            {
                return (value1 == value2);
            }

            return false;
        }

        public static bool IsMore(AbilityParams abParam, Ability ability)
        {
            AbilityParams abParam_0 = new AbilityParams(abParam.ParamsList[0]);
            AbilityParams abParam_1 = new AbilityParams(abParam.ParamsList[1]);

            bool isSuccess = true;

            int value1 = 0;
            isSuccess &= AbilityParams.ParamToInt(abParam_0, ability, out value1);

            int value2 = 0;
            isSuccess &= AbilityParams.ParamToInt(abParam_1, ability, out value2);

            if (isSuccess)
            {
                return (value1 > value2);
            }

            return false;
        }

        public static bool IsLess(AbilityParams abParam, Ability ability)
        {
            AbilityParams abParam_0 = new AbilityParams(abParam.ParamsList[0]);
            AbilityParams abParam_1 = new AbilityParams(abParam.ParamsList[1]);

            bool isSuccess = true;

            int value1 = 0;
            isSuccess &= AbilityParams.ParamToInt(abParam_0, ability, out value1);

            int value2 = 0;
            isSuccess &= AbilityParams.ParamToInt(abParam_1, ability, out value2);

            if (isSuccess)
            {
                return (value1 < value2);
            }

            return false;
        }

        public static bool IsMoreEqual(AbilityParams abParam, Ability ability)
        {
            AbilityParams abParam_0 = new AbilityParams(abParam.ParamsList[0]);
            AbilityParams abParam_1 = new AbilityParams(abParam.ParamsList[1]);

            bool isSuccess = true;

            int value1 = 0;
            isSuccess &= AbilityParams.ParamToInt(abParam_0, ability, out value1);

            int value2 = 0;
            isSuccess &= AbilityParams.ParamToInt(abParam_1, ability, out value2);

            if (isSuccess)
            {
                return (value1 >= value2);
            }

            return false;
        }

        public static bool IsLessEqual(AbilityParams abParam, Ability ability)
        {
            AbilityParams abParam_0 = new AbilityParams(abParam.ParamsList[0]);
            AbilityParams abParam_1 = new AbilityParams(abParam.ParamsList[1]);

            bool isSuccess = true;

            int value1 = 0;
            isSuccess &= AbilityParams.ParamToInt(abParam_0, ability, out value1);

            int value2 = 0;
            isSuccess &= AbilityParams.ParamToInt(abParam_1, ability, out value2);

            if (isSuccess)
            {
                return (value1 <= value2);
            }

            return false;
        }

        public static bool IsNotEqual(AbilityParams abParam, Ability ability)
        {
            AbilityParams abParam_0 = new AbilityParams(abParam.ParamsList[0]);
            AbilityParams abParam_1 = new AbilityParams(abParam.ParamsList[1]);

            bool isSuccess = true;

            int value1 = 0;
            isSuccess &= AbilityParams.ParamToInt(abParam_0, ability, out value1);

            int value2 = 0;
            isSuccess &= AbilityParams.ParamToInt(abParam_1, ability, out value2);

            if (isSuccess)
            {
                return (value1 != value2);
            }

            return false;
        }
            
        public static bool IsHandEqual(AbilityParams abParam, Ability ability)
        {
            //AbilityParams abParam_0 = new AbilityParams(abParam.ParamsList[0]);
            AbilityParams abParam_1 = new AbilityParams(abParam.ParamsList[1]);

            bool isSuccess = true;

            List<UniqueCardData> cardList;
            isSuccess &= AbilityParams.TextToHandCardList(abParam.ParamsList[0], ability, out cardList);

            int handCount = 0;
            isSuccess &= AbilityParams.ParamToInt(abParam_1, ability, out handCount);

            if (isSuccess)
            {
                return (cardList.Count == handCount);
            }

            return false;
        }
          
        public static bool IsHandMore(AbilityParams abParam, Ability ability)
        {
            //AbilityParams abParam_0 = new AbilityParams(abParam.ParamsList[0]);
            AbilityParams abParam_1 = new AbilityParams(abParam.ParamsList[1]);

            bool isSuccess = true;

            List<UniqueCardData> cardList;
            isSuccess &= AbilityParams.TextToHandCardList(abParam.ParamsList[0], ability, out cardList);

            int handCount = 0;
            isSuccess &= AbilityParams.ParamToInt(abParam_1, ability, out handCount);

            if (isSuccess)
            {
                return (cardList.Count > handCount);
            }

            return false;
        }

        public static bool IsHandLess(AbilityParams abParam, Ability ability)
        {
            //AbilityParams abParam_0 = new AbilityParams(abParam.ParamsList[0]);
            AbilityParams abParam_1 = new AbilityParams(abParam.ParamsList[1]);

            bool isSuccess = true;

            List<UniqueCardData> cardList;
            isSuccess &= AbilityParams.TextToHandCardList(abParam.ParamsList[0], ability, out cardList);

            int handCount = 0;
            isSuccess &= AbilityParams.ParamToInt(abParam_1, ability, out handCount);

            if (isSuccess)
            {
                return (cardList.Count < handCount);
            }

            return false;
        }

        public static bool IsHandMoreEqual(AbilityParams abParam, Ability ability)
        {
            //AbilityParams abParam_0 = new AbilityParams(abParam.ParamsList[0]);
            AbilityParams abParam_1 = new AbilityParams(abParam.ParamsList[1]);

            bool isSuccess = true;

            List<UniqueCardData> cardList;
            isSuccess &= AbilityParams.TextToHandCardList(abParam.ParamsList[0], ability, out cardList);

            int handCount = 0;
            isSuccess &= AbilityParams.ParamToInt(abParam_1, ability, out handCount);

            if (isSuccess)
            {
                return (cardList.Count >= handCount);
            }

            return false;
        }

        public static bool IsHandLessEqual(AbilityParams abParam, Ability ability)
        {
            //AbilityParams abParam_0 = new AbilityParams(abParam.ParamsList[0]);
            AbilityParams abParam_1 = new AbilityParams(abParam.ParamsList[1]);

            bool isSuccess = true;

            List<UniqueCardData> cardList;
            isSuccess &= AbilityParams.TextToHandCardList(abParam.ParamsList[0], ability, out cardList);

            int handCount = 0;
            isSuccess &= AbilityParams.ParamToInt(abParam_1, ability, out handCount);

            if (isSuccess)
            {
                return (cardList.Count <= handCount);
            }

            return false;
        }

        public static bool IsHandNotEqual(AbilityParams abParam, Ability ability)
        {
            //AbilityParams abParam_0 = new AbilityParams(abParam.ParamsList[0]);
            AbilityParams abParam_1 = new AbilityParams(abParam.ParamsList[1]);

            bool isSuccess = true;

            List<UniqueCardData> cardList;
            isSuccess &= AbilityParams.TextToHandCardList(abParam.ParamsList[0], ability, out cardList);

            int handCount = 0;
            isSuccess &= AbilityParams.ParamToInt(abParam_1, ability, out handCount);

            if (isSuccess)
            {
                return (cardList.Count != handCount);
            }

            return false;
        }

        public static bool IsCanAct(AbilityParams abParam, Ability ability)
        {
            //AbilityParams abParam_0 = new AbilityParams(abParam.ParamsList[0]);

            List<BattleCardData> cardList;
            bool isSuccess = AbilityParams.TextToBattleCardList(abParam.ParamsList[0], ability, out cardList);

            if (isSuccess)
            {
                foreach (BattleCardData card in cardList)
                {
                    if (!card.IsCardCanAct())
                    {
                        return false;
                    }
                }

                return true;
            }

            return false;
        }

        public static bool Condition(AbilityParams abParam, Ability ability)
        {
            Equation equa = Equation.BoolEquaTextToVar(abParam.ParamsList[0], Equation.EquationType.Bool);
            return equa.GetBool(ability);
        }

        public static bool Condition(string abParamText, Ability ability)
        {
            Equation equa = Equation.BoolEquaTextToVar(abParamText, Equation.EquationType.Bool);
            return equa.GetBool(ability);
        }
    }
    #endregion

    #region Ability IntParser Class
    public static class IntParser
    {
        public static int BattleCardCount(AbilityParams abParam, Ability ability)
        {
            AbilityParams abParam_0 = new AbilityParams(abParam.ParamsList[0]);

            List<BattleCardData> cardList;
            bool isSuccess = AbilityParams.ParamToBattleCardList(abParam_0, ability, out cardList);

            if (isSuccess)
            {
                return cardList.Count;
            }

            return 0;
        }

        public static int UniqueCardCount(AbilityParams abParam, Ability ability)
        {
            AbilityParams abParam_0 = new AbilityParams(abParam.ParamsList[0]);

            List<UniqueCardData> cardList;
            bool isSuccess = AbilityParams.ParamToUniqueCardList(abParam_0, ability, out cardList);

            if (isSuccess)
            {
                return cardList.Count;
            }

            return 0;
        }
    }
    #endregion

    #region Ability PlayerIndexParser Class
    public static class PlayerIndexParser
    {
        public static PlayerIndex GetOwnerIndex(Ability ability)
        {
            return ability.OwnerPlayerIndex;
        }

        public static PlayerIndex GetEnemyIndex(Ability ability)
        {
            return BattleManager.Instance.GetEnemyIndex(ability.OwnerPlayerIndex);
        }
    }
    #endregion

    #region Ability BattleZoneIndexParser Class
    public static class BattleZoneIndexParser
    {
        public static bool GetBattleZoneIndex(string key, Ability ability, out BattleZoneIndex battleZone)
        {
            if (ability != null && ability.IsHasOwner)
            {
                switch (key.ToUpper())
                {
                    case "BATTLEFIELD":
                    {
                        battleZone = BattleZoneIndex.Battlefield;
                        return true;
                    }

                    case "PLAYERBASE":
                    {
                        bool isLocal = BattleManager.Instance.IsLocalPlayer(ability.OwnerPlayerIndex);

                        if (isLocal)
                        {
                            if (ability.OwnerPlayerIndex == PlayerIndex.One)
                            {
                                battleZone = BattleZoneIndex.BaseP1;
                            }
                            else
                            {
                                battleZone = BattleZoneIndex.BaseP2;
                            }
                        }
                        else
                        {
                            if (ability.OwnerPlayerIndex == PlayerIndex.One)
                            {
                                battleZone = BattleZoneIndex.BaseP2;
                            }
                            else
                            {
                                battleZone = BattleZoneIndex.BaseP1;
                            }
                        }

                        return true;
                    }

                    case "ENEMYBASE":
                    {
                        bool isLocal = BattleManager.Instance.IsLocalPlayer(ability.OwnerPlayerIndex);

                        if (isLocal)
                        {
                            if (ability.OwnerPlayerIndex == PlayerIndex.One)
                            {
                                battleZone = BattleZoneIndex.BaseP2;
                            }
                            else
                            {
                                battleZone = BattleZoneIndex.BaseP1;
                            }
                        }
                        else
                        {
                            if (ability.OwnerPlayerIndex == PlayerIndex.One)
                            {
                                battleZone = BattleZoneIndex.BaseP1;
                            }
                            else
                            {
                                battleZone = BattleZoneIndex.BaseP2;
                            }
                        }

                        return true;
                    }

                    default: break;
                }
            }

            battleZone = BattleZoneIndex.Battlefield;
            return false;
        }
    }
    #endregion

    #region Ability CardZoneIndexParser Class
    public static class CardZoneIndexParser
    {
        public static bool GetCardZoneIndex(string key, Ability ability, out CardZoneIndex cardZone)
        {
            if (ability != null && ability.IsHasOwner)
            {
                switch (key.ToUpper())
                {
                    case "DECK": 
                    {
                        cardZone = CardZoneIndex.Deck;
                        return true;
                    }

                    case "HAND":
                    {
                        cardZone = CardZoneIndex.Hand;
                        return true;
                    }  

                    case "GRAVEYARD":
                    {
                        cardZone = CardZoneIndex.Graveyard;
                        return true;
                    }

                    case "REMOVEZONE":
                    {
                        cardZone = CardZoneIndex.RemoveZone;
                        return true;
                    }

                    default: break;
                }
            }

            cardZone = CardZoneIndex.Deck;
            return false;
        }
    }
    #endregion

    #region Ability Class
    public class Ability
    {
        #region Delegates
        public delegate void ActiveCallback();
        public delegate void UpdateCallback();
        public delegate void DestroyCallback();

        public delegate void OnFinishPrepare();
        public delegate void OnCancelPrepare();
        public delegate void OnFinishAction();
        public delegate void OnCancelAction();
        #endregion

        #region Protected Properties
        protected int _abilityIndex = -1;
        protected Ability2Data _data = null;
        protected UniqueCardData _uniqueCard = null;
        protected BattleCardData _battleCard = null;
        protected bool _isUseTrigger;

        protected ActiveCallback _activeCallback = null;
        protected UpdateCallback _updateCallback = null;
        protected DestroyCallback _destroyCallback= null;

        protected OnFinishPrepare _onFinishedPrepare = null;
        protected OnCancelPrepare _onCanceledPrepare = null;
        protected OnFinishAction _onFinishedAction = null;
        protected OnCancelAction _onCanceledAction = null;

        protected CardAbility.AbilityTarget  _target;

        protected List<CardAbility.AbilityCost> _costList = null;
        protected List<AbilityParams> _conditionList = null;
        protected List<CardAbility.AbilityEffect> _effectList = null;
        protected int _actionIndex;
        #endregion

        #region Public Properties
        public int Index { get { return _abilityIndex; } }
        public string ID { get { return _data.AbilityID; } }

        public UniqueCardData UniqueCard { get { return _uniqueCard; } }
        public BattleCardData BattleCard { get { return _battleCard; } }

        public CardAbility.AbilityActiveZone ActiveZone { get { return _data.ActiveZone; } }
        public CardAbility.AbilityActivePhase ActivePhase { get { return _data.ActivePhase; } }
        public Ability2Data.AbilityTargetType TargetType { get { return _data.TargetType; } }
        public Ability2Data.AbilityBenefitType BenefitType { get { return _data.BenefitType; } }

        public PlayerCardData PlayerCardData
        {
            get
            {
                if(IsSummon)
                {
                    return _battleCard.PlayerCardData;
                }
                else
                {
                    return _uniqueCard.PlayerCardData;
                }
            }
        }

        public PlayerIndex OwnerPlayerIndex
        {
            get
            {
                if(IsSummon)
                {
                    return _battleCard.OwnerPlayerIndex;
                }
                else
                {
                    return _uniqueCard.OwnerPlayerIndex;
                }
            }
        }

        public bool IsHasOwner { get { return (_battleCard != null || _uniqueCard != null); } }
        public bool IsSummon { get { return (_battleCard != null); } }
        #endregion

        #region Constructors
        private Ability(int abilityIndex, Ability2Data abilityData, UniqueCardData uniqueCard)
        {
            _abilityIndex = abilityIndex;
            _data = abilityData;
            _uniqueCard = uniqueCard;
            _battleCard = null;
        }

        private Ability(int abilityIndex, Ability2Data abilityData, BattleCardData battleCard)
        {
            _abilityIndex = abilityIndex;
            _data = abilityData;
            _uniqueCard = null;
            _battleCard = battleCard;
        }
        #endregion

        #region Methods
        public bool Init()
        {
            //ClearAllCallback();

            BindEvent();

            GenCost();
            GenCondition();
            GenEffect();

            return true;
        }

        private void BindEvent()
        {
            // To do bind trigger event
            foreach(Ability2Data.AbilityTriggerType triggerType in _data.TriggerTypeList)
            {
                switch(triggerType)
                {
                    case Ability2Data.AbilityTriggerType.Enter:       
                    {
                        BattleManager.Instance.BindBattleEventTrigger(
                              this.OnEventTrigger
                            , this.OwnerPlayerIndex
                            , BattleEventTrigger.BattleTriggerType.CARD_BE_SUMMONED
                            , this.BattleCard.BattleCardID
                        );
                        break;
                    }
                            
                    case Ability2Data.AbilityTriggerType.Use:    
                    {
                        _isUseTrigger = true;
                        break;
                    }
                       
                    /*
                    case Ability2Data.AbilityTriggerType.Remain:    
                    {
                        BattleManager.Instance.BindBattleEventTrigger(
                              this.OnDestroyEvent
                            , this.OwnerPlayerIndex
                            , BattleEventTrigger.BattleTriggerType.CARD_BE_DESTROYED
                            , this.BattleCard.BattleCardID
                        );

                        BattleManager.Instance.BindBattleEventTrigger(
                              this.OnDestroyEvent
                            , this.OwnerPlayerIndex
                            , BattleEventTrigger.BattleTriggerType.CARD_LEAVED
                            , this.BattleCard.BattleCardID
                        );

                        BattleManager.Instance.BindBattleEventTrigger(
                              this.OnUpdateEvent
                            , this.OwnerPlayerIndex
                            , BattleEventTrigger.BattleTriggerType.CARD_BE_SUMMONED
                            , BattleEventTrigger.BattleSide.Friendly
                        );

                        BattleManager.Instance.BindBattleEventTrigger(
                              this.OnUpdateEvent
                            , this.OwnerPlayerIndex
                            , BattleEventTrigger.BattleTriggerType.CARD_BE_DESTROYED
                            , BattleEventTrigger.BattleSide.Friendly
                        );

                        BattleManager.Instance.BindBattleEventTrigger(
                              this.OnUpdateEvent
                            , this.OwnerPlayerIndex
                            , BattleEventTrigger.BattleTriggerType.CARD_LEAVED
                            , BattleEventTrigger.BattleSide.Friendly
                        );

                        break;
                    }*/

                    case Ability2Data.AbilityTriggerType.BeginPhase:  
                    {
                        BattleManager.Instance.BindBattleEventTrigger(
                              this.OnEventTrigger
                            , this.OwnerPlayerIndex
                            , BattleEventTrigger.BattleTriggerType.BEGIN_OF_PHASE
                            , BattleEventTrigger.BattleSide.Friendly
                        );
                        break;
                    }

                    case Ability2Data.AbilityTriggerType.Ruin:       
                    {
                        BattleManager.Instance.BindBattleEventTrigger(
                              this.OnEventTrigger
                            , this.OwnerPlayerIndex
                            , BattleEventTrigger.BattleTriggerType.CARD_BE_DESTROYED
                            , this.BattleCard.BattleCardID
                        );
                        break;
                    }
                }
            }
        }
            
        private void GenCost()
        {
            _costList = new List<AbilityCost>();

            // Create all cost.
            List<AbilityParams> paramlist = AbilityParams.GetAbilityParamsList(_data.Cost);
            foreach (AbilityParams param in paramlist)
            {
                CardAbility.AbilityCost cost = null;
                bool isSuccess = CardAbility.AbilityCost.CreateAbilityCost(param, this, out cost);
                if (isSuccess && cost != null)
                {
                    _costList.Add(cost);
                }
            }
        }

        private void GenCondition()
        {
            _conditionList = new List<AbilityParams>();

            // Create all cost condition.
            foreach (CardAbility.AbilityCost cost in _costList)
            {
                List<CardAbility.AbilityParams> paramList = cost.GenerateCondition();
                if (paramList != null && paramList.Count > 0)
                {
                    foreach(CardAbility.AbilityParams param in paramList)
                    {
                        _conditionList.Add(param);
                    }
                }
            }
        }

        private void GenEffect()
        {
            _effectList = new List<CardAbility.AbilityEffect>();

            // Create all cost effect.
            foreach (CardAbility.AbilityCost cost in _costList)
            {
                List<CardAbility.AbilityEffect> effectList = cost.GenerateEffect();
                if (effectList != null && effectList.Count > 0)
                {
                    foreach(CardAbility.AbilityEffect effect in effectList)
                    {
                        _effectList.Add(effect);
                    }
                }
            }

            // Create all effect.
            List<AbilityParams> paramlist = AbilityParams.GetAbilityParamsList(_data.Effect);
            foreach (AbilityParams param in paramlist)
            {
                CardAbility.AbilityEffect effect = CardAbility.AbilityEffect.GenerateEffect(param, this);
                if (effect != null)
                {
                    _effectList.Add(effect);
                }
            }
        }
            
        public void Active()
        {
            if (IsActiveZone() && IsActivePhase() && IsCompleteCondition() && IsHaveTarget())
            {
                this.ActiveAbility(false);
            }
        }
            
        public void ActiveAbility(bool isUseByPlayer)
        {
            if(this.IsSummon)
            {
                BattleManager.Instance.RequestUseAbilityBattleCard(this.BattleCard.BattleCardID, this.Index, isUseByPlayer);
            }
            else
            {
                BattleManager.Instance.RequestUseHandCard(this.UniqueCard.UniqueCardID, isUseByPlayer);
            }
        }

        private void StartPrepare()
        {
            #if UNITY_EDITOR
            {
                string text = "";
                text = string.Format("Start Prepare [{0}] {1}."
                    , this.ID
                    , this._data.Description_EN
                );
                Debug.Log(text);
            }
            #endif

            _actionIndex = 0;
            this.OnPrepare();
        }

        private void OnPrepare()
        {
            if(_actionIndex < _effectList.Count)
            {
                // do prepare
                Debug.Log("Prepare: " + _effectList[_actionIndex].GetDebugText());
                _effectList[_actionIndex].Prepare(this.EndPrepare, this.CancelPrepare);
            }
        }

        private void EndPrepare()
        {
            // next action
            ++_actionIndex;

            if(_actionIndex < _effectList.Count)
            {
                this.OnPrepare();
            }
            else
            {
                this.FinishPrepare();
            }
        }

        private void CancelPrepare()
        {
            #if UNITY_EDITOR
            {
                string text = "";
                text = string.Format("Cancel Prepare [{0}] {1}."
                    , this.ID
                    , this._data.Description_EN
                );
                Debug.Log(text);
            }
            #endif

            if(_onCanceledPrepare != null)
            {
                _onCanceledPrepare.Invoke();
                _onCanceledPrepare = null;
            }

            FinishAction();
        }

        private void FinishPrepare()
        {
            // next prepare.   
            #if UNITY_EDITOR
            {
                string text = "";
                text = string.Format("Finish Prepare [{0}] {1}."
                    , this.ID
                    , this._data.Description_EN
                );
                Debug.Log(text);
            }
            #endif

            if(_onFinishedPrepare != null)
            {
                _onFinishedPrepare.Invoke();
                _onFinishedPrepare = null;
            }

            StartAction();
        }

        private void StartAction()
        {
            #if UNITY_EDITOR
            {
                string text = "";
                text = string.Format("Start Action [{0}] {1}."
                    , this.ID
                    , this._data.Description_EN
                );
                Debug.Log(text);
            }
            #endif

            _actionIndex = 0;

            if (string.Compare(this.ID, "A0001") == 0) // Auto: You recieve {0}G.
            {
                // Skip Preform Use Ability
                this.OnAction();
            }
            else
            {
                // Preform Use Ability
                List<ShowUseAbilityUI.PerformUseAction> stepList = new List<ShowUseAbilityUI.PerformUseAction>();

                ShowUseAbilityUI.PerformUseAction step = new ShowUseAbilityUI.PerformUseAction();
                step.PopupText = "";

                stepList.Add(step);

                BattleManager.Instance.PreformUseAbility(this.PlayerCardData, stepList, this.OnAction);
            }
        }

        private void OnAction()
        {
            if(_actionIndex < _effectList.Count)
            {
                // do action
                Debug.Log("Action: " + _effectList[_actionIndex].GetDebugText());
                _effectList[_actionIndex].Action(this.EndAction);
            }
        }

        private void EndAction()
        {
            // next action
            ++_actionIndex;

            if(_actionIndex < _effectList.Count)
            {
                this.OnAction();
            }
            else
            {
                this.FinishAction();
            }
        }

        private void FinishAction()
        {
            #if UNITY_EDITOR
            {
                string text = "";
                text = string.Format("Finish Action [{0}] {1}."
                    , this.ID
                    , this._data.Description_EN
                );
                Debug.Log(text);
            }
            #endif

            if(_onFinishedAction != null)
            {
                _onFinishedAction.Invoke();
                _onFinishedAction = null;
            }
        }

        private void CancelAction()
        {
            #if UNITY_EDITOR
            {
                string text = "";
                text = string.Format("Cancel Action [{0}] {1}."
                    , this.ID
                    , this._data.Description_EN
                );
                Debug.Log(text);
            }
            #endif

            if(_onCanceledAction != null)
            {
                _onCanceledAction.Invoke();
                _onCanceledAction = null;
            }
        }

        bool IsCanUse()
        {
            return (_isUseTrigger && IsActiveZone() && IsActivePhase() && IsCompleteCondition() && IsHaveTarget()); 
        }

        bool IsActiveZone()
        {
            if (BattleManager.Instance != null)
            {
                AbilityActiveZone.ActiveZone zone;
                if (IsSummon)
                {
                    switch (_battleCard.CurrentZone)
                    {
                        case BattleZoneIndex.BaseP1:
                        {
                            if (_battleCard.OwnerPlayerIndex == PlayerIndex.One)
                                zone = AbilityActiveZone.ActiveZone.PlayerField;
                            else
                                zone = AbilityActiveZone.ActiveZone.EnemyField;
                        }
                        break;

                        case BattleZoneIndex.Battlefield: 
                            zone = AbilityActiveZone.ActiveZone.BattleField;
                            break;

                        case BattleZoneIndex.BaseP2: 
                        {
                            if (_battleCard.OwnerPlayerIndex == PlayerIndex.Two)
                                zone = AbilityActiveZone.ActiveZone.PlayerField;
                            else
                                zone = AbilityActiveZone.ActiveZone.EnemyField;
                        }
                        break;

                        default: return false;
                    }
                }
                else
                {
                    switch (_uniqueCard.CurrentZone)
                    {
                        case CardZoneIndex.Deck:
                            zone = AbilityActiveZone.ActiveZone.Deck;
                            break;

                        case CardZoneIndex.Hand: 
                            zone = AbilityActiveZone.ActiveZone.Hand;
                            break;

                        case CardZoneIndex.Graveyard: 
                            zone = AbilityActiveZone.ActiveZone.Graveyard;
                            break;

                        case CardZoneIndex.RemoveZone: 
                            zone = AbilityActiveZone.ActiveZone.RemoveZone;
                            break;
                           
                        default: return false;
                    }
                }

                return (_data.ActiveZone.IsZone(zone));
            }

            return false;
        }

        bool IsActivePhase()
        {
            if (BattleManager.Instance != null)
            {
                CardAbility.AbilityActivePhase.ActivePhase phase;

                PlayerIndex activePlayerIndex = BattleManager.Instance.CurrentActivePlayerIndex;
                BattlePhase battlePhase = BattleManager.Instance.CurrentBattlePhase;

                if (battlePhase >= BattlePhase.BeginBegin && battlePhase <= BattlePhase.EndBegin)
                {
                    if (OwnerPlayerIndex == activePlayerIndex)
                    {
                        phase = AbilityActivePhase.ActivePhase.Owner_Begin;
                    }
                    else
                    { 
                        phase = AbilityActivePhase.ActivePhase.Other_Begin;
                    }
                }
                else if (battlePhase >= BattlePhase.BeginPostBegin && battlePhase <= BattlePhase.EndPostBegin)
                {
                    if (OwnerPlayerIndex == activePlayerIndex)
                    {
                        phase = AbilityActivePhase.ActivePhase.Owner_PostBegin;
                    }
                    else
                    { 
                        phase = AbilityActivePhase.ActivePhase.Other_PostBegin;
                    }
                }
                else if (battlePhase >= BattlePhase.BeginDraw && battlePhase <= BattlePhase.EndDraw)
                {
                    if (OwnerPlayerIndex == activePlayerIndex)
                    {
                        phase = AbilityActivePhase.ActivePhase.Owner_Draw;
                    }
                    else
                    { 
                        phase = AbilityActivePhase.ActivePhase.Other_Draw;
                    }
                }
                else if (battlePhase >= BattlePhase.BeginPreBattle && battlePhase <= BattlePhase.EndPreBattle)
                {
                    if (OwnerPlayerIndex == activePlayerIndex)
                    {
                        phase = AbilityActivePhase.ActivePhase.Owner_PreBattle;
                    }
                    else
                    { 
                        phase = AbilityActivePhase.ActivePhase.Other_PreBattle;
                    }
                }
                else if (battlePhase >= BattlePhase.BeginBattle && battlePhase <= BattlePhase.EndBattle)
                {
                    if (OwnerPlayerIndex == activePlayerIndex)
                    {
                        phase = AbilityActivePhase.ActivePhase.Owner_Battle;
                    }
                    else
                    { 
                        phase = AbilityActivePhase.ActivePhase.Other_Battle;
                    }
                }
                else if (battlePhase >= BattlePhase.BeginPostBattle && battlePhase <= BattlePhase.EndPostBattle)
                {
                    if (OwnerPlayerIndex == activePlayerIndex)
                    {
                        phase = AbilityActivePhase.ActivePhase.Owner_PostBattle;
                    }
                    else
                    { 
                        phase = AbilityActivePhase.ActivePhase.Other_PostBattle;
                    }
                }
                else if (battlePhase >= BattlePhase.BeginEnd && battlePhase <= BattlePhase.EndEnd)
                {
                    if (OwnerPlayerIndex == activePlayerIndex)
                    {
                        phase = AbilityActivePhase.ActivePhase.Owner_End;
                    }
                    else
                    { 
                        phase = AbilityActivePhase.ActivePhase.Other_End;
                    }
                }
                else if (battlePhase >= BattlePhase.BeginCleanUp && battlePhase <= BattlePhase.EndCleanUp)
                {
                    if (OwnerPlayerIndex == activePlayerIndex)
                    {
                        phase = AbilityActivePhase.ActivePhase.Other_CleanUp;
                    }
                    else
                    { 
                        phase = AbilityActivePhase.ActivePhase.Owner_CleanUp;
                    }
                }
                else
                {
                    return false;
                }

                return (_data.ActivePhase.IsPhase(phase));
            }

            return false;
        }

        bool IsCompleteCondition()
        {
            bool result = BoolParser.Condition(_data.Condition, this);
            if (result)
            {
                foreach(AbilityParams param in _conditionList)
                {
                    bool isSuccess = CardAbility.AbilityParams.ParamToBool(param, this, out result);
                    if (isSuccess)
                    {
                        if (!result)
                        { 
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            return result;
        }

        bool IsHaveTarget()
        {
            switch(_data.TargetType)
            {
                case Ability2Data.AbilityTargetType.None: return true;
                case Ability2Data.AbilityTargetType.Player:
                {
                    CardAbility.AbilityParams param = new CardAbility.AbilityParams(_data.Target);

                    PlayerIndex result;
                    bool isSuccess = CardAbility.AbilityParams.ParamToPlayerIndex(param, this, out result);
                    if (isSuccess)
                    {
                        return true;
                    }
                }
                break;

                case Ability2Data.AbilityTargetType.BattleCard:
                {
                    List<BattleCardData> result;
                    bool isSuccess = CardAbility.AbilityParams.TextToBattleCardList(_data.Target, this, out result);
                    if (isSuccess)
                    {
                        if (result != null && result.Count > 0)
                        {
                            return true;
                        }
                    }
                }
                break;

                case Ability2Data.AbilityTargetType.UniqueCard:
                {
                    List<UniqueCardData> result;
                    bool isSuccess = CardAbility.AbilityParams.TextToUniqueCardList(_data.Target, this, out result);
                    if (isSuccess)
                    {
                        if (result != null && result.Count > 0)
                        {
                            return true;
                        }
                    }
                }
                break;
            }

            return false;
        }

        public bool GetTarget(out List<BattleCardData> targetList)
        {
            switch(_data.TargetType)
            {
                case Ability2Data.AbilityTargetType.BattleCard:
                {
                    bool isSuccess = CardAbility.AbilityParams.TextToBattleCardList(_data.Target, this, out targetList);
                    if (isSuccess)
                    {
                        return true;
                    }
                }
                break;
            }

            targetList = null;
            return false;
        }

        public bool GetTarget(out List<UniqueCardData> targetList)
        {
            switch(_data.TargetType)
            {
                case Ability2Data.AbilityTargetType.UniqueCard:
                {
                    bool isSuccess = CardAbility.AbilityParams.TextToUniqueCardList(_data.Target, this, out targetList);
                    if (isSuccess)
                    {
                        return true;
                    }
                }
                break;
            }

            targetList = null;
            return false;
        }
        #endregion

        #region Event Methods
        public void OnEventTrigger()
        {
            Active();
        }

        /*
        public void OnActiveEvent()
        {
            if (_activeCallback != null)
            {
                _activeCallback.Invoke();
            }
        }

        public void OnUpdateEvent()
        {
            if (_updateCallback != null)
            {
                _updateCallback.Invoke();
            }
        }

        public void OnDestroyEvent()
        {
            if (_destroyCallback != null)
            {
                _destroyCallback.Invoke();
            }
        }

        public void BindActiveEvent(ActiveCallback callback)
        {
            _activeCallback += callback;
        }

        public void UnbindActiveEvent(ActiveCallback callback)
        {
            _activeCallback -= callback;
        }

        public void ClearActiveEvent()
        {
            _activeCallback = null;
        }

        public void BindUpdateEvent(UpdateCallback callback)
        {
            _updateCallback += callback;
        }

        public void UnbindUpdateEvent(UpdateCallback callback)
        {
            _updateCallback -= callback;
        }

        public void ClearUpdateEvent()
        {
            _updateCallback = null;
        }

        public void BindDestroyEvent(UpdateCallback callback)
        {
            _destroyCallback += callback;
        }

        public void UnbindDestroyEvent(UpdateCallback callback)
        {
            _destroyCallback -= callback;
        }

        public void ClearDestroyEvent()
        {
            _destroyCallback = null;
        }

        public void ClearAllCallback()
        {
            ClearActiveEvent();
            ClearUpdateEvent();
            ClearDestroyEvent();
        }
        */
        #endregion

        #region Static Methods
        public static bool CreateAbility(string abilityID, UniqueCardData uniqueCard, int abilityIndex, out Ability ability)
        {
            Ability2Data data;
            bool isSuccess = Ability2Data.CreateData(abilityID, out data);
            if (isSuccess)
            {
                ability = new Ability(abilityIndex, data, uniqueCard);
                bool isInit = ability.Init();

                if (isInit)
                {
                    return true;
                }
            }

            ability = null;
            return false;
        }

        public static bool CreateAbility(string abilityID, BattleCardData battleCard, int abilityIndex, out Ability ability)
        {
            Ability2Data data;
            bool isSuccess = Ability2Data.CreateData(abilityID, out data);
            if (isSuccess)
            {
                ability = new Ability(abilityIndex, data, battleCard);
                bool isInit = ability.Init();

                if (isInit)
                {
                    return true;
                }
            }

            ability = null;
            return false;
        }
        #endregion
    }

    /*
    public class BuffAbility : Ability
    {
        
    }
    */
    #endregion
}
    
   
