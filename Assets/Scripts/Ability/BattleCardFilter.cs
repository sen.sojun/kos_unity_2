﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#region BattleCardFilter Class
public abstract class BattleCardFilter
{
    #region Protected Properties
    protected CardAbility.Ability _ability;
    #endregion

    #region Public Properties
    public CardAbility.Ability Ability { get { return _ability; } }
    #endregion

    #region Constructors
    protected BattleCardFilter( CardAbility.Ability ability)
    {
        _ability = ability;
    }
    #endregion

    #region Methods
    public virtual bool FilterResult(BattleCardData card)
    {
        return true;
    }
    #endregion

    public static bool CreateBattleCardFilter(CardAbility.AbilityParams abParam , CardAbility.Ability ability, out BattleCardFilter result)
    {
        result = null;

        switch(abParam.Key.ToUpper())
        {
            case "TYPE":        result = new BattleCardTypeFilter(abParam, ability); break;
            case "SUBTYPE":     result = new BattleCardSubTypeFilter(abParam, ability); break;
            case "JOB":         result = new BattleCardJobFilter(abParam, ability); break;
            case "CLAN":        result = new BattleCardClanFilter(abParam, ability); break;
            case "SYMBOL":      result = new BattleCardSymbolFilter(abParam, ability); break;
            case "ME":          result = new BattleCardOwnerFilter(abParam, ability); break;
            case "OWNER":       result = new BattleCardOwnerFilter(abParam, ability); break;
            case "ZONE":        result = new BattleCardZoneFilter(abParam, ability); break;
            case "SELECTED":    result = new BattleCardSelectedFilter(abParam, ability); break;
        }

        if (result != null)
        {
            return true;
        }

        return false;
    }

    /*
    public static List<BattleCardData> GetBattleCard(CardAbility.AbilityParams abParam, CardAbility.Ability ability)
    {
        List<BattleCardData> result = new List<BattleCardData>();

        if(BattleManager.Instance != null)
        {
            Equation equa = Equation.BoolEquaTextToVar(abParam.ParamsList[0]);
            List<BattleCardData> cardList;
            BattleManager.Instance.GetAllBattleCard(out cardList);

            if (cardList.Count > 0)
            {
                foreach (BattleCardData card in cardList)
                {
                    Stack<bool> stack = new Stack<bool>();
                    for(int index = 0; index < equa.VarList.Count; ++index)
                    {
                        Equation.EquationVar equaVar = equa.VarList[index];

                        if (equaVar.Type == Equation.EquationVar.VarType.Value)
                        {
                            // Value
                            CardAbility.AbilityParams abParam_0 = new CardAbility.AbilityParams(equa.VarList[index].Value);
                            BattleCardFilter filter;
                            CardAbility.AbilityParams.ParamToBattleCardFilter(abParam_0, ability, out filter);

                            stack.Push(filter.FilterResult(card));
                        }
                        else
                        {
                            // Op
                            switch(equaVar.Value)
                            {
                                case "!":
                                {
                                    bool v1 = stack.Pop();
                                    stack.Push(!v1);
                                }
                                break;

                                case "&":
                                {
                                    bool v1 = stack.Pop();
                                    bool v2 = stack.Pop();
                                    stack.Push(v1 && v2);
                                }
                                break;

                                case "|":
                                {
                                    bool v1 = stack.Pop();
                                    bool v2 = stack.Pop();
                                    stack.Push(v1 || v2);
                                }
                                break;
                            }
                        }
                    }

                    bool isCandidate = stack.Pop();
                   
                    if (isCandidate)
                    {
                        result.Add(card);
                    }
                }
            }
        }
        return result;
    }
    */
}
#endregion

#region BattleCardTypeFilter Class
public class BattleCardTypeFilter : BattleCardFilter
{
    #region Private Properties
    private CardTypeData.CardType _cardType;
    #endregion

    #region Public Properties
    public CardTypeData.CardType CardType { get { return _cardType; } }
    #endregion

    #region Constructors
    public BattleCardTypeFilter(CardTypeData.CardType cardType, CardAbility.Ability ability) : base(ability)
    {
        _cardType = cardType;
    }

    public BattleCardTypeFilter(CardAbility.AbilityParams abParam, CardAbility.Ability ability) : base(ability)
    {
        CardAbility.AbilityParams abParam_0 = new CardAbility.AbilityParams(abParam.ParamsList[0]);
        CardAbility.AbilityParams.ParamToCardType(abParam_0, _ability, out _cardType);
    }
    #endregion

    #region Methods
    public override bool FilterResult(BattleCardData card)
    {
        return card.IsCardType(CardType);
    }
    #endregion
}
#endregion

#region BattleCardSubTypeFilter Class
public class BattleCardSubTypeFilter : BattleCardFilter
{
    #region Private Properties
    private CardSubTypeData.CardSubType _cardSubType;
    #endregion

    #region Public Properties
    public CardSubTypeData.CardSubType CardSubType { get { return _cardSubType; } }
    #endregion

    #region Constructors
    public BattleCardSubTypeFilter(CardSubTypeData.CardSubType cardSubType, CardAbility.Ability ability) : base(ability)
    {
        _cardSubType = cardSubType;
    }

    public BattleCardSubTypeFilter(CardAbility.AbilityParams abParam, CardAbility.Ability ability) : base(ability)
    {
        CardAbility.AbilityParams abParam_0 = new CardAbility.AbilityParams(abParam.ParamsList[0]);
        CardAbility.AbilityParams.ParamToCardSubType(abParam_0, _ability, out _cardSubType);
    }
    #endregion

    #region Methods
    public override bool FilterResult(BattleCardData card)
    {
        return card.IsCardSubType(CardSubType);
    }
    #endregion
}
#endregion

#region BattleCardJobFilter Class
public class BattleCardJobFilter : BattleCardFilter
{
    #region Private Properties
    private CardJobData.JobType _job;
    #endregion

    #region Public Properties
    public CardJobData.JobType Job { get { return _job; } }
    #endregion

    #region Constructors
    public BattleCardJobFilter(CardJobData.JobType job, CardAbility.Ability ability) : base(ability)
    {
        _job = job;
    }

    public BattleCardJobFilter(CardAbility.AbilityParams abParam, CardAbility.Ability ability) : base(ability)
    {
        CardAbility.AbilityParams abParam_0 = new CardAbility.AbilityParams(abParam.ParamsList[0]);
        CardAbility.AbilityParams.ParamToCardJob(abParam_0, _ability, out _job);
    }
    #endregion

    #region Methods
    public override bool FilterResult(BattleCardData card)
    {
        return card.IsCardJobType(Job);
    }
    #endregion
}
#endregion

#region BattleCardClanFilter Class
public class BattleCardClanFilter : BattleCardFilter
{
    #region Private Properties
    private CardClanData.CardClanType _clan;
    #endregion

    #region Public Properties
    public CardClanData.CardClanType Clan { get { return _clan; } }
    #endregion

    #region Constructors
    public BattleCardClanFilter(CardClanData.CardClanType clan, CardAbility.Ability ability) : base(ability)
    {
        _clan = clan;
    }

    public BattleCardClanFilter(CardAbility.AbilityParams abParam, CardAbility.Ability ability) : base(ability)
    {
        CardAbility.AbilityParams abParam_0 = new CardAbility.AbilityParams(abParam.ParamsList[0]);
        CardAbility.AbilityParams.ParamToCardClan(abParam_0, _ability, out _clan);
    }
    #endregion

    #region Methods
    public override bool FilterResult(BattleCardData card)
    {
        return card.IsCardClan(Clan);
    }
    #endregion
}
#endregion

#region BattleCardSymbolFilter Class
public class BattleCardSymbolFilter : BattleCardFilter
{
    #region Private Properties
    private CardSymbolData.CardSymbol _symbol;
    #endregion

    #region Public Properties
    public CardSymbolData.CardSymbol Symbol { get { return _symbol; } }
    #endregion

    #region Constructors 
    public BattleCardSymbolFilter(CardSymbolData.CardSymbol symbol, CardAbility.Ability ability) : base(ability)
    {
        _symbol = symbol;
    }

    public BattleCardSymbolFilter(CardAbility.AbilityParams abParam, CardAbility.Ability ability) : base(ability)
    {
        CardAbility.AbilityParams abParam_0 = new CardAbility.AbilityParams(abParam.ParamsList[0]);
        CardAbility.AbilityParams.ParamToCardSymbol(abParam_0, _ability, out _symbol);
    }
    #endregion

    #region Methods
    public override bool FilterResult(BattleCardData card)
    {
        return card.IsCardSymbol(Symbol);
    }
    #endregion
}
#endregion

#region BattleCardMeFilter Class
public class BattleCardMeFilter : BattleCardFilter
{
    #region Constructors
    public BattleCardMeFilter(CardAbility.Ability ability) : base(ability)
    {
    }

    public BattleCardMeFilter(CardAbility.AbilityParams abParam, CardAbility.Ability ability) : base(ability)
    {
    }
    #endregion

    #region Methods
    public override bool FilterResult(BattleCardData card)
    {
        if (this.Ability.BattleCard.BattleCardID == card.BattleCardID)
            return true;
        else
            return false;
    }
    #endregion
}
#endregion

#region BattleCardOwnerFilter Class
public class BattleCardOwnerFilter : BattleCardFilter
{
    #region Private Properties
    private LocalPlayerIndex _playerIndex;
    #endregion

    #region Public Properties
    public LocalPlayerIndex Index { get { return _playerIndex; } }
    #endregion

    #region Constructors
    public BattleCardOwnerFilter(LocalPlayerIndex playerIndex, CardAbility.Ability ability) : base(ability)
    {
        _playerIndex = playerIndex;
    }

    public BattleCardOwnerFilter(CardAbility.AbilityParams abParam, CardAbility.Ability ability) : base(ability)
    {
        CardAbility.AbilityParams abParam_0 = new CardAbility.AbilityParams(abParam.ParamsList[0]);
        CardAbility.AbilityParams.ParamToLocalPlayerIndex(abParam_0, _ability, out _playerIndex);
    }
    #endregion

    #region Methods
    public override bool FilterResult(BattleCardData card)
    {
        switch(Index)
        {
            case LocalPlayerIndex.Owner: return (card.IsCardOwner(Ability.OwnerPlayerIndex));
            case LocalPlayerIndex.Enemy: return (!card.IsCardOwner(Ability.OwnerPlayerIndex));
        }
        return false;
    }
    #endregion
}
#endregion

#region BattleCardZoneFilter Class
public class BattleCardZoneFilter : BattleCardFilter
{
    #region Private Properties
    private BattleZoneIndex _battleZone;
    #endregion

    #region Public Properties
    public BattleZoneIndex BattleZone { get { return _battleZone; } }
    #endregion

    #region Constructors
    public BattleCardZoneFilter(BattleZoneIndex battleZone, CardAbility.Ability ability) : base(ability)
    {
        _battleZone = battleZone;
    }

    public BattleCardZoneFilter(CardAbility.AbilityParams abParam, CardAbility.Ability ability) : base(ability)
    {
        CardAbility.AbilityParams abParam_0 = new CardAbility.AbilityParams(abParam.ParamsList[0]);
        CardAbility.AbilityParams.ParamToBattleZone(abParam_0, _ability, out _battleZone);
    }
    #endregion

    #region Methods
    public override bool FilterResult(BattleCardData card)
    {
        if (card != null)
        {
            return (card.CurrentZone == this.BattleZone);
        }

        return false;
    }
    #endregion
}
#endregion

#region BattleCardSelectedFilter Class
public class BattleCardSelectedFilter : BattleCardFilter
{
    #region Public Properties
    public List<int> CardIDList { get { return CardAbility.AESelectBattleCard.SelectBattleCardList; } }
    #endregion

    #region Constructors
    public BattleCardSelectedFilter(CardAbility.AbilityParams abParam, CardAbility.Ability ability) : base(ability)
    {
    }
    #endregion

    #region Methods
    public override bool FilterResult(BattleCardData card)
    {
        if (card != null)
        {
            foreach(int id in CardIDList)
            {
                if (card.BattleCardID == id)
                {
                    return true;
                }
            }
        }

        return false;
    }
    #endregion
}
#endregion