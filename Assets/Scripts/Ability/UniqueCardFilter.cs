﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#region UniqueCardFilter Class
public abstract class UniqueCardFilter
{
    #region Protected Properties
    protected CardAbility.Ability _ability;
    #endregion

    #region Public Properties
    public CardAbility.Ability Ability { get { return _ability; } }
    #endregion

    #region Constructors
    protected UniqueCardFilter( CardAbility.Ability ability)
    {
        _ability = ability;
    }
    #endregion

    #region Methods
    public virtual bool FilterResult(UniqueCardData card)
    {
        return true;
    }
    #endregion

    public static bool CreateUniqueCardFilter(CardAbility.AbilityParams abParam , CardAbility.Ability ability, out UniqueCardFilter result)
    {
        result = null;

        switch(abParam.Key.ToUpper())
        {
            case "TYPE":        result = new UniqueCardTypeFilter(abParam, ability); break;
            case "SUBTYPE":     result = new UniqueCardSubTypeFilter(abParam, ability); break;
            case "JOB":         result = new UniqueCardJobFilter(abParam, ability); break;
            case "CLAN":        result = new UniqueCardClanFilter(abParam, ability); break;
            case "SYMBOL":      result = new UniqueCardSymbolFilter(abParam, ability); break;
            case "OWNER":       result = new UniqueCardOwnerFilter(abParam, ability); break;
            case "ZONE":        result = new UniqueCardZoneFilter(abParam, ability); break;
            case "AIRUNIT":     result = new UniqueCardFlyFilter(abParam, ability); break;
            case "KNOCKBACK":   result = new UniqueCardKnockbackFilter(abParam, ability); break;
            case "PIERCING":    result = new UniqueCardPiercingFilter(abParam, ability); break;

        }

        if (result != null)
        {
            return true;
        }

        return false;
    }

    /*
    public static List<UniqueCardData> GetUniqueCard(CardAbility.AbilityParams abParam, CardAbility.Ability ability)
    {
        List<UniqueCardData> result = new List<UniqueCardData>();

        if(BattleManager.Instance != null)
        {
            Equation equa = Equation.BoolEquaTextToVar(abParam.ParamsList[0]);
            List<UniqueCardData> cardList;
            BattleManager.Instance.GetAllUniqueCard(out cardList);

            if (cardList.Count > 0)
            {
                foreach (UniqueCardData card in cardList)
                {
                    Stack<bool> stack = new Stack<bool>();
                    for(int index = 0; index < equa.VarList.Count; ++index)
                    {
                        Equation.EquationVar equaVar = equa.VarList[index];

                        if (equaVar.Type == Equation.EquationVar.VarType.Value)
                        {
                            // Value
                            CardAbility.AbilityParams abParam_0 = new CardAbility.AbilityParams(equa.VarList[index].Value);
                            UniqueCardFilter filter;
                            CardAbility.AbilityParams.ParamToUniqueCardFilter(abParam_0, ability, out filter);

                            stack.Push(filter.FilterResult(card));
                        }
                        else
                        {
                            // Op
                            switch(equaVar.Value)
                            {
                                case "!":
                                    {
                                        bool v1 = stack.Pop();
                                        stack.Push(!v1);
                                    }
                                    break;

                                case "&":
                                    {
                                        bool v1 = stack.Pop();
                                        bool v2 = stack.Pop();
                                        stack.Push(v1 && v2);
                                    }
                                    break;

                                case "|":
                                    {
                                        bool v1 = stack.Pop();
                                        bool v2 = stack.Pop();
                                        stack.Push(v1 || v2);
                                    }
                                    break;
                            }
                        }
                    }

                    bool isCandidate = stack.Pop();

                    if (isCandidate)
                    {
                        result.Add(card);
                    }
                }
            }
        }
        return result;
    }
    */
}
#endregion

#region UniqueCardTypeFilter Class
public class UniqueCardTypeFilter : UniqueCardFilter
{
    #region Private Properties
    private CardTypeData.CardType _cardType;
    #endregion

    #region Public Properties
    public CardTypeData.CardType CardType { get { return _cardType; } }
    #endregion

    #region Constructors
    public UniqueCardTypeFilter(CardTypeData.CardType cardType, CardAbility.Ability ability) : base(ability)
    {
        _cardType = cardType;
    }

    public UniqueCardTypeFilter(CardAbility.AbilityParams abParam, CardAbility.Ability ability) : base(ability)
    {
        CardAbility.AbilityParams abParam_0 = new CardAbility.AbilityParams(abParam.ParamsList[0]);
        CardAbility.AbilityParams.ParamToCardType(abParam_0, _ability, out _cardType);
    }
    #endregion

    #region Methods
    public override bool FilterResult(UniqueCardData card)
    {
        return card.PlayerCardData.IsCardType(CardType);
    }
    #endregion
}
#endregion

#region UniqueCardSubTypeFilter Class
public class UniqueCardSubTypeFilter : UniqueCardFilter
{
    #region Private Properties
    private CardSubTypeData.CardSubType _cardSubType;
    #endregion

    #region Public Properties
    public CardSubTypeData.CardSubType CardSubType { get { return _cardSubType; } }
    #endregion

    #region Constructors
    public UniqueCardSubTypeFilter(CardSubTypeData.CardSubType cardSubType, CardAbility.Ability ability) : base(ability)
    {
        _cardSubType = cardSubType;
    }

    public UniqueCardSubTypeFilter(CardAbility.AbilityParams abParam, CardAbility.Ability ability) : base(ability)
    {
        CardAbility.AbilityParams abParam_0 = new CardAbility.AbilityParams(abParam.ParamsList[0]);
        CardAbility.AbilityParams.ParamToCardSubType(abParam_0, _ability, out _cardSubType);
    }
    #endregion

    #region Methods
    public override bool FilterResult(UniqueCardData card)
    {
        return card.PlayerCardData.IsCardSubType(CardSubType);
    }
    #endregion
}
#endregion

#region UniqueCardJobFilter Class
public class UniqueCardJobFilter : UniqueCardFilter
{
    #region Private Properties
    private CardJobData.JobType _job;
    #endregion

    #region Public Properties
    public CardJobData.JobType Job { get { return _job; } }
    #endregion

    #region Constructors
    public UniqueCardJobFilter(CardJobData.JobType job, CardAbility.Ability ability) : base(ability)
    {
        _job = job;
    }

    public UniqueCardJobFilter(CardAbility.AbilityParams abParam, CardAbility.Ability ability) : base(ability)
    {
        CardAbility.AbilityParams abParam_0 = new CardAbility.AbilityParams(abParam.ParamsList[0]);
        CardAbility.AbilityParams.ParamToCardJob(abParam_0, _ability, out _job);
    }
    #endregion

    #region Methods
    public override bool FilterResult(UniqueCardData card)
    {
        return card.PlayerCardData.IsCardJobType(Job);
    }
    #endregion
}
#endregion

#region UniqueCardClanFilter Class
public class UniqueCardClanFilter : UniqueCardFilter
{
    #region Private Properties
    private CardClanData.CardClanType _clan;
    #endregion

    #region Public Properties
    public CardClanData.CardClanType Clan { get { return _clan; } }
    #endregion

    #region Constructors
    public UniqueCardClanFilter(CardClanData.CardClanType clan, CardAbility.Ability ability) : base(ability)
    {
        _clan = clan;
    }

    public UniqueCardClanFilter(CardAbility.AbilityParams abParam, CardAbility.Ability ability) : base(ability)
    {
        CardAbility.AbilityParams abParam_0 = new CardAbility.AbilityParams(abParam.ParamsList[0]);
        CardAbility.AbilityParams.ParamToCardClan(abParam_0, _ability, out _clan);
    }
    #endregion

    #region Methods
    public override bool FilterResult(UniqueCardData card)
    {
        return card.PlayerCardData.IsCardClan(Clan);
    }
    #endregion
}
#endregion

#region UniqueCardSymbolFilter Class
public class UniqueCardSymbolFilter : UniqueCardFilter
{
    #region Private Properties
    private CardSymbolData.CardSymbol _symbol;
    #endregion

    #region Public Properties
    public CardSymbolData.CardSymbol Symbol { get { return _symbol; } }
    #endregion

    #region Constructors 
    public UniqueCardSymbolFilter(CardSymbolData.CardSymbol symbol, CardAbility.Ability ability) : base(ability)
    {
        _symbol = symbol;
    }

    public UniqueCardSymbolFilter(CardAbility.AbilityParams abParam, CardAbility.Ability ability) : base(ability)
    {
        CardAbility.AbilityParams abParam_0 = new CardAbility.AbilityParams(abParam.ParamsList[0]);
        CardAbility.AbilityParams.ParamToCardSymbol(abParam_0, _ability, out _symbol);
    }
    #endregion

    #region Methods
    public override bool FilterResult(UniqueCardData card)
    {
        return card.PlayerCardData.IsCardSymbol(Symbol);
    }
    #endregion
}
#endregion

#region UniqueCardOwnerFilter Class
public class UniqueCardOwnerFilter : UniqueCardFilter
{
    #region Private Properties
    private LocalPlayerIndex _playerIndex;
    #endregion

    #region Public Properties
    public LocalPlayerIndex Index { get { return _playerIndex; } }
    #endregion

    #region Constructors
    public UniqueCardOwnerFilter(LocalPlayerIndex playerIndex, CardAbility.Ability ability) : base(ability)
    {
        _playerIndex = playerIndex;
    }

    public UniqueCardOwnerFilter(CardAbility.AbilityParams abParam, CardAbility.Ability ability) : base(ability)
    {
        CardAbility.AbilityParams abParam_0 = new CardAbility.AbilityParams(abParam.ParamsList[0]);
        CardAbility.AbilityParams.ParamToLocalPlayerIndex(abParam_0, _ability, out _playerIndex);
    }
    #endregion

    #region Methods
    public override bool FilterResult(UniqueCardData card)
    {
        switch(Index)
        {
            case LocalPlayerIndex.Owner: return (card.IsCardOwner(Ability.OwnerPlayerIndex));
            case LocalPlayerIndex.Enemy: return (!card.IsCardOwner(Ability.OwnerPlayerIndex));
        }
        return false;
    }
    #endregion
}
#endregion

#region UniqueCardZoneFilter Class
public class UniqueCardZoneFilter : UniqueCardFilter
{
    #region Private Properties
    private CardZoneIndex _cardZone;
    #endregion

    #region Public Properties
    public CardZoneIndex CardZone { get { return _cardZone; } }
    #endregion

    #region Constructors
    public UniqueCardZoneFilter(CardZoneIndex cardZoneIndex, CardAbility.Ability ability) : base(ability)
    {
        _cardZone = cardZoneIndex;
    }

    public UniqueCardZoneFilter(CardAbility.AbilityParams abParam, CardAbility.Ability ability) : base(ability)
    {
        CardAbility.AbilityParams abParam_0 = new CardAbility.AbilityParams(abParam.ParamsList[0]);
        CardAbility.AbilityParams.ParamToCardZone(abParam_0, _ability, out _cardZone);
    }
    #endregion

    #region Methods
    public override bool FilterResult(UniqueCardData card)
    {
        if (card != null)
        {
            return (card.IsCardZone(this.CardZone));
        }

        return false;
    }
    #endregion
}
#endregion

#region UniqueCardFlyFilter Class
public class UniqueCardFlyFilter : UniqueCardFilter
{
    #region Constructors
    public UniqueCardFlyFilter(CardAbility.Ability ability) : base(ability)
    {
    }

    public UniqueCardFlyFilter(CardAbility.AbilityParams abParam, CardAbility.Ability ability) : base(ability)
    {
    }
    #endregion

    #region Methods
    public override bool FilterResult(UniqueCardData card)
    {
        if (card != null)
        {
            return (card.PlayerCardData.IsAirUnit());
        }

        return false;
    }
    #endregion
}
#endregion

#region UniqueCardKnockbackFilter Class
public class UniqueCardKnockbackFilter : UniqueCardFilter
{
    #region Constructors
    public UniqueCardKnockbackFilter(CardAbility.Ability ability) : base(ability)
    {
    }

    public UniqueCardKnockbackFilter(CardAbility.AbilityParams abParam, CardAbility.Ability ability) : base(ability)
    {
    }
    #endregion

    #region Methods
    public override bool FilterResult(UniqueCardData card)
    {
        if (card != null)
        {
            return (card.PlayerCardData.IsKnockback());
        }

        return false;
    }
    #endregion
}
#endregion

#region UniqueCardFlyFilter Class
public class UniqueCardPiercingFilter : UniqueCardFilter
{
    #region Constructors
    public UniqueCardPiercingFilter(CardAbility.Ability ability) : base(ability)
    {
    }

    public UniqueCardPiercingFilter(CardAbility.AbilityParams abParam, CardAbility.Ability ability) : base(ability)
    {
    }
    #endregion

    #region Methods
    public override bool FilterResult(UniqueCardData card)
    {
        if (card != null)
        {
            return (card.PlayerCardData.IsPiercing());
        }

        return false;
    }
    #endregion
}
#endregion

#region UniqueCardSelectedFilter Class
public class UniqueCardSelectedFilter : UniqueCardFilter
{
    #region Public Properties
    public List<int> CardIDList { get { return CardAbility.AESelectUniqueCard.SelectUniqueCardList; } }
    #endregion

    #region Constructors
    public UniqueCardSelectedFilter(CardAbility.AbilityParams abParam, CardAbility.Ability ability) : base(ability)
    {
    }
    #endregion

    #region Methods
    public override bool FilterResult(UniqueCardData card)
    {
        if (card != null)
        {
            foreach(int id in CardIDList)
            {
                if (card.UniqueCardID == id)
                {
                    return true;
                }
            }
        }

        return false;
    }
    #endregion
}
#endregion