﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardAbility
{
    #region AbilityCost Class
    public abstract class AbilityCost
    {
        #region Protected Properties
        protected Ability _ability;
        protected AbilityParams _abParams;
        #endregion

        #region Public Properties
        public Ability Ability { get { return _ability; } }
        public AbilityParams AbParams { get { return _abParams; } }
        #endregion

        #region Constructors
        protected AbilityCost(AbilityParams abParams, Ability ability)
        {
            _ability = ability;
            _abParams = abParams;
        }
        #endregion

        #region Methods
        public virtual List<AbilityParams> GenerateCondition()
        {
            return null;
        }

        public virtual List<AbilityEffect> GenerateEffect()
        {
            return null;
        }

        public static bool CreateAbilityCost(AbilityParams abParams, Ability ability, out AbilityCost result)
        {
            result = null;

            switch (abParams.Key)
            {
                case "ACT":     result = new CostAct(abParams, ability); break; 
                case "DISCARD": result = new CostDiscard(abParams, ability); break;
                    
            }

            if (result != null)
            {
                return true;
            }

            return false;
        }

        /*
        protected bool GetPlayer(int paramIndex, out PlayerIndex playerIndex) 
        {   
            if (AbParams != null && AbParams.ParamsList != null && AbParams.ParamsList.Count > paramIndex)
            {
                AbilityParams param = new AbilityParams(AbParams.ParamsList[paramIndex]);

                bool isSuccess = AbilityParams.ParamToPlayerIndex(param, this.Ability, out playerIndex);
                if (isSuccess)
                {
                    return true;
                }
            }

            playerIndex = PlayerIndex.One;
            return false; 
        }

        protected bool GetInt(int paramIndex, out int number) 
        {   
            if (AbParams != null && AbParams.ParamsList != null && AbParams.ParamsList.Count > paramIndex)
            {
                AbilityParams param = new AbilityParams(AbParams.ParamsList[paramIndex]);

                bool isSuccess = AbilityParams.ParamToInt(param, this.Ability, out number);
                if (isSuccess)
                {
                    return true;
                }
            }

            number = 0;
            return false; 
        }

        protected bool GetBattleCard(int paramIndex, out List<BattleCardData> battleCardList)
        {
            if (AbParams != null && AbParams.ParamsList != null && AbParams.ParamsList.Count > paramIndex)
            {
                Equation equa = Equation.BoolEquaTextToVar(_abParams.ParamsList[paramIndex], Equation.EquationType.BattleCardFilter);
                battleCardList = equa.GetBattleCard(this.Ability);

                return true;
            }

            battleCardList = new List<BattleCardData>();
            return false;
        }

        protected bool GetUniqueCard(int paramIndex, out List<UniqueCardData> uniqueCardDataList)
        {
            if (AbParams != null && AbParams.ParamsList != null && AbParams.ParamsList.Count > paramIndex)
            {
                Equation equa = Equation.BoolEquaTextToVar(_abParams.ParamsList[paramIndex], Equation.EquationType.UniqueCardFilter);
                uniqueCardDataList = equa.GetUniqueCard(this.Ability);

                return true;
            }

            uniqueCardDataList = new List<UniqueCardData>();
            return false;
        }

        protected bool GetCondition(int paramIndex, out bool result)
        {
            if (AbParams != null && AbParams.ParamsList.Count > paramIndex)
            {
                Equation equa = Equation.BoolEquaTextToVar(_abParams.ParamsList[paramIndex], Equation.EquationType.Bool);
                result = equa.GetBool(this.Ability);

                return true;
            }

            result = false;
            return false;
        }

        protected bool GetEffect(int paramIndex, out AbilityEffect effect)
        {
            if (AbParams != null && AbParams.ParamsList != null && AbParams.ParamsList.Count > paramIndex)
            {
                AbilityParams param = new AbilityParams(AbParams.ParamsList[paramIndex]);
                effect = AbilityEffect.GenerateEffect(param, this.Ability);
                if (effect != null)
                {
                    return true;
                }
            }

            effect = null;
            return false;
        }
        */
        #endregion
    }
    #endregion

    #region CostAct Class
    public class CostAct : AbilityCost
    {
        #region Constructors
        public CostAct(AbilityParams abParams, Ability ability) : base(abParams, ability)
        {
        }
        #endregion

        #region Methods
        public override List<AbilityParams> GenerateCondition()
        {
            List<AbilityParams> conditionList = new List<AbilityParams>();

            string abilityText = string.Format(
                  "ISCANACT({0})"
                , AbParams.ParamsList[0]
            );
            AbilityParams param_0 = new AbilityParams(abilityText);

            conditionList.Add(param_0);      

            return conditionList;
        }

        public override List<AbilityEffect> GenerateEffect()
        {
            List<AbilityEffect> effectList = new List<AbilityEffect>();

            string abilityText = string.Format(
                  "ACT({0})"
                , AbParams.ParamsList[0]
            );
            AbilityParams param_0 = new AbilityParams(abilityText);

            effectList.Add(new AEAct(param_0, this.Ability));

            return effectList;
        }
        #endregion
    }
    #endregion

    #region CostDiscard Class
    public class CostDiscard : AbilityCost
    {
        #region Constructors
        public CostDiscard(AbilityParams abParams, Ability ability) : base(abParams, ability)
        {
        }
        #endregion

        #region Methods
        public override List<AbilityParams> GenerateCondition()
        {
            List<AbilityParams> conditionList = new List<AbilityParams>();

            string abilityText = string.Format(
                  "ISHANDMOREEQUAL(OWNER(OWNER), {0})"
                , AbParams.ParamsList[0]
            );
            AbilityParams param_0 = new AbilityParams(abilityText);

            conditionList.Add(param_0);

            return conditionList;
        }

        public override List<AbilityEffect> GenerateEffect()
        {
            List<AbilityEffect> effectList = new List<AbilityEffect>();

            string abilityText = string.Format(
                  "DISCARD(OWNER(OWNER), {0})"
                , AbParams.ParamsList[0]
            );
            AbilityParams param_0 = new AbilityParams(abilityText);

            effectList.Add(new AEDiscard(param_0, this.Ability));
            return effectList;
        }
        #endregion
    }
    #endregion


}
