﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardAbility
{
    public class AbilityActivePhase 
    {
        #region Enums
        public enum ActivePhase : int
        {
              Owner_Begin          = 0
            , Owner_PostBegin
            , Owner_Draw
            , Owner_PreBattle
            , Owner_Battle
            , Owner_PostBattle
            , Owner_End
            , Owner_CleanUp

            , Other_Begin     
            , Other_PostBegin
            , Other_Draw
            , Other_PreBattle
            , Other_Battle
            , Other_PostBattle
            , Other_End
            , Other_CleanUp
        }
        #endregion

        #region Private Properties
        private int _activePhase = 0;
        #endregion

        #region Constructors
        public AbilityActivePhase(int activePhase)
        {
            _activePhase = activePhase;
        }
        #endregion

        #region Methods
        public void AddPhase(AbilityActivePhase.ActivePhase phase)
        {
            _activePhase |= (1 << (int)phase);
        }

        public bool IsPhase(AbilityActivePhase.ActivePhase phase)
        {
            int result = (_activePhase & (int)phase);
            return (result != 0);
        }
        #endregion

        #region Relational Operator Overloading
        public static bool operator ==(AbilityActivePhase p1, AbilityActivePhase.ActivePhase ap2)
        {
            if((System.Object)p1 == null) return false;
            else return p1.IsPhase(ap2);
        }
        public static bool operator !=(AbilityActivePhase p1, AbilityActivePhase.ActivePhase ap2)
        {
            return !(p1 == ap2);
        }

        public static bool operator ==(AbilityActivePhase p1, AbilityActivePhase p2)
        {
            if((System.Object)p1 == null && (System.Object)p2 == null) return true;
            else if((System.Object)p1 == null || (System.Object)p2 == null) return false;
            else return (p1._activePhase == p2._activePhase);
        }
        public static bool operator !=(AbilityActivePhase p1, AbilityActivePhase p2)
        {
            return !(p1 == p2);
        }
        public override bool Equals(System.Object obj)
        {
            // Check for null values and compare run-time types.
            if (obj == null || GetType() != obj.GetType())
                return false;

            AbilityActivePhase p = (obj as AbilityActivePhase);
            return (_activePhase == p._activePhase);
        }
        public override int GetHashCode()
        {
            return 0;
        }
        #endregion
    }
}
