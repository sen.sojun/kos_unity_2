﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityTrigger  
{
    #region Enums
    public enum TriggerType : int
    {
          AT_BEGIN_OF_PHASE
        , AT_REMAIN
        , AT_USE
        , AT_ENTER
        , AT_RUIN
        , AT_DEALT_DAMAGE
    }
    #endregion

    private static AbilityTrigger _instance;

    public static AbilityTrigger Instance 
    { 
        get
        {
            return _instance; 
        }
    }

    public static void BindTriggerEvent()
    {
         
    }
}
   
