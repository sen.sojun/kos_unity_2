﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardAbility
{
    #region BuffParam
    public class BuffParam
    {
        #region Protected Properties
        protected int _shield;
        protected int _hp;
        protected int _attack;
        protected int _speed;
        protected int _move;
        protected int _range;
        protected PassiveIndex _passiveIndex;
        protected List<CardSubTypeData> _subTypeList;
        protected List<CardJobData> _jobList;
        protected int _activeTurn;

        protected Ability _ability;
        protected AbilityParams _abParams;
        #endregion

        #region Public Properties
        public int Shield { get { return _shield; } }
        public int HP { get { return _hp; } }
        public int Attack { get { return _attack; } }
        public int Speed { get { return _speed; } }
        public int Move { get { return _move; } }
        public int Range { get { return _range; } }
        public PassiveIndex PassiveIndex { get { return _passiveIndex; } }
        public List<CardSubTypeData> SubTypeList { get { return _subTypeList; } }
        public List<CardJobData> JobList { get { return _jobList; } }
        public int ActiveTurn { get { return _activeTurn; } } 

        public Ability Ability { get { return _ability; } }
        public AbilityParams AbParams { get { return _abParams; } }
        #endregion

        #region BuffParam Constructors
        public BuffParam()
        {
            _shield = 0;
            _hp = 0;
            _attack = 0;
            _speed = 0;
            _move = 0;
            _range = 0;
            _passiveIndex = new PassiveIndex();
            _activeTurn = 0;

            _subTypeList = new List<CardSubTypeData>();
            _jobList = new List<CardJobData>();
        }

        public BuffParam(
              int shield
            , int hp
            , int attack
            , int speed
            , int move
            , int range
            , PassiveIndex passiveIndex
            , List<CardSubTypeData> subTypeList
            , List<CardJobData> jobList
            , int activeTurn = -1
        )
        {
            _shield = shield;
            _hp = hp;
            _attack = attack;
            _speed = speed;
            _move = move;
            _range = range;
            _passiveIndex = passiveIndex;
            _activeTurn = activeTurn;

            _subTypeList = new List<CardSubTypeData>(subTypeList);
            _jobList = new List<CardJobData>(jobList);
        }
        #endregion

        #region Constructors
        protected BuffParam(AbilityParams abParams, Ability ability)
        {
            _ability = ability;
            _abParams = abParams;
        }
        #endregion

        #region Static Methods
        public static List<BuffParam> CreateBuffParamList(Ability ability, List<string> paramTextList)
        {
            List<BuffParam> resultList = new List<BuffParam>();
            foreach (string paramText in paramTextList)
            {
                CardAbility.AbilityParams abParam = new AbilityParams(paramText);
                BuffParam buffParam = CreateBuffParam(abParam, ability);
                if (buffParam != null)
                {
                    resultList.Add(buffParam);
                }
            }
            return resultList;
        }

        public static BuffParam CreateBuffParam(AbilityParams abParams, Ability ability)
        {
            switch (abParams.Key)
            {
                case "SHIELD":  return new ShieldBuffParam(abParams, ability);   
                case "HP":      return new HPBuffParam(abParams, ability);       
                case "ATTACK":  return new AttackBuffParam(abParams, ability);     
                case "SPEED":   return new SpeedBuffParam(abParams, ability);     
                case "MOVE":    return new MoveBuffParam(abParams, ability);      
                case "RANGE":   return new RangeBuffParam(abParams, ability);     
                case "PASSIVE": return new PassiveBuffParam(abParams, ability);  
                case "TURN":    return new TurnBuffParam(abParams, ability);      
                case "SUBTYPE": return new SubTypeBuffParam(abParams, ability);   
                case "JOB":     return new JobBuffParam(abParams, ability);     
            }

            return null;
        }

        public static BuffParam CombineBuffParam(List<BuffParam> buffParamList)
        {
            BuffParam result = new BuffParam();

            foreach(BuffParam buffParam in buffParamList)
            {
                buffParam.UpdateValue();

                result._shield          = result.Shield + buffParam.Shield;
                result._hp              = result.HP + buffParam.HP;
                result._attack          = result.Attack + buffParam.Attack;
                result._speed           = result.Speed + buffParam.Speed;
                result._move            = result.Move + buffParam.Move;
                result._range           = result.Range + buffParam.Range;
                result._passiveIndex    = result.PassiveIndex + buffParam.PassiveIndex;

                foreach (CardSubTypeData subType in buffParam.SubTypeList)
                {
                    if (!result._subTypeList.Contains(subType))
                    {
                        result._subTypeList.Add(subType);
                    }
                }

                foreach (CardJobData job in buffParam.JobList)
                {
                    if (!result._jobList.Contains(job))
                    {
                        result._jobList.Add(job);
                    }
                }

                if(result._activeTurn >= 0)
                {
                    if(buffParam.ActiveTurn > 0)
                    {
                        // Add
                        result._activeTurn += buffParam.ActiveTurn;
                    }
                    else
                    {
                        // Replace
                        result._activeTurn = -1;
                    }
                }
            }

            return result;
        }

        public virtual void UpdateValue()
        {
            // update value.
        }

        protected bool GetInt(int paramIndex, out int number) 
        {   
            if (AbParams != null && AbParams.ParamsList != null && AbParams.ParamsList.Count > paramIndex)
            {
                AbilityParams param = new AbilityParams(AbParams.ParamsList[paramIndex]);

                bool isSuccess = AbilityParams.ParamToInt(param, this.Ability, out number);
                if (isSuccess)
                {
                    return true;
                }
            }

            number = 0;
            return false; 
        }

        protected bool GetPassiveIndex(int paramIndex, out PassiveIndex result)
        {   
            if (AbParams != null && AbParams.ParamsList != null && AbParams.ParamsList.Count > paramIndex)
            {
                AbilityParams param = new AbilityParams(AbParams.ParamsList[paramIndex]);

                bool isSuccess = AbilityParams.ParamToPassiveIndex(param, this.Ability, out result);
                if (isSuccess)
                {
                    return true;
                }
            }

            result = new PassiveIndex();
            return false; 
        }
            
        protected bool GetSubType(int paramIndex, out CardSubTypeData.CardSubType result) 
        {   
            if (AbParams != null && AbParams.ParamsList != null && AbParams.ParamsList.Count > paramIndex)
            {
                AbilityParams param = new AbilityParams(AbParams.ParamsList[paramIndex]);

                bool isSuccess = AbilityParams.ParamToCardSubType(param, this.Ability, out result);
                if (isSuccess)
                {
                    return true;
                }
            }

            result = CardSubTypeData.CardSubType.None;
            return false; 
        }

        protected bool GetJob(int paramIndex, out CardJobData.JobType result)
        {   
            if (AbParams != null && AbParams.ParamsList != null && AbParams.ParamsList.Count > paramIndex)
            {
                AbilityParams param = new AbilityParams(AbParams.ParamsList[paramIndex]);

                bool isSuccess = AbilityParams.ParamToCardJob(param, this.Ability, out result);
                if (isSuccess)
                {
                    return true;
                }
            }

            result = CardJobData.JobType.None;
            return false; 
        }
        #endregion
    }
    #endregion

    #region MoveBuffParam
    public class MoveBuffParam : BuffParam
    {
        #region MoveBuffParam Constructors
        public MoveBuffParam(AbilityParams abParams, Ability ability) : base(abParams, ability)
        {
            UpdateValue();
        }
        #endregion

        #region Methods
        public override void UpdateValue()
        {
            GetInt(0, out _move);
        }
        #endregion
    }
    #endregion

    #region ShieldBuffParam
    public class ShieldBuffParam : BuffParam
    {
        #region ShieldBuffParam Constructors
        public ShieldBuffParam(AbilityParams abParams, Ability ability) : base(abParams, ability)
        {
            UpdateValue();
        }
        #endregion

        #region Methods
        public override void UpdateValue()
        {
            GetInt(0, out _shield);
        }
        #endregion
    }
    #endregion

    #region HPBuffParam
    public class HPBuffParam : BuffParam
    {
        #region HPBuffParam Constructors
        public HPBuffParam(AbilityParams abParams, Ability ability) : base(abParams, ability)
        {
            UpdateValue();
        }
        #endregion

        #region Methods
        public override void UpdateValue()
        {
            GetInt(0, out _hp);
        }
        #endregion
    }
    #endregion

    #region AttackBuffParam
    public class AttackBuffParam : BuffParam
    {
        #region AttackBuffParam Constructors
        public AttackBuffParam(AbilityParams abParams, Ability ability) : base(abParams, ability)
        {
            UpdateValue();
        }
        #endregion

        #region Methods
        public override void UpdateValue()
        {
            GetInt(0, out _attack);
        }
        #endregion
    }
    #endregion

    #region SpeedBuffParam
    public class SpeedBuffParam : BuffParam
    {
        #region Constructors
        public SpeedBuffParam(AbilityParams abParams, Ability ability) : base(abParams, ability)
        {
            UpdateValue();
        }
        #endregion

        #region SpeedBuffParam Methods
        public override void UpdateValue()
        {
            GetInt(0, out _speed);
        }
        #endregion
    }
    #endregion

    #region RangeBuffParam
    public class RangeBuffParam : BuffParam
    {
        #region Constructors
        public RangeBuffParam(AbilityParams abParams, Ability ability) : base(abParams, ability)
        {
            UpdateValue();
        }
        #endregion

        #region Methods
        public override void UpdateValue()
        {
            GetInt(0, out _range);
        }
        #endregion
    }
    #endregion

    #region PassiveBuffParam
    public class PassiveBuffParam : BuffParam
    {
        #region Constructors
        public PassiveBuffParam(AbilityParams abParams, Ability ability) : base(abParams, ability)
        {
            UpdateValue();
        }
        #endregion

        #region Methods
        public override void UpdateValue()
        {
            GetInt(0, out _range);
        }
        #endregion
    }
    #endregion

    #region TurnBuffParam
    public class TurnBuffParam : BuffParam
    {
        #region Constructors
        public TurnBuffParam(AbilityParams abParams, Ability ability) : base(abParams, ability)
        {
            UpdateValue();
        }
        #endregion

        #region Methods
        public override void UpdateValue()
        {
            GetInt(0, out _activeTurn);
        }
        #endregion
    }
    #endregion

    #region SubTypeBuffParam
    public class SubTypeBuffParam : BuffParam
    {
        #region Constructors
        public SubTypeBuffParam(AbilityParams abParams, Ability ability) : base(abParams, ability)
        {
            UpdateValue();
        }
        #endregion

        #region Methods
        public override void UpdateValue()
        {
            CardSubTypeData.CardSubType subType;
            GetSubType(0, out subType);

            _subTypeList = new List<CardSubTypeData>();
            _subTypeList.Add(new CardSubTypeData(CardSubTypeData.EnumCardSubTypeToID(subType)));
        }
        #endregion
    }
    #endregion

    #region JobBuffParam
    public class JobBuffParam : BuffParam
    {
        #region Constructors
        public JobBuffParam(AbilityParams abParams, Ability ability) : base(abParams, ability)
        {
            UpdateValue();
        }
        #endregion

        #region Methods
        public override void UpdateValue()
        {
            CardJobData.JobType jobType;
            GetJob(0, out jobType);

            _jobList = new List<CardJobData>();
            _jobList.Add(new CardJobData(CardJobData.EnumJobTypeToID(jobType)));
        }
        #endregion
    }
    #endregion
}
