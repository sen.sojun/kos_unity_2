﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardAbility
{
    public class AbilityActiveZone 
    {
        #region Enums
        public enum ActiveZone : int
        {
              Deck          = 0
            , Hand
            , PlayerField
            , BattleField
            , EnemyField
            , Graveyard
            , RemoveZone
        }
        #endregion

        #region Private Properties
        private int _activeZone = 0;
        #endregion

        #region Constructors
        public AbilityActiveZone(int activeZone)
        {
            _activeZone = activeZone;
        }

        public AbilityActiveZone(AbilityActiveZone abActiveZone)
        {
            _activeZone = abActiveZone._activeZone;
        }
        #endregion

        #region Methods
        public void AddZone(AbilityActiveZone.ActiveZone zone)
        {
            _activeZone |= (1 << (int)zone);
        }

        public bool IsZone(AbilityActiveZone.ActiveZone zone)
        {
            int result = (_activeZone & (int)zone);
            return (result != 0);
        }
        #endregion

        #region Relational Operator Overloading
        public static bool operator ==(AbilityActiveZone z1, AbilityActiveZone.ActiveZone az2)
        {
            if((System.Object)z1 == null) return false;
            else return z1.IsZone(az2);
        }
        public static bool operator !=(AbilityActiveZone z1, AbilityActiveZone.ActiveZone az2)
        {
            return !(z1 == az2);
        }

        public static bool operator ==(AbilityActiveZone z1, AbilityActiveZone z2)
        {
            if((System.Object)z1 == null && (System.Object)z2 == null) return true;
            else if((System.Object)z1 == null || (System.Object)z2 == null) return false;
            else return (z1._activeZone == z2._activeZone);
        }
        public static bool operator !=(AbilityActiveZone z1, AbilityActiveZone z2)
        {
            return !(z1 == z2);
        }
        public override bool Equals(System.Object obj)
        {
            // Check for null values and compare run-time types.
            if (obj == null || GetType() != obj.GetType())
                return false;

            AbilityActiveZone z = (obj as AbilityActiveZone);
            return (_activeZone == z._activeZone);
        }
        public override int GetHashCode()
        {
            return 0;
        }
        #endregion
    }
}

