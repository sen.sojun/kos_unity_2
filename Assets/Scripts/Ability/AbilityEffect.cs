﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardAbility
{
    #region AbilityEffect Class
    public abstract class AbilityEffect 
    {
        #region Protected Properties
        protected Ability _ability;
        protected AbilityParams _abParams;

        protected CardAbility.Ability.OnFinishPrepare _finishPrepareCallback;
        protected CardAbility.Ability.OnCancelPrepare _cancelPrepareCallback;

        protected CardAbility.Ability.OnFinishAction _finishActionCallback;
        #endregion

        #region Public Properties
        public Ability Ability { get { return _ability; } }
        public AbilityParams AbParams { get { return _abParams; } }
        
        public bool IsOwnerTurn
        {   
            get
            {
                if(_ability != null)
                {
                    return (_ability.OwnerPlayerIndex == BattleManager.Instance.CurrentActivePlayerIndex);
                }
                return false;
            }
        }
        #endregion

        #region Constructors
        protected AbilityEffect(AbilityParams abParams, Ability ability)
        {
            _ability = ability;
            _abParams = abParams;
        }
        #endregion

        #region Methods
        public virtual void Prepare(CardAbility.Ability.OnFinishPrepare finishCallback, CardAbility.Ability.OnCancelPrepare cancelCallback)
        {
            _finishPrepareCallback = finishCallback;
            _cancelPrepareCallback = cancelCallback;
        }

        public virtual void Action(Ability.OnFinishAction callback)
        {
            _finishActionCallback = callback;
        }

        public virtual void Deactive()
        {
            // do something.
        }

        public virtual string GetDebugText()
        {
            string paramText = "";

            foreach(string text in AbParams.ParamsList)
            {
                if (paramText.Length > 0)
                {
                    paramText += ", " + text;
                }
                else
                {
                    paramText += text;
                }
            }

            return string.Format("{0}({1})", AbParams.Key, paramText);
        }

        public static AbilityEffect GenerateEffect(AbilityParams abParams, Ability ability)
        {
            switch (abParams.Key)
            {
                case "GETGOLD": return (new AEGetGold(abParams, ability)); 
                case "DISCARD": return (new AEDiscard(abParams, ability)); 
                case "ACT":     return (new AEAct(abParams, ability)); 
                case "IF":      return (new AEIf(abParams, ability)); 
                case "IFELSE":  return (new AEIfElse(abParams, ability)); 
                case "BUFF":    return (new AEBuff(abParams, ability)); 
                case "GETBUFF": return (new AEGetBuff(abParams, ability)); 
            }

            return null;
        }

        protected bool GetPlayer(int paramIndex, out PlayerIndex playerIndex) 
        {   
            if (AbParams != null && AbParams.ParamsList != null && AbParams.ParamsList.Count > paramIndex)
            {
                AbilityParams param = new AbilityParams(AbParams.ParamsList[paramIndex]);

                bool isSuccess = AbilityParams.ParamToPlayerIndex(param, this.Ability, out playerIndex);
                if (isSuccess)
                {
                    return true;
                }
            }

            playerIndex = PlayerIndex.One;
            return false; 
        }

        protected bool GetInt(int paramIndex, out int number) 
        {   
            if (AbParams != null && AbParams.ParamsList != null && AbParams.ParamsList.Count > paramIndex)
            {
                AbilityParams param = new AbilityParams(AbParams.ParamsList[paramIndex]);

                bool isSuccess = AbilityParams.ParamToInt(param, this.Ability, out number);
                if (isSuccess)
                {
                    return true;
                }
            }

            number = 0;
            return false; 
        }

        protected bool GetBool(int paramIndex, out bool value) 
        {   
            if (AbParams != null && AbParams.ParamsList != null && AbParams.ParamsList.Count > paramIndex)
            {
                AbilityParams param = new AbilityParams(AbParams.ParamsList[paramIndex]);

                bool isSuccess = AbilityParams.ParamToBool(param, this.Ability, out value);
                if (isSuccess)
                {
                    return true;
                }
            }

            value = false;
            return false; 
        }

        protected bool GetBattleCard(int paramIndex, out List<BattleCardData> battleCardList)
        {
            if (AbParams != null && AbParams.ParamsList != null && AbParams.ParamsList.Count > paramIndex)
            {
                Equation equa = Equation.BoolEquaTextToVar(_abParams.ParamsList[paramIndex], Equation.EquationType.BattleCardFilter);
                battleCardList = equa.GetBattleCard(this.Ability);

                return true;
            }

            battleCardList = new List<BattleCardData>();
            return false;
        }

        protected bool GetUniqueCard(int paramIndex, out List<UniqueCardData> uniqueCardDataList)
        {
            if (AbParams != null && AbParams.ParamsList != null && AbParams.ParamsList.Count > paramIndex)
            {
                Equation equa = Equation.BoolEquaTextToVar(_abParams.ParamsList[paramIndex], Equation.EquationType.UniqueCardFilter);
                uniqueCardDataList = equa.GetUniqueCard(this.Ability);

                return true;
            }

            uniqueCardDataList = new List<UniqueCardData>();
            return false;
        }

        protected bool GetCondition(int paramIndex, out bool result)
        {
            if (AbParams != null && AbParams.ParamsList.Count > paramIndex)
            {
                Equation equa = Equation.BoolEquaTextToVar(_abParams.ParamsList[paramIndex], Equation.EquationType.Bool);
                result = equa.GetBool(this.Ability);

                return true;
            }

            result = false;
            return false;
        }

        protected bool GetEffect(int paramIndex, out AbilityEffect effect)
        {
            if (AbParams != null && AbParams.ParamsList != null && AbParams.ParamsList.Count > paramIndex)
            {
                AbilityParams param = new AbilityParams(AbParams.ParamsList[paramIndex]);
                effect = AbilityEffect.GenerateEffect(param, this.Ability);
                if (effect != null)
                {
                    return true;
                }
            }

            effect = null;
            return false;
        }
        #endregion
    }
    #endregion

    #region AEGetGold Class
    public class AEGetGold : AbilityEffect
    {
        private PlayerIndex _playerIndex;
        private int _gold;

        #region Constructors
        public AEGetGold(AbilityParams abParams, Ability ability) : base(abParams, ability)
        {
        }
        #endregion

        #region Methods
        public override void Prepare(Ability.OnFinishPrepare finishCallback, Ability.OnCancelPrepare cancelCallback)
        {
            base.Prepare(finishCallback, cancelCallback);

            bool isSuccess = true;

            isSuccess &= GetPlayer(0, out _playerIndex);
            isSuccess &= GetInt(1, out _gold);

            if (isSuccess)
            {
                if (_finishPrepareCallback != null)
                {
                    _finishPrepareCallback.Invoke();
                }
            }
            else
            {
                if (_cancelPrepareCallback != null)
                {
                    _cancelPrepareCallback.Invoke();
                }
            }
        }

        public override void Action(Ability.OnFinishAction callback)
        {
            _finishActionCallback = callback;

            BattleManager.Instance.RequestGetGold(_playerIndex, _gold);

            if (_finishActionCallback != null)
            {
                _finishActionCallback.Invoke();
            }
        }

        public override string GetDebugText()
        {
            return string.Format("GetGold({0})", AbParams.ParamsList[0]);
        }
        #endregion
    }
    #endregion

    #region AEDiscard Class
    public class AEDiscard : AbilityEffect
    {
        private PlayerIndex _playerIndex;
        private int _discardNum;

        #region Constructors
        public AEDiscard(AbilityParams abParams, Ability ability) : base(abParams, ability)
        {
        }
        #endregion

        #region Methods
        public override void Prepare(Ability.OnFinishPrepare finishCallback, Ability.OnCancelPrepare cancelCallback)
        {
            base.Prepare(finishCallback, cancelCallback);

            bool isSuccess = true;

            isSuccess &= GetPlayer(0, out _playerIndex);
            isSuccess &= GetInt(1, out _discardNum);

            if (isSuccess)
            {
                if (_finishPrepareCallback != null)
                {
                    _finishPrepareCallback.Invoke();
                }
            }
            else
            {
                if (_cancelPrepareCallback != null)
                {
                    _cancelPrepareCallback.Invoke();
                }
            }
        }

        public override void Action(Ability.OnFinishAction callback)
        {
            base.Action(callback);

            BattleManager.Instance.RequestDiscard(_playerIndex, _discardNum);

            if (_finishActionCallback != null)
            {
                _finishActionCallback.Invoke();
            }

        }
        #endregion
    }
    #endregion

    #region AEAct Class
    public class AEAct : AbilityEffect
    {
        List<BattleCardData> _battleCardList;

        #region Constructors
        public AEAct(AbilityParams abParams, Ability ability) : base(abParams, ability)
        {
        }
        #endregion

        #region Methods
        public override void Prepare(Ability.OnFinishPrepare finishCallback, Ability.OnCancelPrepare cancelCallback)
        {
            base.Prepare(finishCallback, cancelCallback);

            bool isSuccess = true;

            isSuccess &= GetBattleCard(0, out _battleCardList);

            if (isSuccess)
            {
                if (_finishPrepareCallback != null)
                {
                    _finishPrepareCallback.Invoke();
                }
            }
            else
            {
                if (_cancelPrepareCallback != null)
                {
                    _cancelPrepareCallback.Invoke();
                }
            }
        }

        public override void Action(Ability.OnFinishAction callback)
        {
            base.Action(callback);

            foreach (BattleCardData card in _battleCardList)
            {
                BattleManager.Instance.RequestActBattleCard(card.BattleCardID);
            }

            if (_finishActionCallback != null)
            {
                _finishActionCallback.Invoke();
            }

        }
        #endregion
    }
    #endregion

    #region AEIf Class
    public class AEIf : AbilityEffect
    {
        private bool _result;
        private AbilityEffect _effect;

        #region Constructors
        public AEIf(AbilityParams abParams, Ability ability) : base(abParams, ability)
        {
        }
        #endregion

        #region Methods
        public override void Prepare(Ability.OnFinishPrepare finishCallback, Ability.OnCancelPrepare cancelCallback)
        {
            base.Prepare(finishCallback, cancelCallback);

            bool isSuccess = true;

            isSuccess &= GetCondition(0, out _result);
            isSuccess &= GetEffect(1, out _effect);

            if (isSuccess)
            {
                if (_result)
                {
                    _effect.Prepare(this.OnFinishPrepare, cancelCallback);
                }
                else
                {
                    OnFinishAction();
                }
            }
            else
            {
                _result = false;
                OnFinishAction();
            }
        }

        private void OnFinishPrepare()
        {
            if (_finishPrepareCallback != null)
            {
                _finishPrepareCallback.Invoke();
            }
        }

        public override void Action(Ability.OnFinishAction callback)
        {
            base.Action(callback);

            if (_result)
            {
                _effect.Action(this.OnFinishAction);
            }
            else
            {
                OnFinishAction();
            }
        }

        private void OnFinishAction()
        {
            if (_finishActionCallback != null)
            {
                _finishActionCallback.Invoke();
            }
        }  
        #endregion
    }
    #endregion

    #region AEIfElse Class
    public class AEIfElse : AbilityEffect
    {
        private bool _result;
        private AbilityEffect _effect_1;
        private AbilityEffect _effect_2;

        #region Constructors
        public AEIfElse(AbilityParams abParams, Ability ability) : base(abParams, ability)
        {
        }
        #endregion

        #region Methods
        public override void Prepare(Ability.OnFinishPrepare finishCallback, Ability.OnCancelPrepare cancelCallback)
        {
            base.Prepare(finishCallback, cancelCallback);

            bool isSuccess = true;

            isSuccess &= GetCondition(0, out _result);
            isSuccess &= GetEffect(1, out _effect_1);
            isSuccess &= GetEffect(2, out _effect_2);

            if (isSuccess)
            {
                if (_result)
                {
                    _effect_1.Prepare(this.OnFinishPrepare, cancelCallback);
                }
                else
                {
                    _effect_2.Prepare(this.OnFinishPrepare, cancelCallback);
                }
            }
            else
            {
                _result = false;
                _effect_1 = null;
                _effect_2 = null;

                if (_cancelPrepareCallback != null)
                {
                    _cancelPrepareCallback.Invoke();
                }
            }
        }

        private void OnFinishPrepare()
        {
            if (_finishPrepareCallback != null)
            {
                _finishPrepareCallback.Invoke();
            }
        }

        public override void Action(Ability.OnFinishAction callback)
        {
            base.Action(callback);

            if (_result)
            {
                _effect_1.Action(this.OnFinishAction);
            }
            else
            {
                _effect_2.Action(this.OnFinishAction);
            }
        }

        private void OnFinishAction()
        {
            if (_finishActionCallback != null)
            {
                _finishActionCallback.Invoke();
            }
        }  
        #endregion
    }
    #endregion

    #region AEBuff Class
    public class AEBuff : AbilityEffect
    {
        private string _buffID;
        private static int _buffIndex = 0;
         
        #region BuffAE Properties
        protected bool _isOwnerDestroyed = false;
        protected Equation _condition;
        protected Equation _target;
        protected List<BattleCardData> _targetList = null;
        protected List<CardBuff> _buffList = null;
        protected List<BuffParam> _buffParamList = null;

        public string BuffID {get { return _buffID; } }
        public bool IsOwnerDestroyed { get { return _isOwnerDestroyed; } }
        #endregion

        #region Constructors
        public AEBuff(AbilityParams abParams, Ability ability) : base(abParams, ability)
        {
            _buffID = string.Format("{0}_{1}", this.GetType().ToString(), GetBuffIndex());
            Init();
        }

        ~AEBuff()
        {
            Deinit();
        }
        #endregion

        #region Methods
        public void Init()
        {
            //Debug.Log("Init: BuffAE");

            if(BattleManager.Instance != null && _ability != null)
            {
                if(_ability.IsSummon)
                {
                    _condition = Equation.BoolEquaTextToVar(_abParams.ParamsList[0], Equation.EquationType.Bool);
                    _target = Equation.BoolEquaTextToVar(_abParams.ParamsList[1], Equation.EquationType.BattleCardFilter);

                    List<string> paramTextList = new List<string>();
                    for(int index = 2; index < _abParams.ParamsList.Count; ++index)
                    {
                        paramTextList.Add(_abParams.ParamsList[index]);
                    }
                    _buffParamList = BuffParam.CreateBuffParamList(this._ability, paramTextList);

                    BattleManager.Instance.BindBattleEventTrigger(
                          this.OnOwnerDestroyed
                        , _ability.OwnerPlayerIndex
                        , BattleEventTrigger.BattleTriggerType.CARD_BE_DESTROYED
                        , _ability.BattleCard.BattleCardID
                    );

                    BattleManager.Instance.BindBattleEventTrigger(
                          this.OnOwnerDestroyed
                        , _ability.OwnerPlayerIndex
                        , BattleEventTrigger.BattleTriggerType.CARD_LEAVED
                        , _ability.BattleCard.BattleCardID
                    );

                    BattleManager.Instance.BindBattleEventTrigger(
                          this.OnUnitUpdate
                        , _ability.OwnerPlayerIndex
                        , BattleEventTrigger.BattleTriggerType.CARD_BE_SUMMONED
                        , BattleEventTrigger.BattleSide.Friendly
                    );

                    BattleManager.Instance.BindBattleEventTrigger(
                          this.OnUnitUpdate
                        , _ability.OwnerPlayerIndex
                        , BattleEventTrigger.BattleTriggerType.CARD_BE_DESTROYED
                        , BattleEventTrigger.BattleSide.Friendly
                    );

                    BattleManager.Instance.BindBattleEventTrigger(
                          this.OnUnitUpdate
                        , _ability.OwnerPlayerIndex
                        , BattleEventTrigger.BattleTriggerType.CARD_LEAVED
                        , BattleEventTrigger.BattleSide.Friendly
                    );
                }
            }
        }

        public void Deinit ()
        {
            //Debug.Log("Deinit: BuffAE");

            if(BattleManager.Instance != null && _ability != null)
            {
                if(_ability.IsSummon)
                {
                    _condition = null;
                    _target = null;
                    _buffParamList = null;

                    BattleManager.Instance.UnbindBattleEventTrigger(
                          this.OnOwnerDestroyed
                        , _ability.OwnerPlayerIndex
                        , BattleEventTrigger.BattleTriggerType.CARD_BE_DESTROYED
                        , _ability.BattleCard.BattleCardID
                    );

                    BattleManager.Instance.UnbindBattleEventTrigger(
                          this.OnOwnerDestroyed
                        , _ability.OwnerPlayerIndex
                        , BattleEventTrigger.BattleTriggerType.CARD_LEAVED
                        , _ability.BattleCard.BattleCardID
                    );

                    BattleManager.Instance.UnbindBattleEventTrigger(
                          this.OnUnitUpdate
                        , _ability.OwnerPlayerIndex
                        , BattleEventTrigger.BattleTriggerType.CARD_BE_SUMMONED
                        , BattleEventTrigger.BattleSide.Friendly
                    );

                    BattleManager.Instance.UnbindBattleEventTrigger(
                          this.OnUnitUpdate
                        , _ability.OwnerPlayerIndex
                        , BattleEventTrigger.BattleTriggerType.CARD_BE_DESTROYED
                        , BattleEventTrigger.BattleSide.Friendly
                    );

                    BattleManager.Instance.UnbindBattleEventTrigger(
                          this.OnUnitUpdate
                        , _ability.OwnerPlayerIndex
                        , BattleEventTrigger.BattleTriggerType.CARD_LEAVED
                        , BattleEventTrigger.BattleSide.Friendly
                    );
                }
            }

            OnOwnerDestroyed();
        }

        protected virtual void OnUnitUpdate()
        {
            //Debug.Log("BuffAE: OnUnitUpdate " + this._abilityData.BattleCard.BattleCardID);

            if(IsOwnerDestroyed) return;

            if(IsActive())
            {
                // Active
                List<BattleCardData> prevTargetList = _targetList;

                List<BattleCardData> targetList = null;
                GetTargetList(out targetList);

                List<BattleCardData> removeTargetList = new List<BattleCardData>();
                List<BattleCardData> oldTargetList = new List<BattleCardData>();
                List<BattleCardData> newTargetList = new List<BattleCardData>();

                if(targetList != null && targetList.Count > 0)
                {
                    //Debug.Log("Terget: " + targetList.Count.ToString());

                    if(prevTargetList != null && prevTargetList.Count > 0)
                    {
                        //Debug.Log("Prev: " + prevTargetList.Count.ToString());

                        // has old target.
                        for(int i = 0; i < targetList.Count; ++i)
                        {
                            for(int j = 0; j < prevTargetList.Count; ++j)
                            {
                                if(targetList[i] == prevTargetList[j])
                                {
                                    // Is remain target.
                                    oldTargetList.Add(targetList[i]);

                                    targetList.RemoveAt(i);
                                    prevTargetList.RemoveAt(j);

                                    --i;
                                    break;
                                }
                            }
                        }

                        if(prevTargetList.Count > 0)
                        {
                            removeTargetList.AddRange(prevTargetList);
                        }

                        if(targetList.Count > 0)
                        {
                            newTargetList.AddRange(targetList);
                        }
                    }
                    else
                    {
                        //Debug.Log("Prev: No target.");

                        // no old target.
                        newTargetList.AddRange(targetList);
                    }
                }
                else
                {
                    //Debug.Log("Terget: No target.");

                    // No target.
                    // Remove all current target.
                    if(prevTargetList != null && prevTargetList.Count > 0)
                    {
                        removeTargetList.AddRange(prevTargetList);
                    }
                }

                /*
                Debug.Log("Remove: " + removeTargetList.Count);
                Debug.Log("New: " + newTargetList.Count);
                Debug.Log("Old: " + oldTargetList.Count);
                */

                List<BattleCardData> currentTargetList = new List<BattleCardData>();

                for(int index = 0; index < removeTargetList.Count; ++index)
                {
                    //Debug.Log("Remove Terget: " + removeTargetList.Count.ToString());
                    RemoveBuff(removeTargetList[index]);
                }
                for(int index = 0; index < newTargetList.Count; ++index)
                {
                    //Debug.Log("New Terget: " + newTargetList.Count.ToString());
                    AddBuff(newTargetList[index]);
                }

                currentTargetList.AddRange(oldTargetList);
                currentTargetList.AddRange(newTargetList);

                _targetList = currentTargetList;
            }
            else
            {
                // Deactive
                if(_targetList != null && _targetList.Count > 0)
                {
                    for(int index = 0; index < _targetList.Count; ++index)
                    {
                        RemoveBuff(_targetList[index]);
                    }

                    _targetList.Clear();
                }
            }
        }

        protected void OnOwnerDestroyed()
        {
            //Debug.Log("BuffAE: OnOwnerDestroyed");
            if(!_isOwnerDestroyed)
            {
                if(_targetList != null && _targetList.Count > 0)
                {
                    while(_targetList.Count > 0)
                    {
                        BattleCardData battleCard = _targetList[0];
                        _targetList.RemoveAt(0);

                        RemoveBuff(battleCard);
                    }

                    _targetList.Clear();
                }
            }

            _isOwnerDestroyed = true;
        }

        protected virtual bool IsActive()
        {
            if (_condition != null)
            {
                return _condition.GetBool(this.Ability);
            }

            return true;
        }

        protected virtual bool GetTargetList(out List<BattleCardData> targetList)
        {
            if (_target != null)
            {
                targetList = _target.GetBattleCard(this.Ability);
                return true;
            }

            targetList = null;
            return false;   
        }

        /*
        protected virtual bool GetTargetList(out List<UniqueCardData> targetList)
        {
            targetList = null;
            return false;   
        }
        */

        protected virtual void AddBuff(BattleCardData battleCard)
        {
            BuffParam buffParam = BuffParam.CombineBuffParam(_buffParamList);

            CardBuff buff = new CardBuff(
                  this.BuffID
                , this.Ability.BattleCard.BattleCardID
                , battleCard.BattleCardID
                , buffParam.Shield
                , buffParam.HP
                , buffParam.Attack
                , buffParam.Speed
                , buffParam.Move
                , buffParam.Range
                , buffParam.PassiveIndex
                , buffParam.SubTypeList
                , buffParam.JobList
                , this.OnUnitUpdate
                , buffParam.ActiveTurn
            );

            BattleManager.Instance.RequestAddBuffBattleCard(
                  battleCard.BattleCardID
                , buff
            );
        }

        protected virtual void RemoveBuff(BattleCardData battleCard)
        {
            battleCard.RemoveBuff(
                  this._ability.BattleCard.BattleCardID
                , this.BuffID
            );
        }

        public override void Prepare(Ability.OnFinishPrepare finishCallback, Ability.OnCancelPrepare cancelCallback)
        {
            base.Prepare(finishCallback, cancelCallback);
        
            if(_finishPrepareCallback != null)
            {
                _finishPrepareCallback.Invoke();
            }
        }

        public override void Action(Ability.OnFinishAction callback)
        {
            base.Action(callback);

            if(_finishActionCallback != null)
            {
                _finishActionCallback.Invoke();
            }
        }

        public static int GetBuffIndex()
        {
            return _buffIndex++;
        }
        #endregion
    }
    #endregion

    #region AEGetBuff Class
    public class AEGetBuff : AbilityEffect
    {
        private string _buffID;
        private static int _buffIndex = 0;

        protected bool _isOwnerDestroyed = false;
        protected Equation _target;
        protected List<BattleCardData> _targetList = null;
        protected List<BuffParam> _buffParamList = null;

        public string BuffID { get { return _buffID; } }

        #region Constructors
        public AEGetBuff(AbilityParams abParams, Ability ability) : base(abParams, ability)
        {
            _buffID = string.Format("{0}_{1}", this.GetType().ToString(), _buffIndex++);
            _target = Equation.BoolEquaTextToVar(_abParams.ParamsList[0], Equation.EquationType.BattleCardFilter);

            List<string> paramTextList = new List<string>();
            for(int index = 1; index < _abParams.ParamsList.Count; ++index)
            {
                paramTextList.Add(_abParams.ParamsList[index]);
            }

            _buffParamList = BuffParam.CreateBuffParamList(this._ability, paramTextList);
        }
        #endregion

        #region Methods
        protected virtual bool GetTargetList(out List<BattleCardData> targetList)
        {
            if (_target != null)
            {
                targetList = _target.GetBattleCard(this.Ability);
                return true;
            }

            targetList = null;
            return false;   
        }
            
        public override void Prepare(Ability.OnFinishPrepare finishCallback, Ability.OnCancelPrepare cancelCallback)
        {
            bool isHasTarget = GetTargetList(out _targetList);

            if(isHasTarget)
            {
                if (finishCallback != null)
                {
                    finishCallback.Invoke();
                }
            }
            else
            {
                if (cancelCallback != null)
                {
                    cancelCallback.Invoke();
                }
            }
        }

        public override void Action(Ability.OnFinishAction callback)
        {
            BuffParam buffParam = BuffParam.CombineBuffParam(_buffParamList);

            foreach (BattleCardData card in _targetList)
            {
                AddBuff(card, buffParam);
            }

            if (callback != null)
            {
                callback.Invoke();
            }
        }

        protected virtual void AddBuff(BattleCardData battleCard, BuffParam buffParam)
        {
            CardBuff buff = new CardBuff(
                  this.BuffID
                , this.Ability.BattleCard.BattleCardID
                , battleCard.BattleCardID
                , buffParam.Shield
                , buffParam.HP
                , buffParam.Attack
                , buffParam.Speed
                , buffParam.Move
                , buffParam.Range
                , buffParam.PassiveIndex
                , buffParam.SubTypeList
                , buffParam.JobList
                , null
                , buffParam.ActiveTurn
            );

            BattleManager.Instance.RequestAddBuffBattleCard(
                battleCard.BattleCardID
                , buff
            );
        }
        #endregion
    }
    #endregion

    #region AEDealDamage Class
    public class AEDealDamage : AbilityEffect
    {
        protected Equation _target;
        protected List<BattleCardData> _targetList = null;
        protected int _damage;
        protected bool _isPiercing;

        #region Constructors
        public AEDealDamage(AbilityParams abParams, Ability ability) : base(abParams, ability)
        {
        }
        #endregion

        #region Methods
        protected virtual bool GetTargetList(out List<BattleCardData> targetList)
        {
            if (_target != null)
            {
                targetList = _target.GetBattleCard(this.Ability);
                return true;
            }

            targetList = null;
            return false;   
        }

        public override void Prepare(Ability.OnFinishPrepare finishCallback, Ability.OnCancelPrepare cancelCallback)
        {
            base.Prepare(finishCallback, cancelCallback);

            bool isSuccess = true;
            isSuccess &= GetBattleCard(0, out _targetList);
            isSuccess &= GetInt(1, out _damage);
            isSuccess &= GetBool(2, out _isPiercing);

            if (isSuccess)
            {
                OnFinish();
            }
            else
            {
                OnCancel();
            }
        }

        private void OnFinish()
        {
            if (_finishPrepareCallback != null)
            {
                _finishPrepareCallback.Invoke();
            }
        }

        private void OnCancel()
        {
            if (_cancelPrepareCallback != null)
            {
                _cancelPrepareCallback.Invoke();
            }
        }

        public override void Action(Ability.OnFinishAction callback)
        {
            base.Action(callback);

            foreach(BattleCardData card in _targetList)
            {
                BattleManager.Instance.RequestDealDamageBattleCard
                (
                      card.BattleCardID
                    , _damage
                    , _isPiercing
                );
            }

            if (_finishActionCallback != null)
            {
                _finishActionCallback.Invoke();
            }

        }
        #endregion
    }
    #endregion

    #region AESelectBattleCard Class
    public class AESelectBattleCard : AbilityEffect
    {
        protected int _targetNum;
        protected bool _isCanCancel;
        protected Equation _target;
        protected List<BattleCardData> _targetList = null;

        public static List<int> SelectBattleCardList;

        #region Constructors
        public AESelectBattleCard(AbilityParams abParams, Ability ability) : base(abParams, ability)
        {
        }
        #endregion

        #region Methods
        protected virtual bool GetSelectableList(out List<BattleCardData> targetList)
        {
            if (_target != null)
            {
                targetList = _target.GetBattleCard(this.Ability);
                return true;
            }

            targetList = null;
            return false;   
        }

        public override void Prepare(Ability.OnFinishPrepare finishCallback, Ability.OnCancelPrepare cancelCallback)
        {
            base.Prepare(finishCallback, cancelCallback);

            bool isSuccess = true;

            isSuccess &= GetBattleCard(0, out _targetList);
            isSuccess &= GetInt(1, out _targetNum);
            isSuccess &= GetBool(2, out _isCanCancel);

            BattleManager.Instance.RequestSelectTarget(
                  ""
                , this.Ability.OwnerPlayerIndex
                , _targetList
                , _targetNum
                , this.OnFinish
                , this.OnCancel
                , (BattleManager.Instance.IsHaveTimeout && !this.IsOwnerTurn)
                , _isCanCancel
            );
        }

        private void OnFinish(List<int> battleIDList)
        {
            SelectBattleCardList = new List<int>(battleIDList);

            if (_finishPrepareCallback != null)
            {
                _finishPrepareCallback.Invoke();
            }
        }

        private void OnCancel()
        {
            if (_cancelPrepareCallback != null)
            {
                _cancelPrepareCallback.Invoke();
            }
        }

        public override void Action(Ability.OnFinishAction callback)
        {
            base.Action(callback);

            if (_finishActionCallback != null)
            {
                _finishActionCallback.Invoke();
            }
        }
        #endregion
    }
    #endregion

    #region AESelectUniqueCard Class
    public class AESelectUniqueCard : AbilityEffect
    {
        protected int _targetNum;
        protected bool _isCanCancel;
        protected Equation _target;
        protected List<UniqueCardData> _targetList = null;

        public static List<int> SelectUniqueCardList;

        #region Constructors
        public AESelectUniqueCard(AbilityParams abParams, Ability ability) : base(abParams, ability)
        {
        }
        #endregion

        #region Methods
        protected virtual bool GetSelectableList(out List<BattleCardData> targetList)
        {
            if (_target != null)
            {
                targetList = _target.GetBattleCard(this.Ability);
                return true;
            }

            targetList = null;
            return false;   
        }

        public override void Prepare(Ability.OnFinishPrepare finishCallback, Ability.OnCancelPrepare cancelCallback)
        {
            base.Prepare(finishCallback, cancelCallback);

            bool isSuccess = true;
       
            isSuccess &= GetUniqueCard(0, out _targetList);
            isSuccess &= GetInt(1, out _targetNum);
            isSuccess &= GetBool(2, out _isCanCancel);

            BattleManager.Instance.StartSelectUniqueCard(
                  ""
                , this.Ability.OwnerPlayerIndex
                , _targetList
                , _targetNum
                , this.OnFinish
                , this.OnCancel
                , null
                , !IsOwnerTurn
                , _isCanCancel
            );
        }

        private void OnFinish(List<int> uniqueIDList)
        {
            SelectUniqueCardList = new List<int>(uniqueIDList);

            if (_finishPrepareCallback != null)
            {
                _finishPrepareCallback.Invoke();
            }
        }

        private void OnCancel()
        {
            if (_cancelPrepareCallback != null)
            {
                _cancelPrepareCallback.Invoke();
            }
        }

        public override void Action(Ability.OnFinishAction callback)
        {
            base.Action(callback);

            if (_finishActionCallback != null)
            {
                _finishActionCallback.Invoke();
            }
        }
        #endregion
    }
    #endregion
}