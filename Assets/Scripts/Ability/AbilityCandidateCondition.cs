﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AbilityCandidateCondition
{
    /*
    #region PlayerCard Compare

    // Properties
    public static bool IsCardID(this PlayerCardData playerCardData, string cardID)
    {
        return (string.Compare(playerCardData.CardID, cardID) == 0);
    }

    public static bool IsCardType(this PlayerCardData playerCardData, CardTypeData typeData)
    {
        return (playerCardData.TypeData == typeData);
    }

    public static bool IsCardType(this PlayerCardData playerCardData, CardTypeData.CardType cardType)
    {
        return (playerCardData.TypeData == cardType);
    }

    public static bool IsCardClan(this PlayerCardData playerCardData, CardClanData clanData)
    {
        return (playerCardData.ClanData == clanData);
    }

    public static bool IsCardClan(this PlayerCardData playerCardData, CardClanData.CardClanType clan)
    {
        return (playerCardData.ClanData == clan);
    }

    public static bool IsCardSubType(this PlayerCardData playerCardData, CardSubTypeData subTypeData)
    {
        foreach (CardSubTypeData subType in playerCardData.SubTypeDataList)
        {
            if (subType == subTypeData)
            {
                return true;
            }
        }

        return false;
    }

    public static bool IsCardSubType(this PlayerCardData playerCardData, CardSubTypeData.CardSubType cardSubType)
    {
        foreach (CardSubTypeData subType in playerCardData.SubTypeDataList)
        {
            if (subType == cardSubType)
            {
                return true;
            }
        }
        return false;
    }

    public static bool IsCardJobType(this PlayerCardData playerCardData, CardJobData jobData)
    {
        foreach (CardJobData cardJob in playerCardData.JobDataList)
        {
            if (cardJob == jobData)
            {
                return true;
            }
        }

        return false;
    }

    public static bool IsCardJobType(this PlayerCardData playerCardData, CardJobData.JobType job)
    {
        foreach (CardJobData cardJob in playerCardData.JobDataList)
        {
            if (cardJob == job)
            {
                return true;
            }
        }
        return false;
    }

    public static bool IsCardSymbol(this PlayerCardData playerCardData, CardSymbolData symbolData)
    {
        return (playerCardData.SymbolData == symbolData);
    }

    public static bool IsCardSymbol(this PlayerCardData playerCardData, CardSymbolData.CardSymbol symbol)
    {
        return (playerCardData.SymbolData == symbol);
    }

    #endregion
    */


    #region BattleCard Compare

    // Properties

    public static bool IsCardID(this BattleCardData battleCardData, string cardID)
    {
        return (string.Compare(battleCardData.CardID, cardID) == 0);
    }

    public static bool IsCardType(this BattleCardData battleCardData, CardTypeData typeData)
    {
        return (battleCardData.TypeData == typeData);
    }

    public static bool IsCardType(this BattleCardData battleCardData, CardTypeData.CardType cardType)
    {
        return (battleCardData.TypeData == cardType);
    }

    public static bool IsCardClan(this BattleCardData battleCardData, CardClanData clanData)
    {
        return (battleCardData.ClanData == clanData);
    }

    public static bool IsCardClan(this BattleCardData battleCardData, CardClanData.CardClanType clan)
    {
        return (battleCardData.ClanData == clan);
    }

    public static bool IsCardSubType(this BattleCardData battleCardData, CardSubTypeData subTypeData)
    {
        foreach (CardSubTypeData subType in battleCardData.SubTypeDataList)
        {
            if (subType == subTypeData)
            {
                return true;
            }
        }

        return false;
    }

    public static bool IsCardSubType(this BattleCardData battleCardData, CardSubTypeData.CardSubType cardSubType)
    {
        foreach (CardSubTypeData subType in battleCardData.SubTypeDataList)
        {
            if (subType == cardSubType)
            {
                return true;
            }
        }
        return false;
    }

    public static bool IsCardJobType(this BattleCardData battleCardData, CardJobData jobData)
    {
        foreach (CardJobData cardJob in battleCardData.JobDataList)
        {
            if (cardJob == jobData)
            {
                return true;
            }
        }

        return false;
    }

    public static bool IsCardJobType(this BattleCardData battleCardData, CardJobData.JobType job)
    {
        foreach (CardJobData cardJob in battleCardData.JobDataList)
        {
            if (cardJob == job)
            {
                return true;
            }
        }
        return false;
    }

    public static bool IsCardSymbol(this BattleCardData battleCardData, CardSymbolData symbolData)
    {
        return (battleCardData.SymbolData == symbolData);
    }

    public static bool IsCardSymbol(this BattleCardData battleCardData, CardSymbolData.CardSymbol symbol)
    {
        return (battleCardData.SymbolData == symbol);
    }
         
    // Costs

    public static bool IsCardCostEqual(this BattleCardData battleCardData, int cost)
    {
        return (battleCardData.Cost == cost);
    }

    public static bool IsCardCostMoreEqual(this BattleCardData battleCardData, int cost)
    {
        return (battleCardData.Cost >= cost);
    }

    public static bool IsCardCostMoreThan(this BattleCardData battleCardData, int cost)
    {
        return (battleCardData.Cost > cost);
    }

    public static bool IsCardCostLessEqual(this BattleCardData battleCardData, int cost)
    {
        return (battleCardData.Cost <= cost);
    }

    public static bool IsCardCostLessThan(this BattleCardData battleCardData, int cost)
    {
        return (battleCardData.Cost < cost);
    }

    // Shield

    public static bool IsCardShieldEqual(this BattleCardData battleCardData, int shield)
    {
        return (battleCardData.CurrentShield == shield);
    }

    public static bool IsCardShieldMoreEqual(this BattleCardData battleCardData, int shield)
    {
        return (battleCardData.CurrentShield >= shield);
    }

    public static bool IsCardShieldMoreThan(this BattleCardData battleCardData, int shield)
    {
        return (battleCardData.CurrentShield > shield);
    }

    public static bool IsCardShieldLessEqual(this BattleCardData battleCardData, int shield)
    {
        return (battleCardData.CurrentShield <= shield);
    }

    public static bool IsCardShieldLessThan(this BattleCardData battleCardData, int shield)
    {
        return (battleCardData.CurrentShield < shield);
    }

    // HP

    public static bool IsCardHPEqual(this BattleCardData battleCardData, int hp)
    {
        return (battleCardData.CurrentHP == hp);
    }

    public static bool IsCardHPMoreEqual(this BattleCardData battleCardData, int hp)
    {
        return (battleCardData.CurrentHP >= hp);
    }

    public static bool IsCardHPMoreThan(this BattleCardData battleCardData, int hp)
    {
        return (battleCardData.CurrentHP > hp);
    }

    public static bool IsCardHPLessEqual(this BattleCardData battleCardData, int hp)
    {
        return (battleCardData.CurrentHP <= hp);
    }

    public static bool IsCardHPLessThan(this BattleCardData battleCardData, int hp)
    {
        return (battleCardData.CurrentHP < hp);
    }

    // Attack

    public static bool IsCardAtkEqual(this BattleCardData battleCardData, int atk)
    {
        return (battleCardData.CurrentAttack == atk);
    }

    public static bool IsCardAtkMoreEqual(this BattleCardData battleCardData, int atk)
    {
        return (battleCardData.CurrentAttack >= atk);
    }

    public static bool IsCardAtkMoreThan(this BattleCardData battleCardData, int atk)
    {
        return (battleCardData.CurrentAttack > atk);
    }

    public static bool IsCardAtkLessEqual(this BattleCardData battleCardData, int atk)
    {
        return (battleCardData.CurrentAttack <= atk);
    }

    public static bool IsCardAtkLessThan(this BattleCardData battleCardData, int atk)
    {
        return (battleCardData.CurrentAttack < atk);
    }

    // Speed

    public static bool IsCardSpeedEqual(this BattleCardData battleCardData, int speed)
    {
        return (battleCardData.CurrentSpeed == speed);
    }

    public static bool IsCardSpeedMoreEqual(this BattleCardData battleCardData, int speed)
    {
        return (battleCardData.CurrentSpeed >= speed);
    }

    public static bool IsCardSpeedMoreThan(this BattleCardData battleCardData, int speed)
    {
        return (battleCardData.CurrentSpeed > speed);
    }

    public static bool IsCardSpeedLessEqual(this BattleCardData battleCardData, int speed)
    {
        return (battleCardData.CurrentSpeed <= speed);
    }

    public static bool IsCardSpeedLessThan(this BattleCardData battleCardData, int speed)
    {
        return (battleCardData.CurrentSpeed < speed);
    }
        
    // Status

    public static bool IsCardOwner(this BattleCardData battleCardData, PlayerIndex ownerIndex)
    {
        return (battleCardData.OwnerPlayerIndex == ownerIndex);
    }

    public static bool IsCardLeader(this BattleCardData battleCardData)
    {
        return battleCardData.IsLeader;
    }

    public static bool IsCardFacedown(this BattleCardData battleCardData)
    {
        return battleCardData.IsFaceDown;
    }

    public static bool IsCardCanMove(this BattleCardData battleCardData)
    {
        return battleCardData.IsCanMove;
    }

    public static bool IsCardCanAtk(this BattleCardData battleCardData)
    {
        return battleCardData.IsCanAttack;
    }

    public static bool IsCardCanAct(this BattleCardData battleCardData)
    {
        return !battleCardData.IsActive;
    }

    public static bool IsCardBattleZone(this BattleCardData battleCardData, BattleZoneIndex battleZone)
    {
        return (battleCardData.CurrentZone == battleZone);
    }
    #endregion
}

