﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardAbility
{
    /*
    #region AbilityCondition Class
    public abstract class AbilityCondition
    {
        #region Private Properties
        private Ability _ability;
        #endregion

        #region Public Properties
        public Ability Ability { get { return _ability; } }
        #endregion

        #region Methods
        public void SetAbility(Ability ability)
        {
            _ability = ability;
        }

        public virtual bool IsCondition()
        {
            return false;
        }

        public static AbilityCondition GenerateCondition(AbilityParams abParams)
        {
            switch (abParams.Key)
            {
                case "ACBattleCardEqual":       return (new ACBattleCardEqual(abParams)); 
                case "ACBattleCardLessEqual":   return (new ACBattleCardLessEqual(abParams)); 
                case "ACBattleCardMoreEqual":   return (new ACBattleCardMoreEqual(abParams)); 

                case "ACUniqueCardEqual":       return (new ACUniqueCardEqual(abParams)); 
                case "ACUniqueCardLessEqual":   return (new ACUniqueCardLessEqual(abParams)); 
                case "ACUniqueCardMoreEqual":   return (new ACUniqueCardMoreEqual(abParams)); 
            }

            return null;
        }

        public static Equation TextToACEquation(string conditionText)
        {
            Equation equa = Equation.BoolEquaTextToVar(
                  conditionText
                , Equation.EquationType.AbilityCondition
            );

            return equa;
        }
        #endregion
    }
    #endregion

    #region ACBattleCardEqual Class
    public class ACBattleCardEqual : AbilityCondition
    {
        #region Private Properties
        private int _num;
        private BattleCardFilter _filter;
        #endregion

        #region Constructors
        public ACBattleCardEqual(int num, BattleCardFilter cardFilter)
        {
            _num = num;
            _filter = cardFilter;
        }

        public ACBattleCardEqual(AbilityParams effectParams)
        {
            _num = int.Parse(effectParams.ParamsList[0]);

        }
        #endregion

        #region Methods
        public override bool IsCondition()
        {
            List<BattleCardData> cardList;
            BattleManager.Instance.GetAllBattleCard(out cardList, _filter);

            if(cardList != null && cardList.Count == _num)
            {
                return true;
            }

            return false;
        }
        #endregion
    }
    #endregion

    #region ACBattleCardLessEqual Class
    public class ACBattleCardLessEqual : AbilityCondition
    {
        #region Private Properties
        private int _num;
        private BattleCardFilter _filter;
        #endregion

        #region Constructors
        public ACBattleCardLessEqual(int num, BattleCardFilter cardFilter)
        {
            _num = num;
            _filter = cardFilter;
        }

        public ACBattleCardLessEqual(AbilityParams effectParams)
        {
            _num = int.Parse(effectParams.ParamsList[0]);

        }
        #endregion

        public override bool IsCondition()
        {
            List<BattleCardData> cardList;
            BattleManager.Instance.GetAllBattleCard(out cardList, _filter);

            if(cardList != null && cardList.Count <= _num)
            {
                return true;
            }

            return false;
        }
    }
    #endregion

    #region ACBattleCardMoreEqual Class
    public class ACBattleCardMoreEqual : AbilityCondition
    {
        #region Private Properties
        private int _num;
        private BattleCardFilter _filter;
        #endregion

        #region Constructors
        public ACBattleCardMoreEqual(int num, BattleCardFilter cardFilter)
        {
            _num = num;
            _filter = cardFilter;
        }

        public ACBattleCardMoreEqual(AbilityParams effectParams)
        {
            _num = int.Parse(effectParams.ParamsList[0]);

        }
        #endregion

        #region Methods
        public override bool IsCondition()
        {
            List<BattleCardData> cardList;
            BattleManager.Instance.GetAllBattleCard(out cardList, _filter);

            if(cardList != null && cardList.Count >= _num)
            {
                return true;
            }

            return false;
        }
        #endregion
    }
    #endregion

    #region ACUniqueCardEqual Class
    public class ACUniqueCardEqual : AbilityCondition
    {
        #region Private Properties
        private int _num;
        private UniqueCardFilter _filter;
        #endregion

        #region Constructors
        public ACUniqueCardEqual(int num, UniqueCardFilter cardFilter)
        {
            _num = num;
            _filter = cardFilter;
        }

        public ACUniqueCardEqual(AbilityParams effectParams)
        {
            _num = int.Parse(effectParams.ParamsList[0]);

        }
        #endregion

        #region Methods
        public override bool IsCondition()
        {
            List<UniqueCardData> cardList;
            BattleManager.Instance.GetAllUniqueCard(out cardList, _filter);

            if(cardList != null && cardList.Count == _num)
            {
                return true;
            }

            return false;
        }
        #endregion
    }
    #endregion

    #region ACUniqueCardLessEqual Class
    public class ACUniqueCardLessEqual : AbilityCondition
    {
        #region Private Properties
        private int _num;
        private UniqueCardFilter _filter;
        #endregion

        #region Constructors
        public ACUniqueCardLessEqual(int num, UniqueCardFilter cardFilter)
        {
            _num = num;
            _filter = cardFilter;
        }

        public ACUniqueCardLessEqual(AbilityParams effectParams)
        {
            _num = int.Parse(effectParams.ParamsList[0]);

        }
        #endregion

        #region Methods
        public override bool IsCondition()
        {
            List<UniqueCardData> cardList;
            BattleManager.Instance.GetAllUniqueCard(out cardList, _filter);

            if(cardList != null && cardList.Count <= _num)
            {
                return true;
            }

            return false;
        }
        #endregion
    }
    #endregion

    #region ACUniqueCardMoreEqual Class
    public class ACUniqueCardMoreEqual : AbilityCondition
    {
        #region Private Properties
        private int _num;
        private UniqueCardFilter _filter;
        #endregion

        #region Constructors
        public ACUniqueCardMoreEqual(int num, UniqueCardFilter cardFilter)
        {
            _num = num;
            _filter = cardFilter;
        }

        public ACUniqueCardMoreEqual(AbilityParams effectParams)
        {
            _num = int.Parse(effectParams.ParamsList[0]);

        }
        #endregion

        #region Methods
        public override bool IsCondition()
        {
            List<UniqueCardData> cardList;
            BattleManager.Instance.GetAllUniqueCard(out cardList, _filter);

            if(cardList != null && cardList.Count >= _num)
            {
                return true;
            }

            return false;
        }
        #endregion
    }

    */
}
