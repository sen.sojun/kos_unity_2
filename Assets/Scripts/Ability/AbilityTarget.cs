﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardAbility
{
    #region AbilityTarget Class
    public class AbilityTarget 
    {
        #region Protected Properties
        protected Ability _ability;
        protected List<CardAbility.AbilityParams> _paramList;
        #endregion

        #region Public Properties
        public Ability Ability { get { return _ability; } }
        public List<CardAbility.AbilityParams> ParamList { get { return _paramList; } }
        #endregion

        #region Constructors
        protected AbilityTarget(AbilityParams abParams, Ability ability)
        {
            _ability = ability;

            _paramList = new List<CardAbility.AbilityParams>();
            foreach(string paramText in abParams.ParamsList)
            {
                _paramList.Add(new CardAbility.AbilityParams(paramText));
            }
        }
        #endregion

        #region Methods
        public virtual bool IsHasTarget()
        {
            return false;
        }

        public static AbilityTarget GenerateTarget(Ability2Data.AbilityTargetType TargetType, AbilityParams abParams, Ability ability)
        {
            switch (TargetType)
            {
                case Ability2Data.AbilityTargetType.None:       return (new ATNone(abParams, ability)); 
                case Ability2Data.AbilityTargetType.BattleCard: return (new ATBattleCard(abParams, ability)); 
                case Ability2Data.AbilityTargetType.UniqueCard: return (new ATUniqueCard(abParams, ability)); 
                case Ability2Data.AbilityTargetType.Player:     return (new ATPlayer(abParams, ability)); 
            }

            return null;
        }
        #endregion
    }
    #endregion

    #region ATNone Class
    public class ATNone : AbilityTarget
    {
        #region Constructors
        public ATNone(AbilityParams abParams, Ability ability) : base(abParams, ability)
        {
        }
        #endregion

        #region Methods
        public override bool IsHasTarget()
        {
            return true;
        }
        #endregion
    }
    #endregion

    #region ATBattleCard Class
    public class ATBattleCard : AbilityTarget
    {
        #region Constructors
        public ATBattleCard(AbilityParams abParams, Ability ability) : base(abParams, ability)
        {
            
        }
        #endregion

        #region Methods
        private List<BattleCardData> GetTarget()
        {
            List<BattleCardData> result;
            AbilityParams.ParamToBattleCardList(_paramList[0], _ability, out result);
            return result;
        }

        public override bool IsHasTarget()
        {
            List<BattleCardData> targetList = GetTarget();
            if (targetList != null && targetList.Count > 0)
            {
                return true;
            }

            return false;
        }
        #endregion
    }
    #endregion

    #region ATUniqueCard Class
    public class ATUniqueCard : AbilityTarget
    {
        #region Constructors
        public ATUniqueCard(AbilityParams abParams, Ability ability) : base(abParams, ability)
        {
        }
        #endregion

        #region Methods
        private List<UniqueCardData> GetTarget()
        {
            List<UniqueCardData> result;
            AbilityParams.ParamToUniqueCardList(_paramList[0], _ability, out result);
            return result;
        }

        public override bool IsHasTarget()
        {
            List<UniqueCardData> targetList = GetTarget();
            if (targetList != null && targetList.Count > 0)
            {
                return true;
            }

            return false;
        }
        #endregion
    }
    #endregion

    #region ATPlayer Class
    public class ATPlayer : AbilityTarget
    {
        #region Constructors
        public ATPlayer(AbilityParams abParams, Ability ability) : base(abParams, ability)
        {
        }
        #endregion

        #region Methods
        private bool GetTarget()
        {
            PlayerIndex result;
            return AbilityParams.ParamToPlayerIndex(_paramList[0], _ability, out result);
        }

        public override bool IsHasTarget()
        {
            return GetTarget();
        }
        #endregion
    }
    #endregion
}
