﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProfileResultUI : MonoBehaviour {

    public Text ResultText;
    public Button CloseButton;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ShowUI(string msg)
    {
        ResultText.text = msg;

        CloseButton.onClick.RemoveAllListeners();
        CloseButton.onClick.AddListener(this.HideUI);

        gameObject.SetActive(true);
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }

}
