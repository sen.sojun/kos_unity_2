﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AnnouncementUI : MonoBehaviour {
	//public static ShaderVariantCollection 

	public UITexture AnnounceMentTextTure;
	public UILabel LabelTextTopic;
	public UILabel LabelID;
	public UILabel LabelTitle;
	public UILabel LabelAnnouncement;
	public UILabel LabelButtonOk;

	public static bool IsShowAnnouncement = true;

	private List<AnnouncementDBData> _announcementDataList = null;
	private int _currentIndex = 0;

	// Use this for initialization
	void Start ()
	{
	
	}

	void Init()
	{

		bool isSuccess = AnnouncementDB.GetAllData(out _announcementDataList);
		if (isSuccess)
		{
			ShowCurrentData();
		}
		else
		{
			Debug.LogError("AnnouncementScript/Init: AnnouncementDB.GetAllData Failed!!");
		}
	}


	public void ShowCurrentData()
	{
		ShowData(_currentIndex);
	}
	// Update is called once per frame
	void Update () {
	
	}

	public void ShowAnnouncementUI()
	{
		//Load announcement data from csv file
		Init ();
		IsShowAnnouncement = false;
		gameObject.SetActive (true);
	}


	public void ShowData(int index)
	{
		if (_announcementDataList != null && _announcementDataList.Count > index)
		{
			AnnouncementDBData announcementData = _announcementDataList[index];
			//tutorialData.DebugPrintData();

			if (announcementData.IsActive == 1) 
			{
				//PopupBoxUI.ShowMessageUI(tutorialData.TextEng,tutorialData.TutorialButtonText);
				Texture imageTexture = Resources.Load(announcementData.ImageID, typeof(Texture)) as Texture;
				if (imageTexture != null)
                {
                    
					AnnounceMentTextTure.mainTexture = imageTexture;
				}
				else
				{
					Debug.LogError(
                        string.Format(
                              "Announcement Image not found! ({0}"
                            , announcementData.ImageID
                        )
                    );
				}
				LabelTextTopic.text = announcementData.Title;
				LabelID.text = announcementData.Code;
				LabelTitle.text = announcementData.Date;
				LabelButtonOk.GetComponent<UILocalize>().key = announcementData.ButtonTextKey;

                string announcementText = "";
                switch (LocalizationManager.CurrentLanguage)
                {
                    case LocalizationManager.Language.Thai:
                    {
                        announcementText = announcementData.TextTh;
                    }
                    break;

                    case LocalizationManager.Language.English:
                    default:
                    {
                        announcementText = announcementData.TextEng;
                    }
                    break;
                }
                LabelAnnouncement.text = announcementText;
            } 
		
		}
	}


}
