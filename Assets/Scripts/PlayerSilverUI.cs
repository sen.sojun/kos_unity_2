﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerSilverUI : MonoBehaviour 
{
    public int VideoSilverCoin = 50;
    public ConfirmBox ConfirmBoxUI;

    public delegate void OnViewAds();
    public delegate void OnCancel();

    private bool _isShowRewardedVideo = false;
    private OnViewAds _onViewAds = null;
    private OnCancel _onCancel = null;

    // Use this for initialization
    void Start () 
    {   
    }
    
    // Update is called once per frame
    void Update () 
    {   
    }

    public void ShowUI()
    {
        gameObject.SetActive(true);
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }
    
    public void OnShowAdsComplete(bool isSuccess)
    {
        Debug.Log("OnShowAdsComplete : isSuccess (" + isSuccess + ")");
        
        if(isSuccess)
        {
            int silver = PlayerSave.GetPlayerSilverCoin() + VideoSilverCoin;
            PlayerSave.SavePlayerSilverCoin(silver);
            
            if(KOSServer.Instance != null)
            {
                KOSServer.RequestAddSilver(VideoSilverCoin, null, null);
            }
            
            if(MenuManagerV2.Instance != null)
            {
                MenuManagerV2.Instance.UpdateTopBarText();
            }
            
            if(LobbyUIManager.Instance != null)
            {
                LobbyUIManager.Instance.UpdateTopBarText();
            }
        }
        
        TopBarManager topBar = GameObject.FindObjectOfType<TopBarManager>();
        if(topBar != null)
        {
            topBar.ToggleSilverUI();
        }
        
        if(ConfirmBoxUI != null)
        {
            if(isSuccess)
            {
                ConfirmBoxUI.ShowUI(
                      ""
                    , string.Format(Localization.Get("ADS_GOT_REWARD"), VideoSilverCoin)
                    , ConfirmBoxUI.HideUI
                );
            }
            else
            {
                ConfirmBoxUI.ShowUI("", Localization.Get("ADS_NOT_COMPLETE"), ConfirmBoxUI.HideUI);
            }
        }
        
        _isShowRewardedVideo = false;
        HideUI();
    }

    public void OnViewAdsEvent()
    {
        if(!_isShowRewardedVideo)
        {
            _isShowRewardedVideo = true;
            MyAdmobManager.ShowRewardedVideo(OnShowAdsComplete);
    
            /*
            if(_onViewAds != null)
            {
                _onViewAds.Invoke();
            }
            */
        }
    }

    public void OnCancelEvent()
    {
        _isShowRewardedVideo = false;
        
        if (_onCancel != null)
        {
            _onCancel.Invoke();
        }
    }
}
