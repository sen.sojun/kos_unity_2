﻿using UnityEngine;
using System.Collections;



public class ShopConfirmationUI : MonoBehaviour {

	public delegate void OnPurchase ();
	public delegate void OnBack();

	public UILabel HeadLabel;
	public UILabel DescriptionLabel;

	public GameObject ConfirmButton;
	public GameObject RightBackButton;
	public GameObject CenterBackButton;

	private OnPurchase _onPurchase;
	private OnBack     _onBack;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ShowUI(string headLabelText,string descriptionText,
                       OnPurchase onPurchase,OnBack onBack,
                       bool confirmShowButton,bool rightBackButtonShow,
                       bool centerBackButtonShow)
	{
		HeadLabel.text = headLabelText;
		DescriptionLabel.text = descriptionText;
		ConfirmButton.SetActive (confirmShowButton);
		RightBackButton.SetActive (rightBackButtonShow);
		CenterBackButton.SetActive (centerBackButtonShow);

		_onPurchase = onPurchase;
		_onBack = onBack;

		gameObject.SetActive (true);
		BattleUIManager.RequestBringForward (gameObject);
	}

	public void HideUI()
	{
		gameObject.SetActive (false);
	}

	public void OnPurchaseClick()
	{
		if ( _onPurchase != null ) 
		{
			_onPurchase.Invoke ();
		}
	}

	public void OnBackClick()
	{
		if (_onBack != null) 
		{
			_onBack.Invoke ();
		}
		
	}



}
