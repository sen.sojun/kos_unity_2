﻿using UnityEngine;
using System.Collections;

public class AdsMessageUI : MonoBehaviour 
{
	public UILabel MessageLabel;

	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
	}

	public void ShowUI(string text)
	{
		MessageLabel.text = text;

		gameObject.SetActive (true);
		BattleUIManager.RequestBringForward (gameObject);
	}

	public void HideUI()
	{
		gameObject.SetActive (false);
	}
}
