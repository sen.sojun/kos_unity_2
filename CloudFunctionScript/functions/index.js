//'use strict';

const functions = require('firebase-functions');
const admin = require('firebase-admin');
const util = require('util');
var responseText_buyShopPack;
var responseText_redeemGold;
var silverRefPath = 'player/%s/UsersilverCoin';
var goldRefPath = 'player/%s/UsergoldCoin';
var stashRefPath = 'player_stash/%s';
var receiptRefPath = 'receipts/%s/%s';
var starterPackCost = 150;
var premiumPackCost = 15;

admin.initializeApp(functions.config().firebase);

function randomIntInc (low, high) {
  return Math.floor(Math.random() * (high - low + 1) + low);
}

function randomCards(shopPackID){
  var cards = [];

  switch(shopPackID)
  {
    case '1': // starter pack
    {
      // 4 common or mass
      for(var i = 0; i < 4; ++i)
      {
        var cardID = randomCommonOrMassCard();
        cards.push(cardID);
      }
      // 1 any
      {
        var cardID = randomAnyCard();
        cards.push(cardID);
      }
    }    
    break;

    case '2': // premium pack
    {
      // 3 common or mass
      for(var i = 0; i < 3; ++i)
      {
        var cardID = randomCommonOrMassCard();
        cards.push(cardID);
      }
      // 1 uncommon
      {
        var cardID = randomUncommonCard();
        cards.push(cardID);
      }
      // 1 rare
      {
        var cardID = randomRareCard();
        cards.push(cardID);
      }
    }
    break;
  }

  return cards;
}

var commonOrMassIDList = [];
var commonOrMassChance = [];
var commonOrMassSumChance = 0;
function randomCommonOrMassCard(){
  if(commonOrMassIDList.length <= 0)
  {  
    commonOrMassSumChance = 0;
    var chance = 0;

    // Mass
    commonOrMassIDList.push('C0004'); chance = 150; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
    commonOrMassIDList.push('C0011'); chance = 150; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
    commonOrMassIDList.push('C0012'); chance = 150; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
    commonOrMassIDList.push('C0018'); chance = 150; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
    commonOrMassIDList.push('C0061'); chance = 100; commonOrMassChance.push(chance); commonOrMassSumChance += chance;

    // Common
    commonOrMassIDList.push('C0001'); chance = 100; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
    commonOrMassIDList.push('C0002'); chance = 100; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
    commonOrMassIDList.push('C0003'); chance = 100; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
    commonOrMassIDList.push('C0005'); chance = 100; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
    commonOrMassIDList.push('C0006'); chance = 100; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
    commonOrMassIDList.push('C0007'); chance = 100; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
    commonOrMassIDList.push('C0008'); chance = 100; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
    commonOrMassIDList.push('C0009'); chance = 100; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
    commonOrMassIDList.push('C0010'); chance = 100; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
    commonOrMassIDList.push('C0013'); chance = 100; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
    commonOrMassIDList.push('C0014'); chance = 100; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
    commonOrMassIDList.push('C0015'); chance = 100; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
    commonOrMassIDList.push('C0016'); chance = 100; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
    commonOrMassIDList.push('C0017'); chance = 100; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
    commonOrMassIDList.push('C0019'); chance = 100; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
    commonOrMassIDList.push('C0020'); chance = 100; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
    commonOrMassIDList.push('C0021'); chance = 100; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
    commonOrMassIDList.push('C0022'); chance = 100; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
    commonOrMassIDList.push('C0023'); chance = 100; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
    commonOrMassIDList.push('C0024'); chance = 100; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
    commonOrMassIDList.push('C0064'); chance = 100; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
    commonOrMassIDList.push('C0065'); chance = 100; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
    commonOrMassIDList.push('C0066'); chance = 100; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
    commonOrMassIDList.push('C0067'); chance = 100; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
    commonOrMassIDList.push('C0068'); chance = 100; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
    commonOrMassIDList.push('C0069'); chance = 100; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
    commonOrMassIDList.push('C0070'); chance = 100; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
    commonOrMassIDList.push('C0071'); chance = 100; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
    commonOrMassIDList.push('C0072'); chance = 100; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
    commonOrMassIDList.push('C0094'); chance = 150; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
    //commonOrMassIDList.push('C0102'); chance = 0; commonOrMassChance.push(chance); commonOrMassSumChance += chance;
  }

  var r = randomIntInc(1, commonOrMassSumChance);
  //console.log('randomCommonOrMassCard ', r);

  for(var i = 0; i<commonOrMassIDList.length; ++i)
  {
    r -= commonOrMassChance[i];
    if(r <= 0)
    {
      return commonOrMassIDList[i];
    }
  }

  return commonOrMassIDList[commonOrMassIDList.length - 1];
}

var uncommonIDList = [];
var uncommonChance = [];
var uncommonSumChance = 0;
function randomUncommonCard(){
  if(uncommonIDList.length <= 0)
  {  
    uncommonSumChance = 0;
    var chance = 0;

    // Uncommon
    uncommonIDList.push('C0025'); chance = 30; uncommonChance.push(chance); uncommonSumChance += chance;
    uncommonIDList.push('C0026'); chance = 30; uncommonChance.push(chance); uncommonSumChance += chance;
    uncommonIDList.push('C0027'); chance = 30; uncommonChance.push(chance); uncommonSumChance += chance;
    uncommonIDList.push('C0028'); chance = 30; uncommonChance.push(chance); uncommonSumChance += chance;
    uncommonIDList.push('C0029'); chance = 30; uncommonChance.push(chance); uncommonSumChance += chance;
    uncommonIDList.push('C0030'); chance = 30; uncommonChance.push(chance); uncommonSumChance += chance;
    uncommonIDList.push('C0031'); chance = 30; uncommonChance.push(chance); uncommonSumChance += chance;
    uncommonIDList.push('C0032'); chance = 30; uncommonChance.push(chance); uncommonSumChance += chance;
    uncommonIDList.push('C0033'); chance = 30; uncommonChance.push(chance); uncommonSumChance += chance;
    uncommonIDList.push('C0034'); chance = 30; uncommonChance.push(chance); uncommonSumChance += chance;
    uncommonIDList.push('C0035'); chance = 30; uncommonChance.push(chance); uncommonSumChance += chance;
    uncommonIDList.push('C0036'); chance = 30; uncommonChance.push(chance); uncommonSumChance += chance;
    uncommonIDList.push('C0037'); chance = 30; uncommonChance.push(chance); uncommonSumChance += chance;
    uncommonIDList.push('C0038'); chance = 30; uncommonChance.push(chance); uncommonSumChance += chance;
    uncommonIDList.push('C0039'); chance = 30; uncommonChance.push(chance); uncommonSumChance += chance;
    uncommonIDList.push('C0040'); chance = 30; uncommonChance.push(chance); uncommonSumChance += chance;
    uncommonIDList.push('C0073'); chance = 30; uncommonChance.push(chance); uncommonSumChance += chance;
    uncommonIDList.push('C0075'); chance = 30; uncommonChance.push(chance); uncommonSumChance += chance;
    uncommonIDList.push('C0076'); chance = 30; uncommonChance.push(chance); uncommonSumChance += chance;
    uncommonIDList.push('C0077'); chance = 30; uncommonChance.push(chance); uncommonSumChance += chance;
    //uncommonIDList.push('C0078'); chance = 0; uncommonChance.push(chance); uncommonSumChance += chance;
    uncommonIDList.push('C0089'); chance = 30; uncommonChance.push(chance); uncommonSumChance += chance;
    uncommonIDList.push('C0090'); chance = 30; uncommonChance.push(chance); uncommonSumChance += chance;
    //uncommonIDList.push('C0101'); chance = 0; uncommonChance.push(chance); uncommonSumChance += chance;
  }

  var r = randomIntInc(1, uncommonSumChance);
  console.log('randomCommonOrMassCard ', r);

  for(var i = 0; i<uncommonIDList.length; ++i)
  {
    r -= uncommonChance[i];
    if(r <= 0)
    {
      return uncommonIDList[i];
    }
  }

  return uncommonIDList[uncommonIDList.length - 1];
}

var rareIDList = [];
var rareChance = [];
var rareSumChance = 0;
function randomRareCard(){
  if(rareIDList.length <= 0)
  {  
    rareSumChance = 0;
    var chance = 0;
    
    // Rare
    rareIDList.push('C0041'); chance = 10; rareChance.push(chance); rareSumChance += chance;
    rareIDList.push('C0043'); chance = 10; rareChance.push(chance); rareSumChance += chance;
    rareIDList.push('C0045'); chance = 10; rareChance.push(chance); rareSumChance += chance;
    rareIDList.push('C0046'); chance = 10; rareChance.push(chance); rareSumChance += chance;
    rareIDList.push('C0047'); chance = 10; rareChance.push(chance); rareSumChance += chance;
    rareIDList.push('C0051'); chance = 10; rareChance.push(chance); rareSumChance += chance;
    rareIDList.push('C0052'); chance = 10; rareChance.push(chance); rareSumChance += chance;
    rareIDList.push('C0060'); chance = 10; rareChance.push(chance); rareSumChance += chance;
    rareIDList.push('C0074'); chance = 10; rareChance.push(chance); rareSumChance += chance;
    rareIDList.push('C0079'); chance = 10; rareChance.push(chance); rareSumChance += chance;
    rareIDList.push('C0080'); chance = 10; rareChance.push(chance); rareSumChance += chance;
    //rareIDList.push('C0081'); chance = 0; rareChance.push(chance); rareSumChance += chance;
    rareIDList.push('C0082'); chance = 10; rareChance.push(chance); rareSumChance += chance;
    rareIDList.push('C0086'); chance = 8; rareChance.push(chance); rareSumChance += chance;
    //rareIDList.push('C0087'); chance = 0; rareChance.push(chance); rareSumChance += chance;
    rareIDList.push('C0091'); chance = 10; rareChance.push(chance); rareSumChance += chance;
    rareIDList.push('C0092'); chance = 10; rareChance.push(chance); rareSumChance += chance;
    rareIDList.push('C0093'); chance = 10; rareChance.push(chance); rareSumChance += chance;
    //rareIDList.push('C0095'); chance = 0; rareChance.push(chance); rareSumChance += chance;
    //rareIDList.push('C0096'); chance = 0; rareChance.push(chance); rareSumChance += chance;
    //rareIDList.push('C0097'); chance = 0; rareChance.push(chance); rareSumChance += chance;
    //rareIDList.push('C0098'); chance = 0; rareChance.push(chance); rareSumChance += chance;
    //rareIDList.push('C0099'); chance = 0; rareChance.push(chance); rareSumChance += chance;
    //rareIDList.push('C0100'); chance = 0; rareChance.push(chance); rareSumChance += chance;

    // Super Rare
    rareIDList.push('C0042'); chance = 5; rareChance.push(chance); rareSumChance += chance;
    rareIDList.push('C0044'); chance = 5; rareChance.push(chance); rareSumChance += chance;
    rareIDList.push('C0048'); chance = 5; rareChance.push(chance); rareSumChance += chance;
    rareIDList.push('C0049'); chance = 5; rareChance.push(chance); rareSumChance += chance;
    rareIDList.push('C0050'); chance = 10; rareChance.push(chance); rareSumChance += chance;
    rareIDList.push('C0053'); chance = 5; rareChance.push(chance); rareSumChance += chance;
    rareIDList.push('C0054'); chance = 5; rareChance.push(chance); rareSumChance += chance;
    rareIDList.push('C0055'); chance = 3; rareChance.push(chance); rareSumChance += chance;
    rareIDList.push('C0056'); chance = 1; rareChance.push(chance); rareSumChance += chance;
    rareIDList.push('C0057'); chance = 3; rareChance.push(chance); rareSumChance += chance;
    rareIDList.push('C0058'); chance = 3; rareChance.push(chance); rareSumChance += chance;
    rareIDList.push('C0059'); chance = 5; rareChance.push(chance); rareSumChance += chance;
    rareIDList.push('C0062'); chance = 5; rareChance.push(chance); rareSumChance += chance;
    //rareIDList.push('C0063'); chance = 0; rareChance.push(chance); rareSumChance += chance;
    //rareIDList.push('C0083'); chance = 0; rareChance.push(chance); rareSumChance += chance;
    //rareIDList.push('C0084'); chance = 0; rareChance.push(chance); rareSumChance += chance;
    //rareIDList.push('C0085'); chance = 0; rareChance.push(chance); rareSumChance += chance;
    //rareIDList.push('C0088'); chance = 0; rareChance.push(chance); rareSumChance += chance;
  }

  var r = randomIntInc(1, rareSumChance);
  //console.log('randomCommonOrMassCard ', r);

  for(var i = 0; i<rareIDList.length; ++i)
  {
    r -= rareChance[i];
    if(r <= 0)
    {
      return rareIDList[i];
    }
  }

  return rareIDList[rareIDList.length - 1];
}

var allIDList = [];
var allChance = [];
var allSumChance = 0;
function randomAnyCard(){
  if(allIDList.length <= 0)
  {  
    allSumChance = 0;
    var chance = 0;

    // Mass
    allIDList.push('C0004'); chance = 150; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0011'); chance = 150; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0012'); chance = 150; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0018'); chance = 150; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0061'); chance = 100; allChance.push(chance); allSumChance += chance;

    // Common
    allIDList.push('C0001'); chance = 100; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0002'); chance = 100; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0003'); chance = 100; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0005'); chance = 100; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0006'); chance = 100; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0007'); chance = 100; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0008'); chance = 100; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0009'); chance = 100; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0010'); chance = 100; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0013'); chance = 100; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0014'); chance = 100; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0015'); chance = 100; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0016'); chance = 100; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0017'); chance = 100; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0019'); chance = 100; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0020'); chance = 100; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0021'); chance = 100; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0022'); chance = 100; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0023'); chance = 100; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0024'); chance = 100; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0064'); chance = 100; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0065'); chance = 100; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0066'); chance = 100; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0067'); chance = 100; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0068'); chance = 100; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0069'); chance = 100; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0070'); chance = 100; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0071'); chance = 100; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0072'); chance = 100; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0094'); chance = 150; allChance.push(chance); allSumChance += chance;
    //allIDList.push('C0102'); chance = 0; allChance.push(chance); allSumChance += chance;

    // Uncommon
    allIDList.push('C0025'); chance = 30; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0026'); chance = 30; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0027'); chance = 30; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0028'); chance = 30; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0029'); chance = 30; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0030'); chance = 30; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0031'); chance = 30; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0032'); chance = 30; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0033'); chance = 30; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0034'); chance = 30; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0035'); chance = 30; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0036'); chance = 30; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0037'); chance = 30; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0038'); chance = 30; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0039'); chance = 30; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0040'); chance = 30; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0073'); chance = 30; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0075'); chance = 30; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0076'); chance = 30; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0077'); chance = 30; allChance.push(chance); allSumChance += chance;
    //allIDList.push('C0078'); chance = 0; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0089'); chance = 30; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0090'); chance = 30; allChance.push(chance); allSumChance += chance;
    //allIDList.push('C0101'); chance = 0; allChance.push(chance); allSumChance += chance;

    // Rare
    allIDList.push('C0041'); chance = 10; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0043'); chance = 10; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0045'); chance = 10; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0046'); chance = 10; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0047'); chance = 10; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0051'); chance = 10; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0052'); chance = 10; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0060'); chance = 10; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0074'); chance = 10; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0079'); chance = 10; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0080'); chance = 10; allChance.push(chance); allSumChance += chance;
    //allIDList.push('C0081'); chance = 0; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0082'); chance = 10; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0086'); chance = 8; allChance.push(chance); allSumChance += chance;
    //allIDList.push('C0087'); chance = 0; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0091'); chance = 10; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0092'); chance = 10; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0093'); chance = 10; allChance.push(chance); allSumChance += chance;
    //allIDList.push('C0095'); chance = 0; allChance.push(chance); allSumChance += chance;
    //allIDList.push('C0096'); chance = 0; allChance.push(chance); allSumChance += chance;
    //allIDList.push('C0097'); chance = 0; allChance.push(chance); allSumChance += chance;
    //allIDList.push('C0098'); chance = 0; allChance.push(chance); allSumChance += chance;
    //allIDList.push('C0099'); chance = 0; allChance.push(chance); allSumChance += chance;
    //allIDList.push('C0100'); chance = 0; allChance.push(chance); allSumChance += chance;

    // Super Rare
    allIDList.push('C0042'); chance = 5; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0044'); chance = 5; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0048'); chance = 5; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0049'); chance = 5; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0050'); chance = 10; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0053'); chance = 5; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0054'); chance = 5; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0055'); chance = 3; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0056'); chance = 1; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0057'); chance = 3; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0058'); chance = 3; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0059'); chance = 5; allChance.push(chance); allSumChance += chance;
    allIDList.push('C0062'); chance = 5; allChance.push(chance); allSumChance += chance;
    //allIDList.push('C0063'); chance = 0; allChance.push(chance); allSumChance += chance;
    //allIDList.push('C0083'); chance = 0; allChance.push(chance); allSumChance += chance;
    //allIDList.push('C0084'); chance = 0; allChance.push(chance); allSumChance += chance;
    //allIDList.push('C0085'); chance = 0; allChance.push(chance); allSumChance += chance;
    //allIDList.push('C0088'); chance = 0; allChance.push(chance); allSumChance += chance;
  }

  var r = randomIntInc(1, allSumChance);
  //console.log('randomCommonOrMassCard ', r);

  for(var i = 0; i<allIDList.length; ++i)
  {
    r -= allChance[i];
    if(r <= 0)
    {
      return allIDList[i];
    }
  }

  return allIDList[allIDList.length - 1];
}

function IsValidPack(shopPackID){
  switch(shopPackID)
  {
    case '1':
    case '2': 
      return true;

    default: return false;
  }
}

function GetPackCost(shopPackID){
  switch(shopPackID)
  {
    case '1': return starterPackCost;
    case '2': return premiumPackCost;

    default: return 0;
  }
}

function GetPackCostPath(shopPackID){
  switch(shopPackID)
  {
    case '1': return silverRefPath;
    case '2': return goldRefPath;

    default: return "";
  }
}

function checkAndBuyShopPack(shopPackID, playerID, validCallback, invalidCallback){
  if(!IsValidPack(shopPackID))
  {
    console.log('Invalid shop pack ID.');
    invalidCallback();
    return;
  }

  var costPath = util.format(GetPackCostPath(shopPackID),  playerID);
  admin.database().ref(costPath).once('value').then(function(snapshot) {
    var key = snapshot.key;
    var current = Number(snapshot.val());

    var cost = GetPackCost(shopPackID); // pack cost
    if(current >= cost) 
    {
      var newCurrent = current - cost; 
      // Update new current.
      admin.database().ref(costPath).set(newCurrent).then(validCallback).catch(err => {
        console.log('Error ', err);
        invalidCallback();
      });
    }
    else
    {
      console.log('Not enough money.');
      invalidCallback();
    }
  });
}

function getCardNumInPack(shopPackID){
  switch(shopPackID)
  {
    case '1':
    case '2':
    {
      return 5;
    }

    default:
    {
      return 0;
    }
  }
}

function writeCardData(playerID, cards, onComplete){

  var cardIDList = [];
  var cardNumList = [];
  for(var i = 0; i < cards.length; ++i)
  {
    var isFound = false;
    for(var j = 0; j < cardIDList.length; ++j)
    {
      if(cardIDList[j] === cards[i])
      {
        // found duplicate card.
        isFound = true;
        cardNumList[j] = cardNumList[j] + 1;
        break;
      }
    }

    if(!isFound)
    {
      cardIDList.push(cards[i]);
      cardNumList.push(1);
    }
  }

  var stashPath = util.format(stashRefPath, playerID);
  //console.log('stashPath : ' + stashPath);

  admin.database().ref(stashPath).once('value').then(function(snapshot) {
    for(var i = 0; i < cardIDList.length; ++i)
    {
      var isFound = false;

      snapshot.forEach(function(childSnapshot) {
        var key = childSnapshot.key;
        var data = Number(childSnapshot.val());

        if(cardIDList[i] === key.toString())
        {
          // Found duplicate card
          isFound = true;
          var newData = data + cardNumList[i];

          var text = util.format(
              '[Stack] >> %s : %d' 
            , cardIDList[i] 
            , newData
          );
          console.log(text);

          if(i + 1 >= cardIDList.length)
          {
            // last one
            admin.database().ref(stashPath + '/' + cardIDList[i]).set(newData).then(onComplete);
          }
          else
          {
            admin.database().ref(stashPath + '/' + cardIDList[i]).set(newData);
          }
          return true; // break foreach 
        }
      });

      if(!isFound)
      {
        // new card
        var text = util.format(
            '[New] >> %s : %d' 
          , cardIDList[i] 
          , cardNumList[i]
        );
        console.log(text);
    
        if(i + 1 >= cardIDList.length)
        {
          // last one
          admin.database().ref(stashPath + '/' + cardIDList[i]).set(cardNumList[i]).then(onComplete);
        }
        else
        {
          admin.database().ref(stashPath + '/' + cardIDList[i]).set(cardNumList[i]);
        }
      }
    }
  });
}

function readCardData(playerID, onComplete){
  var stashPath = util.format(stashRefPath, playerID);
  admin.database().ref(stashPath).once('value').then(function(snapshot) {
    snapshot.forEach(function(childSnapshot) {
      var key = childSnapshot.key;
      var num = Number(childSnapshot.val());

      var text = util.format(
        '%s : %d' 
        , key
        , num
      );
      console.log(text);
    });
    onComplete();
  });
}

// Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions

exports.requestBuyShopPack = functions.https.onRequest((request, response) => {
  
  //console.log(request.url);
  responseText_buyShopPack = '';

  const params = request.url.split('/');
  const shopPackID = params[1];
  const playerHash = params[2];
  console.log(util.format('shopID : %s playerHash : %s', shopPackID, playerHash));

  checkAndBuyShopPack(shopPackID, playerHash, 
    function(){
      // valid
      //console.log('Buy valid!');

      var cards = randomCards(shopPackID, getCardNumInPack(shopPackID));

      if(cards.length > 0)
      {
        var cardText = '';
        for(var i = 0; i < cards.length; ++i)
        {
          if(cardText.length > 0)
          {
            cardText = cardText + ',' + cards[i];
          }
          else
          {
            cardText = cards[i];
          }
        }
        //console.log(cardText);
        
        writeCardData(playerHash, cards, 
          //readData(playerID, 
            function(){
              responseText_buyShopPack = util.format('%d %s', 1, cardText);
              response.status(200).send(responseText_buyShopPack);
            }
          //)
        );

        //responseText = util.format('%d %s', 1, cardText);
        //response.status(200).send(responseText);
      }
      else
      {
        responseText_buyShopPack = util.format('%d', 0);
        response.status(200).send(responseText_buyShopPack);
      }
    }, 
    function(){
      // invalid
      //console.log('Buy invalid!');
      
      responseText_buyShopPack = util.format('%d', 0);
      response.status(200).send(responseText_buyShopPack);
    }
  );
});

/*
function checkAndBuyGoldPack(shopPackID, playerID, validCallback, invalidCallback){
  if(!IsValidPack(shopPackID))
  {
    console.log('Invalid shop pack ID.');
    invalidCallback();
    return;
  }

  var costPath = util.format(GetPackCostPath(shopPackID),  playerID);
  admin.database().ref(costPath).once('value').then(function(snapshot) {
    var key = snapshot.key;
    var current = Number(snapshot.val());

    var cost = GetPackCost(shopPackID); // pack cost
    if(current >= cost) 
    {
      var newCurrent = current - cost; 
      // Update new current.
      admin.database().ref(costPath).set(newCurrent).then(validCallback).catch(err => {
        console.log('Error ', err);
        invalidCallback();
      });
    }
    else
    {
      console.log('Not enough money.');
      invalidCallback();
    }
  });
}
*/

//================================= Request Redeem Gold Pack =================================

function GetReceiptPath(){
  return receiptRefPath;
}

function checkAndRedeemReceipt(transactionID, playerHash, validCallback, invalidCallback){

  var receiptPath = util.format(GetReceiptPath(), playerHash, transactionID);

  console.log('Path: ' + receiptPath);

  admin.database().ref(receiptPath).once('value').then(function(snapshot) {

    if(snapshot.val() != null)
    {
      var goldAmount = Number(snapshot.val().Amount);
      var text = util.format(
          'KEY[%s] TransactionID[%s] IsRedeem[%s] Amount[%d]' 
        , snapshot.key
        , snapshot.val().TransactionID
        , snapshot.val().IsRedeem
        , goldAmount
      );
      console.log(text);

      if(!snapshot.val().IsRedeem)
      {
        admin.database().ref(receiptPath + '/' + 'IsRedeem').set(true).then(function(){ 

          var goldPath = util.format(goldRefPath,  playerHash);
          admin.database().ref(goldPath).once('value').then(function(childSnapshot) {
            
            //var key = childSnapshot.key;
            var current = Number(childSnapshot.val());
            var newCurrent = current + goldAmount; 
            
            responseText_redeemGold += newCurrent;

            // Update new gold
            admin.database().ref(goldPath).set(newCurrent).then(validCallback).catch(err => {
              console.log('Error ', err);
              invalidCallback();
            });
          });
        }).catch(err => {
          console.log('Error ', err);
          invalidCallback();
        });
        return;
      }
      else
      {
        console.log('Failed: already redeem this transaction.');
        invalidCallback(); 
      }
    }
    else
    {
      console.log('Failed: not found this transaction.');
      invalidCallback();        
    }
  });
}

exports.requestRedeemGoldPack = functions.https.onRequest((request, response) => {
  
  //console.log(request.url);
  responseText_redeemGold = '';

  const params = request.url.split('/');
  const playerHash = params[1];
  const transactionID = params[2];

  //console.log(util.format('playerHash : %s transactionID : %s', playerHash, transactionID));
  //responseText_redeemGold += util.format('playerHash : %s transactionID : %s', playerHash, transactionID);

  checkAndRedeemReceipt(transactionID, playerHash, 
    function(newGold){
      response.status(200).send("1 " + responseText_redeemGold);
    }
    , function(){
      response.status(200).send("0");
    }
  );
});
